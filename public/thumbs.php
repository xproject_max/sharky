<?php
	
	// Constants
	$CACHE_DIR = "cache";
	
	function diewith($msg)
	{
		header("location: http://dragon.local/fr/page-introuvable/");
		exit;
	}
	
	// Get params
	$uri = $_REQUEST['uri'] or diewith("missing 'uri' argument.");
	$inWidth = number_format($_REQUEST['w'],0,'','');
	$inHeight = $_REQUEST['h'];
	$method = $_REQUEST['method'];
	$ratioChoice = isset($_REQUEST['ratio']) ? $_REQUEST['ratio'] : '';
	
	// Handle client cache (304)
	$srcTime = @filemtime($uri) or diewith("Unable to open 'uri'");
	$reqTimeStr = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']);
	
	// Browser cache version not too old ?
	if ((!empty($reqTimeStr)) and ($srcTime <= strtotime($reqTimeStr))) {
		// End the request with status 304
		header("HTTP/1.1 304 Not modified");
		exit;
	} else {
		// Set the last change in HTTP reponse
		header("Last-Modified: " . date('r', $srcTime));
	}
	
	// Get actual size of source image
	$imgInfo = getimagesize($uri) or diewith("Unable to open '$uri'");
	$srcWidth = $imgInfo[0];
	$srcHeight = $imgInfo[1];
	$srcType = $imgInfo[2];
	switch ($srcType) {
		case 1 :
			$srcType = "gif";
			break;
		case 2 :
			$srcType = "jpeg";
			break;
		case 3 :
			$srcType = "png";
			break;
		default:
			$srcType = "???";
	}
	
	// Compute the size wanted
	if ($method == "stretch") {
		
		// Exact size
		$outWidth = $inWidth;
		$outHeight = $inHeight;
		
	} else {
		if ($method == "resize") {
			$xRatio = ($inWidth) ? ($srcWidth / $inWidth) : 0;
			$yRatio = ($inHeight) ? ($srcHeight / $inHeight) : 0;
			$ratio = min($xRatio, $yRatio);
			$outWidth = intval($srcWidth / $ratio);
			$outHeight = intval($srcHeight / $ratio);
		} else {
			if ($method == "fitLargest") {
				$xRatio = ($inWidth) ? ($srcWidth / $inWidth) : 0;
				$yRatio = ($inHeight) ? ($srcHeight / $inHeight) : 0;
				$ratio = max($xRatio, $yRatio);
				$outWidth = intval($srcWidth / $ratio);
				$outHeight = intval($srcHeight / $ratio);
			} else {/* Default : 'fit' */
				// Max size : resize
				// Max size : resize
				$xRatio = ($inWidth) ? ($srcWidth / $inWidth) : 0;
				$yRatio = ($inHeight) ? ($srcHeight / $inHeight) : 0;
				switch ($ratioChoice) {
					case  'l':
						$ratio = max($xRatio, 1);
						break;
					case 'h':
						$ratio = max($yRatio, 1);
						break;
					default:
						$ratio = max($xRatio, $yRatio, 1);
				}
				$outWidth = intval($srcWidth / $ratio);
				$outHeight = intval($srcHeight / $ratio);
			}
		}
	}
	
	// Compute name of cache image
	$cacheName = md5($uri) . '-' . basename($uri) . '#' . $outWidth . 'x' . $outHeight;
	$cacheFile = dirname(__FILE__) . '/' . $CACHE_DIR . '/' . $cacheName;
	
	// If cache doesn't exist or too old, build it.
	if (!file_exists($cacheFile) or ($srcTime > filectime($cacheFile))) {
		
		//if ($imgInfo[0]<$outWidth){$outWidth=$imgInfo[0];}
		//if ($imgInfo[1]<$outHeight){$outHeight=$imgInfo[1];}
		
		// Create output image
		$outImg = imagecreatetruecolor($outWidth, $outHeight);
		
		// Load src image
		switch ($srcType) {
			case "png":
				$srcImg = imagecreatefrompng($uri);
				$blending = false;
				break;
			case "gif":
				$srcImg = imagecreatefromgif($uri);
				$blending = true;
				break;
			case "jpeg":
				$srcImg = imagecreatefromjpeg($uri);
				break;
			default:
				diewith("unsupported file type '$uri'");
		};
		
		// preserve transparency for PNG and GIF images
		if ($srcType == 'png' || $srcType == 'gif') {
			// allocate a color for thumbnail
			$background = imagecolorallocate($outImg, 0, 0, 0);
			// define a color as transparent
			imagecolortransparent($outImg, $background);
			// set the blending mode for thumbnail
			imagealphablending($outImg, $blending);
			// set the flag to save alpha channel
			imagesavealpha($outImg, true);
		}
		
		// Resize image
		imagecopyresampled($outImg, $srcImg, 0, 0, 0, 0, $outWidth, $outHeight, $srcWidth, $srcHeight);
		
		// Save to cached thumb
		switch ($srcType) {
			case "png":
				$res = imagepng($outImg, $cacheFile);
				break;
			case "gif":
				$res = imagegif($outImg, $cacheFile);
				break;
			case "jpeg":
				$res = imagejpeg($outImg, $cacheFile, 85);
				break;
			default:
				diewith("unsupported file type '$uri'");
		}
		
		// Check result
		if (!$res)
			diewith("Unable to save thumb to '$cacheFile'. Check the access right of the HTTP server.");
	}
	
	// HTTP Header
	header("Content-Type:image/$srcType");
	
	// Dump cache file
	readfile($cacheFile) or diewith("Unable to open cached thumb '$cacheFile'");
?>
