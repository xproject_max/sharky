var is_running,
mobile,
urls,
url_cart,
heightS,
scrollLoco,
lastIndex;

scrollLoco="";
is_running = false;
mobile = false;
urls = $('#url_scripts').data('url-scripts');
url_cart = $('#url_cart').data('url-cart');
heightS = $(window).height();
lastIndex = 0; 

var falseimp = false;
var falsemet = false;
var falseimpcy = false;
var falsemetric = false;
var addTotalf = false;
var addTotalimf = false;
var isImp = false;
var isImpCy = false;

//detect if mobile
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {  
	mobile=true;
} 
 
 
CLIENT = {
	
    initialize: function () { 
			 
			if( scrollLoco != ""){
				 scrollLoco.destroy()
			}
			scrollLoco = new LocomotiveScroll({
			el: document.querySelector('[data-scroll-container]'),
			elMobile: document.querySelector('[data-scroll-container]'),
			name: 'scroll',
			offset: [0, 0],
			repeat: true,
			smooth: true, // smooth scroll
			smoothMobile: true, // smooth scroll on mobile
			direction: 'vertical', // or horizontal
			lerp: 1, // inertia
			class: 'is-inview',
			scrollbarClass: 'c-scrollbar',
			scrollingClass: 'has-scroll-scrolling',
			draggingClass: 'has-scroll-dragging',
			smoothClass: 'has-scroll-smooth',
			initClass: 'has-scroll-init',
			getSpeed: true,
			getDirection: true,
			multiplier: 0.75,
			firefoxMultiplier: 15,
			touchMultiplier: 15,
			scrollFromAnywhere: true
		}); 
		
		setTimeout(function () {


		 ScrollTrigger.refresh();
		
			$('[data-scroll-section]').each(function(){
		  
					$(this).css('marginTop', '-2px');  
					$(this).css('transition', 'all 1s');   
			});
			
			
			gsap.registerPlugin(ScrollTrigger);
			 
			ScrollTrigger.scrollerProxy(document.querySelector('[data-scroll-container]'), {
				  scrollTop(value) {
					return arguments.length
					  ? scrollLoco.scrollTo(value, 0, 0)
					  : scrollLoco.scroll.instance.scroll.y;
				  },
				  getBoundingClientRect() {
					return {
					  left: 0,
					  top: 0,
					  width: window.innerWidth,
					  height: window.innerHeight
					};
				  },
				  pinType: document.querySelector('[data-scroll-container]').style.transform ? "transform" : "fixed"
			}); 
			
			var isScrollingLoco = true;
			scrollLoco.on("scroll", () =>{
				ScrollTrigger.update;
				if(isScrollingLoco){
					isScrollingLoco = false;
					scrollLoco.update();
					setTimeout(function(){
						isScrollingLoco = true;
					}, 2000);
				}
			}); 
			 
			
			barba.hooks.beforeLeave(data => {
				scrollLoco.destroy(); 
				 
			});
			 
			barba.hooks.enter(data => { 
				scrollLoco.destroy(); 
				$('[data-scroll-section]').each(function(){
		  
					$(this).css('marginTop', '-2px');  
					$(this).css('transition', 'all 1s');   
			});
			 
			});

			// update the scroll after entering a page
			barba.hooks.after(() => {  
				jQuery('body').removeClass('nav-active');  
				setTimeout(function(){ 
					CLIENT.initialize(); 
					MENU.initialize();   
					 
				},1000);
				
			});

			 
			$('canvas').attr("width", "auto");
            //page d'accueil
			if ($('#content.home').length > 0) {
				home.initialize();
				console.log("home");
			}
			if ($('#content.about').length > 0) {
				about.initialize();
				console.log("about");
			}
			
			if ($('#content.services').length > 0) {
				services.initialize();
				console.log("services");
			}
			if ($('#content.single_service').length > 0) {
				single_service.initialize();
			}
			if ($('#content.blogs').length > 0) {
				blogs.initialize();
			}
			if ($('#content.single_blog').length > 0) {
				single_blog.initialize();
			}
			if ($('#content.contact').length > 0) {
				contact.initialize();
			}
            if ($('#content.thankyou').length > 0) {
				thankyou.initialize();
			}
			if ($('#content.shop').length > 0) {
				shop.initialize();
			}
			if ($('#content.single_product').length > 0) {
				single_product.initialize();
			}
			if ($('#content.users').length > 0) {
				users.initialize();
			}
			if ($('#content.single_user').length > 0) {
				single_user.initialize();
			}
			if ($('#content.submission').length > 0) {
				submission.initialize();
            }
			if ($('#content.realisations').length > 0) {
				realisations.initialize();
            }
			if ($('#content.single_realisation').length > 0) {
				single_realisation.initialize();
            }
			if ($('#content.projects').length > 0) {
				projects.initialize();
            }
			if ($('#content.single_project').length > 0) {
				single_project.initialize();
            }
			if ($('#content.calendar').length > 0) {
				calendar.initialize();
			}
			if ($('#content.calculator').length > 0) {
				calculator.initialize();
			}
			if ($('#content.maintenance').length > 0) {
				maintenance.initialize();
			}
			if ($('#content.error').length > 0) {
				error.initialize();
			}
			if ($('#content.faq').length > 0) {
				faq.initialize();
			}
			if ($('#content.sitemap').length > 0) {
				sitemap.initialize();
			}
			if ($('#content.page').length > 0) {
				page.initialize();
			}
			if ($('#content.politiques_terms').length > 0) {
				politiques_terms.initialize();
			}
			if ($('#content.subscription').length > 0) {
				subscription.initialize();
			}
			if ($('#content.cart').length > 0) {
				cart.initialize();
			}
			if ($('#content.checkout').length > 0) {
				checkout.initialize();
			}
			if ($('#content.collections').length > 0) {
				collections.initialize();
			}
			if ($('#content.single_collection').length > 0) {
				single_collection.initialize();
			}
			if ($('#content.payment_failed').length > 0) {
				payment_failed.initialize();
			}
			if ($('#content.payment_thankyou').length > 0) {
				payment_thankyou.initialize();
			}
			if ($('#content.pay_orders').length > 0) {
				pay_orders.initialize();
			}
			if ($('#content.promotions').length > 0) {
				promotions.initialize();
			}
			if ($('#content.single_promotion').length > 0) {
				single_promotion.initialize();
			}
			if ($('#content.activation').length > 0) {
				activation.initialize();
			}
			if ($('#content.login').length > 0) {
				login.initialize();
			}
			if ($('#content.recovery').length > 0) {
				recovery.initialize();
			}
			if ($('#content.contests').length > 0) {
				contests.initialize();
			}
			if ($('#content.single_contest').length > 0) {
				single_contest.initialize();
			}

			
			if($('.c-scrollbar').length > 1){
				$('.c-scrollbar').remove();
				$('.c-scrollbar').init();
			}
			
			var navFixe = false;
				
			var lastdistance = 1000;
			scrollLoco.on("scroll", (obj) => {
				
			  if (obj == "undefined") return;
			  if (obj.direction == "undefined") return;
			  if (obj.scroll == "undefined") return;
			  if (obj.scroll.y == "undefined") return;
			  wheel = obj.direction; 
			  if(obj.scroll.y < $('#content').offset().top && obj.scroll.y > 50){
					$('#nav-menu').css('z-index', '-1'); 
				}
				if(obj.scroll.y < lastdistance && obj.scroll.y > 50 && obj.scroll.y > $('#content').offset().top ){
					
					if(!$('#nav-menu').hasClass('fixed')){
					
						$('#nav-menu').css('z-index', '9999'); 
						$('#nav-menu').addClass('fixed'); 
					}
			   }else{ 
					if( obj.scroll.y < 50){
						$('#nav-menu').css('z-index', '9999'); 
					}
					$('#nav-menu').removeClass('fixed');  
			   }
			   if(!navFixe){
				   navFixe = true;
				   setTimeout(function(){
					lastdistance = obj.scroll.y;
					navFixe = false;
				   },1000);
			   }

			}); 
			gsap.registerPlugin(MorphSVGPlugin);
			gsap.to($("#twittersvg"), { 
				morphSVG:"#facebooksvg",
				scrollTrigger: { 
					trigger: "#content",
					start: "-= 500px",
					end: "bottom 50%",
					scrub: true, 
				}
			});
			gsap.to('#scrollProgress', {
			  value: 100,
			  ease: 'none',
			  scrollTrigger: { scrub: 0.3 }
			}); 
		}, 100);
 
    }
};

$(function (e) {
	
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) { 
		mobile=true;
    }  
	 
	
    home = {
        initialize: function (e) {
            if ($('#content.home').length > 0) { 
				 
				//Filtering gallery
				$('.subcateg').on('click',{ passive: true}, function(e){
					e.preventDefault();
					$('.filter_categs div').removeClass('active');
					$(this).parents('div.categ').addClass('active');
					$(this).addClass('active');
					//get the id to filtering
					var idSubCateg = $(this).data('filter-realisation'); 
					$.ajax({
						type: "post",
						url: urls+"ajax.php",
						data: {filter_realisations: true, idSubCateg: idSubCateg},
						success: function(data){
							$('.slick').slick( 'unslick' ); // Remove slick
							$('.slick').empty(); // Remove elements
							$('.slick').html(data);
							if(data.indexOf("isEmpty")>=0){
								
								init_slider(true);
							}else{
								init_slider();
							}
						}
					});
					
				});
				  
						
				 function resetSlick($slick_slider, settings) {
					$(window).on('resize', function() {
						if ($(window).width() < 320) {
							if ($slick_slider.hasClass('slick-initialized')) {
								$slick_slider.slick('unslick');
							}
							return
						}

						if (!$slick_slider.hasClass('slick-initialized')) {
							return $slick_slider.slick(settings);
						}
					});
				}
				
				function init_slider($empty=false){
					slick_slider = $('#slick1');
					settings = {
						rows: ($empty ? 1 : 2),
						slidesToShow: 3,
						slidesToScroll: 1,
						autoplay: true,
						autoplaySpeed: 2000,  
						pauseOnHover: true,

						responsive: [
							{
								breakpoint: 1024,
								settings: {
									rows: 1,
									slidesToShow: 1,
									slidesToScroll: 1, 
								}
							}, 
							{
								breakpoint: 480,
								settings: {
									rows: 1,
									slidesToShow: 1,
									slidesToScroll: 1, 
								}
							}
							// You can unslick at a given breakpoint now by adding:
							// settings: "unslick"
							// instead of a settings object
						]

					};
					
					slick_slider.slick(settings);
					resetSlick(slick_slider, settings);
				}
				//contact form labels 
				$('input,textarea').on('change', function() {
					var input = $(this);
					if (input.val().length) {
					  input.addClass('populated');
					} else {
					  input.removeClass('populated');
					}
				  }); 
            }
        }
    };
    about = {
        initialize: function (e) {
            if ($('#content.about').length > 0) {
                 
            }
        }
    };
    services = {
        initialize: function (e) {
            if ($('#content.services').length > 0) {
                 $('.filters_rectangle a').on('click',{ passive: true}, function(e) {
					 
					e.preventDefault(); 
					scrollLoco.scrollTo($(this).attr('href'));
				 });
            }
        }
    };
    single_service = {
        initialize: function (e) {
            if ($('#content.single_service').length > 0) {
                 
            }
        }
    };
    blogs = {
        initialize: function (e) {
            if ($('#content.blogs').length > 0) {
                 
            }
        }
    };
    single_blog = {
        initialize: function (e) {
            if ($('#content.single_blog').length > 0) {
                 
            }
        }
    };
    contact = {
        initialize: function (e) {
            if ($('#content.contact').length > 0) {
				//contact form labels 
				$('input,textarea').on('change', function() {
					var input = $(this);
					if (input.val().length) {
					  input.addClass('populated');
					} else {
					  input.removeClass('populated');
					}
				  }); 
            }
        }
    };
    thankyou = {
        initialize: function (e) {
            if ($('#content.thankyou').length > 0) {

            }
 
        }
    };
    shop = {
        initialize: function (e) {
            if ($('#content.shop').length > 0) {

            }
 
        }
    };
    single_product = {
        initialize: function (e) {
            if ($('#content.single_product').length > 0) {

            }
 
        }
    };
    users = {
        initialize: function (e) {
            if ($('#content.users').length > 0) {

            }
 
        }
    };
    single_user = {
        initialize: function (e) {
            if ($('#content.single_user').length > 0) {

            }
 
        }
    };
    submission = {
        initialize: function (e) {
            if ($('#content.submission').length > 0) {

            }
 
        }
    };
    realisations = {
        initialize: function (e) {
			if ($('#content.realisations').length > 0) {
				//Filtering gallery
				$('.subcateg').on('click',{ passive: true}, function(e){
					e.preventDefault();
					$('.filter_categs div').removeClass('active');
					$(this).parents('div.categ').addClass('active');
					$(this).addClass('active');
					//get the id to filtering
					var idSubCateg = $(this).data('filter-realisation'); 
					$.ajax({
						type: "post",
						url: urls+"ajax.php",
						data: {filter_realisations_noslider: true, idSubCateg: idSubCateg},
						success: function(data){
							$('#section_realisation').html(data); 
						}
					});
					
				});
            }
        }
    };
	calculator = {
		initialize: function (e){
			if($('#content.calculator').length > 0){
				 

				var tabs            = $('.tabs');
				var tabList         = $('.tabs__list');
				var tabItem         = $('.tabs__item');
				var tabItemActive   = $('.tabs__item--active');
				var tabLink         = $('.tabs__link');
				
				var width            = $(window).width();

				var tabSwitcher = function() {
					// On tab link click
					tabLink.on('click',{ passive: true}, function(e) {
						var currentAttrValue = jQuery(this).attr('href');

						// Show/Hide Tabs
						$('.tabs ' + currentAttrValue).addClass('tabs__area--active').siblings().removeClass('tabs__area--active');

						// Change/remove current tab to active
						$(this).parent('li').addClass('tabs__item--active').siblings().removeClass('tabs__item--active');

						e.preventDefault();
					});
				};

				var tabToggle = function() {
					tabItem.on('click',{ passive: true}, function(e) {
						$(this).parent(tabList).toggleClass('tabs__list--open');
					});
				};

				var tabController = function() {
					tabToggle();
				};

				 
				$(window).resize(function() {
					var width = $(window).width();
					if ($(window).width() != width) {
						width = $(window).width();
						tabController();
					}
				});

				tabController();
				tabSwitcher();
				
				
				//change metrique et imperial
				
				$('.convertImp').on('click',{ passive: true}, function(e){
					e.preventDefault();
					e.stopImmediatePropagation();
					e.stopPropagation();
					 
					convertToMetrique(); 
				});
				//add totals
				$('.addTotal').on('click',{ passive: true}, function(e){
					e.preventDefault();
					e.stopImmediatePropagation();
					e.stopPropagation();
					
					addTotal();
				});
				$('.addTotalImp').on('click',{ passive: true}, function(e){
					e.preventDefault();
					e.stopImmediatePropagation();
					e.stopPropagation();
					
					addTotalImp();
				});
				$('.remove').on('click',{ passive: true}, function(e){
					e.preventDefault();
					e.stopPropagation();
					
					var id= $(this).data('remove');
					
					$.ajax({
						type: "post",
						url: urls+"ajax.php",
						data: {
							removerow: true,  
							id: id
						},
						success: function(data){
							
							updateTotals();
							
						}
					});
				});
				$('.convertImpCylindre').on('click',{ passive: true}, function(e){
					e.preventDefault();
					e.stopPropagation();
					 
					convertToMetriqueCylindre();
					 
				});
				$('.addTotalCylindre').on('click',{ passive: true}, function(e){
					e.preventDefault();
					e.stopImmediatePropagation();
					e.stopPropagation();
					
					addTotalCylindre();
				});
				$('.addTotalCylindreImp').on('click',{ passive: true}, function(e){
					e.preventDefault();
					e.stopImmediatePropagation();
					e.stopPropagation();
					
					addTotalCylindreImp();
				});
				
				
				function convertToImp(){
					//getting/setting var
					var longueur = $('.longeur_input input');
					var largeur = $('.largeur_input input');
					var epaisseur = $('.epaisseur_input input');
					var mesurelongueur = $('.longeur_mesure span');
					var mesurelargeur = $('.largeur_mesure span');
					var mesureepaisseur = $('.epaisseur_mesure span');
					
					mesurelongueur.html("Pied(s)");
					mesurelargeur.html("Pied(s)");
					mesureepaisseur.html("Pouce(s)");
					
					longueur.val(parseFloat(longueur.val() * 3.281));
					largeur.val(parseFloat(largeur.val() * 3.281));
					epaisseur.val(parseFloat(epaisseur.val() * 0.3937));
					
					
					$('.addTotal').addClass('addTotalImp');
					$('.addTotal').removeClass('addTotal'); 
					
					if(!falseimp){
						falseimp=true;
						setTimeout(function(){
							falseimp = false;
							calculator.initialize();
						},100);
					}
				}
				
				function convertToMetrique(){
					if(isImp){
						isImp = false;
						//getting/setting var
						var longueur = $('.longeur_input input');
						var largeur = $('.largeur_input input');
						var epaisseur = $('.epaisseur_input input');
						var mesurelongueur = $('.longeur_mesure span');
						var mesurelargeur = $('.largeur_mesure span');
						var mesureepaisseur = $('.epaisseur_mesure span');
						
						mesurelongueur.html("M");
						mesurelargeur.html("M");
						mesureepaisseur.html("CM");
						
						longueur.val(parseFloat(longueur.val() / 3.281));
						largeur.val(parseFloat(largeur.val() / 3.281));
						epaisseur.val(parseFloat(epaisseur.val() / 0.3937));
						
						
						$('.addTotalImp').addClass('addTotal');
						$('.addTotalImp').removeClass('addTotalImp');
						
						if(!falsemet){
							falsemet=true;
							setTimeout(function(){
								falsemet = false;
								calculator.initialize();
							},100);
						}
					}else{
						isImp = true;
							//getting/setting var
						var longueur = $('.longeur_input input');
						var largeur = $('.largeur_input input');
						var epaisseur = $('.epaisseur_input input');
						var mesurelongueur = $('.longeur_mesure span');
						var mesurelargeur = $('.largeur_mesure span');
						var mesureepaisseur = $('.epaisseur_mesure span');
						
						mesurelongueur.html("Pied(s)");
						mesurelargeur.html("Pied(s)");
						mesureepaisseur.html("Pouce(s)");
						
						longueur.val(parseFloat(longueur.val() * 3.281));
						largeur.val(parseFloat(largeur.val() * 3.281));
						epaisseur.val(parseFloat(epaisseur.val() * 0.3937));
						
						 
						calculator.initialize();  
					}
				}
				function convertToImpCylindre(){
					//getting/setting var
					var diametre = $('.diametre_inputCylindre input');
					var hauteur = $('.hauteur_inputCylindre input'); 
					var mesurelongueur = $('.diametre_mesureCylindre span');
					var mesurelargeur = $('.hauteur_mesureCylindre span'); 
					
					mesurelongueur.html("Pouce(s)");
					mesurelargeur.html("Pied(s)"); 
					
					diametre.val(parseFloat(diametre.val() * 0.3937));
					hauteur.val(parseFloat(hauteur.val() * 30.48)); 
					
					$('.addTotalCylindre').addClass('addTotalCylindreImp');
					$('.addTotalCylindre').removeClass('addTotalCylindre');
					
					if(!falseimpcy){
						falseimpcy=true;
						setTimeout(function(){
							falseimpcy = false;
							calculator.initialize();
						},100);
					}
				}
				function convertToMetriqueCylindre(){
					if(isImpCy){
						isImpCy=false;
						//getting/setting var
						var diametre = $('.diametre_inputCylindre input');
						var hauteur = $('.hauteur_inputCylindre input'); 
						var mesurelongueur = $('.diametre_mesureCylindre span');
						var mesurelargeur = $('.hauteur_mesureCylindre span'); 
						
						mesurelongueur.html("CM");
						mesurelargeur.html("CM"); 
						
						diametre.val(parseFloat(diametre.val() / 0.3937));
						hauteur.val(parseFloat(hauteur.val() / 30.48)); 
						
						$('.addTotalCylindreImp').addClass('addTotalCylindre');
						$('.addTotalCylindreImp').removeClass('addTotalCylindreImp');
						if(!falsemetric){
							falsemetric=true;
							setTimeout(function(){
								falsemetric = false;
								calculator.initialize();
							},100);
						}
					}else{
						
						isImpCy=true;
						
						//getting/setting var
						var diametre = $('.diametre_inputCylindre input');
						var hauteur = $('.hauteur_inputCylindre input'); 
						var mesurelongueur = $('.diametre_mesureCylindre span');
						var mesurelargeur = $('.hauteur_mesureCylindre span'); 
						
						mesurelongueur.html("Pouce(s)");
						mesurelargeur.html("Pied(s)"); 
						
						diametre.val(parseFloat(diametre.val() * 0.3937));
						hauteur.val(parseFloat(hauteur.val() * 30.48)); 
						
						$('.addTotalCylindre').addClass('addTotalCylindreImp');
						$('.addTotalCylindre').removeClass('addTotalCylindre');
						
						if(!falseimpcy){
							falseimpcy=true;
							setTimeout(function(){
								falseimpcy = false;
								calculator.initialize();
							},100);
						}
					}
				}
				function addTotal(){
					if(!isImp){
						var name = $('.nom_input input').val();
						var longueur = $('.longeur_input input').val();
						var largeur = $('.largeur_input input').val();
						var epaisseur = $('.epaisseur_input input').val();
						var mesurelongueur = $('.longeur_mesure span').val();
						var mesurelargeur = $('.largeur_mesure span').val();
						var mesureepaisseur = $('.epaisseur_mesure span').val();
						
						$.ajax({
							type: "post",
							url: urls+"ajax.php",
							data: {
								addT: true, 
								name: name,
								longueur: longueur,
								largeur :largeur,
								epaisseur:epaisseur,
								mesurelongueur:mesurelongueur,
								mesurelargeur:mesurelargeur,
								mesureepaisseur:mesureepaisseur
							},
							success: function(data){
								
								updateTotals();
								if(!addTotalf){
									addTotalf=true;
									setTimeout(function(){
										addTotalf = false;
										calculator.initialize();
									},100);
								}
							}
						});
					}else{
						var name = $('.nom_input input').val();
						var longueur = $('.longeur_input input').val();
						var largeur = $('.largeur_input input').val();
						var epaisseur = $('.epaisseur_input input').val();
						var mesurelongueur = $('.longeur_mesure span').val();
						var mesurelargeur = $('.largeur_mesure span').val();
						var mesureepaisseur = $('.epaisseur_mesure span').val();
						
						$.ajax({
							type: "post",
							url: urls+"ajax.php",
							data: {
								addTImp: true, 
								name: name,
								longueur: longueur,
								largeur :largeur,
								epaisseur:epaisseur,
								mesurelongueur:mesurelongueur,
								mesurelargeur:mesurelargeur,
								mesureepaisseur:mesureepaisseur
							},
							success: function(data){
								
								updateTotals();
								if(!addTotalimf){
									addTotalimf=true;
									setTimeout(function(){
										addTotalimf = false;
										calculator.initialize();
									},100);
								}
							}
						});
					}
				}
				function addTotalImp(){
					
					var name = $('.nom_input input').val();
					var longueur = $('.longeur_input input').val();
					var largeur = $('.largeur_input input').val();
					var epaisseur = $('.epaisseur_input input').val();
					var mesurelongueur = $('.longeur_mesure span').val();
					var mesurelargeur = $('.largeur_mesure span').val();
					var mesureepaisseur = $('.epaisseur_mesure span').val();
					
					$.ajax({
						type: "post",
						url: urls+"ajax.php",
						data: {
							addTImp: true, 
							name: name,
							longueur: longueur,
							largeur :largeur,
							epaisseur:epaisseur,
							mesurelongueur:mesurelongueur,
							mesurelargeur:mesurelargeur,
							mesureepaisseur:mesureepaisseur
						},
						success: function(data){
							
							updateTotals();
							if(!addTotalimf){
								addTotalimf=true;
								setTimeout(function(){
									addTotalimf = false;
									calculator.initialize();
								},100);
							}
						}
					});
				}
				
				function addTotalCylindre(){
					if(!isImpCy){
						var name = $('.nom_inputCylindre input').val();
						var diametre = $('.diametre_inputCylindre input').val();
						var hauteur = $('.hauteur_inputCylindre input').val(); 
						
						$.ajax({
							type: "post",
							url: urls+"ajax.php",
							data: {
								addTCylindre: true, 
								name: name,
								diametre: diametre,
								hauteur :hauteur
							},
							success: function(data){
								
								updateTotals();
								
								if(!addTotalimf){
									addTotalimf=true;
									setTimeout(function(){
										addTotalimf = false;
										calculator.initialize();
									},100);
								}
							}
						});
					}else{
						var name = $('.nom_inputCylindre input').val();
						var diametre = $('.diametre_inputCylindre input').val();
						var hauteur = $('.hauteur_inputCylindre input').val();  
						
						$.ajax({
							type: "post",
							url: urls+"ajax.php",
							data: {
								addTotalCylindreImp: true, 
								name: name,
								diametre: diametre,
								hauteur :hauteur
							},
							success: function(data){
								
								updateTotals();
								
								if(!addTotalimf){
									addTotalimf=true;
									setTimeout(function(){
										addTotalimf = false;
										calculator.initialize();
									},100);
								}
							}
						});
					}
				}
				function addTotalCylindreImp(){
					
					var name = $('.nom_inputCylindre input').val();
					var diametre = $('.diametre_inputCylindre input').val();
					var hauteur = $('.hauteur_inputCylindre input').val();  
					
					$.ajax({
						type: "post",
						url: urls+"ajax.php",
						data: {
							addTotalCylindreImp: true, 
							name: name,
							diametre: diametre,
							hauteur :hauteur
						},
						success: function(data){
							
							updateTotals();
							
							if(!addTotalimf){
								addTotalimf=true;
								setTimeout(function(){
									addTotalimf = false;
									calculator.initialize();
								},100);
							}
						}
					});
				}
				
				function updateTotals(){
					//refresh options
					$.ajax({
						type: "post",
						url: urls+"ajax.php",
						data: { 
							refreshOptions: true
						},
						success: function(data){
							
							$('.wrapper_options').html(data);
						}
					});
					
					//refresh total
					$.ajax({
						type: "post",
						url: urls+"ajax.php",
						data: { 
							refreshTotal: true
						},
						success: function(data){
							
							$('.resultTotal').html(data);
							if(!addTotalimf){
								addTotalimf=true;
								setTimeout(function(){
									addTotalimf = false;
									calculator.initialize();
								},100);
							}
						}
					});
				}
				
			}
		}
	};
    single_realisation = {
        initialize: function (e) {
			if ($('#content.single_realisation').length > 0) {

            }
 
        }
    };
    projects = {
        initialize: function (e) { 
			if ($('#content.projects').length > 0) {

            }
		}
    };
    single_project = {
        initialize: function (e) {
			if ($('#content.single_project').length > 0) {

            }
             
		}
	};
	calendar = {
        initialize: function (e) {
			if ($('#content.calendar').length > 0) {

            }
            
        }
    };
	maintenance = {
        initialize: function (e) {
			if ($('#content.maintenance').length > 0) {

            }
            
        }
    };
	error = {
        initialize: function (e) {
			if ($('#content.error').length > 0) {

            }
            
        }
    };
	faq = {
        initialize: function (e) {
			if ($('#content.faq').length > 0) {

            }
            
        }
    };
	sitemap = {
        initialize: function (e) {
			if ($('#content.sitemap').length > 0) {

            }
            
        }
    };
	page = {
        initialize: function (e) {
			if ($('#content.page').length > 0) {

            }
            
        }
    };
	politiques_terms = {
        initialize: function (e) {
			if ($('#content.politiques_terms').length > 0) {

            }
            
        }
    };
	subscription = {
        initialize: function (e) {
			if ($('#content.subscription').length > 0) {

            }
            
        }
    };
	cart = {
        initialize: function (e) {
			if ($('#content.cart').length > 0) {

            }
            
        }
    };
	checkout = {
        initialize: function (e) {
			if ($('#content.checkout').length > 0) {

            }
            
        }
    };
	collections = {
        initialize: function (e) {
			if ($('#content.collections').length > 0) {

            }
            
        }
    };
	single_collection = {
        initialize: function (e) {
			if ($('#content.single_collection').length > 0) {

            }
            
        }
    };
	payment_failed = {
        initialize: function (e) {
			if ($('#content.payment_failed').length > 0) {

            }
            
        }
    };
	payment_thankyou = {
        initialize: function (e) {
			if ($('#content.payment_thankyou').length > 0) {

            }
            
        }
    };
	pay_orders = {
        initialize: function (e) {
			if ($('#content.pay_orders').length > 0) {

            }
            
        }
    };
	promotions = {
        initialize: function (e) {
			if ($('#content.promotions').length > 0) {

            }
            
        }
    };
	single_promotion = {
        initialize: function (e) {
			if ($('#content.single_promotion').length > 0) {

            }
            
        }
    };
	activation = {
        initialize: function (e) {
			if ($('#content.activation').length > 0) {

            }
            
        }
    };
	login = {
        initialize: function (e) {
			if ($('#content.login').length > 0) {

            }
            
        }
    };
	recovery = {
        initialize: function (e) {
			if ($('#content.recovery').length > 0) {

            }
            
        }
    };
	contests = {
        initialize: function (e) {
			if ($('#content.contests').length > 0) {

            }
            
        }
    };
	single_contest = {
        initialize: function (e) {
			if ($('#content.single_contest').length > 0) {

            }
            
        }
    };
	 

	// Globals
	if ($('.toppage').length > 0) {
		$('.toppage').on('click',{ passive: true}, function(e){
			e.preventDefault();
			$("html, body").animate({ scrollTop:0 }, 3000);
		});
	} 

});	

simulateCssEvent = function(type){
    var id = 'simulatedStyle';

    var generateEvent = function(selector){
        var style = "";
        for (var i in document.styleSheets) {
            var rules = document.styleSheets[i].cssRules;
            for (var r in rules) {
                if(rules[r].cssText && rules[r].selectorText){
                    if(rules[r].selectorText.indexOf(selector) > -1){
                        var regex = new RegExp(selector,"g");
                        var text = rules[r].cssText.replace(regex,"");
                        style += text+"\n";
                    }
                }
            }
        }
        $("head").append("<style id="+id+">"+style+"</style>");
    };

    var stopEvent = function(){
        $("#"+id).remove();
    };

    switch(type) {
        case "hover":
            return generateEvent(":hover");
        case "stop":
            return stopEvent();
    }
}