<?php
// Grab our config settings
require_once('../administration/config.php');
 
// Grab the FreakMailer class
require_once('MailClass.php');
 
// instantiate the class
$mailer = new FreakMailer();
 
// Set the subject
$mailer->Subject = 'This is a test';
 
// Body
$mailer->Body = 'This is a test of my mail system!';
 
// Add an address to send to.
//$mailer->AddAddress('p.simard@cameleonmedia.com', 'Patrick Simard');
$mailer->AddAddress('drisate@hotmail.com', 'Patrick Simard');

if(!$mailer->Send())
{
    echo 'There was a problem sending this mail!';
}
else
{
    echo 'Mail sent!';
}
$mailer->ClearAddresses();
$mailer->ClearAttachments();
?>