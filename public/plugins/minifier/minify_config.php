<?php

	$dir = __DIR__;
	require(	dirname($dir).'/lessc.inc.php');
	require_once (dirname($dir).'/scss/scss.inc.php');
	require_once(dirname($dir).'/minifier/minify/src/Minify.php');
	require_once(dirname($dir).'/minifier/minify/src/JS.php');
	require_once(dirname($dir).'/minifier/minify/src/Exception.php');
	require_once(dirname($dir).'/minifier/path_converter/src/ConverterInterface.php');
	require_once(dirname($dir).'/minifier/path_converter/src/Converter.php');
	use MatthiasMullie\Minify;
	use ScssPhp\ScssPhp\Compiler;
	$compile = true;
	if($compile){
 
			
			
			//CSS WEBSITE CLIENT
			$compile_css = true;
			
			
			if(
				$compile_css &&
				(
					(file_exists(dirname(dirname($dir)).'/css/style.less') && filemtime(dirname(dirname($dir)).'/css/style.less') > filemtime(dirname(dirname($dir)).'/css/style.min.css'))
				)
			){
				//CSS/LESS
				$styles = array();
				$styles['less']="";
				$styles['scss']="";
				$styles['css']="";

				$style_files = array(   
					dirname(dirname($dir)).'/css/style.less'

				);



				foreach($style_files as $style_file){
					if(preg_match('/.less$/', $style_file)){
						$styles['less'] .= file_get_contents($style_file);
					}
					else if(preg_match('/.scss$/', $style_file)){
						$styles['scss'] .= file_get_contents($style_file);
					}
					else if(preg_match('/.css$/', $style_file)){
						$styles['css'] .= file_get_contents($style_file);
					}
				}

				if($styles['less'] != ''){
					try {
						$less = new lessc;
						$less->setImportDir(dirname(dirname($dir)).'/css');
						$compiled_css = $less->compile($styles['less']);

						if ($compiled_css) {
							$styles['css'] .= $compiled_css;
						}
					} catch (exception $e) {
						echo "LESS COMPILATION ERROR => " . $e->getMessage();
						exit;
					}
				}
				if($styles['scss'] != ''){
					try {
						$scss = new Compiler;

						$compiled_css = $scss->compile($styles['scss']);

						if ($compiled_css) {
							$styles['css'] .= $compiled_css;
						}
					} catch (exception $e) {
						echo "SCSS COMPILATION ERROR in => " . $e->getMessage();
						exit;
					}
				}
				// Remove comments
				$styles['css'] = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $styles['css']);
				// Remove space after colons
				$styles['css'] = str_replace(': ', ':', $styles['css']);
				// Remove whitespace
				$styles['css'] = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $styles['css']);
				// Enable GZip encoding.
				ob_start("ob_gzhandler");
				$myfile = fopen(dirname(dirname($dir)).'/css/style.min.css', "w") or die("Unable to open file!");
				fwrite($myfile, $styles['css']);
				fclose($myfile);


				// require_once(dirname(dirname($dir)).'/minified.css.php');
			}



			//ADMIN WEBSITE
			
			if(
				$compile_css &&
				(
					(file_exists(dirname(dirname($dir)).'/administration/css/admin.less') && filemtime(dirname(dirname($dir)).'/administration/css/admin.less') > filemtime(dirname(dirname($dir)).'/administration/css/style.min.css')) ||
					(file_exists(dirname(dirname($dir)).'/administration/css/style.css') && filemtime(dirname(dirname($dir)).'/administration/css/style.css') > filemtime(dirname(dirname($dir)).'/administration/css/style.min.css'))
				)
			){
				//CSS/LESS
				$styles = array();
				$styles['less']="";
				$styles['css']="";

				$style_files = array( 
					dirname($dir).'/fancybox/dist/jquery.fancybox.min.css',
					dirname(dirname($dir)).'/administration/css/animate.less', 
					dirname(dirname($dir)).'/administration/css/normalizer.less', 
					dirname(dirname($dir)).'/administration/css/admin.less'
				);


				foreach($style_files as $style_file){
					if(preg_match('/.less$/', $style_file)){
						$styles['less'] .= file_get_contents($style_file);
					}
					else if(preg_match('/.css$/', $style_file)){
						$styles['css'] .= file_get_contents($style_file);
					}
				}

				if($styles['less'] != ''){
					try {
						$less = new lessc;

						$compiled_css = $less->compile($styles['less']);

						if ($compiled_css) {
							$styles['css'] .= $compiled_css;
						}
					} catch (exception $e) {
						echo "LESS COMPILATION ERROR => " . $e->getMessage();
						exit;
					}
				}

				$myfile = fopen(dirname(dirname($dir)).'/administration/css/style.min.css', "w") or die("Unable to open file!");
				fwrite($myfile, $styles['css']);
				fclose($myfile);

				require_once(dirname(dirname($dir)).'/administration/minified.css.php');
			}
			$compile_js = true;

  
			//CLIENT JS
			if($compile_js && filemtime(	dirname(dirname($dir)).'/js/scripts.js') > filemtime(dirname(dirname($dir)).'/js/scripts.min.js')){
				//JS
				$js_minifier = new Minify\JS();
				$styles['js'] = "";
				$scripts_files = array( 
					dirname(dirname($dir)).'/js/scripts.js'
				);

				foreach($scripts_files as $scripts_file){
					$js_minifier->add($scripts_file);

				}

				$res = $js_minifier->minify(dirname(dirname($dir)).'/js/scripts.min.js');
				$styles['js'] .= $res;
				// Remove comments
				$styles['js'] = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $styles['js']);
				// Remove space after colons
				$styles['js'] = str_replace(': ', ':', $styles['js']);
				// Remove whitespace
				$styles['js'] = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $styles['js']);
				// Enable GZip encoding.
				ob_start("ob_gzhandler");
				$myfile = fopen(dirname(dirname($dir)).'/js/scripts.min.js', "w") or die("Unable to open file!");
				fwrite($myfile, $styles['js']);
				fclose($myfile);


			}
			
			//ADMIN
			if($compile_js && filemtime(	dirname(dirname($dir)).'/administration/js/scripts.js') > filemtime(dirname(dirname($dir)).'/administration/js/scripts.min.js')){
				//JS
				$js_minifier = new Minify\JS();

				$scripts_files = array( 
					dirname(dirname($dir)).'/administration/js/normalizer.js',
					dirname(dirname($dir)).'/administration/js/scripts.js',
					dirname($dir).'/fancybox/dist/jquery.fancybox.min.js', 
					dirname(dirname($dir)).'/administration/js/app.js',
					dirname(dirname($dir)).'/administration/js/material-dashboard.js', 
					dirname(dirname($dir)).'/administration/js/wow.min.js'
				);

				foreach($scripts_files as $scripts_file){
					$js_minifier->add($scripts_file);
				}

				$js_minifier->minify(dirname(dirname($dir)).'/administration/js/scripts.min.js');
			
			}
		 
	}
?>
