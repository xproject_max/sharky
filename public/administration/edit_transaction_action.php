<?php
    require_once "../../app/administration/initializing.php";
    require_once  "../../app/administration/config/lang.php";
	
 	$Tess_db = new Database("tess_db");
	$i=0;
	$total = 0;	$unique=rand(0,1000).time();
	if(isset($_POST['data_confirmed'])){ 
		$i=0;
		$orders_array = " { ";
		foreach($_POST['data_confirmed'] as $key){

			$order_id = $key['order_id'];
			if($order_id ==  "NULL"){
				$order_id = null;
			}
			$values = str_replace(',','.',$key['payment_due']);
			$values = number_format($values, 2, '.', '');
			$balance_left = $key['balance_left'];
			$payment_method = $key['payment_method'];
			$date_payment = $key['date_payment'];
			$user_id = $key['user_id'];
			$notes = $key['notes'];  
			$total += $values;
			$tr_id = $key['transactions_id'];
			$update_tr = $Tess_db->query('
				UPDATE transactions_detail SET notes = :notes,
				payment_date = :date_payment,
				payment_method = :payment_method
				WHERE transactions_id = :id 
			');
			$Tess_db->bind($update_tr, ":notes", $key['notes']);
			$Tess_db->bind($update_tr, ":date_payment", strtotime($key['date_payment']." ".date('H:i:s')));
			$Tess_db->bind($update_tr, ":payment_method", $key['payment_method']);
			$Tess_db->bind($update_tr, ":id", $key['id']);
			$Tess_db->execute($update_tr);
			
			global $user_id, $tr_id,$notes, $date_payment,$payment_method;
			
			$i++;
		}	 	
		$update_t = $Tess_db->query('
			UPDATE transactions SET notes = :notes,
			payment_date = :date_payment,
			payment_method = :payment_method
			WHERE id = :id 
		');
		$Tess_db->bind($update_t, ":notes", $notes);
		$Tess_db->bind($update_t, ":date_payment", strtotime($date_payment." ".date('H:i:s')));
		$Tess_db->bind($update_t, ":payment_method", $key['payment_method']);
		$Tess_db->bind($update_t, ":id", $key['id']);
		$Tess_db->execute($update_t);
	}
	if(isset($_POST['data'])){
		echo "
	";
		echo '
	<div class="content_top_payment">
		<h1>Confirmation de la modification</h1>
	</div>
	<div class="content_mid_payment">
		<table>
		';
		echo "
			<tr>
				<th>No. commande</th>
				<th>Montant de transaction</th> 
				<th>Moyen de paiement utilisé</th>
				<th>Date transaction</th>
				<th>Note de transaction</th>
			</tr>
		";
		foreach($_POST['data'] as $key => $value){
			if(trim($value['payment_due']) != ""){
				$payment_method = $value['payment_method'];
				$date_payment = $value['date_payment'];
				$values = str_replace(',','.',$value['payment_due']);
				$values = number_format($values, 2, '.', '');
				 
				$total += $values;
				echo '
			<tr class="order can_delete_'.$i.'">

				<td>
				';
				if($value['order_id'] == "" || $value['order_id'] == null ||  $value['order_id'] == "NULL"){
					echo '<input type="hidden" disabled="disabled" class="popup_order_id is_order_'.$i.'"  value="NULL" />
					<input type="text" disabled="disabled" value="Solde au compte" />';
				}else{
					echo'<input disabled="disabled" type="text" class="popup_order_id is_order_'.$i.'" value="'.$value['order_id'].'" />';
				}
				echo'
				</td> 
				<td hidden="hidden">
					<input type="text" disabled="disabled" class="popup_user_id is_user_'.$i.'" value="'.$value['user_id'].'" />
					<input type="text" disabled="disabled" class="popup_id is_user_'.$i.'" value="'.$value['id'].'" />
					<input type="text" disabled="disabled" class="popup_transactions_id is_user_'.$i.'" value="'.$value['transactions_id'].'" />
				</td>
				<td>
					<input type="text" disabled="disabled" class="rows_counted popup_payment_due can_delete_val_'.$i.'" value="'.number_format($values, 2, '.', '').'" />
				</td>
				 
				<td><input type="text" disabled="disabled" class="payment_method payment_method'.$i.'" value="'.$payment_method.'" /></td>
				<td><input type="text" disabled="disabled" class="date_payment date_payment'.$i.'" value="'.$date_payment.'" /></td>
				<td>
					<textarea maxlength="32" disabled="disabled" class="popup_user_notes" value="'.$value['notes'].'">'.$value['notes'].'</textarea>
				</td>
			</tr>
				';
				$i++;
			}
		}
		echo '
			<tr> ';
			
		echo '
			</tr>
		</table>
	</div>
	<div class="content_bottom_payment">
		<button class="btn_cancel_payments">Retour</button>&nbsp;<button class="btn_edit_payment_finals">Confirmer</button>
	</div> 	
		';
		unset($_POST);
	}
?>