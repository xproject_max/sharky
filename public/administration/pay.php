<?php
    require_once "../../app/administration/initializing.php";
    require_once  "../../app/administration/config/lang.php";
	
 	$Tess_db = new Database();
	$i=0;
	$total = 0;	$unique=rand(0,1000).time();
	if(isset($_POST['data_confirmed'])){ 
		if(isset($_POST['data_confirmed'][0]['user_id'])){
			/* ---------------------- USERS --------------------------*/
			$i=0;
			$orders_array = " { ";
			foreach($_POST['data_confirmed'] as $key){

				$order_id = $key['order_id']; 
				$values = str_replace(',','.',$key['payment_due']);
				$values = number_format($values, 2, '.', '');
				$balance_left = $key['balance_left'];
				$payment_method = $key['payment_method'];
				$date_payment = $key['date_payment'];
				$date_payment = strtotime($date_payment);
				$user_id = $key['user_id'];
				global $user_id, $date_payment, $order_id;
				$notes = $key['notes'];

				$total += $values;
				
				/* ON REGARDE D'ABORD SI LA TRANSACTION EST EXISTANTE */
				
				$query_exists = $Tess_db->query('
					SELECT *
					FROM transactions
					WHERE unique_verif
					= :verif 
				');
				$Tess_db->bind($query_exists, ":verif", $unique);
				if($Tess_db->rowCount($query_exists) > 0){ 
					while($already_exist = $Tess_db->fetch($query_exists, "array")){
						$id_before = $already_exist['id'];
						$user_id_before = $already_exist['user_id'];
						$order_id_before = (isset($already_exist['order_id']) ? $already_exist['order_id'] : "NULL");
						$transaction_type_before = $already_exist['transaction_type'];
						$total_price_before = $already_exist['total_price'];
						$payment_method_before = $already_exist['payment_method'];
						$payment_date_before = $already_exist['payment_date'];
						$notes_before = $already_exist['notes'];

						/* SI LA TRANSACTION EXISTE ON ASSOCIE LES DÉTAILS TRANSACTIONNELS À CELLE-CI */
					 
						$order_id = (($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : $order_id);
						$notes = (($notes ==  "NULL" || $notes ==  "") ? 'NULL' : $notes);
						
						$insert_multi_transactions = $Tess_db->query('INSERT INTO `transactions_detail` (
							`transactions_id`,
							`user_id`,
							`order_id`,
							`transaction_type`,
							`total_price`,
							`payment_method`,
							`payment_date`,
							`notes`,
							`credit_used`
							) 
							values (
							:id_before,
							:user_id,
							:order_id,
							"paiement_admin",
							:values,
							:payment_method,
							:date_payment,
							:notes,
							"1"
							);
						');
						$Tess_db->bind($insert_multi_transactions, ":id_before", $id_before);
						$Tess_db->bind($insert_multi_transactions, ":user_id", $user_id_before);
						$Tess_db->bind($insert_multi_transactions, ":order_id", $order_id);
						$Tess_db->bind($insert_multi_transactions, ":values", $values);
						$Tess_db->bind($insert_multi_transactions, ":payment_method", $payment_method);
						$Tess_db->bind($insert_multi_transactions, ":date_payment", $date_payment);
						$Tess_db->bind($insert_multi_transactions, ":notes",$notes);
						
						if($Tess_db->execute($insert_multi_transactions)){
							 
							/* ON AJUSTE LE MONTANT RESTANT À PAYER PAR TRANSACTION */
							
							$update_current_orders = $Tess_db->query('
								UPDATE tbl_orders
								SET amount_to_pay = round(amount_to_pay - :values ,2)
								WHERE id = :order_id 
							');
							$Tess_db->bind($update_current_orders, ":values", $values);
							$Tess_db->bind($update_current_orders, ":order_id", $order_id);
							if($Tess_db->execute($update_current_orders)){
								$update_current_transactions = $Tess_db->query('
								UPDATE transactions
								SET total_price = total_price + :values 
								WHERE order_id = :order_id 
								');
								$Tess_db->bind($update_current_transactions, ":values", $values);
								$Tess_db->bind($update_current_transactions, ":order_id", (($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : $order_id));
								if($Tess_db->execute($update_current_transactions)){
									$query_order = $Tess_db->query('
										SELECT *
										FROM tbl_orders
										WHERE id
										= :order_id 
										LIMIT 1;
									');
									$Tess_db->bind($query_order, ":order_id", (($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : $order_id));
									$Tess_db->execute($query_order);
									while($order = $Tess_db->fetch($query_order, "array")){
										$balance_left = $order['amount_to_pay'];
										$paid = $order['status'];
										if($balance_left == 0 && $paid == "unpaid"){
											$update_paid = $Tess_db->query('
												UPDATE tbl_orders
												SET amount_to_pay =0,status = "paid" WHERE
												id = :order_id 
											'); 
											$Tess_db->bind($update_paid, ":order_id", $order_id);
											$Tess_db->execute($update_paid);
										}
									}
								}
							}
						} else{
							echo $insert_multi_transactions->errorCode();	
							echo  print_r($insert_multi_transactions->errorInfo()); 
						}
					
					}
				}else{
				
					/* SI LA TRANSACTION N'EXISTE PAS, ON LA CRÉÉ */

					$query_insert_transactions = $Tess_db->query('
						INSERT INTO `transactions`
						(`user_id`,
						`transaction_type`, `total_price`,
						`payment_method`, `payment_date`, `notes`, `unique_verif`)
						VALUES (
						:user_id,
						"paiement_admin",
						:values,
						:payment_method,
						:date_payment,
						:notes,
						:unique
						);
					'); 
					$Tess_db->bind($query_insert_transactions, ":user_id", $user_id); 
					$Tess_db->bind($query_insert_transactions, ":values", $values);
					$Tess_db->bind($query_insert_transactions, ":payment_method",$payment_method);
					$Tess_db->bind($query_insert_transactions, ":date_payment",$date_payment);
					$Tess_db->bind($query_insert_transactions, ":notes",$notes);
					$Tess_db->bind($query_insert_transactions, ":unique",$unique);
					if($Tess_db->execute($query_insert_transactions)){ 
						
						$trid = $Tess_db->query("SELECT LAST_INSERT_ID() as `fktr` FROM `transactions`");
						$trid = $Tess_db->single($trid)->fktr;

					/* ON AJOUTE LES DETAILS TRANSACTIONNELS À CELLE-CI */
				
						$query_insert_multi = $Tess_db->query('
							INSERT INTO transactions_detail (
							`transactions_id`,
							`user_id`,
							`order_id`,
							`transaction_type`,
							`total_price`,
							`payment_method`,
							`payment_date`,
							`notes`,
							`credit_used`
							)
							values(
								:fktr,
								:user_id,
								:order_id,
								"paiement_admin",
								:values,
								:payment_method,
								:date_payment,
								:notes,
								1
								);
						');					
						
						$Tess_db->bind($query_insert_multi, ":fktr", $trid); 
						$Tess_db->bind($query_insert_multi, ":user_id", $user_id); 
						$Tess_db->bind($query_insert_multi, ":order_id", (($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : $order_id));
						$Tess_db->bind($query_insert_multi, ":values",number_format($values,2));
						$Tess_db->bind($query_insert_multi, ":payment_method",$payment_method);
						$Tess_db->bind($query_insert_multi, ":date_payment",$date_payment);
						$Tess_db->bind($query_insert_multi, ":notes",(isset($notes) ? $notes : "" ));
						if($Tess_db->execute($query_insert_multi)){
							$update_current_transactions = $Tess_db->query('
								UPDATE tbl_orders
								SET amount_to_pay = round(amount_to_pay - :values ,2)
								WHERE id = :order_id
							');
							$Tess_db->bind($update_current_transactions, ":values",$values); 
							$Tess_db->bind($update_current_transactions, ":order_id",(($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : $order_id)); 
							if($Tess_db->execute($update_current_transactions)){
								$query_order = $Tess_db->query('
									SELECT *
									FROM tbl_orders
									WHERE id
									= :order_id
									LIMIT 1;
								');
								$Tess_db->bind($query_order, ":order_id",(($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : $order_id)); 
								$Tess_db->execute($query_order);
								while($order = $Tess_db->fetch($query_order, "array")){
									$balance_left = $order['amount_to_pay'];
									$paid = $order['status'];
									if($balance_left == 0 && $paid == "unpaid"){
										$update_paid = $Tess_db->query('
											UPDATE tbl_orders
											SET amount_to_pay =0,status = "paid" WHERE
											id = :order_id
										');
										$Tess_db->bind($update_paid, ":order_id",(($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : $order_id)); 
										if($Tess_db->execute($update_paid)){

										}
									}
								}
							}
						}else{
							echo '
							INSERT INTO transactions_detail (
							`transactions_id`,
							`user_id`,
							`order_id`,
							`transaction_type`,
							`total_price`,
							`payment_method`,
							`payment_date`,
							`notes`,
							`credit_used`
							)
							values(
								'.$trid.',
								'.$user_id.',
								'.(($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : '"'.$order_id.'"').',
								"paiement_admin",
								'.number_format($values,2).',
								"'.$payment_method.'",
								'.$date_payment.',
								"'.(isset($notes) ? $notes : "" ).'",
								1
								);</br>
								
							Tess_db->bind(query_insert_multi, ":fktr", '.$trid.'); 
							Tess_db->bind(query_insert_multi, ":user_id", '.$user_id.'); 
							Tess_db->bind(query_insert_multi, ":order_id", '.(($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : $order_id).');
							Tess_db->bind(query_insert_multi, ":values",'.number_format($values,2).');
							Tess_db->bind(query_insert_multi, ":payment_method",'.$payment_method.');
							Tess_db->bind(query_insert_multi, ":date_payment",'.$date_payment.');
							Tess_db->bind(query_insert_multi, ":notes",'.(isset($notes) ? $notes : "" ).');
							';
						}
					} else{
						// error_log(mysql_error() . "\n", 3, "/var/tmp/my-errors.log");
					}
					if($order_id ==  "NULL" || $order_id ==  0){
						$query_account_balance = $Tess_db->query('
						SELECT total_price FROM transactions_detail WHERE (order_id is NULL
						AND credit_used is null) AND :user_id
						');
						$Tess_db->bind($query_account_balance, ":user_id", $user_id);
						if($Tess_db->rowCount($query_account_balance) > 0){
							$update_td = $Tess_db->query('
								UPDATE transactions_detail
								SET credit_used="1"
								WHERE credit_used is null AND order_id is null
								AND user_id = :user_id
							');
							$Tess_db->bind($update_td, ":user_id", $user_id);
							$Tess_db->execute($update_td);
							while($account_credit = $Tess_db->fetch($query_account_balance, "array")){

								$credit_value = $account_credit['total_price']; 
								if($credit_value != 0){
					 
								}
							}
						}					
					}
				}
				$orders_array .= "Numéro commande: #".$order_id." => Montant payé:".$values."$ || ";
				$i++;
			}
			$orders_array .= ' } ';
			// $addcredit = mysql_query('
				// INSERT INTO `credit_log`(`id_user`, `type`, `value`, `description`, `date`, `id`)
				// VALUES ('.$user_id.', "manual_input", '.$total.', "'.$orders_array.'", "'.((($date_payment) ? strtotime($date_payment." ".date('H:i:s')) : time()) + 10).'", "")

			// ');
			// if(@$addcredit){
				// ok
			// }
			$update_current_transactions = $Tess_db->query('
				UPDATE tbl_users
				SET solde = round(solde + :total ,2)
				WHERE id = :user_id
			');
			$Tess_db->bind($update_current_transactions, ":total", $total);
			$Tess_db->bind($update_current_transactions, ":user_id", $user_id);
			$Tess_db->execute($update_current_transactions);
		
		}else{
			/* ---------------------- company --------------------------*/
						$i=0;
			$orders_array = " { ";
			foreach($_POST['data_confirmed'] as $key){

				$order_id = $key['order_id']; 
				$values = str_replace(',','.',$key['payment_due']);
				$values = number_format($values, 2, '.', '');
				$balance_left = $key['balance_left'];
				$payment_method = $key['payment_method'];
				$date_payment = $key['date_payment'];
				$date_payment = strtotime($date_payment);
				$company_id = $key['company_id'];
				global $company_id, $date_payment, $order_id;
				$notes = $key['notes'];

				$total += $values;
				
				/* ON REGARDE D'ABORD SI LA TRANSACTION EST EXISTANTE */
				
				$query_exists = $Tess_db->query('
					SELECT *
					FROM transactions
					WHERE unique_verif
					= :verif 
				');
				$Tess_db->bind($query_exists, ":verif", $unique);
				if($Tess_db->rowCount($query_exists) > 0){ 
					while($already_exist = $Tess_db->fetch($query_exists, "array")){
						$id_before = $already_exist['id'];
						$company_id_before = $already_exist['company_id'];
						$order_id_before = (isset($already_exist['order_id']) ? $already_exist['order_id'] : "NULL");
						$transaction_type_before = $already_exist['transaction_type'];
						$total_price_before = $already_exist['total_price'];
						$payment_method_before = $already_exist['payment_method'];
						$payment_date_before = $already_exist['payment_date'];
						$notes_before = $already_exist['notes'];

						/* SI LA TRANSACTION EXISTE ON ASSOCIE LES DÉTAILS TRANSACTIONNELS À CELLE-CI */
					 
						$order_id = (($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : $order_id);
						$notes = (($notes ==  "NULL" || $notes ==  "") ? 'NULL' : $notes);
						
						$insert_multi_transactions = $Tess_db->query('INSERT INTO `transactions_detail` (
							`transactions_id`,
							`company_id`,
							`order_id`,
							`transaction_type`,
							`total_price`,
							`payment_method`,
							`payment_date`,
							`notes`,
							`credit_used`
							) 
							values (
							:id_before,
							:company_id,
							:order_id,
							"paiement_admin",
							:values,
							:payment_method,
							:date_payment,
							:notes,
							"1"
							);
						');
						$Tess_db->bind($insert_multi_transactions, ":id_before", $id_before);
						$Tess_db->bind($insert_multi_transactions, ":company_id", $company_id_before);
						$Tess_db->bind($insert_multi_transactions, ":order_id", $order_id);
						$Tess_db->bind($insert_multi_transactions, ":values", $values);
						$Tess_db->bind($insert_multi_transactions, ":payment_method", $payment_method);
						$Tess_db->bind($insert_multi_transactions, ":date_payment", $date_payment);
						$Tess_db->bind($insert_multi_transactions, ":notes",$notes);
						
						if($Tess_db->execute($insert_multi_transactions)){
							 
							/* ON AJUSTE LE MONTANT RESTANT À PAYER PAR TRANSACTION */
							
							$update_current_orders = $Tess_db->query('
								UPDATE tbl_orders
								SET amount_to_pay = round(amount_to_pay - :values ,2)
								WHERE id = :order_id 
							');
							$Tess_db->bind($update_current_orders, ":values", $values);
							$Tess_db->bind($update_current_orders, ":order_id", $order_id);
							if($Tess_db->execute($update_current_orders)){
								$update_current_transactions = $Tess_db->query('
								UPDATE transactions
								SET total_price = total_price + :values 
								WHERE order_id = :order_id 
								');
								$Tess_db->bind($update_current_transactions, ":values", $values);
								$Tess_db->bind($update_current_transactions, ":order_id", (($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : $order_id));
								if($Tess_db->execute($update_current_transactions)){
									$query_order = $Tess_db->query('
										SELECT *
										FROM tbl_orders
										WHERE id
										= :order_id 
										LIMIT 1;
									');
									$Tess_db->bind($query_order, ":order_id", (($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : $order_id));
									while($order = $Tess_db->fetch($query_order, "array")){
										$balance_left = $order['amount_to_pay'];
										$paid = $order['status'];
										if($balance_left == 0 && $paid == "unpaid"){
											$update_paid = $Tess_db->query('
												UPDATE tbl_orders
												SET amount_to_pay =0,status = "paid" WHERE
												id = :order_id 
											'); 
											$Tess_db->bind($update_paid, ":order_id", $order_id);
											if($update_paid){

											}
										}
									}
								}
							}
						} else{
							echo $insert_multi_transactions->errorCode();	
							echo  print_r($insert_multi_transactions->errorInfo()); 
						}
					
					}
				}else{
				
					/* SI LA TRANSACTION N'EXISTE PAS, ON LA CRÉÉ */

					$query_insert_transactions = $Tess_db->query('
						INSERT INTO `transactions`
						(`company_id`,
						`transaction_type`, `total_price`,
						`payment_method`, `payment_date`, `notes`, `unique_verif`)
						VALUES (
						:company_id,
						"paiement_admin",
						:values,
						:payment_method,
						:date_payment,
						:notes,
						:unique
						);
					'); 
					$Tess_db->bind($query_insert_transactions, ":company_id", $company_id); 
					$Tess_db->bind($query_insert_transactions, ":values", $values);
					$Tess_db->bind($query_insert_transactions, ":payment_method",$payment_method);
					$Tess_db->bind($query_insert_transactions, ":date_payment",$date_payment);
					$Tess_db->bind($query_insert_transactions, ":notes",$notes);
					$Tess_db->bind($query_insert_transactions, ":unique",$unique);
					if($Tess_db->execute($query_insert_transactions)){ 
						
						$trid = $Tess_db->query("SELECT LAST_INSERT_ID() as `fktr` FROM `transactions`");
						$trid = $Tess_db->single($trid)->fktr;

					/* ON AJOUTE LES DETAILS TRANSACTIONNELS À CELLE-CI */
				
						$query_insert_multi = $Tess_db->query('
							INSERT INTO transactions_detail (
							`transactions_id`,
							`company_id`,
							`order_id`,
							`transaction_type`,
							`total_price`,
							`payment_method`,
							`payment_date`,
							`notes`,
							`credit_used`
							)
							values(
								:fktr,
								:company_id,
								:order_id,
								"paiement_admin",
								:values,
								:payment_method,
								:date_payment,
								:notes,
								1
								);
						');					
						
						$Tess_db->bind($query_insert_multi, ":fktr", $trid); 
						$Tess_db->bind($query_insert_multi, ":company_id", $company_id); 
						$Tess_db->bind($query_insert_multi, ":order_id", (($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : $order_id));
						$Tess_db->bind($query_insert_multi, ":values",number_format($values,2));
						$Tess_db->bind($query_insert_multi, ":payment_method",$payment_method);
						$Tess_db->bind($query_insert_multi, ":date_payment",$date_payment);
						$Tess_db->bind($query_insert_multi, ":notes",(isset($notes) ? $notes : "" ));
						if($Tess_db->execute($query_insert_multi)){
							$update_current_transactions = $Tess_db->query('
								UPDATE tbl_orders
								SET amount_to_pay = round(amount_to_pay - :values ,2)
								WHERE id = :order_id
							');
							$Tess_db->bind($update_current_transactions, ":values",$values); 
							$Tess_db->bind($update_current_transactions, ":order_id",(($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : $order_id)); 
							if($Tess_db->execute($update_current_transactions)){
								$query_order = $Tess_db->query('
									SELECT *
									FROM tbl_orders
									WHERE id
									= :order_id
									LIMIT 1;
								');
								$Tess_db->bind($query_order, ":order_id",(($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : $order_id)); 
								while($order = $Tess_db->fetch($query_order, "array")){
									$balance_left = $order['amount_to_pay'];
									$paid = $order['status'];
									if($balance_left == 0 && $paid == "unpaid"){
										$update_paid = $Tess_db->query('
											UPDATE tbl_orders
											SET amount_to_pay =0,status = "paid" WHERE
											id = :order_id
										');
										$Tess_db->bind($update_paid, ":order_id",(($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : $order_id)); 
										if($Tess_db->execute($update_paid)){

										}
									}
								}
							}
						}else{
							echo '
							INSERT INTO transactions_detail (
							`transactions_id`,
							`company_id`,
							`order_id`,
							`transaction_type`,
							`total_price`,
							`payment_method`,
							`payment_date`,
							`notes`,
							`credit_used`
							)
							values(
								'.$trid.',
								'.$company_id.',
								'.(($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : '"'.$order_id.'"').',
								"paiement_admin",
								'.number_format($values,2).',
								"'.$payment_method.'",
								'.$date_payment.',
								"'.(isset($notes) ? $notes : "" ).'",
								1
								);</br>
								
							Tess_db->bind(query_insert_multi, ":fktr", '.$trid.'); 
							Tess_db->bind(query_insert_multi, ":company_id", '.$company_id.'); 
							Tess_db->bind(query_insert_multi, ":order_id", '.(($order_id ==  "NULL" || $order_id ==  0) ? 'NULL' : $order_id).');
							Tess_db->bind(query_insert_multi, ":values",'.number_format($values,2).');
							Tess_db->bind(query_insert_multi, ":payment_method",'.$payment_method.');
							Tess_db->bind(query_insert_multi, ":date_payment",'.$date_payment.');
							Tess_db->bind(query_insert_multi, ":notes",'.(isset($notes) ? $notes : "" ).');
							';
						}
					} else{
						// error_log(mysql_error() . "\n", 3, "/var/tmp/my-errors.log");
					}
					if($order_id ==  "NULL" || $order_id ==  0){
						$query_account_balance = $Tess_db->query('
						SELECT total_price FROM transactions_detail WHERE (order_id is NULL
						AND credit_used is null) AND :company_id
						');
						$Tess_db->bind($query_account_balance, ":company_id", $company_id);
						if($Tess_db->rowCount($query_account_balance) > 0){
							$update_td = $Tess_db->query('
								UPDATE transactions_detail
								SET credit_used="1"
								WHERE credit_used is null AND order_id is null
								AND company_id = :company_id
							');
							$Tess_db->bind($update_td, ":company_id", $company_id);
							$Tess_db->execute($update_td);
							while($account_credit = $Tess_db->fetch($query_account_balance, "array")){

								$credit_value = $account_credit['total_price']; 
								if($credit_value != 0){
					 
								}
							}
						}					
					}
				}
				$orders_array .= "Numéro commande: #".$order_id." => Montant payé:".$values."$ || ";
				$i++;
			}
			$orders_array .= ' } ';
			// $addcredit = mysql_query('
				// INSERT INTO `credit_log`(`id_company`, `type`, `value`, `description`, `date`, `id`)
				// VALUES ('.$company_id.', "manual_input", '.$total.', "'.$orders_array.'", "'.((($date_payment) ? strtotime($date_payment." ".date('H:i:s')) : time()) + 10).'", "")

			// ');
			// if(@$addcredit){
				// ok
			// }
			$update_current_transactions = $Tess_db->query('
				UPDATE tbl_companies
				SET solde = round(solde + :total ,2)
				WHERE id = :company_id
			');
			$Tess_db->bind($update_current_transactions, ":total", $total);
			$Tess_db->bind($update_current_transactions, ":company_id", $company_id);
			$Tess_db->execute($update_current_transactions);
		
		}
	}
	if(isset($_POST['data'])){
		echo "
	";
		echo '
	<div class="content_top_payment">
		<h1>Confirmation de paiement</h1>
	</div>
	<div class="content_mid_payment">
		<table '.(isset($value['user_id']) ? "" : 'class="companies_pay_table"').'>
		';
		echo "
			<tr>
				<th>No. commande</th>
				<th>MONTANT DUE</th>
				<th>MONTANT à payer</th>
				<th>Balance</th>
				<th>Moyen de paiement utilisé</th>
				<th>Date transaction</th>
				<th>Note de transaction</th>
			</tr>
		";
		foreach($_POST['data'] as $key => $value){
			if(trim($value['payment_due']) != ""){
				$payment_method = $value['payment_method'];
				$date_payment = $value['date_payment'];
				$values = str_replace(',','.',$value['payment_due']);
				$values = number_format($values, 2, '.', '');
				if($value['payment_due'] > ($value['balance_left'])){
					$values = number_format($value['balance_left'], 2, '.', '');
				}
				$total += $values;
				echo '
			<tr class="order can_delete_'.$i.'">

				<td>
				';
				if($value['order_id'] == "" || $value['order_id'] == null ||  $value['order_id'] == "NULL"){
					echo '<input type="hidden" disabled="disabled" class="popup_order_id is_order_'.$i.'"  value="NULL" />
					<input type="text" disabled="disabled" value="Solde au compte" />';
				}else{
					echo'<input disabled="disabled" type="text" class="popup_order_id is_order_'.$i.'" value="'.$value['order_id'].'" />';
				}
				echo'
				</td>
				<td>
					<input type="text" disabled="disabled" class="popup_balance_left is_left_'.$i.'" value="'.$value['balance_left'].'" />
				</td>
				<td hidden="hidden">
					<input type="text" disabled="disabled" class="'.(isset($value['user_id']) ? "popup_user_id" : "popup_company_id").' is_user_'.$i.'" value="'.(isset($value['user_id']) ? $value['user_id'] : $value['company_id']).'" />
				</td>
				<td>
					<input type="text" disabled="disabled" class="rows_counted popup_payment_due can_delete_val_'.$i.'" value="'.number_format($values, 2, '.', '').'" />
				</td>
				<td>'.number_format((($value['balance_left']) - $values), 2, '.', '').'</td>
				<td><input type="text" disabled="disabled" class="payment_method payment_method'.$i.'" value="'.$payment_method.'" /></td>
				<td><input type="text" disabled="disabled" class="date_payment date_payment'.$i.'" value="'.$date_payment.'" /></td>
				<td>
					<textarea maxlength="32" disabled="disabled" class="'.(isset($value['user_id']) ? "popup_user_notes" : "popup_company_notes").'" value="'.$value['notes'].'">'.$value['notes'].'</textarea>
				</td>
			</tr>
				';
				$i++;
			}
		}
		echo '
			<tr>
				<td colspan="7"><input type="text" disabled="disabled" class="total_payments" value="'.number_format($total, 2, '.', '').'" />';
			
		echo '
			</tr>
		</table>
	</div>
	<div class="content_bottom_payment">
		<button class="btn_cancel_payments">Retour</button>&nbsp;<button class="btn_pay_payment_finals">Confirmer</button>
	</div> 	
		';
		unset($_POST);
	}
?>