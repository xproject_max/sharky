<?php
    require_once "../../app/administration/initializing.php";
    require_once  "../../app/administration/config/lang.php";
	
 	$Tess_db = new Database();
	// var_dump($_POST);
	echo '
<div class="centent_payment_history">
	 
	<div class="content_mid_payment">
	';
	$i=0;
	$total=0;
	$balance =0;
 	if(isset($_POST['dataedit'])){
		
		if(isset($_POST['dataedit'][0]['user_id'])){
			$result = array();
			$getMethodPayment = getMethodPayments();
			$listPayments = "";
			foreach($getMethodPayment as $key => $value){
				$listPayments .= '	<option value="'.$key.'">'.$value[$l].'</option>';
			}
			foreach($_POST['dataedit'] as $key => $value){
				$date_exist = $value['date_payment'];
			}
			echo '
			<table id="table_p_h">
				<tr>
					<td colspan="5">
					<label for="payment_method">Choix du moyen de paiement</label>
					<select class="payment_method" name="payment_method">
						<option value="credit">Crédit</option>
						'.$listPayments.'
					</select>
					</td>
					<td colspan="2">
						 <input type="text" id="datepicker_drop_point" class="date_payment" name="date_payment"  value="'.$date_exist.'" />
						 
					</td>
				</tr>
				<tr>
					<th>No. commande</th>
					<th>MONTANT DUE</th>
					<th>MONTANT à payer</th>
					<th>Balance</th>
					<th>Note de transaction</th>
					<th> </th>
					<th>-Supprimer-</th>
				</tr>
			';
			foreach($_POST['dataedit'] as $key => $value){
				if(trim($value['payment_due']) != ""){
					$values = str_replace(',','.',$value['payment_due']);
					$values = number_format($values, 2, '.', '');
					if($value['payment_due'] > ($value['balance_left'])){
						$values = ($value['balance_left']);
					}
					if($value['payment_due'] < 0){
						$values = 0;
					}
					$total += $values;
					echo '
				<tr class="order can_delete_'.$i.'">
					<td>';
					if($value['order_id'] == "" || $value['order_id'] == null ||  $value['order_id'] == "NULL"){
						echo '<input type="hidden" disabled="disabled" class="popup_order_id is_order_'.$i.'"  value="NULL" />
						<input type="text" disabled="disabled" value="Solde au compte" />';
					}else{
						echo'<input disabled="disabled" type="text" class="popup_order_id is_order_'.$i.'" value="'.$value['order_id'].'" />';
					}
					echo'
					</td>
					<td hidden="hidden">
						<input disabled="disabled" type="text" class="popup_user_id is_user_'.$i.'" value="'.$value['user_id'].'" />
					</td>
					<td>
						<input type="text" disabled="disabled" class="popup_balance_left is_left_'.$i.'" value="'.number_format($value['balance_left'], 2, '.', '').'" />
					</td>
					<td>
						<input type="text" class="rows_counted popup_payment_due can_delete_val_'.$i.'" value="'.number_format($values, 2, '.', '').'" />
					</td>
					<td>'.number_format((($value['balance_left']) - $values), 2, '.', '').'</td>
					<td>
						<textarea maxlength="32" class="popup_user_notes" value="'.(isset($value['notes']) ? $value['notes'] : "").'">'.(isset($value['notes']) ? $value['notes'] : "").'</textarea>
					</td>
					<td> 
					</td>
					<td onclick="delete_case('.$i.');"><a href="#">Supprimer</a></td>
				</tr>
					';
					$i++;
				}		 
			}		
			echo '
				<tr>
					<td colspan="7">
						<input type="text" disabled="disabled" class="total_payments" value="'.$total.'" />
					</td>
				</tr>
			</table>
			';
		}else{
			$result = array();
			$getMethodPayment = getMethodPayments();
			$listPayments = "";
			foreach($getMethodPayment as $key => $value){
				$listPayments .= '	<option value="'.$key.'">'.$value[$l].'</option>';
			}
			foreach($_POST['dataedit'] as $key => $value){
				$date_exist = $value['date_payment'];
			}
			echo '
			<table id="table_p_h"  class="companies_pay_table">
				<tr>
					<td colspan="5">
					<label for="payment_method">Choix du moyen de paiement</label>
					<select class="payment_method" name="payment_method">
						<option value="credit">Crédit</option>
						'.$listPayments.'
					</select>
					</td>
					<td colspan="2">
						 <input type="text" id="datepicker_drop_point" class="date_payment" name="date_payment"  value="'.$date_exist.'" />
						 
					</td>
				</tr>
				<tr>
					<th>No. commande</th>
					<th>MONTANT DUE</th>
					<th>MONTANT à payer</th>
					<th>Balance</th>
					<th>Note de transaction</th>
					<th> </th>
					<th>-Supprimer-</th>
				</tr>
			';
			foreach($_POST['dataedit'] as $key => $value){
				if(trim($value['payment_due']) != ""){
					$values = str_replace(',','.',$value['payment_due']);
					$values = number_format($values, 2, '.', '');
					if($value['payment_due'] > ($value['balance_left'])){
						$values = ($value['balance_left']);
					}
					if($value['payment_due'] < 0){
						$values = 0;
					}
					$total += $values;
					echo '
				<tr class="order can_delete_'.$i.'">
					<td>';
					if($value['order_id'] == "" || $value['order_id'] == null ||  $value['order_id'] == "NULL"){
						echo '<input type="hidden" disabled="disabled" class="popup_order_id is_order_'.$i.'"  value="NULL" />
						<input type="text" disabled="disabled" value="Solde au compte" />';
					}else{
						echo'<input disabled="disabled" type="text" class="popup_order_id is_order_'.$i.'" value="'.$value['order_id'].'" />';
					}
					echo'
					</td>
					<td hidden="hidden">
						<input disabled="disabled" type="text" class="popup_company_id is_user_'.$i.'" value="'.(isset($value['company_id']) ? $value['company_id'] : $_POST['dataedit'][0]['company_id'] ).'" />
					</td>
					<td>
						<input type="text" disabled="disabled" class="popup_balance_left is_left_'.$i.'" value="'.number_format($value['balance_left'], 2, '.', '').'" />
					</td>
					<td>
						<input type="text" class="rows_counted popup_payment_due can_delete_val_'.$i.'" value="'.number_format($values, 2, '.', '').'" />
					</td>
					<td>'.number_format((($value['balance_left']) - $values), 2, '.', '').'</td>
					<td>
						<textarea maxlength="32" class="popup_company_notes" value="'.(isset($value['notes']) ? $value['notes'] : "").'">'.(isset($value['notes']) ? $value['notes'] : "").'</textarea>
					</td>
					<td> 
					</td>
					<td onclick="delete_case('.$i.');"><a href="#">Supprimer</a></td>
				</tr>
					';
					$i++;
				}		 
			}		
			echo '
				<tr>
					<td colspan="7">
						<input type="text" disabled="disabled" class="total_payments" value="'.$total.'" />
					</td>
				</tr>
			</table>
			';
			
		}
	}
	if(isset($_POST['data'])){
		//USER WEBSITE
		if(isset($_POST['data'][0]['user_id'])){
			$user = $Tess_db->query('
				SELECT
					tbl_users.* 
				FROM tbl_users 
				WHERE tbl_users.id = :id');
			$Tess_db->bind($user, ":id", $_POST['data'][0]['user_id']);
			$user = $Tess_db->single($user); 
			$user = object_to_array($user); 
			$result = array();
			$getMethodPayment = getMethodPayments();
			$listPayments = "";
			foreach($getMethodPayment as $key => $value){
				$listPayments .= '	<option value="'.$key.'">'.$value[$l].'</option>';
			}
			echo '
			<table id="table_p_h">
				<tr>
					<td colspan="5">
						<label for="payment_method">Choix du moyen de paiement</label>
						<select class="payment_method" name="payment_method">
							<option value="credit">Crédit</option>
							'.$listPayments.'
						</select>
					</td>
					<td colspan="2"> 
						<label for="date_payment">Date de paiement</label>
						 <input type="text" id="datepicker" class="date_payment datetimepicker" autocomplete="off" name="date_payment"  value="'.date("Y-m-d H:i:s").'" />
						 
					</td>
				</tr>
				<tr>
					<th>No. commande</th>
					<th>MONTANT DUE</th>
					<th>MONTANT à payer</th>
					<th>Balance</th>
					<th>Note de transaction</th>
					<th> </th>
					<th>-Supprimer-</th>
				</tr>
			';

			$total = 0;
			$query_orders = $Tess_db->query('
				SELECT * FROM tbl_orders WHERE amount_to_pay != 0 AND status="unpaid" AND fk_users = :id  ORDER BY date_created asc'
			);
			$Tess_db->bind($query_orders, ":id", $_POST['data'][0]['user_id']);  
			if($Tess_db->rowCount($query_orders) > 0){

				while($or = $Tess_db->fetch($query_orders, "array")){


					 
					$price = (($balance >=$or['amount_to_pay']) ? ($or['amount_to_pay']) : $balance);
					echo '
				<tr class="order can_delete_'.$i.'">
					<td>
						<input disabled="disabled" type="text" class="popup_order_id is_order_'.$i.'" value="'.$or['id'].'" />
					</td>
					<td hidden="hidden">
						<input disabled="disabled" type="text" class="popup_user_id is_user_'.$i.'" value="'.$user['id'].'" />
					</td>
					<td>
						<input type="text" disabled="disabled" class="popup_balance_left is_left_'.$i.'" value="'.number_format(($or['amount_to_pay']), 2, '.', '').'" />
					</td>
					<td>
						<input type="text" class="rows_counted popup_payment_due can_delete_val_'.$i.'" value="'.number_format($price, 2, '.', '').'" />
					</td>
					<td>'.($or['amount_to_pay']-$price).'</td>
					<td>
						<textarea maxlength="32" class="popup_user_notes" value=""></textarea>
					</td>
					
					<td></td>
					<td onclick="delete_case('.$i.');"><a href="#">Supprimer</a></td>
				</tr>
					';
					$balance -= $or['amount_to_pay'];
					$total += $or['amount_to_pay'];
					$i++;
				}
			}else{
				$balance = $_POST['data'][0]['balance_left_account'];
			}
			if($balance < 0){ 
				$balance = $_POST['data'][0]['balance_left_account'];
			}else{
				$balance = $balance;
			}
			
			echo'
				<tr class="order can_delete_'.$i.'">
					<td>
						<input type="hidden" disabled="disabled" class="popup_order_id is_order_'.$i.'"  value="NULL" />
						<input type="text" disabled="disabled" value="Solde au compte" />
					</td>
					<td hidden="hidden">
						<input disabled="disabled" type="text" class="popup_user_id is_user_'.$i.'" value="'.$user['id'].'" />
					</td>
					<td>
						<input type="text" disabled="disabled" class="popup_balance_left is_left_'.$i.'" value="'.number_format(($balance), 2, '.', '').'" />
					</td>
					<td>
						<input type="text" class="rows_counted popup_payment_due can_delete_val_'.$i.'" value="" />
					</td>
					<td></td>
					<td>
						<textarea maxlength="32" class="popup_user_notes" value=""></textarea>
					</td>
					<td></td>
					<td onclick="delete_case('.$i.');"><a href="#">Supprimer</a></td>
				</tr>
			';
			
			$total += $balance;	
			
			echo '
				<tr>
					<td colspan="7">
						<input type="text" disabled="disabled" class="total_payments" value="'.number_format($_POST['data'][0]['payment_due'], 2, '.', '').'" />
					</td>
				</tr>
			</table>
			';
		}else{
			//COMPANY WEBSITE
			$user = $Tess_db->query('
				SELECT
					tbl_companies.* 
				FROM tbl_companies 
				WHERE tbl_companies.id = :id');
			$Tess_db->bind($user, ":id", $_POST['data'][0]['company_id']);
			$user = $Tess_db->single($user); 
			$user = object_to_array($user); 
			$result = array();
			$getMethodPayment = getMethodPayments();
			$listPayments = "";
			foreach($getMethodPayment as $key => $value){
				$listPayments .= '	<option value="'.$key.'">'.$value[$l].'</option>';
			}
			echo '
			<table id="table_p_h" class="companies_pay_table">
				<tr>
					<td colspan="5">
						<label for="payment_method">Choix du moyen de paiement</label>
						<select class="payment_method" name="payment_method">
							<option value="credit">Crédit</option>
							'.$listPayments.'
						</select>
					</td>
					<td colspan="2"> 
						<label for="date_payment">Date de paiement</label>
						 <input type="text" id="datepicker" class="date_payment datetimepicker" autocomplete="off" name="date_payment"  value="'.date("Y-m-d H:i:s").'" />
						 
					</td>
				</tr>
				<tr>
					<th>No. commande</th>
					<th>MONTANT DUE</th>
					<th>MONTANT à payer</th>
					<th>Balance</th>
					<th>Note de transaction</th>
					<th> </th>
					<th>-Supprimer-</th>
				</tr>
			';

			$total = 0;
			$query_orders = $Tess_db->query('
				SELECT * FROM tbl_orders WHERE amount_to_pay != 0 AND status="unpaid" AND fk_companies = :id and (fk_users is null or fk_users = 0)  ORDER BY date_created asc'
			);
			$Tess_db->bind($query_orders, ":id", $_POST['data'][0]['company_id']);  
			if($Tess_db->rowCount($query_orders) > 0){

				while($or = $Tess_db->fetch($query_orders, "array")){


					 
					$price = (($balance >=$or['amount_to_pay']) ? ($or['amount_to_pay']) : $balance);
					echo '
				<tr class="order can_delete_'.$i.'">
					<td>
						<input disabled="disabled" type="text" class="popup_order_id is_order_'.$i.'" value="'.$or['id'].'" />
					</td>
					<td hidden="hidden">
						<input disabled="disabled" type="text" class="popup_company_id is_user_'.$i.'" value="'.$user['id'].'" />
					</td>
					<td>
						<input type="text" disabled="disabled" class="popup_balance_left is_left_'.$i.'" value="'.number_format(($or['amount_to_pay']), 2, '.', '').'" />
					</td>
					<td>
						<input type="text" class="rows_counted popup_payment_due can_delete_val_'.$i.'" value="'.number_format($price, 2, '.', '').'" />
					</td>
					<td>'.($or['amount_to_pay']-$price).'</td>
					<td>
						<textarea maxlength="32" class="popup_company_notes" value=""></textarea>
					</td>
					
					<td></td>
					<td onclick="delete_case_company('.$i.');"><a href="#">Supprimer</a></td>
				</tr>
					';
					$balance -= $or['amount_to_pay'];
					$total += $or['amount_to_pay'];
					$i++;
				}
			}else{
				$balance = $_POST['data'][0]['balance_left_account'];
			}
			if($balance < 0){ 
				$balance = $_POST['data'][0]['balance_left_account'];
			}else{
				$balance = $balance;
			}
			
			echo'
				<tr class="order can_delete_'.$i.'">
					<td>
						<input type="hidden" disabled="disabled" class="popup_order_id is_order_'.$i.'"  value="NULL" />
						<input type="text" disabled="disabled" value="Solde au compte" />
					</td>
					<td hidden="hidden">
						<input disabled="disabled" type="text" class="popup_company_id is_user_'.$i.'" value="'.$user['id'].'" />
					</td>
					<td>
						<input type="text" disabled="disabled" class="popup_balance_left is_left_'.$i.'" value="'.number_format(($balance), 2, '.', '').'" />
					</td>
					<td>
						<input type="text" class="rows_counted popup_payment_due can_delete_val_'.$i.'" value="" />
					</td>
					<td></td>
					<td>
						<textarea maxlength="32" class="popup_company_notes" value=""></textarea>
					</td>
					<td></td>
					<td onclick="delete_case_company('.$i.');"><a href="#">Supprimer</a></td>
				</tr>
			';
			
			$total += $balance;	
			
			echo '
				<tr>
					<td colspan="7">
						<input type="text" disabled="disabled" class="total_payments" value="'.number_format($_POST['data'][0]['payment_due'], 2, '.', '').'" />
					</td>
				</tr>
			</table>
			';
			
		}
	}
	echo '
	</div>
	<div class="content_bottom_payment">
		<button class="btn_cancel_payment">Annuler</button>&nbsp;<button class="btn_pay_payment">Payer</button>
	</div> 
</div>
	';

	unset($_POST);
?>
