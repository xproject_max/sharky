<?php 
ini_set('max_execution_time', '0'); // for infinite time of execution 
/*
$now = time(); // or your date as well
$your_date = strtotime($_POST['last_edit'] );
$datediff = $now - $your_date;

$diff = round($datediff / (60 * 60 * 24));
 
*/
require(dirname(__DIR__)."/plugins/chrome-1.1.0/vendor/autoload.php");
use HeadlessChromium\BrowserFactory;
use HeadlessChromium\Clip;

$browserFactory = new BrowserFactory();
$browser = $browserFactory->createBrowser();
$page = $browser->createPage(); 

// navigate
$navigation = $page->navigate($_POST['url']);

// wait for the page to be loaded
$navigation->waitForNavigation();

// create a rectangle by specifying to left corner coordinates + width and height
$x = 0;
$y = 0;
$width = 1920;
$height = 1080;  
$clip = new Clip($x, $y, $width, $height); $page->setViewport(1920, 1080)->await();
echo '<img width="200" src="data:image/jpg;base64,'.$page->screenshot(array('clip' => $clip))->getBase64().'"/>'; 

$page->screenshot(array('clip' => $clip))->saveToFile('thumbs/'.$_POST['category'].'/'.$_POST['id'].'--'.$_POST['lastedit'].'.jpg');