
  /*
   * Helpers
  */
  jQuery(function(){
    $('.card').delay(200).css('opacity','1');
    $('#fond').css('marginTop','0');
  });

  var urls = $('#url_scripts').data('url-scripts');
  var is_running = false;
  function TessNotify(text, type){

    if(is_running == false){
      $.notify({
            message: '<div id="panel">'+text.trim()+'</div><input type="hidden" hidden="hidden" id="word" />'

        }, {
          type: type,
          timer: 4000,
          placement: {
              from: 'top',
              align: 'right'
          },
          onClosed: make_run()

      });
      // TessTalk(text)
      is_running = true;

    }
  }
  function make_run(){
      setTimeout(function(){
      is_running = false;
      }, 4000);
  }
  function showPages(url){
    $('#container').html();
    $('#container').load(url+' #container', function (e){
        setTimeout(function() {
        $(document).resize();
        $('*').removeClass('active');
        tess.initialize();
    }, 100);
    });
  }

  function TessModal(title, text){
      swal({
        title: title,
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success hide",
        html: text
      }).catch(swal.noop)
  }
  $('.connect_form').on('submit', function (e) {
      e.preventDefault();
      $('textarea.ckeditor').each(function () {
         var $textarea = $(this);
         $textarea.val(CKEDITOR.instances[$textarea.attr('name')].getData());
      });
      // data = new FormData(this);
      data =$(this).serialize();
      url = $(this).attr('action');

      if(is_running == false){
        $.ajax({
            type: "post",
            url  : url,
            data : data,
            datatype: "json",
            beforeSend: function()
            {
              $('#resultf').fadeIn(250);
              $("#resultf").html('<div style="color: rgba(88,1,1,0.81) !important;" class="la-ball-scale-multiple la"><div></div><div></div><div></div></div>');
            },
            success: function (data) {
                var data = jQuery.parseJSON(data);
                var color=data[0];
                var icon=data[1];
                var msg=data[2];
                var text = msg.trim();
                type = ['', 'info', 'danger', 'success', 'warning', 'rose', 'primary'];
                $.notify({
                    message: '<div id="panel">'+text.trim()+'</div><input type="hidden" hidden="hidden" id="word" />'

                }, {
                    type: type[color],
                    timer: 3000,
                    icon:'fad fa-waveform',
                    placement: {
                        from: 'top',
                        align: 'right'
                    },
                    onClosed: make_run()

                });
                // TessTalk(text)
                is_running = true;
                if(color==3)
                    setTimeout(function(){location.reload(); }, 2000);
            }
        });
      }
  });



  function TessTalk(text){
      $('#resultf').fadeIn(50);
      var random = Math.random( );
      var utterance = (utterance + random);
      utterance = new SpeechSynthesisUtterance();

      var global_words = [];
      utterance.lang = 'en-US';
      utterance.rate = 0.7;
      var words   = text.split(" ");
      var wordIndex = 0;
      global_words = words;
      // Draw the text in a div
      drawTextInPanel(words);
      spokenTextArray = words;
      utterance.text = text;
      speechSynthesis.speak(utterance);

      utterance.onboundary = function(event){
        var e = document.getElementById('panel');
        var word = getWordAt(e.value,event.charIndex);
        // Show Speaking word : x
        document.getElementById("word").innerHTML = word;
        if(document.getElementById("word_span_"+wordIndex) !== null)
            document.getElementById("word_span_"+wordIndex).style.color = "#ffffff";


        wordIndex++;
      };

      utterance.onend = function(){

        document.getElementById("word").innerHTML = "";
        wordIndex = 0;
        document.getElementById("panel").innerHTML += "";

        $('#resultf').fadeOut(1000);
      };

      // Get the word of a string given the string and the index
      function getWordAt(str, pos) {
        // Perform type conversions.
        str = String(str);
        pos = Number(pos) >>> 0;

        // Search for the word's beginning and end.
        var left = str.slice(0, pos + 1).search(/\S+$/),
        right = str.slice(pos).search(/\s/);

        // The last word in the string is a special case.
        if (right < 0) {
          return str.slice(left);
        }
        // Return the word, using the located bounds to extract it from the string.
        return str.slice(left, right + pos);
      }

      function drawTextInPanel(words_array){

        var panel = document.getElementById("panel");
        var html = "";
        panel.innerHTML = "";
        for(var i = 0;i < words_array.length;i++){
          html = '<b id="word_span_'+i+'">'+words_array[i]+'</b>&nbsp;';
          panel.innerHTML += html;
        }
      }
  }
  //Media image
  $('body').on('click', '.delete-btn-img', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      var inputval = $(this).data('id-img');
      var imgname = $(this).data('name-img');
      $.ajax({
        type : "post",
        url: ""+urls+"ajax.php",
        data : { page_id: inputval,imgname: imgname, delete_image: true },
        success : function (data){
          alert("deleted");
        }
      });
  });

  $('.deleteImage').click(function() {
      deleteImage($(this));
  });

  $('.deleteImageMembres').click(function() {
      deleteImageMembres($(this));
  });


  if($('.collapse .nav').children('li').find('a.active').length !== 0){
      var current = $('.collapse .nav').children('li').find('a.active');
      current.parents('li').parents('ul').parents('div.collapse').addClass('show');
  }
  $('.finder-icon').on('click', function (t){
    t.preventDefault();
    t.stopImmediatePropagation();
    var inputval = $('.search').val();
    $.ajax({
        type: "post",
        url : ""+urls+"ajax.php",
        data: {search_everywhere : '1', id: inputval},
        success: function(data){
          TessModal('',data);
          tess.initialize();
        }
    });
  });
  $('body').on('click', 'a.load_pages', function (e){
    e.preventDefault();
    e.stopImmediatePropagation();
    var inputval = $(this).attr('mid');
    $.ajax({
      type : "post",
      url: ""+urls+"ajax.php",
      data : { page_id: inputval, show_pages: true },
      success : function (data){
          $('#content').html(data);
          $('*').removeClass('active');
          tess.initialize();
      }
    });
  });
  $('body').on('click', 'a.load_modules', function (e){
    e.preventDefault();
    e.stopImmediatePropagation();
    var inputval = $(this).attr('mid');
    $.ajax({
      type : "post",
      url: ""+urls+"ajax.php",
      data : { page_id: inputval, show_modules: true },
      success : function (data){
          $('#content').html(data);
          $('*').removeClass('active');
          tess.initialize();
      }
    });
  });
  $('body').on('click', 'a.load_templates', function (e){
    e.preventDefault();
    e.stopImmediatePropagation();
    var inputval = $(this).attr('mid');
    $.ajax({
      type : "post",
      url: ""+urls+"ajax.php",
      data : { page_id: inputval, show_templates: true },
      success : function (data){
          $('#content').html(data);
          $('*').removeClass('active');
          tess.initialize();
      }
    });
  });
  $('body').on('click', 'a.load_inputs', function (e){
    e.preventDefault();
    e.stopImmediatePropagation();
    var inputval = $(this).attr('mid');
    $.ajax({
      type : "post",
      url: ""+urls+"ajax.php",
      data : { page_id: inputval, show_inputs: true },
      success : function (data){
          $('#content').html(data);
          $('*').removeClass('active');
          tess.initialize();
      }
    });
  });
  $('body').on('click', 'a.load_newsletter', function (e){
    e.preventDefault();
    e.stopImmediatePropagation();
    var inputval = $(this).data('mid');
    $.ajax({
      type : "post",
      url: ""+urls+"ajax.php",
      data : { page_id: inputval, show_newsletter: true },
      success : function (data){
          $('.container_newsletter').html(data);
          $('*').removeClass('active');
          $('a[data-mid='+inputval+']').addClass('active');
          tess.initialize();
      }
    });
  });
  function load_page(id){
    $.ajax({
      type : "post",
      url: ""+urls+"ajax.php",
      data : { page_id: id, show_pages: true },
      success : function (data){
          $('#container').html(data);
          tess.initialize();
      }
    });
  }
  $('.nav-link ').each(function(e){
      $(this).on('click', function (es){
          es.preventDefault();
          if(!$(this).find('a').hasClass('username') && $(this).hasClass('username') === false && !$(this).parent().hasClass('username')){
              var url = $(this).attr('href');
              var url_linv = $(this).attr('url_linv');
              window.history.pushState('obj', url, url);
              $('.nav-x').attr('href',url_linv);
              $('*').removeClass('active');
              $(this).addClass('active');
              $('#container').html();
              $('#container').load(url+ ' #container', function (e){
                  setTimeout(function() {
                  $(document).resize();
                  tess.initialize();
              }, 100);
              });

          }
      });
  });

  function confirm_link(message, url) {
      if(confirm(message)) location.href = url;
  }
  function readURL(input) {
      var parent = $(input).attr('parent');
      var id = $(input).attr('id-page');
      var name = $(input).attr('name');
        var reader = new FileReader();

        reader.onload = function(e) {
          if($('#form_edit_page').length > 0){
            var formData = new FormData($('#form_edit_page')[0]);
            formData.append("id_page",id);
          }
          if($('#form_add_page').length > 0){
            var formData = new FormData($('#form_add_page')[0]);
            formData.append("id_page",id);
          }
          if($('#form_add_product').length > 0){
            var formData = new FormData($('#form_add_product')[0]);
            formData.append("id_product",id);
          }
          if($('#form_edit_product').length > 0){

            var formData = new FormData($('#form_edit_product')[0]);
            formData.append("id_product",id);
          }
          if($('#form_add_blog').length > 0){
            var formData = new FormData($('#form_add_blog')[0]);
            formData.append("id_blog",id);
          }
          if($('#form_edit_blog').length > 0){

            var formData = new FormData($('#form_edit_blog')[0]);
            formData.append("id_blog",id);
          }
          if($('#form_add_contest').length > 0){
            var formData = new FormData($('#form_add_contest')[0]);
            formData.append("id_contest",id);
          }
          if($('#form_edit_contest').length > 0){

            var formData = new FormData($('#form_edit_contest')[0]);
            formData.append("id_contest",id);
          }
          if($('#form_add_promotion').length > 0){
            var formData = new FormData($('#form_add_promotion')[0]);
            formData.append("id_promotion",id);
          }
          if($('#form_edit_promotion').length > 0){

            var formData = new FormData($('#form_edit_promotion')[0]);
            formData.append("id_promotion",id);
          }
          if($('#form_add_tess_user').length > 0){
            var formData = new FormData($('#form_add_tess_user')[0]);
            formData.append("id_tess_users",id);
          }
          if($('#form_edit_tess_user').length > 0){

            var formData = new FormData($('#form_edit_tess_user')[0]);
            formData.append("id_tess_users",id);
          }
          if($('#form_add_users').length > 0){
            var formData = new FormData($('#form_add_users')[0]);
            formData.append("id_user",id);
          }
          if($('#form_edit_users').length > 0){

            var formData = new FormData($('#form_edit_users')[0]);
            formData.append("id_user",id);
          }
          if($('#form_add_companies').length > 0){
            var formData = new FormData($('#form_add_companies')[0]);
            formData.append("id_company",id);
          }
          if($('#form_edit_companies').length > 0){

            var formData = new FormData($('#form_edit_companies')[0]);
            formData.append("id_company",id);
          }
          if($('.edit_image_form').length > 0){

            var formData = new FormData($('.edit_image_form')[0]);
            formData.append("id_edit_image",id);
          }

          formData.append("upload",true);
          formData.append("current_img",name);
          $.ajax({
            type : "post",
            url : ""+urls+"ajax.php",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
              $("."+parent).attr('src', e.target.result).fadeIn('slow');
              // $(input).removeAttr("name");
              $("."+name).attr("name", name);
              $("."+name).val(data.trim());
            }
          });
        }
        reader.readAsDataURL(input.files[0]);

    }

  function removeAccents(strAccents) {
      var strAccents = strAccents.split('');
      var strAccentsOut = new Array();
      var strAccentsLen = strAccents.length;
      var accents = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
      var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
      for (var y = 0; y < strAccentsLen; y++) {
          if (accents.indexOf(strAccents[y]) != -1) {
              strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1);
          } else
              strAccentsOut[y] = strAccents[y];
      }
      strAccentsOut = strAccentsOut.join('');
      return strAccentsOut;
  }
  function stripAccents(tmp){
      tmp = tmp.replace(/ /g, '-');
      tmp = tmp.replace(/,/g, '-');
      tmp = tmp.replace(/:/g, '-');
      tmp = tmp.replace(/\./g, '-');
      tmp = tmp.replace(/\//g, '-');
      tmp = tmp.replace(/&/g, '-');
      tmp = tmp.replace(/\'/g, '-');
      tmp = tmp.replace(/\’/g, '-');
      tmp = tmp.replace(/\"/g, '-');
      tmp = tmp.replace(/\?/g, '');
      tmp = tmp.replace(/!/g, '');
      tmp = tmp.replace(/&([eaiouc]).*?;/i, '$1');
      tmp = tmp.replace(/--/g, '-');
      tmp = tmp.replace(/--/g, '-');
      tmp = tmp.replace(/%/g, '');
      tmp = tmp.toLowerCase();
      tmp = removeAccents(tmp);
      return tmp;
  }

  function deleteImage(btn) {
      row = $(btn).attr('data-filerow');
      pageid = $(btn).attr('data-pageid');
      if(confirm("Vous êtes sur le point de supprimer cet image de la page. Voulez-vous vraiement continuer?")){
              $.post("ajax.php","action=delete-image&row="+row+"&pageid="+pageid,function(data, e){
                  if(data){
                      $('#waitingImg_'+row).html('');
                      $('#waitingField_'+row).val('');
                      $(btn).remove();
                  } else {
                      alert(e);
                  }
              }, "html");
          }
  }

  function deleteImageMembres(btn) {
      row = $(btn).attr('data-filerow');
      pageid = $(btn).attr('data-pageid');
      if(confirm("Vous êtes sur le point de supprimer cet image de la page. Voulez-vous vraiement continuer?")){
          $.post("ajax.php","action=delete-imageProfile&row="+row+"&pageid="+pageid,function(data, e){
              if(data){
                  $('#waitingImg_profile_image').html('');
                  $('#waitingField_profile_image').val('');
                  $(btn).remove();
              } else {
                  alert(e);
              }
          }, "html");
      }
  }
  function delete_case(i){
      var total = $('.total_payments').val();
      var thisval = $('.can_delete_val_'+i).val();
      var newtotal = (total - thisval);
      $('.total_payments').val(''+newtotal.toFixed(2)+'');
      $('.can_delete_'+i).remove();
  }
