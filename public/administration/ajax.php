<?php
    use Modelss\Newsletter; 
    require_once "../../app/administration/initializing.php";
    require_once "../../app/administration/controllers/Tess.php";
    require_once "../../app/administration/controllers/Pages.php";
    require_once "../../app/administration/controllers/Orders.php";
    require_once "../../app/administration/controllers/Products.php";
    require_once "../../app/administration/controllers/Blogs.php";
    require_once "../../app/administration/controllers/Contests.php";
    require_once "../../app/administration/controllers/Promotions.php";
    require_once "../../app/administration/controllers/Roles.php";
    require_once "../../app/administration/controllers/Dictionary.php";
    require_once "../../app/administration/controllers/Quotes.php";
    require_once "../../app/administration/controllers/Medias.php"; 
    require_once "../../app/administration/models/Order.php";
    require_once "../../app/administration/models/Page.php";
    require_once "../../app/administration/models/User.php";
    require_once "../../app/administration/models/Company.php";
    require_once "../../app/administration/models/Product.php";
    require_once "../../app/administration/models/Blog.php";
    require_once "../../app/administration/models/Contest.php";
    require_once "../../app/administration/models/Promotion.php";
    require_once "../../app/administration/models/Role.php";
    require_once "../../app/administration/models/Tess_model.php";
    require_once "../../app/administration/models/Transaction.php";
    require_once "../../app/administration/models/Dictionary_model.php";
    require_once "../../app/administration/models/Media.php";
    require_once "../../app/administration/models/Newsletter.php";
    require_once "../../app/administration/models/Quotes_model.php"; 
    require_once "../../app/administration/models/Newsletter.php";
    require_once  "../../app/administration/config/lang.php";

    /* -------------------------------- IMAGES -------------------------------------*/

    //Post pictures or any uploads
    if(isset($_POST['upload'])){
        $upload = new Uploading();
        echo ltrim(rtrim($upload->upload($_POST, $_FILES)));
    }

    if(isset($_POST['delete_image'])){
       $pageid = $_POST['page_id'];
       $img = $_POST['imgname'];
       $Tess_db = new Database();
       $query_update = $Tess_db->query("
        UPDATE tbl_pages_inputs i RIGHT JOIN tbl_pages tp ON i.fk_pages = tp.id SET i.`value` = '', tp.`image` = '' WHERE tp.`image` = :img AND tp.id = :id
        ");
        $Tess_db->bind($query_update, ":img", $img);
        $Tess_db->bind($query_update, ":id", $pageid);
        if($Tess_db->execute($query_update)){

            // var_dump("UPDATE tbl_pages_inputs i RIGHT JOIN tbl_pages tp ON i.fk_pages = tp.id SET i.`value` = '', tp.`image` = '' WHERE tp.`value` = $img AND tp.id = $pageid", $img, $pageid);
            // die();
            return true;
        }else{
            // var_dump(print_r($query_update->errorInfo()));
            // die();
            return false;
        }
    }


    /* --------------------------------  LOGIN FORM -------------------------------*/
    if(isset($_POST['login_form'])){
        $return = [];
        $Users = new Tess();
        $data['secure_post'] = $Users->user->validate_users($_POST);
        $return[2] ="";
        $i=1;
        // var_dump($data['secure_post']);
        foreach($data['secure_post'] as $posted){
            if(is_array($posted)) {
                // var_dump($i, sizeof($data['secure_post']));
                if($i == sizeof($data['secure_post']))
                    $posted['message_error'] = strip_tags($posted['message_error']);
                $return[2] .= ($posted['message_error']);
            }
            $i++;

        }
        $return[1] = "fad fa-globe-asia";
        $return[0] = (isset($data['secure_post']['success']) ? 3 : (empty($_POST['username']) || empty($_POST['password']) ? 4 : 2));
        echo json_encode($return);
    }


    /* --------------------------------------- PAGE SYSTEM -------------------------------- */


    //Load add pages
    if(isset($_POST['load_add_page'])){
        global $lang, $l;
        $Pages = new Pages();
        $datapage = new Page();
        $page=array();
        foreach($_POST as $post => $value){
            if($value !== ""){
                $page[$post] = $value;
            }
        }
        $data = $page;
        $data['page'] = getEmptyValue("tbl_pages");
        // var_dump(getEmptyValue("tbl_pages"));
        return $Pages->view("pages/page", $data);
    }


    //If edit pages posted
    if(isset($_POST['edit_page'])){
        $Pages = new Pages();
        $datapage = new Page();
        $page_editing = $datapage->AddEditPageWebsite($_POST); //No second parameters then just editing page
        $page = $datapage->getPageWebsite($_POST['id_page']);
        generateSitemap();
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add pages posted
    if(isset($_POST['add_page'])){
        $Pages = new Pages();
        $datapage = new Page();
        $page_editing = $datapage->AddEditPageWebsite($_POST, true); //If second parameters true then adding new page;
        generateSitemap();
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //  MENU CHILDS
	
    if(isset($_POST['action']) && $_POST['action'] == "closelevel"){
        $Pages = new Pages(); 
		var_dump($_SESSION['saved_status_accordion']);
		unset($_SESSION['saved_status_accordion'][$_POST['id']]); 
		var_dump($_SESSION['saved_status_accordion']);
    }
	
    if(isset($_POST['action']) && $_POST['action'] == "load-child"){
        $Pages = new Pages();
		if($_POST['hided'] == "1"){ 
			unset($_SESSION['saved_status_accordion']);
		}else{
        $data = $Pages->page->getChildList($_POST['id'], $_POST['level']);
        return $data;
		}
    }
    //If delete pages posted
    if(isset($_POST['delete_page'])){
        $Pages = new Pages();
        $datapage = new Page();
        $page_editing = $datapage->DeletePageWebsite($_POST['page_id']); //If second parameters true then adding new page;

        generateSitemap();
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }


     //children modules PAGE
    if(isset($_POST['show_childs_pages'])){
        $Tess_db = new Database();
        require_once ("../../app/administration/config/lang.php");
        $templates = explode('-', $_POST['dtype']);
        if(count($templates) > 0) {
            $menu =  '<table>
            <tr><th><strong>'.$lang['choose_module'].'</strong><th></tr>';
            foreach($templates as $template) {
                try{
                    $letemplates = $Tess_db->query('SELECT * FROM tess_controller WHERE id = :id ');
                    $Tess_db->bind($letemplates,':id',$template);
                    $letemplates = $Tess_db->single($letemplates);
                    if($letemplates)
                        $menu .=  '<tr><td><a  href="#" dtype='.$letemplates->id.'" parent="'.$_POST['parent'].'" class="btn btn-success new_page add_child" style="width:100%"><span style="font-size:15px">'.$letemplates->title.'</span><br/><small>'.$letemplates->desc.'</small></a></td></tr>';


                }catch(PDOException $e){
                    return false;
                }
            }
            $menu .= '</table>';
            echo $menu;
        }
    }


    //If changing order/status pages
    if (isset($_POST['update_pages'])){
        global $lang, $l;

        $Pages = new Pages();
        $datapage = new Page();
        $online = $datapage->update_onlines($_POST);
        $order = $datapage->update_orders($_POST);
        $return[0] = $online;
        $return[1] = "success";
        echo json_encode($return);
	}


    //Load edit pages
    if(isset($_POST['show_pages'])){
        global $lang, $l;
       $Pages = new Pages();
       $datapage = new Page();
       $page = $datapage->getPageWebsite($_POST['page_id']);
       return $Pages->view("pages/page", $page[0]);
    }

    /* ----------------------------------------- MODULES SYSTEM ---------------------- */
    //If edit module posted
    if(isset($_POST['edit_module'])){
        $Pages = new Pages();
        $datapage = new Page();
        $page_editing = $datapage->AddEditModuleWebsite($_POST); //No second parameters then just editing page
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add module posted
    if(isset($_POST['add_module'])){
        $Pages = new Pages();
        $datapage = new Page();
        $page_editing = $datapage->AddEditModuleWebsite($_POST, true); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If delete module posted
    if(isset($_POST['delete_module'])){
        $Pages = new Pages();
        $datapage = new Page();
        $page_editing = $datapage->DeleteModuleWebsite($_POST['page_id']); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }


    //Load add module
    if(isset($_POST['load_add_module'])){
        global $lang;
        $Pages = new Pages();
        $datapage = new Page();
        $page=array();
        if(!isset($_POST['id']) || $_POST['id'] == ""){
            foreach($_POST as $post => $value){
                if($value !== ""){
                    $page[$post] = $value;
                }
            }
            $data = $page;
            $data['page'] = getEmptyValue("tess_controller");
        }else{
            $data = $datapage->getModules($_POST['id']);
        }
        return $Pages->view("pages/module", $data);
    }


    //Load modules
    if(isset($_POST['show_modules'])){
        global $lang, $l;
       $Pages = new Pages();
       $datapage = new Page();
       $page = $datapage->getModules($_POST['page_id']);
       return $Pages->view("pages/module", $page);
    }


    /* -------------------------------------------- INPUTS SYSTEM ------------------------------ */


    //If edit inputs posted
    if(isset($_POST['edit_inputs'])){
        global $lang, $l;
        $Pages = new Pages();
        $datapage = new Page();
        $page_editing = $datapage->AddEditInputsWebsite($_POST); //No second parameters then just editing page
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add inputs posted
    if(isset($_POST['add_inputs'])){
        global $lang, $l;
        $Pages = new Pages();
        $datapage = new Page();
        $page_editing = $datapage->AddEditInputsWebsite($_POST, true); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If delete inputs posted
    if(isset($_POST['delete_inputs'])){
        global $lang, $l;
        $Pages = new Pages();
        $datapage = new Page();
        $page_editing = $datapage->DeleteInputsWebsite($_POST['page_id']); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }

    //If changing order/status inputs
    if (isset($_POST['update_inputs'])){
        global $lang, $l;

        $Pages = new Pages();
        $datapage = new Page();
        $order = $datapage->updateOrdersInputs($_POST);
        $return[0] = $order;
        $return[1] = "success";
        echo json_encode($return);
	}


    //Load add inputs
    if(isset($_POST['load_add_inputs'])){
        $Pages = new Pages();
        $datapage = new Page();
        $page=array();
        if(!isset($_POST['id']) || $_POST['id'] == ""){
            foreach($_POST as $post => $value){
                if($value !== ""){
                    $page[$post] = $value;
                }
            }
            $data = $page;
            $data['page'] = getEmptyValue("inputs");
        }else{
            $data = $datapage->getInputs($_POST['id']);
        }
        return $Pages->view("pages/input", $data);
    }

    //Load inputs
    if(isset($_POST['show_inputs'])){
        global $lang, $l;
       $Pages = new Pages();
       $datapage = new Page();
       $page = $datapage->getInputs($_POST['page_id']);
       return $Pages->view("pages/input", $page);
    }

    /* --------------------------------------------- TEMPLATES SYSTEM ---------------------- */
    //If edit templates posted
    if(isset($_POST['edit_templates'])){
        global $lang, $l;
        $Pages = new Pages();
        $datapage = new Page();
        $page_editing = $datapage->AddEditTemplatesWebsite($_POST); //No second parameters then just editing page
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add templates posted
    if(isset($_POST['add_templates'])){
        global $lang, $l;
        $Pages = new Pages();
        $datapage = new Page();
        $page_editing = $datapage->AddEditTemplatesWebsite($_POST, true); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If delete templates posted
    if(isset($_POST['delete_templates'])){
        global $lang, $l;
        $Pages = new Pages();
        $datapage = new Page();
        $page_editing = $datapage->DeleteTemplatesWebsite($_POST['page_id']); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }



    //Load add templates
    if(isset($_POST['load_add_templates'])){
        global $lang;
        $Pages = new Pages();
        $datapage = new Page();
        $page=array();
        if(!isset($_POST['id']) || $_POST['id'] == ""){
            foreach($_POST as $post => $value){
                if($value !== ""){
                    $page[$post] = $value;
                }
            }
            $data = $page;
            $data['page'] = getEmptyValue("templates");
        }else{
            $data = $datapage->getTemplates($_POST['id']);
        }
        return $Pages->view("pages/template", $data);
    }


    //Load templates
    if(isset($_POST['show_templates'])){
        global $lang, $l;
       $Pages = new Pages();
       $datapage = new Page();
       $page = $datapage->getTemplates($_POST['page_id']);
       return $Pages->view("pages/template", $page);
    }

    /* ----------------------------------------- ORDER SYSTEM -------------------------------- */

    //PDF
    if(isset($_GET['bill'])){
		$decoded_bill = base64_decode($_GET['bill']);
		$exploded_bill = explode('||', $decoded_bill);
        $Tess_db = new Database();

        $bill = $Tess_db->query('SELECT * FROM tbl_orders WHERE id = :id AND date_created = :date');
        $Tess_db->bind($bill, ":id", $exploded_bill[0]);
        $Tess_db->bind($bill, ":date", $exploded_bill[1]);
        $bill = $Tess_db->single($bill);
        if(isset($_GET['download'])){
            create_pdf_template('bill', true, false,$bill, "bill");
        }else{
            create_pdf_template('bill', 'view', false,$bill, "bill");
        }
	}else if(isset($_GET['payment'])){
        $Tess_db = new Database();
        $payment_bill = base64_decode($_GET['payment']);
        $exploded_billp = explode('-', $payment_bill);
        $payment_exists = $Tess_db->query('SELECT * FROM tbl_orders WHERE id = :id AND date_created = :date');
        $Tess_db->bind($payment_exists, ":id", $exploded_billp[0]);
        $Tess_db->bind($payment_exists, ":date", $exploded_billp[1]);
        if($Tess_db->rowCount($payment_exists) > 0){
            $payment = $Tess_db->single($payment_exists);

            if($_GET['download']){
                create_pdf_template('payment_bill', true, false, $_GET['id'], "payment_orders");
            }else{
                create_pdf_template('payment_bill', 'view', false, $_GET['id'], "payment_orders");
            }
        }
        else{
            $Tess_db = new Database();
            // $user_mail->set_user($payment['id_user']);
            // $user_mail->set_order_infos_payments(0);
            if($_GET['download']){
                create_pdf_template('payment_bill_credit', true, false, $_GET['id'], "payment");
            }else{
                create_pdf_template('payment_bill_credit', 'view', false, $_GET['id'], "payment");
            }
        }
    }
    else{
        // echo 'Error';
    }

    // Export orders
    if(isset($_GET['export_btn'])){
        $orders = new Order();
        $start_date_timestamp = $_POST['start_date'];
        $end_date_timestamp = $_POST['end_date'];
        $array_orders = $orders->getOrdersFromTimePeriod($start_date_timestamp, $end_date_timestamp, $_POST);
        // var_dump($array_orders);
        $orders->generateOrdersXLSReports($array_orders, "export_orders");
	}
    // Filter orders
    if(isset($_POST['filter_orders'])){
        $orders = new Order();
        $start_date_timestamp = $_POST['start_date'];
        $end_date_timestamp = $_POST['end_date'];
        $array_orders = $orders->getOrdersFromTimePeriod($start_date_timestamp, $end_date_timestamp, $_POST);
        echo $orders->fetchOrdersFiltered($array_orders);
	}
//Load add products
    if(isset($_POST['load_add_order'])){
        global $lang, $l;
        $Products = new Orders();
        $Product = new Order();
        $page=array();
        foreach($_POST as $post => $value){
            if($value !== ""){
                $page[$post] = $value;
            }
        }
        $data = $page;
        $data['page'] = getEmptyValue("tbl_orders");
        return $Products->view("orders/order", $data);
    }

    //If edit orders posted
    if(isset($_POST['edit_order'])){
        $orders = new Order();
        $order = new Order();
        $page_editing = $order->AddEditorderWebsite($_POST); //No second parameters then just editing page
        $page = $order->getorderWebsite($_POST['id_order']);
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add orders posted
    if(isset($_POST['add_order'])){
        // $orders = new Order();
        // $order = new Order();
        // $page_editing = $order->AddEditorderWebsite($_POST, true); //If second parameters true then adding new page;
        // $return[0] = $page_editing['message'];
        // $return[1] = $page_editing['color'];
        // echo json_encode($return);
		$userid = "";
		$User = new User();
		$product_id="";
		$Product = new Product();
		$order_id="";
		$Order = new Order();
		$tid = "";
		$transaction = new Transaction();
		
		//User part
		if(!$User->ManualCheckIfExists($_POST['email'])){
			$userid = $User->ManualCreateUser($_POST);
		}else{
			$userid = $User->ManualGetUser($_POST)->id;
		}
		
		//Product part
		$product_id = $Product->ManualCreateProduct($_POST);
		
		//Order part
		$order_id = $Order->ManualCreateOrder($_POST, $userid, $product_id);
		
		$infoAccount = $User->getUserWebsite($userid);
		$_POST['total'] = $Order->getOrder($order_id);
		$_POST['payment_method'] = "credit_card";
		$tid = $transaction->add_transaction($_POST, $infoAccount, true);
		$transaction->add_transaction_details($tid, $order_id, $_POST, $infoAccount, true);
		
		if($userid == "" || $product_id == "" || $order_id == ""){
			$return[0] = "Error trying to add";
			$return[1] = "ff0000";
		}else{
			$return[0] = "success";
			$return[1] = "00ff00";
			
		}
		echo json_encode($return);
    }
    //If delete orders posted
    if(isset($_POST['delete_order'])){
        $orders = new Order();
        $order = new Order();
        $page_editing = $order->DeleteorderWebsite($_POST['order_id']); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }

    //  MENU CHILDS orders
    if(isset($_POST['action']) && $_POST['action'] == "load-child-orders"){
        $orders = new Order();
        $data = $orders->page->getChildList($_POST['id'], $_POST['level']);
        return $data;
    }
     
    //If changing order/status orders
    if (isset($_POST['update_orders'])){
        global $lang, $l;
        $orders = new Order();
        $order = new Order();
        $online = $order->update_onlines($_POST);
        $order = $order->update_orders($_POST);
        $return[0] = $online;
        $return[1] = "success";
        echo json_encode($return);
	}

    /* ------------------------------------------- PRODUCTS SYSTEM ----------------------------- */

    //Load add products
    if(isset($_POST['load_add_product'])){
        global $lang, $l;
        $Products = new Products();
        $Product = new Product();
        $page=array();
        foreach($_POST as $post => $value){
            if($value !== ""){
                $page[$post] = $value;
            }
        }
        $data = $page;
        $data['page'] = getEmptyValue("tbl_products");
        return $Products->view("products/product", $data);
    }

    //If edit products posted
    if(isset($_POST['edit_product'])){
        $Products = new Products();
        $Product = new Product();
        $page_editing = $Product->AddEditProductWebsite($_POST); //No second parameters then just editing page
        $page = $Product->getProductWebsite($_POST['id_product']);
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add products posted
    if(isset($_POST['add_product'])){
        $Products = new Products();
        $Product = new Product();
        $page_editing = $Product->AddEditProductWebsite($_POST, true); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If delete products posted
    if(isset($_POST['delete_product'])){
        $Products = new Products();
        $Product = new Product();
        $page_editing = $Product->DeleteProductWebsite($_POST['product_id']); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }

    //  MENU CHILDS Products
    if(isset($_POST['action']) && $_POST['action'] == "load-child-products"){
        $Products = new Products();
        $data = $Products->page->getChildList($_POST['id'], $_POST['level']);
        return $data;
    }
    //Load edit products
    if(isset($_POST['show_products'])){
        global $lang, $l;
       $Pages = new Products();
       $datapage = new Product();
       $page = $datapage->getProductWebsite($_POST['product_id']);
       return $Pages->view("products/product", $page[0]);
    }

    //If changing order/status products
    if (isset($_POST['update_products'])){
        global $lang, $l;
        $Products = new Products();
        $Product = new Product();
        $online = $Product->update_onlines($_POST);
        $order = $Product->update_orders($_POST);
        $return[0] = $online;
        $return[1] = "success";
        echo json_encode($return);
	}


    //children modules products
    if(isset($_POST['show_childs_products'])){
        $Tess_db = new Database();
        require_once ("../../app/administration/config/lang.php");
        $templates = explode('-', $_POST['dtype']);
        if(count($templates) > 0) {
            $menu =  '<table>
            <tr><th><strong>'.$lang['choose_module'].'</strong><th></tr>';
            foreach($templates as $template) {
                try{
                    $letemplates = $Tess_db->query('SELECT * FROM tess_controller WHERE id = :id ');
                    $Tess_db->bind($letemplates,':id',$template);
                    $letemplates = $Tess_db->single($letemplates);
                    if($letemplates)
                        $menu .=  '<tr><td><a  href="#" dtype='.$letemplates->id.'" parent="'.$_POST['parent'].'" class="btn btn-success new_product add_child_product" style="width:100%"><span style="font-size:15px">'.$letemplates->title.'</span><br/><small>'.$letemplates->desc.'</small></a></td></tr>';


                }catch(PDOException $e){
                    return false;
                }
            }
            $menu .= '</table>';
            echo $menu;
        }
    }

    /* ----------------------------------------- BLOG SYSTEM ------------------------------- */

    //Load add blogs
    if(isset($_POST['load_add_blog'])){
        global $lang, $l;
        $blogs = new Blogs();
        $blog = new Blog();
        $page=array();
        foreach($_POST as $post => $value){
            if($value !== ""){
                $page[$post] = $value;
            }
        }
        $data = $page;
        $data['page'] = getEmptyValue("tbl_blogs");
        return $blogs->view("blogs/blog", $data);
    }

    //  MENU CHILDS blogs
    if(isset($_POST['action']) && $_POST['action'] == "load-child-blogs"){
        $Blogs = new Blogs();
        $data = $Blogs->page->getChildList($_POST['id'], $_POST['level']);
        return $data;
    }

    //If edit blogs posted
    if(isset($_POST['edit_blog'])){
        $blogs = new Blogs();
        $blog = new Blog();
        $page_editing = $blog->AddEditblogWebsite($_POST); //No second parameters then just editing page
        $page = $blog->getblogWebsite($_POST['id_blog']);
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add blogs posted
    if(isset($_POST['add_blog'])){
        $blogs = new Blogs();
        $blog = new Blog();
        $page_editing = $blog->AddEditblogWebsite($_POST, true); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If delete blogs posted
    if(isset($_POST['delete_blog'])){
        $blogs = new Blogs();
        $blog = new Blog();
        $page_editing = $blog->DeleteblogWebsite($_POST['blog_id']); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }

    //  MENU CHILDS blogs
    if(isset($_POST['action']) && $_POST['action'] == "load-child-blogs"){
        $Products = new Blogs();
        $data = $Products->page->getChildList($_POST['id'], $_POST['level']);
        return $data;
    }

    //If changing order/status blogs
    if (isset($_POST['update_blogs'])){
        global $lang, $l;
        $blogs = new Blogs();
        $blog = new Blog();
        $online = $blog->update_onlines($_POST);
        $order = $blog->update_orders($_POST);
        $return[0] = $online;
        $return[1] = "success";
        echo json_encode($return);
	}


    //children modules blogs
    if(isset($_POST['show_childs_blogs'])){
        $Tess_db = new Database();
        require_once ("../../app/administration/config/lang.php");
        $templates = explode('-', $_POST['dtype']);
        if(count($templates) > 0) {
            $menu =  '<table>
            <tr><th><strong>'.$lang['choose_module'].'</strong><th></tr>';
            foreach($templates as $template) {
                try{
                    $letemplates = $Tess_db->query('SELECT * FROM tess_controller WHERE id = :id ');
                    $Tess_db->bind($letemplates,':id',$template);
                    $letemplates = $Tess_db->single($letemplates);
                    if($letemplates)
                        $menu .=  '<tr><td><a  href="#" dtype='.$letemplates->id.'" parent="'.$_POST['parent'].'" class="btn btn-success new_blog add_child_blog" style="width:100%"><span style="font-size:15px">'.$letemplates->title.'</span><br/><small>'.$letemplates->desc.'</small></a></td></tr>';


                }catch(PDOException $e){
                    return false;
                }
            }
            $menu .= '</table>';
            echo $menu;
        }
    }

    //Load edit blogs
    if(isset($_POST['show_blogs'])){
        global $lang, $l;
       $Pages = new Blogs();
       $datapage = new Blog();
       $page = $datapage->getblogWebsite($_POST['blog_id']);
       return $Pages->view("blogs/blog", $page[0]);
    }
    /* ----------------------------------------- CONTEST SYSTEM ----------------------------------------- */

    //If edit contests posted
    if(isset($_POST['edit_contest'])){
        $contests = new contests();
        $contest = new contest();
        $page_editing = $contest->AddEditcontestWebsite($_POST); //No second parameters then just editing page
        $page = $contest->getcontestWebsite($_POST['id_contest']);
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add contests posted
    if(isset($_POST['add_contest'])){
        $contests = new contests();
        $contest = new contest();
        $page_editing = $contest->AddEditcontestWebsite($_POST, true); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If delete contests posted
    if(isset($_POST['delete_contest'])){
        $contests = new contests();
        $contest = new contest();
        $page_editing = $contest->DeletecontestWebsite($_POST['contest_id']); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }


    //If changing order/status contests
    if (isset($_POST['update_contests'])){
        global $lang, $l;
        $contests = new contests();
        $contest = new contest();
        $online = $contest->update_onlines($_POST);
        $order = $contest->update_orders($_POST);
        $return[0] = $online;
        $return[1] = "success";
        echo json_encode($return);
	}


    //  MENU CHILDS contests
    if(isset($_POST['action']) && $_POST['action'] == "load-child-contests"){
        $Contests = new Contests();
        $data = $Contests->page->getChildList($_POST['id'], $_POST['level']);
        return $data;
    }


    //children modules contests
    if(isset($_POST['show_childs_contests'])){
        $Tess_db = new Database();
        require_once ("../../app/administration/config/lang.php");
        $templates = explode('-', $_POST['dtype']);
        if(count($templates) > 0) {
            $menu =  '<table>
            <tr><th><strong>'.$lang['choose_module'].'</strong><th></tr>';
            foreach($templates as $template) {
                try{
                    $letemplates = $Tess_db->query('SELECT * FROM tess_controller WHERE id = :id ');
                    $Tess_db->bind($letemplates,':id',$template);
                    $letemplates = $Tess_db->single($letemplates);
                    if($letemplates)
                        $menu .=  '<tr><td><a  href="#" dtype='.$letemplates->id.'" parent="'.$_POST['parent'].'" class="btn btn-success new_contest add_child_contest" style="width:100%"><span style="font-size:15px">'.$letemplates->title.'</span><br/><small>'.$letemplates->desc.'</small></a></td></tr>';


                }catch(PDOException $e){
                    return false;
                }
            }
            $menu .= '</table>';
            echo $menu;
        }
    }

    //Load add contests
    if(isset($_POST['load_add_contest'])){
        global $lang, $l;
        $contests = new contests();
        $contest = new contest();
        $page=array();
        foreach($_POST as $post => $value){
            if($value !== ""){
                $page[$post] = $value;
            }
        }
        $data = $page;
        $data['page'] = getEmptyValue("tbl_contests");
        return $contests->view("contests/contest", $data);
    }

    //Load edit contests
    if(isset($_POST['show_contests'])){
        global $lang, $l;
       $Contests = new Contests();
       $Contest = new Contest();
       $page = $Contest->getcontestWebsite($_POST['contest_id']);
       return $Contests->view("contests/contest", $page[0]);
    }
    /* ---------------------------------------- PROMOTIONS SYSTEM ----------------------------------- */


    //Load add promotions
    if(isset($_POST['load_add_promotion'])){
        global $lang, $l;
        $promotions = new promotions();
        $promotion = new promotion();
        $page=array();
        foreach($_POST as $post => $value){
            if($value !== ""){
                $page[$post] = $value;
            }
        }
        $data = $page;
        $data['page'] = getEmptyValue("tbl_promotions");
        return $promotions->view("promotions/promotion", $data);
    }

    //MENU CHILDS promotions
    if(isset($_POST['action']) && $_POST['action'] == "load-child-promotions"){
        $Promotions = new Promotions();
        $data = $Promotions->page->getChildList($_POST['id'], $_POST['level']);
        return $data;
    }

    //If edit promotions posted
    if(isset($_POST['edit_promotion'])){
        $promotions = new promotions();
        $promotion = new promotion();
        $page_editing = $promotion->AddEditpromotionWebsite($_POST); //No second parameters then just editing page
        $page = $promotion->getpromotionWebsite($_POST['id_promotion']);
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add promotions posted
    if(isset($_POST['add_promotion'])){
        $promotions = new promotions();
        $promotion = new promotion();
        $page_editing = $promotion->AddEditpromotionWebsite($_POST, true); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If delete promotions posted
    if(isset($_POST['delete_promotion'])){
        $promotions = new promotions();
        $promotion = new promotion();
        $page_editing = $promotion->DeletepromotionWebsite($_POST['promotion_id']); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }


    //If changing order/status promotions
    if (isset($_POST['update_promotions'])){
        global $lang, $l;
        $promotions = new promotions();
        $promotion = new promotion();
        $online = $promotion->update_onlines($_POST);
        $order = $promotion->update_orders($_POST);
        $return[0] = $online;
        $return[1] = "success";
        echo json_encode($return);
	}


    //children modules promotions
    if(isset($_POST['show_childs_promotions'])){
        $Tess_db = new Database();
        require_once ("../../app/administration/config/lang.php");
        $templates = explode('-', $_POST['dtype']);
        if(count($templates) > 0) {
            $menu =  '<table>
            <tr><th><strong>'.$lang['choose_module'].'</strong><th></tr>';
            foreach($templates as $template) {
                try{
                    $letemplates = $Tess_db->query('SELECT * FROM tess_controller WHERE id = :id ');
                    $Tess_db->bind($letemplates,':id',$template);
                    $letemplates = $Tess_db->single($letemplates);
                    if($letemplates)
                        $menu .=  '<tr><td><a  href="#" dtype='.$letemplates->id.'" parent="'.$_POST['parent'].'" class="btn btn-success new_promotion add_child_promotion" style="width:100%"><span style="font-size:15px">'.$letemplates->title.'</span><br/><small>'.$letemplates->desc.'</small></a></td></tr>';


                }catch(PDOException $e){
                    return false;
                }
            }
            $menu .= '</table>';
            echo $menu;
        }
    }

    //Load edit promotions
    if(isset($_POST['show_promotions'])){
        global $lang, $l;
       $Promotions = new Promotions();
       $Promotion = new Promotion();
       $page = $Promotion->getpromotionWebsite($_POST['promotion_id']);
       return $Promotions->view("promotions/promotion", $page[0]);
    }
    /* ------------------------------------------- TESS USERS ------------------------------------ */

    //If edit edit_tess_users posted
    if(isset($_POST['edit_tess_user'])){
        $Tess_model = new Tess_model();
        $page_editing = $Tess_model->AddEditTessMembers($_POST); //No second parameters then just editing page
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add add_tess_users posted
    if(isset($_POST['add_tess_user'])){
        $Tess_model = new Tess_model();
        $page_editing = $Tess_model->AddEditTessMembers($_POST, true); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }

    //Load add tess_user
    if(isset($_POST['load_add_tess_user'])){
        global $lang, $l;
        $Tess_model = new Tess_model();
        $page=array();
        foreach($_POST as $post => $value){
            if($value !== ""){
                $page[$post] = $value;
            }
        }
        $data = $page;
        $data['page'] = getEmptyValue("tess_members");
        return $Tess_model->view("tess/profile", $data);
    }
    //If changing order/status tess users
    if (isset($_POST['update_tess_users'])){
        global $lang, $l;
        $tess_users = new Tess_model();
        $online = $tess_users->update_onlines($_POST);
        $return[0] = $online;
        $return[1] = "success";
        echo json_encode($return);
	}

    //If delete promotions posted
    if(isset($_POST['delete_tess_users'])){
        $delete_tess_users = new Tess_model();
        $page_editing = $delete_tess_users->DeletetessUsersWebsite($_POST['usertess_id']); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }

    //Load edit tess user
    if(isset($_POST['show_tess_users'])){
        global $lang, $l;
       $Tess_model = new Tess_model();
       $page = $Tess_model->gettessUserWebsite($_POST['tess_users_id']);
       return $Tess_model->view("tess/profile", $page[0]);
    }

    /* --------------------------------------- TESS ROLES -------------------------------------- */

    //If edit roles posted
    if(isset($_POST['edit_roles'])){
        $Role = new Role();
        $page_editing = $Role->AddEditTessroles($_POST); //No second parameters then just editing page
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add roles posted
    if(isset($_POST['add_roles'])){
        $Role = new Role();
        $page_editing = $Role->AddEditTessroles($_POST, true); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }

    //Load add roles
    if(isset($_POST['load_add_roles'])){
        global $lang, $l;
        $Role= new Roles();
        $page=array();
        foreach($_POST as $post => $value){
            if($value !== ""){
                $page[$post] = $value;
            }
        }
        $data = $page;
        $data['page'] = getEmptyValue("tess_members_roles");
        return $Role->view("roles/role", $data);
    }
    //If changing order/status tess roles
    if (isset($_POST['update_roles'])){
        global $lang, $l;
        $roles = new Role();
        $online = $roles->update_rights($_POST);
        $return[0] = $online;
        $return[1] = "success";
        echo json_encode($return);
	}

    //If delete roles posted
    if(isset($_POST['delete_roles'])){
        $delete_roles = new Role();
        $page_editing = $delete_roles->DeleteTessRoles($_POST['roles_id']); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }


    //Load edit tess roles
    if(isset($_POST['show_roles'])){
        global $lang, $l;
       $Roles = new Roles();
       $Role = new Role();
       $page = $Role->getrolestess($_POST['roles_id']);
       return $Roles->view("roles/role", $page[0]);
    }
    /* --------------------------------------- QUOTES -------------------------------------- */

    //If edit roles posted
    if(isset($_POST['edit_quotes'])){
        $Role = new Quotes_model();
        $page_editing = $Role->AddEditQuotes($_POST); //No second parameters then just editing page
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add roles posted
    if(isset($_POST['add_quotes'])){
        $Role = new Quotes_model();
        $page_editing = $Role->AddEditQuotes($_POST, true); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }

    //Load add roles
    if(isset($_POST['load_edit_quotes'])){
        global $lang, $l;
        $Role= new Quotes_model();
        $page = $Role->getquotes($_POST['id_quotes']);
        
        foreach($_POST as $post => $value){
            if($value !== ""){
                $page[$post] = $value;
            }
        }
        $data = $page; 
        return $Role->view("quotes/quote", $data[0]);
    } 

    //If delete roles posted
    if(isset($_POST['delete_quotes'])){
        $delete_roles = new Quotes_model();
        $page_editing = $delete_roles->DeleteQuotes($_POST['quotes_id']); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }


    //Load edit tess roles
    if(isset($_POST['show_quotes'])){
        global $lang, $l;
       $Roles = new Quotes();
       $Role = new Quotes_model();
       $page = $Role->getquotes($_POST['quotes_id']);
       return $Roles->view("quotes/quote", $page[0]);
    }
    /* --------------------------------------- DOMAINS -------------------------------------- */

    //If edit roles posted
    if(isset($_POST['edit_domains'])){
        $Role = new Domains_model();
        $page_editing = $Role->AddEditDomains($_POST); //No second parameters then just editing page
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add roles posted
    if(isset($_POST['add_domains'])){
        $Role = new Domains_model();
        $page_editing = $Role->AddEditDomains($_POST, true); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }

    //Load add roles
    if(isset($_POST['load_add_domains'])){
        global $lang, $l;
        $Role= new Domains_model();
        $page=array();
        foreach($_POST as $post => $value){
            if($value !== ""){
                $page[$post] = $value;
            }
        }
        $data = $page;
        $data['page'] = getEmptyValue("tbl_domains");
        return $Role->view("domains/domain", $data);
    } 
	//Load add roles
    if(isset($_POST['load_edit_domains'])){
        global $lang, $l;
        $Role= new Domains_model();
        $page = $Role->getdomains($_POST['id_domains']);
        
        foreach($_POST as $post => $value){
            if($value !== ""){
                $page[$post] = $value;
            }
        }
        $data = $page; 
        return $Role->view("domains/domain", $data[0]);
    } 

    //If delete roles posted
    if(isset($_POST['delete_domains'])){
        $delete_roles = new Domains_model();
        $page_editing = $delete_roles->DeleteDomains($_POST['domains_id']); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }


    //Load edit tess roles
    if(isset($_POST['show_domains'])){
        global $lang, $l;
       $Roles = new Domains_model();
       $Role = new Domains_model();
       $page = $Role->getdomains($_POST['domains_id']);
       return $Roles->view("domains/domain", $page[0]);
    }
	
    //If changing order/status tess roles
    if (isset($_POST['update_status_domains'])){ 
        global $lang, $l;
		$domains = new Domains_model(); 
        $online = $domains->update_orders($_POST);
        $online = $domains->update_onlines($_POST);
        $return[0] = $online;
        $return[1] = "success";
        echo json_encode($return);
	
	}
	
	
    /* ------------------------------------------------ WEBSITE ROLES ------------------------------------ */

    //If edit roles posted
    if(isset($_POST['edit_website_roles'])){
        $Role = new Role();
        $page_editing = $Role->AddEditWebsiteRoles($_POST); //No second parameters then just editing page
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add roles posted
    if(isset($_POST['add_website_roles'])){
        $Role = new Role();
        $page_editing = $Role->AddEditWebsiteRoles($_POST, true); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }

    //Load add roles
    if(isset($_POST['load_add_website_roles'])){
        global $lang, $l;
        $Role= new Roles();
        $page=array();
        foreach($_POST as $post => $value){
            if($value !== ""){
                $page[$post] = $value;
            }
        }
        $data = $page;
        $data['page'] = getEmptyValue("tbl_roles");
        return $Role->view("roles/website_role", $data);
    }
    //If changing order/status tess roles
    if (isset($_POST['update_website_roles'])){
        global $lang, $l;
        $roles = new Role();
        $online = $roles->update_website_rights($_POST);
        $return[0] = $online;
        $return[1] = "success";
        echo json_encode($return);
	}

    //If delete roles posted
    if(isset($_POST['delete_website_roles'])){
        $delete_roles = new Role();
        $page_editing = $delete_roles->DeleteWebsiteRoles($_POST['roles_id']);
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //Load edit website roles
    if(isset($_POST['show_website_roles'])){
        global $lang, $l;
       $Roles = new Roles();
       $Role = new Role();
       $page = $Role->getWebsiteRoles($_POST['roles_id']);
       return $Roles->view("roles/website_role", $page[0]);
    }

    /* ---------------------------------------- WEBSITE USERS ----------------------- */


    // Export Users
    if(isset($_GET['export_user'])){
        $orders = new User();
        $start_date_timestamp = $_POST['start_date'];
        $end_date_timestamp = $_POST['end_date'];
        $array_users = $orders->getUsersFromTimePeriod($start_date_timestamp, $end_date_timestamp, $_POST);
        $orders->generateUsersXLSReports($array_users, "export_users");
	}
    // Filter Users
    if(isset($_POST['filter_users'])){
        $orders = new User();
        $db = new Database("Tess_db");
        $start_date_timestamp = $_POST['start_date'];
        $end_date_timestamp = $_POST['end_date'];
        $array_users = $orders->getUsersFromTimePeriod($start_date_timestamp, $end_date_timestamp, $_POST);
        echo $orders->fetchUsersFiltered($array_users);
	}

    //If edit edit_tess_users posted
    if(isset($_POST['edit_user'])){
        $user = new User();
        $page_editing = $user->AddEditUsers($_POST); //No second parameters then just editing page
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add add_tess_users posted
    if(isset($_POST['add_user'])){
        $user = new User();
        $page_editing = $user->AddEditUsers($_POST, true); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }

    //Load add tess_user
    if(isset($_POST['load_add_user'])){
        global $lang, $l;
        $User = new User();
        $page=array();
        foreach($_POST as $post => $value){
            if($value !== ""){
                $page[$post] = $value;
            }
        }
        $data = $page;
        $data['page'] = getEmptyValue("tbl_users");
        return $User->view("users/user", $data);
    }
    //If changing order/status tess users
    if (isset($_POST['update_status_users'])){
        global $lang, $l;
        $User = new User();
        $online = $User->update_onlines($_POST);
        $return[0] = $online;
        $return[1] = "success";
        echo json_encode($return);
	}

    //If delete promotions posted
    if(isset($_POST['delete_users'])){
        $User = new User();
        $page_editing = $User->DeleteUsersWebsite($_POST['users_id']); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }

    //Load edit tess user
    if(isset($_POST['show_users'])){
        global $lang, $l, $_GET;
       $User = new User();
       $page = $User->getUserWebsite((isset($_POST['users_id']) ? $_POST['users_id'] : $_GET['userid']));
       return $User->view("users/user", $page[0]);
    }

    /* Add credit to user */
    if(isset($_POST['add_credit'])){
        $return = array();
        $User = new User();
        $page_editing = $User->add_credit($_POST['id'], $_POST);
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
        exit;
    }
 /* ----------------------------------- WEBSITE companies ----------------------- */


    // Export companies
    if(isset($_GET['export_companies'])){
        $companies = new Company();
        $start_date_timestamp = $_POST['start_date'];
        $end_date_timestamp = $_POST['end_date'];
        $array_companies = $companies->getCompaniesFromTimePeriod($start_date_timestamp, $end_date_timestamp, $_POST);
        $companies->generateCompaniesXLSReports($array_companies, "export_companies");
	}
    // Filter companies
    if(isset($_POST['filter_companies'])){
        $companies = new Company();
        $db = new Database("Tess_db");
        $start_date_timestamp = $_POST['start_date'];
        $end_date_timestamp = $_POST['end_date'];
        $array_companies = $companies->getCompaniesFromTimePeriod($start_date_timestamp, $end_date_timestamp, $_POST);
        echo $companies->fetchCompaniesFiltered($array_companies);
	}


    //If edit edit_tess_companies posted
    if(isset($_POST['edit_company'])){
        $company = new company();
        $page_editing = $company->AddEditcompanies($_POST); //No second parameters then just editing page
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add add_tess_companies posted
    if(isset($_POST['add_company'])){
        $company = new company();
        $page_editing = $company->AddEditcompanies($_POST, true); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }

    //Load add tess_company
    if(isset($_POST['load_add_company'])){
        global $lang, $l;
        $company = new company();
        $page=array();
        foreach($_POST as $post => $value){
            if($value !== ""){
                $page[$post] = $value;
            }
        }
        $data = $page;
        $data['page'] = getEmptyValue("tbl_companies");
        return $company->view("companies/company", $data);
    }
    //If changing order/status tess companies
    if (isset($_POST['update_status_companies'])){
        global $lang, $l;
        $company = new company();
        $online = $company->update_onlines($_POST);
        $return[0] = $online;
        $return[1] = "success";
        echo json_encode($return);
	}

    //If delete promotions posted
    if(isset($_POST['delete_companies'])){
        $company = new company();
        $page_editing = $company->DeletecompaniesWebsite($_POST['companies_id']); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }

    //Load edit tess company
    if(isset($_POST['show_companies'])){
        global $lang, $l, $_GET;
       $company = new company();
       $page = $company->getcompanyWebsite((isset($_POST['companies_id']) ? $_POST['companies_id'] : $_GET['companyid']));
       return $company->view("companies/company", $page[0]);
    }

    /* Add credit to company */
    if(isset($_POST['add_credit_company'])){
        $return = array();
        $company = new company();
        $page_editing = $company->add_credit($_POST['id'], $_POST);
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
        exit;
    }

    /* --------------------------------------- DICTIONARY CATEG -------------------------------------- */

    //If edit categories posted
    if(isset($_POST['edit_categories'])){
        $Role = new Dictionary_model();
        $page_editing = $Role->AddEditCategories($_POST); //No second parameters then just editing page
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add categories posted
    if(isset($_POST['add_categories'])){
        $Role = new Dictionary_model();
        $page_editing = $Role->AddEditCategories($_POST, true); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }

    //Load add categories
    if(isset($_POST['load_add_categories'])){
        global $lang, $l;
        $Role= new Dictionary();
        $page=array();
        foreach($_POST as $post => $value){
            if($value !== ""){
                $page[$post] = $value;
            }
        }
        $data = $page;
        $data['page'] = getEmptyValue("dictionary_categ");
        return $Role->view("dictionary/category", $data);
    }
    //If changing order/status tess categories
    if (isset($_POST['update_categories'])){
        global $lang, $l;
        $categories = new Dictionary_model();
        $online = $categories->update_order($_POST);
        $return[0] = $online;
        $return[1] = "success";
        echo json_encode($return);
	}

    //If delete categories posted
    if(isset($_POST['delete_categories'])){
        $delete_categories = new Dictionary_model();
        $page_editing = $delete_categories->DeleteCategories($_POST['categories_id']); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }


    //Load edit tess categories
    if(isset($_POST['show_categories'])){
        global $lang, $l;
       $Categories = new Dictionary_model();
       $Dictionary = new Dictionary();
       $page = $Categories->getcategories($_POST['categories_id']);
       return $Dictionary->view("dictionary/category", $page[0]);
    }

    /* --------------------------------------- DICTIONARY TERM -------------------------------------- */

    //If edit terms posted
    if(isset($_POST['edit_terms'])){
        $Role = new Dictionary_model();
        $page_editing = $Role->AddEditTerms($_POST); //No second parameters then just editing page
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add terms posted
    if(isset($_POST['add_terms'])){
        $Role = new Dictionary_model();
        $page_editing = $Role->AddEditTerms($_POST, true); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }

    //MENU CHILDS terms
    if(isset($_POST['action']) && $_POST['action'] == "load-child-terms"){
        $Terms = new Dictionary_model();
        $data = $Terms->getChildTerms($_POST['id'], $_POST['level']);
        return $data;
    }
    //Load add terms
    if(isset($_POST['load_add_terms'])){
        global $lang, $l;
        $Role= new Dictionary();
        $page=array();
        foreach($_POST as $post => $value){
            if($value !== ""){
                $page[$post] = $value;
            }
        }
        $data = $page;
        $data['page'] = getEmptyValue("dictionary_categ");
        return $Role->view("dictionary/term", $data);
    }
    //If changing order/status tess terms
    if (isset($_POST['update_terms'])){
        global $lang, $l;
        $terms = new Dictionary_model();
        $online = $terms->update_order($_POST);
        $return[0] = $online;
        $return[1] = "success";
        echo json_encode($return);
	}

    //If delete terms posted
    if(isset($_POST['delete_terms'])){
        $delete_terms = new Dictionary_model();
        $page_editing = $delete_terms->DeleteTerms($_POST['terms_id']); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }


    //Load edit tess terms
    if(isset($_POST['show_terms'])){
        global $lang, $l;
       $Terms = new Dictionary_model();
       $Dictionary = new Dictionary();
       $page = $Terms->getterms($_POST['terms_id']);
       return $Dictionary->view("dictionary/term", $page[0]);
    }


    if(isset($_POST['show_childs_terms'])){
        global $lang, $l;
       $Terms = new Dictionary_model();
       $Dictionary = new Dictionary();
       $page['page'] = getEmptyValue("dictionary_term");
       $page['page']->categ =  $_POST['parent'];
       $page['load_add_terms'] = true;
       return $Dictionary->view("dictionary/term", $page);

    }

    /* --------------------------------------- MEDIAS -------------------------------------- */

    //If edit medias posted
    if(isset($_POST['edit_medias'])){
        $Role = new Media();
        $page_editing = $Role->AddEditmedias($_POST); //No second parameters then just editing page
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }
    //If add medias posted
    if(isset($_POST['add_medias'])){
        $Role = new Media();
        $page_editing = $Role->AddEditmedias($_POST, true); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }

    //MENU CHILDS medias
    if(isset($_POST['action']) && $_POST['action'] == "load-child-medias"){
        $medias = new Media();
        $data = $medias->getChildmedias($_POST['id'], $_POST['level']);
        return $data;
    }
    //Load add medias
    if(isset($_POST['load_add_medias'])){
        global $lang, $l;
        $Role= new Media();
        $page=array();
        foreach($_POST as $post => $value){
            if($value !== ""){
                $page[$post] = $value;
            }
        }
        $data = $page;
        $data['page'] = getEmptyValue("tbl_pictures_categories");
        return $Role->view("medias/media", $data);
    }
    //If changing order/status tess medias
    if (isset($_POST['update_medias'])){
        global $lang, $l;
        $medias = new Media();
        $online = $medias->update_order($_POST);
        $return[0] = $online;
        $return[1] = "success";
        echo json_encode($return);
	}

    //If delete medias posted
    if(isset($_POST['delete_medias'])){
        $delete_medias = new Media();
        $page_editing = $delete_medias->Deletemedias($_POST['medias_id']); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);
    }


    //Load edit tess medias
    if(isset($_POST['show_medias'])){
        global $lang, $l;
       $medias = new Media();
       $page = $medias->getmedias($_POST['medias_id']);

       return $medias->view("medias/media", $page);
    }


    if(isset($_POST['show_childs_medias'])){
        global $lang, $l;
       $media = new Media();
       $medias = new Medias();
       $page['page'] = getEmptyValue("tbl_pictures_categories");
       $page['page']->id =  $_POST['medias_id'];
       $page['load_add_medias'] = true;
       return $medias->view("medias/media", $page);

    }

    /* -- ORDER IMAGES BY CATEG -- */
    if(isset($_POST['imageIds'])){
        $order_medias = new Media();
        $page_editing = $order_medias->update_orders_imgcateg($_POST); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);

    }

    if(isset($_POST['edit_image_form'])){
        $order_medias = new Media();
        $page_editing = $order_medias->update_current_image($_POST); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);

    }

    if(isset($_POST['multiple_file_upload'])){
        $order_medias = new Media();
        $page_editing = $order_medias->insert_images($_POST, $_FILES); //If second parameters true then adding new page;
        $return[0] = $page_editing['message'];
        $return[1] = $page_editing['color'];
        echo json_encode($return);

    }

    if(isset($_POST['edit_single_image'])){
        $order_medias = new Media();
        $page_editing = $order_medias->form_edit_image($_POST['id']);
        return $page_editing;
    }

    /* -- infolettre templates -- */

        if(isset($_POST['load_add_newsletters_templates'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page=array();
            foreach($_POST as $post => $value){
                if($value !== ""){
                    $page[$post] = $value;
                }
            }
            $data = $page;
            $data['page'] = getEmptyValue("newsletter_templates");
            return $Newsletter->view("newsletter/template", $data);
        }

        if(isset($_POST['load_edit_newsletters_templates'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page=array();
            foreach($_POST as $post => $value){
                if($value !== ""){
                    $page[$post] = $value;
                }
            }
            $data = $page;
            $data = $Newsletter->fetchAllTemplates($_POST['id_newsletters_templates']);
            return $Newsletter->view("newsletter/template", $data);
        }

        if(isset($_POST['add_newsletters_templates'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page_editing = $Newsletter->AddEditNewsletterTemplates($_POST, true); //No second parameters then just editing page
            $return[0] = $page_editing['message'];
            $return[1] = $page_editing['color'];
            echo json_encode($return);
        }

        if(isset($_POST['edit_newsletters_templates'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page_editing = $Newsletter->AddEditNewsletterTemplates($_POST); //No second parameters then just editing page
            $return[0] = $page_editing['message'];
            $return[1] = $page_editing['color'];
            echo json_encode($return);
        }

        if(isset($_POST['delete_newsletters_templates'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page_editing = $Newsletter->deleteNewsletterTemplates($_POST['newsletters_templates_id']); //No second parameters then just editing page
            $return[0] = $page_editing['message'];
            $return[1] = $page_editing['color'];
            echo json_encode($return);
        }
    /* -- infolettre groups -- */

        if(isset($_POST['load_add_newsletters_groups'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page=array();
            foreach($_POST as $post => $value){
                if($value !== ""){
                    $page[$post] = $value;
                }
            }
            $data = $page;
            $data['page'] = getEmptyValue("newsletter_groups");
            return $Newsletter->view("newsletter/group", $data);
        }

        if(isset($_POST['load_edit_newsletters_groups'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page=array();
            foreach($_POST as $post => $value){
                if($value !== ""){
                    $page[$post] = $value;
                }
            }
            $data = $page;
            $data = $Newsletter->fetchAllGroups($_POST['id_newsletters_groups']);
            return $Newsletter->view("newsletter/group", $data);
        }

        if(isset($_POST['add_newsletters_groups'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page_editing = $Newsletter->AddEditNewsletterGroups($_POST, true); //No second parameters then just editing page
            $return[0] = $page_editing['message'];
            $return[1] = $page_editing['color'];
            echo json_encode($return);
        }

        if(isset($_POST['edit_newsletters_groups'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page_editing = $Newsletter->AddEditNewsletterGroups($_POST); //No second parameters then just editing page
            $return[0] = $page_editing['message'];
            $return[1] = $page_editing['color'];
            echo json_encode($return);
        }

        if(isset($_POST['delete_newsletters_groups'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page_editing = $Newsletter->deleteNewsletterGroups($_POST['newsletters_groups_id']); //No second parameters then just editing page
            $return[0] = $page_editing['message'];
            $return[1] = $page_editing['color'];
            echo json_encode($return);
        }
    /* -- infolettre members -- */

        if(isset($_POST['load_add_newsletters_members'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page=array();
            foreach($_POST as $post => $value){
                if($value !== ""){
                    $page[$post] = $value;
                }
            }
            $data = $page;
            $data['page'] = getEmptyValue("newsletter_members");
            return $Newsletter->view("newsletter/member", $data);
        }

        if(isset($_POST['load_edit_newsletters_members'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page=array();
            foreach($_POST as $post => $value){
                if($value !== ""){
                    $page[$post] = $value;
                }
            }
            $data = $page;
            $data = $Newsletter->fetchAllMembers($_POST['id_newsletters_members']);
            return $Newsletter->view("newsletter/member", $data);
        }

        if(isset($_POST['add_newsletters_members'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page_editing = $Newsletter->AddEditNewsletterMembers($_POST, true); //No second parameters then just editing page
            $return[0] = $page_editing['message'];
            $return[1] = $page_editing['color'];
            echo json_encode($return);
        }

        if(isset($_POST['edit_newsletters_members'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page_editing = $Newsletter->AddEditNewsletterMembers($_POST); //No second parameters then just editing page
            $return[0] = $page_editing['message'];
            $return[1] = $page_editing['color'];
            echo json_encode($return);
        }

        if(isset($_POST['delete_newsletters_members'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page_editing = $Newsletter->deleteNewsletterMembers($_POST['newsletters_members_id']); //No second parameters then just editing page
            $return[0] = $page_editing['message'];
            $return[1] = $page_editing['color'];
            echo json_encode($return);
        }
    /* -- infolettre campains -- */

        if(isset($_POST['load_add_newsletters_campains'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page=array();
            foreach($_POST as $post => $value){
                if($value !== ""){
                    $page[$post] = $value;
                }
            }
            $data = $page;
            $data['page'] = getEmptyValue("newsletter_campains");
            return $Newsletter->view("newsletter/campain", $data);
        }

        if(isset($_POST['load_edit_newsletters_campains'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page=array();
            foreach($_POST as $post => $value){
                if($value !== ""){
                    $page[$post] = $value;
                }
            }
            $data = $page;
            $data = $Newsletter->fetchAllCampains($_POST['id_newsletters_campains']);
            return $Newsletter->view("newsletter/campain", $data);
        }

        if(isset($_POST['add_newsletters_campains'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page_editing = $Newsletter->AddEditNewsletterCampains($_POST, true); //No second parameters then just editing page
            $return[0] = $page_editing['message'];
            $return[1] = $page_editing['color'];
            echo json_encode($return);
        }

        if(isset($_POST['edit_newsletters_campains'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page_editing = $Newsletter->AddEditNewsletterCampains($_POST); //No second parameters then just editing page
            $return[0] = $page_editing['message'];
            $return[1] = $page_editing['color'];
            echo json_encode($return);
        }

        if(isset($_POST['delete_newsletters_campains'])){
            global $lang, $l;
            $Newsletter = new Newsletter();
            $page_editing = $Newsletter->deleteNewsletterCampains($_POST['newsletters_campains_id']); //No second parameters then just editing page
            $return[0] = $page_editing['message'];
            $return[1] = $page_editing['color'];
            echo json_encode($return);
        }

        if(isset($_POST['load_template_campains'])){

            global $lang, $l;
            $Newsletter = new Newsletter();
            $data = $Newsletter->getTemplateView($_POST['newsletters_temp_id']);
            echo $data;
        }

        if(isset($_POST['send_newsletters'])){

            sleep(1);
            global $lang, $l, $_SESSION;
            $Newsletter = new Newsletter();
            if(crypt_my_password($_POST['password']) !== $_SESSION['pass_word']){
                $return[0] = "haha c'est pas envoyer bich";
                $return[1] = "danger";
                $return[2] = "Metter lbon calis de mot de passe";
            }else{
                $return[0] = "C'est un succès";
                $return[1] = "success";
                $str = "";
                //On va fetch toutes lles courriels communs de  chacun des groupes de la campagne choisi.
                $result = $Newsletter->fetchCommonEmails($_POST['choose_campain']);
                //fetch le message a envoyer de la newsletters
                $message_array = $Newsletter->fetchAllCampains($_POST['choose_campain']);
                foreach($result as $recipient){
                    $to = $recipient->email;
                    $message = str_replace("__FIRST_NAME__", $recipient->email, $message_array->message)."
                    <br>";
                    $subject = $message_array->subject;
                    $from = $message_array->from_mail;
                    DragonMediaMail($subject, $message, $from, "Newsletter Dragon", $to, $to);
                }
                $return[2] = "Les emails ont été envoyés avec succès";
            }

            echo json_encode($return);
        }

        //Load edit tess medias
        if(isset($_POST['show_newsletter'])){
            global $lang, $l, $_GET;
           $medias = new Media();
           switch($_POST['page_id']){
               case "1":
                    $page = "campains.php";
                    $_GET['newsletter_url'] = 1;

               break;
               case "2":
                    $page = "members.php";
                    $_GET['newsletter_url'] = 2;

               break;
               case "3":
                    $page = "groups.php";
                    $_GET['newsletter_url'] = 3;

               break;
               case "4":
                    $page = "templates.php";
                    $_GET['newsletter_url'] = 4;

               break;
               case "5":
                    $page = "sender.php";
                    $_GET['newsletter_url'] = 5;

               break;

               default:
                    $page = "campains.php";
                    $_GET['newsletter_url'] = 1;
               break;
           }

           include(APPROOT."/views/newsletter/".$page);
        }


    //If search
    if(isset($_POST['search_everywhere'])){
		global $_SESSION;
        $Tess_db = new Database();
        $Pages = new Pages();
        $datapage = new Page();
        require_once ("../../app/administration/config/lang.php");

        echo $lang['results'].':';

        /* ----------------------------------------------------- pages ------------------------------------------------------------- */
        echo'
        <p class="text-left">'.$lang['menu_pages'].': </br>
         ';
		 
        //get data
        $infopage = $datapage->seekInPageWebsite($_POST['id']);

        //result in search is directly the id
        if(isset($infopage['from_id'])){
            echo '<a href="#" mid="'.$_POST['id'].'" class="load_pages red">(id) '.$lang['title'].': '.$infopage['from_id'][0]->title.'</a></br>';

        }

        //result in search is from text
        if(isset($infopage['from_text'])){
            foreach( $infopage['from_text'] as $page){
                echo '<a href="#" mid="'.$page->id.'" class="load_pages">(text)  '.$lang['title'].': '.$page->title.'</a></br>';
            }
        }
        echo '</p>';
		if($_SESSION['status'] < "2"){
			/* ----------------------------------------------------- modules  ------------------------------------------------------------- */
			echo'
			<p class="text-left">'.$lang['menu_modules'].': </br>
			 ';
			//get data
			$infopage = $datapage->seekInModuleWebsite($_POST['id']);

			//result in search is directly the id
			if(isset($infopage['from_id'])){
				echo '<a href="#" mid="'.$_POST['id'].'" class="load_modules red">(id) '.$lang['title'].': '.$infopage['from_id'][0]->title.'</a></br>';

			}

			//result in search is from text
			if(isset($infopage['from_text'])){
				foreach( $infopage['from_text'] as $page){
					echo '<a href="#" mid="'.$page->id.'" class="load_modules">(text)  '.$lang['title'].': '.$page->title.'</a></br>';
				}
			}
			echo '</p>';
			/* ----------------------------------------------------- templates  ------------------------------------------------------------- */
			echo'
			<p class="text-left">'.$lang['menu_templates'].': </br>
			 ';
			//get data
			$infopage = $datapage->seekInTemplateWebsite($_POST['id']);

			//result in search is directly the id
			if(isset($infopage['from_id'])){
				echo '<a href="#" mid="'.$_POST['id'].'" class="load_templates red">(id) '.$lang['title'].': '.$infopage['from_id'][0]->name.'</a></br>';

			}

			//result in search is from text
			if(isset($infopage['from_text'])){
				foreach( $infopage['from_text'] as $page){
					echo '<a href="#" mid="'.$page->id.'" class="load_templates">(text)  '.$lang['title'].': '.$page->name.'</a></br>';
				}
			}
			echo '</p>';
			/* ----------------------------------------------------- Inputs  ------------------------------------------------------------- */
			echo'
			<p class="text-left">'.$lang['menu_inputs'].': </br>
			 ';
			//get data
			$infopage = $datapage->seekInInputsWebsite($_POST['id']);

			//result in search is directly the id
			if(isset($infopage['from_id'])){
				echo '<a href="#" mid="'.$_POST['id'].'" class="load_inputs red">(id) '.$lang['title'].': '.$infopage['from_id'][0]->title.'</a></br>';

			}

			//result in search is from text
			if(isset($infopage['from_text'])){
				foreach( $infopage['from_text'] as $page){
					echo '<a href="#" mid="'.$page->id.'" class="load_inputs">(text)  '.$lang['title'].': '.$page->title.'</a></br>';
				}
			}
			echo '</p>';
		}
    }
