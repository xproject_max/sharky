<?php
	
    require_once "../../app/administration/initializing.php";
    require_once  "../../app/administration/config/lang.php";
	
 	$Tess_db = new Database("tess_db");
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors','on');
	
	$i=0;
	$total=0;
	$balance =0;
	
	if(isset($_POST['data'])){
		
 
		$getMethodPayment = getMethodPayments();
		$listPayments = "";
		 
		$total = 0;
		$query_orders = $Tess_db->query('
			SELECT * FROM transactions_detail WHERE transactions_id = :id'
		); 
		$Tess_db->bind($query_orders, ":id", $_POST['data']);
		if($Tess_db->rowCount($query_orders) > 0){

			while($or = $Tess_db->fetch($query_orders, "array")){
				$payment_method = $or['payment_method'];
				$date = $or['payment_date'];
				global $payment_method; 
			}  
			foreach($getMethodPayment as $key => $value){
				if($key == $payment_method){
					$selected = 'selected="selected"';
					
				}else{
					$selected ='';
				}
				$listPayments .= '	<option '.$selected.' value="'.$key.'">'.$value[''].'</option>';
			}
			
	
				echo '
<div class="centent_payment_history">
	<div class="content_top_payment">
		<h1>Modification de la transaction #'.$_POST['data'].'</h1>
	</div>
	<div class="content_mid_payment">
				';
				echo '
		
		<table id="table_p_h">
			<tr>
				<td colspan="2">
					<label for="payment_method">Choix du moyen de paiement</label>
					<select class="payment_method" name="payment_method">
						<option value="credit">Crédit</option>
						'.$listPayments.'
					</select>
				</td>
				<td colspan="1"> 
					<label for="date_payment">Date de paiement</label>
					 <input type="text" id="datepicker_drop_point" class="date_payment" autocomplete="off" name="date_payment"  value="'.date('d-m-y',$date).'" />
					 
				</td>
			</tr>
			<tr>
				<th>No. commande</th>
				<th>Montant de transaction</th>
				<th>Note de transaction</th>
			</tr>
				';

			mysqli_data_seek($Tess_db->dbh,$query_orders, 0);
			while($or = $Tess_db->fetch($query_orders, "array")){
				echo '
			<tr class="order can_delete_'.$i.'">
				<td>';
				if($or['order_id'] == "" || $or['order_id'] == null ||  $or['order_id'] == "NULL" ||  $or['order_id'] == 0){
					echo '<input type="hidden" disabled="disabled" class="popup_order_id is_order_'.$i.'"  value="NULL" />
					<input type="text" disabled="disabled" value="Solde au compte" />';
				}else{
					echo'<input disabled="disabled" type="text" class="popup_order_id is_order_'.$i.'" value="'.$or['order_id'].'" />';
				}
				echo'
				</td>
				<td hidden="hidden">
					<input disabled="disabled" type="text" class="popup_user_id is_user_'.$i.'" value="'.$or['user_id'].'" />
					<input disabled="disabled" type="text" class="popup_id is_user_'.$i.'" value="'.$or['id'].'" />
					<input disabled="disabled" type="text" class="popup_transactions_id is_user_'.$i.'" value="'.$or['transactions_id'].'" />
				</td> 
				<td>
					<input type="text" disabled="disabled" class="rows_counted popup_payment_due can_delete_val_'.$i.'" value="'.number_format($or['total_price'], 2, '.', '').'" />
				</td> 
				<td>
					<textarea maxlength="32" class="popup_user_notes" value="'.$or['notes'].'">'.$or['notes'].'</textarea>
				</td> 
			</tr>
				'; 
				$i++;
			}
		}
		 
		
		echo ' 
		</table>
		';
	}
	echo '
	</div>
	<div class="content_bottom_payment">
		<button class="btn_cancel_payment">Annuler</button>&nbsp;<button class="btn_edit_payment">Modifier</button>
	</div> 
</div>
	';

	unset($_POST);
?>
