<?php
	class Orders extends Controller {
		public $page = [];
		public $lang = "";
		public function __construct()
		{
			$this->pageinfo = $this->model("Page");
			$this->orders = $this->model("Order");
			$this->lang = $this->getLang();
			
			
			if($this->page->is_maintenance()){
				$page = $this->pageinfo->getPage(__MAINTENANCEID__);
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					// "Orders" => ["total"=>$total,"Three_months" =>$Three_Months_Total_Sold],
					// "Users" => ["total"=>$total_accounts,"Three_months" =>$Three_Months_Total_Accounts],
					"page" => $page
				];
				$this->view("pages/politiques", $data);
				exit();
			}
		}
		public function orders(){ 
			
			$page = $this->pageinfo->getPage(__ORDERSID__);
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"page" => $page
			];
			$this->view("orders/orders", $data);
		}
		
	}