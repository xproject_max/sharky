<?php
	
	
	class Companies extends Controller
	{
		public $page = [];
		public $user = [];
		public $lang = "";
		public $data_secure = [];
		
		public function __construct(){
			$this->page = $this->model("Page");
			$this->company = $this->model("Company");
			$this->lang = $this->getLang();
			
			if($this->page->is_maintenance()){
				$page = $this->page->getPage(__MAINTENANCEID__);
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					// "Orders" => ["total"=>$total,"Three_months" =>$Three_Months_Total_Sold],
					// "Users" => ["total"=>$total_accounts,"Three_months" =>$Three_Months_Total_Accounts],
					"page" => $page
				];
				$this->view("pages/conditionspolitiques", $data);
				exit();
			}
		}
		
		public function companies(){
			$page = $this->page->getPage(__PROMOSID__);
			//Init data
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"page" =>  $page,
			];
			
			//Send view
			$this->view("companies/companies", $data);
		}
		public function company(){
			
			$page = $this->page->getPage(__CARTID__);
			if($_SERVER['REQUEST_METHOD'] == "GET"){
				$company = $this->company->getcompany($_GET['companyid']);
			}
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"company" =>  $company,
				"page" =>  $page,
			];
			//Send view
			$this->view("companies/company", $data);
			
		}
	}