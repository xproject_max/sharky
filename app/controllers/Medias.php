<?php
	class Medias extends Controller {
		public $page = [];
		public $lang = "";
		public function __construct()
		{
			$this->page = $this->model("Page");
			$this->media = $this->model("Media");
			$this->lang = $this->getLang();
			
			
			if($this->page->is_maintenance()){
				$page = $this->page->getPage(__MAINTENANCEID__);
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					// "Orders" => ["total"=>$total,"Three_months" =>$Three_Months_Total_Sold],
					// "Users" => ["total"=>$total_accounts,"Three_months" =>$Three_Months_Total_Accounts],
					"page" => $page
				];
				$this->view("pages/politiques", $data);
				exit();
			}
		}
		public function medias(){
			
			$page = $this->page->getPage(__SITEMAPID__);
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"page" => $page
			];
			$this->view("medias/medias", $data);
		}
		public function media(){
			
			$page = $this->page->getPage(__CHECKOUTID__);
			$media = [];
			if($_SERVER['REQUEST_METHOD'] == "GET"){
				$media = $this->media->getmedias($_GET['mediaid']);
			}
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"media" =>  $media,
				"page" => $page
			];
			$this->view("medias/media", $data);
		}
		
	}