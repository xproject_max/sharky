<?php
	
	
	class Users extends Controller
	{
		public $page = [];
		public $user = [];
		public $lang = "";
		public $data_secure = [];
		
		
		public function __construct(){
			$this->page = $this->model("Page");
			$this->user = $this->model("User");
			$this->lang = $this->lang;
			
			if($this->page->is_maintenance()){
				$page = $this->page->getPage(__MAINTENANCEID__);
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					// "Orders" => ["total"=>$total,"Three_months" =>$Three_Months_Total_Sold],
					// "Users" => ["total"=>$total_accounts,"Three_months" =>$Three_Months_Total_Accounts],
					"page" => $page
				];
				$this->view("pages/politiques", $data);
				exit();
			}
		}
		
		public function users(){
			$page = $this->page->getPage(__CONTESTSID__);
			//Init data
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"page" =>  $page,
			];
			
			//Send view
			$this->view("users/users", $data);
		}
		
		public function clientarea(){  
		redirect("users/login"); 
			 
			
		}
		public function login(){
			die('log');
			$page = $this->page->getPage(__LOGINID__);
			if($_SERVER['REQUEST_METHOD'] == "GET"){
				$user = $this->user->getUserWebsite($_GET['userid']);
			}
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"user" =>  $user,
				"page" =>  $page,
			];
			//Send view
			$this->view("users/login", $data);
			
		}
		
		public function recovery($id=""){
			$page = $this->page->getPage(__RECOVERYACCOUNTID__);
			$data = [
				"title" => "promotions",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("users/recovery", $data);
		}
		public function activation($id=""){
			$page = $this->page->getPage(__ACTIVATIONACCOUNTID__);
			$data = [
				"title" => "promotions",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("users/activation", $data);
		}
	}