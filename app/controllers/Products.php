<?php
	class Products extends Controller {
		public $page = [];
		public $lang = "";
		public function __construct()
		{
			$this->page = $this->model("Page");
			$this->product = $this->model("Product");
			$this->order = $this->model("Order");
			$this->user = $this->model("User");
			$this->lang = $this->getLang();
			
			
			if($this->page->is_maintenance()){
				$page = $this->page->getPagenormal(__MAINTENANCEID__);
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					// "Orders" => ["total"=>$total,"Three_months" =>$Three_Months_Total_Sold],
					// "Users" => ["total"=>$total_accounts,"Three_months" =>$Three_Months_Total_Accounts],
					"page" => $page
				];
				$this->view("pages/politiques", $data);
				exit();
			}
		}
		public function products($pageid=1){
			
			$page = $this->page->getPage($pageid);
			$menu_array = $this->product->getPage($pageid);
			$data = [
				"title" => "Pages",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("products/products", $data);
		}
		public function error($id=""){
			$page = $this->page->getPage(__404ID__);
			$data = [
				"title" => "404",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/404", $data);
		}
		public function collections($pageid=1){
			
			$page = $this->product->getPage($pageid);
			$menu_array = $this->product->getPage($pageid);
			// var_dump($menu_array);
			$data = [
				"title" => "Pages",
				"lang" =>  $this->lang,
				"menu_array" => $menu_array,
				"page" =>  $page
			];
			$this->view("products/collections", $data);
		}
		public function collection($pageid=1){
			
			$page = $this->product->getPage($pageid);
			$menu_array = $this->product->getPage($pageid);
			// var_dump($menu_array);
			$data = [
				"title" => "Pages",
				"lang" =>  $this->lang,
				"menu_array" => $menu_array,
				"page" =>  $page
			];
			$this->view("products/collection", $data);
		}
		public function product($pageid=1){
			// var_dump($pageid);
			$page = $this->product->getPage($pageid);
			$menu_array = $this->product->getPage($pageid);
			// var_dump($menu_array);
			$data = [
				"title" => "Pages",
				"lang" =>  $this->lang,
				"menu_array" => $menu_array,
				"page" =>  $page
			];
			$this->view("products/product", $data);
		}
		
		
		//plage horaire
		public function schedule($id=""){
			
			//Check for POST
			if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['email'])){
				//Process form
				
				$page = $this->page->getPage(__SCHEDULEID__);
				//Sanitize datas
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
				//Validate datas and secure it
				$data_secure = $this->page->initalize_schedule($_POST);
				
				//Combine datas in array to send to the view
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					"page" =>  $page,
					"secure_post" => $data_secure
				];
				//Send view
				$this->view("products/schedule", $data);
				
			}else {
				
				$page = $this->page->getPage(__SCHEDULEID__);
				$data = [
					"title" => "order",
					"lang" =>  $this->lang,
					"page" =>  $page
				];
				$this->view("products/schedule", $data);
				
			}
		}
		
		//plage horaire
		public function payorders($id=""){
			
			//Check for POST
			if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['email'])){
				//Process form
				
				$page = $this->page->getPage(__PAYORDERSID__);
				//Sanitize datas
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
				//Validate datas and secure it
				$data_secure = $this->page->check_unpaid_orders($_POST);
				
				//Combine datas in array to send to the view
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					"page" =>  $page,
					"secure_post" => $data_secure
				];
				//Send view
				$this->view("products/payorders", $data);
				
			}else {
				
				$page = $this->page->getPage(__PAYORDERSID__);
				$data = [
					"title" => "order",
					"lang" =>  $this->lang,
					"page" =>  $page
				];
				$this->view("products/payorders", $data);
				
			}
		}
		
		public function cart($id=""){
			$page = $this->page->getPage(__CARTID__);
			$menu_array = $this->page->getPage(__CARTID__);
			$data = [
				"title" => "Pages",
				"lang" =>  $this->lang,
				"menu_array" => $menu_array,
				"page" =>  $page
			];
			$this->view("products/cart", $data);
		}
		public function thankyoupayment($id=""){
			$page = $this->page->getPage(__PAYMENTTHANKYOUID__);
			$data = [
				"title" => "thankyoupayment",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("products/thankyoupayment", $data);
		}
		public function failedpayment($id=""){
			$page = $this->page->getPage(__PAYMENTFAILID__);
			$data = [
				"title" => "failedpayment",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("products/failedpayment", $data);
		}
		public function checkout($id=""){
			$page = $this->page->getPage(__CHECKOUTID__);
			$data = [
				"title" => "checkout",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("products/checkout", $data);
		}
		
	}