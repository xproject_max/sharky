<?php
	class Dictionary extends Controller {
		public $page = [];
		public $lang = "";
		public function __construct()
		{
			$this->page = $this->model("Page");
			$this->dictionary = $this->model("Dictionary_model");
			$this->lang = $this->getLang();
			
			
			if($this->page->is_maintenance()){
				$page = $this->page->getPage(__MAINTENANCEID__);
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					// "Orders" => ["total"=>$total,"Three_months" =>$Three_Months_Total_Sold],
					// "Users" => ["total"=>$total_accounts,"Three_months" =>$Three_Months_Total_Accounts],
					"page" => $page
				];
				$this->view("pages/conditionspolitiques", $data);
				exit();
			}
		}
		public function terms(){
			
			$page = $this->page->getPage(__404ID__);
			$menu_array = $this->dictionary->getMenu();
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"menu_array" =>  $menu_array,
				"page" => $page
			];
			$this->view("dictionary/terms", $data);
		}
		public function categories(){
			
			$page = $this->page->getPage(15);
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"page" => $page
			];
			$this->view("dictionary/categories", $data);
		}
		public function category(){
			
			$page = $this->page->getPage(15);
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"page" => $page
			];
			$this->view("dictionary/category", $data);
		}
		public function term(){
			
			$page = $this->page->getPage(__404ID__);
			$menu_array = $this->dictionary->getMenu();
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"page" => $page
			];
			$this->view("dictionary/term", $data);
		}
		
	}