<?php
	class Newsletter extends Controller {
		public $page = [];
		public $lang = "";
		public function __construct()
		{
			$this->page = $this->model("Page");
			$this->media = $this->model("Media");
			$this->lang = $this->getLang();
			
			if($this->page->is_maintenance()){
				$page = $this->page->getPage(__MAINTENANCEID__);
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					// "Orders" => ["total"=>$total,"Three_months" =>$Three_Months_Total_Sold],
					// "Users" => ["total"=>$total_accounts,"Three_months" =>$Three_Months_Total_Accounts],
					"page" => $page
				];
				$this->view("pages/politiques", $data);
				exit();
			}
		}
		
		public function index(){
			
			$page = $this->page->getPage(__CGVSID__);
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"page" => $page
			];
			$this->view("newsletter/newsletter", $data);
		}
		public function newsl(){
			
			$page = $this->page->getPage(__CGVSID__);
			
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"page" => $page
			];
			$this->view("newsletter/newsletter", $data);
		}
		
	}