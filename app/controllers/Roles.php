<?php
	class Roles extends Controller {
		public $page = [];
		public $lang = "";
		public function __construct()
		{
			$this->page = $this->model("Page");
			$this->lang = $this->getLang();
			
			if($this->page->is_maintenance()){
				$page = $this->page->getPage(__MAINTENANCEID__);
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					// "Orders" => ["total"=>$total,"Three_months" =>$Three_Months_Total_Sold],
					// "Users" => ["total"=>$total_accounts,"Three_months" =>$Three_Months_Total_Accounts],
					"page" => $page
				];
				$this->view("pages/conditionspolitiques", $data);
				exit();
			}
		}
		public function roles(){
			
			$page = $this->page->getPage(__CONDITIONSID__);
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"page" => $page
			];
			$this->view("roles/roles", $data);
		}
		public function website_roles(){
			
			$page = $this->page->getPage(25);
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"page" => $page
			];
			$this->view("roles/website_roles", $data);
		}
		
	}