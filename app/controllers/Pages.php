<?php
	class Pages extends Controller {
		public $page = [];
		public $lang = "";
		public function __construct()
		{
			$this->page = $this->model("Page"); 
			$this->users = $this->model("User");
			$this->lang = $this->lang;
			if($this->page->is_maintenance()){
				$page = $this->page->getPage(__MAINTENANCEID__);
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					// "Orders" => ["total"=>$total,"Three_months" =>$Three_Months_Total_Sold],
					// "Users" => ["total"=>$total_accounts,"Three_months" =>$Three_Months_Total_Accounts],
					"page" => $page
				];
				$this->view("pages/politiques", $data);
				exit();
			}
		}
		public function index(){
			// $total = $this->orders->TotalPaid();
			// $Three_Months_Total_Sold = $this->orders->Three_Months_Total_Sold();
			// $total_accounts = $this->users->Total_accounts();
			// $Three_Months_Total_Accounts = $this->users->Three_Months_Total_Accounts();
			if($_SERVER['REQUEST_METHOD'] == "POST"){
				//Process form
				
				$page = $this->page->getPage(1);
				//Sanitize datas
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
				//Validate datas and secure it
				$data_secure = $this->page->validate_contact_form($_POST);
				
				//Combine datas in array to send to the view
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					"page" =>  $page,
					"secure_post" => $data_secure
				];
				//Send view
				$this->view("pages/index", $data);
				
			}else {
				$page = $this->page->getPage(1);
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					// "Orders" => ["total"=>$total,"Three_months" =>$Three_Months_Total_Sold],
					// "Users" => ["total"=>$total_accounts,"Three_months" =>$Three_Months_Total_Accounts],
					"page" => $page
				];
				$this->view("pages/index", $data);
			}
		}
		public function about($id=""){
			
			$page = $this->page->getPage( $id );
			$data = [
				"title" => "about",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/about", $data);
		} 
		public function cities($id=""){
			
			$page = $this->page->getPage( $id );
			$data = [
				"title" => "cities",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/cities", $data);
		} 
		public function city($id=""){
			
			$page = $this->page->getPage( $id );
			$data = [
				"title" => "city",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/city", $data);
		} 
		public function services($id=""){
			
			$page = $this->page->getPage( __SERVICESID__ );
			$data = [
				"title" => "services",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/services", $data);
		} 
		
		
		public function calculator($id=""){
			
			$page = $this->page->getPage(71);
			$data = [
				"title" => "calculator",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/calculator", $data);
		} 
		
		
		public function contact($id=""){
			
			//Check for POST
			if($_SERVER['REQUEST_METHOD'] == "POST"){
				//Process form
				
				$page = $this->page->getPage(__CONTACTID__);
				//Sanitize datas
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
				//Validate datas and secure it
				$data_secure = $this->page->validate_contact_form($_POST);
				
				//Combine datas in array to send to the view
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					"page" =>  $page,
					"secure_post" => $data_secure
				];
				//Send view
				$this->view("pages/contact", $data);
				
			}else {
				
				$page = $this->page->getPage(__CONTACTID__);
				$data = [
					"title" => "about",
					"lang" =>  $this->lang,
					"page" =>  $page
				];
				$this->view("pages/contact", $data);
			}
		}

		public function submission($id=""){
			
			//Check for POST
			if($_SERVER['REQUEST_METHOD'] == "POST"){
				//Process form
				
				$page = $this->page->getPage($id);
				//Sanitize datas
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
				//Validate datas and secure it
				$data_secure = $this->page->validate_contact_form($_POST);
				
				//Combine datas in array to send to the view
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					"page" =>  $page,
					"secure_post" => $data_secure
				];
				//Send view
				$this->view("pages/submission", $data);
				
			}else {
				
				$page = $this->page->getPage($id);
				$data = [
					"title" => "about",
					"lang" =>  $this->lang,
					"page" =>  $page
				];
				$this->view("pages/submission", $data);
			}
		} 
		 
		public function error($id=""){
			$page = $this->page->getPage(__404ID__);
			$data = [
				"title" => "404",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/404", $data);
		}
		public function sitemap($id=""){
			$page = $this->page->getPage(__SITEMAPID__);
			$data = [
				"title" => "promotions",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/sitemap", $data);
		} 
		public function politiques($id=""){
			$page = $this->page->getPage($id);
			$data = [
				"title" => "promotions",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/politiques", $data);
		}
		public function thankyou($id=""){
			$page = $this->page->getPage(__THANKYOUID__);
			$data = [
				"title" => "promotions",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/thankyou", $data);
		} 
		
		
		public function maintenance($id=""){
			$page = $this->page->getPage(__MAINTENANCEID__);
			$data = [
				"title" => "maintenance",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/maintenance", $data);
		} 
		
		public function realisations($id=""){
			$page = $this->page->getPage(__REALISATIONSID__);
			$data = [
				"title" => "realisations",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/realisations", $data);
		} 
		public function projects($id=""){
			$page = $this->page->getPage(64);
			$data = [
				"title" => "projects",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/projects", $data);
		} 
		public function project($id=""){
			$page = $this->page->getPage($id);
			$data = [
				"title" => "project",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/project", $data);
		} 
		public function aservice($id=""){
			
			$page = $this->page->getPage($id);
			$data = [
				"title" => "aservice",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/aservice", $data);
		} 
		
		
		public function subscription($id=""){
			$page = $this->page->getPage(__SUBSCRIPTIONSID__);
			$data = [
				"title" => "subscription",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/subscription", $data);
		} 
		
		public function faq($id=""){
			$page = $this->page->getPage(__FAQID__);
			$data = [
				"title" => "faq",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/faq", $data);
		}  
		
		public function cart($id=""){
			$page = $this->page->getPage(__CARTID__);
			$data = [
				"title" => "faq",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("pages/cart", $data);
		}  
		public function users(){
			$page = $this->page->getPage(__CONTESTSID__);
			//Init data
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"page" =>  $page,
			];
			
			//Send view
			$this->view("users/users", $data);
		}
		public function blogs(){
			$page = $this->page->getPage(__BLOGID__);
			//Init data
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"page" =>  $page,
			];
			
			//Send view
			$this->view("blogs/blogs", $data);
		}
		
		
		public function clientarea(){ 
			if (!isset($_SESSION['user_id_website'])) {
				redirect(getUrlLang(__LOGINAREAID__));
				exit();
			}
			$page = $this->page->getPage(__USERAREAID__); 
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang, 
				"page" =>  $page,
			];
			//Send view
			$this->view("users/clientarea", $data);
			
		}
		public function login(){
			if (isset($_SESSION['user_id_website'])) {
				redirect(getUrlLang(__USERAREAID__));
			}
			$page = $this->page->getPage(__LOGINAREAID__); 
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"user" =>  $user,
				"page" =>  $page,
			];
			//Send view
			$this->view("users/login", $data);
			
		}
		
		public function recovery($id=""){
			$page = $this->page->getPage(__RECOVERYACCOUNTID__);
			$data = [
				"title" => "promotions",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("users/recovery", $data);
		}
		public function activation($id=""){
			$page = $this->page->getPage(__ACTIVATIONACCOUNTID__);
			$data = [
				"title" => "promotions",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("users/activation", $data);
		}
		public function products($id=""){
			$page = $this->page->getPage(__PRODUCTSID__);
			$data = [
				"title" => "products",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("products/products", $data);
		}
		public function orders($id=""){
			
			$page = $this->page->getPage(__ORDERSID__);
			$data = [
				"title" => "orders",
				"lang" =>  $this->lang,
				"page" =>  $page
			];
			$this->view("orders/orders", $data);
		}
	}
