<?php
	class Promotions extends Controller {
		
		public $page = [];
		public $lang = "";
		
		public function __construct()
		{
			$this->page = $this->model("Page");
			$this->promotion = $this->model("Promotion");
			$this->lang = $this->getLang();
			
			if($this->page->is_maintenance()){
				$page = $this->page->getPage(__MAINTENANCEID__);
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					// "Orders" => ["total"=>$total,"Three_months" =>$Three_Months_Total_Sold],
					// "Users" => ["total"=>$total_accounts,"Three_months" =>$Three_Months_Total_Accounts],
					"page" => $page
				];
				$this->view("pages/politiques", $data);
				exit();
			}
		}
		public function promotions($pageid=1){
			
			$page = $this->page->getPage($pageid);
			$menu_array = $this->promotion->getPage($pageid);
			$data = [
				"title" => "Pages",
				"lang" =>  $this->lang,
				"menu_array" => $menu_array,
				"page" =>  $page
			];
			$this->view("promotions/promotions", $data);
		}
		public function promotion($pageid=1){
			
			$page = $this->page->getPage($pageid);
			$menu_array = $this->promotion->getPage($pageid);
			$data = [
				"title" => "Pages",
				"lang" =>  $this->lang,
				"menu_array" => $menu_array,
				"page" =>  $page
			];
			$this->view("promotions/promotion", $data);
		}
		public function dates_promotion($pageid=1){
			
			$page = $this->page->getPage($pageid);
			$menu_array = $this->promotion->getPage($pageid);
			$data = [
				"title" => "Pages",
				"lang" =>  $this->lang,
				"menu_array" => $menu_array,
				"page" =>  $page
			];
			$this->view("promotions/dates_promotion", $data);
		}
		
	}