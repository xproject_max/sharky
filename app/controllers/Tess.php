<?php
	
	
	class Tess extends Controller
	{
		public $page = [];
		public $user = [];
		public $lang = "";
		public $data_secure = [];
		
		public function __construct(){
			$this->page = $this->model("Page");
			$this->user = $this->model("Tess_model");
			$this->lang = $this->getLang();
			
			if($this->page->is_maintenance()){
				$page = $this->page->getPage(__MAINTENANCEID__);
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					// "Orders" => ["total"=>$total,"Three_months" =>$Three_Months_Total_Sold],
					// "Users" => ["total"=>$total_accounts,"Three_months" =>$Three_Months_Total_Accounts],
					"page" => $page
				];
				$this->view("pages/conditionspolitiques", $data);
				exit();
			}
		}
		
		public function login(){
			//Get data of current page
			$page = $this->page->getPage(3);
			//Check for POST
			if($_SERVER['REQUEST_METHOD'] == "POST"){
				//Process form
				
				//Sanitize datas
				$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
				
				//Validate datas and secure it
				$data_secure = $this->user->validate_users($_POST);
				
				//Combine datas in array to send to the view
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					"page" =>  $page,
					"secure_post" => $data_secure
				];
				//Send view
				$this->view("tess/login", $data);
				
			}else {
				
				//Init data
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					"page" =>  $page,
				];
				
				//Send view
				$this->view("tess/login", $data);
			}
		}
		
		public function users(){
			//Get data of current page
			$page = $this->page->getPage(13);
			$menu_array = $this->user->getMenu();
			
			//Init data
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"menu_array" =>  $menu_array,
				"page" =>  $page,
			];
			
			//Send view
			$this->view("tess/users", $data);
			
		}
		
		public function profile(){
			//Get data of current page
			$user = $this->user->getAll($_SESSION['user_id']);
			$page = $this->page->getPage(5);
			// $menu_array = $this->user->getMenu($_SESSION['user_id']);
			
			//Init data
			$data = [
				"title" => "Home",
				"lang" =>  $this->lang,
				"user" =>  $user,
				"page" =>  $page,
			];
			
			$this->view("tess/profile", $data);
		}
		public function logout(){
			unset($_SESSION['user_id']);
			unset($_SESSION['user_email']);
			unset($_SESSION['user_name']);
			session_destroy();
			redirect('',$this->lang);
			exit;
		}
	}
