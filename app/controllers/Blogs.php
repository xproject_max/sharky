<?php
	class Blogs extends Controller {
		
		public $page = [];
		public $lang = "";
		
		public function __construct()
		{
			$this->page = $this->model("Page");
			$this->blog = $this->model("Blog");
			$this->lang = $this->getLang();
			
			if($this->page->is_maintenance()){
				$page = $this->page->getPage(__MAINTENANCEID__);
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					// "Orders" => ["total"=>$total,"Three_months" =>$Three_Months_Total_Sold],
					// "Users" => ["total"=>$total_accounts,"Three_months" =>$Three_Months_Total_Accounts],
					"page" => $page
				];
				$this->view("pages/politiques", $data);
				exit();
			}
		}
		public function blogs($pageid=1){
			
			$page = $this->page->getPage(__BLOGID__); 
			$data = [
				"title" => "Pages",
				"lang" =>  $this->lang,
				"menu_array" => $menu_array,
				"page" =>  $page
			];
			$this->view("blogs/blogs", $data);
		}
		public function dates_publication($pageid=1){
			
			$page = $this->page->getPage($pageid);
			$menu_array = $this->page->getPage($pageid);
			$data = [
				"title" => "Pages",
				"lang" =>  $this->lang,
				"menu_array" => $menu_array,
				"page" =>  $page
			];
			$this->view("blogs/dates_publication", $data);
		}
		public function blog($pageid=1){
			
			$page = $this->page->getPage($pageid);
			$menu_array = $this->blog->getPage($pageid);
			$data = [
				"title" => "Pages",
				"lang" =>  $this->lang,
				"menu_array" => $menu_array,
				"page" =>  $page
			];
			$this->view("blogs/blog", $data);
		}
		
	}