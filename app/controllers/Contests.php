<?php
	class Contests extends Controller {
		
		public $page = [];
		public $lang = "";
		
		public function __construct()
		{
			$this->page = $this->model("Page");
			$this->contest = $this->model("Contest");
			$this->lang = $this->getLang();
			
			if($this->page->is_maintenance()){
				$page = $this->page->getPage(__MAINTENANCEID__);
				$data = [
					"title" => "Home",
					"lang" =>  $this->lang,
					// "Orders" => ["total"=>$total,"Three_months" =>$Three_Months_Total_Sold],
					// "Users" => ["total"=>$total_accounts,"Three_months" =>$Three_Months_Total_Accounts],
					"page" => $page
				];
				$this->view("pages/politiques", $data);
				exit();
			}
		}
		public function contests($pageid=1){
			
			$page = $this->page->getPage($pageid);
			$menu_array = $this->contest->getPage($pageid);
			$data = [
				"title" => "Pages",
				"lang" =>  $this->lang,
				"menu_array" => $menu_array,
				"page" =>  $page
			];
			$this->view("contests/contests", $data);
		}
		public function contest($pageid=1){
			
			$page = $this->page->getPage($pageid);
			$menu_array = $this->contest->getPage($pageid);
			$data = [
				"title" => "Pages",
				"lang" =>  $this->lang,
				"menu_array" => $menu_array,
				"page" =>  $page
			];
			$this->view("contests/contest", $data);
		}
		public function dates_contest($pageid=1){
			
			$page = $this->page->getPage($pageid);
			$menu_array = $this->contest->getPage($pageid);
			$data = [
				"title" => "Pages",
				"lang" =>  $this->lang,
				"menu_array" => $menu_array,
				"page" =>  $page
			];
			$this->view("contests/dates_contest", $data);
		}
		
	}