
    <?php
    	/* Configuration file */
		/*
			possibility:
			LOCAL, DEV, PROD
		*/
		$hosting = "LOCAL";
		$update = false;
		$urllocal = "http://sharky.local/"; 
		$urldev = "http://sharky.local/"; 
		$urlprod = "http://sharky.local/"; 
		
		if($hosting == "LOCAL"){
			/* DATABASE LOCAL */
			DEFINE ( "DB_HOST", "127.0.0.1");
			DEFINE ( "DB_USER", "root");
			DEFINE ( "DB_PASS", "");
			DEFINE ( "DB_NAME", "sharkydb");
			DEFINE ( "PROTOCOL", "http");
			DEFINE ( "DOMAINNAME", "sharky.local");

			/* APP ROOT */ 
			DEFINE ( "APPROOT" , dirname(dirname(__FILE__)));
			/* URL ROOT */
			DEFINE ( "URLROOT" , "http://sharky.local/");
			/* URL ROOT */
			DEFINE ( "URLIMG" , "http://sharky.local/uploads");
			/* URL ROOT */
			DEFINE ( "URLFILES" , "http://sharky.local/uploads/lesprojetssharky/files/");
			/* SITENAME */
			DEFINE ( "SITENAME" , "lesprojetssharky");
			/* SALT PASSWORD */
			DEFINE ( "SALT_PASS" , "asdflesprojetssharkytyuiop123ABC_some_static_salt_string");
			/* Multi language? */
			DEFINE ( "MULTI_LANG", true);
			/* ARRAY LANGUAGES */
			DEFINE ( "LANGUAGES" , [
				"fr", "en"
			]);

			DEFINE ( "__SEO_META__", "");
			DEFINE ( "__SEO_META___en", "");

			DEFINE( "__FONT1__" , "https://fonts.googleapis.com/css?family=Lexend:100,200,300,regular,500,600,700,800,900&display=fallback");
			DEFINE( "__FONT2__" , "https://fonts.googleapis.com/css?family=Open+Sans:300,regular,500,600,700,800,300italic,italic,500italic,600italic,700italic,800italic&display=fallback");


			/* ARRAY EXTENSIONS ALLOWED */
			DEFINE ( "APPEXT" , [
				"jpg", "jpeg", "png", "mp3", "mp4", "avi", "mov", "ico", "pdf"
			]);
		}else if ($hosting == "DEV"){

			/* DATABASE DEV */ 
			DEFINE ( "DB_HOST", "127.0.0.1");
			DEFINE ( "DB_USER", "root");
			DEFINE ( "DB_PASS", "");
			DEFINE ( "DB_NAME", "sharkydb");

				DEFINE ( "PROTOCOL", "http");
				DEFINE ( "DOMAINNAME", "sharky.local");
			/* APP ROOT */ 
			DEFINE ( "APPROOT" , dirname(dirname(__FILE__)));
			/* URL ROOT */
			DEFINE ( "URLROOT" , "http://sharky.local/");
			/* URL ROOT */
			DEFINE ( "URLIMG" , "http://sharky.local/uploads");
			/* URL ROOT */
			DEFINE ( "URLFILES" , "http://sharky.local/uploads/lesprojetssharky/files/");
			/* SITENAME */
			DEFINE ( "SITENAME" , "lesprojetssharky");
			/* SALT PASSWORD */
			DEFINE ( "SALT_PASS" , "asdflesprojetssharkytyuiop123ABC_some_static_salt_string");
			/* Multi language? */
			DEFINE ( "MULTI_LANG", true);
			/* ARRAY LANGUAGES */
			DEFINE ( "LANGUAGES" , [
				"fr", "en"
			]);

			DEFINE ( "__SEO_META__", "");
			DEFINE ( "__SEO_META___en", "");

			DEFINE( "__FONT1__" , "https://fonts.googleapis.com/css?family=Lexend:100,200,300,regular,500,600,700,800,900");
			DEFINE( "__FONT2__" , "https://fonts.googleapis.com/css?family=Open+Sans:300,regular,500,600,700,800,300italic,italic,500italic,600italic,700italic,800italic");


			/* ARRAY EXTENSIONS ALLOWED */
			DEFINE ( "APPEXT" , [
				"jpg", "jpeg", "png", "mp3", "mp4", "avi", "mov", "ico", "pdf"
			]);
		}else if ($hosting == "PROD"){

		}
		

    	$currency = "CAD";

    	DEFINE( "__PAYMENTFAILID__" , 10);
    	DEFINE( "__PAYMENTTHANKYOUID__" , 9);
    	DEFINE( "__THANKYOUID__" , 8);
    	DEFINE( "__HOMEID__" , 1);
    	DEFINE( "__ABOUTID__" , 2);
    	DEFINE( "__PRODUCTSID__" , 5);
    	DEFINE( "__SERVICESID__" , 4);
    	DEFINE( "__CONTACTID__" , 3);
    	DEFINE( "__SUBMISSIONID__" ,6);
    	DEFINE( "__404ID__" , 7);
    	DEFINE( "__LOGINAREAID__" , 11);
    	DEFINE( "__USERAREAID__" , 12);
    	DEFINE( "__CONTESTSID__" , 13);
    	DEFINE( "__PROMOSID__" , 14);
    	DEFINE( "__BLOGID__" , 15);
    	DEFINE( "__ACTIVATIONACCOUNTID__" , 16);
    	DEFINE( "__RECOVERYACCOUNTID__" , 17);
    	DEFINE( "__NOTFOUNDMAILID__" , 11);
    	DEFINE( "__CARTID__" , 18);
    	DEFINE( "__CHECKOUTID__" , 19);
    	DEFINE( "__PAYORDERSID__" , 20);
    	DEFINE( "__SCHEDULEID__" , 21);
    	DEFINE( "__SITEMAPID__" , 22);
    	DEFINE( "__CONDITIONSID__" , 23);
    	DEFINE( "__POLITIQUESID__" , 24);
    	DEFINE( "__CGVSID__" ,25);
		DEFINE( "__RETURNPOLICYID__" , 26);
		DEFINE( "__DELIVERYPOLICYID__" , 27);
    	DEFINE( "__REALISATIONSID__" , 28);
    	DEFINE( "__MAINTENANCEID__" , 29);
    	DEFINE( "__ORDERSID__" , 45);
    	DEFINE( "__SUBSCRIPTIONSID__" , 30);
    	DEFINE( "__FAQID__" , 31);
    	DEFINE( "__BFULLTEXTID__" , 32);
    	DEFINE( "__BFULLIMGID__" , 33);
    	DEFINE( "__BTEXTIMGID__" , 34);
    	DEFINE( "__BDOUBLETEXTID__" , 35);
    	DEFINE( "__BDOUBLEIMGID__" , 36);
    	DEFINE( "__BVIDEOID__" , 37);
    	DEFINE( "__BTEXTVIDEOID__" , 38);
    	DEFINE( "__APRODUCT__" , 39);
    	DEFINE( "__ANARTICLE__" , 40);
    	DEFINE( "__APROMO__" , 41);
    	DEFINE( "__ACONTEST__" , 42);
    	DEFINE( "__AREALISATION__" , 43);
    	DEFINE( "__ASUBSCRIPTION__" , 44);
    	DEFINE( "__CATEGCONTESTS__" , 46);
    	DEFINE( "__CATEGPROMOS__" , 47);
    	DEFINE( "__CATEGARTICLES__" , 48);
    	DEFINE( "__CATEGPRODUCTS__" , 49);
    	DEFINE( "__CAROUSELID__" , 50);
    	DEFINE( "__ASLIDEID__" , 51);
    	DEFINE( "__AQUESTIONID__" , 52);
    	DEFINE( "__CITIESID__" , 53);
    	DEFINE( "__CITYID__" , 54);

    	DEFINE( "CONTACT_EMAIL" , "lesprojetsshar");
    	DEFINE( "CLIENT_EMAIL" , "lesprojetsshar");
    	DEFINE( "CLIENT_PHONE" , "514-442-5641");
    	DEFINE( "__ADDRESS__" , "552 Rue Bisaillon 3");
    	DEFINE( "__CITY__" , "Saint-Jean-sur-Richelieu");
    	DEFINE( "__POSTAL_CODE__" , "J3B 1K6");

    	DEFINE( "APPIDFACEBOOK" , "");
    	DEFINE( "__GOOGLEAPIKEY__" , "");
    	DEFINE( "__GOOGLEAPICOORDS__" , "");

      //SendGrid
      DEFINE ( "SENDGRID_KEY" ,"zzzzzzz");
      putenv("SENDGRID_API_KEY=".SENDGRID_KEY);

      //Stripe
    	//sandbox
        // DEFINE("STRIPE_API_KEY", "zzzzzzz");
        // DEFINE("STRIPE_PUBLISHABLE_KEY", "zzzzzzz");
    	//live
        DEFINE("STRIPE_API_KEY", "zzzzzzz");
        DEFINE("STRIPE_PUBLISHABLE_KEY", "zzzzzzz");
        $currency = "CAD";

        //Paypal
        define("PPL_MODE", "sandbox");

    	if(PPL_MODE=="sandbox"){

        		define("PPL_API_USER", "zzzzzzz");
        		define("PPL_API_PASSWORD", "zzzzzzz");
        		define("PPL_API_SIGNATURE", "zzzzzzz");
            define("PPL_LOGO_IMG", "CHANGE LOGO URL");
            define("PPL_RETURN_URL", "http://sharky.local/public/paiement.php");
            define("PPL_CANCEL_URL", "http://sharky.local/fr/paiement/");
    	}
    	else{

          define("PPL_API_USER", "zzzzzzz");
          define("PPL_API_PASSWORD", "zzzzzzz");
          define("PPL_API_SIGNATURE", "zzzzzzz");
          define("PPL_LOGO_IMG", "CHANGE LOGO URL");
          define("PPL_RETURN_URL", "http://sharky.local/public/paiement.php");
          define("PPL_CANCEL_URL", "http://sharky.local/fr/paiement/");
    	}

      	define("PPL_LANG", "FR");
      	define("PPL_CURRENCY_CODE", "CAD");
		
		
		if($update){
			//on change le htaccess root
			$htaccess = "
			Options +FollowSymLinks

			RewriteEngine On

			# --- Changer le domaine. Oblige le site a utiliser les www. Evite les doublons. BILINGUE



			#Prevent directory listings

			Options All -Indexes



			RewriteCond %{REQUEST_URI} ^/+(administration)$ [NC]

			RewriteRule !^(public/administration) ".URLROOT."administration/ [L,NC]

			RewriteCond %{HTTP_HOST} ^(www\.)?".DOMAINNAME."$ [NC]

			RewriteRule !^(public) /public/%{REQUEST_URI} [L,NC]

			ErrorDocument 404 ".PROTOCOL."://".DOMAINNAME."/page-introuvable

			";
			file_put_contents(dirname(dirname(__DIR__))."/.htaccess", $htaccess);
			//on change .htaccess du public
			$htaccess_public = "
			# Activation du module de réécriture d'URL :
			RewriteEngine on


			<FilesMatch '\.(?i:gif|jpe?g|png|ico|css|js|swf)$'>

			  <IfModule mod_headers.c>
				Header set Cache-Control 'max-age=172800, public, must-revalidate'
			  </IfModule>

			</FilesMatch>
			# BEGIN Expire headers  
			<IfModule mod_expires.c>  
			  # Turn on the module.
			  ExpiresActive on
			  # Set the default expiry times.
			  #ExpiresDefault 'access plus 1 month'
			  ExpiresByType image/jpg 'access plus 1 month'
			 # ExpiresByType image/svg+xml 'access 1 month'
			  ExpiresByType image/gif 'access plus 1 month'
			  ExpiresByType image/jpeg 'access plus 1 month'
			  ExpiresByType image/png 'access plus 1 month'
			  ExpiresByType image/webp 'access plus 2 month'
			  ExpiresByType text/css 'access plus 1 month'
			  ExpiresByType text/javascript 'access plus 1 month'
			  ExpiresByType application/javascript 'access plus 1 month'
			  ExpiresByType application/x-shockwave-flash 'access plus 1 month'
			  ExpiresByType image/ico 'access plus 1 month'
			  ExpiresByType image/x-icon 'access plus 1 month'
			  #ExpiresByType text/html 'access plus 600 seconds'
			</IfModule>  
			# END Expire headers   

			#HSTS
			<IfModule mod_headers.c>
				# this domain should only be contacted in HTTPS for the next 6 months
				#Header add Strict-Transport-Security `max-age=15768000`
			</IfModule>

			# --- Changer le domaine. Oblige le site a utiliser les www. Evite les doublons NOT BILINGUE
			RewriteCond %{HTTP_HOST} !^".DOMAINNAME."$ [NC]
			RewriteRule ^(.*)$ ".PROTOCOL."://".DOMAINNAME."/$1 [L,R=301]
			".(MULTI_LANG ?
			"
			# --- Changer le domaine. Oblige le site a utiliser les www. Evite les doublons. BILINGUE
			RewriteCond %{HTTP_HOST} !^".DOMAINNAME."$ [NC]
			RewriteRule ^(fr|en|es)/(.*)/?$ ".PROTOCOL."://".DOMAINNAME."/$1/$2/ [L,R=301]
			" : "")."
			#Prevent directory listings
			Options All -Indexes

			RewriteCond %{REQUEST_URI} ^/+(css|js|scripts|uploads|img|media|images|phpmailer) [NC]
			RewriteRule .* - [S=4]
			RewriteRule ^ville/([^.]+)$ index.php?seo=$1&lang=fr [L]
			RewriteRule ^city/([^.]+)$ index.php?seo=$1&lang=en [L]
			RewriteRule ^(fr|en|es)/?$ index.php?lang=$1 [L,QSA]
		   ".(MULTI_LANG ?
			"RewriteRule ^(fr|en|es)/([^.]+?)/?$ index.php?lang=$1&url=$2 [L,QSA]" :"
			RewriteRule ^([^.]+?)/?$ index.php?url=$1 [L,QSA]")."


			#RewriteRule ^DEBUT_URL_SEO_(.*+)$ index.php?seo=$1 [L] # Page SEO

			#GZIP
			<IfModule mod_deflate.c>
				AddOutputFilterByType DEFLATE text/html text/plain text/xml text/css text/javascript application/javascript application/x-javascript
			</IfModule>

			RewriteCond %{REQUEST_FILENAME} !-f
			RewriteCond %{REQUEST_FILENAME} -d

			RewriteRule  ^(.*)+=([^*]+)x([^*]+)$  /public/thumbs.php?uri=$1&w=$2&h=$3&method=fitLargest
			RewriteRule  ^(.*)~-([^*]+)x([^*]+)$  /public/thumbs.php?uri=$1&w=$2&h=$3&method=resize
			RewriteRule  ^(.*)~l=(.*)$  ./thumbs.php?uri=$1&w=$2&h=$2&method=fit&ratio=l
			RewriteRule  ^(.*)~h=(.*)$  ./thumbs.php?uri=$1&w=$2&h=$2&method=fit&ratio=h
			RewriteRule  ^(.*)~(.*)x(.*)$  ./thumbs.php?uri=$1&w=$2&h=$3&method=fit
			RewriteRule  ^(.*)~(.*)$  ./thumbs.php?uri=$1&w=$2&h=$2&method=fit
			RewriteRule  ^(.*)=(.*)x(.*)$  ./thumbs.php?uri=$1&w=$2&h=$3&method=stretch
			".(MULTI_LANG ?
			"
			ErrorDocument 404 ".PROTOCOL."://".DOMAINNAME."/fr/page-introuvable":
			"ErrorDocument 404 ".PROTOCOL."://".DOMAINNAME."/page-introuvable
			")."
			";

			file_put_contents(dirname(dirname(__DIR__))."/public/.htaccess", $htaccess_public);

			$static_salt = "asdflesprojetssharkytyuiop123ABC_some_static_salt_string";
			//on change le htaccess de l'administration
			$htaccess_administration = "
			# Activation du module de réécriture d'URL :
			RewriteEngine on


			#HSTS
			<IfModule mod_headers.c>
			  # this domain should only be contacted in HTTPS for the next 6 months
			  #Header add Strict-Transport-Security `max-age=15768000`
			</IfModule>

			# --- Changer le domaine. Oblige le site a utiliser les www. Evite les doublons NOT BILINGUE

			# --- Changer le domaine. Oblige le site a utiliser les www. Evite les doublons. BILINGUE
			#RewriteCond %{HTTP_HOST} !^".DOMAINNAME."/administration$ [NC]
			#RewriteRule ^(fr|en|es)/(.*)/?$ ".PROTOCOL."://".DOMAINNAME."/administration/$1/$2/ [L,R=301]
			#Prevent directory listings
			Options All -Indexes

			RewriteCond %{REQUEST_URI} ^/+(css|js|scripts|uploads|img|media|images|phpmailer) [NC]
			RewriteRule .* - [S=4]
			RewriteRule ^ville/([^.]+)$ index.php?seo=$1&lang=fr [L]
			RewriteRule ^city/([^.]+)$ index.php?seo=$1&lang=en [L]
			RewriteRule ^(fr|en|es)/?$ index.php?lang=$1 [L,QSA]
			RewriteRule ^(fr|en|es)/([^.]+?)/?$ index.php?lang=$1&url=$2 [L,QSA]


			#RewriteRule ^DEBUT_URL_SEO_(.*+)$ index.php?seo=$1 [L] # Page SEO

			#GZIP
			<IfModule mod_deflate.c>
			  AddOutputFilterByType DEFLATE text/html text/plain text/xml text/css text/javascript application/javascript application/x-javascript
			</IfModule>

			RewriteCond %{REQUEST_FILENAME} !-f
			RewriteCond %{REQUEST_FILENAME} -d

			RewriteRule  ^(.*)+=([^*]+)x([^*]+)$  /public/thumbs.php?uri=$1&w=$2&h=$3&method=fitLargest
			RewriteRule  ^(.*)~-([^*]+)x([^*]+)$  /public/thumbs.php?uri=$1&w=$2&h=$3&method=resize
			RewriteRule  ^(.*)~l=(.*)$  ./thumbs.php?uri=$1&w=$2&h=$2&method=fit&ratio=l
			RewriteRule  ^(.*)~h=(.*)$  ./thumbs.php?uri=$1&w=$2&h=$2&method=fit&ratio=h
			RewriteRule  ^(.*)~(.*)x(.*)$  ./thumbs.php?uri=$1&w=$2&h=$3&method=fit
			RewriteRule  ^(.*)~(.*)$  ./thumbs.php?uri=$1&w=$2&h=$2&method=fit
			RewriteRule  ^(.*)=(.*)x(.*)$  ./thumbs.php?uri=$1&w=$2&h=$3&method=stretch

			ErrorDocument 404 ".PROTOCOL."://".DOMAINNAME."/administration/fr/404
			";

			file_put_contents(dirname(dirname(__DIR__))."/public/administration/.htaccess", $htaccess_administration);

			// change le config php de app/administration

			$config = '
			<?php
				/* Configuration file */

				/* DATABASE LOCAL */
				DEFINE ( "DB_HOST", "'.DB_HOST.'");
				DEFINE ( "DB_USER", "'.DB_USER.'");
				DEFINE ( "DB_PASS", "'.DB_PASS.'");
				DEFINE ( "DB_NAME", "'.DB_NAME.'");
				DEFINE ( "PROTOCOL", "http");
				DEFINE ( "DOMAINNAME", "sharky.local");

				/* APP ROOT */
			  DEFINE ( "APPROOT" , dirname(dirname(__FILE__)));
			  /* URL ROOT */
				DEFINE ( "URLROOT" , "'.URLROOT.'administration/");
				/* URL ROOT */
				DEFINE ( "URLIMG" , "'.URLROOT.'uploads/lesprojetssharky/");
			  DEFINE ( "URLCLIENT" , "'.URLROOT.'");
				/* URL ROOT */
				DEFINE ( "URLFILES" , "'.URLROOT.'uploads/lesprojetssharky/files/");
				/* SITENAME */
				DEFINE ( "SITENAME" , "Dragon CMS - Administration");
				DEFINE ( "COMPANYNAME" , "'.SITENAME.'");
				/* SALT PASSWORD */
				DEFINE ( "SALT_PASS" , "asdf'.trim(SITENAME).'tyuiop123ABC_some_static_salt_string");
				/* Multi language? */
				DEFINE ( "MULTI_LANG", '.(MULTI_LANG ? "true" : "false").');
				/* ARRAY LANGUAGES */
				DEFINE ( "LANGUAGES" , [
					"fr", "en"
				]);
				/* ARRAY EXTENSIONS ALLOWED */
				DEFINE ( "APPEXT" , [
					"jpg", "jpeg", "png", "mp3", "mp4", "avi", "mov", "ico", "pdf"
				]);

				$currency = "CAD";
				DEFINE( "CONTACT_EMAIL" , "'.CONTACT_EMAIL.'");
			?>

			';

			file_put_contents(dirname(dirname(__DIR__))."/app/administration/config/config.php", $config);


			$imagephp = '<?php
				$str = "";
				$str .= rawurlencode($_GET["path"]);
				$str = str_replace("%2F", "/", $str);
				if($str != "") {
					if(strpos($str,"images/") !== false){
						$file = "'.URLROOT.'$str";
					}else{
						$file = "'.URLROOT.'uploads/'.SITENAME.'/$str";
					}

					readfile($file);
				}
			  ';
			file_put_contents(dirname(dirname(__DIR__))."/public/image.php", $imagephp);



				$path = dirname(dirname(__DIR__))."/public/thumbs.php";
			$content = file_get_contents($path);
			$thumbsphp = str_replace($urldev , URLROOT, $content);
			$thumbsphp = str_replace($urllocal , URLROOT, $content);
			$thumbsphp = str_replace($urlprod , URLROOT, $content);

			file_put_contents($path, $thumbsphp);

			$path = dirname(dirname(__DIR__))."/public/css/style.less";
			$content = file_get_contents($path);
			$styleless = str_replace($urldev , URLROOT, $content);
			$styleless = str_replace($urllocal , URLROOT, $content);
			$styleless = str_replace($urlprod , URLROOT, $content);

			file_put_contents($path, $styleless);

		}
      ?>

    