<?php
	
	/* -- GLOBALS -- */
	$_LANG = [];
	
	$_LANG['fr']['all_realisations'] = "Toutes les réalisations";
	$_LANG['en']['all_realisations'] = "All realisations";
	
	$_LANG['fr']['our_blogs'] = "Notre blogue";
	$_LANG['en']['our_blogs'] = "Our blog";
	
	$_LANG['fr']['service_categ_1'] = "Dégustation";
	$_LANG['en']['service_categ_1'] = "Dégustation";
	
	$_LANG['fr']['service_categ_2'] = "Importation";
	$_LANG['en']['service_categ_2'] = "Importation"; 
	
	$_LANG['fr']['service_seo_1'] = "Dégustation";
	$_LANG['en']['service_seo_1'] = "Dégustation";
	$_LANG['fr']['service_seo_2'] = "Importation";
	$_LANG['en']['service_seo_2'] = "Importation"; 
	
	$_LANG['fr']['our_blogs_text'] = "Restez à l'affut des dernières informations dans l'industrie du web. Des informations qui ne seront que bénéfiques pour vous et votre entreprise.";
	$_LANG['en']['our_blogs_text'] = "Stay up-to-date of the latest information in the web industry. Information that will benefit you and your business.";
	
	$_LANG['fr']['request_quote'] = "Soumission rapide!";
	$_LANG['en']['request_quote'] = "Get a quote fast !";
	
	$_LANG['fr']['see_services'] = "Voir tous les services";
	$_LANG['en']['see_services'] = "See all services";
	
	$_LANG['fr']['previous_project'] = "Projet précédent";
	$_LANG['en']['previous_project'] = "Previous project";

	$_LANG['fr']['next_project'] = "Projet suivant";
	$_LANG['en']['next_project'] = "Next project";
	
	$_LANG['fr']['more_informations'] = "Plus d'informations";
	$_LANG['en']['more_informations'] = "More informations";
	$_LANG['fr']['summary_submission'] = "Sommaire";
	$_LANG['en']['summary_submission'] = "Summary";
	
	$_LANG['fr']['thankyou_message'] = "Merci, nous vous contacterons sous peu !";
	$_LANG['en']['thankyou_message'] = "Thank you, we will contact you soon !";
	$_LANG['fr']['realisations'] = "Portfolio";
	$_LANG['en']['realisations'] = "Projects";
	$_LANG['fr']['submit'] = "Envoyer";
	$_LANG['en']['submit'] = "Submit";
	$_LANG['fr']['submission'] = "Soumission";
	$_LANG['en']['submission'] = "Request a quote";
	$_LANG['fr']['colors'] = "Couleurs";
	$_LANG['en']['colors'] = "Colors";
	
	$_LANG['fr']['filter_by_cat'] = "Filtrer les réalisation";
	$_LANG['en']['filter_by_cat'] = "Realisations filter";
	
	$_LANG['fr']['edit_password'] = "Modifier le mot de passe";
	$_LANG['en']['edit_password'] = "Edit password";
	
	$_LANG['fr']['visit_website'] = "Visitez le site";
	$_LANG['en']['visit_website'] = "Visit website";
	
	$_LANG['fr']['view_project'] = "Voir le projet";
	$_LANG['en']['view_project'] = "See project";
	
	$_LANG['fr']['summary_submission'] = "Sommaire";
	$_LANG['en']['summary_submission'] = "Summary";
	
	$_LANG['fr']['orders'] = "Commandes";
	$_LANG['en']['orders'] = "Orders";
	
	$_LANG['fr']['logout'] = "Déconnexion";
	$_LANG['en']['logout'] = "Logout";
	
	$_LANG['fr']['thankyou_message'] = "Merci, nous vous contacterons sous peu.";
	$_LANG['en']['thankyou_message'] = "Thank you, we will contact you soon.";
	$_LANG['fr']['describe_project'] = "Décrivez-nous plus en détail votre projet";
	$_LANG['en']['describe_project'] = "Describe your project";
	$_LANG['fr']['phone_number'] = "Numéro de téléphone";
	$_LANG['en']['phone_number'] = "Phone";
	$_LANG['fr']['email_address'] = "Adresse courriel";
	$_LANG['en']['email_address'] = "Email";
	
	$_LANG['fr']['next'] = "Suivant";
	$_LANG['en']['next'] = "Next";
	
	$_LANG['fr']['previous'] = "Précédent";
	$_LANG['en']['previous'] = "Back";
	
	$_LANG['fr']['request_quote_text'] = "Obtenez une soumission rapidement en nous envoyant ce court formulaire.";
	$_LANG['en']['request_quote_text'] = "Get a quote fast by doing this short form !";
	
	$_LANG['fr']['our_projects'] = "Quelques projets dont nous sommes fières!";
	$_LANG['en']['our_projects'] = "Some projects that we are proud !";
	
	$_LANG['fr']['projects_text'] = "Toute réalisation web est possible pour nous, contactez-nous et nous réaliseront vos projets !";
	$_LANG['en']['projects_text'] = "Any web realization is possible for us, contact us and we will realize your projects!";
	
	
	$_LANG['fr']['dragon_agency'] = "Dragon Média - Agence de conception web et marketing";
	$_LANG['en']['dragon_agency'] = "Dragon Media - Web development and marketing agency";
	
	
	$_LANG['fr']['media_text'] = "Soyez visible <br> sur les réseaux sociaux.";
	$_LANG['en']['media_text'] = "Be visible <br> on social networks.";
	
	$_LANG['fr']['dragon_value'] = "Nos valeurs";
	$_LANG['en']['dragon_value'] = "Our values";
	
	$_LANG['fr']['our_services'] = "Nos services";
	$_LANG['en']['our_services'] = "Our services";
	
	
	$_LANG['fr']['cms_title'] = "Un gestionnaire de contenu puissant";
	$_LANG['en']['cms_title'] = "Powerfull CMS";
	
	
	$_LANG['fr']['services_text'] = "Notre priorité, c'est vous et de vous aider à atteindre vos objectifs en ligne. Pour cela, nous offrons les services nécessaires pour vous donner une visibilité sur le web. De la conception jusqu'à la publicité.";
	$_LANG['en']['services_text'] = "Our priority is you and helping you achieve your online goals. For this, we offer the necessary services to give you visibility on the web. From design to advertising.";
	
	$_LANG['fr']['dragon_value_text'] = "Pour notre agence, il est très important de vous offrir le meilleur service ainsi qu'un site qui correspond à vos besoins dans les délais demandés.";
	$_LANG['en']['dragon_value_text'] = "For our agency, it is very important for us to offer you the best service as well as a site that meets your needs within the requested deadlines.";
	
	
	$_LANG['fr']['cms_text'] = "Nous mettons à votre disposition notre propre gestionnaire de contenus qui vous permettra entre autres de modifier dynamiquement et rapidement le contenu de votre page web.
Grâce à sa simplicité d'utilisation, l'accès rapide aux diverses fonctionnalités vous permettra d'économiser beaucoup de temps.
Développé selon les derniers standards de l'industrie, vous aurez des fondations solides pour votre site web.
	";
	$_LANG['en']['cms_text'] = "We provide you with our own content manager which will allow you, among other things, to dynamically and quickly modify the content of your web page.
Thanks to its ease of use, quick access to various features will save you a lot of time.
Developed according to the latest industry standards, you will have a solid foundation for your website.
	";
	
	
	$_LANG['fr']['logouturl'] = "fr/deconnexion";
	$_LANG['en']['logouturl'] = "en/logout";
	
	$_LANG['fr']['remove'] = "Retirer";
	$_LANG['en']['remove'] = "Remove";
	
	$_LANG['fr']['pay_order'] = "Payer une commande";
	$_LANG['en']['pay_order'] = "Pay an order";
	
	$_LANG['fr']['cart'] = "Mon panier";
	$_LANG['en']['cart'] = "My cart";
	
	$_LANG['fr']['pickup_order'] = "Ramasser une commande";
	$_LANG['en']['pickup_order'] = "Pickup an order";
	
	$_LANG['fr']['edited_success'] = 'Les modifications ont été effectuées avec succès';
	$_LANG['en']['edited_success'] = 'Edited successfully';
	
	$_LANG['fr']['edited_error'] = 'Il y a eu un problème, les modifications n\'ont pas été effectuées';
	$_LANG['en']['edited_error'] = 'Something went wrong, couldn\'t be edited';
	
	$_LANG['fr']['added_success'] = 'Ajouté avec succès';
	$_LANG['en']['added_success'] = 'Added successfully';
	
	$_LANG['fr']['added_error'] = 'Il y a eu un problème, insertion impossible';
	$_LANG['en']['added_error'] = 'Something went wrong, couldn\'t be added';
	
	$_LANG['fr']['deleted_success'] = 'Supprimé avec succès';
	$_LANG['en']['deleted_success'] = 'Deleted successfully';
	
	$_LANG['fr']['email_exists'] = '- Ce courriel existe déjà';
	$_LANG['en']['email_exists'] = '- This email already exist';
	
	$_LANG['fr']['username_exists'] = '- Ce pseudo existe déjà';
	$_LANG['en']['username_exists'] = '- This username already exist';
	
	$_LANG['fr']['password_is_same'] = '- Le mot de passe actuel est le même';
	$_LANG['en']['password_is_same'] = '- The current password is the same';
	
	$_LANG['fr']['passwords_not_match'] = '- Les mots de passes de correponds pas';
	$_LANG['en']['passwords_not_match'] = '- Passwords doesn\'t match';
	
	$_LANG['fr']['deleted_error'] = 'Il y a eu un problème, suppression impossible';
	$_LANG['en']['deleted_error'] = 'Something went wrong, couldn\'t be deleted';
	
	$_LANG['fr']['templates'] = 'Gabarit(s)';
	$_LANG['en']['templates'] = 'Templates';
	
	$_LANG['fr']['template'] = 'Gabarit';
	$_LANG['en']['template'] = 'Template';
	
	$_LANG['fr']['title'] = 'Titre';
	$_LANG['en']['title'] = 'Title';
	
	$_LANG['fr']['online'] = 'En ligne';
	$_LANG['en']['online'] = 'Online';
	
	$_LANG['fr']['text'] = 'Texte';
	$_LANG['en']['text'] = 'Text';
	
	$_LANG['fr']['order_rank'] = 'Ordre';
	$_LANG['en']['order_rank'] = 'Order';
	
	$_LANG['fr']['rights_add'] = 'Droits d\'ajouter';
	$_LANG['en']['rights_add'] = 'Rights to add';
	
	$_LANG['fr']['rights_edit'] = 'Droits de modifier';
	$_LANG['en']['rights_edit'] = 'Rights to edit';
	
	$_LANG['fr']['rights_del'] = 'Droits de supprimer';
	$_LANG['en']['rights_del'] = 'Rights to delete';
	
	$_LANG['fr']['child_modules'] = 'Modules enfants';
	$_LANG['en']['child_modules'] = 'Child modules';
	
	$_LANG['fr']['gallery'] = 'Galerie';
	$_LANG['en']['gallery'] = 'Gallery';
	
	$_LANG['fr']['is_product'] = 'C\'est un produit?';
	$_LANG['en']['is_product'] = 'Is product?';
	
	$_LANG['fr']['is_blog'] = 'C\'est un article?';
	$_LANG['en']['is_blog'] = 'Is blog?';
	
	$_LANG['fr']['is_contest'] = 'C\'est un concours?';
	$_LANG['en']['is_contest'] = 'Is contest?';
	
	$_LANG['fr']['is_promotion'] = 'C\'est une promotion?';
	$_LANG['en']['is_promotion'] = 'Is promotion?';
	
	$_LANG['fr']['edit'] = 'Modifier';
	$_LANG['en']['edit'] = 'Edit';
	
	$_LANG['fr']['add'] = 'Ajouter';
	$_LANG['en']['add'] = 'Add';
	
	$_LANG['fr']['inputs'] = 'Champs';
	$_LANG['en']['inputs'] = 'Inputs';
	
	$_LANG['fr']['input_name'] = 'Nom du champ';
	$_LANG['en']['input_name'] = 'Input\'s name';
	
	$_LANG['fr']['input_title'] = 'Titre du champ (Visible client)';
	$_LANG['en']['input_title'] = 'Input\'s title (What clients can see)';
	
	$_LANG['fr']['rights_roles'] = 'Droits et rôles';
	$_LANG['en']['rights_roles'] = 'Rights and roles';
	
	$_LANG['fr']['input_type'] = 'Type(s) de champ';
	$_LANG['en']['input_type'] = 'Input\'s type';
	
	$_LANG['fr']['add'] = 'Ajouter';
	$_LANG['en']['add'] = 'Add';
	
	$_LANG['fr']['add'] = 'Ajouter';
	$_LANG['en']['add'] = 'Add';
	
	/* -- MENU -- */
	
	$_LANG['fr']['menu_search'] = 'Rechercher..';
	$_LANG['en']['menu_search'] = 'Search..';
	
	$_LANG['fr']['results'] = 'Résultat';
	$_LANG['en']['results'] = 'Results';
	
	$_LANG['fr']['menu_home'] = 'Accueil';
	$_LANG['en']['menu_home'] = 'Home';
	
	$_LANG['fr']['menu_realisations'] = 'Réalisations';
	$_LANG['en']['menu_realisations'] = 'Realisations';
	
	$_LANG['fr']['faq'] = 'Foire aux questions';
	$_LANG['en']['faq'] = 'Frequency asked questions';
	
	$_LANG['fr']['cgv'] = 'Conditions de ventes';
	$_LANG['en']['cgv'] = 'Conditions of Sale';
	
	$_LANG['fr']['returnpolicy'] = 'Politique de retour';
	$_LANG['en']['returnpolicy'] = 'Return policy';
	
	$_LANG['fr']['deliverypolicy'] = 'Politique de livraison';
	$_LANG['en']['deliverypolicy'] = 'Delivery policy';
	
	$_LANG['fr']['citiesfooter'] = 'Villes';
	$_LANG['en']['citiesfooter'] = 'Cities served';
	
	$_LANG['fr']['menu_schedule'] = 'Réservations';
	$_LANG['en']['menu_schedule'] = 'Schedules';
	
	$_LANG['fr']['menu_about'] = 'À propos';
	$_LANG['en']['menu_about'] = 'about us';
	
	$_LANG['fr']['menu_pages'] = 'Système de pages';
	$_LANG['en']['menu_pages'] = 'Pages system';

	$_LANG['fr']['personalized'] = 'Personnaliser';
	$_LANG['en']['personalized'] = 'Personalize';

	$_LANG['fr']['myaccount'] = 'Mon espace';
	$_LANG['en']['myaccount'] = 'My space';
	
	$_LANG['fr']['menu_login'] = 'Connexion';
	$_LANG['en']['menu_login'] = 'Login';
	
	$_LANG['fr']['menu_logout'] = 'Déconnexion';
	$_LANG['en']['menu_logout'] = 'Logout';
	
	$_LANG['fr']['menu_contact'] = 'contactez-nous';
	$_LANG['en']['menu_contact'] = 'contact us';
	
	$_LANG['fr']['menu_profile'] = 'Profil';
	$_LANG['en']['menu_profile'] = 'My profile';
	
	$_LANG['fr']['welcome_back'] = 'Bienvenue ';
	$_LANG['en']['welcome_back'] = 'Welcome ';
	
	$_LANG['fr']['menu_dashboard'] = 'Tableau de bord ';
	$_LANG['en']['menu_dashboard'] = 'Dashboard ';
	
	$_LANG['fr']['menu_modules'] = 'Modules';
	$_LANG['en']['menu_modules'] = 'Modules ';
	
	$_LANG['fr']['menu_tool'] = 'Outils';
	$_LANG['en']['menu_tool'] = 'Tools ';
	
	$_LANG['fr']['menu_inputs'] = 'Champs';
	$_LANG['en']['menu_inputs'] = 'Inputs ';
	
	$_LANG['fr']['menu_templates'] = 'Gabarits';
	$_LANG['en']['menu_templates'] = 'Templates ';
	
	$_LANG['fr']['menu_subscriptions'] = 'Abonnement';
	$_LANG['en']['menu_subscriptions'] = 'Subscriptions ';
	
	$_LANG['fr']['menu_shop'] = 'Boutique';
	$_LANG['en']['menu_shop'] = 'Shop ';
	
	$_LANG['fr']['menu_projects'] = 'Portfolio';
	$_LANG['en']['menu_projects'] = 'Projects ';
	
	$_LANG['fr']['menu_newsletter'] = 'Infolettre';
	$_LANG['en']['menu_newsletter'] = 'Newsletter ';
	
	$_LANG['fr']['menu_orders'] = 'Commandes';
	$_LANG['en']['menu_orders'] = 'Orders ';
	
	$_LANG['fr']['menu_products'] = 'Produits';
	$_LANG['en']['menu_products'] = 'Products ';
	
	$_LANG['fr']['menu_blogs'] = 'Système d\'articles';
	$_LANG['en']['menu_blogs'] = 'Blog\'s system ';
	
	$_LANG['fr']['menu_contests'] = 'Système de concours';
	$_LANG['en']['menu_contests'] = 'Contest\'s system ';
	
	$_LANG['fr']['menu_promotions'] = 'Promotions';
	$_LANG['en']['menu_promotions'] = 'Promotions ';
	
	$_LANG['fr']['menu_accounts'] = 'Comptes';
	$_LANG['en']['menu_accounts'] = 'Accounts';
	
	$_LANG['fr']['menu_dictionary'] = 'Dictionnaire';
	$_LANG['en']['menu_dictionary'] = 'Dictionary';
	
	$_LANG['fr']['menu_terms'] = 'Termes';
	$_LANG['en']['menu_terms'] = 'Terms';
	
	$_LANG['fr']['menu_category'] = 'Catégories';
	$_LANG['en']['menu_category'] = 'Categories';
	
	$_LANG['fr']['menu_roles'] = 'Roles';
	$_LANG['en']['menu_roles'] = 'Roles';
	
	$_LANG['fr']['menu_roles_website'] = 'Site roles';
	$_LANG['en']['menu_roles_website'] = 'Roles website';
	
	$_LANG['fr']['website_accounts'] = 'Utilisateurs';
	$_LANG['en']['website_accounts'] = 'Users';
	
	$_LANG['fr']['company_accounts'] = 'Entreprises';
	$_LANG['en']['company_accounts'] = 'Companies';
	
	$_LANG['fr']['tess_accounts'] = 'Tess';
	$_LANG['en']['tess_accounts'] = 'Tess ';
	
	$_LANG['fr']['menu_tess_users'] = 'Utilisateurs Tess';
	$_LANG['en']['menu_tess_users'] = 'Tess users ';
	
	$_LANG['fr']['menu_media'] = 'Médias';
	$_LANG['en']['menu_media'] = 'Medias ';
	
	/* -- LOGIN FORM -- */
	
	$_LANG['fr']['username'] = 'Nom d\'utilisateur';
	$_LANG['en']['username'] = 'Username';
	
	$_LANG['fr']['password'] = 'Mot de passe';
	$_LANG['en']['password'] = 'Password';
	
	$_LANG['fr']['form_nom'] = 'Votre nom';
	$_LANG['en']['form_nom'] = 'Your name';
	
	$_LANG['fr']['form_courriel'] = 'Votre courriel';
	$_LANG['en']['form_courriel'] = 'Your email';
	
	$_LANG['fr']['form_telephone'] = '# de téléphone';
	$_LANG['en']['form_telephone'] = 'Phone number';
	
	$_LANG['fr']['form_message'] = 'Votre message';
	$_LANG['en']['form_message'] = 'Your message';
	
	$_LANG['fr']['form_envoyer'] = 'Envoyer';
	$_LANG['en']['form_envoyer'] = 'Send';
	
	$_LANG['fr']['error_username'] = '- Vous devez choisir un identifiant valid avec plus de 5 caractères minimum.<br/>';
	$_LANG['en']['error_username'] = '- You must enter valid username with more than 5 characters.<br/>';
	
	$_LANG['fr']['message_success'] = '- Connexion en cours.. Bienvenue ';
	$_LANG['en']['message_success'] = '- Initializing - Welcome back';
	
	$_LANG['fr']['error_username_empty'] = '- Vous devez entrer un identifiant.<br/>';
	$_LANG['en']['error_username_empty'] = '- You must enter a username.<br/>';
	
	$_LANG['fr']['error_password_empty'] = '- Vous devez entrer un mot de passe.<br/>';
	$_LANG['en']['error_password_empty'] = '- You must enter a password.<br/>';
	
	$_LANG['fr']['error_username_not_exist'] = '- Ce nom d\'utilisateur n\'existe pas.<br/>';
	$_LANG['en']['error_username_not_exist'] = '- This username doesn\'t exist.<br/>';
	
	$_LANG['fr']['error_user_not_exist'] = '- Le mot de passe ne correspond pas. <br/>';
	$_LANG['en']['error_user_not_exist'] = '- The password does not match. <br/>';
	
	$_LANG['fr']['error_attempts'] = '- Votre mot de passe n\'est pas le bon. Tentatives restantes: ';
	$_LANG['en']['error_attempts'] = '- Your password is not the correct one. Remaining attempts:';
	
	$_LANG['fr']['error_attempts_max'] = '- Vous n\'avez plus de tentatives </br>';
	$_LANG['en']['error_attempts_max'] = '- You have no more attempts </br>';
	
	$_LANG['fr']['error_username_not_exist'] = '- Ce nom d\'utilisateur n\'existe pas.<br/>';
	$_LANG['en']['error_username_not_exist'] = '- This username doesn\'t exist.<br/>';
	
	$_LANG['fr']['erreur_nom'] = '- Vous devez entrer votre nom<br/>';
	$_LANG['en']['erreur_nom'] = '- You must enter your name<br/>';
	
	$_LANG['fr']['erreur_courriel'] = '- Vous devez entrer une adresse courriel<br/>';
	$_LANG['en']['erreur_courriel'] = '- You must provide an email address<br/>';
	
	$_LANG['fr']['erreur_courriel2'] = '- Votre adresse courriel n\'est pas valide<br/>';
	$_LANG['en']['erreur_courriel2'] = '- The email address you provided is not valid<br/>';
	
	$_LANG['fr']['erreur_telephone'] = '- Vous devez entrer un numéro de téléphone<br/>';
	$_LANG['en']['erreur_telephone'] = '- You must provide a phone number<br/>';
	
	$_LANG['fr']['erreur_telephone2'] = '- Votre numéro de téléphone n\'est pas valide<br/>';
	$_LANG['en']['erreur_telephone2'] = '- Your phone number is not valid<br/>';
	
	$_LANG['fr']['erreur_message'] = '- Vous devez écrire un message';
	$_LANG['en']['erreur_message'] = '- You must enter a message';
	
	$_LANG['fr']['erreur_bot'] = '- Oups, êtes-vous un robot?!? <br/>';
	$_LANG['en']['erreur_bot'] = '- Oops, are you a robot?!? <br/>';
	
	$_LANG['fr']['erreur_unknown'] = 'Une erreur inconnue est survenue';
	$_LANG['en']['erreur_unknown'] = 'An unknown error has occured';
	
	$_LANG['fr']['confirm_envoi'] = 'Votre message a bien été envoyé';
	$_LANG['en']['confirm_envoi'] = 'You message has been sent successfully';
	
	
	$_LANG['fr']['error_empty'] = 'Vous avez laissé ce champs vide.';
	$_LANG['en']['error_empty'] = 'You have left this input empty';
	
	$_LANG['fr']['error_min_three_char'] = 'Ce champs doit comporter au minimum 3 caractères';
	$_LANG['en']['error_min_three_char'] = 'This field must contain at least 3 characters';
	
	
	
	/* -- Dashboard -- */
	
	$_LANG['fr']['capacity_storage_used'] = 'Capacité du storage utilisée';
	$_LANG['en']['capacity_storage_used'] = 'Website capacity used';
	
	$_LANG['fr']['website_visits'] = 'Nombre de visiteurs';
	$_LANG['en']['website_visits'] = 'Website Visits';
	
	$_LANG['fr']['revenue'] = 'Profits';
	$_LANG['en']['revenue'] = 'Revenue';
	
	$_LANG['fr']['Coming_soon'] = 'À venir';
	$_LANG['en']['Coming_soon'] = 'Coming soon';
	
	$_LANG['fr']['followers'] = 'Abonné(e)s';
	$_LANG['en']['followers'] = 'Followers';
	
	$_LANG['fr']['total_last_three_months'] = 'Ventes des trois derniers mois';
	$_LANG['en']['total_last_three_months'] = 'Last three months sales';
	
	$_LANG['fr']['accounts_last_three_months'] = 'Nombre des comptes créés depuis les trois derniers mois';
	$_LANG['en']['accounts_last_three_months'] = 'Last three months new website accounts';
	
	/* -- SYSTÈME DE PAGES -- */
	
	$_LANG['fr']['pages_th_order'] = 'Ordre';
	$_LANG['en']['pages_th_order'] = 'Order';
	
	$_LANG['fr']['pages_th_title'] = 'Titre de page';
	$_LANG['en']['pages_th_title'] = 'Title page';
	
	$_LANG['fr']['pages_th_online'] = 'En ligne?';
	$_LANG['en']['pages_th_online'] = 'Online?';
	
	$_LANG['fr']['pages_th_actions'] = 'Action(s)';
	$_LANG['en']['pages_th_actions'] = 'Action(s)';
	
	$_LANG['fr']['pages_th_templates'] = 'Gabarit';
	$_LANG['en']['pages_th_templates'] = 'Template';
	
	$_LANG['fr']['pages_th_save'] = 'Sauvegarder';
	$_LANG['en']['pages_th_save'] = 'Save';
	
	$_LANG['fr']['add'] = 'Ajouter';
	$_LANG['en']['add'] = 'Add';
	
	$_LANG['fr']['new_page'] = 'Nouvelle page';
	$_LANG['en']['new_page'] = 'New page';
	
	$_LANG['fr']['child_of'] = ' enfant de ';
	$_LANG['en']['child_of'] = ' child of ';
	
	$_LANG['fr']['choose_module'] = 'Choisissez le templates que vous voulez assignez à la nouvelle page';
	$_LANG['en']['choose_module'] = 'Choose the templates you want to assign to the page';
	
	/* -- Pages Modules -- */
	
	$_LANG['fr']['new_module'] = 'Nouveau module';
	$_LANG['en']['new_module'] = 'New module';
	
	/* -- Pages Orders -- */
	
	$_LANG['fr']['orders_number'] = '# de commande';
	$_LANG['en']['orders_number'] = '# order';
	
	$_LANG['fr']['product_name'] = 'Titre du produit';
	$_LANG['en']['product_name'] = 'Product number';
	
	$_LANG['fr']['quantity'] = 'Quantité';
	$_LANG['en']['quantity'] = 'Quantity';
	
	$_LANG['fr']['first_name'] = 'Prénom';
	$_LANG['en']['first_name'] = 'First name';
	
	$_LANG['fr']['last_name'] = 'Nom';
	$_LANG['en']['last_name'] = 'Last name';
	
	$_LANG['fr']['email'] = 'Courriel';
	$_LANG['en']['email'] = 'Email';
	
	$_LANG['fr']['price'] = 'Prix';
	$_LANG['en']['price'] = 'Price';
	
	$_LANG['fr']['status'] = 'État';
	$_LANG['en']['status'] = 'Status';
	
	$_LANG['fr']['date'] = 'Date';
	$_LANG['en']['date'] = 'Date';
	
	$_LANG['fr']['date_start'] = 'Date de début';
	$_LANG['en']['date_start'] = 'Starting date';
	
	$_LANG['fr']['date_end'] = 'Date de fin';
	$_LANG['en']['date_end'] = 'Ending date';
	
	$_LANG['fr']['date_created'] = 'Date de création';
	$_LANG['en']['date_created'] = 'Creation date';
	
	$_LANG['fr']['filter'] = 'Filtrer';
	$_LANG['en']['filter'] = 'Filter';
	
	$_LANG['fr']['filter_export'] = 'Rechercher ou exporter';
	$_LANG['en']['filter_export'] = 'Filter or export';
	
	/* -- PAGE PRODUCT -- */
	
	$_LANG['fr']['new_product'] = 'Nouveau produit';
	$_LANG['en']['new_product'] = 'New product';
	
	$_LANG['fr']['pages_th_img'] = 'Image';
	$_LANG['en']['pages_th_img'] = 'Image';
	
	$_LANG['fr']['pages_th_price'] = 'Prix';
	$_LANG['en']['pages_th_price'] = 'Price';
	
	$_LANG['fr']['pages_th_qty'] = 'Quantité';
	$_LANG['en']['pages_th_qty'] = 'Quantity';
	
	$_LANG['fr']['pages_th_company'] = 'Compagnie';
	$_LANG['en']['pages_th_company'] = 'Company';
	
	/* -- PAGE USER -- */
	
	$_LANG['fr']['account_informations'] = 'Informations du compte';
	$_LANG['en']['account_informations'] = 'Account information';
	
	$_LANG['fr']['personal_informations'] = 'Informations personnelles';
	$_LANG['en']['personal_informations'] = 'Personnal information';
	
	$_LANG['fr']['retype_password'] = 'saisir à nouveau le mot de passe';
	$_LANG['en']['retype_password'] = 'Retype password';
	
	$_LANG['fr']['address'] = 'Adresse';
	$_LANG['en']['address'] = 'Address';
	
	$_LANG['fr']['region'] = 'Région';
	$_LANG['en']['region'] = 'Region';
	
	$_LANG['fr']['state'] = 'Province';
	$_LANG['en']['state'] = 'State';
	
	$_LANG['fr']['city'] = 'Ville';
	$_LANG['en']['city'] = 'City';
	
	$_LANG['fr']['gender'] = 'Genre';
	$_LANG['en']['gender'] = 'Gender';
	
	$_LANG['fr']['birthdate'] = 'Date de naissance';
	$_LANG['en']['birthdate'] = 'Birthdate';
	
	$_LANG['fr']['roles'] = 'Rôles';
	$_LANG['en']['roles'] = 'Roles';
	
	$_LANG['fr']['online'] = 'En ligne?';
	$_LANG['en']['online'] = 'Online';
	
	$_LANG['fr']['permissions'] = 'Permissions';
	$_LANG['en']['permissions'] = 'Permissions';
	
	$_LANG['fr']['phone'] = 'Téléphone';
	$_LANG['en']['phone'] = 'Phone';
	
	$_LANG['fr']['country'] = 'Pays';
	$_LANG['en']['country'] = 'Country';
	
	$_LANG['fr']['zipcode'] = 'Code postal';
	$_LANG['en']['zipcode'] = 'Zipcode';
	
	$_LANG['fr']['next_step'] = 'Prochaine étape';
	$_LANG['en']['next_step'] = 'Next step';
	
	
	/* -- PAGE COMPANY -- */
	
	$_LANG['fr']['company_name'] = 'Nom de compagnie';
	$_LANG['en']['company_name'] = 'Company name';
	
	$_LANG['fr']['contact_name'] = 'Contact';
	$_LANG['en']['contact_name'] = 'Contact name';
	
	/* -- PAGE BLOG -- */
	
	$_LANG['fr']['new_blog'] = 'Nouvelle article';
	$_LANG['en']['new_blog'] = 'New blog';
	
	/* -- PAGE CONTEST -- */
	
	$_LANG['fr']['new_contest'] = 'Nouveaux concours';
	$_LANG['en']['new_contest'] = 'New contest';
	
	/* -- PAGE PROMOTION -- */
	
	$_LANG['fr']['new_promotion'] = 'Nouvelle promotion';
	$_LANG['en']['new_promotion'] = 'New promotion';
	
	
	/* -- PAGE 404 -- */
	
	$_LANG['fr']['oops'] = 'oups ! Il n\'y a personne ici';
	$_LANG['en']['oops'] = 'oops! no one is here';
	
	$_LANG['fr']['back_home'] = 'retour à l\'accueil';
	$_LANG['en']['back_home'] = 'back home';
	
	$_LANG['fr']['back'] = 'Retour';
	$_LANG['en']['back'] = 'Back';
	
	$l = "";
	if(!isset($language)){
		$language = new Lang();
		$language = $language->lang;
		if($language == "en"){
			$l = "_en";
			$linv = "fr";
			
		}else{
			$l = "";
			$linv = "en";
		}
		$lang = $_LANG[$language];
		
		$lang = dictionary_initialize($language);
		$lang = array_merge($lang, $_LANG[$language]);
	}else{$language = new Lang();
		$language = $language->lang;
		if($language == "en"){
			$l = "_en";
			$linv = "fr";
			
		}else{
			$l = "";
			$linv = "en";
		}
		
		$lang = $_LANG[$language];
	}
	$currentLang = $language;
?>
