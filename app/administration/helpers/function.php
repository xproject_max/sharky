 <?php
     // Simple page redirect
     use PHPMailer\PHPMailer\Exception;
     use PHPMailer\PHPMailer\PHPMailer;
  // Simple page redirect
   function redirect($page,$lang="")
   {
      $language = new Lang('en');
      $lang = ($lang ? $lang : $language->lang);
      header('location: ' . URLROOT . '/' . $lang . "/" . $page);
   }

   function has_rights_to_see($rights, $pageid){
	   $db = new Database();
		$getr= $db->query("SELECT * FROM tbl_pages_admin WHERE id = :id");
		$db->bind($getr, ":id", $pageid);
		$r = $db->single($getr)->rights;
		if($r >= $rights){
			return true;
		}
   }

   //cut text
   function Cut($string, $max_length){
      $string = strip_tags($string);

      if (strlen($string) > $max_length) {
         $string = substr($string, 0, $max_length);
         $pos = strrpos($string, " ");
         if ($pos === false) {
            return substr($string, 0, $max_length);
         }
         return substr($string, 0, $pos);
      } else {
         return $string;
      }
   }

   //transform object data to array
   function object_to_array($data)
   {
       if (is_array($data) || is_object($data))
       {
           $result = array();
           foreach ($data as $key => $value)
           {
               $result[$key] = object_to_array($value);
           }
           return $result;
       }
       return $data;
   }
   function DragonMediaMail($subject, $message, $de, $de_name, $to, $to_name, $reply_to = "", $file_array = false) {

      include_once dirname(dirname(dirname(__DIR__)))."/public/plugins/mailtest/vendor/autoload.php";
      $cemail = new PHPMailer(true);                              // Passing `true` enables exceptions
      try {
          //Server settings
          // $cemail->SMTPDebug = 2;                                 // Enable verbose debug output
          $cemail->isSMTP();                                      // Set mailer to use SMTP
          $cemail->Host = 'mail.dragon-media.ca';  // Specify main and backup SMTP servers
          $cemail->SMTPAuth = true;                               // Enable SMTP authentication
          $cemail->Username = 'info@dragon-media.ca';                 // SMTP username
          $cemail->Password = 'Ww]V70M~D6_E';                           // SMTP password
          $cemail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
          $cemail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            ));                          // Enable TLS encryption, `ssl` also accepted
          $cemail->Port = 587;                                    // TCP port to connect to

          //Recipients
          $cemail->setFrom($de, $de_name);
          $cemail->addAddress($to,  $to_name);     // Add a recipient

          //Attachments

          $cemail->CharSet = 'UTF-8';
          //Content
          $cemail->isHTML(true);                                  // Set email format to HTML
          $cemail->Subject = $subject;
          $cemail->Body    = "<html><body>".$message."</body></html>";
          $cemail->AltBody = $message;

          $cemail->send();
      } catch (Exception $e) {
          echo 'Message could not be sent. Mailer Error: ', $cemail->ErrorInfo;
      }
   }
   function DragonProtectv1($value) {
      if (strpos($value,'<') !== false ||
      strpos($value,'<script') !== false ||
      strpos($value,'</script') !== false ||
      strpos($value,'>') !== false ||
      strpos($value,'&#039;') !== false ||
      strpos($value,'&LT;') !== false ||
      strpos($value,'SCRIPT') !== false ||
      strpos($value,'&gt;') !== false ) {
         $value = str_replace('<script', '*', $value);
         $value = str_replace('SCRIPT', '*', $value);
         $value = str_replace('&LT;SCRIPT&GT;', '*', $value);
         $value = str_replace('</script', '*', $value);
         $value = str_replace('>', '*', $value);
         $value = str_replace('<', '*', $value);
         $value = str_replace('&LT;', '*', $value);
         $value = str_replace('&lt;', '*', $value);
         $value = str_replace('&GT;', '*', $value);
         $value = str_replace('&gt;', '*', $value);
         $value = str_replace('<script', '*', $value);
         $value = str_replace('/script>', '*', $value);
         $value = str_replace('&#039;', '\'', $value);
      }

      $value = str_replace('`', '\'', $value);
      $value = str_replace('\r', ' ', $value);
      $value = str_replace('\n', ' ', $value);
      $value = str_replace('"', '\'', $value);
      $value = htmlspecialchars($value,ENT_QUOTES,'UTF-8');
      $value = trim($value);
      $value = html_entity_decode($value);
      $value = nl2br($value);
      $value = stripslashes($value);
      return $value;
   }
   // function to get size of website folder
   function convertToReadableSize($size){
      $base = log($size) / log(1024);
      $suffix = array("", "KB", "MB", "GB", "TB");
      $f_base = floor($base);
      return round(pow(1024, $base - floor($base)), 2) .'||'. $suffix[$f_base].' / 50 GB';
   }

   //get empty values of tables
   function getEmptyValue($table){
      $db = new Database();
      try{
         $queryx = new ObjectEmpty();
         $query = $db->query("SELECT COLUMN_NAME from information_schema.columns
         where table_name =  :table AND TABLE_SCHEMA = 'tess_db'");
         $db->bind($query, ":table", $table);
         $db->execute($query);
         while($queryr = $db->fetch($query)){
            $queryx->{$queryr->COLUMN_NAME} = "";
         }
         return $queryx;
      }catch(PDOException $e){
         return false;
      }
   }

   //generate url
   function getUrlLang($id, $lang = "", $only_slug = false){
      global $l, $language;
      //Initiate db
      $language = new Lang();
      $lang = ($lang ? $lang : $language->lang);
      $db = new Database();

      $prefix_lang = ''.(!empty($lang) ? ($lang == 'fr' ? 'fr' : 'en') : $language);
      $suffix = !empty($lang) ? ($lang == 'fr' ? '' : '_en') : $l;


      try{
         $r = $db->query("SELECT tbl_custom_admin_urls.url_id".$suffix." ,
                tbl_pages_admin.parent,
                tbl_pages_admin.root,
                tbl_pages_admin.id
                FROM tbl_custom_admin_urls
                LEFT JOIN tbl_pages_admin
                ON tbl_custom_admin_urls.page_id = tbl_pages_admin.id
                WHERE tbl_custom_admin_urls.page_id = :id
                AND  tbl_custom_admin_urls.url_id".$suffix." is not null
                ORDER BY tbl_custom_admin_urls.date_creation DESC LIMIT 1"
         );
         $db->bind($r,':id', $id);
         $r = $db->single($r);

         if(!$only_slug){
            if($r){
               if(!empty($r->{'url_id'.$suffix}) && $r->{'parent'} != '1')
                  return $prefix_lang.'/'.getUrlLang($r->{'parent'},$lang, 'recursive').$r->{'url_id'.$suffix}.'/';
               else if(!empty($r->{'url_id'.$suffix}) && $r->{'parent'} == '1')
                  return $prefix_lang.'/'.$r->{'url_id'.$suffix}.'/';
               else
                  return $prefix_lang.'/'.getUrlLang($r->{'parent'}, $lang, 'recursive').$id.'/';
            }
            else {
               $parent = $db->query("SELECT parent FROM tbl_pages_admin WHERE id = :id");
               $db->bind($parent,':id', $id);
               $parent= $db->single($parent);
               if($parent)
                  return $prefix_lang.'/'.getUrlLang($parent->{'parent'}, $lang, 'recursive').$id.'/';
               else
                  return $prefix_lang.'/'.$id.'/';
            }
         }
         else{
            if($r){
               if(!empty($r->{'url_id'.$suffix}))
                  return ($only_slug == 'recursive' ? getUrlLang($r->parent, $lang, 'recursive') : '').$r->{'url_id'.$suffix}.'/';
               else
                  return ($only_slug == 'recursive' ? getUrlLang($r->parent, $lang, 'recursive') : '').$id.'/';
            }
            else{
               if($id) {
                  $parent = $db->query("SELECT parent FROM tbl_pages_admin WHERE id = :id ");
                  $db->bind($parent,':id', $id);
                  $parent = $db->resultSet($parent);
                  if($parent->{'parent'})
                     return getUrlLang($parent->{'parent'}, $lang, 'recursive');
               }
            }
         }
      } catch(PDOException $e){
         return false;
      }
   }
   //generate url
   function getUrlLangClient($id, $lang = "", $only_slug = false){
      global $l, $language;
      //Initiate db
      $language = new Lang();
      $lang = ($lang ? $lang : $language->lang);
      $db = new Database();

      
		if(MULTI_LANG){
			$prefix_lang = '/'.(!empty($lang) ? ($lang == 'fr' ? 'fr' : 'en') : $language);
			$suffix = !empty($lang) ? ($lang == 'fr' ? '' : '_en') : $l;
		}else {
			
			  $prefix_lang = ''; 
			  $suffix = "";
		}


      try{
         $r = $db->query("SELECT tbl_custom_urls.url_id".$suffix." ,
                tbl_pages.parent,
                tbl_pages.root,
                tbl_pages.id
                FROM tbl_custom_urls
                LEFT JOIN tbl_pages
                ON tbl_custom_urls.page_id = tbl_pages.id
                WHERE tbl_custom_urls.page_id = :id
                AND  tbl_custom_urls.url_id".$suffix." is not null
                ORDER BY tbl_custom_urls.date_creation DESC LIMIT 1"
         );
         $db->bind($r,':id', $id);
         $r = $db->single($r);

         if(!$only_slug){
            if($r){
               if(!empty($r->{'url_id'.$suffix}) && $r->{'parent'} != '1')
                  return $prefix_lang.'/'.getUrlLangClient($r->{'parent'},$lang, 'recursive').$r->{'url_id'.$suffix}.'/';
               else if(!empty($r->{'url_id'.$suffix}) && $r->{'parent'} == '1')
                  return $prefix_lang.'/'.$r->{'url_id'.$suffix}.'/';
               else
                  return $prefix_lang.'/'.getUrlLangClient($r->{'parent'}, $lang, 'recursive').$id.'/';
            }
            else {
               $parent = $db->query("SELECT parent FROM tbl_pages WHERE id = :id");
               $db->bind($parent,':id', $id);
               $parent= $db->single($parent);
               if($parent)
                  return $prefix_lang.'/'.getUrlLangClient($parent->{'parent'}, $lang, 'recursive').$id.'/';
               else
                  return $prefix_lang.'/'.$id.'/';
            }
         }
         else{
            if($r){
               if(!empty($r->{'url_id'.$suffix}))
                  return ($only_slug == 'recursive' ? getUrlLangClient($r->parent, $lang, 'recursive') : '').$r->{'url_id'.$suffix}.'/';
               else
                  return ($only_slug == 'recursive' ? getUrlLangClient($r->parent, $lang, 'recursive') : '').$id.'/';
            }
            else{
               if($id) {
                  $parent = $db->query("SELECT parent FROM tbl_pages WHERE id = :id ");
                  $db->bind($parent,':id', $id);
                  $parent = $db->resultSet($parent);
                  if($parent->{'parent'})
                     return getUrlLangClient($parent->{'parent'}, $lang, 'recursive');
               }
            }
         }
      } catch(PDOException $e){
         return false;
      }
   }
function getUrlLangProducts($id, $lang = "", $only_slug = false)
	{
		global $l, $language;
		//Initiate db
		$language = new Lang();
		$lang = ($lang ? $lang : $language->lang);
		$db = new Database();
		$id_page_products = __PRODUCTSID__;
		$query_getproducts = $db->query("SELECT url_id" . ($lang !== "fr" ? "_" . $lang : "") . " as geturl FROM tbl_custom_urls WHERE page_id = :id");
		$db->bind($query_getproducts, ":id", $id_page_products);
		$products_url = $db->single($query_getproducts)->geturl;
		$prefix_lang = '/' . (!empty($lang) ? ($lang == 'fr' ? 'fr' : 'en') : $language) . "/" . $products_url;
		$suffix = !empty($lang) ? ($lang == 'fr' ? '' : '_en') : $l;


		try {
			$r = $db->query(
				"SELECT tbl_products_urls.url_id" . $suffix . " ,
                tbl_products.parent,
                tbl_products.root,
                tbl_products.id
                FROM tbl_products_urls
                LEFT JOIN tbl_products
                ON tbl_products_urls.product_id = tbl_products.id 
                LEFT JOIN ( SELECT id as idtess,has_slug FROM tess_controller) tc
                ON tc.idtess = tbl_products.type
                WHERE tbl_products_urls.product_id = :id
                AND tbl_products.online = 1
                AND tc.has_slug = 1 
                AND  tbl_products_urls.url_id" . $suffix . " is not null
                ORDER BY tbl_products_urls.date_creation DESC LIMIT 1"
			);
			$db->bind($r, ':id', $id);
			$r = $db->single($r);

			if (!$only_slug) {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} != '0')
						return $prefix_lang . '/' . getUrlLangProducts($r->{'parent'}, $lang, 'recursive') . $r->{'url_id' . $suffix} . '/'; else if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} == '0')
						return $prefix_lang . '/' . $r->{'url_id' . $suffix} . '/'; else
						return $prefix_lang . '/' . getUrlLangProducts($r->{'parent'}, $lang, 'recursive') . $id . '/';
				} else {
					$parent = $db->query("SELECT parent FROM tbl_products WHERE id = :id");
					$db->bind($parent, ':id', $id);
					$parent = $db->single($parent);
					if ($parent)
						return $prefix_lang . '/' . getUrlLangProducts($parent->{'parent'}, $lang, 'recursive') . $id . '/'; else
						return $prefix_lang . '/' . $id . '/';
				}
			} else {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}))
						return ($only_slug == 'recursive' ? getUrlLangProducts($r->parent, $lang, 'recursive') : '') . $r->{'url_id' . $suffix} . '/'; else
						return ($only_slug == 'recursive' ? getUrlLangProducts($r->parent, $lang, 'recursive') : '') . $id . '/';
				} else {
					if ($id) {
						$parent = $db->query("SELECT parent FROM tbl_products JOIN tbl_products_urls t on tbl_products.id = t.product_id WHERE tbl_products.id = :id AND");
						$db->bind($parent, ':id', $id);
						$parent = $db->resultSet($parent);
						if ($page->{'parent'})
							return getUrlLangProducts($parent->{'parent'}, $lang, 'recursive');
					}
				}
			}
		} catch (PDOException $e) {
			return false;
		}
	}

	/* ------------------ URL BLOGS --------------- */
	function getUrlLangBlogs($id, $lang = "", $only_slug = false)
	{
		global $l, $language;
		//Initiate db
		$language = new Lang();
		$lang = ($lang ? $lang : $language->lang);
		$db = new Database();
		$id_page_products = __BLOGID__;
		$query_getproducts = $db->query("SELECT url_id" . ($lang !== "fr" ? "_" . $lang : "") . " as geturl FROM tbl_custom_urls WHERE page_id = :id");
		$db->bind($query_getproducts, ":id", $id_page_products);
		$products_url = $db->single($query_getproducts)->geturl;
		$prefix_lang = '/' . (!empty($lang) ? ($lang == 'fr' ? 'fr' : 'en') : $language) . "/" . $products_url;
		$suffix = !empty($lang) ? ($lang == 'fr' ? '' : '_en') : $l;


		try {
			$r = $db->query(
				"SELECT tbl_blogs_urls.url_id" . $suffix . " ,
                tbl_blogs.parent,
                tbl_blogs.root,
                tbl_blogs.id
                FROM tbl_blogs_urls
                LEFT JOIN tbl_blogs
                ON tbl_blogs_urls.blog_id = tbl_blogs.id                
                LEFT JOIN ( SELECT id as idtess,has_slug FROM tess_controller) tc
                ON tc.idtess = tbl_blogs.type
                WHERE tbl_blogs_urls.blog_id = :id
                AND tbl_blogs.online = 1
                AND tc.has_slug = 1  
                AND  tbl_blogs_urls.url_id" . $suffix . " is not null
                ORDER BY tbl_blogs_urls.date_creation DESC LIMIT 1"
			);
			$db->bind($r, ':id', $id);
			$r = $db->single($r);

			if (!$only_slug) {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} != '0')
						return $prefix_lang . '/' . getUrlLangBlogs($r->{'parent'}, $lang, 'recursive') . $r->{'url_id' . $suffix} . '/';

				} else {
					$parent = $db->query("SELECT parent FROM tbl_blogs WHERE id = :id");
					$db->bind($parent, ':id', $id);
					$parent = $db->single($parent);
				}
			} else {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}))
						return ($only_slug == 'recursive' ? getUrlLangBlogs($r->parent, $lang, 'recursive') : '') . $r->{'url_id' . $suffix} . '/';

				} else {
					if ($id) {
						$parent = $db->query("SELECT parent FROM tbl_blogs JOIN tbl_blogs_urls t on tbl_blogs.id = t.blog_id WHERE tbl_blogs.id = :id AND");
						$db->bind($parent, ':id', $id);
						$parent = $db->resultSet($parent);
						if ($page->{'parent'})
							return getUrlLangBlogs($parent->{'parent'}, $lang, 'recursive');
					}
				}
			}
		} catch (PDOException $e) {
			return false;
		}
	}

	/* ------------------ URL contests --------------- */
	function getUrlLangContests($id, $lang = "", $only_slug = false)
	{
		global $l, $language;
		//Initiate db
		$language = new Lang();
		$lang = ($lang ? $lang : $language->lang);
		$db = new Database();
		$id_page_products = __CONTESTSID__;
		$query_getproducts = $db->query("SELECT url_id" . ($lang !== "fr" ? "_" . $lang : "") . " as geturl FROM tbl_custom_urls WHERE page_id = :id");
		$db->bind($query_getproducts, ":id", $id_page_products);
		$products_url = $db->single($query_getproducts)->geturl;
		$prefix_lang = '/' . (!empty($lang) ? ($lang == 'fr' ? 'fr' : 'en') : $language) . "/" . $products_url;
		$suffix = !empty($lang) ? ($lang == 'fr' ? '' : '_en') : $l;


		try {
			$r = $db->query(
				"SELECT tbl_contests_urls.url_id" . $suffix . " ,
                tbl_contests.parent,
                tbl_contests.root,
                tbl_contests.id
                FROM tbl_contests_urls
                LEFT JOIN tbl_contests
                ON tbl_contests_urls.contest_id = tbl_contests.id
                WHERE tbl_contests_urls.contest_id = :id
                AND  tbl_contests_urls.url_id" . $suffix . " is not null
                ORDER BY tbl_contests_urls.date_creation DESC LIMIT 1"
			);
			$db->bind($r, ':id', $id);
			$r = $db->single($r);

			if (!$only_slug) {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} != '0')
						return $prefix_lang . '/' . getUrlLangContests($r->{'parent'}, $lang, 'recursive') . $r->{'url_id' . $suffix} . '/'; else if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} == '0')
						return $prefix_lang . '/' . $r->{'url_id' . $suffix} . '/'; else
						return $prefix_lang . '/' . getUrlLangContests($r->{'parent'}, $lang, 'recursive') . $id . '/';
				} else {
					$parent = $db->query("SELECT parent FROM tbl_contests WHERE id = :id");
					$db->bind($parent, ':id', $id);
					$parent = $db->single($parent);
					if ($parent)
						return $prefix_lang . '/' . getUrlLangContests($parent->{'parent'}, $lang, 'recursive') . $id . '/'; else
						return $prefix_lang . '/' . $id . '/';
				}
			} else {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}))
						return ($only_slug == 'recursive' ? getUrlLangContests($r->parent, $lang, 'recursive') : '') . $r->{'url_id' . $suffix} . '/'; else
						return ($only_slug == 'recursive' ? getUrlLangContests($r->parent, $lang, 'recursive') : '') . $id . '/';
				} else {
					if ($id) {
						$parent = $db->query("SELECT parent FROM tbl_contests JOIN tbl_contests_urls t on tbl_contests.id = t.contest_id WHERE tbl_contests.id = :id AND");
						$db->bind($parent, ':id', $id);
						$parent = $db->resultSet($parent);
						if ($page->{'parent'})
							return getUrlLangContests($parent->{'parent'}, $lang, 'recursive');
					}
				}
			}
		} catch (PDOException $e) {
			return false;
		}
	}

	/* ------------------ URL Promotions --------------- */
	function getUrlLangPromotions($id, $lang = "", $only_slug = false)
	{
		global $l, $language;
		//Initiate db
		$language = new Lang();
		$lang = ($lang ? $lang : $language->lang);
		$db = new Database();
		$id_page_products = __PROMOSID__;
		$query_getproducts = $db->query("SELECT url_id" . ($lang !== "fr" ? "_" . $lang : "") . " as geturl FROM tbl_custom_urls WHERE page_id = :id");
		$db->bind($query_getproducts, ":id", $id_page_products);
		$products_url = $db->single($query_getproducts)->geturl;
		$prefix_lang = '/' . (!empty($lang) ? ($lang == 'fr' ? 'fr' : 'en') : $language) . "/" . $products_url;
		$suffix = !empty($lang) ? ($lang == 'fr' ? '' : '_en') : $l;


		try {
			$r = $db->query(
				"SELECT tbl_promotions_urls.url_id" . $suffix . " ,
                tbl_promotions.parent,
                tbl_promotions.root,
                tbl_promotions.id
                FROM tbl_promotions_urls
                LEFT JOIN tbl_promotions
                ON tbl_promotions_urls.promotion_id = tbl_promotions.id
                WHERE tbl_promotions_urls.promotion_id = :id
                AND  tbl_promotions_urls.url_id" . $suffix . " is not null
                ORDER BY tbl_promotions_urls.date_creation DESC LIMIT 1"
			);
			$db->bind($r, ':id', $id);
			$r = $db->single($r);

			if (!$only_slug) {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} != '0')
						return $prefix_lang . '/' . getUrlLangPromotions($r->{'parent'}, $lang, 'recursive') . $r->{'url_id' . $suffix} . '/'; else if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} == '0')
						return $prefix_lang . '/' . $r->{'url_id' . $suffix} . '/'; else
						return $prefix_lang . '/' . getUrlLangPromotions($r->{'parent'}, $lang, 'recursive') . $id . '/';
				} else {
					$parent = $db->query("SELECT parent FROM tbl_promotions WHERE id = :id");
					$db->bind($parent, ':id', $id);
					$parent = $db->single($parent);
					if ($parent)
						return $prefix_lang . '/' . getUrlLangPromotions($parent->{'parent'}, $lang, 'recursive') . $id . '/'; else
						return $prefix_lang . '/' . $id . '/';
				}
			} else {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}))
						return ($only_slug == 'recursive' ? getUrlLangPromotions($r->parent, $lang, 'recursive') : '') . $r->{'url_id' . $suffix} . '/'; else
						return ($only_slug == 'recursive' ? getUrlLangPromotions($r->parent, $lang, 'recursive') : '') . $id . '/';
				} else {
					if ($id) {
						$parent = $db->query("SELECT parent FROM tbl_promotions JOIN tbl_promotions_urls t on tbl_promotions.id = t.promotion_id WHERE tbl_promotions.id = :id AND");
						$db->bind($parent, ':id', $id);
						$parent = $db->resultSet($parent);
						if ($page->{'parent'})
							return getUrlLangPromotions($parent->{'parent'}, $lang, 'recursive');
					}
				}
			}
		} catch (PDOException $e) {
			return false;
		}
	}
   function crypt_my_password($pass=""){
       $hash = "dsfnjiosdhjfoisdj127834891$,1293471829m";

       return hash('sha512', $pass . $hash);
   }

   //generate the menu
   function generate_menu($parent=0, $menu_array=[], $level=0) {
      global $l, $linv, $language, $Tess_db;
      //Instantiate menu string
      $menu = "";
      $Tess_db = new Database();
      $Page = new Page();
      $ajax_level = 3;
      //Instantiate variables child
      $has_child = false;
      $liid = 0;
      if(!empty($menu_array)){

         foreach($menu_array as $value) {
            try{
               $has_child = $Tess_db->query('SELECT * FROM tbl_pages WHERE parent = :parent');
               $Tess_db->bind($has_child,':parent', $value['mid']);
               $has_child = $Tess_db->resultSet($has_child);

               $rights = $Tess_db->query('SELECT * FROM tess_controller WHERE id = :rights ');
               $Tess_db->bind($rights,':rights', $value['type']);
               $rights = $Tess_db->single($rights);

               if(!$Page->checkIfModulesExist($value['type'])){
                  $rights = getEmptyValue("tess_controller");
               }

               $menu .=  '
                  <tr class="tr_'.$liid.' '.(isset($_SESSION['saved_status_accordion']) && in_array($value['mid'], $_SESSION['saved_status_accordion']) ? "show_accordion" : "").' Level_'.$level.'"
                  mid="'.$value['mid'].'" url="'.URLROOT.getUrl($value['mid']).'" url_linv="'.URLROOT.getUrl($value['mid'], $linv).'" level="'.$level.'">
					';
					$menu .= '<td style="width: 76px">';
               if($has_child){
                  if(isset($_SESSION['pages-actives'][$value['mid']]) && $_SESSION['pages-actives'][$value['mid']] == "actif"){
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler page active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = true;
                  } else if(isset($_SESSION['pages-actives'][$value['mid']]) && $_SESSION['pages-actives'][$value['mid']] == "inactif") {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler page " style="cursor:pointer;" data-tooltip="Ouvrir la catégorie"><button type="button" rel="tooltip" class="btn btn-warning" data-original-title="" title="">
                                    <i class="fad fa-chevron-square-up arrows_menu"></i>d
                                 </button></a>';
                     $look_for_child = false;
                  } else if($ajax_level <= $level) {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler page " style="cursor:pointer;" data-tooltip="Ouvrir la catégorie">
                                    <i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = false;
                  } else if($ajax_level > $level) {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler page  active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = true;
                  }else {
                     $menu .=  '';
                  }
               }
               $menu .= "</td>";
					$menu.='
					<td  level="'.$level.'">
               ';
               $menu .=  '<input type="text" name="order[]" style="text-align:center;width:30px !important;background:black;color:white;"
                           value="'.$value['order'].'">
                           <input class="text-center" type="hidden" name="id[]" style="text-align:center;width:15px !important;" value="'.$value['mid'].'">
               ';
               $menu .= '</td><td  class="title_table_list" >';
			   if ($_SESSION['status'] <= $rights->mod or $_SESSION['status'] == "1") {
                  $menu .=  '<a style="line-height: 1.3;font-weight:800;" data-toggle="tooltip" data-placement="top"
                              data-original-title="ID: ' . $value['mid'] . ' & Parent: ' . $value['parent'] . '"
                              title="ID: ' . $value['mid'] . ' & Parent: ' . $value['parent'] . '" data-description="'.$rights->desc.'"
                              href="index.php?cp=page&mod=addmod&id=' . $value['mid'] . '&parent='.$value['parent'].'">';
               }
               if($value['name'] != '') {
				   $db = new Database();
				   $qgetmodule = $db->query("SELECT * FROM tess_controller WHERE id = :id");
				   $db->bind($qgetmodule, ":id", $value['type']);
				   $db->execute($qgetmodule);
				   $module = $db->single($qgetmodule);
				   if($module->desc != ""){
					   $desc = $module->desc;
				   }else{
					   $desc = $module->title;
				   }
                  $menu .=  "<span class='moduleblock'>[".$desc."]</span> Titre: ".$value['name'];
               } else {
                  $menu .=  '(Sans title)';
               }
               // Si on a le drois de modifier
               if (isset($_SESSION['status']) && $_SESSION['status'] <= $rights->mod or $_SESSION['status']=="1") {
                  $menu .=  '</a>';
               }
			   if(is_numeric(rtrim(ltrim(getUrlLangClient($value['mid']), "/"), "/"))){
				   $menu.= '<div class="capture"></div>';
			   }else{
				   $db = new Database();
				   $qgetpage = $db->query("SELECT * FROM tbl_pages WHERE id = :id");
				   $db->bind($qgetpage, ":id", $value['mid']);
				   $db->execute($qgetpage);
				   $pg = $db->single($qgetpage);
				   
				   if($pg->edit_ts == "" || $pg->edit_ts == "0"){
					   $gettime = time();
					   $qgetpage = $db->query("UPDATE tbl_pages SET edit_ts = :edited WHERE id = :id");
					   $db->bind($qgetpage, ":id", $value['mid']);
					   $db->bind($qgetpage, ":edited",  $gettime);
					   $db->execute($qgetpage); 
					   $pg->edit_ts = $gettime;
				   }
				   if(!file_exists("".dirname(dirname(APPROOT))."/public/administration/thumbs/pages/".$value['mid']."--".(is_int($pg->edit_ts) ? $_POST['lastedit'] : strtotime($pg->edit_ts)).".jpg")){
					   $mask = "".dirname(dirname(APPROOT))."/public/administration/thumbs/pages/".$value['mid']."--*.*";
						array_map('unlink', glob($mask)); 
					  $menu.= '<div class="capture" data-category="pages" data-id="'.$value['mid'].'" data-lastedit="'.(is_int($pg->edit_ts) ? $_POST['lastedit'] : strtotime($pg->edit_ts)).'" data-url="'.URLCLIENT.getUrlLangClient($value['mid']).'"></div>'; 
				   }else{
					   $menu.= '<img width="200" src="'.URLROOT."thumbs/pages/".$value['mid']."--".(is_int($pg->edit_ts) ? $_POST['lastedit'] : strtotime($pg->edit_ts)).".jpg".'"/>'; 
				   }
			   }
               
               $menu .= '</td>';
               $menu .= '</td>'."\n";
				$dte = Date("d-m-y H:i:s",(str_replace('-','/', $pg->edit_ts)));
				$menu .= '<td>['.$dte.'] par '.($pg->last_edit_author != "" ? $pg->last_edit_author : "un inconnu" ).'</td>';
               
               $menu .=  '
                    <td class="td-actions text-right">
                        <div class="togglebutton">
                            <label>
                                <input name="online[]['.$value['mid'].']" value="0" type="hidden" />
                                <input name="online[]['.$value['mid'].']" value="1" type="checkbox" '.($value['online'] == 1 ? 'checked=""' : '').' />
                                <span class="toggle"></span>
                            </label>
                        </div>
                    </td>
                    ';
               $menu .=  '<td class="td-actions text-right" ><span class="td-actions text-right"  > ';
               if ($_SESSION['status'] <= $rights->add){
                  if($rights->dtype != "") {
                     $menu .=  '<a class="btn btn-info add_child" data-tooltip="Ajouter" dtype="'.$rights->dtype.'" parent="'.$value['mid'].'" href="index.php?cp=page&amp;mod=addmod&amp;ajout=1&amp;parent='.$value['mid'].'&amp;templates=""><i class="fas fa-plus-square"></i></a> ';
                  }
               }
               if ($_SESSION['status'] <= $rights->mod or $_SESSION['status'] == "1") {
                  $menu .=  '<a class="btn btn-warning" data-toggle="tooltip" data-placement="top"  data-original-title="Type: ' . $value['type'] . '" title="Type: ' . $value['type'] . '"  href="index.php?cp=page&mod=addmod&id=' . $value['mid'] . '&parent='.$value['parent'].'"><i class="fas fa-pen-square"></i></a> ';
               }
               if ($_SESSION['status'] <= $rights->del || $value['type'] == 0) {
                  $menu .=  '<a id="'.$value['mid'].'"  class="btn btn-danger delete-btn" data-tooltip="Supprimer" ><i class="fas fa-times-square"></i></a> ';
               }
               $menu .=  '
                        </span>';
                //call function again to generate nested list for subcategories belonging to this category
               $menu .=  '</td>'."</tr> ";
            }catch(PDOException $e){
               return false;
            }
         }
      }

      return $menu;
   }

   //Generate products system menu
   function generate_products_menu($parent=0, $menu_array=[], $level=0) {
      global $l, $linv, $language, $Tess_db;
      //Instantiate menu string
      $menu = "";
      $Tess_db = new Database();
      $Page = new Product();
      $ajax_level = 3;
      //Instantiate variables child
      $has_child = false;
      $liid = 0;
      $custom = array();
      if(!empty($menu_array)){
         foreach($menu_array as $value) {
            $custom['prix'] = "";
            $custom['Quantité'] = "";
            $custom['Compagnies'] = "";
            $custom_inputs = $Page->getAllCustoms($value['mid']);


            foreach($custom_inputs as $array){
               $get_type = $Tess_db->query("SELECT * FROM inputs WHERE id = :id");
               $Tess_db->bind($get_type, ":id", $array->type);
               $key = $Tess_db->single($get_type);

               if($key->title == "Compagnies"){
                   $query_title_company = $Tess_db->query("SELECT cm.company_name FROM tbl_companies t JOIN tbl_companies_meta cm ON cm.id = t.fk_companies_meta WHERE t.id = :id");
                   $Tess_db->bind($query_title_company, ":id", $array->value);
                   $array->value = (isset($Tess_db->single($query_title_company)->company_name) ? $Tess_db->single($query_title_company)->company_name : "");
               }

               $custom[$key->title] = $array->value;

            }
            try{
               $has_child = $Tess_db->query('SELECT * FROM tbl_products WHERE parent = :parent');
               $Tess_db->bind($has_child,':parent', $value['mid']);
               $has_child = $Tess_db->resultSet($has_child);

               $rights = $Tess_db->query('SELECT * FROM tess_controller WHERE id = :rights ');
               $Tess_db->bind($rights,':rights', $value['type']);
               $rights = $Tess_db->single($rights);

               if(!$Page->checkIfModulesExist($value['type'])){
                  $rights = getEmptyValue("tess_controller");
               }

               $menu .=  '
                  <tr class="tr_'.$liid.' Level_'.$level.'"
                  mid="'.$value['mid'].'" url="'.URLROOT.getUrl($value['mid']).'" url_linv="'.URLROOT.getUrl($value['mid'], $linv).'" level="'.$level.'">
                    
               ';
			   $menu .= '<td style="width: 76px">';
               if($has_child){
                  if(isset($_SESSION['pages-actives'][$value['mid']]) && $_SESSION['pages-actives'][$value['mid']] == "actif"){
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler-product page active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = true;
                  } else if(isset($_SESSION['pages-actives'][$value['mid']]) && $_SESSION['pages-actives'][$value['mid']] == "inactif") {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler-product page " style="cursor:pointer;" data-tooltip="Ouvrir la catégorie"><button type="button" rel="tooltip" class="btn btn-warning" data-original-title="" title="">
                                    <i class="fad fa-chevron-square-up arrows_menu"></i>d
                                 </button></a>';
                     $look_for_child = false;
                  } else if($ajax_level <= $level) {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler-product page " style="cursor:pointer;" data-tooltip="Ouvrir la catégorie">
                                    <i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = false;
                  } else if($ajax_level > $level) {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler-product page  active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = true;
                  }else {
                     $menu .=  '';
                  }
               }
               $menu .= "</td>";
               $menu .=  ' <td  level="'.$level.'"><input type="text" name="order[]" style="text-align:center;width:30px !important;background:black;color:white;"
                           value="'.$value['order'].'">
                           <input class="text-center" type="hidden" name="id[]" style="text-align:center;width:15px !important;" value="'.$value['mid'].'">
               ';
               $value['image'] = (isset($value['image']) && $value['image'] !== "" ? $value['image'] : 'default-avatar.png');
               $menu .= '</td>

              <td>
                  <div style="    height: 40px;
                      width: 40px;
                      display: table;
                      background: rgb(0, 0, 0);
                      overflow: hidden;
                      border-radius: 50%;
                      /* border: 1px solid rgba(0,0,0,0.2); */
                      margin: 0 auto;">
                     <div style="display:table-cell;vertical-align:middle">
                        <img src="'.URLIMG.$value['image'].'" width="40px"  />
                     </div>
				  </div>
               <td class="title_table_list">';
               if ($_SESSION['status'] <= $rights->mod or $_SESSION['status'] == "1") {
                  $menu .=  '<a style="line-height: 1.3;font-weight:800;" data-toggle="tooltip" data-placement="top"
                              data-original-title="ID: ' . $value['mid'] . ' & Parent: ' . $value['parent'] . '"
                              title="ID: ' . $value['mid'] . ' & Parent: ' . $value['parent'] . '" data-description="'.$rights->desc.'"
                              href="index.php?cp=page&mod=addmod&id=' . $value['mid'] . '&parent='.$value['parent'].'">';
               }
               if($value['name'] != '') {
                  $menu .=  $value['name'];
               } else {
                  $menu .=  '(Sans titre)';
               }
               // Si on a le drois de modifier
               if (isset($_SESSION['status']) && $_SESSION['status'] <= $rights->mod or $_SESSION['status']=="1") {
                  $menu .=  '</a>';
               }
               $menu .= '</td>';
               $menu .= '</td>'."\n";

               
               $menu .=  '
                    <td>'.$custom['prix'].'</td>
                    <td>'.$custom['Quantité'].'</td>
                    <td>'.$custom['Compagnies'].'</td>
                    <td class="td-actions text-right">
                        <div class="togglebutton">
                            <label>
                                <input name="online[]['.$value['mid'].']" value="0" type="hidden" />
                                <input name="online[]['.$value['mid'].']" value="1" type="checkbox" '.($value['online'] == 1 ? 'checked=""' : '').' />
                                <span class="toggle"></span>
                            </label>
                        </div>
                    </td>
                    ';
               $menu .=  '<td class="td-actions text-right" ><span class="td-actions text-right"  > ';
               if ($_SESSION['status'] <= $rights->add){
                  if($rights->dtype != "") {
                     $menu .=  '<a class="btn btn-info add_child_products" data-tooltip="Ajouter" dtype="'.$rights->dtype.'" parent="'.$value['mid'].'" href="index.php?cp=page&amp;mod=addmod&amp;ajout=1&amp;parent='.$value['mid'].'&amp;templates=""><i class="fas fa-plus-square"></i></a> ';
                  }
               }
               if ($_SESSION['status'] <= $rights->mod or $_SESSION['status'] == "1") {
                  $menu .=  '<a class="btn btn-warning" data-toggle="tooltip" data-placement="top"  data-original-title="Type: ' . $value['type'] . '" title="Type: ' . $value['type'] . '"  href="index.php?cp=page&mod=addmod&id=' . $value['mid'] . '&parent='.$value['parent'].'"><i class="fas fa-pen-square"></i></a> ';
               }
               if ($_SESSION['status'] <= $rights->del || $value['type'] == 0) {
                  $menu .=  '<a id="'.$value['mid'].'"  class="btn btn-danger delete-product" data-tooltip="Supprimer" ><i class="fas fa-times-square"></i></a> ';
               }
               $menu .=  '
                        </span>';
                //call function again to generate nested list for subcategories belonging to this category
               $menu .=  '</td>'."</tr>";
            }catch(PDOException $e){
               return false;
            }
         }
      }

      return $menu;
   }

   //pcq cest foking chiant sans root
   function getRoot($id=0){

       //on trouve le premier parent
	   
		   $db = new Database();
		   $query_parent = $db->query('SELECT * FROM tbl_pages WHERE id = :id');
		   $db->bind($query_parent, ":id", $id);
		   $infoParent = $db->single($query_parent);
		 
		   $reponse = 0;
		   if(is_object($infoParent)){
			   //si le parent est pas egal a 0, on y va de facon recursive;
			   if($infoParent->{'parent'} !== "0"){
				   return getRoot($infoParent->parent);
			   }else{

				   $reponse = (int) $infoParent->id;
				}
		   }

        return $reponse;
   }

  
function generateSitemap(){
	global $l, $lang, $doc_root, $nomDuSite, $base_url, $bilingue, $install_SEO, $array_no_index;

	$modules_path = APPROOT.'/views/';

	$path_directory = rtrim(APPROOT, '/');
	$filename = 'sitemap.xml';

	if(!file_exists(dirname(dirname(APPROOT)).'/public/'.$filename)){
		fopen(dirname(dirname(APPROOT)).'/public/'.$filename, "w");
	}

    $db = new Database();
	$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9"></urlset>');
	$request_pages = $db->query('
		SELECT
			tp.*,
			tc.module
		FROM tbl_pages tp
		LEFT JOIN tess_controller tc ON tp.type = tc.id
		WHERE
			tc.module != "" AND
            tp.online = 1
            
			AND  tp.id != '. __404ID__ .'
			AND  tp.id != '. __SITEMAPID__  .'
			AND  tp.id != '.__CONDITIONSID__  .'
			AND  tp.id != '.__POLITIQUESID__  .'
			AND  tp.id != '.__THANKYOUID__ .'
			AND  tp.id != '.__PRODUCTSID__ .' 
			AND  tp.id != '.__CHECKOUTID__ .'
			AND  tp.id != '.__DELIVERYPOLICYID__ .'
			AND  tp.id != '.__RETURNPOLICYID__ .'
			AND  tp.id != '.__PAYMENTFAILID__ .'
			AND  tp.id != '.__RECOVERYACCOUNTID__ .'
			AND  tp.id != '.__MAINTENANCEID__ .'
			AND  tp.id != '.__ACTIVATIONACCOUNTID__.'
			AND  tp.id != '.__APRODUCT__.'
			AND  tp.id != '.__PAYMENTTHANKYOUID__.'
			AND  tp.id != '.__ANARTICLE__.'
			AND  tp.id != '.__APROMO__.'
			AND  tp.id != '.__PROMOSID__.'
			AND  tp.id != '.__ACONTEST__.'
			AND  tp.id != '.__AREALISATION__.'
			AND  tp.id != '.__ASUBSCRIPTION__.'
			AND  tp.id != '.__CATEGCONTESTS__.'
			AND  tp.id != '.__CATEGPROMOS__.'
			AND  tp.id != '.__CATEGARTICLES__.'
			AND  tp.id != '.__CATEGPRODUCTS__.'
			AND  tp.id != '.__CAROUSELID__.'
			AND  tp.id != '.__ASLIDEID__.' 
			AND  tp.id != '.__BFULLTEXTID__.'
			AND  tp.id != '.__BFULLIMGID__.'
			AND  tp.id != '.__BTEXTIMGID__.'
			AND  tp.id != '.__BDOUBLETEXTID__.'
			AND  tp.id != '.__BDOUBLEIMGID__.'
			AND  tp.id != '.__BVIDEOID__.'
			AND  tp.id != '.__BTEXTVIDEOID__.' 
			AND  tp.id != '. __CGVSID__.'
		ORDER BY tp.id ASC
	');
    $db->execute($request_pages);
	if($db->rowCount($request_pages) > 0){
		while($page = $db->fetch($request_pages)){
             // if(file_exists($modules_path . $page->module . '.php')){
                  $url = $xml->addChild('url');
                  // error_log($modules_path. $page->module.".php", 0);
                  // error_log(rtrim(URLROOT, "/") . getUrlLang($page->id, 'fr'), 0);
                  $url->addChild('loc', rtrim(URLCLIENT, "/") . getUrlLangClient($page->id, 'fr'));
                  if($page->edit_ts != ''){
                      $url->addChild('lastmod', date('c', strtotime(str_replace('-','/', $page->edit_ts))));
                  }
                  $url_en = $xml->addChild('url');
                  $url_en->addChild('loc', rtrim(URLCLIENT, "/") . getUrlLangClient($page->id, 'en'));
                  if($page->edit_ts != ''){
                      $url_en->addChild('lastmod', date('c', strtotime(str_replace('-','/', $page->edit_ts))));
                  }
             // }
		}
	}
    $queryproducts = $db->query('
		SELECT tbl_products.*
        FROM tbl_products
        LEFT JOIN (SELECT id as idtc, has_slug FROM tess_controller) tc
        ON tc.idtc = tbl_products.type
        WHERE tbl_products.online = 1 AND tbl_products.id != 1 ORDER BY parent, `order` ASC
	');
    $db->execute($queryproducts);
	if($db->rowCount($queryproducts) > 0){
		while($page = $db->fetch($queryproducts)){
             // if(file_exists($modules_path . $page->module . '.php')){
                  $url = $xml->addChild('url');
                  // error_log($modules_path. $page->module.".php", 0);
                  // error_log(rtrim(URLROOT, "/") . getUrlLang($page->id, 'fr'), 0);
                  $url->addChild('loc', rtrim(URLCLIENT, "/") . getUrlLangProducts($page->id, 'fr'));
                  if($page->edit_ts != ''){
                      $url->addChild('lastmod', date('c', $page->edit_ts));
                  }
                  $url_en = $xml->addChild('url');
                  $url_en->addChild('loc', rtrim(URLCLIENT, "/") . getUrlLangProducts($page->id, 'en'));
                  if($page->edit_ts != ''){
                      $url_en->addChild('lastmod', date('c', $page->edit_ts));
                  }
             // }
		}
	}
    $queryproducts = $db->query('
		SELECT tbl_blogs.*
        FROM tbl_blogs
        LEFT JOIN (SELECT id as idtc, has_slug FROM tess_controller) tc
        ON tc.idtc = tbl_blogs.type
        WHERE tbl_blogs.online = 1 ORDER BY parent, `order` ASC
	');
    $db->execute($queryproducts);
	if($db->rowCount($queryproducts) > 0){
		while($page = $db->fetch($queryproducts)){
             // if(file_exists($modules_path . $page->module . '.php')){
                  $url = $xml->addChild('url');
                  // error_log($modules_path. $page->module.".php", 0);
                  // error_log(rtrim(URLROOT, "/") . getUrlLang($page->id, 'fr'), 0);
                  $url->addChild('loc', rtrim(URLCLIENT, "/") . getUrlLangBlogs($page->id, 'fr'));
                  if($page->edit_ts != ''){
                      $url->addChild('lastmod', date('c', $page->edit_ts));
                  }
                  $url_en = $xml->addChild('url');
                  $url_en->addChild('loc', rtrim(URLCLIENT, "/") . getUrlLangBlogs($page->id, 'en'));
                  if($page->edit_ts != ''){
                      $url_en->addChild('lastmod', date('c', $page->edit_ts));
                  }
             // }
		}
	}
	$xml->asXml(dirname(dirname(APPROOT)).'/public/'.$filename);
}

   //Generate blogs system menu
   function generate_blogs_menu($parent=0, $menu_array=[], $level=0) {
      global $l, $linv, $language, $Tess_db;
      //Instantiate menu string
      $menu = "";
      $Tess_db = new Database();
      $Page = new Blog();
      $ajax_level = 3;
      //Instantiate variables child
      $has_child = false;
      $liid = 0;
      $custom = array();
      if(!empty($menu_array)){
         foreach($menu_array as $value) {
            $custom['prix'] = "";
            $custom['Quantité'] = "";
            $custom['Compagnies'] = "";
            $custom['date_created'] = "";
            $custom_inputs = $Page->getAllCustoms($value['mid']);


            foreach($custom_inputs as $array){
               $get_type = $Tess_db->query("SELECT * FROM inputs WHERE id = :id");
               $Tess_db->bind($get_type, ":id", $array->type);
               $key = $Tess_db->single($get_type);

               if($key->title == "Compagnies"){
                   $query_title_company = $Tess_db->query("SELECT cm.company_name FROM tbl_companies t JOIN tbl_companies_meta cm ON cm.id = t.fk_companies_meta WHERE t.id = :id");
                   $Tess_db->bind($query_title_company, ":id", $array->value);
                   $array->value = $Tess_db->single($query_title_company)->company_name;
               }

               $custom[$key->title] = $array->value;

            }
            try{
               $has_child = $Tess_db->query('SELECT * FROM tbl_blogs WHERE parent = :parent');
               $Tess_db->bind($has_child,':parent', $value['mid']);
               $has_child = $Tess_db->resultSet($has_child);

               $rights = $Tess_db->query('SELECT * FROM tess_controller WHERE id = :rights ');
               $Tess_db->bind($rights,':rights', $value['type']);
               $rights = $Tess_db->single($rights);

               if(!$Page->checkIfModulesExist($value['type'])){
                  $rights = getEmptyValue("tess_controller");
               }

               $menu .=  '
                  <tr class="tr_'.$liid.' Level_'.$level.'"
                  mid="'.$value['mid'].'" url="'.URLROOT.getUrl($value['mid']).'" url_linv="'.URLROOT.getUrl($value['mid'], $linv).'" level="'.$level.'">
                     
               ';$menu .= '<td style="width: 76px">';
               if($has_child){
                  if(isset($_SESSION['pages-actives'][$value['mid']]) && $_SESSION['pages-actives'][$value['mid']] == "actif"){
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler-blog page active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = true;
                  } else if(isset($_SESSION['pages-actives'][$value['mid']]) && $_SESSION['pages-actives'][$value['mid']] == "inactif") {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler-blog page " style="cursor:pointer;" data-tooltip="Ouvrir la catégorie"><button type="button" rel="tooltip" class="btn btn-warning" data-original-title="" title="">
                                    <i class="fad fa-chevron-square-up arrows_menu"></i>d
                                 </button></a>';
                     $look_for_child = false;
                  } else if($ajax_level <= $level) {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler-blog page " style="cursor:pointer;" data-tooltip="Ouvrir la catégorie">
                                    <i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = false;
                  } else if($ajax_level > $level) {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler-blog page  active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = true;
                  }else {
                     $menu .=  '';
                  }
               }
               $menu .= "</td>";
               $menu .=  '<td  level="'.$level.'"><input type="text" name="order[]" style="text-align:center;width:30px !important;background:black;color:white;"
                           value="'.$value['order'].'">
                           <input class="text-center" type="hidden" name="id[]" style="text-align:center;width:15px !important;" value="'.$value['mid'].'">
               ';
               $value['image'] = (isset($value['image']) && $value['image'] !== "" ? $value['image'] : 'default-avatar.png');
               $menu .= '</td>

              <td>
                  <div style="    height: 40px;
                      width: 40px;
                      display: table;
                      background: rgb(0, 0, 0);
                      overflow: hidden;
                      border-radius: 50%;
                      /* border: 1px solid rgba(0,0,0,0.2); */
                      margin: 0 auto;">
                     <div style="display:table-cell;vertical-align:middle">
                        <img src="'.URLIMG.$value['image'].'" width="40px"  />
                     </div>
				  </div>
               <td class="title_table_list" >';
               if ($_SESSION['status'] <= $rights->mod or $_SESSION['status'] == "1") {
                  $menu .=  '<a style="line-height: 1.3;font-weight:800;" data-toggle="tooltip" data-placement="top"
                              data-original-title="ID: ' . $value['mid'] . ' & Parent: ' . $value['parent'] . '"
                              title="ID: ' . $value['mid'] . ' & Parent: ' . $value['parent'] . '" data-description="'.$rights->desc.'"
                              href="index.php?cp=page&mod=addmod&id=' . $value['mid'] . '&parent='.$value['parent'].'">';
               }
               if($value['name'] != '') {
                  $menu .=  $value['name'];
               } else {
                  $menu .=  '(Sans titre)';
               }
               // Si on a le drois de modifier
               if (isset($_SESSION['status']) && $_SESSION['status'] <= $rights->mod or $_SESSION['status']=="1") {
                  $menu .=  '</a>';
               }
               $menu .= '</td>';
               $menu .= '</td>'."\n";

               
               $menu .=  '
                    <td>'.date("Y-m-d h:i:s", $value['date_created']).'</td>
                    <td class="td-actions text-right">
                        <div class="togglebutton">
                            <label>
                                <input name="online[]['.$value['mid'].']" value="0" type="hidden" />
                                <input name="online[]['.$value['mid'].']" value="1" type="checkbox" '.($value['online'] == 1 ? 'checked=""' : '').' />
                                <span class="toggle"></span>
                            </label>
                        </div>
                    </td>
                    ';
               $menu .=  '<td class="td-actions text-right" ><span class="td-actions text-right"  > ';
               if ($_SESSION['status'] <= $rights->add){
                  if($rights->dtype != "") {
                     $menu .=  '<a class="btn btn-info add_child_blogs" data-tooltip="Ajouter" dtype="'.$rights->dtype.'" parent="'.$value['mid'].'" href="index.php?cp=page&amp;mod=addmod&amp;ajout=1&amp;parent='.$value['mid'].'&amp;templates=""><i class="fas fa-plus-square"></i></a> ';
                  }
               }
               if ($_SESSION['status'] <= $rights->mod or $_SESSION['status'] == "1") {
                  $menu .=  '<a class="btn btn-warning" data-toggle="tooltip" data-placement="top"  data-original-title="Type: ' . $value['type'] . '" title="Type: ' . $value['type'] . '"  href="index.php?cp=page&mod=addmod&id=' . $value['mid'] . '&parent='.$value['parent'].'"><i class="fas fa-pen-square"></i></a> ';
               }
               if ($_SESSION['status'] <= $rights->del || $value['type'] == 0) {
                  $menu .=  '<a id="'.$value['mid'].'"  class="btn btn-danger delete-blog" data-tooltip="Supprimer" ><i class="fas fa-times-square"></i></a> ';
               }
               $menu .=  '
                        </span>';
                //call function again to generate nested list for subcategories belonging to this category
               $menu .=  '</td>'."</tr>";
            }catch(PDOException $e){
               return false;
            }
         }
      }

      return $menu;
   }

   //Generate contests system menu
   function generate_contests_menu($parent=0, $menu_array=[], $level=0) {
      global $l, $linv, $language, $Tess_db;
      //Instantiate menu string
      $menu = "";
      $Tess_db = new Database();
      $Page = new Contest();
      $ajax_level = 3;
      //Instantiate variables child
      $has_child = false;
      $liid = 0;
      $custom = array();
      if(!empty($menu_array)){
         foreach($menu_array as $value) {
            $custom['prix'] = "";
            $custom['Quantité'] = "";
            $custom['Compagnies'] = "";
            $custom['date_created'] = "";
            $custom_inputs = $Page->getAllCustoms($value['mid']);


            foreach($custom_inputs as $array){
               $get_type = $Tess_db->query("SELECT * FROM inputs WHERE id = :id");
               $Tess_db->bind($get_type, ":id", $array->type);
               $key = $Tess_db->single($get_type);

               if($key->title == "Compagnies"){
                   $query_title_company = $Tess_db->query("SELECT cm.company_name FROM tbl_companies t JOIN tbl_companies_meta cm ON cm.id = t.fk_companies_meta WHERE t.id = :id");
                   $Tess_db->bind($query_title_company, ":id", $array->value);
                   $array->value = $Tess_db->single($query_title_company)->company_name;
               }

               $custom[$key->title] = $array->value;

            }
            try{
               $has_child = $Tess_db->query('SELECT * FROM tbl_contests WHERE parent = :parent');
               $Tess_db->bind($has_child,':parent', $value['mid']);
               $has_child = $Tess_db->resultSet($has_child);

               $rights = $Tess_db->query('SELECT * FROM tess_controller WHERE id = :rights ');
               $Tess_db->bind($rights,':rights', $value['type']);
               $rights = $Tess_db->single($rights);

               if(!$Page->checkIfModulesExist($value['type'])){
                  $rights = getEmptyValue("tess_controller");
               }

               $menu .=  '
                  <tr class="tr_'.$liid.' Level_'.$level.'"
                  mid="'.$value['mid'].'" url="'.URLROOT.getUrl($value['mid']).'" url_linv="'.URLROOT.getUrl($value['mid'], $linv).'" level="'.$level.'">
                     
               ';$menu .= '<td style="width: 76px">';
               if($has_child){
                  if(isset($_SESSION['pages-actives'][$value['mid']]) && $_SESSION['pages-actives'][$value['mid']] == "actif"){
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler-contest page active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = true;
                  } else if(isset($_SESSION['pages-actives'][$value['mid']]) && $_SESSION['pages-actives'][$value['mid']] == "inactif") {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler-contest page " style="cursor:pointer;" data-tooltip="Ouvrir la catégorie"><button type="button" rel="tooltip" class="btn btn-warning" data-original-title="" title="">
                                    <i class="fad fa-chevron-square-up arrows_menu"></i>d
                                 </button></a>';
                     $look_for_child = false;
                  } else if($ajax_level <= $level) {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler-contest page " style="cursor:pointer;" data-tooltip="Ouvrir la catégorie">
                                    <i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = false;
                  } else if($ajax_level > $level) {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler-contest page  active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = true;
                  }else {
                     $menu .=  '';
                  }
               }
               $menu .= "</td>";
               $menu .=  '<td  level="'.$level.'"><input type="text" name="order[]" style="text-align:center;width:30px !important;background:black;color:white;"
                           value="'.$value['order'].'">
                           <input class="text-center" type="hidden" name="id[]" style="text-align:center;width:15px !important;" value="'.$value['mid'].'">
               ';
               $value['image'] = (isset($value['image']) && $value['image'] !== "" ? $value['image'] : 'default-avatar.png');
               $menu .= '</td>

              <td>
                  <div style="    height: 40px;
                      width: 40px;
                      display: table;
                      background: rgb(0, 0, 0);
                      overflow: hidden;
                      border-radius: 50%;
                      /* border: 1px solid rgba(0,0,0,0.2); */
                      margin: 0 auto;">
                     <div style="display:table-cell;vertical-align:middle">
                        <img src="'.URLIMG.$value['image'].'" width="40px"  />
                     </div>
				  </div>
               <td  class="title_table_list" >';
               if ($_SESSION['status'] <= $rights->mod or $_SESSION['status'] == "1") {
                  $menu .=  '<a style="line-height: 1.3;font-weight:800;" data-toggle="tooltip" data-placement="top"
                              data-original-title="ID: ' . $value['mid'] . ' & Parent: ' . $value['parent'] . '"
                              title="ID: ' . $value['mid'] . ' & Parent: ' . $value['parent'] . '" data-description="'.$rights->desc.'"
                              href="index.php?cp=page&mod=addmod&id=' . $value['mid'] . '&parent='.$value['parent'].'">';
               }
               if($value['name'] != '') {
                  $menu .=  $value['name'];
               } else {
                  $menu .=  '(Sans titre)';
               }
               // Si on a le drois de modifier
               if (isset($_SESSION['status']) && $_SESSION['status'] <= $rights->mod or $_SESSION['status']=="1") {
                  $menu .=  '</a>';
               }
               $menu .= '</td>';
               $menu .= '</td>'."\n";

               
               $menu .=  '
                    <td>'.date("Y-m-d h:i:s", $value['date_created']).'</td>
                    <td class="td-actions text-right">
                        <div class="togglebutton">
                            <label>
                                <input name="online[]['.$value['mid'].']" value="0" type="hidden" />
                                <input name="online[]['.$value['mid'].']" value="1" type="checkbox" '.($value['online'] == 1 ? 'checked=""' : '').' />
                                <span class="toggle"></span>
                            </label>
                        </div>
                    </td>
                    ';
               $menu .=  '<td class="td-actions text-right" ><span class="td-actions text-right"  > ';
               if ($_SESSION['status'] <= $rights->add){
                  if($rights->dtype != "") {
                     $menu .=  '<a class="btn btn-info add_child_contests" data-tooltip="Ajouter" dtype="'.$rights->dtype.'" parent="'.$value['mid'].'" href="index.php?cp=page&amp;mod=addmod&amp;ajout=1&amp;parent='.$value['mid'].'&amp;templates=""><i class="fas fa-plus-square"></i></a> ';
                  }
               }
               if ($_SESSION['status'] <= $rights->mod or $_SESSION['status'] == "1") {
                  $menu .=  '<a class="btn btn-warning" data-toggle="tooltip" data-placement="top"  data-original-title="Type: ' . $value['type'] . '" title="Type: ' . $value['type'] . '"  href="index.php?cp=page&mod=addmod&id=' . $value['mid'] . '&parent='.$value['parent'].'"><i class="fas fa-pen-square"></i></a> ';
               }
               if ($_SESSION['status'] <= $rights->del || $value['type'] == 0) {
                  $menu .=  '<a id="'.$value['mid'].'"  class="btn btn-danger delete-contest" data-tooltip="Supprimer" ><i class="fas fa-times-square"></i></a> ';
               }
               $menu .=  '
                        </span>';
                //call function again to generate nested list for subcategories belonging to this category
               $menu .=  '</td>'."</tr>";
            }catch(PDOException $e){
               return false;
            }
         }
      }

      return $menu;
   }

   //Generate promotions system menu
   function generate_promotions_menu($parent=0, $menu_array=[], $level=0) {
      global $l, $linv, $language, $Tess_db;
      //Instantiate menu string
      $menu = "";
      $Tess_db = new Database();
      $Page = new promotion();
      $ajax_level = 3;
      //Instantiate variables child
      $has_child = false;
      $liid = 0;
      $custom = array();
      if(!empty($menu_array)){
         foreach($menu_array as $value) {
            $custom['prix'] = "";
            $custom['Quantité'] = "";
            $custom['Compagnies'] = "";
            $custom['date_created'] = "";
            $custom_inputs = $Page->getAllCustoms($value['mid']);


            foreach($custom_inputs as $array){
               $get_type = $Tess_db->query("SELECT * FROM inputs WHERE id = :id");
               $Tess_db->bind($get_type, ":id", $array->type);
               $key = $Tess_db->single($get_type);

               if($key->title == "Compagnies"){
                   $query_title_company = $Tess_db->query("SELECT cm.company_name FROM tbl_companies t JOIN tbl_companies_meta cm ON cm.id = t.fk_companies_meta WHERE t.id = :id");
                   $Tess_db->bind($query_title_company, ":id", $array->value);
                   $array->value = $Tess_db->single($query_title_company)->company_name;
               }

               $custom[$key->title] = $array->value;

            }
            try{
               $has_child = $Tess_db->query('SELECT * FROM tbl_promotions WHERE parent = :parent');
               $Tess_db->bind($has_child,':parent', $value['mid']);
               $has_child = $Tess_db->resultSet($has_child);

               $rights = $Tess_db->query('SELECT * FROM tess_controller WHERE id = :rights ');
               $Tess_db->bind($rights,':rights', $value['type']);
               $rights = $Tess_db->single($rights);

               if(!$Page->checkIfModulesExist($value['type'])){
                  $rights = getEmptyValue("tess_controller");
               }

               $menu .=  '
                  <tr class="tr_'.$liid.' Level_'.$level.'"
                  mid="'.$value['mid'].'" url="'.URLROOT.getUrl($value['mid']).'" url_linv="'.URLROOT.getUrl($value['mid'], $linv).'" level="'.$level.'">
                     
               ';$menu .= '<td style="width: 76px">';
               if($has_child){
                  if(isset($_SESSION['pages-actives'][$value['mid']]) && $_SESSION['pages-actives'][$value['mid']] == "actif"){
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler-promotion page active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = true;
                  } else if(isset($_SESSION['pages-actives'][$value['mid']]) && $_SESSION['pages-actives'][$value['mid']] == "inactif") {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler-promotion page " style="cursor:pointer;" data-tooltip="Ouvrir la catégorie"><button type="button" rel="tooltip" class="btn btn-warning" data-original-title="" title="">
                                    <i class="fad fa-chevron-square-up arrows_menu"></i>d
                                 </button></a>';
                     $look_for_child = false;
                  } else if($ajax_level <= $level) {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler-promotion page " style="cursor:pointer;" data-tooltip="Ouvrir la catégorie">
                                    <i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = false;
                  } else if($ajax_level > $level) {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler-promotion page  active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = true;
                  }else {
                     $menu .=  '';
                  }
               }
               $menu .= "</td>";
               $menu .=  '<td  level="'.$level.'"><input type="text" name="order[]" style="text-align:center;width:30px !important;background:black;color:white;"
                           value="'.$value['order'].'">
                           <input class="text-center" type="hidden" name="id[]" style="text-align:center;width:15px !important;" value="'.$value['mid'].'">
               ';
               $value['image'] = (isset($value['image']) && $value['image'] !== "" ? $value['image'] : 'default-avatar.png');
               $menu .= '</td>

              <td>
                  <div style="    height: 40px;
                      width: 40px;
                      display: table;
                      background: rgb(0, 0, 0);
                      overflow: hidden;
                      border-radius: 50%;
                      /* border: 1px solid rgba(0,0,0,0.2); */
                      margin: 0 auto;">
                     <div style="display:table-cell;vertical-align:middle">
                        <img src="'.URLIMG.$value['image'].'" width="40px"  />
                     </div>
				  </div>
               <td class="title_table_list" >';
               if ($_SESSION['status'] <= $rights->mod or $_SESSION['status'] == "1") {
                  $menu .=  '<a style="line-height: 1.3;font-weight:800;" data-toggle="tooltip" data-placement="top"
                              data-original-title="ID: ' . $value['mid'] . ' & Parent: ' . $value['parent'] . '"
                              title="ID: ' . $value['mid'] . ' & Parent: ' . $value['parent'] . '" data-description="'.$rights->desc.'"
                              href="index.php?cp=page&mod=addmod&id=' . $value['mid'] . '&parent='.$value['parent'].'">';
               }
               if($value['name'] != '') {
                  $menu .=  $value['name'];
               } else {
                  $menu .=  '(Sans titre)';
               }
               // Si on a le drois de modifier
               if (isset($_SESSION['status']) && $_SESSION['status'] <= $rights->mod or $_SESSION['status']=="1") {
                  $menu .=  '</a>';
               }
               $menu .= '</td>';
               $menu .= '</td>'."\n";

               
               $menu .=  '
                    <td>'.date("Y-m-d h:i:s", $value['date_created']).'</td>
                    <td class="td-actions text-right">
                        <div class="togglebutton">
                            <label>
                                <input name="online[]['.$value['mid'].']" value="0" type="hidden" />
                                <input name="online[]['.$value['mid'].']" value="1" type="checkbox" '.($value['online'] == 1 ? 'checked=""' : '').' />
                                <span class="toggle"></span>
                            </label>
                        </div>
                    </td>
                    ';
               $menu .=  '<td class="td-actions text-right" ><span class="td-actions text-right"  > ';
               if ($_SESSION['status'] <= $rights->add){
                  if($rights->dtype != "") {
                     $menu .=  '<a class="btn btn-info add_child_promotions" data-tooltip="Ajouter" dtype="'.$rights->dtype.'" parent="'.$value['mid'].'" href="index.php?cp=page&amp;mod=addmod&amp;ajout=1&amp;parent='.$value['mid'].'&amp;templates=""><i class="fas fa-plus-square"></i></a> ';
                  }
               }
               if ($_SESSION['status'] <= $rights->mod or $_SESSION['status'] == "1") {
                  $menu .=  '<a class="btn btn-warning" data-toggle="tooltip" data-placement="top"  data-original-title="Type: ' . $value['type'] . '" title="Type: ' . $value['type'] . '"  href="index.php?cp=page&mod=addmod&id=' . $value['mid'] . '&parent='.$value['parent'].'"><i class="fas fa-pen-square"></i></a> ';
               }
               if ($_SESSION['status'] <= $rights->del || $value['type'] == 0) {
                  $menu .=  '<a id="'.$value['mid'].'"  class="btn btn-danger delete-promotion" data-tooltip="Supprimer" ><i class="fas fa-times-square"></i></a> ';
               }
               $menu .=  '
                        </span>';
                //call function again to generate nested list for subcategories belonging to this category
               $menu .=  '</td>'."</tr>";
            }catch(PDOException $e){
               return false;
            }
         }
      }

      return $menu;
   }

   //Generate promotions system menu
   function generate_tess_users_menu($parent=0, $menu_array=[], $level=0) {
      global $l, $linv, $language, $Tess_db;
      //Instantiate menu string
      $menu = "";
      $Tess_db = new Database();
      $ajax_level = 3;
      //Instantiate variables child
      $has_child = false;
      $liid = 0;
      $custom = array();
      if(!empty($menu_array)){
         foreach($menu_array as $value) {

               $value['profile_image'] = (isset($value['profile_image']) && $value['profile_image'] !== "" ? $value['profile_image'] : 'default-avatar.png');
               $menu .=  '
                  <tr class="tr_'.$liid.' Level_'.$level.'"
                  mid="'.$value['id'].'" url="'.URLROOT.getUrl($value['id']).'" url_linv="'.URLROOT.getUrl($value['id'], $linv).'" level="'.$level.'">
                     <td  level="'.$level.'">
               ';
               $menu .=  $value['id'];
               $menu .= '</td>

              <td>
                  <div style="    height: 40px;
                      width: 40px;
                      display: table;
                      background: #d1d1d1;
                      overflow: hidden;
                      border-radius: 50%;
                      /* border: 1px solid rgba(0,0,0,0.2); */
                      margin: 0 auto;">
                     <div style="display:table-cell;vertical-align:middle">
                        <img src="'.URLIMG.$value['profile_image'].'" width="40px"  />
                     </div>
				  </div>
               <td class="title_table_list" >';
               if ($_SESSION['status'] == "1") {
                  $menu .=  '<a style="line-height: 1.3;font-weight:800;" data-toggle="tooltip" data-placement="top"
                              data-original-title="ID: ' . $value['id'] . '"
                              title="ID: ' . $value['id'] . ' & Parent:x" data-description=" "
                              href="index.php?cp=page&mod=addmod&id=' . $value['id'] . '">';
               }
               if($value['pseudo'] != '') {
                  $menu .=  $value['pseudo'];
               } else {
                  $menu .=  '(Sans pseudo)';
               }
               // Si on a le drois de modifier
               if (isset($_SESSION['status']) &&  $_SESSION['status']=="1") {
                  $menu .=  '</a>';
               }
               $menu .= '</td>';

                    // <td>'.$value['first_name'].'</td>
                    // <td>'.$value['last_name'].'</td>
               $menu .=  '
                    <td>'.$value['status'].'</td>
                    <td>'.$value['email'].'</td>
                    <td>'.date("Y-m-d h:i:s", $value['date_created']).'</td>
                    <td class="td-actions ">
                        <div class="togglebutton">
                            <label>
                                <input name="online[]['.$value['id'].']" value="0" type="hidden" />
                                <input name="online[]['.$value['id'].']" value="1" type="checkbox" '.($value['online'] == 1 ? 'checked=""' : '').' />
                                <span class="toggle"></span>
                            </label>
                        </div>
                    </td>
                    ';
               $menu .=  '<td class="td-actions " ><span class="td-actions "  > ';

               if ($_SESSION['status'] == "1") {
                  $menu .=  '<a class="btn btn-warning" data-toggle="tooltip" data-placement="top"  href="#" data-original-title="Type: ' . $value['status'] . '"  ><i class="fas fa-pen-square"></i></a> ';
               }
               if ($_SESSION['status'] == "1") {
                  $menu .=  '<a id="'.$value['id'].'"  class="btn btn-danger delete-tess-users" data-tooltip="Supprimer" ><i class="fas fa-times-square"></i></a> ';
               }
               $menu .=  '
                        </span>';
                //call function again to generate nested list for subcategories belonging to this category
               $menu .=  '</td>'."</tr>";

         }
      }

      return $menu;
   }

   //Generate menu terms
   function generate_menu_terms($parent=0, $menu_array=[], $level=0) {
      global $l, $linv, $language, $Tess_db;
      //Instantiate menu string
      $menu = "";
      $Tess_db = new Database();
      $Page = new Dictionary();
      $ajax_level = 3;
      //Instantiate variables child
      $has_child = false;
      $liid = 0;
      if(!empty($menu_array)){

         foreach($menu_array as $value) {
            try{
               $has_child = $Tess_db->query('SELECT * FROM dictionary_term WHERE categ = :parent');
               $Tess_db->bind($has_child,':parent', $value['mid']);
               $has_child = $Tess_db->resultSet($has_child);


               $menu .=  '
                  <tr class="tr_'.$liid.' Level_'.$level.'"
                  mid="'.$value['mid'].'" url="'.URLROOT.getUrl($value['mid']).'" url_linv="'.URLROOT.getUrl($value['mid'], $linv).'" level="'.$level.'">
                     
               ';$menu .= '<td style="width: 76px">';
               if($has_child){
                  if(isset($_SESSION['pages-actives'][$value['mid']]) && $_SESSION['pages-actives'][$value['mid']] == "actif"){
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler page terms active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = true;
                  } else if(isset($_SESSION['pages-actives'][$value['mid']]) && $_SESSION['pages-actives'][$value['mid']] == "inactif") {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler page terms " style="cursor:pointer;" data-tooltip="Ouvrir la catégorie"><button type="button" rel="tooltip" class="btn btn-warning" data-original-title="" title="">
                                    <i class="fad fa-chevron-square-up arrows_menu"></i>d
                                 </button></a>';
                     $look_for_child = false;
                  } else if($ajax_level <= $level) {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler page terms " style="cursor:pointer;" data-tooltip="Ouvrir la catégorie">
                                    <i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = false;
                  } else if($ajax_level > $level) {
                     $menu .=  '<a id="page-'.$value['mid'].'" class="btn-derouler page terms active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                     $look_for_child = true;
                  }else {
                     $menu .=  '';
                  }
               }
               $menu .= "</td>";
               $menu .=  '<td  level="'.$level.'"><p>
                          '.$value['order'].'</p>
               ';
               $menu .= '</td><td  class="title_table_list" >';
               // $menu .=  '<a style="line-height: 1.3;font-weight:800;" data-toggle="tooltip" data-placement="top"
                           // data-original-title="ID: ' . $value['mid'] . '" href="#">';

               if($value['name'] != '') {
                  $menu .=  $value['name'];
               } else {
                  $menu .=  '(Sans title)';
               }
               // $menu .=  '</a>';
               $menu .= '</td>';
               $menu .= '</td>'."\n";

               

               $menu .=  '<td class="td-actions text-right" ><span class="td-actions text-right"  > ';

               $menu .=  '<a class="btn btn-info add_child_terms" data-tooltip="Ajouter" dtype="3" parent="'.$value['mid'].'" href="index.php?cp=page&amp;mod=addmod&amp;ajout=1&amp;parent='.$value['mid'].'&amp;templates=""><i class="fas fa-plus-square"></i></a> ';

               // $menu .=  '<a class="btn btn-warning" data-toggle="tooltip" data-placement="top"  data-original-title="Type: ' . $value['type'] . '" title="Type: ' . $value['type'] . '"  href="index.php?cp=page&mod=addmod&id=' . $value['mid'] . '&parent='.$value['parent'].'"><i class="fas fa-pen-square"></i></a> ';

               // $menu .=  '<a id="'.$value['mid'].'"  class="btn btn-danger delete-btn-terms" data-tooltip="Supprimer" ><i class="fas fa-times-square"></i></a> ';

               $menu .=  '
                        </span>';
                //call function again to generate nested list for subcategories belonging to this category
               $menu .=  '</td>'."</tr>";
            }catch(PDOException $e){
               return false;
            }
         }
      }

      return $menu;
   }

   //get inputs type
   function getInputsType(){
       $array_types = [
           /* Text, textarea & globals */
           "Text",
           "Textarea",
           "Color",
           "Datepicker",
           /* WYSIWYG */
           "WYSIWYG",
           /* UPLOAD IMAGE */
           "Upload Image",
           /* UPLOAD OTHER */
           "Upload Autre",
           /* Join relation tables */
           "Jointure page",
           /* Radio button (multiple with labels) */
           "Radio",
           /* Categories & checkboxes */
           "Cat&eacute;gorie",
           "Checkbox",
           /* Nice dual radio switch */
           "toggle",
           /* Categories radios */
           "Cat&eacute;gorie radio",
           /* SELECT */
           "Select"
       ];
       return $array_types;
   }

   //Notes position
   function getNotesPosition(){
       $array_positions = [
           "haut" => "En haut",
           "droit" => "À droite",
           "gauche" => "À gauche",
           "bas" => "En bas",
           "title" => "Dessous le title"
       ];
       return $array_positions;
   }

   //Header of pdf page
   function pdf_header(){
      global $l, $lang, $language, $base_url, $nomDuSite;

      $html = '
      <page backtop="10mm" backleft="5mm" backright="5mm" backbottom="10mm" format="A3">
      <table width="1035" style="border-collapse:collapse;border-spacing:0;background:#3b3b3b;">
         <tr>
            <td colspan="3" width="1035" height="20"></td>
         </tr>
         <tr>
            <td width="20"></td>
            <td width="995" style="text-align:center;">
               <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#bebdbe;font-size:10px;line-height:1.4;font-weight:400;">
               <span style="text-transform:uppercase; font-weight:500">    Tess industries</span>
               Tous droits réservés
               </span>
            </td>
            <td width="20"></td>
         </tr>
         <tr>
            <td colspan="3" width="1035" height="20"></td>
         </tr>
      </table>
      ';

      return $html;
   }

   //Footer of pdf page
   function pdf_footer(){
      global $l, $lang, $language, $base_url, $nomDuSite;

      $html = '
      <table width="1035" style="border-collapse:collapse;border-spacing:0;background:#3b3b3b;">
         <tr>
            <td colspan="3" width="1035" height="20"></td>
         </tr>
         <tr>
            <td width="20"></td>
            <td width="995" style="text-align:center;">
               <span style="display:inline-block;margin:0;padding:0;font-family:arial, sans-serif;color:#fff;font-size:12px;line-height:1.4;font-weight:400;">
                  Suivez-nous
               </span><br/>
               <span style="display:inline-block;margin:0;padding:0;">
                  facebook
               </span><br/><br/>
               <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#bebdbe;font-size:10px;line-height:1.4;font-weight:400;">
               <span style="text-transform:uppercase; font-weight:500">© '. date('Y') .' Tess industries</span>
                  Tous droits réservés
               </span>
            </td>
            <td width="20"></td>
         </tr>
         <tr>
            <td colspan="3" width="1035" height="20"></td>
         </tr>
      </table>
      ';

      $html .= '
      </page>
      ';

      return $html;
   }

   //Generate pdf page
   function create_pdf_template($template, $download = false, $mail = true,$bill = [], $type="bill"){
      global $l, $lang, $language, $Tess, $doc_root;

      require "../plugins/html2pdf/html2pdf.class.php" ;

      $html = pdf_header();
      if($template == 'bill'){
         $html .=' <table><tr><td>Commande # '.$bill->id.'</td></tr></table>';
      }

      if($template == 'payment_bill'){
         $html .=' <table><tr><td>Paiement # '.$bill->id.'</td></tr></table>';
      }

      if($template == 'payment_bill_credit'){
         $html .=' <table><tr><td>Paiement solde</td></tr></table>';
      }

      $html .= pdf_footer();

      //Il n'affiche pas les "à" à moins qu'ils soient en html entities
      $contentPDF = str_replace('à', '&agrave;', $html);
      $contentPDF = str_replace('é', '&eacute;', $html);
      // echo $contentPDF;
      try{
         ob_end_clean();
         $pdf = new \HTML2PDF('P', 'A3', 'En');
         $pdf->writeHTML($contentPDF);
         if($download === true){
            $pdf->output('export_command.pdf', 'D'); //D pour télécharger
         }elseif($download == 'view'){
            $pdf->output('export_command.pdf');
         }else{
            return $pdf->output('', true);
         }
      }catch(HTML2PDF_exception $e){
         die($e);
      }
   }

   //get highest value
   function getHighestValue($data_points, $value=''){
      $max=0;
      foreach($data_points as $point){
         if($value){
            if($max < (float)$point->{$value}){
               $max = $point->{$value};
            }
         }else{
            if($max < (float)$point->total_one){
               $max = $point->total_one;
            }
            if($max < (float)$point->total_two){
               $max = $point->total_two;
            }
            if($max < (float)$point->total_third){
               $max = $point->total_third;
            }
         }
      }
      return $max;
   }

   // Function to gather all countries
   function fetch_countries(){
      $countries = array(
         "Canada"
      );
      return $countries;
   }

   //Function to gather all states
   function fetch_states($country="Canada"){
      $states = array();
      if(!isset($country) || $country == ""){
         $country = "Canada";
      }
      $states["Canada"] = array(
         "Alberta",
         "Colombie-Britannique",
         "Île-du-Prince-Édouard",
         "Manitoba",
         "Nouveau-Brunswick",
         "Nouvelle-Écosse",
         "Ontario",
         "Québec",
         "Saskatchewan",
         "Terre-Neuve-et-Labrador",
         "Nunavut",
         "Territoires du Nord-Ouest",
         "Yukon"
      );
      return $states[$country];
   }

   //Function to gather all regions
   function fetch_regions($state="Québec"){
      $regions = array();
      if(!isset($state) || $state == ""){
         $state = "Québec";
      }
      $regions["Québec"] = array(
         "Abitibi-Témiscamingue",
         "Bas-Saint-Laurent",
         "Capitale-Nationale",
         "Centre-du-Québec",
         "Chaudière-Appalaches",
         "Côte-Nord",
         "Estrie",
         "Gaspésie–Îles-de-la-Madeleine",
         "Lanaudière",
         "Laurentides",
         "Laval",
         "Mauricie",
         "Montérégie",
         "Montréal",
         "Nord-du-Québec",
         "Outaouais",
         "Saguenay–Lac-Saint-Jean"
      );
      return $regions[$state];
   }

   //Function to gather all genders
   function fetch_genders(){
      $genders = array(
         "Homme",
         "Femme",
         "Autre"
      );
      return $genders;
   }

   //function to rename payments
   function rename_payments($value){
      switch($value){
         case 'check':
            $value = 'Chèque';
         break;
         case 'cash':
            $value = 'Comptant';
         break;
         case '':
            $value = 'Non spécifié';
         break;
         case 'credit_card':
            $value = 'Carte de crédit';
         break;
         case 'interac_transfer':
            $value = 'Transfert Interac';
         break;
         case 'bank_transfer':
            $value = 'Transfert bancaire';
         break;
         case 'manual_input':
            $value = 'Saisies manuelles';
         break;
         case 'credit':
            $value = 'Solde au compte';
         break;
         case 'refund':
            $value = 'Annulation';
         break;
         case 'group_orders':
            $value = 'Groupe anciennes commandes';
         break;
      }
      return $value;
   }

   //function to get pagination
   function pagination($query, $per_page = 3,$page = 1, $url = '&', $bind=[]){

       $db = new Database();
       if(isset($_GET['page'])){
         $url= str_replace('&page='.$_GET['page'], '', $url);
       }
       $query_page = $db->query("
           SELECT COUNT(*) as `num` FROM {$query}
       ");
       // var_dump($bind);
        foreach($bind as $key => $value){
            $db->bind($query_page, ":".$key, $value);
        }
       $row = $db->single($query_page);
       $total = $row->num;
       $adjacents = "2";
       $page = ($page == 0 ? 1 : $page);
       $start = ($page - 1) * $per_page;
       $prev = (($page == 1) ? 2 : $page) - 1;
       $next = $page + 1;
       $lastpage = ceil($total/$per_page);
       $lpm1 = $lastpage - 1;
       $pagination = "";
       if($lastpage > 1)
       {
           $pagination .= "
               <span>Page $page de $lastpage</span>
               <ul class='pagination'>
           ";
           $pagination .= "
                   <li class='page-item' colspan='4'>
           ";
           $pagination.= "
                       <a class='page-link' href='{$url}page=1'><<</a>
           ";
           $pagination.= "
                       <a class='page-link' href='{$url}page=$prev'>Précédent</a>
           ";
           if ($lastpage < 7 + ($adjacents * 2))
           {
               for ($counter = 1; $counter <= $lastpage; $counter++)
               {
                   if ($counter == $page)
                       $pagination.= "
                       <a href='{$url}page=$counter' class='page-link current active'>$counter</a>
                       ";
                   else
                       $pagination.= "
                       <a class='page-link' href='{$url}page=$counter'>$counter</a>
                       ";
               }
           }
           elseif($lastpage > 5 + ($adjacents * 2))
           {
               if($page < 1 + ($adjacents * 2))
               {
                   for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                   {
                       if ($counter == $page)
                           $pagination.= "
                       <a class='page-link' href='{$url}page=$counter' class='current'>$counter</a>
                           ";
                       else
                           $pagination.= "
                       <a class='page-link' href='{$url}page=$counter'>$counter</a>
                           ";
                   }
                   $pagination.= "
                       <a href='#'>...</a>
                   ";
                   $pagination.= "
                       <a class='page-link' href='{$url}page=$lpm1'>$lpm1</a>
                   ";
                   $pagination.= "
                       <a  class='page-link' href='{$url}page=$lastpage'>$lastpage</a>
                   ";
               }
               elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
               {
                   $pagination.= "
                       <a class='page-link' href='{$url}page=1'>1</a>
                   ";
                   $pagination.= "
                       <a class='page-link' href='{$url}page=2'>2</a>
                   ";
                   $pagination.= "
                       <a href='#'>...</a>
                   ";
                   for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                   {
                       if ($counter == $page)
                           $pagination.= "
                       <a class='page-link' href='{$url}page=$counter' class='current'>$counter</a>
                           ";
                       else
                           $pagination.= "
                       <a class='page-link' href='{$url}page=$counter'>$counter</a>";
                   }
                   $pagination.= "
                       <a href='#'>..</a>
                   ";
                   $pagination.= "
                       <a class='page-link' href='{$url}page=$lpm1'>$lpm1</a>
                   ";
                   $pagination.= "
                       <a class='page-link' href='{$url}page=$lastpage'>$lastpage</a>
                   ";
               }
               else
               {
                   $pagination.= "
                       <a href='{$url}page=1'>1</a>
                   ";
                   $pagination.= "
                       <a href='{$url}page=2'>2</a>
                   ";
                   $pagination.= "
                       <a href='#'>..</a>
                   ";
                   for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                   {
                       if ($counter == $page)
                           $pagination.= "
                       <a href='{$url}page=$counter' class='page-item current active'>$counter</a>
                           ";
                       else
                           $pagination.= "
                       <a  class='page-link' href='{$url}page=$counter'>$counter</a>
                           ";
                   }
               }
           }

           if ($page < $counter - 1){
               $pagination.= "
                       <a  class='page-link' href='{$url}page=$next'>Suivant</a>
               ";
               $pagination.= "
                       <a class='page-link' href='{$url}page=$lastpage'>>></a>
               ";
           }else{
               $pagination.= "
                       <a class='page-link' href='{$url}page=$counter' class='current'>Suivant</a>
               ";
               $pagination.= "
                       <a class='page-link' href='{$url}page=$counter' class='current'>>></a>
               ";
           }
           $pagination.= "
                   </li>
               </ul>\n
           ";
       }
       echo $pagination;
   }

   //get method payments
   function getMethodPayments($type = ''){
      $array_payment_method = array(
         'credit_card' => array(
            '' => 'Carte de crédit',
            '_en' => 'Credit card'
         ),
         'check' => array(
            '' => 'Chèque',
            '_en' => 'Check'
         ),
         'cash' => array(
            '' => 'Argent comptant',
            '_en' => 'Cash'
         ),
         'interac_transfer' => array(
            '' => 'Virement Interac',
            '_en' => 'Interact transfer'
         ),
         'bank_transfer' => array(
            '' => 'Virement bancaire',
            '_en' => 'bank transfer'
         )
      );

      if($type == ""){
         $return = $array_payment_method;
      }else{
         $return = $array_payment_method[$type];
      }
      return $return;
   }

   //initialize the dictionary
   function dictionary_initialize($the_lang=""){
      global $l, $linv, $language, $Tess_db;

      $Tess_db = new Database();
      if(!$the_lang){

         $the_lang = $language;
      }
      $all_dictionary_entries = $Tess_db->query("SELECT dictionary_term.term_id,
      dictionary.text_value,
      dictionary_term.data_type
      FROM dictionary
      INNER JOIN dictionary_term
      ON dictionary_term.id = dictionary.term_id
      WHERE dictionary.language = :lang
      ");

      $Tess_db->bind($all_dictionary_entries, ":lang", $the_lang);
      $Tess_db->execute($all_dictionary_entries);
      $temp_array = array();
      while($current = $Tess_db->fetch($all_dictionary_entries, 'array')){
         switch($current['data_type']){
               case 'array':
                  $temp_array[$current['term_id']] = unserialize($current['text_value']);
               break;

               default: // String
                  $temp_array[$current['term_id']] = $current['text_value'];
               break;
            }
      }

      return $temp_array;
   }

   //import terms of dictionary
   function dictionary_import($lang_array){
      global $l, $linv, $language, $Tess_db;

      $db = new Database("tess_db");

      foreach($lang_array as $language => $dictionary_entries){

         foreach($dictionary_entries as $term_id => $text_value){

            $term_exists = $Tess_db->query("
            SELECT id
            FROM dictionary_term
            WHERE term_id = :termid
            ");
            $Tess_db->bind($term_exists, ":termid", $term_id);

            if($Tess_db->rowCount($term_exists) > 0){
               $term_id = $Tess_db->single($term_exists)->id;
            }
            else {
               $insert_term = $Tess_db->query('
               INSERT INTO `dictionary_term`(
                  `term_id`,
                  `data_type`,
                  `edit_allowed`
               )
               VALUES (
                  :termid,
                  :type,
                  "1"
               )
               ');
               $Tess_db->bind($insert_term, ":termid", $term_id);
               $Tess_db->bind($insert_term, ":termid", gettype($text_value));
               if($Tess_db->execute($insert_term)){
                  $terms_id = $Tess_db->query('SELECT LAST_INSERT_ID() as lastid FROM dictionary_term');
                  $term_id = $Tess_db->single($terms_id)->lastid;
               }
            }
            $insert_array = array();
            $insert_array[] = (is_array($text_value) ? serialize($text_value) : $text_value);
            $insert_array[] = $language;
            $insert_array[] = $term_id;
         }

      }
      $insert_query = $Tess_db->query('
      INSERT INTO `dictionary` (
      `text_value`,
      `language`,
      `term_id`
      )
      VALUES
      (:type,:lang,:term)');

      $Tess_db->bind($insert_query, ":type", $insert_array[0]);
      $Tess_db->bind($insert_query, ":lang", $insert_array[1]);
      $Tess_db->bind($insert_query, ":term", $insert_array[2]);

      if($Tess_db->execute($insert_query)){
         return $query;
      }else{
         return false;
      }
   }

   //geturl
   function getUrl($id, $lang = "", $only_slug = false){
      global $l, $language;
      //Initiate db
      $language = new Lang();
      $lang = ($lang ? $lang : $language->lang);
      $db = new Database();

      $prefix_lang = '/'.(!empty($lang) ? ($lang == 'fr' ? 'fr' : 'en') : $language);
      $suffix = !empty($lang) ? ($lang == 'fr' ? '' : '_en') : $l;


      try {
         $r = $db->query("SELECT tbl_custom_urls.url_id".$suffix." ,
                tbl_pages.parent,
                tbl_pages.root,
                tbl_pages.id
                FROM tbl_custom_urls
                LEFT JOIN tbl_pages
                ON tbl_custom_urls.page_id = tbl_pages.id
                WHERE tbl_custom_urls.page_id = :id
                AND  tbl_custom_urls.url_id".$suffix." is not null
                ORDER BY tbl_custom_urls.date_creation DESC LIMIT 1"
         );
         $db->bind($r,':id', $id);
         $r = $db->single($r);

         if(!$only_slug){
            if($r){
               if(!empty($r->{'url_id'.$suffix}) && $r->{'parent'} != '1')
                  return $prefix_lang.'/'.getUrl($r->{'parent'},$lang, 'recursive').$r->{'url_id'.$suffix}.'/';
               else if(!empty($r->{'url_id'.$suffix}) && $r->{'parent'} == '1')
                  return $prefix_lang.'/'.$r->{'url_id'.$suffix}.'/';
               else
                  return $prefix_lang.'/'.getUrl($r->{'parent'}, $lang, 'recursive').$id.'/';
            }
            else {
               $parent = $db->query("SELECT parent FROM tbl_pages WHERE id = :id");
               $db->bind($parent, ':id', $id);
               $parent = $db->single($parent);
               if($parent)
                  return $prefix_lang.'/'.getUrl($parent->parent, $lang, 'recursive').$id.'/';
               else
                  return $prefix_lang.'/'.$id.'/';
            }
         }
         else {
            if($r){
               if(!empty($r->{'url_id'.$suffix}))
                  return ($only_slug == 'recursive' ? getUrl($r->parent, $lang, 'recursive') : '').$r->{'url_id'.$suffix}.'/';
               else
                  return ($only_slug == 'recursive' ? getUrl($r->parent, $lang, 'recursive') : '').$id.'/';
             }
             else{
               if($id) {
                  $parent = $db->query("SELECT parent FROM tbl_pages WHERE id = :id ");
                  $db->bind($parent,':id', $id);
                  $parent = $db->resultSet($parent);
                  if($parent[0]->parent)
                     return getUrl($parent[0]->parent, $lang, 'recursive');
               }
            }
         }
      }catch(PDOException $e){
         return false;
      }
   }

   //check connection
   function checkConnectionInfo($client=""){

      $__cred = array();

      require dirname(__DIR__)."/config/credentials.php";
      if(isset($__cred[$client])){
         return $__cred[$client];
      }else{
         return false;
      }

   }
