<?php
    session_name('session_app_2');

    // session_set_cookie_params(60*60*24*5, "/administration/");
    // ini_set("session.gc_maxlifetime", "3600");
    // ini_set("session.cookie_lifetime", "3600");

    session_set_cookie_params(0, "/administration/");
    ini_set("session.gc_maxlifetime", "0");
    ini_set("session.cookie_lifetime", "0");

    session_start();
    require_once "libraries/Database.php";
    /*
     * Load config
     */ 
        require_once "config/config.php"; 
    include_once APPROOT."/helpers/function.php";

    // Autoload Core librairies
    spl_autoload_register(
        function($classname){
            if(strpos($classname, "Modelss") !== false || strpos($classname, "PHPMailer") !== false){

            }else{

                require_once "libraries/". $classname .".php";
            }
        }
    );
?>
