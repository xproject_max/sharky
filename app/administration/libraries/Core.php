<?php
    /*
     * App core class
     * create url & loads class
     */
    class Core extends Lang {
        protected $currentController = "Pages";
        protected $currentMethod = "index";
        public $url = "";
        protected $params = [];

        public function __construct()
        {


            $this->currentController = (isset($_SESSION['user_id']) ? "Pages" : "Tess");
            $this->currentMethod = (isset($_SESSION['user_id']) ? "index" : "login");
            $lang = $this->lang;
 
            $url = $this->getUrl();

            //GetRealPathMethod
            if(isset($_GET['url']) && $_GET['url'] !== "tess/logout")
                $url = $this->getRealPathUrl($url, $lang);
            if(!isset($url[0])){
                $url[0] = "";
            }
            // Look in controllers for first value
            if(file_exists(dirname(__DIR__)."/controllers/".ucwords($url[0]).".php")){
                $this->currentController = ucwords($url[0]);
                unset($url[0]);
            }

            //Require the controller
            require_once dirname(__DIR__)."/controllers/".$this->currentController . ".php";

            //Instantiate controller class
            $this->currentController = new $this->currentController;


            //Check for second part of url
            if(isset($url[1])){
                //Check to see if method exists in controller
                if(method_exists($this->currentController, $url[1])){
                    $this->currentMethod = $url[1];
                    unset($url[1]);
                }
            }
            // Get params
            $this->params = $url ? array_values($url) : [];

            // Call a callback with array of params
            call_user_func_array([$this->currentController, $this->currentMethod], $this->params);

        }
        public function getRealPathUrl($url){
            $db = new Database();
            $url = implode("/",$url);
            $url_end = [];
            $result = [];
            $l = "";
            if($this->getLang() == "en"){
                $l = "_en";
            }

            try{
                $query_url = $db->query("SELECT apppath FROM tbl_custom_admin_urls WHERE url_id".$l." = :url");
                $query_url->bindValue(":url", $url);
                $query_url = $db->resultSet($query_url);
                if(!empty($query_url[0]))
                    $result = explode("/", $query_url[0]->apppath);
                    $url_end = $result;
                    return $url_end;
            }catch(PDOException $e){
                return "false".$e->getMessage();
            }
        }
        public function getUrl(){
            $url = [];
            if(isset($_GET['url'])){
                $url = rtrim($_GET['url'], "/");
                $url = filter_var($url, FILTER_SANITIZE_URL);
                $url = explode("/", $url);
            }
            return $url;
        }

        public function is_dictionary_initialized(){
            if(empty($_LANG)){
                //need import
                return false;
            }

            return true;
        }


    }
