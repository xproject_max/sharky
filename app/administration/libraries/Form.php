<?php
  /*
   * Form class for creating form faster and more efficient.
  */
    class Form extends Core{

        public function __construct()
        {
        }

        public function input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="",$note_position="", $note="", $attributes="", $idjoin=""){
            $type = strtolower($type);
            switch($type){

            //text
            case "text":
            //password
            case "password":
            //hidden
            case "hidden":
            //date
            case "date":
            //tel
            case "tel":
            //email
            case "email":
            //color
            case "color":
                return '
                <div class="form-group bmd-form-group">
                <label for="'.$name.'" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'  '.($note_position=="titre" ?  '( '.$note.' )' : '').'

                </label>
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').' <input type="'.$type.'" class="'.$class.' form-control" '.$attributes.' value="'.$value.'" name="'.$name.'" id="'.$name.'" />
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                </div>
                ';
            break;
            //Datepicker
            case "datepicker":
                return '
                <div class="form-group bmd-form-group">
                <label for="'.$name.'" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                </label>
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').' <input type="'.$type.'" id="datetimepicker" class="datetimepicker '.$class.' form-control" '.$attributes.' value="'.$value.'" name="'.$name.'" id="'.$name.'" />
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                </div>
                ';
            break;


            //textarea
            case "textarea":
                return '
                <div class="form-group bmd-form-group">
                <label for="'.$name.'" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                </label>
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').'  <textarea class="form-control '.$class.'" cols="80" id="'.$name.'" '.$attributes.'  name="'.$name.'" rows="4">'.$value.'</textarea>
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                </div>';
            break;


            //wysiwyg
            case "wysiwyg":
                return '
                <div class="form-group bmd-form-group">
                <label for="'.$name.'" style="position: initial;" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                </label>
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').'  <textarea class="ckeditor '.$class.'" cols="80" id="'.$name.'" '.$attributes.'  name="'.$name.'" rows="30">'.$value.'</textarea>
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                </div>';
            break;


            //upload_img
            case "upload image":
                return '
                <div class="form-group bmd-form-group card-wizard">
                <label for="'.$name.'" style="position: initial;" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                </label>
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').'
                <div id="waitingImg_'.$name.'"></div>
                '.
                ($value == "" ? '
                <div class="picture-container">
                <div class="picture">
                <img src="'.URLIMG.'default-avatar.png" class="picture-src The_wizard_preview_'.$name.'" title="">
                <input type="file"  id-page="'.$options.'" parent="The_wizard_preview_'.$name.'" class="The_wizard_images" '.$attributes.' name="'.$name.'">
                <input type="hidden" class="'.$name.'" value=""  />
                </div>
                </div>'
                :
                '
                <div class="picture-container">
                <div class="picture" >
                <div class="table-cell">
                  <img src="'.(file_exists(URLIMG.$value) ? URLIMG.$value : URLGIMG.$value).'" class="picture-src The_wizard_preview_'.$name.'" title="">
                </div>
                <input type="file"  id-page="'.$options.'" class="The_wizard_images" parent="The_wizard_preview_'.$name.'" '.$attributes.' name="'.$name.'">
                <input type="hidden" class="'.$name.'" value=""  />
                </div>
                </div>
                ').'
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                <br>'.$value.'
                </div>';
            break;


            //upload_other
            case "upload video":
            case "upload autre":
                return '
                <div class="form-group bmd-form-group card-wizard">
                <label for="'.$name.'" style="position: initial;" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                </label>
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').'
                <div id="waitingImg_'.$name.'"></div>
                '.
                ($value == "" ? '
                <div class="picture-container">
                <div class="picture">
                <img src="'.URLIMG.'default-avatar.png" class="picture-src The_wizard_preview_'.$name.'" title="">
                <input type="file"  id-page="'.$options.'" parent="The_wizard_preview_'.$name.'" class="The_wizard_images" '.$attributes.' name="'.$name.'">
                <input type="hidden" class="'.$name.'" value=""  />
                </div>
                </div>'
                :
                '
                <div class="picture-container">
                <div class="picture" >
                <div class="table-cell">
                  <img src="'.URLIMG.$value.'" class="picture-src The_wizard_preview_'.$name.'" title="">
                </div>
                <input type="file"  id-page="'.$options.'" class="The_wizard_images" parent="The_wizard_preview_'.$name.'" '.$attributes.' name="'.$name.'">
                <input type="hidden" class="'.$name.'" value=""  />
                </div>
                </div>
                ').'
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                <br>'.$value.'
                </div>';
            break;


            //joint_pages
            case "jointure page": 
                $nbcol = 3;
				$i = 0;
                $r= "";
                $select = '<table class="center_check"><tr><th>'.$label.'</th></tr>';
				$db = new Database();
                foreach($options as $key => $value){
			
					$valeur1 = $value->id;
					$valeur2 = $value->{'title'};
					if ($i % $nbcol == 0)
						$select .= '<tr>';

					if (isset($valeur1) && !is_null($valeur1) && !is_object($valeur1)) { 
						$check = $db->query("SELECT pi.* FROM tbl_pages_inputs pi where pi.value like CONCAT('%-',:valeur1, '-%') and pi.id = :id ");
						$db->bind($check,":valeur1", $valeur1);
						$db->bind($check,":id", $idjoin);
						$db->execute($check);
						
						$res = $db->rowCount($check);
						if ($res > 0) {
						$r = "checked";
						} else {
						$r= "";
						}
						$select .= '
						<td >
						<div class="form-check">
							<label  >
								<input class="form-check-input" type="checkbox" name="'.html_entity_decode($name).'[]" value="' . $valeur1 .
								'" ' . $r . '>' . $valeur2 . '
								<span class="form-check-sign">
									<span class="check"></span>
								</span>
							</label>
						</div>
						</td>';
					}



					if ($i % $nbcol == ($nbcol - 1))
						$select .= '</tr>';

                }

                $select .= '</table>';  
                return $select;
            break;

            //joint_pages
            case "jointure product": 
                $nbcol = 3;
				$i = 0;
                $r= "";
                $select = '<table class="center_check"><tr><th>'.$label.'</th></tr>';
				$db = new Database();
                foreach($options as $key => $value){
			
					$valeur1 = $value->id;
					$valeur2 = $value->{'title'};
					if ($i % $nbcol == 0)
						$select .= '<tr>';

					if (isset($valeur1) && !is_null($valeur1) && !is_object($valeur1)) { 
						$check = $db->query("SELECT pi.* FROM tbl_products_inputs pi where pi.value like CONCAT('%-',:valeur1, '-%') and pi.id = :id ");
						$db->bind($check,":valeur1", $valeur1);
						$db->bind($check,":id", $idjoin);
						$db->execute($check);
						
						$res = $db->rowCount($check);
						if ($res > 0) {
						$r = "checked";
						} else {
						$r= "";
						}
						$select .= '
						<td >
						<div class="form-check">
							<label  >
								<input class="form-check-input" type="checkbox" name="'.html_entity_decode($name).'[]" value="' . $valeur1 .
								'" ' . $r . '>' . $valeur2 . '
								<span class="form-check-sign">
									<span class="check"></span>
								</span>
							</label>
						</div>
						</td>';
					}



					if ($i % $nbcol == ($nbcol - 1))
						$select .= '</tr>';
					
					$i++;

                }

                $select .= '</table>';  
                return $select;
            break;


            //radio
            case "radio":

                $radios ='
                <div class="form-group bmd-form-group">
                <label for="'.$name.'" style="position: initial;" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                </label>
                <div>
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').''.$options.'
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                </div>
                </div>';

                return $radios;

            break;

            //Catégories
            case "cat&eacute;gorie":
            //checkboxes
            case "checkbox":
                $checkboxes = '
                <div class="form-group bmd-form-group">
                <label for="'.$name.'" style="position: initial;" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                </label>
                <div>
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').''.$options.'
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                </div>
                </div>';
                return  $checkboxes.'<input type="hidden" name="' .  html_entity_decode($name) .'[]" checked>';
            break;

            //Nice toggle
            case "toggle":
                $toggle = '
                <div class="form-group bmd-form-group">
                <label for="'.$name.'" style="position: initial;" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                </label>
                <div class="togglebutton">
                <label onclick="$(this).find(`input`).attr(`checked`);">
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').'<input name="'.$name.'" value="0" type="hidden" />
                <input name="'.$name.'" value="1" '.$attributes.'  type="checkbox" ><span class="toggle"></span>
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                </label>
                </div>
                </div>';
                return  $toggle;
            break;

            //Catégories radios
            case "cat&eacute;gorie radio":
                $categ_radios = '
                <div class="form-group bmd-form-group">
                <label for="'.$name.'" style="position: initial;" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                </label>
                <div>
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').''.$options.'
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                </div>
                </div>';
                return  $categ_radios;
            break;

            //select
            case "select":
                $select ='
                <div class="dropdown bootstrap-select">
                <label for="'.$name.'" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                </label>
                <select class="selectpicker" value="0"  name="'.$name.'" data-size="7" '.$attributes.' data-style="btn bgblack btn-info '.$class.' " title="'.$label.'" tabindex="-98">
                <option  value="0" disabled="disabled" selected="">'.$label.'</option>
                '.$options.'
                </select>
                </div>
                ';
                return $select;
            break;
        }
    }
}
