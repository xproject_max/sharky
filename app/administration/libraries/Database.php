<?php
	/*
	 * PDO Database Class
	 * Connect to the database
	 * Create prepared statements
	 * Bind values
	 * Return rows and results
	 */
	class Database {
		private $host = DB_HOST;
		private $user = DB_USER;
		private $pass = DB_PASS;
		private $dbname = DB_NAME;
		private static $_instance; //The single instance
		private static $dbh;
		private $stmt;
		private $error;
		
		
		public static function getInstance() {
			if(!self::$_instance) { // If no instance then make one
				self::$_instance = self::$dbh; 
			}
			return self::$_instance;
		}
		
		// Magic method clone is for prevent duplication of connection
		private function __clone() { }

		public function getConnection() { 
				return self::getInstance();
		}
		
		
		public function __construct()
		{
			// var_dump("ouverture connexion");
			$dsn = "mysql:host=". $this->host ."; dbname=" . $this->dbname;
			$options = array(
				PDO::ATTR_PERSISTENT => TRUE,
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
			);
			 
			
			if(null === self::$dbh){
				// var_dump("connexion inexistante.. creation");
				self::$dbh = new PDO($dsn, $this->user, $this->pass);
				self::$dbh->exec("SET NAMES utf8");
				date_default_timezone_set('America/Montreal');
				self::$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}else{ 
				// var_dump("connexion exist");
				return $this->getConnection();
			}
		}
		
		// Prepared statements
		public function query($sql){
			try{
				return self::$dbh->prepare($sql);
			}catch(PDOException $e){
				return false;
			}
		}
		//Bind values
		public function bind($stmt,$param, $value, $type=null){
			if(is_null($type)){
				switch(true){
					case is_int($value):
						$type = PDO::PARAM_INT;
						break;
					case is_bool($value):
						$type = PDO::PARAM_BOOL;
						break;
					case is_null($value):
						$type = PDO::PARAM_NULL;
						break;
					default:
						$type = PDO::PARAM_STR;
				}
			}
			if($type == "COLUMNS"){
				return $stmt->bindColumn($param, $value);
			}
			return $stmt->bindValue($param, $value, $type);
			
		}
		
		//  Execute prepared statement
		public function execute($stmt, $array=[]){
			try{
				return $stmt->execute((!empty($array) ? $array : null));
			}catch(PDOException $e){
				
				return false;
			}
		}
		
		// Get results set as array of objects
		public function resultSet($stmt){
			$stmt->execute();
			return $stmt->fetchAll(PDO::FETCH_OBJ);
		}
		
		// Get single record as object
		public function single($stmt){
			$stmt->execute();
			return $stmt->fetch(PDO::FETCH_OBJ);
		}
		// Get single record as object
		public function fetch($stmt, $type=""){
			if($type == "array")
				return $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $stmt->fetch(PDO::FETCH_OBJ);
		}
		
		//Get row count
		public function rowCount($stmt){
			try{
				$stmt->execute();
				return $stmt->rowCount();
			}catch(PDOException $e){
				return false;
			}
		}
		
		//Get row count
		public function errorInfo($stmt){
			return $stmt->errorInfo();
		}
	}
