<?php
    /*
     * Uploading any files in corresponding folders
     */
    class Uploading extends Core{
        public function __construct()
        {
        }
		
		public function upload($post, $files=[]){
			
			$db = new Database(); 
			$upload_dir = dirname(dirname(APPROOT))."/public/uploads/".COMPANYNAME."/";
			$allowed_ext = APPEXT;
			if(!is_dir($upload_dir)) {
				mkdir($upload_dir);
			}
			foreach($files as $key => $val){
				if(isset($_POST['current_img'])){
					$key = $_POST['current_img'];
				}
				// error_log($files[$key]['size']." et tmp: ".$files[$key]['tmp_name'], 0);
				// if ($files[$key]['size'] != "0" && $files[$key]['tmp_name'] != '') {
					try{
						if(isset($post['id_page']))
							$id = !empty($post['id_page']) ? $post['id_page'] : (isset($post['parent']) && $post['parent'] !== "" ? $post['parent'] : 1);
						
						if(isset($post['id_product']))
							$id = !empty($post['id_product']) && $post['id_product'] !== "" ? $post['id_product'] : (isset($post['parent']) && $post['parent'] !== "" ? $post['parent'] : 1);
						
						if(isset($post['id_blog']))
							$id = !empty($post['id_blog']) && $post['id_blog'] !== "" ? $post['id_blog'] : (isset($post['parent']) && $post['parent'] !== "" ? $post['parent'] : 1);
						
						if(isset($post['id_contest']))
							$id = !empty($post['id_contest']) && $post['id_contest'] !== "" ? $post['id_contest'] : (isset($post['parent']) && $post['parent'] !== "" ? $post['parent'] : 1);
						
						if(isset($post['id_promotion']))
							$id = !empty($post['id_promotion']) && $post['id_promotion'] !== "" ? $post['id_promotion'] : (isset($post['parent']) && $post['parent'] !== "" ? $post['parent'] : 1);
						
						if(isset($post['id_tess_users']))
							$id = !empty($post['id_tess_users']) && $post['id_tess_users'] !== "" ? $post['id_tess_users'] : (isset($post['parent']) && $post['parent'] !== "" ? $post['parent'] : 1);
						
						if(isset($post['id_user']))
							$id = !empty($post['id_user']) && $post['id_user'] !== "" ? $post['id_user'] : (isset($post['parent']) && $post['parent'] !== "" ? $post['parent'] : 1);
						
						if(isset($post['id_company']))
							$id = !empty($post['id_company']) && $post['id_company'] !== "" ? $post['id_company'] : (isset($post['parent']) && $post['parent'] !== "" ? $post['parent'] : 1);
						
						if(isset($post['id_edit_image']))
							$id = !empty($post['id_edit_image']) && $post['id_edit_image'] !== "" ? $post['id_edit_image'] : (isset($post['parent']) && $post['parent'] !== "" ? $post['parent'] : 1);
						
						/* Get the right folder*/
						
						if(isset($post['id_page']) && isset($post['id_product']) && isset($post['id_blog'])){
							$query_folder = $db->query('SELECT t.title FROM tbl_pages p LEFT JOIN tess_controller t ON t.id = p.type WHERE p.id = :id');
						}else if(isset($post['id_product']) && $post['id_product'] !== ""){
							$query_folder = $db->query('SELECT t.title FROM tbl_products p LEFT JOIN tess_controller t ON t.id = p.type WHERE p.id = :id');
						}else if(isset($post['id_blog']) && $post['id_blog'] !== ""){
							$query_folder = $db->query('SELECT t.title FROM tbl_blogs p LEFT JOIN tess_controller t ON t.id = p.type WHERE p.id = :id');
						}else if(isset($post['id_contest']) && $post['id_contest'] !== ""){
							$query_folder = $db->query('SELECT t.title FROM tbl_contests p LEFT JOIN tess_controller t ON t.id = p.type WHERE p.id = :id');
						}else if(isset($post['id_promotion']) && $post['id_promotion'] !== ""){
							$query_folder = $db->query('SELECT t.title FROM tbl_promotions p LEFT JOIN tess_controller t ON t.id = p.type WHERE p.id = :id');
						}else if(isset($post['id_tess_users']) && $post['id_tess_users'] !== ""){
							$query_folder = $db->query('SELECT t.pseudo FROM tess_members t WHERE t.id = :id');
							/* Create that folder if not exists */
							if(!is_dir($upload_dir."Tess users/")) {
								mkdir($upload_dir."Tess users/");
							}
						}else if(isset($post['id_company']) && $post['id_company'] !== ""){
							$query_folder = $db->query('SELECT t.username FROM tbl_companies t WHERE t.id = :id');
							/* Create that folder if not exists */
							if(!is_dir($upload_dir."companies/")) {
								mkdir($upload_dir."companies/");
							}
						}else if(isset($post['id_user']) && $post['id_user'] !== ""){
							$query_folder = $db->query('SELECT t.username FROM tbl_users t WHERE t.id = :id');
							/* Create that folder if not exists */
							if(!is_dir($upload_dir."users/")) {
								mkdir($upload_dir."users/");
							}
						}else if(isset($post['id_edit_image']) && $post['id_edit_image'] !== ""){
							$getcateg = $db->query("SELECT replace('-','',categ) as cat FROM tbl_pictures WHERE id = :id");
							$db->bind($getcateg,":id", $post['id_edit_image']);
							$id = $this->db->single($getcateg)->cat;
							$query_folder = $db->query('SELECT tbc.name FROM tbl_pictures_categories tbc WHERE tbc.id = :id');
							
							/* Create that folder if not exists */
							if(!is_dir($upload_dir."edit/")) {
								mkdir($upload_dir."edit/");
							}
						}else{
							$query_folder = $db->query('SELECT title FROM tess_controller WHERE id = 1');
						}
						$db->bind($query_folder,':id', $id);
						$query_folder = $db->single($query_folder);
						 
						if(isset($post['id_tess_users']) && $post['id_tess_users'] !== ""){
							if(!is_dir($upload_dir."Tess users/".$query_folder->pseudo ."/")) {
								mkdir($upload_dir."Tess users/".$query_folder->pseudo ."/");
							}
							$path = "Tess users/".$query_folder->pseudo ."/";
							$value = $query_folder->pseudo."-".rand(0,10000)."-".($files[$key]['name']);
						}
						else if(isset($post['id_user']) && $post['id_user'] !== ""){
							if(!is_dir($upload_dir."users/".$query_folder->username ."/")) {
								mkdir($upload_dir."users/".$query_folder->username ."/");
							}
							$path = "users/".$query_folder->username ."/";
							$value = $query_folder->username."-".rand(0,10000)."-".($files[$key]['name']);
						}
						else if(isset($post['id_company']) && $post['id_company'] !== ""){
							if(!is_dir($upload_dir."companies/".$query_folder->username ."/")) {
								mkdir($upload_dir."companies/".$query_folder->username ."/");
							}
							$path = "companies/".$query_folder->username ."/";
							$value = $query_folder->username."-".rand(0,10000)."-".($files[$key]['name']);
						}
						else if(isset($post['id_edit_image']) && $post['id_edit_image'] !== ""){
							if(!is_dir($upload_dir."edit/".$query_folder->name ."/")) {
								mkdir($upload_dir."edit/".$query_folder->name ."/");
							}
							$path = "companies/".$query_folder->username ."/";
							$value = $query_folder->username."-".rand(0,10000)."-".($files[$key]['name']);
						}else{
							/* Create that folder if not exists */
							if(!is_dir($upload_dir.$query_folder->title ."/")) {
								mkdir($upload_dir.$query_folder->title ."/");
							}
							$path = $query_folder->title ."/";
							$value = $query_folder->title."-".rand(0,10000)."-".($files[$key]['name']);
						} 
						
						/* Get extension */
						$ext = strtolower(pathinfo($value, PATHINFO_EXTENSION));
						$value= str_replace(" ", "-", rtrim($value));
						$value= str_replace("(", "-", rtrim($value));
						$value= str_replace(")", "-", rtrim($value));
						/* if extension allowed */ 
						if(in_array($ext, $allowed_ext)){
							if(move_uploaded_file($files[$key]['tmp_name'], $upload_dir.$path . $value)){
								
								 
								return str_replace(" ", "%20", ltrim($path)) . $value;
							}
						}
					}catch(PDOException $e){
						return false;
					}
					exit();
				// }	 
				
			}
		}
	}
?>