<?php
    /*
     * Base Controller
     * This loads the models and views
     */
    class Controller extends Core {
        public  function __construct(){
            $lang = $this->lang;
        }
        //Load model
        public function model($model){
            //Require model file
            require_once dirname(__DIR__)."/models/".$model.".php";

            // Instantiate model
            return new $model();
        }

        // Load view
        public function view($view, $data = []) {
            global $lang, $l, $language, $_LANG;
            // Check view file
            if(!isset($_SESSION['user_id']))
                $view = "tess/login";
            if(file_exists(dirname(__DIR__)."/views/".$view.".php")) {
                require_once dirname(__DIR__) . "/views/" . $view . ".php";
            }else{
                die("file doesnt exists");
            }

        }
    }
