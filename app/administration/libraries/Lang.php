<?php
    /*
     * Form class for creating form faster and more efficient.
     */
    class Lang{
		public $lang = "";
        public function __construct()
        {

			require_once dirname(__DIR__)."/config/lang.php";
			$this->lang = $this->getLang();
        }
		public function getLang(){

            $l = "";
            if (isset($_GET['lang'])){

                $_SESSION['lang'] = $_GET['lang'];
                $language = $_SESSION['lang'];

            }
            if (isset($_SESSION['lang']) && $_SESSION['lang']=="" and $_COOKIE['lang']==""){

                $_SESSION['lang'] = $language;
                setcookie('lang', $language, (time() + 3600*12));
                $language = $_SESSION['lang'];

            }elseif(isset($_SESSION['lang']) && $_SESSION['lang']!=""){
				
				$language = $_SESSION['lang'];
				
			}elseif(isset($_COOKIE['lang']) && $_COOKIE['lang']!=""){
				
				$language = $_COOKIE['lang'];
				$_SESSION['lang'] = $_COOKIE['lang'];
			
            }else{
                // echo "<h1>Language initiation problem</h1>";
                $language="fr";
                $_SESSION['lang'] = $language;
            }


            return $language;

        }
	}
?>
