<?php
    class Product {
        private $db;

        public function __construct()
        {
            $this->db = new Database();
        } 
        public function getPage($id){
            try{
                $getPage = $this->db->query("SELECT * FROM tbl_pages_admin WHERE id = " .$id);
                return $this->db->resultSet($getPage);
            }catch(PDOException $e){
				return false;
			}
        }
        
        public function checkIfExistJoinInputs($key, $fkproducts){
            $exist = $this->db->query("SELECT * FROM tbl_products_inputs WHERE type = :key AND fk_products = :fkproducts");
            $this->db->bind($exist, ":key", $key);
            $this->db->bind($exist, ":fkproducts", $fkproducts);
            if($this->db->single($exist)){
                return true;
            }else{
                return false;
            }
        }
        public function checkIfUrlExist($product_id){
            $exist = $this->db->query("SELECT * FROM tbl_products_urls WHERE product_id = :product_id");
            $this->db->bind($exist, ":product_id", $product_id);
            if($this->db->single($exist)){
                return true;
            }else{
                return false;
            }
        }
        public function checkIfModulesExist($inputid){
            $exist = $this->db->query("SELECT * FROM tess_controller WHERE id = :inputid");
            $this->db->bind($exist, ":inputid", $inputid);
            if($this->db->single($exist)){
                return true;
            }else{
                return false;
            }
        }
        public function getProductWebsite($id){
            try{
                $query = $this->db->query("SELECT * FROM tbl_products WHERE id = " .$id);
                return $this->db->resultSet($query);
            }catch(PDOException $e){
				return false;
			}
        }
        public function getAll(){
            try{
                $query_get_all = $this->db->query("SELECT tbl_products.*, tess_controller.add, tess_controller.mod, tess_controller.del, tess_controller.dtype FROM tbl_products LEFT JOIN tess_controller ON tbl_products.type = tess_controller.id WHERE parent = 0");
                return $this->db->resultSet($query_get_all);
            }catch(PDOException $e){
				return false;
			}
        }
        public function getAllCustoms($id){
            try{
                $query_get_all = $this->db->query("SELECT * FROM tbl_products_inputs WHERE fk_products = :id");
                $this->db->bind($query_get_all, ":id", $id); 
                return $this->db->resultSet($query_get_all);
            }catch(PDOException $e){
				return false;
			}
        }

        public function getChildList($id, $level){

            global $Tess_db, $linv, $Tess, $Functions, $install_page_order, $_SESSION, $l;

            require_once APPROOT.'/helpers/function.php';
            $lang = new Lang();
            $l = "";
            if($lang->lang == "en")
                $l="_en";

            try{
                $query = $this->db->query("SELECT * FROM tbl_products WHERE parent = :id ORDER BY `order` ASC");
                $this->db->bind($query,":id",$id);
                $child_bool = false;
                $childMenu ='';
                $key=0;
                $currents = $this->db->resultSet($query);
                $custom = array();
                foreach($currents as $current){
                    
                    $custom['prix'] = "";
                    $custom['Quantité'] = "";
                    $custom['Compagnies'] = "";
                    $custom_inputs = $this->getAllCustoms($current->id);
                   
                         
                    foreach($custom_inputs as $array){
                        $get_type = $this->db->query("SELECT * FROM inputs WHERE id = :id");
                        $this->db->bind($get_type, ":id", $array->type);
                        $key = $this->db->single($get_type);
                        
                        if($key->title == "Compagnies"){
                            // $query_title_company = $this->db->query("SELECT cm.company_name FROM tbl_companies t JOIN tbl_companies_meta cm ON cm.id = t.fk_companies_meta WHERE t.id = :id");
                            // $this->db->bind($query_title_company, ":id", $array->value);
                            // $array->value = $this->db->single($query_title_company)->company_name;
                            $array->value = "NA";
                        }
                        
                        $custom[$key->title] = $array->value;
                        
                    }                    
                    $childMenu.='<tr level="'.($level+1).'" mid="'.$current->id.'" url="'.URLROOT.getUrl($current->id).'" url_linv="'.URLROOT.getUrl($current->id, $linv).'" class="firstchild  Level_'.($level+1).'">';
                    
                    $child_check = $this->db->query("SELECT * FROM tbl_products WHERE parent = :id ");
                    $this->db->bind($child_check,":id", $current->id);
                    $child_check = $this->db->single($child_check);
                    if(!empty($child_check)){
                       $child_bool = true;
                    } 
                    
                    $i = 0;
                    $level_i ="";
                    $level_i .= '<img border="0"  src="images/icon1.png"><img border="0"  src="images/icon4.png">';
					//Déroulement
                    $childMenu.='<td style="width:76px">';

                            if($child_bool){
                                if(isset($_SESSION['pages-actives']) && $_SESSION['pages-actives'] == "actif"){
                                    $childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler-product page active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                                    $look_for_child = true;
                                } else if(isset($_SESSION['pages-actives']) && $_SESSION['pages-actives'] == "inactif") {
                                    $childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler-product page" style="cursor:pointer;" data-tooltip="Ouvrir la catégorie"><button type="button" rel="tooltip" class="btn btn-warning" data-original-title="" title="">
                                            <i class="fad fa-chevron-square-up arrows_menu"></i>
                                        </button></a>';
                                    $look_for_child = false;
                                } else if(3 <= $level) {
                                    $childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler-product page" style="cursor:pointer;" data-tooltip="Ouvrir la catégorie">
                                            <i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                                    $look_for_child = false;
                                } else if(3  > $level) {
                                    $childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler-product page active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                                    $look_for_child = true;
                                }else {
                                    $childMenu.= 'noob';
                                }
                            }
                            $child_bool = false;
                    $childMenu.= '</td> ';
                    $childMenu.= '<td level="'.($level+1).'"   >';
                    $childMenu.= '<input type="text" name="order[]" style="text-align:center;width:30px !important;background:black;color:white;" value="'.$current->order.'"><input type="hidden" name="id[]" style="width:15px !important;" value="'.$current->id.'"> ';

                    $bold=true;


                    $rights = $this->db->query('SELECT * FROM tess_controller WHERE id = :rights ');
                    $this->db->bind($rights,':rights', $current->type);
                    $rights = $this->db->resultSet($rights);
                    if(isset($rights[0]))
                        $rights = $rights[0];

                    $childMenu.='</td>
                    <td>
                        <div style="    height: 40px;
                        width: 40px;
                        display: table;
                        background: rgb(0, 0, 0);
                        overflow: hidden;
                        border-radius: 50%;
                        /* border: 1px solid rgba(0,0,0,0.2); */
                        margin: 0 auto;">
                            <div style="display:table-cell;vertical-align:middle">
                                <img src="'.URLIMG.($current->image ? $current->image : "default-avatar.png").'" width="40px"  />
                            </div>
                        </div> 
                    </td>
                    <td  class="title_table_list">';

                    if ($_SESSION['status'] <= $rights->mod or $_SESSION['status']=="1")
                      $childMenu.= '<div class="subcat">'.file_get_contents('../images/sub.svg').'</div><a style="line-height: 1.3" data-toggle="tooltip" data-placement="top"  data-original-title="ID: ' . $current->id . ' & Parent: ' . $current->parent . '"  data-description="'.$rights->desc.'" href="index.php?cp=page&mod=addmod&id=' . $current->id . '&parent='.$current->parent.'">';
                    $childMenu.= $bold ? "<b style='font-weight:800;'>" : "";
                    if($current->{'title'.$l} != ''){
                        $childMenu .= $current->{'title'.$l};
                    }
                    else{
                        $childMenu .= '(Sans titre)';
                    }
                    $childMenu.= $bold ? "</b>" : "";
                    if ($_SESSION['status'] <= $rights->mod or $_SESSION['status']=="1")
                      $childMenu.= '</a>';

                    $childMenu.='</td>';
                    $childMenu.='</td>';
                    
                    $childMenu.= '
                                <td>'.$custom['prix'].'</td>
                                <td>'.$custom['Quantité'].'</td>
                                <td>'.$custom['Compagnies'].'</td>
                                <td class="td-actions text-right">
                                    <div class="togglebutton">
                                        <label onclick="$(this).find(`input`).attr(`checked`);">
                                            <input name="online[]['.$current->id.']" value="0" type="hidden" />
                                            <input name="online[]['.$current->id.']" value="1" type="checkbox" '.($current->online == 1 ? 'checked=""' : '').'><span class="toggle"></span>
                                        </label>
                                    </div>
                                </td>
                                ';
                    $childMenu.= '<td class="td-actions text-right"   >';
                            $childMenu.= '<span class="td-actions text-right">
                                    ';
                            if ($_SESSION['status'] <= $rights->add){
                                if($rights->dtype != "") {
                                    $childMenu .=  '<a class="btn btn-info add_child" data-tooltip="Ajouter" dtype="'.$rights->dtype.'" parent="'.$current->id.'" href="index.php?cp=page&amp;mod=addmod&amp;ajout=1&amp;parent='.$current->id.'&amp;templates=""><i class="fas fa-plus-square"></i></a> ';
                                }
                            }
                            if ($_SESSION['status'] <= $rights->mod or $_SESSION['status'] == "1") {
                            $childMenu.= '<a class="btn btn-warning" data-toggle="tooltip" data-placement="top"  data-original-title="Type: ' . $current->type . '" title="Type: ' . $current->type . '" href="index.php?cp=page&mod=addmod&id=' . $current->id . '&parent='.$current->parent.'"><i class="fas fa-pen-square"></i><div class="ripple-container"></div></a> ';
                            }
                            if ($_SESSION['status'] <= $rights->del) {
                                $childMenu.= '<a id="'.$current->id.'"  class="btn btn-danger delete-product"data-tooltip="Supprimer" ><i class="fas fa-times-square"></i></a> ';
                            }
                            $childMenu.='
                                    </span>';

                    $childMenu.='</td>'."\n";
                    $childMenu.= ' ';
                    if($child_bool&&$look_for_child)

                    $childMenu.= '</td>'."\n";
                    $childMenu.="</tr>";
                    $key++;
                }
                $_SESSION['pages-actives'][$id] = "actif";
                echo $childMenu;
            }catch(PDOException $e){
				return false;
			}

        }

        public function getMenu(){
            global $lang, $l;
            $lang = new Lang();
            $l = "";
            if($lang->lang == "en")
                $l="_en";
            $menu_array = [];
            $custom = [];
            $result = $this->getAll();
            $i=0;
            foreach($result as $row){
                
                $custom_inputs = $this->getAllCustoms($row->id);
                foreach($custom_inputs as $array){
                    $get_type = $this->db->query("SELECT * FROM inputs WHERE id = :id");
                    $this->db->bind($get_type, ":id", $array->type);
                    $key = $this->db->single($get_type);
                    
                    if($key->title == "Compagnies"){
                        $query_title_company = $this->db->query("SELECT cm.company_name FROM tbl_companies t JOIN tbl_companies_meta cm ON cm.id = t.fk_companies_meta WHERE t.id = :id");
                        $this->db->bind($query_title_company, ":id", $array->value);
                        $array->value = (isset($this->db->single($query_title_company)->company_name) ? $this->db->single($query_title_company)->company_name : "");
                    }
                    
                    $custom += [$key->title => $array->value];
                    
                }                    
                $menu_array[$row->id] = array('name' => $row->{'title'.$l},'parent' => $row->parent,'mid' => $row->id,'order' => $row->order, 'add' => $row->{'add'}, 'del' => $row->{'del'}, 'mod' => $row->{'mod'}, 'type' => $row->type, 'dtype' => $row->dtype, 'online' => $row->online, 'image' => $row->image, 'fk_gallery' => $row->fk_gallery);
                $menu_array[$row->id] += $custom;
                $i++;
            }
            return $menu_array;
        }
        
                public function update_orders($postData){
            $p = 0;
            foreach ($postData['id'] as $key => $value) {
                global $lang;
                if (isset($postData['order'][$p])){
                    try{
                        $query_update_orders = $this->db->query("UPDATE tbl_products SET `order` = :orders WHERE id= :id");
                        $this->db->bind($query_update_orders,":orders", $postData['order'][$p]);
                        $this->db->bind($query_update_orders,":id", $value);
                        if($this->db->execute($query_update_orders)){
                            $msg = $lang['edited_success'];
                        }else{
                            $msg =  $lang['edited_error'];
                        }
                    }catch(PDOException $e){
                        return false;
                    }
                }
                $p++;
            }
            return $msg;
        }

        public function update_onlines($postData){
            global $lang;
            foreach ($postData['online'] as $key => $value) {
                foreach($value as $key => $val){
                    try{
                        $query_update_online = $this->db->query("UPDATE tbl_products SET online= :onlines WHERE id = :id");
                        $this->db->bind($query_update_online,":onlines", $val);
                        $this->db->bind($query_update_online,":id", $key);
                        if($this->db->execute($query_update_online)){
                            $msg = $lang['edited_success'];
                        }else{
                            $msg =  $lang['edited_error'];
                        }
                    }catch(PDOException $e){
                        return false;
                    }
                }
            }
            return $msg;
        }
        //Search products
        public function seekInProductWebsite($id){
            try{
                $response = [];
                $query_get_product = $this->db->query("SELECT * FROM tbl_products WHERE id = :id ");
                $this->db->bind($query_get_product,":id", $id);
                $result_id = $this->db->resultSet($query_get_product);
                if($result_id){
                    $response['from_id'] = $result_id;
                }
                $query_seek = $this->db->query("SELECT * FROM tbl_products WHERE
                    notes LIKE CONCAT('%',:id,'%') ||
                    sub_total LIKE CONCAT('%',:id,'%') ||
                    final_price LIKE CONCAT('%',:id,'%')
                    ");
                $this->db->bind($query_seek,":id", $id);
                $result_text = $this->db->resultSet($query_seek);
                if($result_text){
                    $response['from_text'] = $result_text;
                }
                return $response;
            }catch(PDOException $e){
				return false;
			}
        }
         //DELETE Product
        public function DeleteProductWebsite($id){
            global $lang;
            $response = [];
            $response["text"] = "";
            $response["color"] = "";
            try{
                $query_delete_product = $this->db->query("DELETE FROM tbl_products WHERE id = :id");
                $this->db->bind($query_delete_product,":id", $id);
                if($this->db->execute($query_delete_product)){
                    $query_delete_urlproduct = $this->db->query("DELETE FROM tbl_products_urls WHERE product_id = :id");
                    $this->db->bind($query_delete_urlproduct,":id", $id);
                    if($this->db->execute($query_delete_urlproduct)){
                        $response["message"] = $lang['deleted_success'];
                        $response["color"] = "success";
                    }else{
                        $response["message"] = $lang['deleted_error'];
                        $response["color"] = "danger";
                    }
                }else{

                    $response["message"] = $lang['deleted_error'];
                    $response["color"] = "danger";
                }
                return $response;
            }catch(PDOException $e){
				return false;
			}
        }
        public function updateProductInputs($id){
			try{
				$select_ids = $this->db->query("SELECT * FROM tbl_products_inputs WHERE fk_products = :id");
				$this->db->bind($select_ids, ":id", $id);
				$serialized = "";
				$this->db->execute($select_ids);
                $i = 0;
				while($inputs = $this->db->fetch($select_ids)){
					$serialized .= ($i != 0 ? '-' : '').$inputs->id;
                    $i++;
				}
				$update = $this->db->query("UPDATE tbl_products SET custom_inputs = :data WHERE id = :id");
				$this->db->bind($update, ":data", $serialized);
				$this->db->bind($update, ":id", $id);
				return $this->db->execute($update);
			}catch(PDOException $e){
				return false;
			}
		}
        public function AddEditJoinInputs($key,$value,$product_id, $id=false){ // If id then edit
            if($id){
                $edit = $this->db->query("UPDATE tbl_products_inputs SET `value` = :value WHERE `type` = :type AND `fk_products` = :fk_products");
                $this->db->bind($edit,":value", $value);
                $this->db->bind($edit,":type", $key);
                $this->db->bind($edit,":fk_products", $product_id);
                if($this->db->execute($edit)){
                    $this->updateProductInputs($product_id);
                    return true;
                }else{
                    return false;
                }
            }else{
                $add = $this->db->query("INSERT INTO tbl_products_inputs (`type`, `value`, `fk_products`) VALUES(:type, :value, :fk_products )");
                $this->db->bind($add,":type", $key);
                $this->db->bind($add,":value", $value);
                $this->db->bind($add,":fk_products", $product_id);
                if($this->db->execute($add)){
                    $this->updateProductInputs($product_id);
                    return true;
                }else{
                // var_dump('test2'.$this->db->errorInfo($add)[2]);
                    return false;
                }
            }
        }
        //add or edit product
        public function AddEditProductWebsite($postData,$add=false){
            global $lang;
            if($add == false)
                unset($add);
            $response = [];
            $response["text"] = "";
            $response["color"] = "";


            $updates = "";
            $adds = "";
            $insert_sql1 = "";
            $insert_sql2 = "";

            /* custom inputs */
            $insert_inputs1 = "";
            $insert_inputs2 = "";
            $update_inputs = "";
            $can_insert = true;
            $add_customs = false;
			$box_string = "";
			$producteurs_string = "";
			$cepage_string = "";
            $binded = [];
            if($postData['title'] == "" || (!isset($postData['type'])))
                $can_insert = false;
            try{
                //Build string update
                foreach($postData as $key => $value){
					
					
					//label id == 112 a verifier live
					if($key == "wine_box"){
                        foreach($value as $valkey => $val){
                            if($val !== "")
                                $box_string .= "-".$val."-";
                        }
                    }
					if($key == "producteurs"){
                        foreach($value as $valkey => $val){
                            if($val !== "")
                                $producteurs_string .= "-".$val."-";
                        }
                    }
					if($key == "cepage"){
                        foreach($value as $valkey => $val){
                            if($val !== "")
                                $cepage_string .= "-".$val."-";
                        }
                    }
					
                    if ($key != 'B1' and $key != 'query'  and $key != 'supp'
                        and $key != 'url_id' and $key != 'url_id_en' and $key != 'wine_box' and $key != 'producteurs' and $key != 'cepage'
                        and $key != 'edit_product' and $key != 'add_product' and $key != 'id_product' and $key != 'edit_ts'
                        ) {

                        $key = str_replace('*', '', $key);

                        //Get inputs details
                        $query_inputs = $this->db->query("SELECT * FROM inputs where `name` = :key ");
                        $this->db->bind($query_inputs,":key", $key);
                        $inputs = $this->db->single($query_inputs);
                        if($inputs){
                            if ($inputs->type == "Upload Image") {

                            }
                            else if ($inputs->type == "Upload Autre") {

                                if ($_FILES[$key]['size'] != "0") {

                                    // Move the file
                                    $rand = rand("10000", "90000"). "-";
                                    $value = $rand . nomValide($_FILES[$key]['name']);
                                    if (move_uploaded_file($_FILES[$key]['tmp_name'], URLIMG ."files/". $value)) {
                                        //uploaded
                                    }
                                }
                            }
                            else if ($inputs->type == "Checkbox" || $inputs->type == "Cat&eacute;gorie" ) {
                                $str = '';
                                foreach($_POST[$key] as $current){
                                    if($current != "")
                                        $str .= "-$current-";
                                }
                                $value = $str;

                            }
                            else {
                                //Nothing else yet
                            }

                            if($key == 'dtype' && is_array($_POST['dtype'])) {
                                $value="";
                                foreach($_POST['dtype'] as $page){
                                    if($i != 0){$value.= '-';}
                                        $value .= $page;
                                        $i++;
                                }
                            }
                        }

                        if($key !== "type" AND $key !== "parent"
                        AND $key !== "order"
                        AND $key !== "seo_meta_title"
                        AND $key !== "seo_meta_title_en"
                        AND $key !== "seo_meta_description"
                        AND $key !== "seo_meta_description_en"
                        AND $key !== "seo_meta_title_tag"
                        AND $key !== "seo_meta_title_tag_en"
                        AND $key !== "title"
                        AND $key !== "title_en"
                        AND $key !== "texte"
                        AND $key !== "texte_en"
                        AND $key !== "image"
                        AND $key !== "alt"
                        AND $key !== "is_product"
                        AND $key !== "create_ts"
                        AND $key !== "online"
                        AND $key !== "custom_inputs"
                        ){
                            if(isset($postData["id_product"])){
                                if($this->checkIfExistJoinInputs($inputs->id, $postData["id_product"])){
                                    //edit
                                    $this->AddEditJoinInputs($inputs->id,$value,$postData["id_product"],$inputs->id);
                                }else{
                                    //add
                                    $this->AddEditJoinInputs($inputs->id,$value,$postData["id_product"]);
                                }
                            }else{
                                $add_customs = true;
                            }

                        }else{
                            $updates .= "`$key`= :$key, ";
                            $insert_sql1 .="`$key`, ";
                            $insert_sql2 .=":$key, ";

                            $binded[$key] = ( isset($value) ? $value : 1);
                        }
                    }

                }
                // var_dump($updates);
                //Delete last comma
                $updates = substr($updates, 0, -2);

                //updates
                if(!isset($add)){
                    $update = $this->db->query("UPDATE tbl_products SET ".$updates.", edit_ts = :time WHERE id = :id");
                    foreach($binded as $key => $value){
                        $this->db->bind($update,":$key", $value);
                    }
                    $this->db->bind($update,":time", time());
                    $this->db->bind($update,":id", $postData["id_product"]);
                    if($this->db->execute($update)){
                        if(isset($postData['url_id'])){
                            $upd = "";
                            $add1 = "";
                            $add2 = "";

                            if(MULTI_LANG){
                                foreach(LANGUAGES as $l){
                                    if($l != "fr"){
                                        $upd .= ", `url_id_".$l."` = :url_id_".$l."";
                                        $add1 .= ", `url_id_".$l."`";
                                        $add2 .= ", :url_id_".$l."";
                                    }
                                }
                            }
                            if($this->checkIfUrlExist($postData['id_product'])){
                                $query_url = $this->db->query("UPDATE tbl_products_urls SET url_id = :url_id ". $upd." WHERE product_id = :product_id");
                            }else{
                                $query_url = $this->db->query("INSERT INTO tbl_products_urls (`product_id`, `url_id` ". $add1.") VALUES ( :product_id, :url_id ".$add2.")");
                            }
                            $this->db->bind($query_url,":url_id", $postData['url_id']);
                            if(MULTI_LANG){
                                foreach(LANGUAGES as $l){
                                    if($l != "fr"){
                                        $this->db->bind($query_url,":url_id_".$l, $postData['url_id_'.$l]);
                                    }
                                }
                            }
                            $this->db->bind($query_url,":product_id", $postData["id_product"]);
                            if($this->db->execute($query_url)){ 
                            }else{
                                $response["message"] = "URL couldn be added";
                                $response["color"] = "danger";
                            }

                        }
                        $response["message"] = $lang['edited_success'];
                        $response["color"] = "success";
                    }else{
                        $response["message"] = $lang['edited_error'];
                        $response["color"] = "danger";
                    }
					
						
					
					//wine box custom edit
					if($this->checkIfExistJoinInputs(39, $postData["id_product"])){
						// edit
						$this->AddEditJoinInputs(39,$box_string,$postData["id_product"],39);
						
						
						
					}else{
						// add
						$this->AddEditJoinInputs(39,$box_string,$postData["id_product"]);
					}	
					
					
					//producteurs box custom edit
					if($this->checkIfExistJoinInputs(41, $postData["id_product"])){
						// edit
						$this->AddEditJoinInputs(41,$producteurs_string,$postData["id_product"],41);
						
						
						
					}else{
						// add
						$this->AddEditJoinInputs(41,$producteurs_string,$postData["id_product"]);
					}	
					//cepage_string box custom edit
					if($this->checkIfExistJoinInputs(42, $postData["id_product"])){
						// edit
						$this->AddEditJoinInputs(42,$cepage_string,$postData["id_product"],42);
						
						
						
					}else{
						// add
						$this->AddEditJoinInputs(42,$cepage_string,$postData["id_product"]);
					}	
                }else{
                    //ADD
                    if($can_insert){
                        // var_dump($postData);
                        $order = $this->db->query("SELECT `order` FROM tbl_products WHERE parent= :parent order by `order` desc limit 1");
                        $this->db->bind($order,":parent", (isset($postData['parent']) ? $postData['parent'] : 0));
                        $order = $this->db->single($order);
                        
                        if(!isset($order->order) || is_null($order->order)){
                            $lo = 1;
                        }else{
                            $lo = (1 + $order->order);
                        }
                        // var_dump("insert ignore into tbl_pages ($insert_sql1, `order`, `create_ts`) VALUES ($insert_sql3 '$lo', '".time()."')");
                        $adds = $this->db->query("insert ignore into tbl_products ($insert_sql1 `order`, `create_ts`) VALUES ($insert_sql2 '$lo', '".time()."')");
                        foreach($binded as $key => $value){
                           $this->db->bind($adds,":$key", $value);
                        }
                        if($this->db->execute($adds)){
                            if(isset($postData['url_id'])){
                                $current_page_id = $this->db->query("SELECT LAST_INSERT_ID() as 'lastid' FROM tbl_products");
                                $current_page_id = $this->db->single($current_page_id);
                                if($current_page_id->lastid !== 0){
                                    $current_page_id = $current_page_id->lastid;
                                }
                                if($add_customs){                                    
                                    $this->AddEditJoinInputs($inputs->id,$value,$current_page_id);
                                }
                                if(MULTI_LANG){
                                    $insert1 = "";
                                    $insert2 = "";
                                    $insert3 = array();
                                    foreach(LANGUAGES as $l){
                                        if($l != "fr"){
                                            $insert1 .= ", url_id_".$l;
                                            $insert2 .= ", :url_id_".$l;
                                        }
                                    }

                                    $insert_url = $this->db->query("INSERT INTO tbl_products_urls(product_id, url_id ".$insert1.") VALUES( :product_id, :url_id". $insert2.")");
                                    $this->db->bind($insert_url,":product_id", $current_page_id);
                                    $this->db->bind($insert_url,":url_id", $postData['url_id']);
                                    foreach(LANGUAGES as $l){
                                        if($l != "fr"){
                                            $this->db->bind($insert_url,":url_id_".$l, $postData['url_id_'.$l]);
                                        }
                                    }
                                    if($this->db->execute($insert_url)){
                                        $response["message"] = $lang['added_success'];
                                        $response["color"] = "success";
                                    }else{
                                        $response["message"] = $lang['added_error'];
                                        $response["color"] = "danger";
                                    }

                                }else{
                                    $insert_url = $this->db->query("INSERT INTO tbl_products_urls(product_id, url_id) VALUES( :product_id, :url_id)");
                                    $this->db->bind($insert_url,":product_id", $current_page_id);
                                    $this->db->bind($insert_url,":url_id", $postData['url_id']);
                                    if($this->db->single($insert_url)){
                                        $response["message"] = $lang['added_success'];
                                        $response["color"] = "success";
                                    }else{
                                        $response["message"] = $lang['added_error'];
                                        $response["color"] = "danger";
                                    }

                                }
                            }else{
                                $response["message"] = $lang['added_success'];
                                $response["color"] = "success";
                            }
                        }else{
                            $response["message"] = $lang['added_error'];
                            $response["color"] = "danger";
                        } 
						//wine box custom edit
						if($this->checkIfExistJoinInputs(39,$current_page_id)){
							// edit
							$this->AddEditJoinInputs(39,$box_string,$current_page_id,$inputs->id);
						}else{
							// add
							$this->AddEditJoinInputs(39,$box_string,$current_page_id);
						}
						
						//producteurs custom edit
						if($this->checkIfExistJoinInputs(41,$current_page_id)){
							// edit
							$this->AddEditJoinInputs(41,$producteurs_string,$current_page_id,$inputs->id);
						}else{
							// add
							$this->AddEditJoinInputs(41,$producteurs_string,$current_page_id);
						}
						//cepage_string custom edit
						if($this->checkIfExistJoinInputs(42,$current_page_id)){
							// edit
							$this->AddEditJoinInputs(42,$cepage_string,$current_page_id,$inputs->id);
						}else{
							// add
							$this->AddEditJoinInputs(42,$cepage_string,$current_page_id);
						}
					}else{
                        $response["message"] = $lang['added_error'];
                        $response["color"] = "danger";
                    }
                }
				$exists = array();
				// error_log("passe ici bich" , 0);
				if(isset($postData['quantity_box'])){
					$getQtyBox = $this->db->query("SELECT * FROM tbl_products_inputs WHERE fk_products = :productid AND type=:type");
					$this->db->bind($getQtyBox, ":productid", $postData['id_product']);
					$this->db->bind($getQtyBox, ":type", 40);
					$this->db->execute($getQtyBox);
					$html = ""; 
					$exists = array();
					if($this->db->rowCount($getQtyBox)> 0){
						
						// $oldText = $this->db->single($getQtyBox)->value;
						$oldText = $postData['quantity_box'];
						
						// error_log($oldText, 0);
						if($oldText != ""){ 
							//on separe en ligne chaque type de caisse et leur quantité
							$oldText = explode("**", $oldText);
							foreach($oldText as $ligne){
								if($ligne != ""){
									//on separe chaque type de caisse avec leur quantité
									$arrayBox = explode("||", $ligne);  
									if($arrayBox[0] != ""){  
									
										
										//type caisse get id  
										$getId = $this->db->query("SELECT id FROM tbl_pages WHERE title = :title");
										$this->db->bind($getId, ":title", $arrayBox[0]);
										$this->db->execute($getId);
										if($this->db->rowCount($getId) > 0){
											$idBox = $this->db->single($getId)->id;
										}
										$array = explode("-", $box_string); 
										
										if(!isset($exist[$arrayBox[0]])){
											$exist[$arrayBox[0]] = 0;
										}
										
									 
										// error_log("passe ici 2!", 0);
										if(in_array($idBox, $array) && $exist[$arrayBox[0]] == 0){
											//Check si la quantité par caisse exist
											// error_log(" La variable de détection de doublons est rendu a:  ". $exist[$arrayBox[0]], 0);
											$exist[$arrayBox[0]] = $exist[$arrayBox[0]] + 1;
											
											$getBoxesQuantity = $this->db->query("SELECT * FROM tbl_joins_caisse WHERE fk_products = :productid AND fk_box = :boxid");
											$this->db->bind($getBoxesQuantity, ":productid", $postData["id_product"]);
											$this->db->bind($getBoxesQuantity, ":boxid", $idBox);
											$this->db->execute($getBoxesQuantity);
											
											//on update quantity si existe
											if($this->db->rowCount($getBoxesQuantity) > 0){

												$update_qty_single = $this->db->query("
												UPDATE tbl_joins_caisse
												SET quantity = :quantity, pricebottle = :pricebottle 
												WHERE fk_products = :productid 
												AND fk_box = :boxid");
												$this->db->bind($update_qty_single, ":quantity", $arrayBox[1] != "" ? $arrayBox[1] : 0);
												$this->db->bind($update_qty_single, ":pricebottle", $arrayBox[2] != "" ? $arrayBox[2] : 0);
												$this->db->bind($update_qty_single, ":productid", $postData["id_product"]);
												$this->db->bind($update_qty_single, ":boxid", $idBox);
												$this->db->execute($update_qty_single); 
													 
											}else{
												//check if exist dans table jointure auparavant;
												$getOldValueIfExist = $this->db->query("
													SELECT * FROM tbl_joins_caisse 
													WHERE fk_products = :productid AND fk_box = :boxid");
												$this->db->bind($getOldValueIfExist, ":productid", $postData["id_product"]);
												$this->db->bind($getOldValueIfExist, ":boxid", $idBox); 
												$this->db->execute($getOldValueIfExist);
												if($this->db->rowCount($getOldValueIfExist) > 0){
													$arrayValue = $this->db->single($getOldValueIfExist);
													$oldValue = $arrayValue->quantity;
													$oldValuepricebottle = $arrayValue->pricebottle;
												}
												$update_qty_single = $this->db->query("
												INSERT INTO tbl_joins_caisse
												(`fk_box`, `fk_products`, `quantity`)
												VALUES ( :boxid, :productid, :quantity)");
												$this->db->bind($update_qty_single, ":quantity", $arrayBox[1] != "" ? $arrayBox[1] : (isset($oldValue) ? $oldValue : 0));
												$this->db->bind($update_qty_single, ":pricebottle", $arrayBox[2] != "" ? $arrayBox[2] : (isset($oldValuepricebottle) ? $oldValuepricebottle : 0));
												$this->db->bind($update_qty_single, ":productid", $postData["id_product"]);
												$this->db->bind($update_qty_single, ":boxid", $idBox);
												$this->db->execute($update_qty_single); 
											}
											$html .= $arrayBox[0]."||".(isset($arrayBox[1]) && $arrayBox[1] != "" ? $arrayBox[1] : (isset($oldValue) ? $oldValue : 0))."||".(isset($arrayBox[2]) && $arrayBox[2] != "" ? $arrayBox[2] : (isset($oldValuepricebottle) ? $oldValuepricebottle : 0))."**";											
										} 			 									
									//on enleve en conservant les autres et leur quantité
									}
								} 
							}
							//on ajoute si cocher mais pas existant dans le champs quantité caisse
							$array = explode("-", $box_string);
							foreach($array as $key => $value){
								//type caisse get id
								if($value != ""){
									$getTitle = $this->db->query("SELECT * FROM tbl_pages WHERE id = :id");
									$this->db->bind($getTitle, ":id", $value);
									$this->db->execute($getTitle);
									if($this->db->rowCount($getTitle) > 0){
										$title = $this->db->single($getTitle)->title;
										if(!isset($exist[$title])){
											//check if exist dans table jointure auparavant;
											$getOldValueIfExist = $this->db->query("
												SELECT * FROM tbl_joins_caisse 
												WHERE fk_products = :productid AND fk_box = :boxid");
											$this->db->bind($getOldValueIfExist, ":productid", $postData["id_product"]);
											$this->db->bind($getOldValueIfExist, ":boxid", $value); 
											$this->db->execute($getOldValueIfExist);
											if($this->db->rowCount($getOldValueIfExist) > 0){
													$arrayValue = $this->db->single($getOldValueIfExist);
													$oldValue = $arrayValue->quantity;
													$oldValuepricebottle = $arrayValue->pricebottle;
											}
											$html .= $title."||".(isset($oldValue) ? $oldValue : 0)."||".(isset($oldValuepricebottle) ? $oldValuepricebottle : 0)."**";
										}
									}
								}
							}							 
						}else{
							// error_log("passe ici!", 0);
							$array = explode("-", $box_string);
							foreach($array as $key => $value){
								//type caisse get id
								if($value != ""){
									$getTitle = $this->db->query("SELECT * FROM tbl_pages WHERE id = :id");
									$this->db->bind($getTitle, ":id", $value);
									$this->db->execute($getTitle);
									if($this->db->rowCount($getTitle) > 0){
										$title = $this->db->single($getTitle)->title;
										$html .= $title."||0||0**";
									}
								}
							}
						}
						// error_log("le html =  ".$html, 0);
					}
					//on update la textarea
					$update_box = $this->db->query("UPDATE tbl_products_inputs SET value = :value WHERE fk_products = :productid AND type = :type");
					$this->db->bind($update_box, ":value", $html);
					$this->db->bind($update_box, ":productid", $postData["id_product"]);
					$this->db->bind($update_box, ":type", 40);
					$this->db->execute($update_box);
				}
				if(isset($postData['cepage_detail'])){
					$getQtyBox = $this->db->query("SELECT * FROM tbl_products_inputs WHERE fk_products = :productid AND type=:type");
					$this->db->bind($getQtyBox, ":productid", $postData['id_product']);
					$this->db->bind($getQtyBox, ":type", 49);
					$this->db->execute($getQtyBox);
					$html = ""; 
					$exists = array();
					if($this->db->rowCount($getQtyBox)> 0){
						
						// $oldText = $this->db->single($getQtyBox)->value;
						$oldText = $postData['cepage_detail'];
						
						// error_log($oldText, 0);
						if($oldText != ""){ 
							//on separe en ligne chaque type de caisse et leur quantité
							$oldText = explode("**", $oldText);
							foreach($oldText as $ligne){
								if($ligne != ""){
									//on separe chaque type de caisse avec leur quantité
									$arrayBox = explode("||", $ligne);  
									if($arrayBox[0] != ""){  
									
										
										//type caisse get id  
										$getId = $this->db->query("SELECT id FROM tbl_pages WHERE title = :title AND parent = 96");
										$this->db->bind($getId, ":title", $arrayBox[0]);
										$this->db->execute($getId);
										if($this->db->rowCount($getId) > 0){
											$idBox = $this->db->single($getId)->id;
										}
										$array = explode("-", $cepage_string); 
										
										if(!isset($exist[$arrayBox[0]])){
											$exist[$arrayBox[0]] = 0;
										}
										
									 
										// error_log("passe ici 2!", 0);
										if(in_array($idBox, $array) && $exist[$arrayBox[0]] == 0){
											//Check si la quantité par caisse exist
											// error_log(" La variable de détection de doublons est rendu a:  ". $exist[$arrayBox[0]], 0);
											$exist[$arrayBox[0]] = $exist[$arrayBox[0]] + 1;
											 
											$html .= $arrayBox[0]."||".(isset($arrayBox[1]) && $arrayBox[1] != "" ? $arrayBox[1] : 0) ."**";											
										} 			 									
									//on enleve en conservant les autres et leur quantité
									}
								} 
							}
							//on ajoute si cocher mais pas existant dans le champs quantité caisse
							$array = explode("-", $cepage_string);
							foreach($array as $key => $value){
								//type caisse get id
								if($value != ""){
									$getTitle = $this->db->query("SELECT * FROM tbl_pages WHERE id = :id");
									$this->db->bind($getTitle, ":id", $value);
									$this->db->execute($getTitle);
									if($this->db->rowCount($getTitle) > 0){
										$title = $this->db->single($getTitle)->title;
										if(!isset($exist[$title])){
											 
											$html .= $title."||0**";
										}
									}
								}
							}							 
						}else{
							// error_log("passe ici!", 0);
							$array = explode("-", $cepage_string);
							foreach($array as $key => $value){
								//type caisse get id
								if($value != ""){
									$getTitle = $this->db->query("SELECT * FROM tbl_pages WHERE id = :id");
									$this->db->bind($getTitle, ":id", $value);
									$this->db->execute($getTitle);
									if($this->db->rowCount($getTitle) > 0){
										$title = $this->db->single($getTitle)->title;
										$html .= $title."||0**";
									}
								}
							}
						}
						// error_log("le html =  ".$html, 0);
					}
					//on update la textarea
					$update_box = $this->db->query("UPDATE tbl_products_inputs SET value = :value WHERE fk_products = :productid AND type = :type");
					$this->db->bind($update_box, ":value", $html);
					$this->db->bind($update_box, ":productid", $postData["id_product"]);
					$this->db->bind($update_box, ":type", 49);
					$this->db->execute($update_box);
				}
                return $response;
            }catch(PDOException $e){
				return false;
			}
			
			
        }
		public function ManualCreateProduct($postdata){
				global $lang;
				$order = $this->db->query("SELECT `order` FROM tbl_products WHERE parent= 1 order by `order` desc limit 1");
				$this->db->bind($order,":parent", (isset($postData['parent']) ? $postData['parent'] : 0));
				$order = $this->db->single($order);
				
				if(!isset($order->order) || is_null($order->order)){
					$lo = 1;
				}else{
					$lo = (1 + $order->order);
				}
				// var_dump("insert ignore into tbl_pages ($insert_sql1, `order`, `create_ts`) VALUES ($insert_sql3 '$lo', '".time()."')");
				$adds = $this->db->query("
				insert ignore into tbl_products (`type`, `parent`,`root`, `order`, `title`, `title_en`, `texte`, `texte_en`, `create_ts`, `online`, `custom_inputs`)
				VALUES ( 39,'1','1', '$lo', :title , :title_en , :texte, :texte_en, '".time()."', '1', '18-20-21')"); 
				
				$this->db->bind($adds,":title", $postdata['title']); 
				$this->db->bind($adds,":title_en", $postdata['title']); 
				$this->db->bind($adds,":texte", $postdata['texte']); 
				$this->db->bind($adds,":texte_en", $postdata['texte_en']); 
				
				if($this->db->execute($adds)){ 
						$current_page_id = $this->db->query("SELECT LAST_INSERT_ID() as 'lastid' FROM tbl_products");
						$current_page_id = $this->db->single($current_page_id);
						if($current_page_id->lastid !== 0){
							$current_page_id = $current_page_id->lastid;
						}                               
						$this->AddEditJoinInputs(18,$postdata['price'],$current_page_id); 
						$this->AddEditJoinInputs(20,$postdata['quantity'],$current_page_id); 
						$this->AddEditJoinInputs(21,1,$current_page_id); 
						
						return $current_page_id;
				}else{
					$response["message"] = $lang['added_error'];
					$response["color"] = "danger";
				}
			}
    }