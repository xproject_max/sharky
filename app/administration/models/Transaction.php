<?php
	
	class Transaction
	{
		protected $id;
		protected $table;
		private $db;
		
		/* on instanti la class Transaction */

		public function __construct($data = [])
		{
			if (!empty($data)) {
				
				$this->id = $data['id'];
				$this->table = $data['table'];
			}
			$this->db = new Database();
		}
		
		/*
		* On appel la fonction qui fait afficher le tableau d'historique des transactions
		*/
		
		public function TransactionsBoard($type = "User")
		{
			global $_GET, $_SERVER;
			
			switch ($type) {
				case "User":
					$page_id ='.__THANKYOUID__ .';
					$typeid = "userid";
					$typeid_query = "user_id";
					$request = $this->db->query(
						'
                        select
                        t.transaction_type, o.id as ids, o.date_created as dates, tr.transactions_id, t.total_price, t.payment_method,
                         t.notes, t.notes_en, t.id_member, t.is_order, tr.order_id, o.archived, o.refund_id 
                        from
                        transactions t
                         LEFT JOIN	transactions_detail tr
                         ON tr.transactions_id = t.id
                         LEFT JOIN	(SELECT id,date_created,archived, refund_id FROM tbl_orders GROUP BY id) o
                         ON tr.order_id = o.id
                         WHERE t.user_id = :id and (t.company_id is null or t.company_id = 0) 
                         GROUP BY tr.transactions_id
                         ORDER BY t.id DESC, tr.id ASC
                        LIMIT :startpoint , :limit
                    '
					);
					break;
				case "Company":
					
					$page_id = __CARTID__;
					$typeid = "companyid";
					$typeid_query = "company_id";
					$request = $this->db->query(
						'
                        select
                        t.transaction_type, o.id as ids, o.date_created as dates, tr.transactions_id, t.total_price, t.payment_method,
                         t.notes, t.notes_en, t.id_member, t.is_order, tr.order_id, o.archived, o.refund_id 
                        from
                        transactions t
                         LEFT JOIN	transactions_detail tr
                         ON tr.transactions_id = t.id
                         LEFT JOIN	(SELECT id,date_created,archived, refund_id FROM tbl_orders GROUP BY id) o
                         ON tr.order_id = o.id
                         WHERE t.company_id = :id and (t.user_id is null or t.user_id = 0) 
                         GROUP BY tr.transactions_id
                         ORDER BY t.id DESC, tr.id ASC
                        LIMIT :startpoint , :limit
                    '
					);
					break;
			}
			
			$id = $this->id;
			$table = $this->table;
			$form = new Form();
			
			/* On config la pagination */
			$page = (int)(!isset($_GET["page"]) ? 1 : $_GET["page"]);
			$limit = 10; // Limite d'affichage de transaction par page
			$startpoint = ($page * $limit) - $limit;
			$statement = ' ' . $table . ' WHERE ' . $typeid_query . ' = :id ORDER BY id ASC';
			$balance = 0;
			echo pagination($statement, $limit, $page, URLROOT . getUrlLang($page_id) . "?" . $typeid . "=" . $id . "&", ["id" => $id]);
			echo '
            <table class="form_site_users table" style="table-layout: fixed;margin: 0 auto;border: 2px solid black;">
                <tr style="background: #000;color: white;border-bottom:0px">
                    <th>Débit</th>
                    <th>Crédit</th>
                    <th>Solde</th>
                    <th>Description</th>
                    <th>Date</th>
                    <th>Méthode de paiement</th>
                    <th>Action</th>
                </tr>
            ';
			/*
				On va chercher les regroupements de transactions pour
				ensuite détecter si c'est une commande ou non
				
			*/
			
			
			$this->db->bind($request, ":id", $id);
			$this->db->bind($request, ":startpoint", $startpoint);
			$this->db->bind($request, ":limit", $limit);
			if ($this->db->rowCount($request) > 0) {
				$payments_array = array();
				$i = 0;
				while ($transaction = $this->db->fetch($request, "array")) {
					
					$query_exist_order = $this->db->query('SELECT *, sum(credit_used) as credit_used FROM tbl_orders WHERE id = :id');
					$this->db->bind($query_exist_order, ":id", (isset($transaction['order_id']) ? $transaction['order_id'] : 0));
					if ($this->db->rowCount($query_exist_order) > 0) {
						$order_exist = $this->db->single($query_exist_order);
						$order_exist = object_to_array($order_exist);
					}
					
					/*
						On va remplaçer le nom
						des moyens de paiement
					*/
					
					$transaction['payment_method'] = rename_payments($transaction['payment_method']);
					
					$temp_html = '';
					
					$temp_html .= '
                        <tr class="order" style="background: #525253;color: white;">
                            <td colspan="6">
                                ' . (($transaction['is_order'] == 1) ? "Commande #" . $transaction['order_id'] . " " . (($transaction['archived'] == "1") ? "(Annulée)" : "") . "" : "Transaction #" . $transaction['transactions_id'] . "") . '
                                ' . (($transaction['payment_method'] == "Annulation") ? '(Annulation de la commande #' . $transaction['refund_id'] . ')' : '') . '
                                ' . (($transaction['order_id'] == "" || $transaction['notes'] == "Crédit enlevé du compte") ? "(Modification au solde du compte)" : "") . '
                            </td>
                    ';
					$request_order_id = $this->db->query(
						'
                        SELECT id,date_created FROM tbl_orders WHERE id = :id
                    '
					);
					$this->db->bind($request_order_id, ":id", (isset($order_exist['refund_id']) ? $order_exist['refund_id'] : 0));
					if ($this->db->rowCount($request_order_id) > 0) {
						$order_refunded = object_to_array($this->db->single($request_order_id));
					}
					$temp_html .= '
                            <td>
                    ';
					if ($transaction['archived'] == "1" && $transaction['payment_method'] == "Annulation" && $transaction['is_order'] != 1) {
						$temp_html .= '
                                        <a class="bill" target="_blank" href="' . URLCLIENT . 'ajax.php?bill=' . base64_encode($order_refunded['id'] . '||' . $order_refunded['date_created']) . '">Voir</a>
                        ';
					} else if ($transaction['is_order'] == 1) {
						$temp_html .= '	<a class="bill"  target="_blank" href="' . URLCLIENT . 'ajax.php?bill=' . base64_encode($transaction['ids'] . '||' . $transaction['dates']) . '">Voir</a>';
						
					} else {
						$temp_html .= '	<a class="bill"  target="_blank" href="' . URLCLIENT . 'ajax.php?payment=' . base64_encode($order_exist['id'] . '-' . $order_exist['date_created']) . '&id=' . $transaction['transactions_id'] . '">Voir</a>
                        / <a onclick="edit_transaction(' . $transaction['transactions_id'] . ');return false;" href="#">Modifier</a>
                        ';
					}
					$temp_html .= '</td>
                        </tr>
                    ';
					
					$temp_html .= '
                    ';
					/* On cherche les détails pour chaque transaction */
					switch ($type) {
						case "User":
							$query_transactions_details = $this->db->query(
								'
                                select
                                s.id,
                                s.transactions_id,
                                s.op_balance as Open_Balance,
                                s.total_price,
                                s.balance,
                                transactions_detail.*, transactions.id_member, tbl_orders.amount_to_pay, tbl_orders.status, tbl_orders.refund_id
                                from
                                (
                                    select
                                    t.id,
                                    t.transactions_id,
                                    t.total_price,
                                    @cur_bal := @cur_bal + t.total_price as balance,
                                    (@cur_bal + t.total_price) as op_balance,
                                    @prev_client := t.id
                                    from(
                                        select * from transactions_detail WHERE transactions_detail.user_id = :id ORDER BY payment_date ASC, transactions_detail.id ASC
                                    )t,(select @prev_client:=0,@cur_bal:=0,@tot_credit:=0,@tot_debit:= 0,@open_balance:=0)r
                                )s
                                    LEFT JOIN transactions_detail
                                    ON transactions_detail.id = s.id
                                    LEFT JOIN (SELECT * FROM tbl_orders) tbl_orders
                                    ON tbl_orders.id = transactions_detail.order_id
                                    LEFT JOIN transactions
                                    ON transactions.id = transactions_detail.transactions_id
                                    WHERE transactions_detail.transactions_id = :transactions_id 
                                    AND transactions_detail.user_id = :id and (transactions_detail.company_id is null or transactions_detail.company_id = 0) 
                                    ORDER BY transactions_detail.payment_date DESC, transactions_detail.id DESC
                            '
							);
							break;
						case "Company":
							
							$query_transactions_details = $this->db->query(
								'
                                select
                                s.id,
                                s.transactions_id,
                                s.op_balance as Open_Balance,
                                s.total_price,
                                s.balance,
                                transactions_detail.*, transactions.id_member, tbl_orders.amount_to_pay, tbl_orders.status, tbl_orders.refund_id
                                from
                                (
                                    select
                                    t.id,
                                    t.transactions_id,
                                    t.total_price,
                                    @cur_bal := @cur_bal + t.total_price as balance,
                                    (@cur_bal + t.total_price) as op_balance,
                                    @prev_client := t.id
                                    from(
                                        select * from transactions_detail WHERE transactions_detail.company_id = :id ORDER BY transactions_detail.id ASC,transactions_detail.payment_date DESC
                                    )t,(select @prev_client:=0,@cur_bal:=0,@tot_credit:=0,@tot_debit:= 0,@open_balance:=0)r
                                )s
                                    LEFT JOIN transactions_detail
                                    ON transactions_detail.id = s.id
                                    LEFT JOIN (SELECT * FROM tbl_orders) tbl_orders
                                    ON tbl_orders.id = transactions_detail.order_id
                                    LEFT JOIN transactions
                                    ON transactions.id = transactions_detail.transactions_id
                                    WHERE transactions_detail.transactions_id = :transactions_id 
                                    AND transactions_detail.company_id = :id and (transactions_detail.user_id is null or transactions_detail.user_id = 0) 
                                    ORDER BY transactions_detail.id DESC,transactions_detail.payment_date DESC
                            '
							);
							break;
					}
					
					$this->db->bind($query_transactions_details, ":id", $id);
					$this->db->bind($query_transactions_details, ":transactions_id", $transaction['transactions_id']);
					$this->db->bind($query_transactions_details, ":id", $id);
					if ($this->db->rowCount($query_transactions_details) > 0) {
						while ($tdetail = $this->db->fetch($query_transactions_details, "array")) {
							if ($tdetail['total_price'] == "") {
								$tdetail['total_price'] = 0;
							}
							$tdetail['payment_method'] = rename_payments($tdetail['payment_method']);
							$no_duplicate = 0;
							$temp_html .= '
                        <tr class="order">
                            ';
							if ($tdetail['total_price'] < 0) {
								$temp_html .= '
                            <td>
                                ' . number_format((-1 * $tdetail['total_price']), 2, '.', '') . '$
                            </td>
                            <td>0.00$</td>
                                ';
								$temp_html .= '
                            <td>
                                ' . number_format($tdetail['balance'], 2, '.', '') . '$
                            </td>	
                                ';
							} else {
								
								$temp_html .= '
                            <td>0.00$</td>
                            <td>' . number_format(($tdetail['total_price']), 2, '.', '') . '$</td>
                                ';
								$temp_html .= '
                            <td>
                                ' . number_format($tdetail['balance'], 2, '.', '') . '$
                            </td>
                                ';
							}
							$temp_html .= '
                            <td>' . (isset($tdetail['notes']) && trim($tdetail['notes']) !== "" ? $tdetail['notes'] . '<br/><br/>' : '') . '
                            ';
							if (isset($tdetail['order_id']) || $tdetail['order_id'] != "") {
								if ($tdetail['order_id'] == 0) {
									$temp_html .= ' (Modification au solde du compte)' . '<br/>';
									
								} else {
									$temp_html .= 'Commande #' . (($transaction['payment_method'] == "Annulation") ? $tdetail['order_id'] : $tdetail['order_id']) . '<br/>';
								}
							} else {
								if ($tdetail['id_member'] && $tdetail['is_order'] == 0) {
									$requestUpdater = $this->db->query(
										'
                                        SELECT *
                                        FROM tess_members
                                        WHERE id = :id_member 
                                    '
									);
									$this->db->bind($requestUpdater, ":id_member", $tdetail['id_member']);
									
									$member = $this->db->resultSet($requestUpdater, "array");
									$temp_html .= 'Ajouté par : ' . $member[0]->pseudo . '<br/>';
								}
							}
							$temp_html .= '
                            </td>
                            ';
							$temp_html .= '
                            <td>
                                le : ' . date("d-m-Y H:i:s", $tdetail['payment_date']) . '
                            </td>
                            ';
							$temp_html .= '
                            <td>
                                ' . $tdetail['payment_method'] . '
                            </td>
                            <td>
                            </td>
                        </tr>
                            ';
							
							$no_duplicate++;
							
						}
					}
					
					$payments_array[] = $temp_html;
					
				}
				foreach ($payments_array as $payment_html) {
					echo $payment_html . '
                    ';
				}
				
			} else {
				echo '<tr><td colspan="7">Il n\'y a pas de transaction</td></tr>';
			}
			
			echo '
                </table></br>
            ';
			echo pagination($statement, $limit, $page, URLROOT . getUrlLang($page_id) . "?" . $typeid . "=" . $id . "&", ["id" => $id]);
		}
		
		public function formPayUnpaidOrders($type = "User")
		{
			switch ($type) {
				case "User":
					
					$queryamounttotal = $this->db->query(
						'
                    SELECT round(SUM(amount_to_pay),2) as amount_to_pay,
                    round((tbl_users.solde),2) as accountleftamount 
                    FROM tbl_orders INNER JOIN tbl_users ON tbl_users.id = tbl_orders.fk_users
                    WHERE tbl_orders.status = "unpaid" AND amount_to_pay != 0 AND tbl_orders.fk_users = :id '
					);
					break;
				case "Company":
					$queryamounttotal = $this->db->query(
						'
                    SELECT round(SUM(tbl_orders.amount_to_pay),2) as amount_to_pay,
                    round((tbl_companies.solde),2) as accountleftamount 
                    FROM tbl_orders INNER JOIN tbl_companies ON tbl_companies.id = tbl_orders.fk_companies
                    WHERE tbl_orders.status = "unpaid" AND
                    amount_to_pay != 0 AND
                    tbl_orders.fk_companies = :id AND
                    (tbl_orders.fk_users = 0 or tbl_orders.fk_users is null) '
					);
					break;
			}
			
			$html = "";
			$this->db->bind($queryamounttotal, ":id", $this->id);
			if ($this->db->rowCount($queryamounttotal) > 0) {
				echo "<table class='table' style='table-layout: fixed;margin: 0 auto;border: 2px solid black;'> ";
				while ($amountotal = $this->db->fetch($queryamounttotal, "array")) {
					$html .= '
                    <tr   style="background: #000;color: white;border-bottom:0px">
                        <th>
                            Montant souhaitant payer
                           </th>
                        <th>
                            Montant restant total des commandes à payer
                        </th>
                        <th colspan="3">
                            Montant restant total du solde au compte à payer 
                        </th>
                        <th colspan="2">
                           
                        </th>
                    </tr>
                    <tr class="order">
                        <td>
                            <input type="text" class="payment_due_class"
                            name="payment_due" value="" placeholder="montant :(ex:15.00$)" />
                        </td>
                        <td>
                            <input  disabled="disabled" class="balance_left_class" name="balance_left" value="' . $amountotal["amount_to_pay"] . '" />
                            <input type="hidden" class="id_payment_due_class" name="id_payment_due" value="' . $amountotal["amount_to_pay"] . '" />
                            <input type="hidden" class="order_id_class" name="order_id" value="' . $amountotal["amount_to_pay"] . '" />
                        </td>
                        <td colspan="3">
                            <input  disabled="disabled" class="balance_left_class balance_left_class_account" name="balance_left" value="' . ((((($amountotal["amount_to_pay"]) ? $amountotal["accountleftamount"] + $amountotal["amount_to_pay"] : $amountotal["accountleftamount"])) < 0) ? (-1 * (($amountotal["amount_to_pay"]) ? $amountotal["accountleftamount"] + $amountotal["amount_to_pay"] : $amountotal["accountleftamount"])) : (-1 * $amountotal["accountleftamount"])) . '" />
                        </td> 
                        <td colspan="2">
                            <button onclick="payer()" value="payer" >
                                Payer
                            </button>
                        </td>
                    </tr>
                    ';
					$amountt = $amountotal["amount_to_pay"];
					$balancel = ((((($amountotal["amount_to_pay"]) ? $amountotal["accountleftamount"] + $amountotal["amount_to_pay"] : $amountotal["accountleftamount"])) < 0) ? (-1 * (($amountotal["amount_to_pay"]) ? $amountotal["accountleftamount"] + $amountotal["amount_to_pay"] : $amountotal["accountleftamount"])) : 0);
				}
				/*
					Functions javascript pour paiement partiel et vérifications des montants ajoutés.
					Se trouve dans administration/ajax.js
				*/
				$html .= '
                    <tr style="display:none;">
                        <td>
                            <input type="hidden" class="getuserid" data-userid="' . $this->id . '" />
                            <input type="hidden"  class="' . ($type == "Company" ? "getcompanybalance" : "getuserbalance") . '" data-balance="' . ($amountt + $balancel) . '" />
                        </td>
                    </tr>
                ';
				
				
				$html .= '
                </table>
                ';
			} else {
				$amountt = 0;
				$balancel = 0;
				// echo  print_r($queryamounttotal->errorInfo());
			}
			
			echo $html;
		}
		
		public function add_transaction_from_order($order, $user, $debit = false)
		{
			global $_SESSION;
			
			if ($debit == true) {
				$_POST['total'] = number_format(($order->final_price * -1), 2, '.', '');
			}
			$query_insert_transactions = $this->db->query(
				'
                INSERT INTO `transactions`(`user_id`,
                `transaction_type`, `total_price`,
                `payment_method`, `payment_date`, `notes`, `notes_en`, `id_member`,
                `is_order`)
                VALUES (
                :user_id,
                :transaction_type,
                :total_price,
                :payment_method,
                :payment_date,
                :notes,
                :notes_en,
                :id_member,
                1
                )
            '
			);
			$this->db->bind($query_insert_transactions, ":user_id", $user->id);
			$this->db->bind($query_insert_transactions, ":transaction_type", $order->payment_method);
			$this->db->bind($query_insert_transactions, ":total_price", number_format($order->final_price, 2, ".", ""));
			$this->db->bind($query_insert_transactions, ":payment_method", $order->payment_method);
			$this->db->bind($query_insert_transactions, ":payment_date", time());
			$this->db->bind($query_insert_transactions, ":notes", "");
			$this->db->bind($query_insert_transactions, ":notes_en", "");
			$this->db->bind($query_insert_transactions, ":id_member", "");
			if ($this->db->execute($query_insert_transactions)) {
				
				//get the transac id
				$current_transac_id = $this->db->query("SELECT LAST_INSERT_ID() as 'lastid' FROM transactions");
				$current_transac_id = $this->db->single($current_transac_id);
				if ($current_transac_id->lastid !== 0) {
					$current_transac_id = $current_transac_id->lastid;
					$response = $current_transac_id;
				}
			}
			return $response;
			
		}
		
		public function add_transaction($post, $infoAccount, $debit = false)
		{
			global $_SESSION;
			
			$_POST = $post;
			$total = number_format($_POST['total'], 2, '.', '');
			if ($debit == true) {
				$total = number_format(($_POST['total'] * -1), 2, '.', '');
			}
			$query_insert_transactions = $this->db->query(
				'
                INSERT INTO `transactions`(`user_id`,
                `transaction_type`, `total_price`,
                `payment_method`, `payment_date`, `notes`, `notes_en`, `id_member`,
                `is_order`)
                VALUES (
                :user_id,
                :transaction_type,
                :total_price,
                :payment_method,
                :payment_date,
                :notes,
                :notes_en,
                :id_member,
                1
                )
            '
			);
			$this->db->bind($query_insert_transactions, ":user_id", (is_array($infoAccount) ? $infoAccount[0]->id : $infoAccount->id));
			$this->db->bind($query_insert_transactions, ":transaction_type", $_POST['payment_method']);
			$this->db->bind($query_insert_transactions, ":total_price", $total);
			$this->db->bind($query_insert_transactions, ":payment_method", $_POST['payment_method']);
			$this->db->bind($query_insert_transactions, ":payment_date", time());
			$this->db->bind($query_insert_transactions, ":notes", "");
			$this->db->bind($query_insert_transactions, ":notes_en", "");
			$this->db->bind($query_insert_transactions, ":id_member", "");
			if ($this->db->execute($query_insert_transactions)) {
				
				//get the transac id
				$current_transac_id = $this->db->query("SELECT LAST_INSERT_ID() as 'lastid' FROM transactions");
				$current_transac_id = $this->db->single($current_transac_id);
				if ($current_transac_id->lastid !== 0) {
					$current_transac_id = $current_transac_id->lastid;
					$response = $current_transac_id;
				}
			}
			return $response;
			
		}
		
		public function add_transaction_details($tid, $orderid = "", $postdata, $infoAccount, $debit = false)
		{
			$_POST = $postdata;
			
			$total = number_format($_POST['total'], 2, '.', '');
			if ($debit == true) {
				$total = number_format(($_POST['total'] * -1), 2, '.', '');
			}
			$tdetail = $this->db->query(
				'
                INSERT INTO transactions_detail (
                `transactions_id`,
                `user_id`,
                `order_id`,
                `transaction_type`,
                `total_price`,
                `payment_method`,
                `payment_date`,
                `notes`,
                `notes_en`,
                `is_order`
                )
                 VALUES (
                    :transaction_id,
                    :user_id,
                    :order_id,
                    :transaction_type,
                    :total_price,
                    :payment_method,
                    :payment_date,
                    :notes,
                    :notes_en,
                    1
                )
            '
			);
			
			$this->db->bind($tdetail, ":transaction_id", $tid);
			$this->db->bind($tdetail, ":user_id", (is_array($infoAccount) ? $infoAccount[0]->id : $infoAccount->id));
			$this->db->bind($tdetail, ":order_id", (isset($orderid) ? $orderid : ""));
			$this->db->bind($tdetail, ":transaction_type", $_POST['payment_method']);
			$this->db->bind($tdetail, ":total_price", $total);
			$this->db->bind($tdetail, ":payment_method", $_POST['payment_method']);
			$this->db->bind($tdetail, ":payment_date", time());
			$this->db->bind($tdetail, ":notes", "");
			$this->db->bind($tdetail, ":notes_en", "");
			if ($this->db->execute($tdetail)) {
			}
		}
		
		public function add_transaction_details_from_order($tid, $orderid = "", $order, $user, $debit = false)
		{
			
			$tdetail = $this->db->query(
				'
                INSERT INTO transactions_detail (
                `transactions_id`,
                `user_id`,
                `order_id`,
                `transaction_type`,
                `total_price`,
                `payment_method`,
                `payment_date`,
                `notes`,
                `notes_en`,
                `is_order`
                )
                 VALUES (
                    :transaction_id,
                    :user_id,
                    :order_id,
                    :transaction_type,
                    :total_price,
                    :payment_method,
                    :payment_date,
                    :notes,
                    :notes_en,
                    1
                )
            '
			);
			
			$this->db->bind($tdetail, ":transaction_id", $tid);
			$this->db->bind($tdetail, ":user_id", $user->id);
			$this->db->bind($tdetail, ":order_id", $order->id);
			$this->db->bind($tdetail, ":transaction_type", $order->payment_method);
			$this->db->bind($tdetail, ":total_price", $order->final_price);
			$this->db->bind($tdetail, ":payment_method", $order->payment_method);
			$this->db->bind($tdetail, ":payment_date", time());
			$this->db->bind($tdetail, ":notes", "");
			$this->db->bind($tdetail, ":notes_en", "");
			if ($this->db->execute($tdetail)) {
			}
		}
	}