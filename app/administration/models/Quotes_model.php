<?php 

class Quotes_model extends Controller {
    private $db;
    public $lang;
    public $_LANG;
    public $attempts = 3;
    public $delay_attempts = 5;

    public function __construct()
    {
        $this->db = new Database();
        $this->lang = $this->getLang();

    }
     public function getPage($id){
        try{
            $getPage = $this->db->query("SELECT * FROM tbl_pages_admin WHERE id = " .$id);
            return $this->db->resultSet($getPage);
        }catch(PDOException $e){
            return false;
        }
    }
    
    /* ---------------------------------------------TESS quotes------------------------------------- */
    
    
    /* 
    * Get Everything From quotes
    */
    public function getquotes($id){
        try{
            $query = $this->db->query("SELECT * FROM tbl_quotes WHERE id = " .$id);
            return $this->db->resultSet($query);
        }catch(PDOException $e){
            return false;
        }
    }
    public function getallquotes(){
        try{
            $query = $this->db->query("SELECT * FROM tbl_quotes ORDER BY id ASC");
            return $this->db->resultSet($query);
        }catch(PDOException $e){
            return false;
        }
    }
    
     
    public function AddEditQuotes($postData,$add=false){
        global $lang;
        if($add == false)
            unset($add);
        $response = [];
        $response["text"] = "";
        $response["color"] = ""; 
        $updates = "";
        $adds = "";
        $insert_sql1 = "";
        $insert_sql2 = ""; 
        $insert_inputs1 = "";
        $insert_inputs2 = "";
        $update_inputs = "";
        $can_insert = true;
        $add_customs = false;
        $binded = [];
        
        if($postData['name'] == "" )
            $can_insert = false;
        try{
            //Build string update
            foreach($postData as $key => $value){
                if ($key != 'B1' and $key != 'query'  and $key != 'supp'
                    and $key != 'url_id' and $key != 'url_id_en' and $key != 'inputs_id' and $key != 'submit'
                    and $key != 'edit_quotes' and $key != 'add_quotes' and $key != 'id_quotes' and $key != 'edit_ts'
                    ) {

                    $key = str_replace('*', '', $key);

                    $updates .= "`$key`= :$key, ";
                    $insert_sql1 .="`$key`, ";
                    $insert_sql2 .=":$key, ";
                    $binded[$key] = ( isset($value) ? $value : 1);
            
                }

            } 
			$serialize_binded = serialize($binded);
            $updates = substr($updates, 0, -2);
            $insert_sql1 = substr($insert_sql1, 0, -2);
            $insert_sql2 = substr($insert_sql2, 0, -2);

            //updates
            if(!isset($add)){ 
                $update = $this->db->query("UPDATE tbl_quotes SET quotes = :quotes WHERE id = :id"); 
                $this->db->bind($update,":quotes", $serialize_binded);  
                $this->db->bind($update,":id", $postData["id_quotes"]);  
                if($this->db->execute($update)){
                    
                    $response["message"] = $lang['edited_success'];
                    $response["color"] = "success";
                }else{
                    $response["message"] = $lang['edited_error'];
                    $response["color"] = "danger";
                }
            }else{
                //ADD
                if($can_insert){
                    $adds = $this->db->query("insert ignore into tbl_quotes ($insert_sql1) VALUES ($insert_sql2)"); 
                    
                    foreach($binded as $key => $value){
                       $this->db->bind($adds,":$key", $value); 
                    }
                    if($this->db->execute($adds)){
                         
                            $response["message"] = $lang['added_success'];
                            $response["color"] = "success";
                         
                    }else{
                        $response["message"] = $lang['added_error'];
                        $response["color"] = "danger";
                    }
                }else{
                    $response["message"] = $lang['added_error'];
                    $response["color"] = "danger";
                }
            }
            return $response;
        }catch(PDOException $e){
            return false;
        }
    }
    
    
     //DELETE TESS quotes
    public function DeleteQuotes($id){
        global $lang;
        $response = [];
        $response["text"] = "";
        $response["color"] = "";
        try{
            $query_delete_quotes = $this->db->query("DELETE FROM tbl_quotes WHERE id = :id");
            $this->db->bind($query_delete_quotes,":id", $id);
            if($this->db->execute($query_delete_quotes)){
           
                $response["message"] = $lang['deleted_success'];
                $response["color"] = "success";
                 
            }else{

                $response["message"] = $lang['deleted_error'];
                $response["color"] = "danger";
            }
            return $response;
        }catch(PDOException $e){
            return false;
        }
    } 
     
        
}
?>