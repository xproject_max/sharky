<?php
    class Page {
        private $db;

        public function __construct()
        {
            $this->db = new Database();
        }
        //Get admin page info
        public function getPage($id){
            try{
                $query = $this->db->query("SELECT * FROM tbl_pages_admin WHERE id = " .$id);
                return $this->db->resultSet($query);
            }catch(PDOException $e){
				return false;
			}
        }

        //Get website page info
        public function getPageWebsite($id){
            try{
                $query = $this->db->query("SELECT * FROM tbl_pages WHERE id = " .$id);
                return $this->db->resultSet($query);
            }catch(PDOException $e){
				return false;
			}
        }

        public function getModules($id){
            try{
                $query = $this->db->query("SELECT * FROM tess_controller WHERE id = " .$id);
                return $this->db->single($query);
            }catch(PDOException $e){
				return false;
			}
        }

        public function getInputs($id){
            try{
                $query = $this->db->query("SELECT * FROM inputs WHERE id = " .$id);
                return $this->db->single($query);
            }catch(PDOException $e){
				return false;
			}
        }

        public function getTemplates($id){
            try{
                $query = $this->db->query("SELECT * FROM templates WHERE id = " .$id);
                return $this->db->single($query);
            }catch(PDOException $e){
				return false;
			}
        }

        public function seekInPageWebsite($id){
            try{
                $response = [];
                $query_get_page = $this->db->query("SELECT * FROM tbl_pages WHERE id = :id ");
                $this->db->bind($query_get_page,":id", $id);
                $result_id = $this->db->resultSet($query_get_page);
                if($result_id){
                    $response['from_id'] = $result_id;
                }
                $query_seek = $this->db->query("SELECT * FROM tbl_pages WHERE
                    title LIKE CONCAT('%',:id,'%') ||
                    title_en LIKE CONCAT('%',:id,'%') ||
                    texte LIKE CONCAT('%',:id,'%') ||
                    texte_en LIKE CONCAT('%',:id,'%')
                    ");
                $this->db->bind($query_seek,":id", $id);
                $result_text = $this->db->resultSet($query_seek);
                if($result_text){
                    $response['from_text'] = $result_text;
                }
                return $response;
            }catch(PDOException $e){
				return false;
			}
        }

        public function seekInModuleWebsite($id){
            try{
                $response = [];
                $query_get_module = $this->db->query("SELECT * FROM tess_controller WHERE id = :id ");
                $this->db->bind($query_get_module,":id", $id);
                $result_id = $this->db->resultSet($query_get_module);
                if($result_id){
                    $response['from_id'] = $result_id;
                }
                $query_seek = $this->db->query("SELECT * FROM tess_controller WHERE
                    title LIKE CONCAT('%',:id,'%') ||
                    `desc` LIKE CONCAT('%',:id,'%') ||
                    module LIKE CONCAT('%',:id,'%')
                    ");
                $this->db->bind($query_seek,":id", $id);
                $result_text = $this->db->resultSet($query_seek);
                if($result_text){
                    $response['from_text'] = $result_text;
                }
                return $response;
            }catch(PDOException $e){
				return false;
			}
        }

        public function seekInTemplateWebsite($id){
            try{
                $response = [];
                $query_get_template = $this->db->query("SELECT * FROM templates WHERE id = :id ");
                $this->db->bind($query_get_template,":id", $id);
                $result_id = $this->db->resultSet($query_get_template);
                if($result_id){
                    $response['from_id'] = $result_id;
                }
                $query_seek = $this->db->query("SELECT * FROM templates WHERE
                    name LIKE CONCAT('%',:id,'%')
                    ");
                $this->db->bind($query_seek,":id", $id);
                $result_text = $this->db->resultSet($query_seek);
                if($result_text){
                    $response['from_text'] = $result_text;
                }
                return $response;
            }catch(PDOException $e){
				return false;
			}
        }

        public function seekInInputsWebsite($id){
            try{
                $response = [];
                $query_get_inputs = $this->db->query("SELECT * FROM inputs WHERE id = :id ");
                $this->db->bind($query_get_inputs,":id", $id);
                $result_id = $this->db->resultSet($query_get_inputs);
                if($result_id){
                    $response['from_id'] = $result_id;
                }
                $query_seek = $this->db->query("SELECT * FROM inputs WHERE
                    title LIKE CONCAT('%',:id,'%') ||
                    `note` LIKE CONCAT('%',:id,'%') ||
                    `type` LIKE CONCAT('%',:id,'%')
                    ");
                $this->db->bind($query_seek,":id", $id);
                $result_text = $this->db->resultSet($query_seek);
                if($result_text){
                    $response['from_text'] = $result_text;
                }
                return $response;
            }catch(PDOException $e){
				return false;
			}
        }
        public function updateOrdersInputs($postData){
            global $lang;
            $p = 0;

            foreach ($postData['id'] as $key => $value) {
                if (isset($postData['order'][$p])){
                    try{
                        $query_update_orders = $this->db->query("UPDATE inputs SET `order`= :orders WHERE id= :id");
                        $this->db->bind($query_update_orders,":orders", $postData['order'][$p]);
                        $this->db->bind($query_update_orders,":id", $value);
                        if($this->db->execute($query_update_orders)){
                            $msg = $lang['edited_success'];
                        }else{
                            $msg =  $lang['edited_error'];
                        }
                    }catch(PDOException $e){
                        return false;
                    }
                }
                $p++;
            }
            return $msg;
        }
        public function update_orders($postData){
            $p = 0;
            foreach ($postData['id'] as $key => $value) {
                global $lang;
                if (isset($postData['order'][$p])){
                    try{
                        $query_update_orders = $this->db->query("UPDATE tbl_pages SET `order` = :orders WHERE id= :id");
                        $this->db->bind($query_update_orders,":orders", $postData['order'][$p]);
                        $this->db->bind($query_update_orders,":id", $value);
                        if($this->db->execute($query_update_orders)){
                            $msg = $lang['edited_success'];
                        }else{
                            $msg =  $lang['edited_error'];
                        }
                    }catch(PDOException $e){
                        return false;
                    }
                }
                $p++;
            }
            return $msg;
        }

        public function update_onlines($postData){
            global $lang;
            foreach ($postData['online'] as $key => $value) {
                foreach($value as $key => $val){
                    try{
                        $query_update_online = $this->db->query("UPDATE tbl_pages SET online= :onlines WHERE id = :id");
                        $this->db->bind($query_update_online,":onlines", $val);
                        $this->db->bind($query_update_online,":id", $key);
                        if($this->db->execute($query_update_online)){
                            $msg = $lang['edited_success'];
                        }else{
                            $msg =  $lang['edited_error'];
                        }
                    }catch(PDOException $e){
                        return false;
                    }
                }
            }
            return $msg;
        }


        //DELETE PAGE
        public function DeletePageWebsite($id){
            global $lang;
            $response = [];
            $response["text"] = "";
            $response["color"] = "";
            try{
                $query_delete_page = $this->db->query("DELETE FROM tbl_pages WHERE id = :id");
                $this->db->bind($query_delete_page,":id", $id);
                if($this->db->execute($query_delete_page)){
                    $query_delete_urlpage = $this->db->query("DELETE FROM tbl_custom_urls WHERE page_id = :id");
                    $this->db->bind($query_delete_urlpage,":id", $id);
                    if($this->db->execute($query_delete_urlpage)){
                        $response["message"] = $lang['deleted_success'];
                        $response["color"] = "success";
                    }else{
                        $response["message"] = $lang['deleted_error'];
                        $response["color"] = "danger";
                    }
                }else{

                    $response["message"] = $lang['deleted_error'];
                    $response["color"] = "danger";
                }
                return $response;
            }catch(PDOException $e){
				return false;
			}
        }

        //DELETE PAGE
        public function DeleteModuleWebsite($id){
            global $lang;
            $response = [];
            $response["text"] = "";
            $response["color"] = "";
            try{
                $query_delete_module = $this->db->query("DELETE FROM tess_controller WHERE id = :id");
                $this->db->bind($query_delete_module,":id", $id);
                if($this->db->execute($query_delete_module)){

                    $response["message"] = $lang['deleted_success'];
                    $response["color"] = "success";

                }else{

                    $response["message"] = $lang['deleted_error'];
                    $response["color"] = "danger";
                }
                return $response;
            }catch(PDOException $e){
				return false;
			}
        }

		public function updatePageInputs($id){
			try{
				$select_ids = $this->db->query("SELECT * FROM tbl_pages_inputs WHERE fk_pages = :id");
				$this->db->bind($select_ids, ":id", $id);
				$serialized = "";
				$this->db->execute($select_ids);
                $i = 0;
				while($inputs = $this->db->fetch($select_ids)){
					$serialized .= ($i != 0 ? '-' : '').$inputs->id;
                    $i++;
				}
				$update = $this->db->query("UPDATE tbl_pages SET custom_inputs = :data WHERE id = :id");
				$this->db->bind($update, ":data", $serialized);
				$this->db->bind($update, ":id", $id);
				return $this->db->execute($update);
			}catch(PDOException $e){
				return false;
			}
		}

        //DELETE INPUTS
        public function DeleteInputsWebsite($id){
            global $lang;
            $response = [];
            $response["text"] = "";
            $response["color"] = "";
            try{
                $query_delete_input = $this->db->query("DELETE FROM inputs WHERE id = :id");
                $this->db->bind($query_delete_input,":id", $id);
                if($this->db->execute($query_delete_input)){
					$query_get_ids = $this->db->query("SELECT fk_pages FROM tbl_pages_inputs WHERE type = :id ");
					$this->db->bind($query_get_ids, ":id", $id);
					$ids = $this->db->resultSet($query_get_ids);
					$counted = $this->db->rowCount($query_get_ids);

                    $query_alter = $this->db->query("DELETE FROM `tbl_pages_inputs` WHERE type = :id");
                    $this->db->bind($query_alter,":id", $id);
                    if($this->db->execute($query_alter)){
						foreach($ids as $id_joined){
							$this->updatePageInputs($id_joined->fk_pages);
						}
                        $response["message"] = $lang['deleted_success']." ( + ".$counted.")";
                        $response["color"] = "success";
                    }else{
                        $response["message"] = $lang['deleted_error'];
                        $response["color"] = "danger";
                    }

                }else{

                    $response["message"] = $lang['deleted_error'];
                    $response["color"] = "danger";
                }
                return $response;
            }catch(PDOException $e){
				return false;
			}
        }

        //DELETE TEMPLATES
        public function DeleteTemplatesWebsite($id){
            global $lang;
            $response = [];
            $response["text"] = "";
            $response["color"] = "";
            try{
                $query_delete_template = $this->db->query("DELETE FROM templates WHERE id = :id");
                $this->db->bind($query_delete_template,":id", $id);
                if($this->db->execute($query_delete_template)){
                    $response["message"] = $lang['deleted_success'];
                    $response["color"] = "success";

                }else{

                    $response["message"] = $lang['deleted_error'];
                    $response["color"] = "danger";
                }
                return $response;
            }catch(PDOException $e){
				return false;
			}
        }

        public function checkIfExistJoinInputs($key, $pageid){
            $exist = $this->db->query("SELECT * FROM tbl_pages_inputs WHERE type = :key AND fk_pages = :fkpages");
            $this->db->bind($exist, ":key", $key);
            $this->db->bind($exist, ":fkpages", $pageid);
            if($this->db->single($exist)){
                return true;
            }else{
                return false;
            }
        }
        public function checkIfUrlExist($pageid){
            $exist = $this->db->query("SELECT * FROM tbl_custom_urls WHERE page_id = :pageid");
            $this->db->bind($exist, ":pageid", $pageid);
            if($this->db->single($exist)){
                return true;
            }else{
                return false;
            }
        }
        public function checkIfModulesExist($inputid){
            $exist = $this->db->query("SELECT * FROM tess_controller WHERE id = :inputid");
            $this->db->bind($exist, ":inputid", $inputid);
            if($this->db->single($exist)){
                return true;
            }else{
                return false;
            }
        }

        public function AddEditJoinInputs($key,$value,$pageid, $id=false){ // If id then edit
            if($id){
                $edit = $this->db->query("UPDATE tbl_pages_inputs SET `value` = :value WHERE `type` = :type AND `fk_pages` = :fkpages");
                $this->db->bind($edit,":value", $value);
                $this->db->bind($edit,":type", $key);
                $this->db->bind($edit,":fkpages", $pageid);
                if($this->db->execute($edit)){
                    $this->updatePageInputs($pageid);
                    return true;
                }else{
                    return false;
                }
            }else{
                $add = $this->db->query("INSERT INTO tbl_pages_inputs (`type`, `value`, `fk_pages`) VALUES(:type, :value, :fkpages )");
                $this->db->bind($add,":type", $key);
                $this->db->bind($add,":value", $value);
                $this->db->bind($add,":fkpages", $pageid);
                if($this->db->execute($add)){
                    $this->updatePageInputs($pageid);
                    return true;
                }else{
                // var_dump('test2'.$this->db->errorInfo($add)[2]);
                    return false;
                }
            }
        }

        public function updateRoot($root=0, $id=0){ 
            $edit = $this->db->query("UPDATE tbl_pages SET `root` = :root WHERE `id` = :id");
            $this->db->bind($edit,":root", $root);
            $this->db->bind($edit,":id", $id);
            if($this->db->execute($edit)){
                return true;
            }else{
                return false;
            }
        }
        public function AddEditPageWebsite($postData,$add=false){
            global $lang;
            if($add == false)
                unset($add);
            $response = [];
            $response["text"] = "";
            $response["color"] = "";


            $updates = "";
            $adds = "";
            $insert_sql1 = "";
            $insert_sql2 = "";

            /* custom inputs */
            $insert_inputs1 = "";
            $insert_inputs2 = "";
            $insert_sql3 ="";
            $update_inputs = "";
			$label_string = "";
            $can_insert = true;
            $binded = [];
            if($postData['title'] == "" || (!isset($postData['type'])))
                $can_insert = false;
            try{

                //Build string update
                foreach($postData as $key => $value){

					//label id == 112 a verifier live
					if($key == "labels"){
                        foreach($value as $valkey => $val){
                            if($val !== "")
                                $label_string .= "-".$val."-";
                        }
                    }
                    if ($key != 'B1' and $key != 'query'  and $key != 'supp'
                        and $key != 'url_id' and $key != 'url_id_en' and $key != 'labels'
                        and $key != 'edit_page' and $key != 'add_page'   and $key != 'edit_ts'
                        ) {

                        $key = str_replace('*', '', $key);
                        //Get inputs details
                        $query_inputs = $this->db->query("SELECT * FROM inputs where `name` = :key ");
                        $this->db->bind($query_inputs,":key", $key);
                        $inputs = $this->db->single($query_inputs);
                        // var_dump($inputs->id." - ".$key);
                        if($inputs){
                            if ($inputs->type == "Upload Image") {

                            }
                            else if ($inputs->type == "Checkbox" || $inputs->type == "Cat&eacute;gorie" ) {
                                $str = '';
                                foreach($_POST[$key] as $current){
                                    if($current != "")
                                        $str .= "-$current-";
                                }
                                $value = $str;

                            }
                            else {
                                //Nothing else yet
                            }

                            if($key == 'dtype' && is_array($_POST['dtype'])) {
                                $value="";
                                foreach($_POST['dtype'] as $page){
                                    if($i != 0){$value.= '-';}
                                        $value .= $page;
                                        $i++;
                                }
                            }
                        }

                        if(  $key !== "parent"
                        AND $key !== "order"
                        AND $key !== "type"
                        AND $key !== "seo_meta_title"
                        AND $key !== "seo_meta_title_en"
                        AND $key !== "seo_meta_description"
                        AND $key !== "seo_meta_description_en"
                        AND $key !== "seo_meta_title_tag"
                        AND $key !== "seo_meta_title_tag_en"
                        AND $key !== "title"
                        AND $key !== "title_en"
                        AND $key !== "texte"
                        AND $key !== "texte_en"
                        AND $key !== "image"
                        AND $key !== "alt"
                        AND $key !== "is_product"
                        AND $key !== "is_blog"
                        AND $key !== "create_ts"
                        AND $key !== "online" ){
                                if(isset($postData['id_page']) &&  $key !== "id_page"){
                                    // var_dump($key." - ".$inputs->id);
                                    if($this->checkIfExistJoinInputs($inputs->id, $postData["id_page"])){
                                        // edit
                                        $this->AddEditJoinInputs($inputs->id,$value,$postData["id_page"],$inputs->id);
                                    }else{
                                        // add
                                        $this->AddEditJoinInputs($inputs->id,$value,$postData["id_page"]);
                                    }
                                }
                        }else{
                            $updates .= "`$key`= :$key, ";
                            $insert_sql1 .="`$key`, ";
                            $insert_sql2 .=":$key, ";
                            $insert_sql3 .="$value, ";

                            $binded[$key] = ( isset($value) ? $value : 1);
                        }
                    }

                }
                //Delete last comma
                $updates = substr($updates, 0, -2);

                require_once APPROOT.'/helpers/function.php';
                //updates
                if(!isset($add)){
					$debuginsert = $this->db->query("INSERT INTO `tbl_debug` (`event`,`fk_users`) VALUES ( :event,  :author)");
                    $this->db->bind($debuginsert, ":event", "modification de la page \"".$postData["title"]."\" id: ".$postData["id_page"]."");
                    $this->db->bind($debuginsert, ":author", "[CMS] ".$_SESSION['user_name']."");
					$this->db->execute($debuginsert);
					$update = $this->db->query("UPDATE tbl_pages SET ".$updates.", edit_ts = '".time()."', last_edit_author = '".$_SESSION['user_name']."' WHERE id = :id");
                    foreach($binded as $key => $value){
                        $this->db->bind($update,":$key", $value);
                    } 
                    $this->db->bind($update,":id", $postData["id_page"]);
                    if($this->db->execute($update)){
                        if(isset($postData['url_id'])){
                            $upd = "";
                            $add1 = "";
                            $add2 = "";

                            if(MULTI_LANG){
                                foreach(LANGUAGES as $l){
                                    if($l != "fr"){
                                        $upd .= ", `url_id_".$l."` = :url_id_".$l."";
                                        $add1 .= ", `url_id_".$l."`";
                                        $add2 .= ", :url_id_".$l."";
                                    }
                                }
                            }
                            if($this->checkIfUrlExist($postData['id_page'])){
                                $query_url = $this->db->query("UPDATE tbl_custom_urls SET url_id = :url_id ". $upd." WHERE page_id = :page_id");
                            }else{
                                $query_url = $this->db->query("INSERT INTO tbl_custom_urls (`page_id`, `url_id` ". $add1.") VALUES ( :page_id, :url_id ".$add2.")");
                            }
                            $this->db->bind($query_url,":url_id", $postData['url_id']);
                            if(MULTI_LANG){
                                foreach(LANGUAGES as $l){
                                    if($l != "fr"){
                                        $this->db->bind($query_url,":url_id_".$l, $postData['url_id_'.$l]);
                                    }
                                }
                            }
                            $this->db->bind($query_url,":page_id", $postData["id_page"]);
                            if($this->db->execute($query_url)){
                                $response["message"] = $lang['edited_success'];
                                $response["color"] = "success";
                            }else{
                                echo print_r($query_url->errorInfo());
                                $response["message"] = "URL couldn be added";
                                $response["color"] = "danger";
                            }

                        }else{
                            $response["message"] = $lang['edited_success'];
                            $response["color"] = "success";
                        }
                    }else{
                        $response["message"] = $lang['edited_error'];
                        $response["color"] = "danger";
                    }

                    //Edit root
                    $root = getRoot($postData["id_page"]);
                    $this->updateRoot($root,$postData["id_page"]);
					//labels custom edit
					if($this->checkIfExistJoinInputs(44, $postData["id_page"])){
						// edit
						$this->AddEditJoinInputs(44,$label_string,$postData["id_page"],44);
					}else{
						// add
						$this->AddEditJoinInputs(44,$label_string,$postData["id_page"]);
					}
                }else{
                    //ADD
                    if($can_insert){
                        // var_dump($postData);
                        $order = $this->db->query("SELECT `order` FROM tbl_pages WHERE parent= :parent order by `order` desc limit 1");
                        $this->db->bind($order,":parent", (isset($postData['parent']) ? $postData['parent'] : 0));
                        $order = $this->db->single($order);
                        $lo = (1 + $order->order);
                        // var_dump("insert ignore into tbl_pages ($insert_sql1, `order`, `create_ts`) VALUES ($insert_sql3 '$lo', '".time()."')");
                        $adds = $this->db->query("insert ignore into tbl_pages ($insert_sql1 `order`, `create_ts`) VALUES ($insert_sql2 '$lo', '".time()."')");
                        foreach($binded as $key => $value){
                           $this->db->bind($adds,":$key", $value);
                        }
                        if($this->db->execute($adds)){

                            $current_page_id = $this->db->query("SELECT LAST_INSERT_ID() as 'lastid' FROM tbl_pages");
                            $current_page_id = $this->db->single($current_page_id);
                            if($current_page_id->lastid !== 0){
                                $current_page_id = $current_page_id->lastid;
                            }
                            //Edit root
                            $root = getRoot($current_page_id);
                            $this->updateRoot($root, $current_page_id);

                            if(isset($postData['url_id'])){
                                if(MULTI_LANG){
                                    $insert1 = "";
                                    $insert2 = "";
                                    $insert3 = array();
                                    foreach(LANGUAGES as $l){
                                        if($l != "fr"){
                                            $insert1 .= ", url_id_".$l;
                                            $insert2 .= ", :url_id_".$l;
                                        }
                                    }

                                    $insert_url = $this->db->query("INSERT INTO tbl_custom_urls(page_id, url_id ".$insert1.") VALUES( :page_id, :url_id". $insert2.")");
                                    $this->db->bind($insert_url,":page_id", $current_page_id);
                                    $this->db->bind($insert_url,":url_id", $postData['url_id']);
                                    foreach(LANGUAGES as $l){
                                        if($l != "fr"){
                                            $this->db->bind($insert_url,":url_id_".$l, $postData['url_id_'.$l]);
                                        }
                                    }
                                    if($this->db->execute($insert_url)){
                                        $response["message"] = $lang['added_success'];
                                        $response["color"] = "success";
                                    }else{
                                        $response["message"] = $lang['added_error'];
                                        $response["color"] = "danger";
                                    }

                                }else{
                                    $insert_url = $this->db->query("INSERT INTO tbl_custom_urls(page_id, url_id) VALUES( :page_id, :url_id)");
                                    $this->db->bind($insert_url,":page_id", $current_page_id);
                                    $this->db->bind($insert_url,":url_id", $postData['url_id']);
                                    if($this->db->single($insert_url)){
                                        $response["message"] = $lang['added_success'];
                                        $response["color"] = "success";
                                    }else{
                                        $response["message"] = $lang['added_error'];
                                        $response["color"] = "danger";
                                    }

                                }
                            }else{
                                $response["message"] = $lang['added_success'];
                                $response["color"] = "success";
                            }
                        }else{
                            $response["message"] = $lang['added_error'];
                            $response["color"] = "danger";
                        }



						//labels custom edit
						if($this->checkIfExistJoinInputs(44,$current_page_id)){
							// edit
							$this->AddEditJoinInputs(44,$label_string,$current_page_id,$inputs->id);
						}else{
							// add
							$this->AddEditJoinInputs(44,$label_string,$current_page_id);
						}
                    }else{
                        $response["message"] = $lang['added_error'];
                        $response["color"] = "danger";
                    }
                }
                return $response;
            }catch(PDOException $e){
				return false;
			}
        }
        public function AddEditModuleWebsite($postData, $add=false){
            global $lang;
            if(!$add){
                unset($add);
            }
            $i = 0;
            $response = [];
            $response["text"] = "";
            $response["color"] = "";
            $query_add1 = '';
            $query_add2 = '';
            $query_update1 = '';
            $binded = [];
            $dtype = "";
            $can_insert = true;

            if($postData['title'] == "" || (!isset($postData['templates'])))
                $can_insert = false;

            if(isset($postData['dtype'])){
                foreach($postData['dtype'] as $data){
                    if($i != 0){$dtype.= '-';}
                    $dtype .= $data;
                    $i++;
                }
            }
            $postData['dtype'] = $dtype;
            foreach($postData as $key => $value){

                if($key !== "id" AND $key !== "add_module" AND $key !== "module_id" AND $key !== "id_page" AND $key !== "edit_module"){

                    $query_add1 .= "`$key`, ";
                    $query_add2 .= ":$key, ";
                    $query_update1 .= "`$key`= :$key, ";

                    $binded[$key] = ( isset($value) ? $value : 0);
                }
            }
            $query_add1 = substr($query_add1, 0, -2);
            $query_add2 = substr($query_add2, 0, -2);
            $query_update1 = substr($query_update1, 0, -2);
            if(isset($add)){
                //ADD
                if($can_insert){
                    try{
                        $query_add = $this->db->query('insert into tess_controller ('.$query_add1.') VALUES ('.$query_add2.')');
                        foreach($binded as $key => $value){
                            $this->db->bind($query_add, ":$key", "$value");
                        }
                        if($this->db->execute($query_add)){
                            $response["message"] = $lang['added_success'];
                            $response["color"] = "success";
                        }else{
                            $response["message"] = $lang['added_error'];
                            $response["color"] = "danger";
                        }
                    }catch(PDOException $e){
        				return false;
        			}
                }else{
                    $response["message"] = $lang['added_error'];
                    $response["color"] = "danger";
                }
            }else{
                //EDIT
                try{
                    $query_update = $this->db->query('UPDATE tess_controller SET '.$query_update1.' WHERE id = :id');
                    foreach($binded as $key => $value){
                        $this->db->bind($query_update, ":$key", $value);
                    }
                    $this->db->bind($query_update, ":id", $postData['module_id']);
                    if($this->db->execute($query_update)){
                        $response["message"] = $lang['edited_success'];
                        $response["color"] = "success";
                    }else{
                        $response["message"] = $lang['edited_error'];
                        $response["color"] = "danger";
                    }
                }catch(PDOException $e){
    				return false;
    			}
            }
            return $response;
        }
        public function AddEditInputsWebsite($postData, $add=false){
            global $lang;
            if(!$add){
                unset($add);
            }
            $response = [];
            $response["text"] = "";
            $response["color"] = "";
            $can_insert = true;

            if($postData['name'] == "" || (!isset($postData['title'])))
                $can_insert = false;

            $query_add1 = "";
            $query_add2 = "";
            $update_str = "";
            foreach($postData as $key => $value){

                if($key !== "id" AND $key !== "add_inputs" AND $key !== "order" AND $key !== "inputs_id" AND $key !== "inputs_bilingue" AND $key !== "id_page" AND $key !== "edit_inputs"){



                    $query_add1 .= "`$key`, ";
                    $query_add2 .= ":$key, ";
                    $update_str .= "`$key`= :$key, ";

                    $binded[$key] = ( isset($value) ? $value : 0);
                }
            }
            $query_add1 = substr($query_add1, 0, -2);
            $query_add2 = substr($query_add2, 0, -2);
            $update_str = substr($update_str, 0, -2);

            if(isset($add)){
                //ADD
                if($can_insert){
                    try{
                        $max_order = $this->db->query("SELECT `order` FROM inputs order by `order` desc limit 1");
                        $max_order = $this->db->single($max_order);
                        $new_order = ($max_order->order + 1);
                        $query_insert = $this->db->query("INSERT INTO inputs (".$query_add1.", `order`) VALUES (".$query_add2.", :order);");
                        foreach($binded as $key => $value){
                            $this->db->bind($query_insert, ":$key", "$value");
                        }
                        $this->db->bind($query_insert, ":order", $new_order);
                        if($this->db->execute($query_insert)){

                            if(MULTI_LANG && isset($postData['inputs_bilingue']) && $postData['inputs_bilingue'] == 1){
                                foreach(LANGUAGES as $l){
                                    if($l != "fr"){

                                        $max_order = $this->db->query("SELECT `order` FROM inputs order by `order` desc limit 1");
                                        $max_order = $this->db->single($max_order);
                                        $new_order = ($max_order->order + 1);

                                        $query_insert_multi = $this->db->query("INSERT INTO inputs (".$query_add1.", `order`) VALUES (".$query_add2.", :order);");
                                        foreach($binded as $key => $value){
                                            if($key == "name"){
                                                $value = $value."_".$l;
                                            }
                                            else if($key == "title"){
                                                $value = $value. " ".strtoupper($l);
                                            }
                                            $this->db->bind($query_insert_multi, ":$key", "$value");
                                        }
                                        $this->db->bind($query_insert_multi, ":order", $new_order);
                                        if($this->db->execute($query_insert_multi)){

                                            $response["message"] = $lang['added_success'];
                                            $response["color"] = "success";
                                        }else{
                                            $response["message"] = $lang['added_error'];
                                            $response["color"] = "danger";

                                        }

                                    }
                                }
                            }else{

                                $response["message"] = $lang['added_success'];
                                $response["color"] = "success";
                            }
                        }else{

                            $response["message"] = $lang['added_error'];
                            $response["color"] = "danger";
                        }

                    }catch(PDOException $e){
        				return false;
        			}
                }else{
                    $response["message"] = $lang['added_error'];
                    $response["color"] = "danger";
                }
            }else{
                //EDIT
                try{
                    $query_nom = $this->db->query("SELECT id FROM inputs WHERE id = :id");
                    $this->db->bind($query_nom, ":id", $postData['inputs_id']);
                    $name = $this->db->single($query_nom);
                    $alter = $this->db->query("UPDATE `tbl_pages_inputs` SET `type` = :name WHERE type = ".$name->id."");
                    $this->db->bind($alter, ":name", $postData['name']);
                    if($this->db->execute($alter)){
                        //updated
                    }
                    $update = $this->db->query("UPDATE inputs SET ".$update_str." WHERE id = :id");
                    foreach($binded as $key => $value){
                        $this->db->bind($update, ":$key", "$value");
                    }
                    $this->db->bind($update, ":id", $postData['inputs_id']);
                    if($this->db->execute($update)){
                        $response["message"] = $lang['edited_success'];
                        $response["color"] = "success";
                    }else{
                        $response["message"] = $lang['edited_error'];
                        $response["color"] = "danger";
                    }
                }catch(PDOException $e){
                    //$this->error = $e->getMessage();
                    //echo $this->error;
    				return false;
    			}
            }
            return $response;
        }

        public function AddEditTemplatesWebsite($postData, $add=false){
            global $lang;
            if(!$add){
                unset($add);
            }
            $response = [];
            $response["text"] = "";
            $response["color"] = "";
            $can_insert = true;

            if($postData['name'] == "" )
                $can_insert = false;

            $query_add1 = "";
            $query_add2 = "";
            $update_str = "";
            $inputs = "";
            if (isset($postData['inputs'])) {
                foreach ($postData['inputs'] as $key => $value) {
                    $inputs .= "-$value-";

                }
            }

            foreach($postData as $key => $value){
                if($key !== "id" AND $key !== "add_templates" AND $key !== "checkbox" AND $key !== "templates_id" AND $key !== "inputs" AND $key !== "id_page" AND $key !== "edit_templates"){


                    $query_add1 .= "`$key`, ";
                    $query_add2 .= ":$key, ";
                    $update_str .= "`$key`= :$key, ";

                    $binded[$key] = ( isset($value) ? $value : 0);
                }
            }
            $query_add1 = substr($query_add1, 0, -2);
            $query_add2 = substr($query_add2, 0, -2);
            $update_str = substr($update_str, 0, -2);

            if(isset($add)){
                //ADD
                if($can_insert){
                    try{
                        $query_insert = $this->db->query("INSERT INTO templates (".$query_add1.", `inputs`) VALUES (".$query_add2.",:types);");
                        foreach($binded as $key => $value){
                            $this->db->bind($query_insert, ":$key", "$value");
                        }
                        $this->db->bind($query_insert, ":types", $inputs);
                        if($this->db->execute($query_insert)){

                            $response["message"] = $lang['added_success'];
                            $response["color"] = "success";
                        }else{

                            $response["message"] = $lang['added_error'];
                            $response["color"] = "danger";
                        }

                    }catch(PDOException $e){
        				return false;
        			}
                }else{
                    $response["message"] = $lang['added_error'];
                    $response["color"] = "danger";
                }
            }else{
                //EDIT

                try{
                    $update = $this->db->query("UPDATE templates SET ".$update_str.", `inputs` = :types WHERE id = :id");
                    foreach($binded as $key => $value){
                        $this->db->bind($update, ":$key", "$value");
                    }
                    $this->db->bind($update, ":types", $inputs);
                    $this->db->bind($update, ":id", $postData['templates_id']);
                    if($this->db->execute($update)){
                        $response["message"] = $lang['edited_success'];
                        $response["color"] = "success";
                    }else{
                        $response["message"] = $lang['edited_error'];
                        $response["color"] = "danger";
                    }
                }catch(PDOException $e){
                    // $this->error = $e->getMessage();
                    // echo $this->error;
    				return false;
    			}
            }
            return $response;
        }
        public function getAll(){
            try{
                $query_get_all = $this->db->query("SELECT tbl_pages.*, tess_controller.add, tess_controller.mod, tess_controller.del, tess_controller.dtype FROM tbl_pages LEFT JOIN tess_controller ON tbl_pages.type = tess_controller.id WHERE parent = 0");
                return $this->db->resultSet($query_get_all);
            }catch(PDOException $e){
				return false;
			}
        }

        public function getChildList($id, $level){

            global $Tess_db, $linv, $Tess, $Functions, $install_page_order, $_SESSION, $l;

            require_once APPROOT.'/helpers/function.php';
            $lang = new Lang();
            $l = "";
            if($lang->lang == "en")
                $l="_en";

            try{
                $query = $this->db->query("SELECT * FROM tbl_pages WHERE parent = :id ORDER BY `order` ASC");
                $this->db->bind($query,":id",$id);
                $child_bool = false;
                $childMenu ='';
                $key=0;
                $currents = $this->db->resultSet($query);
				 
                foreach($currents as $current){
                    // var_dump($current); 
						$_SESSION['saved_status_accordion'][$id] = $id; 
                    $childMenu.='<tr level="'.($level+1).'"  mid="'.$current->id.'" url="'.URLROOT.getUrl($current->id).'" url_linv="'.URLROOT.getUrl($current->id, $linv).'" class="firstchild '.(isset($_SESSION['saved_status_accordion']) && in_array($current->id, $_SESSION['saved_status_accordion']) ? "show_accordion_child" : "").'  Level_'.($level+1).'">';
                    //Possède des enfants?
                    $child_check = $this->db->query("SELECT * FROM tbl_pages WHERE parent = :id ");
                    $this->db->bind($child_check,":id", $current->id);
                    $child_check = $this->db->single($child_check);
                    if(!empty($child_check)){
                       $child_bool = true;
                    }
                    //Indentation
                    $i = 0;
                    $level_i ="";
                    $level_i .= '<img border="0"  src="images/icon1.png"><img border="0"  src="images/icon4.png">';
 //Déroulement
                    $childMenu.='<td style="width:76px">';

                            if($child_bool){
                                if(isset($_SESSION['pages-actives']) && $_SESSION['pages-actives'] == "actif"){
                                    $childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler page active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                                    $look_for_child = true;
                                } else if(isset($_SESSION['pages-actives']) && $_SESSION['pages-actives'] == "inactif") {
                                    $childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler page" style="cursor:pointer;" data-tooltip="Ouvrir la catégorie"><button type="button" rel="tooltip" class="btn btn-warning" data-original-title="" title="">
                                            <i class="fad fa-chevron-square-up arrows_menu"></i>
                                        </button></a>';
                                    $look_for_child = false;
                                } else if(3 <= $level) {
                                    $childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler page" style="cursor:pointer;" data-tooltip="Ouvrir la catégorie">
                                            <i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                                    $look_for_child = false;
                                } else if(3  > $level) {
                                    $childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler page active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fad fa-chevron-square-down  arrows_menu"></i> </a>';
                                    $look_for_child = true;
                                }else {
                                    $childMenu.= 'noob';
                                }
                            }
                            $child_bool = false;
                    $childMenu.= '</td> ';                  
				  $childMenu.= '<td level="'.($level+1).'"   >';
                    $childMenu.= '<input type="text" name="order[]" style="text-align:center;width:30px !important;background:black;color:white;" value="'.$current->order.'"><input type="hidden" name="id[]" style="width:15px !important;" value="'.$current->id.'"> ';

                    $bold=true;


                    $rights = $this->db->query('SELECT * FROM tess_controller WHERE id = :rights ');
                    $this->db->bind($rights,':rights', $current->type);
                    $rights = $this->db->resultSet($rights);
                    if(isset($rights[0]))
                        $rights = $rights[0];

                    $childMenu.='</td><td  class="title_table_list">';

                    if ($_SESSION['status'] <= $rights->mod or $_SESSION['status']=="1")
                      $childMenu.= '<div class="subcat">'.file_get_contents('../images/sub.svg').'</div><a style="line-height: 1.3" data-toggle="tooltip" data-placement="top"  data-original-title="ID: ' . $current->id . ' & Parent: ' . $current->parent . '"  data-description="'.$rights->desc.'" href="index.php?cp=page&mod=addmod&id=' . $current->id . '&parent='.$current->parent.'">';
                    $childMenu.= $bold ? "<b style='font-weight:800;'>" : "";
                   if($current->{'title'.$l} != '') {
				   $db = new Database();
				   $qgetmodule = $db->query("SELECT * FROM tess_controller WHERE id = :id");
				   $db->bind($qgetmodule, ":id", $current->type);
				   $db->execute($qgetmodule);
				   $module = $db->single($qgetmodule);
					   if($module->desc != ""){
						   $desc = $module->desc;
					   }else{
						   $desc = $module->title;
					   }
					  $childMenu .=  "<span class='moduleblock'>[".$desc."]</span> Titre: ".$current->{'title'.$l};
				   } else {
					  $childMenu .=  '(Sans title)';
				   }
				   // Si on a le drois de modifier
				   if (isset($_SESSION['status']) && $_SESSION['status'] <= $rights->mod or $_SESSION['status']=="1") {
					  $childMenu .=  '</a>';
				   } 
				    $childMenu.= '<div class="capture"></div>';
				   $childMenu .= '</td>'."\n";
					$dte = Date("d-m-y H:i:s",$current->edit_ts);
					$childMenu .= '<td>['.$dte.'] par '.($current->last_edit_author != "" ? $current->last_edit_author : "un inconnu" ).'</td>';
				   
                    $childMenu.= '
                                <td class="td-actions text-right">
                                    <div class="togglebutton">
                                        <label onclick="$(this).find(`input`).attr(`checked`);">
                                            <input name="online[]['.$current->id.']" value="0" type="hidden" />
                                            <input name="online[]['.$current->id.']" value="1" type="checkbox" '.($current->online == 1 ? 'checked=""' : '').'><span class="toggle"></span>
                                        </label>
                                    </div>
                                </td>
                                ';
                    $childMenu.= '<td class="td-actions text-right"   >';
                            $childMenu.= '<span class="td-actions text-right">
                                    ';
                            if ($_SESSION['status'] <= $rights->add){
                                if($rights->dtype != "") {
                                    $childMenu .=  '<a class="btn btn-info add_child" data-tooltip="Ajouter" dtype="'.$rights->dtype.'" parent="'.$current->id.'" href="index.php?cp=page&amp;mod=addmod&amp;ajout=1&amp;parent='.$current->id.'&amp;templates=""><i class="fas fa-plus-square"></i></a> ';
                                }
                            }
                            if ($_SESSION['status'] <= $rights->mod or $_SESSION['status'] == "1") {
                            $childMenu.= '<a class="btn btn-warning" data-toggle="tooltip" data-placement="top"  data-original-title="Type: ' . $current->type . '" title="Type: ' . $current->type . '" href="index.php?cp=page&mod=addmod&id=' . $current->id . '&parent='.$current->parent.'"><i class="fas fa-pen-square"></i><div class="ripple-container"></div></a> ';
                            }
                            if ($_SESSION['status'] <= $rights->del) {
                                $childMenu.= '<a id="'.$current->id.'"  class="btn btn-danger delete-btn"data-tooltip="Supprimer" ><i class="fas fa-times-square"></i></a> ';
                            }
                            $childMenu.='
                                    </span>';

                    $childMenu.='</td>'."\n";
                    $childMenu.= ' ';
                    if($child_bool&&$look_for_child)

                    $childMenu.= '</td>'."\n";
                    $childMenu.="</tr>";
                    $key++;
                }
                $_SESSION['pages-actives'][$id] = "actif";

                echo $childMenu;

            }catch(PDOException $e){
				return false;
			}

        }

        public function getMenu(){
            global $lang, $l;
            $lang = new Lang();
            $l = "";
            if($lang->lang == "en")
                $l="_en";
            $menu_array = [];
            $result = $this->getAll();
            $i=0;
            foreach($result as $row){

                $menu_array[$row->id] = array('name' => $row->{'title'.$l},'parent' => $row->parent,'mid' => $row->id,'order' => $row->order, 'add' => $row->{'add'}, 'del' => $row->{'del'}, 'mod' => $row->{'mod'}, 'type' => $row->type, 'dtype' => $row->dtype, 'online' => $row->online);
                $i++;
            }
            return $menu_array;
        }
    }
