<?php
class Media extends Controller {
    private $db;
    public $lang;
    public $_LANG;
    public $attempts = 3;
    public $delay_attempts = 5;

    public function __construct()
    {
        $this->db = new Database();
        $this->lang = $this->getLang();

    }
    public function getAll($id=""){
        try{
            if($id !== ""){
               $query_get_all = $this->db->query("
                SELECT tbl_pictures.*, um.*, r.name as status
                FROM tbl_pictures
                LEFT JOIN tbl_pictures_roles r
                    ON r.id = tbl_pictures.status
                LEFT JOIN tbl_pictures_categories um
                    ON tbl_pictures.tbl_pictures_categories = um.id 
                WHERE tbl_pictures.id = :id");
               $this->db->bind($query_get_all, ":id", $id);
                return $this->db->single($query_get_all);
            }else{
                $query_get_all = $this->db->query("                
                SELECT tbl_pictures.*, um.*, r.name as status
                FROM tbl_pictures
                LEFT JOIN tbl_pictures_roles r
                    ON r.id = tbl_pictures.status
                LEFT JOIN tbl_pictures_categories um
                    ON tbl_pictures.tbl_pictures_categories = um.id 
                ORDER BY id ASC");
           
                return $this->db->resultSet($query_get_all);
           }               
        }catch(PDOException $e){
            return false;
        }
    }

    public function getmedias($id){
        try{
            $query = $this->db->query("
            SELECT tbl_pictures.*, um.*,
            um.id as id FROM tbl_pictures
            RIGHT JOIN tbl_pictures_categories um
            ON find_in_set(um.id ,replace(tbl_pictures.categ, '-', ','))
            WHERE um.id = " .$id);
            return $this->db->resultSet($query);
        }catch(PDOException $e){
            return false;
        }
    }


    public function update_orders_imgcateg($postData){
        global $lang, $l;  
        
        $lang = object_to_array($lang);
        $imageIdsArray = $postData['imageIds'];

        $count = 1;
        foreach ($imageIdsArray as $id) {

            $sql = $this->db->query("UPDATE tbl_pictures SET `order` = :order WHERE `id` = :id");
            $imageOrder = $count;
            $imageId = $id;
            $this->db->bind($sql, ":order", $imageOrder);
            $this->db->bind($sql, ":id", $imageId); 
            if($this->db->execute($sql)){
                
                $response["message"] = $lang['edited_success'];
                $response["color"] = "success";
            }else{
                 
                $response["message"] = $lang['edited_error'];
                $response["color"] = "danger";
            }
            $count++;
            
        }
        return $response;
    }
    
    public function form_edit_image($id){
        global $lang, $l;
        $form = new Form();
        $html = "";
        
        $query_current = $this->db->query("SELECT * FROM tbl_pictures WHERE id = :id");
        $this->db->bind($query_current, ":id", $id);
        $this->db->execute($query_current);
        while($current = $this->db->fetch($query_current)){
            $html .= '
            <form class="edit_image_form" data-id="'.$current->id.'" action="" method="post"  enctype="multipart/form-data">
                <div class="delta grid_12 grid_1600_12">'.$form->input("picture", "Upload Image", "image", false, $current->picture, "","","", $current->id, "", "").'</div>
                <div class="clear"></div>
                <div class="alpha grid_6 grid_1600_12">'.$form->input("title", "text", $lang['title'], false, $current->title, "","","", $current->id, "", "").'</div>
                <div class="omega grid_6 grid_1600_12">'.$form->input("title_en", "text", $lang['title']." EN", false, $current->title_en, "","","", $current->id, "", "").'</div>
                <div class="clear"></div>
                <div class="alpha grid_6 grid_1600_12">'.$form->input("url", "text", "URL", false, $current->url, "","","", $current->id, "", "").'</div>
                <div class="omega grid_6 grid_1600_12">'.$form->input("url_en", "text", "URL en", false, $current->url_en, "","","", $current->id, "", "").'</div>
                <div class="clear"></div>
                <input type="submit" class="btn btn-info center" value="modifier" />
            </form>';
        }  
        echo $html;
    }
    
    public function update_current_image($postData){
        global $lang;
        
        $response = [];
        $response["text"] = "";
        $response["color"] = "";
        try{
            $pict = "";
            if(isset($postData['picture'])){
                $pict .= "picture = :picture,";
            }
            $query_update = $this->db->query("
            UPDATE tbl_pictures 
            SET ".$pict." title = :title, title_en = :title_en, url = :url, url_en = :url_en WHERE id = :id");
            
            if(isset($postData['picture'])){
                $this->db->bind($query_update, ":picture", $postData['picture']);
            }
            $this->db->bind($query_update, ":title", $postData['title']);
            $this->db->bind($query_update, ":title_en", $postData['title_en']);
            $this->db->bind($query_update, ":url", $postData['url']);
            $this->db->bind($query_update, ":url_en", $postData['url_en']);
            $this->db->bind($query_update, ":id", $postData['id']); 
            if($this->db->execute($query_update)){
               
                $response["message"] = $lang['edited_success'];
                $response["color"] = "success";
           

            }else{
              
                $response["message"] = $lang['edited_error'];
                $response["color"] = "danger";
            }
            return $response;
        }catch(PDOException $e){
            return false;
        }
    }
    
    public function insert_images($postData, $files){
        global $lang;
        
        $response = [];
        $response["text"] = "";
        $response["color"] = ""; 
        $insertValuesSQL = "";
        $upload_dir = dirname(dirname(APPROOT))."/public/uploads/".COMPANYNAME."/";
        // File upload configuration 
        $targetDir = "gallery_categ/".$postData['categ_id']."/"; 
        /* Create that folder if not exists */
        if(!is_dir(dirname(dirname(APPROOT))."/public/uploads/gallery_categ/")) {
            mkdir($upload_dir."gallery_categ/");
        }
        if(!is_dir(dirname(dirname(APPROOT))."/public/uploads/gallery_categ/".$postData['categ_id']."/")) {
            mkdir($upload_dir."gallery_categ/".$postData['categ_id']."/"); 
        }
        $allowTypes = array('jpg','png','jpeg','gif'); 

        $fileNames = array_filter($files['files']['name']); 
        if(!empty($fileNames)){ 
            $inc =0; 
            foreach($files['files']['name'] as $key=>$val){ 
                // File upload path 
                $fileName = basename($files['files']['name'][$key]); 
                $targetFilePath = $targetDir . $fileName; 

                // Check whether file type is valid 
                $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION); 
                if(in_array($fileType, $allowTypes)){ 
                    // Upload file to server 
                    if(move_uploaded_file($files["files"]["tmp_name"][$key], $upload_dir.$targetFilePath)){ 
                        // Image db insert sql 
                        $insertValuesSQL .= "(CONCAT('-',:categ,'-'), :filename_".$inc.", NOW()),"; 
                        $binded[":filename_".$inc.""] = $targetFilePath;
                        $inc++;
                    }else{  
                    } 
                }else{  
                } 
            } 

            if(!empty($insertValuesSQL)){ 
                $insertValuesSQL = trim($insertValuesSQL, ','); 
                // Insert image file name into database 
                $insert_query = $this->db->query("INSERT INTO tbl_pictures (categ, picture, date) VALUES ".$insertValuesSQL.""); 
                
               foreach($binded as $bind => $value){
                    $this->db->bind($insert_query, $bind, $value);
                }
                $this->db->bind($insert_query, ":categ", $postData['categ_id']);
                if($this->db->execute($insert_query)){ 
                    
                    $response["message"] = $lang['added_success'];
                    $response["color"] = "success";
                }else{ 
                    $response["message"] = $lang['added_error'];
                    $response["color"] = "danger"; 
                } 
            }  
        }else{ 
           
            $response["message"] = $lang['added_error'];
            $response["color"] = "danger";
        }   
        return $response;
        
    }
    
    public function getMenu($id=""){
        global $lang, $l;
        $lang = new Lang();
        $l = "";
        if($lang->lang == "en")
            $l="_en";
        $menu_array = [];
        $custom = [];
        $result = $this->getAll($id);
        $i=0;
        if($id){
            $menu_array[$i] = array(
                'id' => $result->id,
                'profile_image' => $result->profile_image,
                'username' => $result->username,
                'password' => $result->password,
                'email' => $result->email,
                'status' => $result->status,
                'date_created' => $result->date_created,
                'permissions' => $result->status,
                'online' => $result->online,
            ); 
        }else{
            foreach($result as $row){

                $menu_array[$i] = array(
                    'id' => $row->id,
                    'profile_image' => $row->profile_image,
                    'username' => $row->username,
                    'password' => $row->password,
                    'email' => $row->email,
                    'status' => $row->status,
                    'date_created' => $row->date_created,
                    'permissions' => $row->status,
                    'online' => $row->online,
                ); 
                $i++;
            }
        }
        return $menu_array; 
    }
 
    public function DeletecompaniesWebsite($id){
        global $lang;
        $response = [];
        $response["text"] = "";
        $response["color"] = "";
        try{
            $query_get_meta = $this->db->query("SELECT tbl_pictures_categories FROM tbl_pictures WHERE id = :id");
            $this->db->bind($query_get_meta, ":id", $id);
            $id_meta = $this->db->single($query_get_meta);
            $query_delete_companies = $this->db->query("DELETE FROM tbl_pictures WHERE id = :id");
            $this->db->bind($query_delete_companies,":id", $id);
            if($this->db->execute($query_delete_companies)){
                $query_delete_meta = $this->db->query("DELETE FROM tbl_pictures_categories WHERE id = :id");
                $this->db->bind($query_delete_meta,":id", $id_meta->tbl_pictures_categories);
                if($this->db->execute($query_delete_meta)){

                    $response["message"] = $lang['deleted_success'];
                    $response["color"] = "success";
                }else{
                    
                    $response["message"] = $lang['deleted_error'];
                    $response["color"] = "danger";
                }

            }else{

                $response["message"] = $lang['deleted_error'];
                $response["color"] = "danger";
            }
            return $response;
        }catch(PDOException $e){
            return false;
        }
    }
    
    public function AddEditcompanies($postData,$add=false){
        global $lang;
        if($add == false)
            unset($add);
        $response = [];
        $response["text"] = "";
        $response["verif"] = "";
        $response["color"] = "";


        $updates = "";
        $updatesd = "";
        $adds = "";
        $insert_sql1 = "";
        $insert_sql2 = "";
        $permission_string = "";

        /* custom inputs */
        $insert_inputs1 = "";
        $insert_inputs2 = "";
        $update_inputs = "";
        $can_insert = true;
        $binded = [];
        if($postData['username'] == "")
            $can_insert = false;
        try{
            //Build string update
            foreach($postData as $key => $value){
                if($key == "inputs"){ 
                    foreach($value as $valkey => $val){
                        if($val !== "")
                            $permission_string .= "-".$val."-";
                    }
                }
                if ($key != 'B1' and $key != 'query'  and $key != 'supp' 
                    and $key != 'edit_company' and $key != 'add_company'
                    and $key != 'id_page' and $key != 'inputs'
                    and $key != 'date_created'
                    and $key != 'passe2' 
                    and $key != (isset($add) ? '' : 'id_company')
                ) 
                {
                    
                    if($key == "password"){
                        if($postData['passe2'] !== ""){
                            if($value == $postData['passe2']){
                                if(!isset($add) && $value !== ""){
                                    if(!$this->checkIfSamePassword($value,$postData["id_company"])){
                                        $value = $this->encryptedPassword($value);
                                        $postData['password'] = $this->encryptedPassword($value);
                                    }else{

                                        $response["verif"] .= $lang['password_is_same']."</br>";
                                    }
                                }else{
                                    $value = $this->encryptedPassword($value);
                                    $postData['password'] = $this->encryptedPassword($value);
                                }
                            }else{
                                $response["verif"] .= $lang['passwords_not_match']."</br>";
                            }
                        }
                    }
                    
                    if($key == "email"){
                        if(isset($add)){
                            if($this->CheckIfEmailExist($value)){
                                $response["verif"] .= $lang['email_exists']."</br>";
                            }
                        }else{
                            if($this->CheckIfEmailExist($value,$postData["id_company"])){
                                $response["verif"] .= $lang['email_exists']."</br>";
                            }                                
                        }
                    }
                    $key_table = "tbl_pictures";
                    if(
                        $key == "company_name" ||
                        $key == "contact_name" ||
                        $key == "region" ||
                        $key == "city" ||
                        $key == "address" ||
                        $key == "zipcode" || 
                        $key == "phone" ||
                        $key == "profile_image"
                    ){
                        $key_table = "tbl_pictures_categories";
                        if($value == "" || ($key == "profile_image" and !is_string($value))){
                            $response["verif"] .= $key." null </br>";
                        }
                    }
                    
                    if($key == "username"){
                        if(isset($add)){
                            if($this->CheckIfusernameExist($value)){
                                $response["verif"] .= $lang['username_exists']."</br>";
                            }
                        }else{
                            if($this->CheckIfusernameExist($value,$postData["id_company"])){
                                $response["verif"] .= $lang['username_exists']."</br>";
                            }                                
                        }
                    }

                    $key = str_replace('*', '', $key); 
                 
                    $updates .= "`$key_table`.`$key`= :$key, ";
                    // $updatesd .= "`$key_table`.`$key`= '$value', "; 

                    $binded[$key] = ( isset($value) ? $value : 1); 
                }

            }
            // var_dump($updates);
            //Delete last comma
            $updates = substr($updates, 0, -2);

            //updates
            if(!isset($add)){
                if($response["verif"] !== ""){ 
                    $response["message"] =  substr($response["verif"], 0, -5);
                    $response["color"] = "danger";
                }else{
                    $update = $this->db->query("UPDATE tbl_pictures LEFT JOIN tbl_pictures_categories ON tbl_pictures_categories.id = tbl_pictures.tbl_pictures_categories  SET ".$updates.", `tbl_pictures`.`permissions`=:permissions  WHERE tbl_pictures.id = :id");
                    foreach($binded as $key => $value){
                        $this->db->bind($update,":$key", $value);
                    }  
                    // echo "UPDATE tbl_pictures LEFT JOIN tbl_pictures_categories ON tbl_pictures_categories.id = tbl_pictures.tbl_pictures_categories SET ".$updates.", `permissions`=:permissions  WHERE id = :id </br>";
                    // echo "UPDATE tbl_pictures LEFT JOIN tbl_pictures_categories ON tbl_pictures_categories.id = tbl_pictures.tbl_pictures_categories SET ".$updatesd." `permissions`=:permissions  WHERE id = :id </br>";
                    // foreach($binded as $key => $value){
                        // echo "this->db->bind(update,:".$key,", ".$value.") </br>";
                    // } 
                    
                    // echo "this->db->bind(permissions,:permissions, ".$permission_string.") </br>";
                    // echo "this->db->bind(id,:id, ".$postData["id_company"].")";
                    $this->db->bind($update,":permissions", $permission_string);
                    $this->db->bind($update,":id", $postData["id_company"]); 
                    if($this->db->execute($update)){
                        
                        $response["message"] = $lang['edited_success'];
                        $response["color"] = "success";
                    }else{
                         
                        $response["message"] = $lang['edited_error'];
                        $response["color"] = "danger";
                    }
                }
            }else{
                //ADD
                if($can_insert){
                    if($response["verif"] !== ""){
                        $response["message"] = $response["verif"];
                        $response["color"] = "danger"; 
                    }else{
                        
                        $adds = $this->db->query("
                        insert ignore into tbl_pictures_categories (
                            `company_name`, 
                            `contact_name`, 
                            `country`, 
                            `state`, 
                            `region`, 
                            `city`, 
                            `address`, 
                            `zipcode`,   
                            `phone`, 
                            `profile_image`
                        ) VALUES ( 
                            :company_name, 
                            :contact_name, 
                            :country, 
                            :state, 
                            :region, 
                            :city, 
                            :address, 
                            :zipcode,   
                            :phone, 
                            :profile_image
                        )"); 
                        $this->db->bind($adds,":company_name", (isset($postData['company_name']) ? $postData['company_name'] : "")); 
                        $this->db->bind($adds,":contact_name", $postData['contact_name']); 
                        $this->db->bind($adds,":country", (isset($postData['country']) ? $postData['country'] : "")); 
                        $this->db->bind($adds,":state", (isset($postData['state']) ? $postData['state'] : "")); 
                        $this->db->bind($adds,":region", $postData['region']); 
                        $this->db->bind($adds,":city", $postData['city']); 
                        $this->db->bind($adds,":address", $postData['address']); 
                        $this->db->bind($adds,":zipcode", $postData['zipcode']);  
                        $this->db->bind($adds,":phone", $postData['phone']); 
                        $this->db->bind($adds,":profile_image", $postData['profile_image']); 
                        if($this->db->execute($adds)){
                            
                            $current_page_id = $this->db->query("SELECT LAST_INSERT_ID() as 'lastid' FROM tbl_pictures_categories");
                            $current_page_id = $this->db->single($current_page_id);
                            if($current_page_id->lastid !== 0){
                                $current_page_id = $current_page_id->lastid;
                            }
                            
                            $insert_url = $this->db->query("
                            INSERT INTO tbl_pictures(
                                `fk_role`,
                                `tbl_pictures_categories`,
                                `email`,
                                `username`,
                                `password`,
                                `permissions`,
                                `date_company_created`, 
                                `online` 
                            ) VALUES(
                                :fk_role, 
                                :tbl_pictures_categories, 
                                :email, 
                                :username, 
                                :password, 
                                :permissions, 
                                '".date("Y-m-d")."',
                                :online
                            )"); 

                            $this->db->bind($insert_url,":fk_role", $postData['fk_role']);
                            $this->db->bind($insert_url,":tbl_pictures_categories", $current_page_id);
                            $this->db->bind($insert_url,":email", $postData['email']);
                            $this->db->bind($insert_url,":username", $postData['username']);
                            $this->db->bind($insert_url,":password", $postData['password']);
                            $this->db->bind($insert_url,":permissions", $permission_string);
                            $this->db->bind($insert_url,":online", $postData['online']);
                             
                            if($this->db->execute($insert_url)){
                                $response["message"] = $lang['added_success'];
                                $response["color"] = "success";
                            }else{
                                // echo  print_r($insert_url->errorInfo()); 
                                $response["message"] = $lang['added_error'];
                                $response["color"] = "danger";
                            }
                        }else{
                            // echo  print_r($adds->errorInfo()); 
                            $response["message"] = $lang['added_error'];
                            $response["color"] = "danger";
                        }
                    }
                }else{ 
                    $response["message"] = $lang['added_error'];
                    $response["color"] = "danger";
                }
            }
            return $response;
        }catch(PDOException $e){
            return false;
        }
    }
    
    /* Add credit and add transactions */
    
    public function add_credit($id, $postData){
        $_POST = $postData;
        $message = array();
        $transaction = new Transaction();
     
        if(isset($_POST['payment_method']) AND ($_POST['credit'] != "" || $_POST['payment_method'] == 0 || $_POST['payment_method'] == "")){
            if($_POST['description_credit'] == "" || $_POST['description_credit_en'] == ""){
                $return['color'] = 'danger';
                $return['message'] = 'Veuillez écrire une description ou moyen de paiement pour le crédit ajouté';
            } 
            else{
                $query_company = $this->db->query('SELECT * FROM tbl_pictures WHERE id = :id_company');
                $this->db->bind($query_company, ":id_company", $_POST['id']);
                $company = object_to_array($this->db->single($query_company, 'array'));
                $credit = str_replace('$','',$_POST['credit']);
                $credit_pre = str_replace(',','.',$credit); 
                if(strpos($credit_pre,'-')){
                    $credit = str_replace('-','',$credit_pre);
                    $credit = $company['solde'] - $credit;
                }else{
                    $credit = $company['solde'] + $credit_pre;
                }

                //echo $credit;
                $update = $this->db->query('
                    UPDATE `tbl_pictures`
                    SET `solde`= :credit
                    WHERE `id` = :id
                ');
                $this->db->bind($update, ":credit", $credit);
                $this->db->bind($update, ":id", $_POST['id']);

                if($this->db->rowCount($update) > 0){
                    $transaction->add_transaction($_POST, $credit_pre, "Company");
                 
                    $return['color'] = 'success';
                    $return['message'] = 'Vous avez bien changé le crédit de se compte'; 
                }else{
                    $return['color'] = 'danger';
                    $return['message'] = 'Une erreur est survenue durant le changement du crédit';
                }
            }
            unset($_POST);
        }else{
            $return['color'] = 'danger';
            $return['message'] = 'Vous devez ajouter un montant et un moyen de paiement';
        }
        return $return;
    }
}
?>