<?php
class Dictionary_model extends Controller {
    private $db;
    public $lang;
    public $_LANG;
    public $attempts = 3;
    public $delay_attempts = 5;

    public function __construct()
    {
        $this->db = new Database();
        $this->lang = $this->getLang();

    }
 
    public function getPage($id){
        try{
            $getPage = $this->db->query("SELECT * FROM tbl_pages_admin WHERE id = " .$id);
            return $this->db->resultSet($getPage);
        }catch(PDOException $e){
            return false;
        }
    } 
    public function getAll(){
        try{
            $query_get_all = $this->db->query("SELECT * FROM dictionary_categ");
            return $this->db->resultSet($query_get_all);
        }catch(PDOException $e){
            return false;
        }
    }
    public function getMenu(){
        global $lang, $l;
        $lang = new Lang();
        $l = "";
        if($lang->lang == "en")
            $l="_en";
        $menu_array = [];
        $result = $this->getAll();
        $i=0;
        foreach($result as $row){

            $menu_array[$row->id] = array('name' => $row->{'name'},'mid' => $row->id,'order' => $row->list_order);
            $i++;
        }
        return $menu_array;
    }
    
    /* ------------------------------- CATEGORIES ---------------------------------- */ 
    
    /* 
    * Get Everything From  CATEG
    */
    public function getcategories($id){
        try{
            $query = $this->db->query("SELECT * FROM dictionary_categ WHERE id = " .$id);
            return $this->db->resultSet($query);
        }catch(PDOException $e){
            return false;
        }
    }
    
    /*
    * Update rights order
    */
    public function update_order($postData){
        $p = 0;
        foreach ($postData['id'] as $key => $value) {
            global $lang;
            if (isset($postData['list_order'][$p])){
                try{
                    $query_update_orders = $this->db->query("UPDATE dictionary_categ SET `list_order` = :list_order WHERE id= :id");
                    $this->db->bind($query_update_orders,":list_order", $postData['list_order'][$p]);
                    $this->db->bind($query_update_orders,":id", $value);
                    if($this->db->execute($query_update_orders)){
                        $msg = $lang['edited_success'];
                    }else{
                        $msg =  $lang['edited_error'];
                    }
                }catch(PDOException $e){
                    return false;
                }
            }
            $p++;
        }
        return $msg;
    }
    
    
    
    //Search categories
    public function seekIncategories($id){
        try{
            $response = [];
            $query_get_categories = $this->db->query("SELECT * FROM dictionary_categ WHERE id = :id ");
            $this->db->bind($query_get_categories,":id", $id);
            $result_id = $this->db->resultSet($query_get_categories);
            if($result_id){
                $response['from_id'] = $result_id;
            }
            $query_seek = $this->db->query("SELECT * FROM tess_members_categories WHERE
                name LIKE CONCAT('%',:id,'%') ||
                list_order LIKE CONCAT('%',:id,'%') 
                ");
            $this->db->bind($query_seek,":id", $id);
            $result_text = $this->db->resultSet($query_seek);
            if($result_text){
                $response['from_text'] = $result_text;
            }
            return $response;
        }catch(PDOException $e){
            return false;
        }
    }
    
    //add or edit tess categories
    public function AddEditcategories($postData,$add=false){
        global $lang;
        if($add == false)
            unset($add);
        $response = [];
        $response["text"] = "";
        $response["color"] = ""; 
        $updates = "";
        $adds = "";
        $insert_sql1 = "";
        $insert_sql2 = ""; 
        $insert_inputs1 = "";
        $insert_inputs2 = "";
        $update_inputs = "";
        $can_insert = true;
        $add_customs = false;
        $binded = [];
        
        if($postData['name'] == "" )
            $can_insert = false;
        try{
            //Build string update
            foreach($postData as $key => $value){
                if ($key != 'B1' and $key != 'query'  and $key != 'supp'
                    and $key != 'url_id' and $key != 'url_id_en' and $key != 'inputs_id'
                    and $key != 'edit_categories' and $key != 'add_categories' and $key != 'id_categories' and $key != 'edit_ts'
                    ) {

                    $key = str_replace('*', '', $key);

                    $updates .= "`$key`= :$key, ";
                    $insert_sql1 .="`$key`, ";
                    $insert_sql2 .=":$key, ";
                    $binded[$key] = ( isset($value) ? $value : 1);
            
                }

            } 
            $updates = substr($updates, 0, -2);
            $insert_sql1 = substr($insert_sql1, 0, -2);
            $insert_sql2 = substr($insert_sql2, 0, -2);

            //updates
            if(!isset($add)){ 
                $update = $this->db->query("UPDATE dictionary_categ SET ".$updates." WHERE id = :id");
                foreach($binded as $key => $value){
                    $this->db->bind($update,":$key", $value);
                } 
                $this->db->bind($update,":id", $postData["id_categories"]);  
                if($this->db->execute($update)){
                    
                    $response["message"] = $lang['edited_success'];
                    $response["color"] = "success";
                }else{
                    $response["message"] = $lang['edited_error'];
                    $response["color"] = "danger";
                }
            }else{
                //ADD
                if($can_insert){
                    $adds = $this->db->query("insert ignore into dictionary_categ ($insert_sql1) VALUES ($insert_sql2)"); 
                    
                    foreach($binded as $key => $value){
                       $this->db->bind($adds,":$key", $value); 
                    }
                    if($this->db->execute($adds)){
                         
                            $response["message"] = $lang['added_success'];
                            $response["color"] = "success";
                         
                    }else{
                        $response["message"] = $lang['added_error'];
                        $response["color"] = "danger";
                    }
                }else{
                    $response["message"] = $lang['added_error'];
                    $response["color"] = "danger";
                }
            }
            return $response;
        }catch(PDOException $e){
            return false;
        }
    }
    
    
     //DELETE TESS ROLES
    public function DeleteCategories($id){
        global $lang;
        $response = [];
        $response["text"] = "";
        $response["color"] = "";
        try{
            $query_delete_categories = $this->db->query("DELETE FROM dictionary_categ WHERE id = :id");
            $this->db->bind($query_delete_categories,":id", $id);
            if($this->db->execute($query_delete_categories)){
           
                $response["message"] = $lang['deleted_success'];
                $response["color"] = "success";
                 
            }else{

                $response["message"] = $lang['deleted_error'];
                $response["color"] = "danger";
            }
            return $response;
        }catch(PDOException $e){
            return false;
        }
    } 
    
    
    /* ------------------------------- TERMS ---------------------------------- */
    
    public function getChildTerms($id, $level){

        global $Tess_db, $linv, $Tess, $Functions, $install_page_order, $_SESSION, $l; 
        require_once APPROOT.'/helpers/function.php';
        $lang = new Lang();
        $l = "";
        if($lang->lang == "en")
            $l="_en";

        try{
            $query = $this->db->query("SELECT * FROM dictionary_term WHERE categ = :id ORDER BY `list_order` ASC");
            $this->db->bind($query,":id",$id);
            $child_bool = false;
            $childMenu ='';
            $key=0;
            // if(!$this->db->execute($query)){
                
                // echo  print_r($query->errorInfo()); 
            // }
            $currents = $this->db->resultSet($query);
            foreach($currents as $current){
                // var_dump($current);
                $childMenu.='<tr level="'.($level+1).'" mid="'.$current->id.'" url="'.URLROOT.getUrl($current->id).'" url_linv="'.URLROOT.getUrl($current->id, $linv).'" class="firstchild  Level_'.($level+1).'">';
                
                //Indentation
                $i = 0;
                $level_i ="";
                $level_i .= '<img border="0"  src="images/icon1.png"><img border="0"  src="images/icon4.png">';
                $childMenu.= '<td level="'.($level+1).'"   >';
                $childMenu.= '<input type="text" name="order[]" style="text-align:center;width:30px !important;background:black;color:white;" value="'.$current->list_order.'"><input type="hidden" name="id[]" style="width:15px !important;" value="'.$current->id.'"> ';

                $bold=true;


                

                $childMenu.='</td><td  class="title_table_list">';

                 
                $childMenu.= '<div class="subcat">'.file_get_contents('../images/sub.svg').'</div><a href="#" style="line-height: 1.3" data-toggle="tooltip" data-placement="top"  id="' . $current->id . '">';
                $childMenu.= $bold ? "<b style='font-weight:800;'>" : "";
                if($current->term_id != '')
                    $childMenu .= $current->{'term_id'};
                else
                    $childMenu .= '(Sans title)';
                $childMenu.= $bold ? "</b>" : ""; 
                  $childMenu.= '</a>';

                $childMenu.='</td>';  
                $childMenu.='<td></td>';  
                $childMenu.= '<td class="td-actions text-right"   >';
                        $childMenu.= '<span class="td-actions text-right">
                                ';
                          
                        $childMenu.= '<a id="'.$current->id.'" class="btn btn-warning" data-toggle="tooltip" data-placement="top" href="#"><i class="fas fa-pen-square"></i><div class="ripple-container"></div></a> ';
                        
                            $childMenu.= '<a id="'.$current->id.'"  class="btn btn-danger delete-btn-terms"data-tooltip="Supprimer" ><i class="fas fa-times-square"></i></a> ';
                       
                        $childMenu.='
                                </span>';

                $childMenu.='</td>'."\n";
                $childMenu.= ' ';
                if($child_bool&&$look_for_child)

                $childMenu.= '</td>'."\n";
                $childMenu.="</tr>";
                $key++;
            }
            $_SESSION['pages-actives'][$id] = "actif";
            echo $childMenu;
        }catch(PDOException $e){
            return false;
        }

    }
        /* 
    * Get Everything From  CATEG
    */
    public function getterms($id){
        try{
            $query = $this->db->query("SELECT * FROM dictionary_term WHERE id = " .$id);
            return $this->db->resultSet($query);
        }catch(PDOException $e){
            return false;
        }
    }
    
    /*
    * Update rights order
    */
    public function update_order_terms($postData){
        $p = 0;
        foreach ($postData['id'] as $key => $value) {
            global $lang;
            if (isset($postData['list_order'][$p])){
                try{
                    $query_update_orders = $this->db->query("UPDATE dictionary_term SET `list_order` = :list_order WHERE id= :id");
                    $this->db->bind($query_update_orders,":list_order", $postData['list_order'][$p]);
                    $this->db->bind($query_update_orders,":id", $value);
                    if($this->db->execute($query_update_orders)){
                        $msg = $lang['edited_success'];
                    }else{
                        $msg =  $lang['edited_error'];
                    }
                }catch(PDOException $e){
                    return false;
                }
            }
            $p++;
        }
        return $msg;
    }
    
    
    
    //Search terms
    public function seekInterms($id){
        try{
            $response = [];
            $query_get_terms = $this->db->query("SELECT * FROM dictionary_term WHERE id = :id ");
            $this->db->bind($query_get_terms,":id", $id);
            $result_id = $this->db->resultSet($query_get_terms);
            if($result_id){
                $response['from_id'] = $result_id;
            }
            $query_seek = $this->db->query("SELECT * FROM tess_members_terms WHERE
                name LIKE CONCAT('%',:id,'%') ||
                list_order LIKE CONCAT('%',:id,'%') 
                ");
            $this->db->bind($query_seek,":id", $id);
            $result_text = $this->db->resultSet($query_seek);
            if($result_text){
                $response['from_text'] = $result_text;
            }
            return $response;
        }catch(PDOException $e){
            return false;
        }
    }
    
    //add or edit tess terms
    public function AddEditterms($postData,$add=false){
        global $lang;
        if($add == false)
            unset($add);
        $response = [];
        $response["text"] = "";
        $response["color"] = ""; 
        $updates = "";
        $adds = "";
        $insert_sql1 = "";
        $insert_sql2 = ""; 
        $insert_inputs1 = "";
        $insert_inputs2 = "";
        $update_inputs = "";
        $can_insert = true;
        $add_customs = false;
        $binded = [];
        $add_terms = false;
        if($postData['term_id'] == "" )
            $can_insert = false;
        try{
            //Build string update
            foreach($postData as $key => $value){
                if ($key != 'B1' and $key != 'query'  and $key != 'supp'
                    and $key != 'url_id' and $key != 'url_id_en' and $key != 'inputs_id'
                    and $key != 'edit_terms' and $key != 'add_terms' and $key != 'id_terms' and $key != 'edit_ts'
                    and $key != 'text_value_string'
                    ) {

                    $key = str_replace('*', '', $key);

                    $updates .= "`$key`= :$key, ";
                    $insert_sql1 .="`$key`, ";
                    $insert_sql2 .=":$key, ";
                    $binded[$key] = ( isset($value) ? $value : 1);
            
                }
                
                if($key == 'text_value_string'){
                    $add_terms = true;
                }

            } 
            $updates = substr($updates, 0, -2);
            $insert_sql1 = substr($insert_sql1, 0, -2);
            $insert_sql2 = substr($insert_sql2, 0, -2);

            //updates
            if(!isset($add)){ 
                $update = $this->db->query("UPDATE dictionary_term SET ".$updates." WHERE id = :id");
                foreach($binded as $key => $value){
                    $this->db->bind($update,":$key", $value);
                } 
                $this->db->bind($update,":id", $postData["id_terms"]);  
                if($this->db->execute($update)){
                    if($add_terms){
                        if(MULTI_LANG){   
                            $is_error = "";
                            foreach(LANGUAGES as $l){ 
                                $add_query = $this->db->query("UPDATE dictionary SET
                                text_value = :text WHERE term_id = :termid AND language = :lang");
                                $this->db->bind($add_query, ":text", $postData['text_value_string'][$l]);
                                $this->db->bind($add_query, ":termid", $postData["id_terms"]);
                                $this->db->bind($add_query, ":lang", $l);
                                if($this->db->execute($add_query)){
                                    //works
                                }else{
                                    $is_error .= "true with ".print_r($add_query->errorInfo());
                                }
                                
                            }
                            if($is_error !== ""){
                                
                                $response["message"] = $lang['edited_error'].$is_error;
                                $response["color"] = "danger";
                            }else{
                                $response["message"] = $lang['edited_success'];
                                $response["color"] = "success";
                            }
                        }
                    }else{
                        $response["message"] = $lang['edited_success'];
                        $response["color"] = "success";
                    }
                }else{
                    $response["message"] = $lang['edited_error'];
                    $response["color"] = "danger";
                }
            }else{
                //ADD
                if($can_insert){
                    $adds = $this->db->query("insert ignore into dictionary_term ($insert_sql1) VALUES ($insert_sql2)"); 
                    
                    foreach($binded as $key => $value){
                       $this->db->bind($adds,":$key", $value); 
                    }
                    if($this->db->execute($adds)){
                         
                        $the_term_id = $this->db->query('SELECT LAST_INSERT_ID() as lastid FROM dictionary_term');
                        $the_term_id = $this->db->single($the_term_id)->lastid;
                         if($add_terms){
                            if(MULTI_LANG){   
                                $is_error = "";
                                foreach(LANGUAGES as $l){ 
                                     $add_query = $this->db->query("insert ignore into dictionary
                                     (`text_value`, `language`, `term_id`) VALUES ( :text, :lang, :termid );");                                            
                                    $this->db->bind($add_query, ":text", $postData['text_value_string'][$l]);
                                    $this->db->bind($add_query, ":termid", $the_term_id);
                                    $this->db->bind($add_query, ":lang", $l);
                                    if($this->db->execute($add_query)){
                                        //works
                                    }else{
                                        $is_error .= "true with ".$l;
                                    }
                                    
                                }
                                if($is_error !== ""){
                                    
                                    $response["message"] = $lang['added_error'];
                                    $response["color"] = "danger";
                                }else{
                                    $response["message"] = $lang['added_success'];
                                    $response["color"] = "success";
                                }
                            }
                        }else{
                            $response["message"] = $lang['added_success'];
                            $response["color"] = "success";
                        }  
                         
                    }else{
                        $response["message"] = $lang['added_error'];
                        $response["color"] = "danger";
                    }
                }else{
                    $response["message"] = $lang['added_error'];
                    $response["color"] = "danger";
                }
            }
            return $response;
        }catch(PDOException $e){
            return false;
        }
    }
       
}
?>