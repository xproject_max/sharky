<?php
	namespace Modelss;

	class Newsletter extends \Controller {
	    private $db;
	    public $lang;
	    public $_LANG;
	    public $attempts = 3;
	    public $delay_attempts = 5;

	    public function __construct()
	    {
	        $this->db = new \Database();

	    }

		/* ---------------------------------------------------------- Groups -------------------------------------------------- */
	    public function fetchAllGroups($id=""){
	        try{
	            if($id !== ""){
	               $query_get_all = $this->db->query("
	                SELECT nt.* FROM newsletter_groups nt WHERE nt.id = :id");
	               $this->db->bind($query_get_all, ":id", $id);
	                return $this->db->single($query_get_all);
	            }else{
	                $query_get_all = $this->db->query("
	                SELECT nt.* FROM newsletter_groups nt
	                ORDER BY nt.id ASC");

	                return $this->db->resultSet($query_get_all);
	           }
	        }catch(PDOException $e){
	            return false;
	        }
	    }

		public function deleteNewsletterGroups($id){
		        global $lang;
		        $response = [];
		        $response["text"] = "";
		        $response["color"] = "";
		        try{
		            $query_delete_newsletters = $this->db->query("DELETE FROM newsletter_groups WHERE id = :id");
		            $this->db->bind($query_delete_newsletters,":id", $id);
		            if($this->db->execute($query_delete_newsletters)){
		                    $response["message"] = $lang['deleted_success'];
		                    $response["color"] = "success";


		            }else{

		                $response["message"] = $lang['deleted_error'];
		                $response["color"] = "danger";
		            }
		            return $response;
		        }catch(PDOException $e){
		            return false;
		        }

		}


		public function AddEditNewsletterGroups($postData, $add=false){
			global $lang;
	        if($add == false)
	            unset($add);
	        $response = [];
	        $response["text"] = "";
	        $response["verif"] = "";
	        $response["color"] = "";


	        $updates = "";
	        $updatesd = "";
	        $adds = "";
	        $insert_sql1 = "";
	        $insert_sql2 = "";
	        $permission_string = "";

	        /* custom inputs */
	        $insert_inputs1 = "";
	        $insert_inputs2 = "";
	        $update_inputs = "";
	        $can_insert = true;
	        $binded = [];

	        try{
	            //Build string update
	            foreach($postData as $key => $value){
	                if($key == "inputs"){
	                    foreach($value as $valkey => $val){
	                        if($val !== "")
	                            $permission_string .= "-".$val."-";
	                    }
	                }
	                if ($key != 'B1' and $key != 'query'  and $key != 'supp'
	                    and $key != 'edit_newsletters_groups' and $key != 'add_newsletters_groups'
	                    and $key != 'id_page' and $key != 'inputs'
	                    and $key != 'date_created'
	                    and $key != 'date_edited'
	                    and $key != (isset($add) ? '' : 'id_newsletters_groups')
	                )
	                {


	                    $key = str_replace('*', '', $key);

	                    $updates .= "`newsletter_groups`.`$key`= :$key, ";
	                    // $updatesd .= "`$key_table`.`$key`= '$value', ";

	                    $binded[$key] = ( isset($value) ? $value : 1);
	                }

	            }
	            // var_dump($updates);
	            //Delete last comma
	            $updates = substr($updates, 0, -2);

	            //updates
	            if(!isset($add)){
	                if($response["verif"] !== ""){
	                    $response["message"] =  substr($response["verif"], 0, -5);
	                    $response["color"] = "danger";
	                }else{
	                    $update = $this->db->query("UPDATE newsletter_groups  SET ".$updates." WHERE newsletter_groups.id = :id");
	                    foreach($binded as $key => $value){
	                        $this->db->bind($update,":$key", $value);
	                    }
	                    $this->db->bind($update,":id", $postData["id_newsletters_groups"]);
	                    if($this->db->execute($update)){

	                        $response["message"] = $lang['edited_success'];
	                        $response["color"] = "success";
	                    }else{

	                        $response["message"] = $lang['edited_error'];
	                        $response["color"] = "danger";
	                    }
	                }
	            }else{
	                //ADD
	                if($can_insert){
	                    if($response["verif"] !== ""){
	                        $response["message"] = $response["verif"];
	                        $response["color"] = "danger";
	                    }else{

	                        $adds = $this->db->query("
	                        insert ignore into newsletter_groups (
	                            `name`,
	                            `date_created`
	                        ) VALUES (
	                            :name,
	                            CURRENT_TIMESTAMP()
	                        )");
	                        $this->db->bind($adds,":name", $postData['name']);
	                        if($this->db->execute($adds)){
								$response["message"] = $lang['added_success'];
								$response["color"] = "success";

	                        }else{
	                            // echo  print_r($adds->errorInfo());
	                            $response["message"] = $lang['added_error'];
	                            $response["color"] = "danger";
	                        }
	                    }
	                }else{
	                    $response["message"] = $lang['added_error'];
	                    $response["color"] = "danger";
	                }
	            }
	            return $response;
	        }catch(PDOException $e){
	            return false;
	        }
		}

		/* ---------------------------------------------------------- Members -------------------------------------------------- */
	    public function fetchAllMembers($id=""){
	        try{
	            if($id !== ""){
	               $query_get_all = $this->db->query("
	                SELECT nt.* FROM newsletter_members nt WHERE nt.id = :id");
	               $this->db->bind($query_get_all, ":id", $id);
	                return $this->db->single($query_get_all);
	            }else{
	                $query_get_all = $this->db->query("
	                SELECT nt.* FROM newsletter_members nt
	                ORDER BY nt.id ASC");

	                return $this->db->resultSet($query_get_all);
	           }
	        }catch(PDOException $e){
	            return false;
	        }
	    }

		public function deleteNewsletterMembers($id){
		        global $lang;
		        $response = [];
		        $response["text"] = "";
		        $response["color"] = "";
		        try{
		            $query_delete_newsletters = $this->db->query("DELETE FROM newsletter_members WHERE id = :id");
		            $this->db->bind($query_delete_newsletters,":id", $id);
		            if($this->db->execute($query_delete_newsletters)){
		                    $response["message"] = $lang['deleted_success'];
		                    $response["color"] = "success";


		            }else{

		                $response["message"] = $lang['deleted_error'];
		                $response["color"] = "danger";
		            }
		            return $response;
		        }catch(PDOException $e){
		            return false;
		        }

		}


		public function AddEditNewsletterMembers($postData, $add=false){
			global $lang;
	        if($add == false)
	            unset($add);
	        $response = [];
	        $response["text"] = "";
	        $response["verif"] = "";
	        $response["color"] = "";


	        $updates = "";
	        $updatesd = "";
	        $adds = "";
	        $insert_sql1 = "";
	        $insert_sql2 = "";
	        $groups_joined = "";

	        /* custom inputs */
	        $insert_inputs1 = "";
	        $insert_inputs2 = "";
	        $update_inputs = "";
	        $can_insert = true;
	        $binded = [];

	        try{
	            //Build string update
	            foreach($postData as $key => $value){
	                if($key == "fk_groups"){
	                    foreach($value as $valkey => $val){
	                        if($val !== "")
	                            $groups_joined .= "-".$val."-";
	                    }
	                }
	                if ($key != 'B1' and $key != 'query'  and $key != 'supp'
	                    and $key != 'edit_newsletters_members' and $key != 'add_newsletters_members'
	                    and $key != 'id_page' and $key != 'fk_groups'
	                    and $key != 'date_created'
	                    and $key != 'date_edited'
	                    and $key != (isset($add) ? '' : 'id_newsletters_members')
	                )
	                {


	                    $key = str_replace('*', '', $key);

	                    $updates .= "`newsletter_members`.`$key`= :$key, ";
	                    // $updatesd .= "`$key_table`.`$key`= '$value', ";

	                    $binded[$key] = ( isset($value) ? $value : 1);
	                }

	            }
	            // var_dump($updates);
	            //Delete last comma
	            $updates = substr($updates, 0, -2);

	            //updates
	            if(!isset($add)){
	                if($response["verif"] !== ""){
	                    $response["message"] =  substr($response["verif"], 0, -5);
	                    $response["color"] = "danger";
	                }else{
	                    $update = $this->db->query("UPDATE newsletter_members  SET ".$updates.", `fk_groups`=:fk_groups WHERE newsletter_members.id = :id");
	                    foreach($binded as $key => $value){
	                        $this->db->bind($update,":$key", $value);
	                    }
	                    $this->db->bind($update,":fk_groups", $groups_joined);
	                    $this->db->bind($update,":id", $postData["id_newsletters_members"]);
	                    if($this->db->execute($update)){

	                        $response["message"] = $lang['edited_success'];
	                        $response["color"] = "success";
	                    }else{

	                        $response["message"] = $lang['edited_error'];
	                        $response["color"] = "danger";
	                    }
	                }
	            }else{
	                //ADD
	                if($can_insert){
	                    if($response["verif"] !== ""){
	                        $response["message"] = $response["verif"];
	                        $response["color"] = "danger";
	                    }else{

	                        $adds = $this->db->query("
	                        insert ignore into newsletter_members (
	                            `email`,
	                            `first_name`,
	                            `last_name`,
	                            `fk_groups`,
	                            `date_joined`
	                        ) VALUES (
	                            :email,
	                            :first_name,
	                            :last_name,
	                            :fk_groups,
	                            CURRENT_TIMESTAMP()
	                        )");
	                        $this->db->bind($adds,":email", $postData['email']);
	                        $this->db->bind($adds,":first_name", $postData['first_name']);
	                        $this->db->bind($adds,":last_name", $postData['last_name']);
	                        $this->db->bind($adds,":fk_groups", $groups_joined);
	                        if($this->db->execute($adds)){
								$response["message"] = $lang['added_success'];
								$response["color"] = "success";

	                        }else{
	                            // echo  print_r($adds->errorInfo());
	                            $response["message"] = $lang['added_error'];
	                            $response["color"] = "danger";
	                        }
	                    }
	                }else{
	                    $response["message"] = $lang['added_error'];
	                    $response["color"] = "danger";
	                }
	            }
	            return $response;
	        }catch(PDOException $e){
	            return false;
	        }
		}

		/* ---------------------------------------------------------- Campain -------------------------------------------------- */
	    public function fetchAllCampains($id=""){
	        try{
	            if($id !== ""){
	               $query_get_all = $this->db->query("
	                SELECT nt.* FROM newsletter_campains nt WHERE nt.id = :id");
	               $this->db->bind($query_get_all, ":id", $id);
	                return $this->db->single($query_get_all);
	            }else{
	                $query_get_all = $this->db->query("
	                SELECT nt.* FROM newsletter_campains nt
	                ORDER BY nt.id ASC");

	                return $this->db->resultSet($query_get_all);
	           }
	        }catch(PDOException $e){
	            return false;
	        }
	    }

		public function deleteNewsletterCampains($id){
		        global $lang;
		        $response = [];
		        $response["text"] = "";
		        $response["color"] = "";
		        try{
		            $query_delete_newsletters = $this->db->query("DELETE FROM newsletter_campains WHERE id = :id");
		            $this->db->bind($query_delete_newsletters,":id", $id);
		            if($this->db->execute($query_delete_newsletters)){
		                    $response["message"] = $lang['deleted_success'];
		                    $response["color"] = "success";


		            }else{

		                $response["message"] = $lang['deleted_error'];
		                $response["color"] = "danger";
		            }
		            return $response;
		        }catch(PDOException $e){
		            return false;
		        }

		}


		public function getTemplateView($id=""){
			global $l, $lang;

			$return = "";
			$qgetTemp = $this->db->query("SELECT * FROM newsletter_templates WHERE id = :id");
			$this->db->bind($qgetTemp, ":id", $id);
			$this->db->execute($qgetTemp);
			$page = $this->db->single($qgetTemp);
			$return.= "</br><h2 class='mtop'>Version FR </h2>";
			// --------------- VARIABLES DE CONCEPTION DU TEMPLATE
			$mois = array("", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");
			$datefr = $mois[date("n")]." ".date("Y");

			$return.= '<textarea name="message"   class="ckeditor" data-custom-config="mailerConfig.js">'.$page->message.'</textarea>';

			$return.= "</br><h2 class='mtop'>Version EN </h2>";

			$return.= '<textarea name="message_en"   class="ckeditor" data-custom-config="mailerConfig.js">'.$page->message_en.'</textarea>';
			return $return;
		}

		public function getTemplateCampagnView($id=""){
			global $l, $lang;

			$return = "";
			$qgetTemp = $this->db->query("SELECT * FROM newsletter_campains WHERE id = :id");
			$this->db->bind($qgetTemp, ":id", $id);
			$this->db->execute($qgetTemp);
			$page = $this->db->single($qgetTemp);
			$return.= "</br><h2 class='mtop'>Version FR </h2>";
			// --------------- VARIABLES DE CONCEPTION DU TEMPLATE
			$mois = array("", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");
			$datefr = $mois[date("n")]." ".date("Y");

			$return.= '<textarea name="message"   class="ckeditor" data-custom-config="mailerConfig.js">'.$page->message.'</textarea>';

			$return.= "</br><h2 class='mtop'>Version EN </h2>";

			$return.= '<textarea name="message_en"   class="ckeditor" data-custom-config="mailerConfig.js">'.$page->message_en.'</textarea>';
			return $return;
		}

		public function fetchCommonEmails($id=""){
			global $l, $lang;

			$db = $this->db;

			$query_get_emails = $db->query("
				SELECT nm.email FROM newsletter_members nm
				LEFT JOIN newsletter_groups ng ON
				find_in_set(ng.id,REPLACE(nm.fk_groups, '-', ','))
				INNER JOIN newsletter_campains nc ON
				find_in_set(ng.id,REPLACE(nc.fk_groups, '-', ','))
				WHERE nc.id = :id
				GROUP BY nm.id
			");
			$db->bind($query_get_emails, ":id", $id);
			$emails = $db->resultSet($query_get_emails);
			return $emails;
		}

		public function AddEditNewsletterCampains($postData, $add=false){
			global $lang;
	        if($add == false)
	            unset($add);
	        $response = [];
	        $response["text"] = "";
	        $response["verif"] = "";
	        $response["color"] = "";


	        $updates = "";
	        $updatesd = "";
	        $adds = "";
	        $insert_sql1 = "";
	        $insert_sql2 = "";
	        $groups_joined = "";

	        /* custom inputs */
	        $insert_inputs1 = "";
	        $insert_inputs2 = "";
	        $update_inputs = "";
	        $can_insert = true;
	        $binded = [];

	        try{
	            //Build string update
	            foreach($postData as $key => $value){
	                if($key == "fk_groups"){
	                    foreach($value as $valkey => $val){
	                        if($val !== "")
	                            $groups_joined .= "-".$val."-";
	                    }
	                }
	                if ($key != 'B1' and $key != 'query'  and $key != 'supp'
	                    and $key != 'edit_newsletters_campains' and $key != 'add_newsletters_campains'
	                    and $key != 'id_page' and $key != 'fk_groups'
	                    and $key != 'date_created'
	                    and $key != 'date_edited'
	                    and $key != (isset($add) ? '' : 'id_newsletters_campains')
	                )
	                {


	                    $key = str_replace('*', '', $key);

	                    $updates .= "`newsletter_campains`.`$key`= :$key, ";
	                    // $updatesd .= "`$key_table`.`$key`= '$value', ";

	                    $binded[$key] = ( isset($value) ? $value : 1);
	                }

	            }
	            // var_dump($updates);
	            //Delete last comma
	            $updates = substr($updates, 0, -2);

	            //updates
	            if(!isset($add)){
	                if($response["verif"] !== ""){
	                    $response["message"] =  substr($response["verif"], 0, -5);
	                    $response["color"] = "danger";
	                }else{
	                    $update = $this->db->query("UPDATE newsletter_campains  SET ".$updates.", `fk_groups`=:fk_groups WHERE newsletter_campains.id = :id");
	                    foreach($binded as $key => $value){
	                        $this->db->bind($update,":$key", $value);
	                    }
	                    $this->db->bind($update,":fk_groups", $groups_joined);
	                    $this->db->bind($update,":id", $postData["id_newsletters_campains"]);
	                    if($this->db->execute($update)){

	                        $response["message"] = $lang['edited_success'];
	                        $response["color"] = "success";
	                    }else{

	                        $response["message"] = $lang['edited_error'];
	                        $response["color"] = "danger";
	                    }
	                }
	            }else{
	                //ADD
	                if($can_insert){
	                    if($response["verif"] !== ""){
	                        $response["message"] = $response["verif"];
	                        $response["color"] = "danger";
	                    }else{

	                        $adds = $this->db->query("
	                        insert ignore into newsletter_campains (
	                            `name`,
	                            `subject`,
	                            `from_mail`,
	                            `message`,
	                            `message_en`,
	                            `fk_templates`,
	                            `fk_groups`,
	                            `status`,
	                            `date`,
	                            `date_created`
	                        ) VALUES (
	                            :name,
	                            :subject,
	                            :from_mail,
	                            :message,
	                            :message_en,
	                            :fk_templates,
	                            :fk_groups,
	                            :status,
	                            :date,
	                            CURRENT_TIMESTAMP()
	                        )");
	                        $this->db->bind($adds,":name", $postData['name']);
	                        $this->db->bind($adds,":subject", $postData['subject']);
	                        $this->db->bind($adds,":from_mail", $postData['from_mail']);
	                        $this->db->bind($adds,":message", $postData['message']);
	                        $this->db->bind($adds,":message_en", $postData['message_en']);
	                        $this->db->bind($adds,":fk_templates", $postData['fk_templates']);
	                        $this->db->bind($adds,":fk_groups", $groups_joined);
	                        $this->db->bind($adds,":status", $postData['status']);
	                        $this->db->bind($adds,":date", $postData['date']);
	                        if($this->db->execute($adds)){
								$response["message"] = $lang['added_success'];
								$response["color"] = "success";

	                        }else{
	                            // echo  print_r($adds->errorInfo());
	                            $response["message"] = $lang['added_error'];
	                            $response["color"] = "danger";
	                        }
	                    }
	                }else{
	                    $response["message"] = $lang['added_error'];
	                    $response["color"] = "danger";
	                }
	            }
	            return $response;
	        }catch(PDOException $e){
	            return false;
	        }
		}
		/* ---------------------------------------------------------- TEMPLATES -------------------------------------------------- */
	    public function fetchAllTemplates($id=""){
	        try{
	            if($id !== ""){
	               $query_get_all = $this->db->query("
	                SELECT nt.* FROM newsletter_templates nt WHERE nt.id = :id");
	               $this->db->bind($query_get_all, ":id", $id);
	                return $this->db->single($query_get_all);
	            }else{
	                $query_get_all = $this->db->query("
	                SELECT nt.* FROM newsletter_templates nt
	                ORDER BY nt.id ASC");

	                return $this->db->resultSet($query_get_all);
	           }
	        }catch(PDOException $e){
	            return false;
	        }
	    }

		public function deleteNewsletterTemplates($id){
		        global $lang;
		        $response = [];
		        $response["text"] = "";
		        $response["color"] = "";
		        try{
		            $query_delete_newsletters = $this->db->query("DELETE FROM newsletter_templates WHERE id = :id");
		            $this->db->bind($query_delete_newsletters,":id", $id);
		            if($this->db->execute($query_delete_newsletters)){
		                    $response["message"] = $lang['deleted_success'];
		                    $response["color"] = "success";


		            }else{

		                $response["message"] = $lang['deleted_error'];
		                $response["color"] = "danger";
		            }
		            return $response;
		        }catch(PDOException $e){
		            return false;
		        }

		}

		public function getTemplateName($id=0){
			global $lang, $l;

			try{
				$qgetName = $this->db->query("SELECT name FROM newsletter_templates WHERE id = :id");
				$this->db->bind($qgetName,":id", $id);

				return $this->db->single($qgetName)->name;

			}catch(PDOException $e){
				return false;
			}

		}
		public function AddEditNewsletterTemplates($postData, $add=false){
			global $lang;
	        if($add == false)
	            unset($add);
	        $response = [];
	        $response["text"] = "";
	        $response["verif"] = "";
	        $response["color"] = "";


	        $updates = "";
	        $updatesd = "";
	        $adds = "";
	        $insert_sql1 = "";
	        $insert_sql2 = "";
	        $permission_string = "";

	        /* custom inputs */
	        $insert_inputs1 = "";
	        $insert_inputs2 = "";
	        $update_inputs = "";
	        $can_insert = true;
	        $binded = [];

	        try{
	            //Build string update
	            foreach($postData as $key => $value){
	                if($key == "inputs"){
	                    foreach($value as $valkey => $val){
	                        if($val !== "")
	                            $permission_string .= "-".$val."-";
	                    }
	                }
	                if ($key != 'B1' and $key != 'query'  and $key != 'supp'
	                    and $key != 'edit_newsletters_templates' and $key != 'add_newsletters_templates'
	                    and $key != 'id_page' and $key != 'inputs'
	                    and $key != 'date_created'
	                    and $key != 'date_edited'
	                    and $key != (isset($add) ? '' : 'id_newsletters_templates')
	                )
	                {


	                    $key = str_replace('*', '', $key);

	                    $updates .= "`newsletter_templates`.`$key`= :$key, ";
	                    // $updatesd .= "`$key_table`.`$key`= '$value', ";

	                    $binded[$key] = ( isset($value) ? $value : 1);
	                }

	            }
	            // var_dump($updates);
	            //Delete last comma
	            $updates = substr($updates, 0, -2);

	            //updates
	            if(!isset($add)){
	                if($response["verif"] !== ""){
	                    $response["message"] =  substr($response["verif"], 0, -5);
	                    $response["color"] = "danger";
	                }else{
	                    $update = $this->db->query("UPDATE newsletter_templates  SET ".$updates." WHERE newsletter_templates.id = :id");
	                    foreach($binded as $key => $value){
	                        $this->db->bind($update,":$key", $value);
	                    }
	                    $this->db->bind($update,":id", $postData["id_newsletters_templates"]);
	                    if($this->db->execute($update)){

	                        $response["message"] = $lang['edited_success'];
	                        $response["color"] = "success";
	                    }else{

	                        $response["message"] = $lang['edited_error'];
	                        $response["color"] = "danger";
	                    }
	                }
	            }else{
	                //ADD
	                if($can_insert){
	                    if($response["verif"] !== ""){
	                        $response["message"] = $response["verif"];
	                        $response["color"] = "danger";
	                    }else{

	                        $adds = $this->db->query("
	                        insert ignore into newsletter_templates (
	                            `name`,
	                            `message`,
	                            `message_en`,
	                            `date_created`,
	                            `date_edited`
	                        ) VALUES (
	                            :name,
	                            :message,
	                            :message_en,
	                            CURRENT_TIMESTAMP(),
	                            CURRENT_TIMESTAMP()
	                        )");
	                        $this->db->bind($adds,":name", $postData['name']);
	                        $this->db->bind($adds,":message", $postData['message']);
	                        $this->db->bind($adds,":message_en", $postData['message_en']);
	                        if($this->db->execute($adds)){
								$response["message"] = $lang['added_success'];
								$response["color"] = "success";

	                        }else{
	                            // echo  print_r($adds->errorInfo());
	                            $response["message"] = $lang['added_error'];
	                            $response["color"] = "danger";
	                        }
	                    }
	                }else{
	                    $response["message"] = $lang['added_error'];
	                    $response["color"] = "danger";
	                }
	            }
	            return $response;
	        }catch(PDOException $e){
	            return false;
	        }
		}

	}
?>
