<?php
    class Order {
        private $db;

        public function __construct()
        {
            $this->db = new Database();
        }

        public function getPage($id){
			try{
				$query_page = $this->db->query("SELECT * FROM tbl_pages_admin WHERE id = " .$id);
				return $this->db->resultSet($query_page);
			}catch(PDOException $e){
				return false;
			}
        }
		
		public function TotalPaid(){
			try{
				$total_paid = $this->db->query("SELECT sum(final_price) as total FROM
					tbl_orders WHERE status = 'paid'");
				return $this->db->resultSet($total_paid);
			}catch(PDOException $e){
				return false;
			}
		}
		
		// public function Three_Months_Total_Sold(){
			// try{
				// $query_total_sold = $this->db->query("SELECT
				 // (SELECT sum(final_price) as total_one FROM tbl_orders WHERE date_created <= DATE_ADD(NOW(), INTERVAL -90 DAY) AND date_created > DATE_ADD(NOW(), INTERVAL -60 DAY)) as total_one,
				 // (SELECT sum(final_price) as total_two FROM tbl_orders WHERE date_created <= DATE_ADD(NOW(), INTERVAL -60 DAY) AND date_created >= DATE_ADD(NOW(), INTERVAL -30 DAY)) as total_two,
				 // (SELECT sum(final_price) as total_third FROM tbl_orders WHERE date_created < DATE_ADD(NOW(), INTERVAL -30 DAY)) as total_third");
				// return $this->db->resultSet($query_total_sold);
			// }catch(PDOException $e){
				// return false;
			// }
		// }
		public function Three_Months_Total_Sold(){
			try{
				$query_total_sold = $this->db->query('SELECT
				 (SELECT sum(final_price) as total_one FROM tbl_orders WHERE 
				 	STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(tbl_orders.date_created)), "%d-%m-%Y"), "%d-%m-%Y") >= STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(DATE_ADD(NOW(), INTERVAL -90 DAY))), "%d-%m-%Y"), "%d-%m-%Y") AND
					STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(tbl_orders.date_created)), "%d-%m-%Y"), "%d-%m-%Y") <= STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(DATE_ADD(NOW(), INTERVAL -60 DAY))), "%d-%m-%Y"), "%d-%m-%Y") AND status = "paid")
					as total_one,
				 (SELECT sum(final_price) as total_two FROM tbl_orders WHERE 
				 	STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(tbl_orders.date_created)), "%d-%m-%Y"), "%d-%m-%Y") >= STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(DATE_ADD(NOW(), INTERVAL -60 DAY))), "%d-%m-%Y"), "%d-%m-%Y") AND
					STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(tbl_orders.date_created)), "%d-%m-%Y"), "%d-%m-%Y") <= STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(DATE_ADD(NOW(), INTERVAL -30 DAY))), "%d-%m-%Y"), "%d-%m-%Y")  AND status = "paid")
					as total_two,
				 (SELECT sum(final_price) as total_third FROM tbl_orders WHERE 
				 	STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(tbl_orders.date_created)), "%d-%m-%Y"), "%d-%m-%Y") >= STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(DATE_ADD(NOW(), INTERVAL -30 DAY))), "%d-%m-%Y"), "%d-%m-%Y")  AND status = "paid")
					as total_third ');
				return $this->db->resultSet($query_total_sold);
			}catch(PDOException $e){
				return false;
			}
		}
		
		public function getOrdersFromTimePeriod($start_date_timestamp="", $end_date_timestamp=""){
			try{ 
				$price = 0;
				$item_separator = '----';
				$item_field_separator = '||';
				$item_fields = array('id', 'final_price', 'state', 'fk_categories', 'quantity_left');
				$concat_sql = '';
				foreach($item_fields as $item_field) {
					$concat_sql .= 'pi.'.$item_field.', "'.$item_field_separator.'",';
				}
				$concat_sql = rtrim($concat_sql, ',');
				$sql = '
					SELECT
					tbl_orders.* ,um.first_name as fname, um.last_name as lname, "NA" as na,
					um.address as address_user, um.city as city_user, um.zipcode as zipcode_user,
					d.status as delivery_status, d.address as delivery_address, d.city as delivery_city, d.zipcode as delivery_zipcode
					FROM
					`tbl_orders`
					LEFT JOIN
					tbl_products
					ON
					tbl_orders.fk_products = tbl_products.id
					LEFT JOIN
					tbl_users
					ON
					tbl_orders.fk_users = tbl_users.id
					LEFT JOIN
					tbl_users_meta um
					ON
					um.id = tbl_users.fk_user_meta
					LEFT JOIN
					tbl_products_inputs pi 
					ON
					pi.fk_products = tbl_products.id
					LEFT JOIN
					tbl_delivery d 
					ON
					d.id = tbl_orders.fk_delivery
					LEFT JOIN
					tbl_products_orders po 
					ON
					po.fk_orders = tbl_orders.id
				';
				$sql_payment_type = '';
				$filter_ext = "";
				$bind_filter = array();
				if(isset($_POST['first_name']) && $_POST['first_name'] !== ""){
					$filter_ext .= " AND um.first_name LIKE CONCAT('%',:first_name,'%')";
					$bind_filter["first_name"] = $_POST['first_name'];
				}
				if(isset($_POST['last_name']) && $_POST['last_name'] !== ""){
					$filter_ext .= " AND um.last_name LIKE CONCAT('%',:last_name,'%')";
					$bind_filter["last_name"] = $_POST['last_name'];
				}
				if(isset($_POST['id']) && $_POST['id'] !== "" ){
					$filter_ext .= " AND tbl_orders.id = :id";
					$bind_filter["id"] = $_POST['id'];
				}
				if(isset($_POST['status']) && $_POST['status'] !== "" ){
					$filter_ext .= " AND tbl_orders.status LIKE CONCAT('%',:status,'%')";
					$bind_filter["status"] = $_POST['status'];
				}
				if(isset($_POST['delivery_status']) && $_POST['delivery_status'] !== ""){
					$filter_ext .= " AND d.status LIKE CONCAT('%',:delivery_status,'%')";
					$bind_filter["delivery_status"] = $_POST['delivery_status'];
				}
				if(isset($_POST['email']) && $_POST['email'] !== "" ){
					$filter_ext .= " AND tbl_users.email LIKE CONCAT('%',:email,'%')";
					$bind_filter["email"] = $_POST['email'];
				}
				if(trim($start_date_timestamp) != '' || trim($end_date_timestamp) != ''){
					if($_POST){
						$start_date_timestamp = strtotime($_POST['start_date']);
						$end_date_timestamp = strtotime($_POST['end_date']);
					}
					if($start_date_timestamp != '' && $end_date_timestamp != ''){
						$sql_sales = '
						WHERE
						STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(tbl_orders.date_created)), "%d-%m-%Y"), "%d-%m-%Y") >= STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(:start_date), "%d-%m-%Y"), "%d-%m-%Y") AND
						STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(tbl_orders.date_created)), "%d-%m-%Y"), "%d-%m-%Y") <= STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(:last_date), "%d-%m-%Y"), "%d-%m-%Y")
						'. ($sql_payment_type != ''? 'AND '. $sql_payment_type : '') .'';
					}elseif($start_date_timestamp != ''){
						$sql_sales = '
						WHERE
						STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(tbl_orders.date_created)), "%d-%m-%Y"), "%d-%m-%Y") >= STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(:start_date), "%d-%m-%Y"), "%d-%m-%Y")
						'. ($sql_payment_type != ''? 'AND '. $sql_payment_type : '') .'';
					}elseif($end_date_timestamp != ''){
						$sql_sales = '
						WHERE
						STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(tbl_orders.date_created)), "%d-%m-%Y"), "%d-%m-%Y") <= STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(:last_date), "%d-%m-%Y"), "%d-%m-%Y")
						'. ($sql_payment_type != ''? 'AND '. $sql_payment_type : '') .'';
					}
				}else{
					$sql_sales = ' ';
					// $sql_sales = 'WHERE tbl_orders.archived="0" ';
				}
				// if ($sql_sales == '') {
					// $sql_sales .= 'WHERE tbl_orders.status = "paid"';
				// } else {
					// $sql_sales .= 'AND tbl_orders.status = "paid"';
				// }
				$sql .= $sql_sales;
				$sql .= '
				AND tbl_orders.fk_users is not null
				GROUP BY
				tbl_orders.id
				ORDER BY
				tbl_orders.id ASC'; 
				$request_sales = $this->db->query($sql);
				if($start_date_timestamp != '')
					$this->db->bind($request_sales, ":start_date", $start_date_timestamp);
				if($end_date_timestamp != '')
					$this->db->bind($request_sales, ":last_date", $end_date_timestamp);
				foreach($bind_filter as $filter => $val){ 
					if($val)
						$this->db->bind($request_sales, (":".$filter.""), $val);
				}
				$last_number_order = ''; 
				$orders = array();
				$i = 0;
				$last_number_order = '';
				$panier = 0; 
				// echo $sql;
				// echo '</br>';
				// echo print_r($bind_filter);
				return $request_sales; 
				

			}catch(PDOException $e){
                    // $this->error = $e->getMessage();
                    // echo $this->error;
			}
		}
		
		function generateOrdersXLSReports($orders_array, $xls_name){ 

			require_once('../../public/plugins/PHPExcel/PHPExcel.php'); 
				$concat_sql = '';
				$this->db->execute($orders_array);
				while($sale = $this->db->fetch($orders_array,"array")){   
					$i = $sale['id']; 
					
					$total_item = 0;

 
					$orders[$i]['id']  = $sale['id'];
					$orders[$i]['fname']  = $sale['fname'];
					$orders[$i]['lname']  = $sale['lname'];
					$orders[$i]['address_user']  = $sale['address_user'];
					$orders[$i]['city_user']  = $sale['city_user'];
					$orders[$i]['sub_total']  = $sale['sub_total'];
					$orders[$i]['fk_products']  = $sale['fk_products'];
					$orders[$i]['na']  = $sale['na'];
					$orders[$i]['fk_users']  = $sale['fk_users'];
					$orders[$i]['fk_companies']  = $sale['fk_companies'];
					$orders[$i]['status']  = $sale['status'];
					$orders[$i]['delivery_status']  = $sale['delivery_status'];
					$orders[$i]['fk_delivery']  = $sale['fk_delivery'];
					$orders[$i]['delivery_address']  = $sale['delivery_address'];
					$orders[$i]['delivery_zipcode']  = $sale['delivery_zipcode'];
					$orders[$i]['delivery_city']  = $sale['delivery_city'];
					$orders[$i]['delivery_type']  = $sale['delivery_type'];

					if(!isset($orders[$i]['shipping'])){
						$orders[$i]['shipping'] = number_format($sale['shipping'], 2, '.', '');
						$orders[$i]['tps'] = number_format($sale['tps'], 2, '.', '');
						$orders[$i]['tvq'] = number_format($sale['tvq'], 2, '.', '');
						$orders[$i]['taxes'] = number_format($sale['tps'] + $sale['tvq'], 2, '.', '');
					}else{
						$orders[$i]['shipping'] += number_format($sale['shipping'], 2, '.', '');
						$orders[$i]['tps'] += number_format($sale['tps'], 2, '.', '');
						$orders[$i]['tvq'] += number_format($sale['tvq'], 2, '.', '');
						$orders[$i]['taxes'] += number_format($sale['tps'] + $sale['tvq'], 2, '.', '');						
					}
 

					$orders[$i]['total_price_notaxes'] = (number_format(($orders[$i]['sub_total'] - $sale['rebate']) + $orders[$i]['shipping'], 2, '.', ''));
					if($orders[$i]['total_price_notaxes'] < 0) {
						$orders[$i]['total_price_notaxes'] = 0;
					}
					$orders[$i]['rebate_percent'] = $sale['rebate_percent'];
					$orders[$i]['rebate'] = number_format($sale['rebate'], 2,'.','');
					$orders[$i]['rebate_subtotal'] = number_format($sale['rebate_subtotal'] , 2,'.','');
					$orders[$i]['final_price'] = number_format($sale['final_price'] , 2,'.','');
					$orders[$i]['amount_to_pay'] = number_format($sale['amount_to_pay'] , 2,'.','');
					$orders[$i]['date']  = $sale['date_created'];
					$orders[$i]['date_paid']  = $sale['date_paid'];

					$last_number_order = $sale['id'];
				}

				$total = array();
				foreach ($orders as $order) {
					if(!isset($total['sub_total'])){
						$total['sub_total'] = number_format($order['sub_total'], 2, '.', '');
						$total['shipping'] = $order['shipping'];
						$total['taxes'] = $order['taxes'];

						$orders['total'] = ($order['sub_total'] + $order['shipping'] + $order['taxes']);
					}else{
						$total['sub_total'] += number_format($order['sub_total'], 2, '.', '');
						$total['shipping'] += $order['shipping'];
						$total['taxes'] += $order['taxes'];

						$orders['total'] += ($order['sub_total'] + $order['shipping'] + $order['taxes']);
					}
				} 
				$orders_array = $orders;
			
			$objPHPExcel = new \PHPExcel();
			$objPHPExcel->getProperties()
				->setCreator('Tess')
				->setLastModifiedBy('Tess')
				->setSubject($xls_name)
				->setDescription($xls_name);
			$work_sheet=0;

				$objPHPExcel->createSheet();

				$array_col = array(
					array('A1', 'ID commande'),
					array('B1', 'Prénom'),
					array('C1', 'Nom'),
					array('D1', 'Adresse'),
					array('E1', 'Ville'),
					array('F1', 'ID produit'),
					array('G1', 'Nom produit'),
					array('H1', 'Prix produit'),
					array('I1', 'Quantité'),
					array('J1', 'Sous-total'),
					array('K1', 'Rabais %'),
					array('L1', 'Rabais fixe'),
					array('M1', 'Sous-total après rabais'),
					array('N1', 'Shipping'),
					array('O1', 'Tps'),
					array('P1', 'Tvq'),
					array('Q1', 'Taxes'),
					array('R1', 'Total sans taxes'),
					array('S1', 'Prix final'),
					array('T1', 'Date de la commande'),
					array('U1', 'Date du paiement'),
					array('V1', 'Adresse de livraison'),
					array('W1', 'Code Postal de livraison'),
					array('X1', 'Ville de livraison'),
					array('Y1', 'Type de livraison')
				);
				$order_key = array(
					'A' => 'id',
					'B' => 'fname',
					'C' => 'lname',
					'D' => 'address_user',
					'E' => 'city_user',
					'F' => 'fk_products',
					'G' => 'na',
					'H' => 'na',
					'I' => 'na',
					'J' => 'sub_total',
					'K' => 'rebate_percent',
					'L' => 'rebate',
					'M' => 'rebate_subtotal',
					'N' => 'shipping',
					'O' => 'tps',
					'P' => 'tvq',
					'Q' => 'taxes',
					'R' => 'total_price_notaxes',
					'S' => 'final_price',
					'T' => 'date',
					'U' => 'date_paid',
					'V' => 'delivery_address',
					'W' => 'delivery_zipcode',
					'X' => 'delivery_city',
					'Y' => 'delivery_type'
				);  
				foreach($array_col as $col){
					$objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValue($col[0], $col[1]);
				}

				$total_per_payment = 0;
				$total_per_payment_no_credit = 0;
				$excel_row = 3;
				foreach($orders_array as $key => $order){

					$j = 0;
					foreach($order_key as $letter => $keyv){

						$objPHPExcel->setActiveSheetIndex($work_sheet)
							->setCellValue($letter.$excel_row, $order[$keyv], \PHPExcel_Cell_DataType::TYPE_STRING);
  
						$j++;
					} 
					$array_products = explode(",",$orders[$key]['fk_products']);
					foreach($array_products as $productid){
							error_log($productid,0);
							error_log( $orders[$key]['id'],0);
						$qproduct = $this->db->query("SELECT po.*, p.title FROM tbl_products_orders po
						LEFT JOIN tbl_products p ON p.id = po.fk_products 
						WHERE po.id = :prodid AND po.fk_orders = :orderid");
						$this->db->bind($qproduct, ":prodid", $productid);
						$this->db->bind($qproduct, ":orderid", $orders[$key]['id']);
						$this->db->execute($qproduct);
						while($prods = $this->db->fetch($qproduct,"array")){
							error_log('test',0);
							$excel_row++;   
							$objPHPExcel->setActiveSheetIndex($work_sheet)
								->setCellValue("A".$excel_row, $order['id'], \PHPExcel_Cell_DataType::TYPE_STRING);
							$objPHPExcel->setActiveSheetIndex($work_sheet)
								->setCellValue("F".$excel_row, $prods['fk_products'], \PHPExcel_Cell_DataType::TYPE_STRING);
							$objPHPExcel->setActiveSheetIndex($work_sheet)
								->setCellValue("G".$excel_row, $prods['title'], \PHPExcel_Cell_DataType::TYPE_STRING);
							$objPHPExcel->setActiveSheetIndex($work_sheet)
								->setCellValue("H".$excel_row, $prods['price'], \PHPExcel_Cell_DataType::TYPE_STRING);
							$objPHPExcel->setActiveSheetIndex($work_sheet)
								->setCellValue("I".$excel_row, $prods['quantity'], \PHPExcel_Cell_DataType::TYPE_STRING);
							
						}
					}
					$total_per_payment_no_credit += ($order['sub_total'] + $order['shipping'] + $order['tps'] + $order['tvq']);
					$excel_row++;  


				}

				$objPHPExcel->setActiveSheetIndex($work_sheet)
								->setCellValue('S'.$excel_row, "TOTAL SOLD", \PHPExcel_Cell_DataType::TYPE_STRING);
				$objPHPExcel->setActiveSheetIndex($work_sheet)
								->setCellValue('S'.($excel_row+1), $total_per_payment_no_credit, \PHPExcel_Cell_DataType::TYPE_STRING);
				foreach(range(3, ($excel_row)) as $rowID) {
					$objPHPExcel->getActiveSheet()->getStyle("A". $rowID .":X". $rowID)->applyFromArray(
						array(
							'borders' => array(
								'bottom' => array(
									'style' => \PHPExcel_Style_Border::BORDER_THIN,
									'color' => array('rgb' => '000000')
								)
							)
						)
					);
				}

				foreach(range('A', 'X') as $columnID) {
					$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
						->setAutoSize(true);
				}

				foreach(range(1, $excel_row) as $rowID) {
					$objPHPExcel->getActiveSheet()->getRowDimension($rowID)->setRowHeight(-1);
				}

				// Rename worksheet
				$page_title = "Orders";
				$objPHPExcel->getActiveSheet()->setTitle(substr($page_title, 0, 30));

				foreach(range(0, $excel_row) as $rowID) {
					foreach(range('A', 'X') as $columnID) {
						$objPHPExcel->getActiveSheet()->getStyle("A". $rowID .":X". $rowID)->getFont()->setSize(9);
					}
				}


				for($j = 0; $j <= ($excel_row - 0); $j++){
					foreach(range('A', 'X') as $letter) {
						$objPHPExcel->getActiveSheet()->getStyle($letter.$j)->getAlignment()->applyFromArray(
							array(
									'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
									'vertical'   => \PHPExcel_Style_Alignment::VERTICAL_TOP,
									'rotation'   => 0,
									'wrap'		 => TRUE
							)
						);
					}
				}
				$work_sheet++;

			$objPHPExcel->setActiveSheetIndex($work_sheet);
			$sheetIndex = $objPHPExcel->getActiveSheetIndex();
			$objPHPExcel->removeSheetByIndex($sheetIndex);
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);


			// Redirect output to a client’s web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'. $xls_name .'.xls"');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			ob_end_clean();
			$objWriter->save('php://output');
			exit;
		}
		
		public function fetchOrdersFiltered($data){
			$i = 0;  
			$this->db->execute($data);
			while($value = $this->db->fetch($data, "array")){
				// var_dump($value);
				$query_products = $this->db->query("SELECT * FROM tbl_products WHERE id = :id");
				$this->db->bind($query_products, ":id", $value['fk_products']);
				$this->db->execute($query_products);
				while($products = $this->db->fetch($query_products, "array")){
					$value['title'] = $products['title']; 
				}
				$query_users = $this->db->query("SELECT
				tbl_users.email as email,
				um.first_name as first_name,
				um.last_name as last_name
				FROM tbl_users
				LEFT JOIN tbl_users_meta um
				ON um.id = tbl_users.FK_user_meta
				WHERE tbl_users.id = :id");
				$this->db->bind($query_users, ":id", $value['fk_users']);
				$this->db->execute($query_users);
				while($users = $this->db->fetch($query_users, "array")){ 
					$value['email'] = $users['email'];
					$value['first_name'] = $users['first_name'];
					$value['last_name'] = $users['last_name'];
				} 
				echo '
				<tr>
					<td>
						<input class="text-center" type="hidden" name="id[]"
						style="text-align:center;width:15px !important;"
						value="'.$value['id'].'">
						<p>
							'.$value['id'].'
						</p>
					</td>
					<td>
						<p data-tooltip="'.$value['title'].'">'.$value['title'].'</p>
					</td>
					<td>
						<p data-tooltip="'.$value['quantity'].'">'.$value['quantity'].'</p>
					</td>
					<td>
						<p data-tooltip="'.$value['first_name'].'">'.$value['first_name'].'</p>
					</td>
					<td>
						<p data-tooltip="'.$value['last_name'].'">'.$value['last_name'].'</p>
					</td>
					<td>
						<p data-tooltip="'.$value['email'].'">'.$value['email'].'</p>
					</td>
					<td>
						<p data-tooltip="'.$value['final_price'].'">'.$value['final_price'].'</p>
					</td>
					<td>
						<p data-tooltip="'.$value['status'].'">'.$value['status'].'</p>
					</td>
					<td>
						<p data-tooltip="'.$value['delivery_status'].'">'.$value['delivery_status'].'</p>
					</td>
					<td>
						<p data-tooltip="'.$value['date_created'].'">'.$value['date_created'].'</p>
					</td>
					<td class="td-actions text-right">
						<a title="Export" target="_blank" class="btn btn-info" href="'.URLROOT.'/ajax.php?
							bill='.base64_encode($value['id'].'||'.$value['date_created']).'">
							<i class="fas fa-file-pdf"></i>
						</a> 
					</td>
				</tr>
				';   
			}
		}
		
		public function ManualCreateOrder($postdata, $userid, $product_id){
			$insert_order = $this->db->query("
				INSERT INTO `tbl_orders` ( 
				`fk_products`,
				`fk_users`,
				`fk_companies`,
				`fk_delivery`,
				`quantity`,
				`tps`,
				`tvq`,
				`shipping`,
				`credit_used`,
				`sub_total`,
				`rebate_percent`,
				`rebate`,
				`rebate_subtotal`,
				`final_price`,
				`amount_to_pay`,
				`status`,
				`paid`,
				`date_created`,
				`date_modified`,
				`date_delivery`,
				`delivery_type`,
				`date_paid`,
				`notes`,
				`txn_id`,
				`paypal_id`,
				`payment_method`,
				`refund_id`,
				`archived`)
				VALUES ( 
				'',
				:userid,
				NULL,
				:fkdelivery,
				:quantity,
				:tps,
				:tvq,
				:shipping,
				0,
				:sub_total,
				:rebate_percent,
				:rebate,
				:rebate_subtotal,
				:final_price,
				:amount_to_pay,
				:status,
				'0', 
				CURRENT_TIMESTAMP,
				CURRENT_TIMESTAMP,
				'',
				:delivery_type,
				'',
				'',
				'',
				'',
				:payment_method,
				'0',
				'0')
			");
			
			$subTotal = number_format(($postdata['price'] * $postdata['quantity']), 2, ".", "");
			$taxesTvq = number_format((0.09975 * $subTotal), 2, '.','');
			$taxesTps = number_format((0.05 * $subTotal), 2, '.','');
			$finalPrice = ($subTotal + $taxesTps + $taxesTvq);
			//bind
			$this->db->bind($insert_order, ":userid", ($userid));
			$this->db->bind($insert_order, ":fkdelivery", 0);
			$this->db->bind($insert_order, ":quantity", $postdata['quantity']);
			$this->db->bind($insert_order, ":tps", $taxesTps);
			$this->db->bind($insert_order, ":tvq", $taxesTvq);
			$this->db->bind($insert_order, ":shipping",0);
			$this->db->bind($insert_order, ":sub_total", $subTotal);
			$this->db->bind($insert_order, ":rebate_percent", 0);
			$this->db->bind($insert_order, ":rebate", 0);
			$this->db->bind($insert_order, ":rebate_subtotal", 0);
			$this->db->bind($insert_order, ":final_price", $finalPrice);
			$this->db->bind($insert_order, ":amount_to_pay",  $finalPrice);
			$this->db->bind($insert_order, ":status", "unpaid");
			$this->db->bind($insert_order, ":delivery_type", "Ramassage");
			$this->db->bind($insert_order, ":payment_method", "credit_card");
			
			if($this->db->execute($insert_order)){
				
				$current_order_id = $this->db->query("SELECT LAST_INSERT_ID() as 'lastid' FROM tbl_orders");
				$current_order_id = $this->db->single($current_order_id);
				if($current_order_id->lastid != 0){
					$current_order_id = $current_order_id->lastid;
					$update_solde = $this->db->query("
						UPDATE tbl_users SET solde = (solde - :total) WHERE id = :id
					");
					$this->db->bind($update_solde, ":total", $finalPrice);
					$this->db->bind($update_solde, ":id", $userid);
					$this->db->execute($update_solde);
				
					$insert_product_order = $this->db->query("
						INSERT INTO `tbl_products_orders` (
							`fk_products`,
							`fk_orders`,
							`quantity`,
							`price` 
							)
						VALUES (
							:productid,
							:orderid,
							:quantity,
							:price 
						)
					");
					$this->db->bind($insert_product_order, ":productid", $product_id);
					$this->db->bind($insert_product_order, ":orderid", $current_order_id);
					$this->db->bind($insert_product_order, ":quantity", $postdata['quantity']);
					$this->db->bind($insert_product_order, ":price", $postdata['price']); 
					
					if($this->db->execute($insert_product_order)){
						
					}
					$query_last_porder = $this->db->query("
						SELECT LAST_INSERT_ID() as `lastid` FROM `tbl_products_orders`
					");
					$last_porder = $this->db->single($query_last_porder)->lastid;
					$str_fk_products[] = $last_porder;
					 
								 
					$fk_products = implode(",", $str_fk_products);
					
					$query_update_order = $this->db->query("
						UPDATE tbl_orders SET fk_products = :fk_products WHERE id = :id
					");
					$this->db->bind($query_update_order, ":fk_products", $fk_products);
					$this->db->bind($query_update_order, ":id", $current_order_id);
					
					$this->db->execute($query_update_order);
					return $current_order_id;
				}
			}else{
				 echo print_r($insert_order->errorInfo());
				 die();
			}
		}
		public function getOrder($id){
			$query_update_order = $this->db->query("
				SELECT * FROM tbl_orders WHERE id = :id
			"); 
			$this->db->bind($query_update_order, ":id", $id);
			
			return $this->db->single($query_update_order)->final_price;
		}
    }