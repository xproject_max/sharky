<?php
class Company extends Controller {
    private $db;
    public $lang;
    public $_LANG;
    public $attempts = 3;
    public $delay_attempts = 5;

    public function __construct()
    {
        $this->db = new Database();
        $this->lang = $this->getLang();

    }
    public function getCompaniesFromTimePeriod($start_date_timestamp, $end_date_timestamp){
        try{   
            $sql = '
                SELECT
                tbl_companies.*, um.*, r.*, tbl_companies.id as id, tbl_companies.date_company_created as date_company_created
                FROM
                `tbl_companies` 
                LEFT JOIN
                tbl_companies_meta um
                ON
                um.id = tbl_companies.fk_companies_meta 
                LEFT JOIN
                tbl_roles r
                ON
                r.id = tbl_companies.fk_role
            ';
            $sql_payment_type = '';
            $sql_sales = "";
            $filter_ext = "";
            $order_solde = "";
            $bind_filter = array();
            if($_POST['company_name']){
                $filter_ext .= " AND um.company_name LIKE CONCAT('%',:company_name,'%')";
                $bind_filter["company_name"] = $_POST['company_name'];
            }
            if($_POST['contact_name']){
                $filter_ext .= " AND um.contact_name LIKE CONCAT('%',:contact_name,'%')";
                $bind_filter["contact_name"] = $_POST['contact_name'];
            }
            if($_POST['username']){
                $filter_ext .= " AND tbl_companies.username LIKE CONCAT('%',:username,'%')";
                $bind_filter["username"] = $_POST['username'];
            }
            if($_POST['id']){
                $filter_ext .= " AND tbl_companies.id = :id";
                $bind_filter["id"] = $_POST['id'];
            }
            if(isset($_POST['role'])){
                $filter_ext .= " AND tbl_companies.fk_role = :role";
                $bind_filter["role"] = $_POST['role'];
            }
            if(isset($_POST['solde'])){
                $order_solde .= " ORDER BY tbl_companies.solde ASC"; 
            }
            if($_POST['email']){
                $filter_ext .= " AND tbl_companies.email LIKE CONCAT('%',:email,'%')";
                $bind_filter["email"] = $_POST['email'];
            }
            if($start_date_timestamp != '' || $end_date_timestamp != ''){
                if($_POST){
                    $start_date_timestamp = strtotime($_POST['start_date']);
                    $end_date_timestamp = strtotime($_POST['end_date']);
                }
                if($start_date_timestamp != '' && $end_date_timestamp != ''){
                    $sql_sales = '
                    WHERE
                    STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(tbl_companies.date_company_created)), "%d-%m-%Y %H:%i:%s"), "%d-%m-%Y %H:%i:%s") >= STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(:start_date), "%d-%m-%Y %H:%i:%s"), "%d-%m-%Y %H:%i:%s") AND
                    STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(tbl_companies.date_company_created)), "%d-%m-%Y %H:%i:%s"), "%d-%m-%Y %H:%i:%s") <= STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(:last_date), "%d-%m-%Y %H:%i:%s"), "%d-%m-%Y %H:%i:%s")
                    '. ($sql_payment_type != ''? 'AND '. $sql_payment_type : '') .'';
                }elseif($start_date_timestamp != ''){
                    $sql_sales = '
                    WHERE
                    STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(tbl_companies.date_company_created)), "%d-%m-%Y %H:%i:%s"), "%d-%m-%Y %H:%i:%s") >= STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(:start_date), "%d-%m-%Y %H:%i:%s"), "%d-%m-%Y %H:%i:%s")
                    '. ($sql_payment_type != ''? 'AND '. $sql_payment_type : '') .'';
                }elseif($end_date_timestamp != ''){
                    $sql_sales = '
                    WHERE
                    STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(tbl_companies.date_company_created)), "%d-%m-%Y %H:%i:%s"), "%d-%m-%Y %H:%i:%s") <= STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(:last_date), "%d-%m-%Y %H:%i:%s"), "%d-%m-%Y %H:%i:%s")
                    '. ($sql_payment_type != ''? 'AND '. $sql_payment_type : '') .'';
                }
            }else{
                $sql_sales = 'WHERE tbl_companies.online="1" ';
            }
            // if ($sql_sales == '') {
                // $sql_sales .= 'WHERE tbl_companies.status = "paid"';
            // } else {
                // $sql_sales .= 'AND tbl_companies.status = "paid"';
            // }
            $sql .= $sql_sales;
            $sql .= $filter_ext .'
            GROUP BY
            tbl_companies.id
            '.($order_solde !== "" ? $order_solde : "
            ORDER BY
            tbl_companies.id ASC").''; 
            // echo print_r($sql);
            $request_sales = $this->db->query($sql);
            if($start_date_timestamp != '')
                $this->db->bind($request_sales, ":start_date", $start_date_timestamp);
            if($end_date_timestamp != '')
                $this->db->bind($request_sales, ":last_date", $end_date_timestamp);
            foreach($bind_filter as $filter => $val){ 
                if($val)
                    $this->db->bind($request_sales, (":".$filter.""), $val);
            }
            $last_number_order = ''; 
            $companies = array();
            $i = 0;
            $last_number_order = '';
            $panier = 0;  
            // var_dump($this->db->resultSet($request_sales));
            // print($sql);
            // die();
            return $request_sales; 
            

        }catch(PDOException $e){
                // $this->error = $e->getMessage();
                // echo $this->error;
        }
    }
 
    function generateCompaniesXLSReports($companies_array, $xls_name){ 

        require_once('../../public/plugins/PHPExcel/PHPExcel.php'); 
        $concat_sql = '';
          
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()
            ->setCreator('Tess')
            ->setLastModifiedBy('Tess')
            ->setSubject($xls_name)
            ->setDescription($xls_name);
        $work_sheet=0;

        $objPHPExcel->createSheet();

        $array_col = array(
            array('A1', 'ID company'),
            array('B1', 'username'),
            array('C1', 'Nom companie'),
            array('D1', 'Nom de contact'),
            array('E1', 'Courriel'),
            array('F1', 'Téléphone'),
            array('G1', 'Pays'),
            array('H1', 'Région'),
            array('I1', 'Ville'),
            array('J1', 'Adresse'),
            array('K1', 'Code Postal'),
            array('L1', 'Date'),
            array('M1', 'Solde')
        );
        $order_key = array(
            'A' => 'id',
            'B' => 'username',
            'C' => 'company_name',
            'D' => 'contact_name',
            'E' => 'email',
            'F' => 'phone',
            'G' => 'country',
            'H' => 'region',
            'I' => 'city',
            'J' => 'address',
            'K' => 'zipcode',
            'L' => 'date_company_created',
            'M' => 'solde'
        );
        $i = 0;
        foreach($array_col as $col){
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValue($col[0], $col[1]);
        }

        $total_per_payment = 0;
        $total_per_payment_no_credit = 0;
        $excel_row = 3;
        
        $this->db->execute($companies_array);
        while($company = $this->db->fetch($companies_array,"array")){  

            $j = 0;
            foreach($order_key as $letter => $keyv){

                $objPHPExcel->setActiveSheetIndex($work_sheet)
                    ->setCellValue($letter.$excel_row, $company[$keyv], \PHPExcel_Cell_DataType::TYPE_STRING);

                $j++;
            } 
            $excel_row++;
            $i++;


        }

         
        foreach(range(3, ($excel_row)) as $rowID) {
            $objPHPExcel->getActiveSheet()->getStyle("A". $rowID .":M". $rowID)->applyFromArray(
                array(
                    'borders' => array(
                        'bottom' => array(
                            'style' => \PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                )
            );
        }

        foreach(range('A', 'M') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        foreach(range(1, $excel_row) as $rowID) {
            $objPHPExcel->getActiveSheet()->getRowDimension($rowID)->setRowHeight(-1);
        }

        // Rename worksheet
        $page_title = "companies";
        $objPHPExcel->getActiveSheet()->setTitle(substr($page_title, 0, 30));

        foreach(range(0, $excel_row) as $rowID) {
            // foreach(range('A', 'K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getStyle("A". $rowID .":M". $rowID)->getFont()->setSize(9);
            // }
        }


        for($j = 0; $j <= ($excel_row - 0); $j++){
            foreach(range('A', 'M') as $letter) {
                $objPHPExcel->getActiveSheet()->getStyle($letter.$j)->getAlignment()->applyFromArray(
                    array(
                            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                            'vertical'   => \PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'rotation'   => 0,
                            'wrap'		 => TRUE
                    )
                );
            }
        }
        $work_sheet++;

        $objPHPExcel->setActiveSheetIndex($work_sheet);
        $sheetIndex = $objPHPExcel->getActiveSheetIndex();
        $objPHPExcel->removeSheetByIndex($sheetIndex);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $xls_name .'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }
    
    public function fetchCompaniesFiltered($data){
        $i = 0;  
        $this->db->execute($data);
        while($company = $this->db->fetch($data)){
            
            echo ' 
            <tr mid="'.$company->id.'" url="'.URLROOT."".getUrlLang(27).'?companyid='.$company->id.'" urlinv="'.URLROOT."".getUrlLang(27, "en").'?companyid='.$company->id.'">
                <td>
                    <input class="text-center" type="hidden" name="id[]"
                    style="text-align:center;width:15px !important;"
                    value="'.$company->id.'">
                    <p>
                        '.$company->id.'
                    </p>
                </td>
                <td>
                    <div style="    height: 40px;
                    width: 40px;
                    display: table;
                    background: rgb(0, 0, 0);
                    overflow: hidden;
                    border-radius: 50%;
                    /* border: 1px solid rgba(0,0,0,0.2); */
                    margin: 0 auto;">
                        <div style="display:table-cell;vertical-align:middle">
                            <img src="'.URLIMG.$company->profile_image.'" width="40px"  />
                        </div>
                    </div>
                </td> 
                <td>
                    <p data-tooltip="'.$company->company_name.'">'.$company->company_name.'</p>
                </td> 
                <td>
                    <p data-tooltip="'.$company->username.'">'.$company->username.'</p>
                </td>
                <td>
                    <p data-tooltip="'.$company->role.'">'.$company->role.'</p>
                </td>
                <td>
                    <p data-tooltip="'.$company->email.'">'.$company->email.'</p>
                </td>
                <td>
                    <p data-tooltip="'.$company->date_company_created.'">'.$company->date_company_created.'</p>
                </td>
                <td>
                    <p data-tooltip="'.$company->solde.'">'.$company->solde.'</p>
                </td>
                <td>												
                    <div class="togglebutton">
                        <label>
                            <input name="online[]['.$company->id.']" value="0" type="hidden" />
                            <input name="online[]['.$company->id.']" value="1" type="checkbox" '.($company->online == 1 ? 'checked=""' : '').' />
                            <span class="toggle"></span>
                        </label>
                    </div>
                </td>
                <td class="td-actions ">
            ';

            echo '<a class="btn btn-warning" data-toggle="tooltip" data-placement="top" 
                    href="index.php?cp=page&mod=addmod
                    &id=' . $company->id . '"><i class="fas fa-pen-square"></i></a> ';
            echo '<a id="'.$company->id.'"  class="btn btn-danger delete-website-companies" data-tooltip="Supprimer" ><i class="fas fa-times-square"></i></a> ';
            
            echo'
                </td>
            </tr>
            ';
        }
    }
 /* companies WEBSITE */ 
    
    
    /* ---------------------------------------- Login part -------------------------------------- */

    public function getcompany($id){
        try{
            $query_get_company = $this->db->query("SELECT * FROM tbl_companies WHERE id = " .$id);
            return $this->db->resultSet($query_get_company);
        }catch (PDOException $e){
            return false;
        }
    }

    public function encryptedPassword($pass){
        $static_salt = SALT_PASS;
        return hash('sha512', $pass . $static_salt);
    }

    public function usernameExist($company){
        try{
            $query_username_exist = $this->db->query("
            SELECT * FROM tbl_companies
            WHERE (username = :username OR email = :username)");
            $this->db->bind($query_username_exist,":username", $company);
            if($this->db->single($query_username_exist)){
                return true;
            }
            return false;
        }catch (PDOException $e){
            return false;
        }
    }

    public function getcompanyInfo($company, $pass){
        $query_get_company_info = $this->db->query("
        SELECT * FROM tbl_companies WHERE (username = :username OR email = :username)
        AND password = :password");
        $this->db->bind($query_get_company_info,":username", $company);
        $this->db->bind($query_get_company_info,":password", $pass);
        if($this->db->single($query_get_company_info)){
            return $this->db->single($query_get_company_info);
        }
        return false;
    }

    public function companyExist($company, $pass){
        $query_exist = $this->db->query("
        SELECT * FROM tbl_companies
        WHERE (username = :username OR email = :username)
        AND password = :password");
        $this->db->bind($query_exist,":username", $company);
        $this->db->bind($query_exist,":password", $pass);
        if($this->db->single($query_exist)){
            return true;
        }
        return false;
    }

    public function checkIfSamePassword($pass,$id){
        $query_exist = $this->db->query("
        SELECT * FROM tbl_companies
        WHERE (password = :password OR password = :passwordhashed)
        AND id = :id");
        $this->db->bind($query_exist,":password", $pass);
        $pass2 = $this->encryptedPassword($pass);
        $this->db->bind($query_exist,":passwordhashed", $pass2);
        $this->db->bind($query_exist,":id", $id);
        if($this->db->single($query_exist)){
            return true;
        }
        return false;
    }

    public function companyAttempts($company)
    {
        try{
            $query_attemps = $this->db->query("
            SELECT um.attempts FROM tbl_companies u
            LEFT JOIN tbl_companies_meta um ON u.fk_companies_meta = um.id 
            WHERE u.username = :username");
            $this->db->bind($query_attemps,":username", $company);
            if($this->db->single($query_attemps)){
                return $this->db->single($query_attemps)->attempts;
            }
            return false;
        }
        catch(PDOException $e){
            return false;
        }
    }
    public function companyDelayAttempts($company)
    {
        try{
            $query_attemps_delay = $this->db->query("
            SELECT um.attempts_delay
            FROM tbl_companies u
            LEFT JOIN tbl_companies_meta um ON u.fk_companies_meta = um.id 
            WHERE u.username = :username");
            $this->db->bind($query_attemps_delay,":username", $company);
            if($this->db->single($query_attemps_delay)){
                return $this->db->single($query_attemps_delay)->attempts_delay;
            }
            return false;
        }
        catch(PDOException $e){
            return false;
        }
    }

    public function addDelay($company){
        try {
            $delay_query = $this->db->query("
            UPDATE tbl_companies_meta
            LEFT JOIN tbl_companies 
            ON tbl_companies.fk_companies_meta = tbl_companies_meta.id
            SET
            tbl_companies_meta.attempts_delay = DATE_ADD(NOW(),
            INTERVAL :minutes MINUTE)
            WHERE tbl_companies.username = :username");
            $this->db->bind($delay_query,":minutes", $this->delay_attempts);
            $this->db->bind($delay_query,":username", $company);
            if ($this->db->execute($delay_query)) {
                //Added time;

            }
        }catch (PDOException $e){
            return false;
        }
    }

    public function addAttempts($company, $attempted){
        /*
         * This function will add an attempt to the company;
         */
        try{
            $attempts_query = $this->db->query("
            UPDATE tbl_companies_meta
            LEFT JOIN tbl_companies 
            ON tbl_companies.fk_companies_meta = tbl_companies_meta.id
            SET tbl_companies_meta.attempts = tbl_companies_meta.attempts + 1 
            WHERE tbl_companies.username = :username");
            $this->db->bind($attempts_query,":username", $company);
            if($this->db->execute($attempts_query)){
                /*
                 * If current attempt equals max attempts,
                 * Start delay before next tries;
                 */
                if(($attempted + 1) == $this->attempts){
                    //Add delay;
                    $this->addDelay($company);
                }
                return true;
            }
        }catch (PDOException $e){
            return false;
        }
    }

    public function resetAttempts($company){
        /*
         * This function will reset attempts to the company;
         */
        try{
            $reset_attempts = $this->db->query("
            UPDATE tbl_companies_meta
            LEFT JOIN tbl_companies 
            ON tbl_companies.fk_companies_meta = tbl_companies_meta.id
            SET tbl_companies_meta.attempts = 0,
            tbl_companies_meta.attempts_delay = null
            WHERE tbl_companies.username = :username");
            $this->db->bind($reset_attempts,":username", $company);
            if ($this->db->execute($reset_attempts)) {
                //remove time;
                return true;
            }
        }catch (PDOException $e){
            return false;
        }
    }

    public function createcompaniesession($data){
        if(!empty($data)){
            $_SESSION['company_id'] = $data->id;
            $_SESSION['company_email'] = $data->email;
            $_SESSION['company_name'] = $data->username;
            $_SESSION['status'] = $data->status;
            $_SESSION['profile_image'] = $data->profile_image;
        }
    }

    public function validate_companies($data){
        global $lang;
        //Declare variables
        $_LANG = [];
        $Message_error = [];
        $Message_error['username'] = "";
        $Message_error['password'] = "";
        $Message_error['login'] = "";
        $Message_error['login_max'] = "";
        $Message_error['login_form'] = "";
        $Message_error['global'] = "";
        //Require dictionary


        //Constraints and verifications

        if(!$this->usernameExist($data['username']) && $data['username'] !== ""){
            $Message_error['username'] .= $lang['error_username_not_exist'];
        }
        if($this->companyAttempts($data['username']) >= $this->attempts){
            $Message_error['login_max'] .= $lang['error_attempts_max'];
        }

        if(strlen($data['username']) == 0){
            $Message_error['username'] .= $lang['error_username_empty'];
        }

        if(strlen($data['password']) == 0){
            $Message_error['password'] .= $lang['error_password_empty'];
        }

        if(!$this->companyExist($data['username'], $this->encryptedPassword($data['password']))
            && $this->usernameExist($data['username']) && $data['username'] !== ""){
            $attempts_left = "";
            if($this->companyAttempts($data['username']) < $this->attempts) {
                $this->addAttempts($data['username'], $this->companyAttempts($data['username']));
            }
            $attempts = $this->companyAttempts($data['username']);
            $attempts_left = $this->attempts - $attempts;

            if($attempts_left < 0)
                $attempts_left = 0;

            $Message_error['login'] .= $lang['error_attempts']. $attempts_left."</br>";
            $Message_error['password'] .= $lang['error_company_not_exist'];
        }else{
            if($this->companyAttempts($data['username']) >= $this->attempts) {
                // Do nothing
                $first_date = strtotime($this->companyDelayAttempts($data['username']));
                $second_date = strtotime(date('Y-m-d H:i:s'));

                if($this->companyDelayAttempts($data['username']) !== "" && $first_date <= $second_date){
                    unset($Message_error["login_max"]);
                    $this->resetAttempts($data['username']);
                    $this->createcompaniesession($this->getcompanyInfo($data['username'], $this->encryptedPassword($data['password'])));
                }
            }else{
                $this->resetAttempts($data['username']);
                $this->createcompaniesession($this->getcompanyInfo($data['username'], $this->encryptedPassword($data['password'])));
            }
        }

        //Prepare new data array to return

        $data_return = [];
        foreach($data as $key => $value){
            if($Message_error[$key] != "")
                $data_return[$key] = ["name"=>$key, "message_error" => $Message_error[$key], "value"=>$value];
            $Message_error['global'] .= $Message_error[$key];
        }

        //Global data login messages
        if($Message_error["login"] != "")
            $data_return['login'] = ["name"=>"login", "message_error" => $Message_error["login"], "value"=>""];
        if(isset($Message_error["login_max"]) && $Message_error["login_max"] != "")
            $data_return['login_max'] = ["name"=>"login_max", "message_error" => $Message_error["login_max"], "value"=>""];

        if(empty($Message_error['global'])){
            $data_return['success'] = $this->getcompanyInfo($data['username'], $this->encryptedPassword($data['password']));
            $data_return['login']['message_error'] = $lang['message_success']." ".$data_return['success']->username." </br>";
        }
        if(!empty($data_return['login_max'])){
            unset($data_return);
            $data_return['login_max'] = ["name"=>"login_max", "message_error" => "veuillez communiquer avec un de nos agents, vous n'avez plus de tentatives", "value"=>""];
        }
        //return data
        return $data_return;
    }


    /* ---------------------------- company WEBSITE --------------------------------- */


    public function getAll($id=""){
        try{
            if($id !== ""){
               $query_get_all = $this->db->query("
                SELECT tbl_companies.*, um.*, r.name as status
                FROM tbl_companies
                LEFT JOIN tbl_companies_roles r
                    ON r.id = tbl_companies.status
                LEFT JOIN tbl_companies_meta um
                    ON tbl_companies.fk_companies_meta = um.id 
                WHERE tbl_companies.id = :id");
               $this->db->bind($query_get_all, ":id", $id);
                return $this->db->single($query_get_all);
            }else{
                $query_get_all = $this->db->query("                
                SELECT tbl_companies.*, um.*, r.name as status
                FROM tbl_companies
                LEFT JOIN tbl_companies_roles r
                    ON r.id = tbl_companies.status
                LEFT JOIN tbl_companies_meta um
                    ON tbl_companies.fk_companies_meta = um.id 
                ORDER BY id ASC");
           
                return $this->db->resultSet($query_get_all);
           }               
        }catch(PDOException $e){
            return false;
        }
    }

    public function getcompanyWebsite($id){
        try{
            $query = $this->db->query("
            SELECT *, um.*, tbl_companies.id as id
            FROM tbl_companies
            LEFT JOIN tbl_companies_meta um
                ON um.id = tbl_companies.fk_companies_meta
            WHERE tbl_companies.id = " .$id);
            return $this->db->resultSet($query);
        }catch(PDOException $e){
            return false;
        }
    }

    public function getMenu($id=""){
        global $lang, $l;
        $lang = new Lang();
        $l = "";
        if($lang->lang == "en")
            $l="_en";
        $menu_array = [];
        $custom = [];
        $result = $this->getAll($id);
        $i=0;
        if($id){
            $menu_array[$i] = array(
                'id' => $result->id,
                'profile_image' => $result->profile_image,
                'username' => $result->username,
                'password' => $result->password,
                'email' => $result->email,
                'status' => $result->status,
                'date_created' => $result->date_created,
                'permissions' => $result->status,
                'online' => $result->online,
            ); 
        }else{
            foreach($result as $row){

                $menu_array[$i] = array(
                    'id' => $row->id,
                    'profile_image' => $row->profile_image,
                    'username' => $row->username,
                    'password' => $row->password,
                    'email' => $row->email,
                    'status' => $row->status,
                    'date_created' => $row->date_created,
                    'permissions' => $row->status,
                    'online' => $row->online,
                ); 
                $i++;
            }
        }
        return $menu_array; 
    }


    public function update_onlines($postData){
        global $lang;
        foreach ($postData['online'] as $key => $value) {
            foreach($value as $key => $val){
                try{
                    $query_update_online = $this->db->query("UPDATE tbl_companies SET online= :onlines WHERE id = :id");
                    $this->db->bind($query_update_online,":onlines", $val);
                    $this->db->bind($query_update_online,":id", $key);
                    if($this->db->execute($query_update_online)){
                        $msg = $lang['edited_success'];
                    }else{
                        $msg =  $lang['edited_error'];
                    }
                }catch(PDOException $e){
                    return false;
                }
            }
        }
        return $msg;
    }
    
    public function CheckIfEmailExist($email, $companyid=""){
        try{
            if($companyid !== ""){ 
                $query = $this->db->query("SELECT * FROM tbl_companies WHERE email = :email AND id != :id");
                $this->db->bind($query, ":email", $email);
                $this->db->bind($query, ":id", $companyid);
                $this->db->execute($query);
                if($this->db->rowCount($query) > 0){
                    return true; 
                }else{
                    return false;
                }
                
            }else{
                $query = $this->db->query("SELECT * FROM tbl_companies WHERE email = :email");
                $this->db->bind($query, ":email", $email);
                $this->db->execute($query);
                if($this->db->rowCount($query) > 0){
                    return true;
                }else{
                    return false;
                }
            }
        }catch(PDOException $e){
            return false;
        }
    }
    public function CheckIfusernameExist($username, $companyid=""){
        try{
            if($companyid !== ""){ 
                $query = $this->db->query("SELECT * FROM tbl_companies WHERE username = :username AND id != :id");
                $this->db->bind($query, ":username", $username);
                $this->db->bind($query, ":id", $companyid);
                $this->db->execute($query);
                if($this->db->rowCount($query) > 0){
                    return true; 
                }else{
                    return false;
                }
                
            }else{
                $query = $this->db->query("SELECT * FROM tbl_companies WHERE username = :username");
                $this->db->bind($query, ":username", $username);
                $this->db->execute($query);
                if($this->db->rowCount($query) > 0){
                    return true;
                }else{
                    return false;
                }
            }
        }catch(PDOException $e){
            return false;
        }
    }

    public function DeletecompaniesWebsite($id){
        global $lang;
        $response = [];
        $response["text"] = "";
        $response["color"] = "";
        try{
            $query_get_meta = $this->db->query("SELECT fk_companies_meta FROM tbl_companies WHERE id = :id");
            $this->db->bind($query_get_meta, ":id", $id);
            $id_meta = $this->db->single($query_get_meta);
            $query_delete_companies = $this->db->query("DELETE FROM tbl_companies WHERE id = :id");
            $this->db->bind($query_delete_companies,":id", $id);
            if($this->db->execute($query_delete_companies)){
                $query_delete_meta = $this->db->query("DELETE FROM tbl_companies_meta WHERE id = :id");
                $this->db->bind($query_delete_meta,":id", $id_meta->fk_companies_meta);
                if($this->db->execute($query_delete_meta)){

                    $response["message"] = $lang['deleted_success'];
                    $response["color"] = "success";
                }else{
                    
                    $response["message"] = $lang['deleted_error'];
                    $response["color"] = "danger";
                }

            }else{

                $response["message"] = $lang['deleted_error'];
                $response["color"] = "danger";
            }
            return $response;
        }catch(PDOException $e){
            return false;
        }
    }
    
    public function AddEditcompanies($postData,$add=false){
        global $lang;
        if($add == false)
            unset($add);
        $response = [];
        $response["text"] = "";
        $response["verif"] = "";
        $response["color"] = "";


        $updates = "";
        $updatesd = "";
        $adds = "";
        $insert_sql1 = "";
        $insert_sql2 = "";
        $permission_string = "";

        /* custom inputs */
        $insert_inputs1 = "";
        $insert_inputs2 = "";
        $update_inputs = "";
        $can_insert = true;
        $binded = [];
        if($postData['username'] == "")
            $can_insert = false;
        try{
            //Build string update
            foreach($postData as $key => $value){
                if($key == "inputs"){ 
                    foreach($value as $valkey => $val){
                        if($val !== "")
                            $permission_string .= "-".$val."-";
                    }
                }
                if ($key != 'B1' and $key != 'query'  and $key != 'supp' 
                    and $key != 'edit_company' and $key != 'add_company'
                    and $key != 'id_page' and $key != 'inputs'
                    and $key != 'date_created'
                    and $key != 'passe2' 
                    and $key != (isset($add) ? '' : 'id_company')
                ) 
                {
                    
                    if($key == "password"){
                        if($postData['passe2'] !== ""){
                            if($value == $postData['passe2']){
                                if(!isset($add) && $value !== ""){
                                    if(!$this->checkIfSamePassword($value,$postData["id_company"])){
                                        $value = $this->encryptedPassword($value);
                                        $postData['password'] = $this->encryptedPassword($value);
                                    }else{

                                        $response["verif"] .= $lang['password_is_same']."</br>";
                                    }
                                }else{
                                    $value = $this->encryptedPassword($value);
                                    $postData['password'] = $this->encryptedPassword($value);
                                }
                            }else{
                                $response["verif"] .= $lang['passwords_not_match']."</br>";
                            }
                        }
                    }
                    
                    if($key == "email"){
                        if(isset($add)){
                            if($this->CheckIfEmailExist($value)){
                                $response["verif"] .= $lang['email_exists']."</br>";
                            }
                        }else{
                            if($this->CheckIfEmailExist($value,$postData["id_company"])){
                                $response["verif"] .= $lang['email_exists']."</br>";
                            }                                
                        }
                    }
                    $key_table = "tbl_companies";
                    if(
                        $key == "company_name" ||
                        $key == "contact_name" ||
                        $key == "region" ||
                        $key == "city" ||
                        $key == "address" ||
                        $key == "zipcode" || 
                        $key == "phone" ||
                        $key == "profile_image"
                    ){
                        $key_table = "tbl_companies_meta";
                        if($value == "" || ($key == "profile_image" and !is_string($value))){
                            $response["verif"] .= $key." null </br>";
                        }
                    }
                    
                    if($key == "username"){
                        if(isset($add)){
                            if($this->CheckIfusernameExist($value)){
                                $response["verif"] .= $lang['username_exists']."</br>";
                            }
                        }else{
                            if($this->CheckIfusernameExist($value,$postData["id_company"])){
                                $response["verif"] .= $lang['username_exists']."</br>";
                            }                                
                        }
                    }

                    $key = str_replace('*', '', $key); 
                 
                    $updates .= "`$key_table`.`$key`= :$key, ";
                    // $updatesd .= "`$key_table`.`$key`= '$value', "; 

                    $binded[$key] = ( isset($value) ? $value : 1); 
                }

            }
            // var_dump($updates);
            //Delete last comma
            $updates = substr($updates, 0, -2);

            //updates
            if(!isset($add)){
                if($response["verif"] !== ""){ 
                    $response["message"] =  substr($response["verif"], 0, -5);
                    $response["color"] = "danger";
                }else{
                    $update = $this->db->query("UPDATE tbl_companies LEFT JOIN tbl_companies_meta ON tbl_companies_meta.id = tbl_companies.fk_companies_meta  SET ".$updates.", `tbl_companies`.`permissions`=:permissions  WHERE tbl_companies.id = :id");
                    foreach($binded as $key => $value){
                        $this->db->bind($update,":$key", $value);
                    }  
                    // echo "UPDATE tbl_companies LEFT JOIN tbl_companies_meta ON tbl_companies_meta.id = tbl_companies.fk_companies_meta SET ".$updates.", `permissions`=:permissions  WHERE id = :id </br>";
                    // echo "UPDATE tbl_companies LEFT JOIN tbl_companies_meta ON tbl_companies_meta.id = tbl_companies.fk_companies_meta SET ".$updatesd." `permissions`=:permissions  WHERE id = :id </br>";
                    // foreach($binded as $key => $value){
                        // echo "this->db->bind(update,:".$key,", ".$value.") </br>";
                    // } 
                    
                    // echo "this->db->bind(permissions,:permissions, ".$permission_string.") </br>";
                    // echo "this->db->bind(id,:id, ".$postData["id_company"].")";
                    $this->db->bind($update,":permissions", $permission_string);
                    $this->db->bind($update,":id", $postData["id_company"]); 
                    if($this->db->execute($update)){
                        
                        $response["message"] = $lang['edited_success'];
                        $response["color"] = "success";
                    }else{
                         
                        $response["message"] = $lang['edited_error'];
                        $response["color"] = "danger";
                    }
                }
            }else{
                //ADD
                if($can_insert){
                    if($response["verif"] !== ""){
                        $response["message"] = $response["verif"];
                        $response["color"] = "danger"; 
                    }else{
                        
                        $adds = $this->db->query("
                        insert ignore into tbl_companies_meta (
                            `company_name`, 
                            `contact_name`, 
                            `country`, 
                            `state`, 
                            `region`, 
                            `city`, 
                            `address`, 
                            `zipcode`,   
                            `phone`, 
                            `profile_image`
                        ) VALUES ( 
                            :company_name, 
                            :contact_name, 
                            :country, 
                            :state, 
                            :region, 
                            :city, 
                            :address, 
                            :zipcode,   
                            :phone, 
                            :profile_image
                        )"); 
                        $this->db->bind($adds,":company_name", (isset($postData['company_name']) ? $postData['company_name'] : "")); 
                        $this->db->bind($adds,":contact_name", $postData['contact_name']); 
                        $this->db->bind($adds,":country", (isset($postData['country']) ? $postData['country'] : "")); 
                        $this->db->bind($adds,":state", (isset($postData['state']) ? $postData['state'] : "")); 
                        $this->db->bind($adds,":region", $postData['region']); 
                        $this->db->bind($adds,":city", $postData['city']); 
                        $this->db->bind($adds,":address", $postData['address']); 
                        $this->db->bind($adds,":zipcode", $postData['zipcode']);  
                        $this->db->bind($adds,":phone", $postData['phone']); 
                        $this->db->bind($adds,":profile_image", $postData['profile_image']); 
                        if($this->db->execute($adds)){
                            
                            $current_page_id = $this->db->query("SELECT LAST_INSERT_ID() as 'lastid' FROM tbl_companies_meta");
                            $current_page_id = $this->db->single($current_page_id);
                            if($current_page_id->lastid !== 0){
                                $current_page_id = $current_page_id->lastid;
                            }
                            
                            $insert_url = $this->db->query("
                            INSERT INTO tbl_companies(
                                `fk_role`,
                                `fk_companies_meta`,
                                `email`,
                                `username`,
                                `password`,
                                `permissions`,
                                `date_company_created`, 
                                `online` 
                            ) VALUES(
                                :fk_role, 
                                :fk_companies_meta, 
                                :email, 
                                :username, 
                                :password, 
                                :permissions, 
                                '".date("Y-m-d")."',
                                :online
                            )"); 

                            $this->db->bind($insert_url,":fk_role", $postData['fk_role']);
                            $this->db->bind($insert_url,":fk_companies_meta", $current_page_id);
                            $this->db->bind($insert_url,":email", $postData['email']);
                            $this->db->bind($insert_url,":username", $postData['username']);
                            $this->db->bind($insert_url,":password", $postData['password']);
                            $this->db->bind($insert_url,":permissions", $permission_string);
                            $this->db->bind($insert_url,":online", $postData['online']);
                             
                            if($this->db->execute($insert_url)){
                                $response["message"] = $lang['added_success'];
                                $response["color"] = "success";
                            }else{
                                // echo  print_r($insert_url->errorInfo()); 
                                $response["message"] = $lang['added_error'];
                                $response["color"] = "danger";
                            }
                        }else{
                            // echo  print_r($adds->errorInfo()); 
                            $response["message"] = $lang['added_error'];
                            $response["color"] = "danger";
                        }
                    }
                }else{ 
                    $response["message"] = $lang['added_error'];
                    $response["color"] = "danger";
                }
            }
            return $response;
        }catch(PDOException $e){
            return false;
        }
    }
    
    /* Add credit and add transactions */
    
    public function add_credit($id, $postData){
        $_POST = $postData;
        $message = array();
        $transaction = new Transaction();
     
        if(isset($_POST['payment_method']) AND ($_POST['credit'] != "" || $_POST['payment_method'] == 0 || $_POST['payment_method'] == "")){
            if($_POST['description_credit'] == "" || $_POST['description_credit_en'] == ""){
                $return['color'] = 'danger';
                $return['message'] = 'Veuillez écrire une description ou moyen de paiement pour le crédit ajouté';
            } 
            else{
                $query_company = $this->db->query('SELECT * FROM tbl_companies WHERE id = :id_company');
                $this->db->bind($query_company, ":id_company", $_POST['id']);
                $company = object_to_array($this->db->single($query_company, 'array'));
                $credit = str_replace('$','',$_POST['credit']);
                $credit_pre = str_replace(',','.',$credit); 
                if(strpos($credit_pre,'-')){
                    $credit = str_replace('-','',$credit_pre);
                    $credit = $company['solde'] - $credit;
                }else{
                    $credit = $company['solde'] + $credit_pre;
                }

                //echo $credit;
                $update = $this->db->query('
                    UPDATE `tbl_companies`
                    SET `solde`= :credit
                    WHERE `id` = :id
                ');
                $this->db->bind($update, ":credit", $credit);
                $this->db->bind($update, ":id", $_POST['id']);

                if($this->db->rowCount($update) > 0){
                    $transaction->add_transaction($_POST, $credit_pre, "Company");
                 
                    $return['color'] = 'success';
                    $return['message'] = 'Vous avez bien changé le crédit de se compte'; 
                }else{
                    $return['color'] = 'danger';
                    $return['message'] = 'Une erreur est survenue durant le changement du crédit';
                }
            }
            unset($_POST);
        }else{
            $return['color'] = 'danger';
            $return['message'] = 'Vous devez ajouter un montant et un moyen de paiement';
        }
        return $return;
    }
}
?>