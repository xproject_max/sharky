<?php
    class Promotions extends Controller {
		
        public $page = [];
        public $lang = "";
		
        public function __construct()
        {
            $this->page = $this->model("Promotion");
            $this->lang = $this->getLang();
        }
        public function promotions(){
            
            $page = $this->page->getPage(24);
            $menu_array = $this->page->getMenu(); 
            $data = [
                "title" => "Pages",
                "lang" =>  $this->lang,
                "menu_array" => $menu_array,
                "page" =>  $page
            ];
            $this->view("promotions/promotions", $data);
        }
 
    }