<?php
    class Contests extends Controller {
		
        public $page = [];
        public $lang = "";
		
        public function __construct()
        {
            $this->page = $this->model("Contest");
            $this->lang = $this->getLang();
        }
        public function contests(){
            
            $page = $this->page->getPage(23);
            $menu_array = $this->page->getMenu(); 
            $data = [
                "title" => "Pages",
                "lang" =>  $this->lang,
                "menu_array" => $menu_array,
                "page" =>  $page
            ];
            $this->view("contests/contests", $data);
        }
 
    }