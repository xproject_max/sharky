<?php


class Users extends Controller
{
    public $page = [];
    public $user = [];
    public $lang = "";
    public $data_secure = [];

    
    public function __construct(){ 
        $this->page = $this->model("Page");
        $this->user = $this->model("User");
        $this->lang = $this->lang;
    }

    public function users(){
        $page = $this->page->getPage(11);
        //Init data
        $data = [
            "title" => "Home",
            "lang" =>  $this->lang,
            "page" =>  $page,
        ];

        //Send view
        $this->view("users/users", $data);
    }
    
    public function user(){
        
        $page = $this->page->getPage(26);
        if($_SERVER['REQUEST_METHOD'] == "GET"){
            $user = $this->user->getUser($_GET['userid']);
        } 
        $data = [
            "title" => "Home",
            "lang" =>  $this->lang,
            "user" =>  $user,
            "page" =>  $page,
        ];
        //Send view
        $this->view("users/user", $data);
        
    }
}