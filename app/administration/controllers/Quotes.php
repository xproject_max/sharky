<?php 
    class Quotes extends Controller {
        public $page = [];
        public $lang = "";
        public function __construct()
        {
            $this->page = $this->model("Quotes_model");
            $this->lang = $this->getLang();
        }

        public function quotes(){

            $page = $this->page->getPage(30);
            $data = [
                "title" => "Home",
                "lang" =>  $this->lang,
                "page" => $page,
				"getquotes" => $this->page->getallquotes()
            ];
            $this->view("quotes/quotes", $data);
        }
        public function quote(){

            $page = $this->page->getPage(32);
            $data = [
                "title" => "Home",
                "lang" =>  $this->lang,
                "page" => $page,
				"getquotes" => $this->page->getquotes(32)
            ];
            $this->view("quotes/quote", $data);
        }
	}
?>