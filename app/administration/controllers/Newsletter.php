<?php
    class Newsletter extends Controller {
        public $page = [];
        public $lang = "";
        public function __construct()
        {
            $this->page = $this->model("Page");
            $this->lang = $this->getLang();
        }

        public function index(){

            $page = $this->page->getPage(29);
            $data = [
                "title" => "Home",
                "lang" =>  $this->lang,
                "page" => $page
            ];
            $this->view("newsletter/newsletter", $data);
        }
        public function newsletter(){

            $page = $this->page->getPage(29);

            $data = [
                "title" => "Home",
                "lang" =>  $this->lang,
                "page" => $page
            ];
            $this->view("newsletter/newsletter", $data);
        }

        public function templates(){

            $page = $this->page->getPage(29);

            $data = [
                "title" => "Home",
                "lang" =>  $this->lang,
                "page" => $page
            ];
            $this->view("newsletter/templates", $data);
        }

        public function campains(){

            $page = $this->page->getPage(29);

            $data = [
                "title" => "Home",
                "lang" =>  $this->lang,
                "page" => $page
            ];
            $this->view("newsletter/campains", $data);
        }

    }
