<?php
    class Blogs extends Controller {
		
        public $page = [];
        public $lang = "";
		
        public function __construct()
        {
            $this->page = $this->model("Blog");
            $this->lang = $this->getLang();
        }
        public function blogs(){
            
            $page = $this->page->getPage(22);
            $menu_array = $this->page->getMenu(); 
            $data = [
                "title" => "Pages",
                "lang" =>  $this->lang,
                "menu_array" => $menu_array,
                "page" =>  $page
            ];
            $this->view("blogs/blogs", $data);
        }
 
    }