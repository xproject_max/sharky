<?php
    class Orders extends Controller {
        public $page = [];
        public $lang = "";
        public function __construct()
        {
            $this->page = $this->model("Order");
            $this->lang = $this->getLang();
        }
        public function orders(){
            
            $page = $this->page->getPage(9);
            $data = [
                "title" => "Home",
                "lang" =>  $this->lang,
                "page" => $page
            ];
            $this->view("orders/orders", $data);
        }
        public function order(){
            
            $page = $this->page->getPage(31);
            $data = [
                "title" => "Home",
                "lang" =>  $this->lang,
                "page" => $page
            ];
            $this->view("orders/order", $data);
        }
 
    }