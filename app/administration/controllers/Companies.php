<?php


class Companies extends Controller
{
    public $page = [];
    public $user = [];
    public $lang = "";
    public $data_secure = [];

    public function __construct(){ 
        $this->page = $this->model("Page");
        $this->company = $this->model("Company");
        $this->lang = $this->getLang();
    }

    public function companies(){
        $page = $this->page->getPage(12);
        //Init data
        $data = [
            "title" => "Home",
            "lang" =>  $this->lang,
            "page" =>  $page,
        ];

        //Send view
        $this->view("companies/companies", $data);
    }
    public function company(){
            
        $page = $this->page->getPage(27);
        if($_SERVER['REQUEST_METHOD'] == "GET"){
            $company = $this->company->getcompany($_GET['companyid']);
        } 
        $data = [
            "title" => "Home",
            "lang" =>  $this->lang,
            "company" =>  $company,
            "page" =>  $page,
        ];
        //Send view
        $this->view("companies/company", $data);
        
    }
}