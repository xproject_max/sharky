<?php
    class Roles extends Controller {
        public $page = [];
        public $lang = "";
        public function __construct()
        {
            $this->page = $this->model("Page");
            $this->lang = $this->getLang();
        }
        public function roles(){
            
            $page = $this->page->getPage(17);
            $data = [
                "title" => "Home",
                "lang" =>  $this->lang,
                "page" => $page
            ];
            $this->view("roles/roles", $data);
        }
        public function website_roles(){
            
            $page = $this->page->getPage(25);
            $data = [
                "title" => "Home",
                "lang" =>  $this->lang,
                "page" => $page
            ];
            $this->view("roles/website_roles", $data);
        }
 
    }