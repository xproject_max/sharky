<?php
    class Products extends Controller {
        public $page = [];
        public $lang = "";
        public function __construct()
        {
            $this->page = $this->model("Product");
            $this->lang = $this->getLang();
        }
        public function products(){
            
            $page = $this->page->getPage(10);
            $menu_array = $this->page->getMenu(); 
            $data = [
                "title" => "Pages",
                "lang" =>  $this->lang,
                "menu_array" => $menu_array,
                "page" =>  $page
            ];
            $this->view("products/products", $data);
        }
 
    }