<?php
    class Pages extends Controller {
        public $page = [];
        public $lang = "";
        public function __construct()
        {
            $this->page = $this->model("Page");
            $this->orders = $this->model("Order");
            $this->users = $this->model("User");
            $this->lang = $this->lang;
        }
        public function index(){
			if(has_rights_to_see($_SESSION['status'], 1)){
            $total = $this->orders->TotalPaid();
            $Three_Months_Total_Sold = $this->orders->Three_Months_Total_Sold();
            $total_accounts = $this->users->Total_accounts();
            $Three_Months_Total_Accounts = $this->users->Three_Months_Total_Accounts();
            $page = $this->page->getPage(1);
            $data = [
                "title" => "Home",
                "lang" =>  $this->lang,
                "Orders" => ["total"=>$total,"Three_months" =>$Three_Months_Total_Sold],
                "Users" => ["total"=>$total_accounts,"Three_months" =>$Three_Months_Total_Accounts],
                "page" => $page
            ];
			
				$this->view("pages/index", $data);
			
			}else{

            $menu_array = $this->page->getMenu();
            $page = $this->page->getPage(4);
            $data = [
                "title" => "Pages",
                "lang" =>  $this->lang,
                "menu_array" => $menu_array,
                "page" =>  $page
            ];
            $this->view("pages/pages", $data);
			}
        }
        public function about($id=""){

            $page = $this->page->getPage(8);
            $data = [
                "title" => "about",
                "lang" =>  $this->lang,
                "page" =>  $page
            ];
            $this->view("pages/about", $data);
        }
        public function contact($id=""){

            $page = $this->page->getPage(2);
            $data = [
                "title" => "about",
                "lang" =>  $this->lang,
                "page" =>  $page
            ];
            $this->view("pages/contact", $data);
        }
        
        public function pages($id=""){

            $menu_array = $this->page->getMenu();
            $page = $this->page->getPage(4);
            $data = [
                "title" => "Pages",
                "lang" =>  $this->lang,
                "menu_array" => $menu_array,
                "page" =>  $page
            ];
            $this->view("pages/pages", $data);
        }
        public function page($id=""){

            $menu_array = $this->page->getMenu();
            // $page = $this->page->getPage(18);
            // $data = [
                // "title" => "Pages",
                // "lang" =>  $this->lang,
                // "menu_array" => $menu_array,
                // "page" =>  $page
            // ];

            //send view
            $this->view("pages/page", []);
        }

        public function modules(){

            $data = [];
            $page = $this->page->getPage(6);
            $data = [
                "title" => "Pages",
                "lang" =>  $this->lang,
                "page" =>  $page
            ];
            $this->view("pages/modules", $data);
        }
        public function module($id=""){
            //send view
            $this->view("pages/module", []);
        }


        public function inputs(){
            if($_SERVER['REQUEST_METHOD'] == "POST"){
                //Process form

                //Sanitize datas
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
                $data = [];
                $page = $this->page->getPage(7);
                $postData = $this->page->updateOrdersInputs($_POST);
                $data = [
                    "title" => "Pages",
                    "lang" =>  $this->lang,
                    "page" =>  $page,
                    "posted" => $postData
                ];
                $this->view("pages/inputs", $data);
            }else{

                $data = [];
                $page = $this->page->getPage(7);
                $data = [
                    "title" => "Pages",
                    "lang" =>  $this->lang,
                    "page" =>  $page
                ];
                $this->view("pages/inputs", $data);
            }
        }

        public function templates(){

            $data = [];
            $page = $this->page->getPage(8);
            $data = [
                "title" => "Pages",
                "lang" =>  $this->lang,
                "page" =>  $page
            ];
            $this->view("pages/templates", $data);
        }

        public function editor(){

            $data = [];
            $page = $this->page->getPage(32);
            $data = [
                "title" => "Pages",
                "lang" =>  $this->lang,
                "page" =>  $page
            ];
            $this->view("pages/editor", $data);
        }

    }
