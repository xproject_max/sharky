<?php
    class Medias extends Controller {
        public $page = [];
        public $lang = "";
        public function __construct()
        {
            $this->page = $this->model("Page");
            $this->media = $this->model("Media");
            $this->lang = $this->getLang();
        }
        public function medias(){
            
            $page = $this->page->getPage(16);
            $data = [
                "title" => "Home",
                "lang" =>  $this->lang,
                "page" => $page
            ];
            $this->view("medias/medias", $data);
        }
        public function media(){
            
            $page = $this->page->getPage(28);
            $media = [];
            if($_SERVER['REQUEST_METHOD'] == "GET"){
                $media = $this->media->getmedias($_GET['mediaid']);
            } 
            $data = [
                "title" => "Home",
                "lang" =>  $this->lang,
                "media" =>  $media,
                "page" => $page
            ];
            $this->view("medias/media", $data);
        }
 
    }