<?php
	if(((is_array($data) && empty($data)) || (is_object($data) && !property_exists($data, "id")))){
		redirect("pages/index");
	}

	require_once APPROOT."/config/lang.php";
	include_once APPROOT."/helpers/function.php";
	 
	//Initialize $page
	$page_id = 20; //inputs form page

	if(!is_object($data)){
		if(isset($data[1])){
			$page = $data[1];
		}else{
			$page = $data;
		}
		if(isset($page['load_add_inputs'])){
			$add = true;
			$form_kind = "form_add_inputs";
		}else{
			$form_kind = "form_edit_inputs";
		}
	}else{
		$page = $data;
		$add = false;
		$form_kind = "form_edit_inputs";
	}

	//Initialize classes
	$db = new Database();
	$form = new Form();

	/* GET OPTIONS FOR TYPE SELECT LIST */
	$options_type = "";
	foreach(getInputsType() as $type){
		$options_type .= '<option '.(isset($page->type) && $page->type == $type ? "selected='selected'" : "").">".$type."</option>";
	}

	/* GET OPTIONS FOR RIGHTS LIST */
	$options_rights = "";
	$rights = $db->query("SELECT * FROM tess_members_roles ORDER BY `right` ASC");
	$db->execute($rights);
	while($role = $db->fetch($rights)){
		$options_rights .= (isset($role->right) ? '<option value="'.$role->right.'" '.($_SESSION['status'] == $role->right ? 'selected="selected"' : '').'>'.$role->name.'</option>' : '');
	}

	/* GET OPTIONS FOR POSITION OF NOTE */
	$options_position_notes = "";
	foreach(getNotesPosition() as $position => $value){
		$options_position_notes .= '<option value="'.$position.'" '.(isset($page->note_end) && $page->note_end == $position ? "selected='selected'" : "").'>'.$value.'</option>';
	}
	
	$getRights = $db->query("SELECT rights FROM tbl_pages_admin WHERE id = :id");
	$db->bind($getRights, ":id", $page_id);
	$rights = $db->single($getRights)->rights;
	if($rights < $_SESSION['status']){
        die('no rights');
    }

?>
<div id="content" class="pages category modules">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_inputs'] ?></h1>
	</div>
	<a class="btn btn-success go_back" href="<?= URLROOT.getUrlLang(7) ?>" ><?= $lang['back'] ?></a>
	<?= (isset($add) ? 'CHAMPS' : $page->{'title'.$l}) ?></br>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="<?= $form_kind ?>" page-id="<?= $page_id ?>"  enctype="multipart/form-data">
					<?php //input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="",$note_position="", $note="", $attributes) ?>
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("name", "text", $lang['input_name'], false, (!$add ? $page->name : '')).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("title", "text",$lang['input_title'], false, (!$add ? $page->title : '')).'</div>'; ?>
					<?= '<div class="clear"></div>'; ?>
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("right", "select", $lang['rights_roles'], false, "","","",$options_rights).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("type", "select", $lang['input_type'], false, (!$add ? $page->type : ''),"","",$options_type).'</div>'; ?>
					<?= '<div class="clear"></div>'; ?>
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("value", "textarea", "val/options", false, (!$add ? $page->value : ''),"","","","","","").'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("note_end", "select", "Position note", false, "","","",$options_position_notes).'</div>'; ?>
					<?= '<div class="clear"></div>'; ?>
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("note", "textarea", "Note", false, (!$add ? $page->note : '')).'</div>'; ?>
					<?= (MULTI_LANG && $add ? '<div class="omega grid_6 grid_1600_12">'.$form->input("inputs_bilingue", "toggle", "Input multi-lang?", false, (!$add ? $page->title : '')).'</div>' : ''); ?>
					<?= '<div class="clear"></div>'; ?>
					<?= (!$add ? '<input type="hidden" value="'.$page->id.'" name="inputs_id" />' : '') ?>
					<?= '<div class="clear"></div><div class="grid_12 grid_1600_12"><input type="submit" class="btn btn-success" value="'.(!$add ? $lang['edit'] : $lang['add']).'" name="submit" /></div>'; ?>
				</form>
			</div>
		</div>
	</div>
</div>
