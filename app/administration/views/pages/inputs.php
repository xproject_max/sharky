<?php
	require_once APPROOT . "/views/inc/header.php";

	$db = new Database();
	if($page->rights < $_SESSION['status']){
        die('no rights');
    }
?>
<div id="content" url="<?= URLROOT."".getUrlLang(7) ?>" class="categories inputs">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_inputs'] ?></h1>
	</div>
	<input type="button" class="btn btn-success new_inputs" value="<?= $lang['add']; ?>"   />
	</br>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="update_status_inputs" name="update_status_inputs">
					<table class="table" style=" margin: 0 auto;">
						<thead>
							<tr>
								<th style="padding-right:15px;"><?= $lang['pages_th_order'] ?></th>
								<th><?= $lang['input_name'] ?></th>
								<th class="td-actions"><?= $lang['input_title'] ?></th>
								<th></th>
								<th class="text-right" style="padding-right:15px;"><?= $lang['pages_th_actions'] ?></th>
							</tr>
						</thead>
						<tbody>
						<?php
							try{
								$query_inputs = $db->query("SELECT * FROM inputs ORDER BY `order` ASC");
								$result = $db->execute($query_inputs);
								$counted = $db->rowCount($query_inputs);
								while ($inputs = $query_inputs->fetch($result)) {

									echo '
										<tr>
											<td>
												<input class="text-center" type="hidden" name="id[]"
												style="text-align:center;width:15px !important;"
												value="'.$inputs->id.'">
												<input type="text" name="order[]"
												style="text-align:center;width:30px !important;
												background:black;color:white;"
												value="'.$inputs->order.'">
											</td>
											<td>
												<p data-tooltip="'.$inputs->desc.'">'.$inputs->title.'</p>
											</td>
											<td>
												'.$inputs->name.'
											</td>
											<td>
											</td>
											<td class="td-actions text-right" style="line-height:1.3; width:43px;">
												<a title="Modifier" id="' . $inputs->id .'" class="btn btn-warning new_inputs" href="#">
												<i class="fas fa-pen-square"></i>
												</a>
												<a id="'.$inputs->id.'" title="Supprimer" class="btn btn-danger delete-inputs" >
												<i class="fas fa-times-square"></i>
												</a>
											</td>
										</tr>
									';
								}
							}catch(PDOException $e){
								return false;
							}
						?>
						</tbody>
					</table></br>
					<input type="submit" class="btn btn-info" style="color:white !important;background: black !important;margin-top:15px"
					 value="<?= $lang['pages_th_save'] ?>" name="order_inputs">

				</form>
			</div>
		</div>
	</div>
</div>
<?php
require_once APPROOT . "/views/inc/footer.php";
?>
