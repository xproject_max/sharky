<?php
require_once APPROOT . "/views/inc/header.php";
$menu_array = $data['menu_array'];

if($page->rights < $_SESSION['status']){
	die('no rights');
}
?>
<script src="<?= URLROOT ?>js/screen.js"></script>
<script>

</script>
<div id="content" url="<?= URLROOT."".getUrlLang(32) ?>" class="pages categories">
	<div class="card header-title text-center">
		<h1>Éditeur de code</h1>
	</div> 
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive"> 
				<div class="editbich" style="position:relative;width:100%;height:100%;min-height: 1000px;"> 
					<object data="<?= PROTOCOL ?>://<?= DOMAINNAME ?>/administration/bich.php" type="text/html" style="overflow:hidden"
					width="100%" height="1000px" typemustmatch></object>
				 </div> 
			</div>
		</div>
	</div>
</div>
<?php
require_once APPROOT . "/views/inc/footer.php";
?>
