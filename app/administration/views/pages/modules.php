<?php
	require_once APPROOT . "/views/inc/header.php";

	$db = new Database();
	if($page->rights < $_SESSION['status']){
        die('no rights');
    }
?>
<div id="content" url="<?= URLROOT."".getUrlLang(6) ?>" class="categories modules">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_modules'] ?></h1>
	</div>
	<input type="button" class="btn btn-success new_module" value="<?= $lang['add']; ?>"   />
	<div class="mtop">
	</br>
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="update_status" name="update_status">
					<table class="table" style=" margin: 0 auto;">
						<thead>
							<tr>
								<th style="padding-right:15px;">ID</th>
								<th>Module</th>
								<th class="td-actions"><?= $lang['pages_th_templates'] ?></th>
								<th></th>
								<th class="text-right" style="padding-right:15px;"><?= $lang['pages_th_actions'] ?></th>
							</tr>
						</thead>
						<tbody>
						<?php
							try{
								$query_modules = $db->query("SELECT * FROM tess_controller");
								$result = $db->execute($query_modules);
								$counted = $db->rowCount($query_modules);
								while ($categ_list = $query_modules->fetch($result)) {

									$query_templates = $db->query("SELECT * FROM templates WHERE id = :id");
									$db->bind($query_templates,":id", $categ_list->templates);
									$templates = $db->single($query_templates);
									echo '
										<tr>
											<td>
												<p>'.$categ_list->id.'</p>
											</td>
											<td>
												<p data-tooltip="'.$categ_list->desc.'">'.$categ_list->title.'</p>
											</td>
											<td>
												'.(isset($templates->name) ? $templates->name : "(No templates)").'
											</td>
											<td>
											</td>
											<td class="td-actions text-right" style="line-height:1.3; width:43px;">
												<a title="Modifier" id="' . $categ_list->id .'" class="btn btn-warning new_module" href="#">
													<i class="fas fa-pen-square"></i>
												</a>
												<a id="'.$categ_list->id.'" title="Supprimer" class="btn btn-danger delete-module" >
													<i class="fas fa-times-square"></i>
												</a>
											</td>
										</tr>
									';
								}
							}catch(PDOException $e){
								return false;
							}
						?>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
require_once APPROOT . "/views/inc/footer.php";
?>
