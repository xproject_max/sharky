<?php

    //Require headers and lang
    require_once APPROOT . "/views/inc/header.php";
	// var_dump($data['Orders']['Three_months'][0]->total_one);
    //Body


	function getTableLog(){
		  $db = new Database();
		  $data ="";
		$queryLogs = $db->query("SELECT * FROM tbl_debug ORDER BY id DESC");
		$db->execute($queryLogs);
		$count = $db->rowCount($queryLogs);
		$data .= '<table class="tablelog">
		<tr class="firstrow"><th>Evenement ('.$count.')</th><th>Date</th><th>Auteur</th></tr>';
		while($row = $db->fetch($queryLogs)){
			$data .= '<tr><td>'.$row->event.'</td><td>'.$row->date.'</td><td>'.$row->fk_users.'</td></tr>';
		}
		$data .= '</table>';
		return $data;
	}

    function getLastOrders(){
        $db = new Database();
        $data ="";
        $queryLogs = $db->query("SELECT * FROM tbl_orders ORDER BY date_created DESC");
        $db->execute($queryLogs);
        $count = $db->rowCount($queryLogs);
        $data .= '<table class="tableorders table">
        <tr class="firstrow"><th>Commandes ('.$count.')</th><th>Acheteur</th><th>Prix</th><th>Payé?</th><th>Paiement</th><th>Date</th></tr>';
        while($row = $db->fetch($queryLogs)){
            $listProduct = array();
            $queryProducts = $db->query('Select
            po.*, p.title FROM tbl_products_orders po LEFT JOIN tbl_products p ON p.id = po.fk_products WHERE po.fk_orders = :orderid');
            $db->bind($queryProducts, ":orderid", $row->id);
            $db->execute($queryProducts);
            while($rproduct = $db->fetch($queryProducts)){
               array_push($listProduct, $rproduct->quantity. " x ".$rproduct->title);
            }
            $listProduct = implode(",",$listProduct);
        $data .= '<tr><td>#'.$row->id.' ('.$listProduct.')</td><td>user #:'.$row->fk_users.'</td><td>'.$row->final_price.'</td><td>'.($row->paid == 1 ? "Payé" : "Non payé").'</td><td>'.$row->payment_method.'</td><td>'.$row->date_created.'</td> </tr>';
        }
        $data .= '</table>';
        return $data;
    }

    function getTopBuyedProducts(){
        $db = new Database();
        $data ="";
        $queryLogs = $db->query("SELECT *,fk_products, count(*) Total FROM tbl_products_orders GROUP BY fk_products ORDER BY total DESC");
        $db->execute($queryLogs);
        $count = $db->rowCount($queryLogs);
        $data .= '<table class="tableproducts">
        <!--tr class="firstrow"><th>Products ('.$count.')</th><th>Total</th></tr-->';
        while($row = $db->fetch($queryLogs)){
            $listProduct = array();
            $queryProducts = $db->query('Select
           * FROM tbl_products WHERE id = :orderid');
            $db->bind($queryProducts, ":orderid", $row->fk_products);
            $db->execute($queryProducts);
            while($rproduct = $db->fetch($queryProducts)){
                array_push($listProduct, $rproduct->title);
                $img = $rproduct->image;
            }
            $listProduct = implode(",",$listProduct);
        $data .= '<tr><td><img src="'.URLCLIENT.'uploads/'.SITENAME.'/'.$img.'" alt="product" width="100px"></td><td>#'.$row->fk_products.' ('.$listProduct.')</td><td>'.$row->Total.' fois</td></tr>';
 
	   }
        $data .= '</table>';
        return $data;
    }
	$file = file_get_contents(URLCLIENT.'plugins/counter.txt');
	if($page->rights < $_SESSION['status']){
        die('no rights');
    }


    echo'
	<div id="content" class="dashboard">
		<style>
			.pace-running {
				background: black url('.URLCLIENT.'images/bg11.jpg);
				height: 100%;
				width: 100%;
				position: fixed;
				z-index: 9999 !important;
				top: 0;

			}.pace-running .pace {
				background: black url('.URLCLIENT.'images/bg11.jpg);
				height: 100%;
				width: 100%;
				position: fixed;
				z-index: 9999;
				top: 0;

			}
		</style>
    	<div class="card header-title text-center">
    		<h1>'.$lang['menu_dashboard'].'</h1>
    	</div>
			<div class="row">
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="card card-stats">
						 <img src="'.(file_exists(URLIMG.$_SESSION['profile_image']) ? URLIMG.$_SESSION['profile_image'] :  URLROOT.'/images/logo/logo.png').'" />
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="card card-stats"> 
						<div class="card-content">
							<i class="fad fa-analytics categ_icon"></i>
							<p class="category">'.$lang['website_visits'].'</p>
							<h3 class="card-title counter-count">'.$file.'</h3>
						</div>
						<div class="card-footer">
							<div class="stats">
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="card card-stats"> 
						<div class="card-content">
							<i class="fad fa-usd-circle categ_icon"></i>
							<p class="category">'.$lang['revenue'].' </p>
							<div><h3 class="card-title counter-count" style="display:inline-block">  '.number_format($data['Orders']["total"][0]->total, 2, '.','').' </h3><sup>$</sup>
							</div>
						</div>
						<div class="card-footer">
							<div class="stats">
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="card card-stats"> 
						<div class="card-content">
							<i class="fad fa-user categ_icon"></i>
							<p class="category">'.$lang['website_accounts'].'</p>
							<h3 class="card-title counter-count">'.($data['Users']['total'][0]->total ? $data['Users']['total'][0]->total:  0).'</h3>
						</div>
						<div class="card-footer">
							<div class="stats">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="card card-chart">
					<h4 class="middletitle">Console administrative</h4>
						<div class="card-content">
						 '.getTableLog().'
					</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card card-chart">
						
						<h4 class="middletitle">'.$lang['total_last_three_months'].'</h4>
						<div class="card-content">
						<div class="card-header" data-background-color="green" data-header-animation="false">
							<div class="ct-chart"
							data-first-month="'.($data['Orders']['Three_months'][0]->total_one ? $data['Orders']['Three_months'][0]->total_one :  0) .'"
							data-second-month="'.($data['Orders']['Three_months'][0]->total_two ? $data['Orders']['Three_months'][0]->total_two :  0).'"
							data-third-month="'.($data['Orders']['Three_months'][0]->total_third ? $data['Orders']['Three_months'][0]->total_third : 0).'"
							data-highest-value="'.(is_array($data['Orders']['Three_months']) ? getHighestValue($data['Orders']['Three_months']) : 50).'"
							id="dailySalesChart"></div>
						</div>
						</div>
						<div class="card-footer">
							<div class="stats">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card card-chart">
						<h4 class="middletitle">'.$lang['accounts_last_three_months'].'</h4>
						
						<div class="card-content">
						<div class="card-header" data-background-color="blue" data-header-animation="false">
							<div class="ct-chart"
							data-first-month="'.($data['Users']['Three_months'][0]->total_one ? $data['Users']['Three_months'][0]->total_one : 0) .'"
							data-second-month="'.($data['Users']['Three_months'][0]->total_two ? $data['Users']['Three_months'][0]->total_two :  0).'"
							data-third-month="'.($data['Users']['Three_months'][0]->total_third ? $data['Users']['Three_months'][0]->total_third : 0).'"
							data-highest-value="'.(is_array($data['Users']['Three_months']) ? getHighestValue($data['Users']['Three_months']) : 50).'"
							id="completedTasksChart"></div>
						</div>
						</div>
						<div class="card-footer">
							<div class="stats">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="card" style="padding:4%">
						<div class="card-content">
							<div class="row">
                                <div class="col-md-6">
                                    <h4  class="middletitle">Dernières commandes</h4>
                                    '.getLastOrders().'
								</div>
                                <div class="col-md-6">
                                    <h4  class="middletitle">Top produits achetés</h4>
                                    '.getTopBuyedProducts().'
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>';
    //Require footer
    require_once APPROOT . "/views/inc/footer.php";
