<?php
	//Require headers and lang
	require_once APPROOT . "/views/inc/header.php";
?>
<div id="content" class="services">
	<div class="fixed_service_title">
		<span class="myBtn"><?= $page->{'primary_title'.$l} ?></span>
	</div>
	<?php
		
		$query_get_alls = $db->query("SELECT * ,tbl_pages_inputs.value as value, i.name as name FROM tbl_pages_inputs LEFT JOIN inputs i ON i.id = tbl_pages_inputs.type WHERE fk_pages = :id");
		
		$db->bind($query_get_alls, ":id", $page->id);
		$db->execute($query_get_alls);
		while ($cust = $db->fetch($query_get_alls)) {
			if(isset($page->{$cust->name}) && $cust->name != "")
				$page->{$cust->name} = $cust->value;
		}
		echo ' 
		' . getBlocksByType($page->id) . '
			';
	?>
</div>
<?php
	//Require footer
	require_once APPROOT . "/views/inc/footer.php";
?>