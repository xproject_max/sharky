<?php
	if(((is_array($data) && empty($data)) || (is_object($data) && !property_exists($data, "id")))){
		redirect("pages/index");
	}
	require_once APPROOT."/config/lang.php";
	include_once APPROOT."/helpers/function.php";
	//Initialize $page
	$page_id = 19; //module form page
	if(!is_object($data)){
		if(isset($data[1])){
			$page = $data[1];
		}else{
			$page = $data;
		}
		if(isset($page['load_add_module'])){
			$add = true;
			$form_kind = "form_add_module";
		}else{
			$form_kind = "form_edit_module";
		}
	}else{
		$page = $data;
		$add = false;
		$form_kind = "form_edit_module";
	}
	$db = new Database();
	if(!$add) {
		$modif = $db->query('SELECT * FROM tess_controller WHERE id= :id');
		$db->bind($modif, ':id', $page->id);
		$modif = $db->single($modif);
	}else{
		$modif = $page['page'];
	}

	$templates_option = '';
	$res = $db->query('SELECT * FROM templates');
	$db->execute($res);

	while($donnees = $db->fetch($res)){
		$templates_option .= '<option value="'.$donnees->id.'" '.(!$add && $modif->templates == $donnees->id ? 'selected="selected"' : '').'>'.$donnees->name.'</option>
		';
	}


	$member_type_add = '';
	$add_role = $db->query('SELECT * FROM tess_members_roles');

	$db->execute($add_role);

	while($donnees = $db->fetch($add_role)){
		$member_type_add .= '<option value="'.$donnees->right.'" '.(!$add && $modif->add == $donnees->right ? 'selected="selected"' : '').'>'.$donnees->name.'</option>
		';
	}

	$member_type_delete = '';
	$del = $db->query('SELECT * FROM tess_members_roles');
	$db->execute($del);

	while($donnees = $db->fetch($del)){
		$member_type_delete .= '<option value="'.$donnees->right.'" '.(!$add && $modif->del == $donnees->right ? 'selected="selected"' : '').'>'.$donnees->name.'</option>
		';
	}

	$member_type_modify = '';
	$mod = $db->query('SELECT * FROM tess_members_roles');
	$db->execute($mod);

	while($donnees = $db->fetch($mod)){
		$member_type_modify .= '<option value="'.$donnees->right.'" '.(!$add && $modif->mod == $donnees->right ? 'selected="selected"' : '').'>'.$donnees->name.'</option>
		';
	}

	$module_enfant = '';
	if(!$add) {
		$dtypes = $db->query('SELECT dtype FROM tess_controller WHERE id = :id');
		$db->bind($dtypes, ":id", $page->id);
		$dtypes = $db->single($dtypes);
		$pagesSelectionner = explode('-', $dtypes->dtype);
	}

	$q_mod = $db->query('SELECT * FROM tess_controller ORDER BY title ASC');
	$db->execute($q_mod);

	while($donnees = $db->fetch($q_mod)) {
		$module_enfant .= '<option value="'.$donnees->id.'" '.(!$add && in_array($donnees->id, $pagesSelectionner) ? 'selected="selected"' : '').'>'.$donnees->title.'</option>
		';
	}
	//Initialize classes
	$db = new Database();
	$form = new Form();
	$getRights = $db->query("SELECT rights FROM tbl_pages_admin WHERE id = :id");
	$db->bind($getRights, ":id", $page_id);
	$rights = $db->single($getRights)->rights;
	if($rights < $_SESSION['status']){
        die('no rights');
    }
?>
<div id="content" class="pages category modules">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_modules'] ?></h1>
	</div>
	<a class="btn btn-success go_back" href="<?= URLROOT.getUrlLang(6) ?>" ><?= $lang['back'] ?></a>
	<?= (isset($add) ? $lang['new_module'] : $page->{'title'.$l}) ?>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="<?= $form_kind ?>" page-id="<?= $page_id ?>"  enctype="multipart/form-data">
					<?php //input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="",$note_position="", $note="", $attributes) ?>
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("title", "text", $lang['title'], false, (!$add ? $modif->title : '')).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("desc", "textarea", "Description", false, (!$add ? $modif->desc : '')).'</div>'; ?>
					<?= '<div class="clear"></div>'; ?>
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("templates", "select", $lang['templates'], false, "","","",$templates_option).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("module", "text", "Module", false, (!$add ? $modif->module : '')).'</div>'; ?>
					<?= '<div class="clear"></div>'; ?>
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("add", "select", $lang['rights_add'], false, "","","", $member_type_add).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("del", "select", $lang['rights_del'], false, "","","", $member_type_delete).'</div>'; ?>
					<?= '<div class="clear"></div>'; ?>
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("mod", "select", $lang['rights_edit'], false, "","","", $member_type_modify).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("dtype[]", "select", $lang['child_modules'], false, "","","", $module_enfant,"","", 'multiple="multiple"').'</div>'; ?>
					<?= '<div class="clear"></div>'; ?>
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("gallery", "toggle", $lang['gallery'], false, "","","", "","","", ($modif->gallery == 1 ? 'checked="checked"' :'')).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("has_slug", "toggle", "URL page", false, "","","", "","","", ($modif->has_slug == 1 ? 'checked="checked"' :'')).'</div>'; ?>
					<?= '<div class="clear"></div>'; ?>
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("is_product", "toggle", $lang['is_product'], false, "","","", "","","", ($modif->is_product == 1 ? 'checked="checked"' :'')).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("is_blog", "toggle", $lang['is_blog'], false, "","","", "","","", ($modif->is_blog == 1 ? 'checked="checked"' :'')).'</div>'; ?>
					<?= '<div class="clear"></div>'; ?>
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("is_contest", "toggle", $lang['is_contest'], false, "","","", "","","", ($modif->is_contest == 1 ? 'checked="checked"' :'')).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("is_promotion", "toggle", $lang['is_promotion'], false, "","","", "","","", ($modif->is_promotion == 1 ? 'checked="checked"' :'')).'</div>'; ?>
					<?= (!$add ? '<input type="hidden" value="'.$modif->id.'" name="module_id" />' : '') ?>
					<?= '<div class="clear"></div><div class="grid_12 grid_1600_12"><input type="submit" class="btn btn-success" value="'.(!$add ? $lang['edit'] : $lang['add']).'" name="submit" /></div>'; ?>
				</form>
			</div>
		</div>
	</div>
</div>
