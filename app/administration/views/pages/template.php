<?php
	if(((is_array($data) && empty($data)) || (is_object($data) && !property_exists($data, "id")))){
		redirect("pages/index");
	}

	require_once APPROOT."/config/lang.php";
	include_once APPROOT."/helpers/function.php";

	//Initialize $page
	$page_id = 21; //template form page
	if(!is_object($data)){
		if(isset($data[1])){
			$page = $data[1];
		}else{
			$page = $data;
		}
		if(isset($page['load_add_templates'])){
			$add = true;
			$form_kind = "form_add_templates";
		}else{
			$form_kind = "form_edit_templates";
		}
	}else{
		$page = $data;
		$add = false;
		$form_kind = "form_edit_templates";
	}
	//Initialize classes
	$db = new Database();
	$form = new Form();
	$options="";
	$query_inputs = $db->query("SELECT * FROM inputs order by `order` asc");
	$db->execute($query_inputs);
	if($query_inputs){

		while($input = $db->fetch($query_inputs)){
			$r="";
			
			if (isset($input->id) and !$add) {
				$check = $db->query("SELECT * FROM templates where inputs like CONCAT('%-',:valeur,'-%') AND id = :id");
				$db->bind($check, ":valeur", $input->id);
				$db->bind($check, ":id", $page->id);
				$result = $db->single($check);
				if($result){
					$r = "checked";
				}
			}
			$options .= '
				<div class="form-check grid_3 grid_1024_4">
					<label  >
						<input class="form-check-input" type="checkbox" name="inputs[]" value="' . $input->id .'" ' . $r . '>' . $input->name . '
						<span class="form-check-sign">
						<span class="check"></span>
						</span>
					</label>
				</div>';
		}
	}
	$getRights = $db->query("SELECT rights FROM tbl_pages_admin WHERE id = :id");
	$db->bind($getRights, ":id", $page_id);
	$rights = $db->single($getRights)->rights;
	if($rights < $_SESSION['status']){
        die('no rights');
    }
?>
<div id="content" class="pages category modules">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_templates'] ?></h1>
	</div>
	<a class="btn btn-success go_back" href="<?= URLROOT.getUrlLang(8) ?>" ><?= $lang['back'] ?></a>
	<?= (isset($add) ? 'CHAMPS' : $page->{'title'.$l}) ?></br>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="<?= $form_kind ?>" page-id="<?= $page_id ?>"  enctype="multipart/form-data">
					<?php //input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="",$note_position="", $note="", $attributes) ?>
					<?= '<div class="delta grid_12 grid_1600_12">'.$form->input("name", "text", $lang['template'], false, (!$add ? $page->name : ''),"","","","","","").'</div>'; ?>
					<?= '<div class="delta grid_12 grid_1600_12">'.$form->input("checkbox", "checkbox", "Type(s)", false, (!$add ? $page->name : ''),"","",$options,"","","").'</div>'; ?>


					<?= (!$add ? '<input type="hidden" value="'.$page->id.'" name="templates_id" />' : '') ?>
					<?= '<div class="clear"></div><div class="grid_12 grid_1600_12"><input type="submit" class="btn btn-success" value="'.(!$add ? $lang['edit'] : $lang['add']).'" name="submit" /></div>'; ?>
				</form>
			</div>
		</div>
	</div>
</div>
