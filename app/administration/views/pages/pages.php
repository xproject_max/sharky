<?php
require_once APPROOT . "/views/inc/header.php";
$menu_array = $data['menu_array'];

if($page->rights < $_SESSION['status']){
	die('no rights');
}
?>
<script src="<?= URLROOT ?>js/screen.js"></script>
<script>

</script>
<div id="content" url="<?= URLROOT."".getUrlLang(4) ?>" class="pages categories">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_pages'] ?></h1>
	</div>
	 
	<?php
	if($_SESSION['status'] < 2){ 
	?>	
	<input type="button" class="btn btn-success new_page" value="<?= $lang['add']; ?> une page"   />
	<?php
	}
	?> 
	</br>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				 
				<form action="" method="post" id="update_status" name="update_status">
					<table class="table" style=" margin: 0 auto;">
						<thead>
							<tr>
								<th>Dérouler</th>
								<th style="padding-right:15px;"><?= $lang['pages_th_order'] ?></th>
								<th><?= $lang['pages_th_title'] ?></th>
								<th>Dernière modification</th>
								<th class="td-actions text-right"><?= $lang['pages_th_online'] ?></th>
								<th class="text-right" style="padding-right:15px;"><?= $lang['pages_th_actions'] ?></th>
							</tr>
						</thead>
						<tbody  >
							<?= generate_menu(0, $menu_array); ?>
						</tbody>
					</table>
					</br>
					<input type="submit" class="btn btn-info" style="color:white !important;background: black !important;margin-top:15px" value="<?= $lang['pages_th_save'] ?>" name="B1">
				</form>
				 
			</div>
		</div>
	</div>
</div>
<?php
require_once APPROOT . "/views/inc/footer.php";
?>
