<?php
	require_once APPROOT . "/views/inc/header.php";

	$db = new Database();
	if($page->rights < $_SESSION['status']){
        die('no rights');
    }
?>
<div id="content" url="<?= URLROOT."".getUrlLang(8) ?>" class="categories templates">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_templates'] ?></h1>
	</div>
	<input type="button" class="btn btn-success new_templates" value="<?= $lang['add']; ?>"   />
	</br>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post">
					<table class="table" style=" margin: 0 auto;">
						<thead>
							<tr>
								<th style="padding-right:15px;">ID</th>
								<th><?= $lang['templates'] ?></th>
								<th class="text-right" style="padding-right:15px;"><?= $lang['pages_th_actions'] ?></th>
							</tr>
						</thead>
						<tbody>
						<?php
							try{
								$query_templates = $db->query("SELECT * FROM templates ORDER BY `id` ASC");
								$result = $db->execute($query_templates);
								$counted = $db->rowCount($query_templates);
								while ($templates = $query_templates->fetch($result)) {

									echo '
										<tr>
											<td>
												<input class="text-center" type="hidden" name="id[]"
												style="text-align:center;width:15px !important;"
												value="'.$templates->id.'">
												<p>
													'.$templates->id.'
												</p>
											</td>
											<td>
												<p data-tooltip="'.$templates->name.'">'.$templates->name.'</p>
											</td>
											<td class="td-actions text-right" style="line-height:1.3; width:43px;">
												<a title="Modifier" id="' . $templates->id .'" class="btn btn-warning new_templates" href="#">
												<i class="fas fa-pen-square"></i>
												</a>
												<a id="'.$templates->id.'" title="Supprimer" class="btn btn-danger delete-templates" >
												<i class="fas fa-times-square"></i>
												</a>
											</td>
										</tr>
									';
								}
							}catch(PDOException $e){
								return false;
							}
						?>
						</tbody>
					</table></br>

				</form>
			</div>
		</div>
	</div>
</div>
<?php
require_once APPROOT . "/views/inc/footer.php";
?>
