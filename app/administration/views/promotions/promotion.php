	<?php
		if(((is_array($data) && empty($data)) || (is_object($data) && !property_exists($data, "id")))){
			redirect("pages/index");
		}
		require_once APPROOT."/config/lang.php";
		include_once APPROOT."/helpers/function.php";
		//Initialize $page
		if(!is_object($data)){
			if(isset($data[0])){
				$page = $data[0];
			}else{
				$page = $data;
			}
			if(isset($page['load_add_promotion'])){
				$add = true;
				if(isset($page['getModule'])){
					$type = $page['getModule'];
				}
				if(isset($page['parent'])){
					$parentid = $page['parent'];
					$page = $page['page'];
				}
				$form_kind = "form_add_promotion";
			}else{
				$form_kind = "form_edit_promotion";
			}
		}else{
			$page = $data;
			$form_kind = "form_edit_promotion";
		}

		//Initialize classes
		$cat = new Category("tbl_promotions");
		$db = new Database();
		$form = new Form();
		$Page = new promotion();

		if(isset($add) && !isset($type)){
			$page = new ObjectEmpty();
			$module = new ObjectEmpty();
			$template = new ObjectEmpty();
			$is_url_id = new ObjectEmpty();


		}else{
			try{
				//Current module
				$module = $db->query("SELECT * FROM tess_controller WHERE id = :type");
				$db->bind($module,':type', (isset($type) ? $type : $page->type));
				$module = $db->single($module);

				if(!$Page->checkIfModulesExist((isset($type) ? $type : $page->type))){
					$module = getEmptyValue("tess_controller");
				}
				//Current template
				$template = $db->query("SELECT * FROM templates where id = :templates");
				$db->bind($template,":templates", $module->templates);
				$template = $db->single($template);
				if($template){
					$current_inputs = explode('-', $template->inputs);
					//Get Order from inputs
					foreach ($current_inputs as $value){
						$order = $db->query("SELECT `order` FROM inputs WHERE id = :value");
						$db->bind($order,":value", $value);
						$order = $db->single($order);

						if ($order){
							$order = str_pad($order->order, 4, "0", STR_PAD_LEFT);
							$ordered_inputs[] = $order.'-'.$value;
						}
					}
					//Get inputs in array
					if(!empty($ordered_inputs)) {

						sort($ordered_inputs);
						foreach ($ordered_inputs as $value){
							//Clear array from empty cases
							if ($value!='-' or $value!=''){
								$p_inputs = explode('-', $value);
								//Gget all inputs
								$inputs[]=$p_inputs[1];
							}
						}
					}
				}else{
					$inputs[] = "";
				}
				//Get parents
				$parent = $db->query("SELECT * FROM tbl_promotions WHERE id = :parent");
				$db->bind($parent,":parent", (isset($parentid) ? $parentid : $page->parent));
				$parent = $db->single($parent);

				//Detect if has url
				$is_url_id = $db->query("SELECT has_slug FROM tess_controller WHERE id = :template");
				$db->bind($is_url_id,":template", (isset($type) ? $type : $page->type));
				$is_url_id = $db->single($is_url_id);
			}catch(PDOException $e){
				return false;
			}
		}
	?>
	<div class="card header-title text-center">
		<h1><?= $lang['menu_promotions'] ?></h1>
	</div>
	<a class="btn btn-success go_back" href="<?= URLROOT.getUrlLang(24) ?>" ><?= $lang['back'] ?></a>
	<?= (isset($add) ? $lang['new_promotion'] : $page->{'title'.$l}) . (isset($parent) && is_object($parent) ? $lang['child_of'] .$parent->{'title'.$l} : "") ?></br>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="<?= $form_kind ?>" page-id="<?= $page->id ?>"  enctype="multipart/form-data">
					<div class="alpha grid_6 grid_1600_12">
					<?php
						//Get options for parents
						$get_array_categories = $cat->get_cat(0, 'is_promotions'); 
						$options ="<option value='0'> Parent: none </option>";
						foreach ($get_array_categories as $value) {

							$clean = explode('+', $value);
							if($clean[1] !== $page->id){
								$options.= "<option value='$clean[1]' ";
								if ( (isset($parentid) ? $parentid : $page->parent) == $clean[1]) {
									$options.= "SELECTED";
								}
								$options.= ">Parent: ".Cut($clean[0], 110)."</option>";
							}
						}
						//input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="")
						echo $form->input("parent", "select", "Parent",false, "", "","", $options);
					?>
					</div>
					<div class="omega grid_6 grid_1600_12">
					<?php
						try{
							//Get current template and fetch templates
							$options = "";
							if( (isset($parentid) ? $parentid : $page->parent) != 0) {
								$template_id = $db->query('SELECT type FROM tbl_promotions WHERE id= :parent');
								$db->bind($template_id,':parent' ,  (isset($parentid) ? $parentid : $page->parent));
								$template_id = $db->single($template_id);
								$modules = $db->query('SELECT * FROM tess_controller WHERE id= :type AND is_promotion = 1');
								$db->bind($modules,":type", $template_id->type);
								$modules = $db->single($modules);
								$array_dtype = explode('-', $modules->dtype);
								if(!empty($array_dtype)){
									foreach($array_dtype as $id_module) {
										$select = $db->query("SELECT * FROM tess_controller WHERE id= :id_module");
										$db->bind($select,":id_module", $id_module);
										$select = $db->single($select);
										if($select){
											$options .= '<option value="'.$select->id.'"
											'.((isset($type) ? $type : $page->type) == $select->id ||
											(isset($template) ? $template->id : $page->templates) == $select->templates ?
											'selected="selected"' : '').'> Type: '. $select->title . '</option>';
											unset($select);
										}else{
											$options .= '<option value=" " > Type: None </option>';

										}
									}
								}else{
									$options .= '<option value="" > Type: None </option>';
								}
							} else {

								$all_modules = $db->query('SELECT * FROM tess_controller WHERE is_promotion = 1 ORDER BY title');
								$all_modules = $db->resultSet($all_modules);
								foreach($all_modules as $key => $donnees){
									$options .= '<option dtype="'.$donnees->id.'" parent="0" class=" '.(isset($page->title) && $page->title !== ""  ? '' : 'new_promotion').'" value="'.$donnees->id.'"
									'.($module->id == $donnees->id ? 'selected="selected"' : '').'>'.$donnees->title.'</option>';

								}
							}
							//input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="")
							echo $form->input("type", "select", $lang['templates'],false, (isset($template_id->type) ? $template_id->type : isset($page->type) ? $page->type : 0) , "","", $options);
						}catch(PDOException $e){
							return false;
						}
					?>
					</div>
					<div class='clear'></div>
					<div class="alpha grid_6 grid_1600_12">
						<?php
						//input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="")
						echo $form->input("title", "text", $lang['title'], true, str_replace('"', '&quot;', $page->title), "","", "");
						if(MULTI_LANG){
							foreach(LANGUAGES as $l){
								if($l != "fr"){
									echo $form->input("title_".$l, "text", $lang['title']." ".$l, false, str_replace('"', '&quot;', $page->{'title_'.$l}), "","", "");
								}
							}
						}
						?>
					</div>
					<div class="omega grid_6 grid_1600_12">
					<?php
					if(isset($is_url_id->has_slug) && $is_url_id->has_slug == 1){
						try{
							echo'
							<td>
							';
							$current_url_id = $db->query("SELECT url_id
								FROM tbl_promotions_urls WHERE promotion_id = :id
								ORDER BY date_creation DESC LIMIT 1");
							$db->bind($current_url_id,":id", $page->id);
							$current_url_id = $db->single($current_url_id);

							echo $form->input("url_id", "text", "URL", true, (isset($current_url_id->url_id) ? $current_url_id->url_id : ""), "","", "");
							if(MULTI_LANG){
								foreach(LANGUAGES as $l){
									if($l != "fr"){
										$current_url_id_lang = $db->query("SELECT url_id_".$l."
											FROM tbl_promotions_urls WHERE promotion_id = :id
											ORDER BY date_creation DESC LIMIT 1");
										$db->bind($current_url_id_lang,":id", $page->id);
										$current_url_id_lang = $db->single($current_url_id_lang);
										echo $form->input("url_id_".$l, "text", "URL ".$l, false, (isset($current_url_id_lang->{'url_id_'.$l}) ? $current_url_id_lang->{'url_id_'.$l} : ""), "","", "");
									}
								}
							}
							echo'
							</td>
							';
						}catch(PDOException $e){
							return false;
						}
					}
					?>
					</div>
					<div class='clear'></div>
					<?php
					if(!empty($inputs)) {
						$i = 0;
						foreach ($inputs as $key => $myinputs) {

							if ($myinputs) {
								try{
									$row = $db->query("SELECT * FROM inputs where id= :id AND id != 1 AND id != 2");
									$db->bind($row,":id",$myinputs);
									$row = $db->single($row);

									if($row){
										$join = $db->query("SELECT * FROM tbl_promotions_inputs WHERE fk_promotions = :id_page AND type = :id");
										$db->bind($join,":id_page",$page->id);
										$db->bind($join,":id",$myinputs);
										$join = $db->single($join);
										if($join){
											$page->{$row->name} = $join->value;
										}
										if(!isset($page->{$row->name})){
											$page->{$row->name} = "";
										}
										if($i == 0 || (($i % 2 == 0))){
											$div = "alpha";
											$clear = "";
										}
										else if($i % 2 == 1){
											$div = "omega";
											$clear = "<div class='clear'></div>";
										}
										//Type text
										if (($row->type == "Texte" || $row->type == "Textarea" || $row->type == "Text" || $row->type == "Datepicker" || $row->type == "Color") && ($_SESSION['status']  <= $row->right || $row->right == '0')) {
											unset($match);
											if ($page->{$row->name}) {
												$value = $page->{$row->name};
											} else {
												if (preg_match('/^\[(.+)\=(.+)\]/i', $row->value, $regs)) {
													if ($regs[1] == "date") {
														$value = date($regs[2]);
														$match = 1;
													}
												}
												if (!isset($match)) {
													$value = $row->value;
												}
											}
											echo "<div class='".$div." grid_6 grid_1600_12'>".$form->input($row->name, ($row->type == "Color" ? "color" : "text"), $row->title, false, $value, "","".($row->type == "Datepicker" ? "datepicker datetimepicker" : ""), "", $row->note_end, $row->note)."</div>";
										}

										if ($row->type == "WYSIWYG" && ($_SESSION['status']  <= $row->right || $row->right == '0')) {

											echo "<div class='delta grid_12 grid_1600_12'>".$form->input($row->name, "WYSIWYG", $row->title, false, $page->{$row->name}, "",""."", "", $row->note_end, $row->note)."</div>";

										}

										if ($row->type == "Upload Image" && ($_SESSION['status']  <= $row->right || $row->right == '0')) {
											if ($page->{$row->name} == "") {
												//														 input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="", $note_end, $note)
												echo "<div class='".$div." grid_6 grid_1024_12'>".$form->input($row->name, "Upload Image", $row->title, false, $page->{$row->name}, "",""."", $page->id, $row->note_end, $row->note)."</div>";
											}else{
												echo "<div class='".$div." grid_6 grid_1024_12'>".$form->input($row->name, "Upload Image", $row->title, false, $page->{$row->name}, "",""."", $page->id, $row->note_end, $row->note)."</div>";
											}
										}

										if ($row->type == "Upload Autre" && ($_SESSION['status']  <= $row->right || $row->right == '0')) {
											if ($page->{$row->name} == "") {
												//														 input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="", $note_end, $note)
												echo "<div class='".$div." grid_6 grid_1024_12'>".$form->input($row->name, "Upload Autre", $row->title, false, $page->{$row->name}, "",""."", "", $row->note_end, $row->note)."</div>";
											}else{
												echo "<div class='".$div." grid_6 grid_1024_12'>".$form->input($row->name, "Upload Autre", $row->title, false, $page->{$row->name}, "",""."", "", $row->note_end, $row->note)."</div>";
											}
										}

										if ($row->type == "Jointure page" && ($_SESSION['status']  <= $row->right || $row->right == '0')) {
											$query_jointure = $db->query(str_replace('drop','',str_replace('delete','',str_replace('update','',$row->value))));
											$query_jointure = $db->resultSet($query_jointure);
											$options = "";
											foreach($query_jointure as $key => $select){

												$options .= '<option value="'.$select->id.'" '.($page->{$row->name} == $select->id ? 'selected="selected"' : '').'>'.$select->title.'</option>';

											}
											echo "<div class='".$div." grid_6 grid_1024_12'>".$form->input($row->name, "Jointure page", $row->title, false, $page->{$row->name}, "",""."", $options, $row->note_end, $row->note)."</div>";

										}

										if ($row->type == "Radio" && ($_SESSION['status']  <= $row->right || $row->right == '0')) {
											$options = "";
											$seperate = explode ('|', html_entity_decode($row->value));
											foreach($seperate as $ch){
												$value = explode ('&', $ch);
												$options .= '<label>'.$value[0].': <input type="radio"  value="'.$value[1].'" name="' . html_entity_decode($row->name) .'" '; if ($page->{$row->name}==$value[1]){$options .= "checked";} $options .= '></label> | ';
											}
											echo "<div class='".$div." grid_6 grid_1024_12'>".$form->input($row->name, "Radio", $row->title, false, $page->{$row->name}, "",""."", $options, $row->note_end, $row->note)."</div>";

										}

										if (($row->type == "Cat&eacute;gorie" || $row->type == "Checkbox") && ($_SESSION['status']  <= $row->right || $row->right == '0')) {
											$options="";
											if($row->type == "Cat&eacute;gorie")
												$categ = (explode("/",html_entity_decode($row->title)));
											else
												$categ = array(html_entity_decode($row->title));

											$icat = 1;
											$seperate = explode ('|', html_entity_decode($row->value));
											foreach($seperate as $ch){
												$Nch = str_replace("&#039;", "'", $ch);
												$value = explode ('&', $Nch);
												if($row['type'] == "Cat&eacute;gorie")
													$name = explode("/",$value[0]);
												else
													$name = $value;
												$options.= '<div ><div ><label><input type="checkbox" value="'.$value[1].'" name="' . html_entity_decode($row->name) .'[]" '; if ( strstr($page->{$row->name}, "-$value[1]-") ) {$options.= "checked";} $options.= '> <span >'.$name[0].'</span></div> </label></div> ';
												if($icat%4 == 0){
													$options.= '<div ></div>';
												}
												$icat++;
											}
											echo "<div class='".$div." grid_6 grid_1024_12'>".$form->input($row->name, $row->type, $row->title, false, $page->{$row->name}, "",""."", $options, $row->note_end, $row->note)."</div>";

										}
										if (($row->type == "Cat&eacute;gorie radio") && ($_SESSION['status']  <= $row->right || $row->right == '0')) {

											$options = "";
											$categ = (explode("/",html_entity_decode($row->title)));
											$seperate = explode ('|', html_entity_decode($row->value));
											$icat = 1;
											foreach($seperate as $ch){
												$Nch = str_replace("&#039;", "'", $ch);
												$value = explode ('&', $Nch);

												$name = explode("/",$value[0]);

												$options.= '<div ><div ><input type="radio" value="'.$value[1].'" name="' . html_entity_decode($row->name) .'" '; if ( $page->{$row->name} == $value[1])  {$options.="checked";} $options.= '></div><div > '.$name[0].'</div> </div> ';
												if($icat%4 == 0){
													$options.= '<div ></div>';
												}
												$icat++;
											}
											echo "<div class='".$div." grid_6 grid_1024_12'>".$form->input($row->name, $row->type, $row->title, false, $page->{$row->name}, "",""."", $options, $row->note_end, $row->note)."</div>";

										}

										if($i == sizeof($inputs)){
											echo "<div class='clear'></div>";
										}else{
											echo $clear;
										}
										$i++;
									}
								}catch(PDOException $e){
									return false;
								}
							}
						}
					}

					try{
						/* If gallery then add link to it */

						$gallery = $db->query("SELECT * FROM tbl_pictures_categories WHERE page like CONCAT('%-',:id,'-%')");
						$db->bind($gallery,":id", $page->id);
						$gallery = $db->single($gallery);

						if($gallery){
							echo "<div class='delta grid_6 grid_1024_12 text-center'><a href='#' module='gallery' data-id='".$gallery->id."' class='btn btn-info'>".$lang['gallery']."</a></div>";
						}

						 
						echo "<div class='clear'></div>";
						echo '
							<div class="col">
								<input type="submit" value="'.$lang[(isset($add) ? 'add' : 'pages_th_save')].'" class="btn btn-success btn-block">
							</div>';
					}catch(PDOException $e){
						return false;
					}
					?>
				</form>
			</div>
		</div>
	</div>
