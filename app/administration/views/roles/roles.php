<?php
	require_once APPROOT . "/views/inc/header.php";

	$db = new Database();
?>
<div id="content" url="<?= URLROOT."".getUrlLang(17) ?>" class="categories roles">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_roles'] ?></h1>
	</div>
	<input type="button" class="btn btn-success new_roles" value="<?= $lang['add']; ?>"   />
	</br>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="update_status_roles" name="update_status_roles">
					<table class="table" style=" margin: 0 auto;">
						<thead>
							<tr>
								<th style="padding-right:15px;"><?= $lang['pages_th_order'] ?> </th>
								<th><?= $lang['menu_roles'] ?></th>
								<th></th>
								<th class="text-right" style="padding-right:15px;"><?= $lang['pages_th_actions'] ?></th>
							</tr>
						</thead>
						<tbody>
						<?php
							try{
								$query_roles = $db->query("SELECT * FROM tess_members_roles ORDER BY `right` ASC");
								$result = $db->execute($query_roles);
								$counted = $db->rowCount($query_roles);
								while ($roles = $query_roles->fetch($result)) {

									echo '
										<tr>
											<td>
												<input class="text-center" type="hidden" name="id[]"
												style="text-align:center;width:15px !important;"
												value="'.$roles->id.'">
												<input type="text" name="right[]"
												style="text-align:center;width:30px !important;
												background:black;color:white;"
												value="'.$roles->right.'">
											</td>
											<td>
												<p data-tooltip="'.$roles->name.'">'.$roles->name.'</p>
											</td> 
											<td>
											</td>
											<td class="td-actions text-right" style="line-height:1.3; width:43px;">
												<a title="Modifier" id="' . $roles->id .'" class="btn btn-warning add_roles" href="#">
												<i class="fas fa-pen-square"></i>
												</a>
												<a id="'.$roles->id.'" title="Supprimer" class="btn btn-danger delete-roles" >
												<i class="fas fa-times-square"></i>
												</a>
											</td>
										</tr>
									';
								}
							}catch(PDOException $e){
								return false;
							}
						?>
						</tbody>
					</table></br>
					<input type="submit" class="btn btn-info" style="color:white !important;background: black !important;margin-top:15px"
					 value="<?= $lang['pages_th_save'] ?>" name="order_roles">

				</form>
			</div>
		</div>
	</div>
</div>
<?php
require_once APPROOT . "/views/inc/footer.php";
?>
