<?php
	require_once APPROOT . "/views/inc/header.php";

	$db = new Database();
?>
<div id="content" url="<?= URLROOT."".getUrlLang(28) ?>" class="categories medias">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_media'] ?></h1>
	</div> 
	</br>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="update_status_medias" name="update_status_medias">
					<table class="table" style=" margin: 0 auto;">
						<thead>
							<tr>
								<th style="padding-right:15px;">ID</th>
								<th><?= $lang['menu_media'] ?></th>
								<th>Counted images</th>
								<th class="text-right" style="padding-right:15px;"><?= $lang['pages_th_actions'] ?></th>
							</tr>
						</thead>
						<tbody>
						<?php
							try{
								$page_id = 16;
								$id = "16";
								$table = "tbl_pictures_categories";
								$form = new Form(); 

								/* On config la pagination */ 
								$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
								$limit = 10; // Limite d'affichage de transaction par page
								$startpoint = ($page * $limit) - $limit;
								$statement = ' '.$table.' ORDER BY id ASC';
								$balance = 0; 
								echo pagination($statement,$limit,$page,URLROOT.getUrlLang($page_id)."?pages=1&");
								$query_medias = $db->query("SELECT * FROM tbl_pictures_categories ORDER BY `id` ASC  LIMIT :startpoint , :limit");
								$db->bind($query_medias, ":startpoint", $startpoint);
								$db->bind($query_medias, ":limit", $limit);  
								$result = $db->execute($query_medias);
								$counted = $db->rowCount($query_medias);
								while ($medias = $query_medias->fetch($result)) {
									$count_inv = $db->query("SELECT count(*) as countd FROM tbl_pictures
									where categ like CONCAT('%-',:categ_list, '-%')");
									$db->bind($count_inv, ":categ_list", $medias->id);
									$counted_img = $db->single($count_inv)->countd;
									echo '
										<tr mid="'.$medias->id.'" url="'.URLROOT."".getUrlLang(28).'?mediaid='.$medias->id.'" urlinv="'.URLROOT."".getUrlLang(28, "en").'?mediaid='.$medias->id.'">
											<td>
												<p>'.$medias->id.'</p>
											</td>
											<td>
												<p data-tooltip="'.$medias->name.'"><a href="#" id="' . $medias->id .'" >'.$medias->name.'</a></p>
											</td> 
											<td>
												<p>'.$counted_img.'</p>
											</td>
											<td class="td-actions text-right" style="line-height:1.3; width:43px;">
												<a title="Modifier" id="' . $medias->id .'" class="btn btn-warning" href="#">
												<i class="fas fa-pen-square"></i>
												</a>
												<a id="'.$medias->id.'" title="Supprimer" class="btn btn-danger delete-imgCateg" >
												<i class="fas fa-times-square"></i>
												</a>
											</td>
										</tr>
									';
								}
							}catch(PDOException $e){
								return false;
							}
						?>
						</tbody>
					</table></br>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
require_once APPROOT . "/views/inc/footer.php";
?>
