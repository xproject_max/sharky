<?php
	if(((is_array($data) && empty($data)) || (is_object($data) && !property_exists($data, "id")))){
		redirect("pages/index");
	}
	 
	//Initialize $page   
	if(!is_object($data)){
		if(isset($data[0])){
			$page = $data[0];
		}else{
			$page = $data;
		}
		if(is_array($page)){
			if(isset($page['load_add_medias'])){
				$add = true; 
				if(isset($page['getModule'])){
					$type = $page['getModule'];
				}
				$page = $page['page'];
				$form_kind = "form_add_medias";
			}else{
				$form_kind = "form_edit_medias";
			}
		}else{
		    //Require headers and lang
			$page = $data[0]; 
			$form_kind = "form_edit_medias";
			
		}
		if(isset($data['media'])){ 
		    //Require headers and lang
			// var_dump($data);
			// if(empty($data['media'])){
				// redirect(($l == "en" ? "medias" : "medias"));
			// }
		    require_once APPROOT . "/views/inc/header.php";
			$page = $data['media'][0];
			// var_dump($page);
		}
	}else{
		$page = $data;
		$form_kind = "form_edit_medias";
	}
	
			

	$db = new Database();
	$form = new Form(); 
	
	$getMeta = $db->query("SELECT * FROM tbl_pictures
	where categ like CONCAT('%-',:categ_list, '-%')");
	$db->bind($getMeta, ":categ_list", (isset($add) ? 1 : (isset($page->id) ? $page->id : ""))); 
	 
	$i = 0;
	if($db->rowCount($getMeta) > 0){
		while($meta = $db->fetch($getMeta, "array")){ 
			$getKey = array_keys($meta);
			foreach($getKey as $key){ 
				if($key !== "id"){
					$page->{$key} = (isset($add) ? "" : $meta[$key]); 
				}
				$i++;
			}
		}
	}else{
		$fakeKey = (getEmptyValue("tbl_pictures"));   
		 
		$getKey = array_keys(object_to_array($fakeKey));
		// var_dump($getKey);
		foreach($getKey as $key){ 
			if($key !== "id"){
				$page->{$key} = ""; 
			}
			$i++; 
		}
	} 
	// var_dump($page);
?>

<div id="content" data-id="<?= $page->id ?>" url="<?= URLROOT.getUrlLang(28) ?>" class=" medias_website medias categories">
	<div class="card header-title  text-center">
		<h1><?= $lang['menu_media'] ?></h1>
	</div>
	<a class="btn btn-success go_back" href="<?= URLROOT.getUrlLang(16) ?>" ><?= $lang['back'] ?></a>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive page-categories">
				<h2><?= $page->name ?></h2> 
				<ul class="nav nav-pills nav-pills-warning nav-pills-icons justify-content-center" role="tablist">
					<li class="nav-item active">
						<a class="nav-link active" data-toggle="tab" href="#link1" role="tablist">
							<i class="fad fa-images"></i> Toutes les images
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link step2 hide" data-toggle="tab" href="#link2" role="tablist">
							<i class="fad fa-image"></i> Modification d'une image
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link step3" data-toggle="tab" href="#link3" role="tablist">
							<i class="fad fa-image"></i> Ajout d'une image
						</a>
					</li>
				</ul>
				<div class="tab-content tab-space tab-subcategories ">
					<div id="link1" class="tab-pane active">
						<form action="" class=" " method="post" page-id="<?= $page->id ?>"  enctype="multipart/form-data">
			
							<div id="sortable" class="grid_12"> 
								<?php
									// var_dump($page);
									$imgs = $db->query("SELECT * FROM tbl_pictures
									where categ like CONCAT('%-',:categ_list, '-%') ORDER BY `order` ASC");
									$db->bind($imgs, ":categ_list", $page->id);
									$db->execute($imgs);
									$i=0;
									while($image = $db->fetch($imgs)){
										echo '<div id="image_'.$image->id.'" data-id="'.$image->id.'" class="delta edit_img ui-state-default grid_3"><img src="'.URLGIMG.$image->picture.'" /></div>';
										$i++;
									}
								?>
							</div>
							<div class="col clear">
								<input id="submit" type="submit" value="<?= $lang[(isset($add) ? 'add' : 'pages_th_save')] ?>" class="btn btn-success btn-block">
							</div>
						</form>		 
					</div>
					<div id="link2" class="tab-pane "></div> 
					<div id="link3" class="tab-pane ">
						<form class="multiple_file_upload" action="" method="post" enctype="multipart/form-data">
							Importe tes images ici
							<input type="file" name="files[]" multiple >
							<input type="hidden" name="categ_id" value="<?= $page->id ?>" >
							<input type="submit" name="submit" value="UPLOAD">
						</form>
					</div> 
				
				</div>
			</div>
		</div>
	</div>
</div>
<style>
#sortable {     list-style-type: none;
    margin: 0;
    padding: 100px 0;
    width: 100%;
    margin-bottom: 5%; 
    min-height: 500px; }
						#sortable div {    margin: 3px;
    padding: 0.4em;
    text-align: center;
    padding-left: 1.5em;
    font-size: 1.4em;
    float: left;
    position: relative;
    height: auto; }
	
	#sortable div:hover{
		cursor:pointer;
	}
						#sortable div img {      height: 150px; 
    margin: 0 auto;
    position: relative;
}
</style>  
<?php   
	if(isset($_GET['url']) && ($_GET['url'] == "medias" || $_GET['url'] == "medias" ||
	$_GET['url'] == "medias/media" || $_GET['url'] == "medias/media" )){
		//Require headers and lang
		require_once APPROOT . "/views/inc/footer.php"; 
	}
?>

<script src="<?= URLROOT ?>/js/ui.js"></script>
 
<script>
tess.initialize();
</script>