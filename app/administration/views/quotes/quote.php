<?php
	if(((is_array($data) && empty($data)) || (is_object($data) && !property_exists($data, "id")))){
		redirect("pages/index");
	}

	require_once APPROOT."/config/lang.php";
	include_once APPROOT."/helpers/function.php";

	//Initialize $page
	$page_id = 32; //quotes form page

	if(!is_object($data)){
		if(isset($data[1])){
			$page = $data[1];
		}else{
			$page = $data;
		}
		if(isset($page['load_add_quotes'])){
			$add = true;
			$form_kind = "form_add_quotes";
		}else{
			$form_kind = "form_edit_quotes";
		}
	}else{
		$page = $data;
		$add = false;
		$form_kind = "form_edit_quotes";
	}

	//Initialize classes
	$db = new Database();
	$form = new Form();
	// var_dump($page);

?>
<div id="content" class="pages category quotes">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_quotes'] ?></h1>
	</div>
	<a class="btn btn-success go_back" href="<?= URLROOT.getUrlLang(17) ?>" ><?= $lang['back'] ?></a>
	<?= (isset($add) ? $lang['menu_quotes'] : $page->{'title'.$l}) ?></br>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="<?= $form_kind ?>" page-id="<?= (!$add ? $page->id : $page_id) ?>"  enctype="multipart/form-data">
					<?php //input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="",$note_position="", $note="", $attributes) ?>
					<?php
						$info = unserialize($page->quotes); 
						// var_dump($info);
						foreach($info as $label => $value){
							$name = $label;
							$label = str_replace("_", " ", $label);
							if(strpos($label,"|") !== false){
								$label = explode('|',$label)[1];
							}else{
								
							}
							echo '<div class="grid_6 grid_1024_12">'.$form->input($name,'textarea',$label,false,$value).'</div>';
						}
					?>
					<?= '<div class="clear"></div>'; ?> 
					<?= (!$add ? '<input type="hidden" value="'.$page->id.'" name="inputs_id" />' : '') ?>
					<?= '<div class="clear"></div><div class="grid_12 grid_1600_12"><input type="submit" class="btn btn-success" value="'.(!$add ? $lang['edit'] : $lang['add']).'" name="submit" /></div>'; ?>
				</form>
			</div>
		</div>
	</div>
</div>
