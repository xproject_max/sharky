<?php
	require_once APPROOT . "/views/inc/header.php";

	$db = new Database();
?>
<div id="content" url="<?= URLROOT."".getUrlLang(17) ?>" class="categories quotes">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_quotes'] ?></h1>
	</div> 
	</br>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive"> 
					<table class="table" style=" margin: 0 auto;">
						<thead>
							<tr>
								<th style="padding-right:15px;">Nom de compagnie</th>
								<th style="padding-right:15px;">Email</th> 
								<th style="padding-right:15px;">Téléphone</th> 
								<th style="padding-right:15px;">status</th> 
								<th></th>
								<th class="text-right" style="padding-right:15px;"><?= $lang['pages_th_actions'] ?></th>
							</tr>
						</thead>
						<tbody>
						<?php
							try{ 
								foreach($data['getquotes'] as $key => $quotes){
									$info = unserialize($quotes->quotes); 
									$i =0;
									echo '
										<tr> 
											<td>
												<p>'.array_values($info)[11].'</p>
											</td> 
											<td>
												<p>'.array_values($info)[12].'</p>
											</td> 
											<td>
												<p>'.array_values($info)[13].'</p>
											</td> 
											<td>
												<p>'.$quotes->status.'</p>
											</td> 
											<td>
											</td>
											<td class="td-actions text-right" style="line-height:1.3; width:43px;">
												<a title="Modifier" id="' . $quotes->id .'" class="btn btn-warning edit_quotes" href="#">
												<i class="fas fa-pen-square"></i>
												</a>
												<a id="'.$quotes->id.'" title="Supprimer" class="btn btn-danger delete-quotes" >
												<i class="fas fa-times-square"></i>
												</a>
											</td>
										</tr>
									';
								}
							}catch(PDOException $e){
								return false;
							}
						?>
						</tbody>
					</table></br>  
			</div>
		</div>
	</div>
</div>
<?php
require_once APPROOT . "/views/inc/footer.php";
?>
