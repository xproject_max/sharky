<?php
require_once APPROOT . "/views/inc/header.php";
$menu_array = $data['menu_array'];  
?>
<div id="content" url="<?= URLROOT."".getUrlLang(23) ?>" class="contests categories">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_contests'] ?></h1>
	</div>
	<input type="button" class="btn btn-success new_contest" value="<?= $lang['add']; ?>"   />
	<?= $lang['menu_contests'] ?>
	</br>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="update_status_contests" name="update_status">
					<table class="table" style=" margin: 0 auto;    ">
						<thead>
							<tr>
								<th style="padding-right:15px;"><?= $lang['pages_th_order'] ?></th>
								<th><?= $lang['pages_th_img'] ?></th>
								<th><?= $lang['title'] ?></th>
								<th></th> 
								<th>Date</th> 
								<th class="td-actions text-right"><?= $lang['pages_th_online'] ?></th>
								<th class="text-right" style="padding-right:15px;"><?= $lang['pages_th_actions'] ?></th> 
							</tr>
						</thead>
						<tbody  >
							<?= generate_contests_menu(0, $menu_array); ?>
						</tbody>
					</table>
					</br>
					<input type="submit" class="btn btn-info" style="color:white !important;background: black !important;margin-top:15px" value="<?= $lang['pages_th_save'] ?>" name="B1">
				</form>
			</div>
		</div>
	</div>
</div>
<?php
require_once APPROOT . "/views/inc/footer.php";
?>
