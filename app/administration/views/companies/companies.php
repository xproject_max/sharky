<?php
require_once APPROOT . "/views/inc/header.php";
// $menu_array = $data['menu_array'];  
	$add = false;
	$db = new Database();
	$form = new Form();
	$page = $_POST;
	
	$select_role = "";
	$query_roles = $db->query("SELECT * FROM tbl_roles ORDER BY rights ASC");
	$db->execute($query_roles);
	while($roles = $db->fetch($query_roles)){
		$select_role .= "<option value='".$roles->id."' >".$roles->role."</option>";
	}
?>
<div id="content" url="<?= URLROOT."".getUrlLang(12) ?>" class="companies companies_website categories">
	<div class="card header-title text-center">
		<h1><?= $lang['company_accounts'] ?></h1>
	</div>
		<div class="grid_12 grid_1024_12">
		<form method="post" target="_blank" action="<?= URLROOT ?>/ajax.php?export_companies=true" id="form_companies_export"  >
			<p><?= $lang['company_accounts'] ?></p>
			<?php //input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="",$note_position="", $note="", $attributes) ?>
			<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("start_date", "text", $lang['date_start'], false,"","","datetimepicker").'</div>'; ?>
			<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("end_date", "text", $lang['date_end'], false,"","","datetimepicker").'</div>'; ?>
			<?= '<div class="clear"></div>'; ?>
			<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("company_name", "text", $lang['company_name'], false).'</div>'; ?>
			<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("contact_name", "text", $lang['contact_name'], false).'</div>'; ?>
			<?= '<div class="clear"></div>'; ?>
			<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("id", "text", "id", false).'</div>'; ?>
			<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("email", "text", $lang['email'], false).'</div>'; ?>
			<?= '<div class="clear"></div>'; ?>
			<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("role", "select", "role", false, "","","",$select_role).'</div>'; ?> 
			<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("username", "text", $lang['username'], false,"","","").'</div>'; ?>
			<?= '<div class="clear"></div>'; ?>
			<?= '<div class="alpha omega grid_6 grid_1600_12">'.$form->input("solde", "toggle", "Order by solde", false,"","","").'</div>'; ?>
			<?= '<div class="clear"></div>'; ?>
			<input type="submit" class="btn btn-success" value="Export companies"></input>
			<input type="button" class="btn btn-success filter_companies" value="Search"></input>
		</form>
	</div>
	<div class="clear"></div>  
	<div class="mtop">
	<input type="button" class="btn btn-success new_companies" value="<?= $lang['add']; ?>"   /> 
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="update_status_companies" name="update_status">
					<table class="table" style=" margin: 0 auto;    ">
						<thead>
							<tr>
								<th style="padding-right:15px;">ID</th>
								<th><?= $lang['pages_th_img'] ?></th>
								<th>Nom compagnie</th>  
								<th><?= $lang['username'] ?></th> 
								<th>rôle</th> 
								<th>Courriel</th>  
								<th>Date</th> 
								<th>Solde</th> 
								<th class="td-actions "><?= $lang['pages_th_online'] ?></th>
								<th class="" style="padding-right:15px;"><?= $lang['pages_th_actions'] ?></th> 
							</tr>
						</thead>
						<tbody class="result_seek">
						<?php
							try{
								$query_orders = $db->query("
									SELECT t.*,
									meta.*,
									m.role,
									m.id as ids,
									t.id as id
									FROM tbl_companies as t
									LEFT JOIN
									tbl_companies_meta meta
										ON t.fk_companies_meta = meta.id
									LEFT JOIN tbl_roles m
										ON t.fk_role = m.rights
									order by t.id desc
								");
								$result = $db->execute($query_orders);
								$counted = $db->rowCount($query_orders);
								while ($user = $query_orders->fetch($result)) {

									echo '
										<tr mid="'.$user->id.'" url="'.URLROOT."".getUrlLang(27).'?companyid='.$user->id.'" urlinv="'.URLROOT."".getUrlLang(27, "en").'?companyid='.$user->id.'">
											<td>
												<input class="text-center" type="hidden" name="id[]"
												style="text-align:center;width:15px !important;"
												value="'.$user->id.'">
												<p>
													'.$user->id.'
												</p>
											</td>
											<td>
												<div style="    height: 40px;
												width: 40px;
												display: table;
												background: rgb(0, 0, 0);
												overflow: hidden;
												border-radius: 50%;
												/* border: 1px solid rgba(0,0,0,0.2); */
												margin: 0 auto;">
													<div style="display:table-cell;vertical-align:middle">
														<img src="'.URLIMG.$user->profile_image.'" width="40px"  />
													</div>
												</div>
											</td> 
											<td>
												<p data-tooltip="'.$user->company_name.'">'.$user->company_name.'</p>
											</td> 
											<td>
												<p data-tooltip="'.$user->username.'">'.$user->username.'</p>
											</td>
											<td>
												<p data-tooltip="'.$user->role.'">'.$user->role.'</p>
											</td>
											<td>
												<p data-tooltip="'.$user->email.'">'.$user->email.'</p>
											</td>
											<td>
												<p data-tooltip="'.$user->date_company_created.'">'.$user->date_company_created.'</p>
											</td>
											<td>
												<p data-tooltip="'.$user->solde.'">'.$user->solde.'</p>
											</td>
											<td>												
												<div class="togglebutton">
													<label>
														<input name="online[]['.$user->id.']" value="0" type="hidden" />
														<input name="online[]['.$user->id.']" value="1" type="checkbox" '.($user->online == 1 ? 'checked=""' : '').' />
														<span class="toggle"></span>
													</label>
												</div>
											</td>
											<td class="td-actions ">
										';

										echo '<a class="btn btn-warning" data-toggle="tooltip" data-placement="top" 
												href="index.php?cp=page&mod=addmod
												&id=' . $user->id . '"><i class="fas fa-pen-square"></i></a> ';
										echo '<a id="'.$user->id.'"  class="btn btn-danger delete-website-companies" data-tooltip="Supprimer" ><i class="fas fa-times-square"></i></a> ';
										
										echo'
											</td>
										</tr>
									';
								}
							}catch(PDOException $e){
								return false;
							}
						?>
						
						</tbody>
					</table>
					</br>
					<input type="submit" class="btn btn-info" style="color:white !important;background: black !important;margin-top:15px" value="<?= $lang['pages_th_save'] ?>" name="B1">
				</form>
			</div>
		</div>
	</div>
</div>
<?php
require_once APPROOT . "/views/inc/footer.php";
?>
