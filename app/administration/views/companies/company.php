<?php
	if(((is_array($data) && empty($data)) || (is_object($data) && !property_exists($data, "id")))){
		redirect("pages/index");
	}
	
	require_once APPROOT . "/models/Transaction.php";
	//Initialize $page  
	// die();
	if(!is_object($data)){
		if(isset($data[0])){
			$page = $data[0];
		}else{
			$page = $data;
		}
		if(is_array($page)){
			if(isset($page['load_add_company'])){
				$add = true; 
				if(isset($page['getModule'])){
					$type = $page['getModule'];
				}
				$page = $page['page'];
				$form_kind = "form_add_companies";
			}else{
				$form_kind = "form_edit_companies";
			}
		}else{
		    //Require headers and lang
			$page = $data[0]; 
			$form_kind = "form_edit_companies";
			
		}
		if(isset($data['company'])){ 
		    //Require headers and lang
			// var_dump($data);
			if(empty($data['company'])){
				redirect(($l == "en" ? "companies" : "entreprises"));
			}
		    require_once APPROOT . "/views/inc/header.php";
			$page = $data['company'][0];
			// var_dump($page);
		}
	}else{
		$page = $data;
		$form_kind = "form_edit_companies";
	}
	
			

	$db = new Database();
	$form = new Form(); 
	
	$getMeta = $db->query("SELECT * FROM tbl_companies_meta WHERE id = :fk_companies_meta");
	$db->bind($getMeta, ":fk_companies_meta",(isset($add) ? 1 : (isset($page->fk_companies_meta) ? $page->fk_companies_meta : $page->id))); 
	$db->execute($getMeta);
	// var_dump($page);
	// var_dump($getMeta);
	$i = 0;
	if($db->rowCount($getMeta) > 0){
		while($meta = $db->fetch($getMeta, "array")){ 
			$getKey = array_keys($meta);
			foreach($getKey as $key){ 
				if($key !== "id"){
					$page->{$key} = (isset($add) ? "" : $meta[$key]); 
				}
				$i++;
			}
		}
	}else{
		$fakeKey = object_to_array(getEmptyValue("tbl_companies_meta"));   
		 
		$getKey = array_keys($fakeKey);
		foreach($getKey as $key){ 
			if($key !== "id"){
				$page->{$key} = ""; 
			}
			$i++; 
		}
	}
	$rights = $db->query("SELECT * FROM tbl_roles ORDER BY `rights` ASC");
	$options_right = "";
	$db->execute($rights);
	while($right = $db->fetch($rights)){
		$options_right .= '<option '.($page->fk_role == $right->id ? 'selected="selected"' :'' ).' value="'.$right->id.'">'.$right->role.'</option>';
	} 
	
	$permissions_array="";
	$res = $db->query("SELECT * FROM tess_controller order by id asc");
	$db->execute($res);
 	while($data = $db->fetch($res,'array')){
        $tablo[] = $data;
    }

    $nbcol = 3;
	$r= "";
    $permissions_array .= '<table class="center_check">';

    $nb = count($tablo);
    for ($i = 0; $i < $nb; $i++) {

        $valeur1 = $tablo[$i]['id'];
        $valeur2 = $tablo[$i]['title'];
        if ($i % $nbcol == 0)
            $permissions_array .= '<tr>';

        if (isset($valeur1) && !is_null($valeur1) && !is_object($valeur1)) { 
            $check = $db->query("SELECT mg.* FROM tess_controller mg RIGHT JOIN tbl_companies m ON m.permissions = mg.id where m.permissions like CONCAT('%-',:valeur1, '-%') and m.id = :id ");
           	$db->bind($check,":valeur1", $valeur1);
           	$db->bind($check,":id", $page->id);
			$db->execute($check);
			$res = $db->rowCount($check);
            if ($res > 0) {
                $r = "checked";
            } else {
             	$r= "";
            }
			$permissions_array .= '
				<td >
					<div class="form-check">
					  <label  >
						<input class="form-check-input" type="checkbox" name="inputs[]" value="' . $valeur1 .
				'" ' . $r . '>' . $valeur2 . '
						<span class="form-check-sign">
						  <span class="check"></span>
						</span>
					  </label>
					</div>
				</td>';
        }
	
        

        if ($i % $nbcol == ($nbcol - 1))
            $permissions_array .= '</tr>';

    }

    $permissions_array .= '</table>'; 
	
	/* Fetch allowed countries */
	$country_str = "";
	if(!empty(fetch_countries())){
		foreach(fetch_countries() as $country){
			
			$country_str .= "<option selected='selected' value='".$country."'>".$country."</option>";
		}
	}
	/* Fetch allowed states */
	$state_str = "";
	if(!empty(fetch_states())){
		foreach(fetch_states((isset($page->country) ? $page->country : "")) as $state){
			
			$state_str .= "<option ".($state == "Québec" ? "selected='selected'" : "")." value='".$state."'>".$state."</option>";
		}
	}
	/* Fetch allowed region */
	$region_str = "";
	if(!empty(fetch_regions())){
		foreach(fetch_regions((isset($page->state) ? $page->state : "")) as $region){
			
			$region_str .= "<option ".($page->region == $region ? "selected='selected'" : "")." value='".$region."'>".$region."</option>";
		}
	}
	/* Method payments */
	$getMethodPayment = getMethodPayments();
	$listPayments = "";
	$listPayments .= "<option value='credit'>Crédit</option>";
	foreach($getMethodPayment as $key => $value){
		$listPayments .= '	<option value="'.$key.'">'.$value[$l].'</option>';
	}
?>

<div id="content" class="profile companies companies_website  categories">
	<div class="card header-title profile text-center">
		<h1><?= $lang['menu_profile'] ?></h1>
	</div>
	<a class="btn btn-success go_back" href="<?= URLROOT.getUrlLang(12) ?>" ><?= $lang['back'] ?></a>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive page-categories">
				<form action="" class="tab-content tab-space tab-subcategories" method="post" id="<?= $form_kind ?>" page-id="<?= $page->id ?>"  enctype="multipart/form-data">
					<ul class="nav nav-pills nav-pills-warning nav-pills-icons justify-content-center" role="tablist">
						<li class="nav-item active">
							<a class="nav-link active" data-toggle="tab" href="#link1" role="tablist">
								<i class="fad fa-user-cog"></i> <?= $lang['account_informations']; ?>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link step2" data-toggle="tab" href="#link2" role="tablist">
								<i class="fad fa-smile-beam"></i> <?= $lang['personal_informations']; ?>
							</a>
						</li>
					<?php
					if(!isset($add)){
					?>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#link3" role="tablist">
								<i class="fad fa-search-dollar"></i> Transactions
							</a>
						</li> 
					<?php
					}
					?>
					</ul>
					<?= "<div class='delta grid_6 grid_1600_12 middle_header'>".$form->input("profile_image", "Upload Image", "", false, (isset($page->profile_image) ? $page->profile_image : ""), "","",$page->id, "bas", "", "")."</div><div class='mbot'></div>" ?>
					<div class="tab-pane content-account active" id="link1">
						<h2><?= $lang['account_informations']; ?></h2>
						<?php 													        /* input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="",$note_position="", $note="", $attributes="") */ ?>
						<?= "<div class='alpha grid_6 grid_1600_12'>".$form->input("username", "text", $lang['username'], false, $page->username, "","","", $page->id, "", "")."</div>" ?>
						<?= "<div class='omega grid_6 grid_1600_12'>".$form->input("password", "password", $lang['password'], false, $page->password, "","","","","", "")."</div>" ?>
						<div class="clear clear_1024"></div>
						<?= "<div class='alpha grid_6 grid_1600_12'>".$form->input("passe2", "password", $lang['retype_password'], false, "", "","","",  "titre", "Laisser vide pour ne rien modifier", "")."</div>" ?>
						<?= "<div class='omega grid_6 grid_1600_12'>".$form->input("email", "text", $lang['email'], false, $page->email, "","","",  "", "", (!isset($add) ? "disabled" : ""))."</div>" ?>
						<div class='clear'></div>
						<?= "<div class='alpha grid_6 grid_1600_12'>".$form->input("fk_role", "select", $lang['roles'], false, "", "","",$options_right,  "", "", "")."</div>" ?>
						<?= "<div class='omega grid_6 grid_1600_12'>".$form->input("online", "toggle", $lang['online'], false, "", "","","",  "", "", ($page->online == 1  ? "checked" : ""))."</div>" ?>
						<div class='clear'></div>
						<?= "<div class='delta grid_12 grid_1600_12'>".$form->input("inputs", "Checkbox", $lang['permissions'], false, "", "","",$permissions_array,  "", "", "")."</div>" ?>
						<div class='clear'></div>
						<?php
							if(!isset($add)){
						?>
						<div class="col">
							<input type="submit" value="<?= $lang[(isset($add) ? 'add' : 'pages_th_save')] ?>" class="btn btn-success btn-block">
						</div>
						<?php 
							}else{ 
						?>
						<div class="col">
							<input type="button" value="<?= $lang['next_step'] ?>" class="btn btn-success btn-block btn-next">
						</div>
						<?php 
							}
						?>
					</div>
					<div class="tab-pane  content-personal"  id="link2">
						<h2><?= $lang['personal_informations']; ?></h2><div class='mbot'></div> 													        
						<?= "<div class='alpha grid_6 grid_1600_12'>".$form->input("company_name", "text", $lang['company_name'], false, $page->company_name, "","","", $page->id, "", "")."</div>" ?>
						<?= "<div class='omega grid_6 grid_1600_12'>".$form->input("contact_name", "text", $lang['contact_name'], false, $page->contact_name, "","","","","", "")."</div>" ?>
						<div class="clear clear_1024"></div>
						<?= "<div class='alpha grid_6 grid_1600_12'>".$form->input("country", "select", $lang['country'], false, "", "","",$country_str,"", "", "disabled")."</div>" ?>
						<?= "<div class='omega grid_6 grid_1600_12'>".$form->input("state", "select", $lang['state'], false, "", "","",$state_str,"", "", "disabled")."</div>" ?>
						<div class='clear'></div>
						<?= "<div class='alpha grid_6 grid_1600_12'>".$form->input("region", "select", $lang['region'], false, "", "","",$region_str, "", "", "")."</div>" ?>
						<?= "<div class='omega grid_6 grid_1600_12'>".$form->input("city", "text", $lang['city'], false, $page->city, "","","","", "", "")."</div>" ?>
						<div class='clear'></div>
						<?= "<div class='alpha grid_6 grid_1600_12'>".$form->input("address", "text", $lang['address'], false,$page->address, "","","","", "", "")."</div>" ?>
						<?= "<div class='omega grid_6 grid_1600_12'>".$form->input("zipcode", "text", $lang['zipcode'], false, $page->zipcode, "","","","", "", "")."</div>" ?>
						<div class='clear'></div>
						<?= "<div class='alpha grid_6 grid_1600_12'>".$form->input("phone", "text", $lang['phone'], false, $page->phone, "","","","", "", "")."</div>" ?>
						<div class='omega clear'></div>				 
						<div class="col">
							<input type="submit" value="<?= $lang[(isset($add) ? 'add' : 'pages_th_save')] ?>" class="btn btn-success btn-block">
						</div> 
					</div>
					<div id="link3"></div>
				</form>				
				<?php
					if(!isset($add)){
				?>
				<div class="form_transactions">
					<div class="note solde">Solde actuel: <?= number_format($page->solde,2).'$'; ?></div>
					<form method="post" class="add_credit_company" >					
						<div class="note solde">Modification au solde manuelle</div>
						<p class="message"></p>
						<?= "<div class='alpha grid_6 grid_1600_12'>".$form->input("credit", "text", "Crédit/Débit", false, "", "","","", $page->id, "", "")."</div>" ?>
						<?= "<div class='omega grid_6 grid_1600_12'>".$form->input("payment_method", "select", "Moyen de paiement", false, "", "","",$listPayments, "", "", "")."</div>" ?>
						<div class='clear'></div>
						<?= "<div class='alpha grid_6 grid_1600_12'>".$form->input("description_credit", "text", "Note", false, "", "","","", "", "", "")."</div>" ?>
						<?= "<div class='omega grid_6 grid_1600_12'>".$form->input("description_credit_en", "text", "Note En", false, "", "","","","","", "")."</div>" ?>
						<div class='clear'></div>
						<input type="hidden" value="<?= $page->id ?>" name="id" /> 
						<input type="hidden" value="<?= URLROOT.getUrlLang(27)."?companyid=".$page->id.""; ?>" id="url_form_credit" name="url" /> 
						<input type="submit" value="<?= $lang['add'] ?>" name="add_credit" class="btn btn-success btn-block" />
					</form>
				</div>
				
				<div class="popup_payments_history_container">
					<div class="popup_payments_history">
					</div>
				</div>
				<?php
						$transaction = new Transaction(["id"=>$page->id, "table"=>"transactions"], "Company");
						
						echo $transaction->TransactionsBoard("Company");
						
						echo $transaction->formPayUnpaidOrders("Company");
					}
				?>
			</div>
		</div>
	</div>
</div>
<script>
tess.initialize();
</script>
<?php   
	if(isset($_GET['url']) && ($_GET['url'] == "entreprises" || $_GET['url'] == "companies" ||
	$_GET['url'] == "entreprises/profil" || $_GET['url'] == "companies/profile" )){
		//Require headers and lang
		require_once APPROOT . "/views/inc/footer.php"; 
	}
?>
