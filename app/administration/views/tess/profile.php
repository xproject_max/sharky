<?php
	if(((is_array($data) && empty($data)) || (is_object($data) && !property_exists($data, "id")))){
		redirect("pages/index");
	}
	//Initialize $page 
	if(!is_object($data)){
		if(isset($data[0])){
			$page = $data[0];
		}else{
			$page = $data;
		}
		if(isset($page['load_add_tess_user'])){
			$add = true; 
			if(isset($page['getModule'])){
				$type = $page['getModule'];
			}
			$page = $page['page'];
			$form_kind = "form_add_tess_user";
		}else{
			$form_kind = "form_edit_tess_user";
		}
		if(isset($data['user'])){ 
		    //Require headers and lang
		    require_once APPROOT . "/views/inc/header.php";
			$page = $data['user'];
			
		}
	}else{
		$page = $data;
		$form_kind = "form_edit_tess_user";
	} 

	$db = new Database();
	$form = new Form();
 
	$rights = $db->query("SELECT * FROM tess_members_roles WHERE id >= :currentRight ORDER BY `right` ASC");
	$options_right = "";
	$db->bind($rights, ":currentRight", $_SESSION['status']);
	$db->execute($rights);
	while($right = $db->fetch($rights)){
		$options_right .= '<option '.($page->status == $right->name ? 'selected="selected"' :'' ).' value="'.$right->id.'">'.$right->name.'</option>';
	} 
	
	$permissions_array="";
	$res = $db->query("SELECT * FROM tess_controller order by id asc");
	$db->execute($res);
 	while($data = $db->fetch($res,'array')){
        $tablo[] = $data;
    }

    $nbcol = 3;
	$r= "";
    $permissions_array .= '<table class="center_check">';

    $nb = count($tablo);
    for ($i = 0; $i < $nb; $i++) {

        $valeur1 = $tablo[$i]['id'];
        $valeur2 = $tablo[$i]['title'];
        if ($i % $nbcol == 0)
            $permissions_array .= '<tr>';

        if (isset($valeur1) && !is_null($valeur1) && !is_object($valeur1)) { 
            $check = $db->query("SELECT mg.* FROM tess_controller mg RIGHT JOIN tess_members m ON m.permissions = mg.id where m.permissions like CONCAT('%-',:valeur1, '-%') and m.id = :id ");
           	$db->bind($check,":valeur1", $valeur1);
           	$db->bind($check,":id", $page->id);
			$db->execute($check);
			$res = $db->rowCount($check);
            if ($res > 0) {
                $r = "checked";
            } else {
             	$r= "";
            }
			$permissions_array .= '
				<td >
					<div class="form-check">
					  <label  >
						<input class="form-check-input" type="checkbox" name="inputs[]" value="' . $valeur1 .
				'" ' . $r . '>' . $valeur2 . '
						<span class="form-check-sign">
						  <span class="check"></span>
						</span>
					  </label>
					</div>
				</td>';
        }

        

        if ($i % $nbcol == ($nbcol - 1))
            $permissions_array .= '</tr>';

    }

    $permissions_array .= '</table>'; 
?>

<div id="content" class="profile tess_users  categories">
	<div class="card header-title profile text-center">
		<h1><?= $lang['menu_profile'] ?></h1>
	</div>
	<a class="btn btn-success go_back" href="<?= URLROOT.getUrlLang(13) ?>" ><?= $lang['back'] ?></a>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="<?= $form_kind ?>" page-id="<?= $page->id ?>"  enctype="multipart/form-data">
					<?php 													        /* input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="",$note_position="", $note="", $attributes="") */ ?>
					<?= "<div class='delta grid_6 grid_1600_12 middle_header'>".$form->input("profile_image", "Upload Image", "", false, ($_SESSION['profile_image'] ? $_SESSION['profile_image'] : ''.PROTOCOL.'://'.DOMAINNAME.'/images/logo/logo.png'), "","",$page->id, "bas", "", "")."</div><div class='mbot'></div>" ?>
					<?= "<div class='alpha grid_6 grid_1600_12'>".$form->input("pseudo", "text", $lang['username'], false, $page->pseudo, "","","", $page->id, "", "")."</div>" ?>
					<?= "<div class='omega grid_6 grid_1600_12'>".$form->input("pass", "password", $lang['password'], false, "", "","","","","", "")."</div>" ?>
					<div class="clear clear_1024"></div>
					<?= "<div class='alpha grid_6 grid_1600_12'>".$form->input("passe2", "password", "Retype password", false, "", "","","",  "titre", "Laisser vide pour ne rien modifier", "")."</div>" ?>
					<?= "<div class='alpha grid_6 grid_1600_12'>".$form->input("actualpass", "password", "Actual password", false, "", "","","",  "titre", "Laisser vide pour ne rien modifier", "")."</div>" ?>
					<?= "<div class='omega grid_6 grid_1600_12'>".$form->input("email", "text", $lang['email'], false, $page->email, "","","",  "", "", "")."</div>" ?>
					<div class='clear'></div>
					<?= "<div class='alpha grid_6 grid_1600_12'>".$form->input("status", "select", "Rôle", false, "", "","",$options_right,  "", "", "")."</div>" ?>
					<?= "<div class='omega grid_6 grid_1600_12'>".$form->input("inputs", "Checkbox", "Permission(s)", false, "", "","",$permissions_array,  "", "", "")."</div>" ?>
					<div class='clear'></div>
					<div class="col">
						<input type="submit" value="<?= $lang[(isset($add) ? 'add' : 'pages_th_save')] ?>" class="btn btn-success btn-block">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php   
	if(isset($_GET['url']) && $_GET['url'] == "tess/utilisateurs/profil" || $_GET['url'] == "tess/users/profile" ){
		//Require headers and lang
		require_once APPROOT . "/views/inc/footer.php"; 
	}
?>
