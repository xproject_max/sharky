<?php
   require_once APPROOT."/config/lang.php";
   include_once APPROOT."/helpers/function.php";
    /*
     * Login page
     */
	if(isset($_SESSION['user_id']))
		redirect('pages/index', $language);
    require_once dirname(dirname(__DIR__))."/libraries/Form.php";

    //Instantiate Form
    $form = new Form();
    $data = isset($data) ? $data : [];
    $page = isset($data['page']) ? $data['page'] : [];

    //Require headers
    require_once APPROOT ."/config/lang.php";
    require_once APPROOT . "/views/inc/header.php";

	echo'
	<div id="content" class="login '.(!isset($_SESSION['user_id']) ? 'isLoginPage' : '').'">
		<div class="background-login">
		</div>
		<div class="background-slider-login">
			<div class="slider-content-login">
				<form action="'.URLROOT.'/ajax.php" url="'.URLROOT.getUrlLang(1).'" class="connect_form" method="post">
					<div class="card">

						<div class="card-body">
							<div class="table-cell img-center"><img src="'.PROTOCOL.'://'.DOMAINNAME.'/images/logo/logo.png" width="250px" ></div>
							<span style="color:#dc3545">'.(isset($data['secure_post']['login']) ? $data['secure_post']['login']['message_error'] : "").'</span>
							<div>
								'.$form->input("username","text",$lang['username'],true,(isset($data['secure_post']) ? $data['secure_post']['username']['value'] : ""), (isset($data['secure_post']) ? $data['secure_post']['username']['message_error'] : ""), " ". (isset($data['secure_post']) ? $data['secure_post']['username']['message_error'] !== "" ? "is-invalid": "is-valid" : "")."").'
								'.$form->input("password","password", $lang['password'],true,(isset($data['secure_post']) ? $data['secure_post']['password']['value'] : ""), (isset($data['secure_post']) ? $data['secure_post']['password']['message_error'] : ""), " ". (isset($data['secure_post']) ? $data['secure_post']['password']['message_error'] !== "" ? "is-invalid": "" : "")."").'
								<input type="hidden" name="login_form" value="1">
								<label class="col-md-3"></label>
							</div>
						</div>
						<div class="card-footer ">
							<div class="row">
								<div class="col">
									<input type="submit" value="'.$lang['menu_login'].'" class="btn btn-success btn-block">
								</div>
							</div>
							  '.(isset($data['secure_post']['success']) ? "<div class='success_box'></div>" : "").'
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	';
    //Require footer
    require_once APPROOT . "/views/inc/footer.php";
?>
