<?php
require_once APPROOT . "/views/inc/header.php";
$menu_array = $data['menu_array'];  
?>
<div id="content" url="<?= URLROOT."".getUrlLang(13) ?>" class="tess_users categories">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_tess_users'] ?></h1>
	</div>
	<input type="button" class="btn btn-success new_tess_user" value="<?= $lang['add']; ?>"   /> 
	<?= $lang['menu_tess_users'] ?>
	</br>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="update_status_tess_users" name="update_status">
					<table class="table" style=" margin: 0 auto;    ">
						<thead>
							<tr>
								<th style="padding-right:15px;">ID</th>
								<th><?= $lang['pages_th_img'] ?></th>
								<th><?= $lang['username'] ?></th> 
								<th>rôle</th> 
								<th>Courriel</th>  
								<th>Date</th> 
								<th class="td-actions "><?= $lang['pages_th_online'] ?></th>
								<th class="" style="padding-right:15px;"><?= $lang['pages_th_actions'] ?></th> 
							</tr>
						</thead>
						<tbody  > 
							<?= generate_tess_users_menu(0, $menu_array); ?>
						</tbody>
					</table>
					</br>
					<input type="submit" class="btn btn-info" style="color:white !important;background: black !important;margin-top:15px" value="<?= $lang['pages_th_save'] ?>" name="B1">
				</form>
			</div>
		</div>
	</div>
</div>
<?php
require_once APPROOT . "/views/inc/footer.php";
?>
