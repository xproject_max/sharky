<?PHP
	//requires model newsletter


	require_once APPROOT."/config/lang.php";
	//Initialize $page
	if(!is_object($data)){
		if(isset($data[0])){
			$page = $data[0];
		}else{
			$page = $data;
		}
		if(is_array($page)){
			if(isset($page['load_add_newsletters_templates'])){
				$add = true;
				if(isset($page['getModule'])){
					$type = $page['getModule'];
				}
				$page = $page['page'];
				$form_kind = "form_add_newsletters_templates";
			}else{
				$form_kind = "form_edit_newsletters_templates";
			}
		}else{
			//Require headers and lang
			$page = $data[0];
			$form_kind = "form_edit_newsletters_templates";

		}
	}else{
		$page = $data;
		$form_kind = "form_edit_newsletters_templates";
	}
 	if(isset($add)){
		?>
		<a class="btn btn-success go_back" href="<?= URLROOT.getUrlLang(29) ?>?newsletter_url=4" ><?= $lang['back'] ?></a>
		<?php
		echo '
		<form id="form_add_newsletters_templates" action=""  method="post" enctype="multipart/form-data">';
		echo '<input class="form-control" type="text" name="name" placeholder="Nom de l\'infolettre" value="" />';
		echo "

			</br><h2 class='mtop'>Version FR </h2>";
		echo '<textarea name="message"  class="ckeditor"  data-custom-config="mailerConfig.js"> </textarea>';
		echo "</br><h2 class='mtop'>Version EN </h2>";
		echo '<textarea name="message_en"   class="ckeditor"   data-custom-config="mailerConfig.js"></textarea>';
		echo'
			<input type="submit" class="btn btn-info btn_submit_template"  value="'.$lang['add'].'">
		</form>';


	}else{

			echo '
			<form id="form_edit_newsletters_templates" action="" method="post" page-id="'.$page->id.'" enctype="multipart/form-data">';
			echo '<input class="form-control" type="text" name="name"  placeholder="Nom de l\'infolettre" value="'.$page->name.'" />';
			echo "</br><h2 class='mtop'>Version FR </h2>";
			// --------------- VARIABLES DE CONCEPTION DU TEMPLATE
			$mois = array("", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");
			$datefr = $mois[date("n")]." ".date("Y");

			echo '<textarea name="message"   class="ckeditor" data-custom-config="mailerConfig.js">'.$page->message.'</textarea>';

			echo "</br><h2 class='mtop'>Version EN </h2>";

			echo '<textarea name="message_en"   class="ckeditor" data-custom-config="mailerConfig.js">'.$page->message_en.'</textarea>';
			echo'
				<input type="submit" class="btn btn-info btn_submit_template"  value="'.$lang['edit'].'">
			</form>';
	}

?>
 <script>$(".ckeditor").each(function () {
                CKEDITOR.replace($(this).attr("name"));
            });
 </script>
