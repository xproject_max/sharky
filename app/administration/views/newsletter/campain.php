<?PHP
	//requires model newsletter


	require_once APPROOT."/config/lang.php";
	//Initialize $page
	if(!is_object($data)){
		if(isset($data[0])){
			$page = $data[0];
		}else{
			$page = $data;
		}
		if(is_array($page)){
			if(isset($page['load_add_newsletters_campains'])){
				$add = true;
				if(isset($page['getModule'])){
					$type = $page['getModule'];
				}
				$page = $page['page'];
				$form_kind = "form_add_newsletters_campains";
			}else{
				$form_kind = "form_edit_newsletters_campains";
			}
		}else{
			//Require headers and lang
			$page = $data[0];
			$form_kind = "form_edit_newsletters_campains";

		}
	}else{
		$page = $data;
		$form_kind = "form_edit_newsletters_campains";
	}
	$form = new Form();
	$db = new Database();

	$group_array="";
	$res = $db->query("SELECT * FROM newsletter_groups order by id asc");
	$db->execute($res);
 	while($data = $db->fetch($res,'array')){
        $tablo[] = $data;
    }

    $nbcol = 3;
	$r= "";
    $group_array .= '<table class="center_check">';

    $nb = count($tablo);
    for ($i = 0; $i < $nb; $i++) {

        $valeur1 = $tablo[$i]['id'];
        $valeur2 = $tablo[$i]['name'];
        if ($i % $nbcol == 0)
            $group_array .= '<tr>';

        if (isset($valeur1) && !is_null($valeur1) && !is_object($valeur1)) {
            $check = $db->query("SELECT mg.* FROM newsletter_groups mg RIGHT JOIN newsletter_campains m ON find_in_set(m.fk_groups,mg.id) where m.fk_groups like CONCAT('%-',:valeur1, '-%') and m.id = :id ");
           	$db->bind($check,":valeur1", $valeur1);
           	$db->bind($check,":id", $page->id);
			$db->execute($check);
			$res = $db->rowCount($check);
            if ($res > 0) {
                $r = "checked";
            } else {
             	$r= "";
            }
			$group_array .= '
				<td >
					<div class="form-check">
					  <label  >
						<input class="form-check-input" type="checkbox" name="fk_groups[]" value="' . $valeur1 .
				'" ' . $r . '>' . $valeur2 . '
						<span class="form-check-sign">
						  <span class="check"></span>
						</span>
					  </label>
					</div>
				</td>';
        }



        if ($i % $nbcol == ($nbcol - 1))
            $group_array .= '</tr>';

    }

    $group_array .= '</table>';


	$getTemplates = $db->query("SELECT * FROM newsletter_templates");
	$option_templates = "";
	if($db->execute($getTemplates)){
		if($db->rowCount($getTemplates) > 0){
			while($temp = $db->fetch($getTemplates)){

				$option_templates.='<option '.($page->fk_templates == $temp->id ? "selected" : "").' value="'.$temp->id.'">'.$temp->name.'</option>';
			}
		}
	}

 	if(isset($add)){
		?>
		 <a class="btn btn-success go_back" href="<?= URLROOT.getUrlLang(29) ?>?newsletter_url=4" ><?= $lang['back'] ?></a>
		<?php
		echo '
		<form id="form_add_newsletters_campains" action="" method="post" page-id="'.$page->id.'" enctype="multipart/form-data">';

		echo '<input class="form-control" type="text" name="name"  placeholder="Nom de la campagne" value="'.$page->name.'" />';
		echo '<input class="form-control" type="text" name="subject"  placeholder="Sujet" value="'.$page->name.'" />';
		echo '<input class="form-control" type="text" name="from_mail"  placeholder="Envoyer depuis le courriel suivant" value="'.$page->name.'" />';
		echo '<label for="status">Status de la campagne?</label><select class="form-control status" type="text" name="status"    >
		<option value="Envoyée">Envoyée</option>
		<option selected value="En attente">En attente</option>
		<option value="Prête pour l\'envoi">Prête pour l\'envoi</option>
		</select>
		';
		echo '<input class="form-control date DateTimePicker" type="date" name="date"  placeholder="Date prévu de la campagne?" value="'.$page->name.'" />';
		echo '<div class="grid_12 mtop"><label>Groupe(s) de destination:</label><br>'.$group_array.'</div>';

		echo'
		<select class="form-control   change_temp" type="text" name="fk_templates"    >'.$option_templates.'</select>
		<div class="grid_12 append_here"></div>
			<input type="submit" class="btn btn-info btn_submit_campains"  value="'.$lang['add'].'">
		</form>';

	}else{
		$Newsletter = new Modelss\Newsletter();
		?>
		<a class="btn btn-success go_back" href="<?= URLROOT.getUrlLang(29) ?>?newsletter_url=4" ><?= $lang['back'] ?></a>
		<?php
		echo '
		<form id="form_edit_newsletters_campains" action="" method="post" page-id="'.$page->id.'" enctype="multipart/form-data">';
		echo '<input class="form-control" type="text" name="name"  placeholder="Nom de la campagne" value="'.$page->name.'" />';
		echo '<input class="form-control" type="text" name="subject"  placeholder="Sujet" value="'.$page->subject.'" />';
		echo '<input class="form-control" type="text" name="from_mail"  placeholder="Envoyer depuis le courriel suivant" value="'.$page->from_mail.'" />';
		echo '<label for="status">Status de la campagne?</label><select class="form-control status" type="text" name="status"  value="'.$page->status.'"  >
		<option '.($page->status == "Envoyée" ? "selected": "").' value="Envoyée">Envoyée</option>
		<option '.($page->status == "En attente" ? "selected": "").'selected value="En attente">En attente</option>
		<option '.($page->status == "Prête pour l\'envoi" ? "selected": "").'value="Prête pour l\'envoi">Prête pour l\'envoi</option>
		</select>
		';
		echo '<input class="form-control date DateTimePicker" type="date" name="date"  placeholder="Date prévu de la campagne?" value="'.$page->date.'" />';
		echo '<div class="grid_12 mtop"><label>Groupe(s) de destination:</label><br>'.$group_array.'</div>';

		echo'
		<select class="form-control   change_temp" type="text" name="fk_templates"    >'.$option_templates.'</select>
		<div class="grid_12 append_here">'.$Newsletter->getTemplateCampagnView($page->id).'</div>
			<input type="submit" class="btn btn-info btn_submit_campains"  value="'.$lang['edit'].'">
		</form>';
	}

?>
 <script>$(".ckeditor").each(function () {
                CKEDITOR.replace($(this).attr("name"));
            });
 </script>
