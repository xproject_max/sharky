<?PHP
	//requires model newsletter


	require_once APPROOT."/config/lang.php";
	//Initialize $page
	if(!is_object($data)){
		if(isset($data[0])){
			$page = $data[0];
		}else{
			$page = $data;
		}
		if(is_array($page)){
			if(isset($page['load_add_newsletters_members'])){
				$add = true;
				if(isset($page['getModule'])){
					$type = $page['getModule'];
				}
				$page = $page['page'];
				$form_kind = "form_add_newsletters_members";
			}else{
				$form_kind = "form_edit_newsletters_members";
			}
		}else{
			//Require headers and lang
			$page = $data[0];
			$form_kind = "form_edit_newsletters_members";

		}
	}else{
		$page = $data;
		$form_kind = "form_edit_newsletters_members";
	}
	$form = new Form();
	$db = new Database();

	$group_array="";
	$res = $db->query("SELECT * FROM newsletter_groups order by id asc");
	$db->execute($res);
 	while($data = $db->fetch($res,'array')){
        $tablo[] = $data;
    }

    $nbcol = 3;
	$r= "";
    $group_array .= '<table class="center_check">';

    $nb = count($tablo);
    for ($i = 0; $i < $nb; $i++) {

        $valeur1 = $tablo[$i]['id'];
        $valeur2 = $tablo[$i]['name'];
        if ($i % $nbcol == 0)
            $group_array .= '<tr>';

        if (isset($valeur1) && !is_null($valeur1) && !is_object($valeur1)) {
            $check = $db->query("SELECT mg.* FROM newsletter_groups mg RIGHT JOIN newsletter_members m ON find_in_set(m.fk_groups,mg.id) where m.fk_groups like CONCAT('%-',:valeur1, '-%') and m.id = :id ");
           	$db->bind($check,":valeur1", $valeur1);
           	$db->bind($check,":id", $page->id);
			$db->execute($check);
			$res = $db->rowCount($check);
            if ($res > 0) {
                $r = "checked";
            } else {
             	$r= "";
            }
			$group_array .= '
				<td >
					<div class="form-check">
					  <label  >
						<input class="form-check-input" type="checkbox" name="fk_groups[]" value="' . $valeur1 .
				'" ' . $r . '>' . $valeur2 . '
						<span class="form-check-sign">
						  <span class="check"></span>
						</span>
					  </label>
					</div>
				</td>';
        }



        if ($i % $nbcol == ($nbcol - 1))
            $group_array .= '</tr>';

    }

    $group_array .= '</table>';
 	if(isset($add)){
		?>
		<a class="btn btn-success go_back" href="<?= URLROOT.getUrlLang(29) ?>?newsletter_url=4" ><?= $lang['back'] ?></a>
		<?php
		echo '
		<form id="form_add_newsletters_members" action="" method="post" page-id="'.$page->id.'" enctype="multipart/form-data">';
		echo '<input class="form-control" type="text" name="email"  placeholder="Courriel" value="'.$page->email.'" />';
		echo '<input class="form-control" type="text" name="first_name"  placeholder="Prénom" value="'.$page->first_name.'" />';
		echo '<input class="form-control" type="text" name="last_name"  placeholder="Nom" value="'.$page->last_name.'" />';
		echo '<div class="grid_12 mtop"><label>Groupe(s):</label><br>'.$group_array.'</div>';
		echo'
			<input type="submit" class="btn btn-info btn_submit_members"  value="'.$lang['add'].'">
		</form>';

	}else{

		?>
		<a class="btn btn-success go_back" href="<?= URLROOT.getUrlLang(29) ?>?newsletter_url=4" ><?= $lang['back'] ?></a>
		<?php
		echo '
		<form id="form_edit_newsletters_members" action="" method="post" page-id="'.$page->id.'" enctype="multipart/form-data">';
		echo '<input class="form-control" type="text" name="email"  placeholder="Courriel" value="'.$page->email.'" />';
		echo '<input class="form-control" type="text" name="first_name"  placeholder="Prénom" value="'.$page->first_name.'" />';
		echo '<input class="form-control" type="text" name="last_name"  placeholder="Nom" value="'.$page->last_name.'" />';
		echo '<div class="grid_12 mtop"><label>Groupe(s):</label><br>'.$group_array.'</div>';
		echo'
			<input type="submit" class="btn btn-info btn_submit_members"  value="'.$lang['edit'].'">
		</form>';
	}

?>
 <script>$(".ckeditor").each(function () {
                CKEDITOR.replace($(this).attr("name"));
            });
 </script>
