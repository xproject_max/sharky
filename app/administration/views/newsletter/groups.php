<?PHP
	//requires model newsletter
 require_once APPROOT."/models/Newsletter.php";


	//On ajoute un bouton pour ajouter un groupe de newsletter
	echo '<a class="btn btn-info add_newsletters_groups">'.$lang['add'].'</a>';

	//Voir les groupes des infolettres et pouvoir les modifiers individuellement

	echo '
		<table class="list_newsletters_tables  ">
			<tr><th>Nom du groupe</th><th class="td-actions">Action(s)</th></tr>
	';
		$newsletters = new \Modelss\Newsletter(); 
		$getAllNewslettersGroups = $newsletters->fetchAllGroups();
		foreach($getAllNewslettersGroups as $group){
			echo '<tr><td>'.$group->name.'</td><td class="td-actions"><a title="Modifier" id="' . $group->id .'" class="btn edit_newsletter_groups btn-warning" href="#">
			<i class="fas fa-pen-square"></i>
			</a>
			<a id="'.$group->id.'" title="Supprimer" class="btn btn-danger delete-newsletterGroups" >
			<i class="fas fa-times-square"></i>
			</a></td></tr>';
		}

	echo'
		</table>
	';

?>
<script>
tess.initialize();
</script>
