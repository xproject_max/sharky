<?php
	if(((is_array($data) && empty($data)) || (is_object($data) && !property_exists($data, "id")))){
		redirect("pages/index");
	}

	require_once APPROOT."/config/lang.php";
	include_once APPROOT."/helpers/function.php";

	//Initialize $page
	$page_id = 20; //inputs form page

	if(!is_object($data)){
		if(isset($data[1])){
			$page = $data[1];
		}else{
			$page = $data;
		}
		if(isset($page['load_add_inputs'])){
			$add = true;
			$form_kind = "form_add_inputs";
		}else{
			$form_kind = "form_edit_inputs";
		}

		    require_once APPROOT . "/views/inc/header.php";
	}else{
		$page = $data;
		$add = false;
		$form_kind = "form_edit_inputs";
	}
	//Initialize classes
	$db = new Database();
	$form = new Form();

?>
<div id="content" class="  categories newsletter">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_newsletter'] ?></h1>
	</div>
	<div class="mtop">

		<div class="card-content">
			<div class="table-responsive">
				<?php

					/*

						main.php (this file)

						detect if has id on url to bring good file. (approx: 20min)

						to-do
						Premiere étape: liens -
						Membres? - Groupes? - Campagnes (listes d'infolettres) ? - infolettres (templates) ? Envoyer une infolettre ?

						Ensuite Séparation des liens en 4 fichiers includes
						Deuxième étape (partie Membres)
						-Statistiques global (combien de membres)
						-Importer une liste de clients (CSV) dans un groupe CSV au pire
						-Ajout d'un membre manuellement/modifier et supprimer
						-système de pagination ofc.

						Troisème étape (Partie Groupes)

						- Ajout/modification et suppression de Groupes
						- Affichage du nombre de membres par groupe
						- Voir ou non les membres afin de pouvoir les deplacers/supprimer dans chacun des groupes?

						Quatrième étape: (Partie Campagne)

						- liste des infolettres envoyées ou prêtes a être envoyées.
						- Modifier/supprimer/Ajouter
						- statistiques(Combien de membres l'ont lue, nombre de personnes envoyées donc le groupe associé etc.)

						Cinquième étape (Infolettres - template)

						- liste infolettres
						- ajout modifier Supprimer
						- multilingue ou none
						- lexique (Ex: __CLIENT_NAME__ , __CLIENT_EMAIL__, etc)

						Sixième étape (ENVOYER)

						- liste deroulante template d'infolettre a choisir.
						- case a cocher du ou des groupes a envoyer l'infolettre.
						- bouton envoyer + une confirmation.
						(sera ajouter a la liste de campagne avec un etat);

					 */

					echo '
						<div class="links">
							<ul>
								<li><a data-mid="1" class="btn btn-info load_newsletter '.((isset($_GET['newsletter_url']) && $_GET['newsletter_url'] == 1) || !isset($_GET['newsletter_url']) ? "active" : "" ).'" href="?newsletter_url=1">Campagne</a></li>
								<li><a data-mid="2" class="btn btn-info load_newsletter '.((isset($_GET['newsletter_url']) && $_GET['newsletter_url'] == 2) ? "active" : "" ).'" href="?newsletter_url=2">Membres</a></li>
								<li><a data-mid="3" class="btn btn-info load_newsletter '.((isset($_GET['newsletter_url']) && $_GET['newsletter_url'] == 3) ? "active" : "" ).'" href="?newsletter_url=3">Groupes</a></li>
								<li><a data-mid="4" class="btn btn-info load_newsletter '.((isset($_GET['newsletter_url']) && $_GET['newsletter_url'] == 4) ? "active" : "" ).'" href="?newsletter_url=4">Templates</a></li>
								<li><a data-mid="5" class="btn btn-info load_newsletter '.((isset($_GET['newsletter_url']) && $_GET['newsletter_url'] == 5) ? "active" : "" ).'" href="?newsletter_url=5">Envoyer une infolettre</a></li>
							</ul>
						</div>
					';
					echo '<div class="container_newsletter">';
 
				   	 	require_once APPROOT."/models/Newsletter.php";

					if(isset($_GET['newsletter_url'])){
						switch($_GET['newsletter_url']){

							case "1":
								include "campains.php";
							break;

							case "2":
								include "members.php";
							break;

							case "3":
								include "groups.php";
							break;

							case "4":
								include "templates.php";
							break;

							case "5":
								include "sender.php";
							break;
							default:
								include "campains.php";

							break;

						}
					}else{
						include "campains.php";
					}
				?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
		//Require headers and lang
		require_once APPROOT . "/views/inc/footer.php";
?>
