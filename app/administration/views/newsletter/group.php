<?PHP
	//requires model newsletter


	require_once APPROOT."/config/lang.php";
	//Initialize $page
	if(!is_object($data)){
		if(isset($data[0])){
			$page = $data[0];
		}else{
			$page = $data;
		}
		if(is_array($page)){
			if(isset($page['load_add_newsletters_groups'])){
				$add = true;
				if(isset($page['getModule'])){
					$type = $page['getModule'];
				}
				$page = $page['page'];
				$form_kind = "form_add_newsletters_groups";
			}else{
				$form_kind = "form_edit_newsletters_groups";
			}
		}else{
			//Require headers and lang
			$page = $data[0];
			$form_kind = "form_edit_newsletters_groups";

		}
	}else{
		$page = $data;
		$form_kind = "form_edit_newsletters_groups";
	}
 	if(isset($add)){
		?>
		<a class="btn btn-success go_back" href="<?= URLROOT.getUrlLang(29) ?>?newsletter_url=4" ><?= $lang['back'] ?></a>
		<?php
		echo '
		<form id="form_add_newsletters_groups" action="" method="post" page-id="'.$page->id.'" enctype="multipart/form-data">';
		echo '<input class="form-control" type="text" name="name"  placeholder="Nom du groupe" value="'.$page->name.'" />';
		echo'
			<input type="submit" class="btn btn-info btn_submit_template"  value="'.$lang['add'].'">
		</form>';

	}else{

			echo '
			<form id="form_edit_newsletters_groups" action="" method="post" page-id="'.$page->id.'" enctype="multipart/form-data">';
			echo '<input class="form-control" type="text" name="name"  placeholder="Nom du groupe" value="'.$page->name.'" />
				<input type="submit" class="btn btn-info btn_submit_template"  value="'.$lang['edit'].'">
			</form>';
	}

?>
 <script>$(".ckeditor").each(function () {
                CKEDITOR.replace($(this).attr("name"));
            });
 </script>
