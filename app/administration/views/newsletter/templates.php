<?PHP
	//requires model newsletter
 require_once APPROOT."/models/Newsletter.php";


	//On ajoute un bouton pour ajouter un template de newsletter
	echo '<a class="btn btn-info add_newsletters_templates">'.$lang['add'].'</a>';

	//Voir les infolettres et pouvoir les modifiers individuellement

	echo '
		<table class="list_newsletters_tables  ">
			<tr><th>Nom template</th><th class="td-actions">Action(s)</th></tr>
	';
		$newsletters = new \Modelss\Newsletter(); 
		$getAllNewsletters = $newsletters->fetchAllTemplates();
		foreach($getAllNewsletters as $newletter){
			echo '<tr><td>'.$newletter->name.'</td><td class="td-actions"><a title="Modifier" id="' . $newletter->id .'" class="btn edit_newsletter_template btn-warning" href="#">
			<i class="fas fa-pen-square"></i>
			</a>
			<a id="'.$newletter->id.'" title="Supprimer" class="btn btn-danger delete-newsletterTemplates" >
			<i class="fas fa-times-square"></i>
			</a></td></tr>';
		}

	echo'
		</table>
	';

?>
<script>
tess.initialize();
</script>
