<?php
	// $infolettre = $Tess_db->select('SELECT * FROM newsletter_archived ORDER BY id DESC LIMIT 0,1');
// ---- Template ------------------------------------------- //
// La balise <<<EOF doit être absolument collée à gauche complètement. Le contenu peut être indenté
// Utiliser le moins de CSS possible
	if($lang == 'FR' ||  $lang == ''){
		$l = '';
		$newsletter = 'Infolettre';
		setlocale(LC_ALL, 'fr_CA');
	}else if($lang == 'EN'){
		$l = '_en';
		$newsletter = 'Newsletter';
		setlocale(LC_ALL, 'en_CA');
	}
	$date = strftime("%A %e %B %Y",time());

	$_LANG['FR']['infolettre'] = 'Infolettre du ';
	$_LANG['EN']['infolettre'] = 'Newsletter of ';
	$_LANG['FR']['desabonner'] = 'Me désabonner X';
	$_LANG['EN']['desabonner'] = 'Unsubscribe X';

	$color1 = "#494949";   	//Gris fonce
	$color2 = "#FFF";  		//Blanc
	$color3 = "#dedddd";  	//Gris header
	$color4 = "#dee7ef";  	//Gris/Bleu border
	$color5 = "#60be2a";  	//Vert
	$color6 = "#efefef";  	//Gris col 1
	$color7 = "#f6f6f6";  	//Gris col 2
	$color8 = "#787878";  	//Gris text
	$color9 = "#aaa9a9";  	//Gris text plus petit
	$color10 = "#000000";  	//Gris text plus petit

	$width = 'width="100%"';
	$site_img = $base_url . 'images/';
	$site_media = $base_url . 'uploads/';

	$template = '';

	$template .= '
	<table '.$width.' style="color: '.$color2.'; background: '.$color10.'">
		<tr '.$width.' bgcolor="'.$color10.'" style="color: '.$color2.'; background: '.$color10.'">
			<td><img src="https://masquez-vous-qc.ca/images/logo/logo.png" style="background:'.$color2.';height:150px;width:auto;"  /></td>
			<td style="text-align: center;text-transform:uppercase;" ><span style="text-align: center;text-transform:uppercase;"> '.strtoupper($newsletter).'</span></tr>
		</tr>
	</table>
	';
?>
