<?PHP
	//requires model newsletter
 require_once APPROOT."/models/Newsletter.php";


	//On ajoute un bouton pour ajouter un membre à la newsletter
	echo '<a class="btn btn-info add_newsletters_members">'.$lang['add'].'</a>';

	//Voir les membres des infolettres et pouvoir les modifiers individuellement

	echo '
		<table class="list_newsletters_tables  ">
			<tr><th>Courriel</th><th>Prénom</th><th>Nom</th><th>Groupe(s)</th><th>Date d\'adhésion</th><th class="td-actions">Action(s)</th></tr>
	';
		$newsletters = new \Modelss\Newsletter(); 
        $db = new Database();
		$getAllNewslettersMembers = $newsletters->fetchAllMembers();
		foreach($getAllNewslettersMembers as $member){
			$groups_joined = "";
			$group_array = explode("-",$member->fk_groups);
			$i= 0;
			foreach($group_array as $group){
                if($group !== ""){
    				$query_get_names = $db->query("
    					SELECT * FROM newsletter_groups
    					WHERE id = :id
    				");
    				$db->bind($query_get_names, ":id", $group);
    				$name = $db->single($query_get_names)->name;
    				$groups_joined .= ($i !== 0 ? "," : "").$name;
    				$i++;
                }
			}
			echo '<tr>
			<td>'.$member->email.'</td>
			<td>'.$member->first_name.'</td>
			<td>'.$member->last_name.'</td>
			<td>'.$groups_joined.'</td>
			<td>'.$member->date_joined.'</td>
			<td class="td-actions"><a title="Modifier" id="' . $member->id .'" class="btn edit_newsletter_members btn-warning" href="#">
			<i class="fas fa-pen-square"></i>
			</a>
			<a id="'.$member->id.'" title="Supprimer" class="btn btn-danger delete-newsletterMembers" >
			<i class="fas fa-times-square"></i>
			</a></td></tr>';
		}

	echo'
		</table>
	';

?>
<script>
tess.initialize();
</script>
