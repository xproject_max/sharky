<?PHP
	//requires model newsletter

	//On ajoute un bouton pour ajouter une campagne à la newsletter
	echo '<a class="btn btn-info add_newsletters_campains">'.$lang['add'].'</a>';

	//Voir les campagnes des infolettres et pouvoir les modifiers individuellement

	echo '
		<table class="list_newsletters_tables  ">
			<tr><th>Nom de la campagne</th><th>Nom du template</th><th>Groupe(s)</th><th>Status</th><th>Date prévu</th><th class="td-actions">Action(s)</th></tr>
	';

		$newsletters = new \Modelss\Newsletter(); 
        $db = new Database();
		$getAllNewslettersCampains = $newsletters->fetchAllCampains();
		foreach($getAllNewslettersCampains as $campain){
			$templateName = $newsletters->getTemplateName($campain->fk_templates);
			$groups_joined = "";
			$group_array = explode("-",$campain->fk_groups);
			$i= 0;
			foreach($group_array as $group){
                if($group !== ""){
    				$query_get_names = $db->query("
    					SELECT * FROM newsletter_groups
    					WHERE id = :id
    				");
    				$db->bind($query_get_names, ":id", $group);
    				$name = $db->single($query_get_names)->name;
    				$groups_joined .= ($i !== 0 ? "," : "").$name;
    				$i++;
                }
			}
			echo '<tr>
			<td>'.$campain->name.'</td>
			<td>'.$templateName.'</td>
			<td>'.$groups_joined.'</td>
			<td>'.$campain->status.'</td>
			<td>'.$campain->date.'</td>
			<td class="td-actions"><a title="Modifier" id="' . $campain->id .'" class="btn edit_newsletter_campains btn-warning" href="#">
			<i class="fas fa-pen-square"></i>
			</a>
			<a id="'.$campain->id.'" title="Supprimer" class="btn btn-danger delete-newsletterCampains" >
			<i class="fas fa-times-square"></i>
			</a></td></tr>';
		}

	echo'
		</table>
	';

?>
<script>
tess.initialize();
</script>
