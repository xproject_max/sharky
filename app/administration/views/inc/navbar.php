<?php
    if(isset($_SESSION['user_id'])) {
        
        $db = new Database();
        
 ?>
<div class="sidebar"  data-background-color="black" data-image="<?= URLROOT.'/images/bg11.jpg' ?>">
 
      <div class="sidebar-wrapper">
        
        <ul class="nav">
		
          <li class="user nav-item username">
			<a class="nav-link username" data-toggle="collapse" href="#pagesExamplesuser">
			  <div class="photo">
				<img src="<?= (file_exists(URLIMG.$_SESSION['profile_image']) ? URLIMG.$_SESSION['profile_image'] :  URLROOT.'/images/logo/logo.png') ?>" />
			  </div>
			  <p> <?= $_SESSION['user_name'] ?>
				<b class="caret"></b>
			  </p>
			</a> 
			<div class="collapse show" id="pagesExamplesuser">
			  <ul class="nav">
			  <?php 
				if(has_rights_to_see($_SESSION['status'], 5)){
			  ?>
				<li class="nav-item">
				  <a class="nav-link <?= $page->id == 5 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(5) ?>" url_linv="<?php echo URLROOT.getUrlLang(5, $linv) ?>">
					<span class="sidebar-mini"><i class="fad fa-user-circle"></i></span>
					<span class="sidebar-normal"><?= $lang['menu_profile']?></span>
				  </a>
				</li>
			  <?php
				}
			  ?> 
				<li class="nav-item">
				 
				  <a class="dropdown-item <?= $page->id == 3 ? "active" : "" ?>" href="<?php echo URLROOT?>fr/tess/logout">
					<span class="sidebar-mini"><i class="fad fa-user-circle"></i></span>
					<span class="sidebar-normal"><?= $lang['menu_logout']?></span> 
				  </a>
				</li> 
			  </ul>
			</div>  
          </li> 
          <?php 
            if(has_rights_to_see($_SESSION['status'], 1)){
          ?>
          <li class="nav-item">
            <a class="nav-link <?= $page->id == 1 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(1) ?>" url_linv="<?php echo URLROOT.getUrlLang(1, $linv) ?>">
              <i class="fad fa-tachometer-alt"></i>
              <p> <?= $lang['menu_dashboard']?> </p>
            </a>
          </li>
          
          <?php 
            }
          ?>
           
          <?php 
          // Si au moins le droit de voir systeme de page (4)
          // ou voir système de blog(22) ou
          // encore système de concours (23) alors on affiche
            if(has_rights_to_see($_SESSION['status'], 4) ||
            has_rights_to_see($_SESSION['status'], 22) ||
            has_rights_to_see($_SESSION['status'], 23)
            ){
          ?>
          <li class="nav-item username">
            <a class="nav-link username" data-toggle="collapse" href="#pagesExamples">
              <i class="fad fa-file-search"></i>
              <p> Pages
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse show" id="pagesExamples">
              <ul class="nav">
              <?php 
                if(has_rights_to_see($_SESSION['status'], 4)){
              ?>
                <li class="nav-item ">
                  <a class="nav-link <?= $page->id == 4 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(4) ?>" url_linv="<?php echo URLROOT.getUrlLang(4, $linv) ?>">
                    <span class="sidebar-mini"> <i class="fad fa-plus-square"></i> </span>
                    <span class="sidebar-normal"> <?= $lang['menu_pages']?> </span>
                  </a>
                </li> 
              <?php 
                }
              ?>
              <?php 
                if(has_rights_to_see($_SESSION['status'], 22)){
              ?>
                <li class="nav-item ">
                  <a class="nav-link <?= $page->id == 22 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(22) ?>" url_linv="<?php echo URLROOT.getUrlLang(22, $linv) ?>">
                    <span class="sidebar-mini"> <i class="fad fa-plus-square"></i> </span>
                    <span class="sidebar-normal"> <?= $lang['menu_blogs']?> </span>
                  </a>
                </li>
              <?php 
                }
              ?>
              <?php 
                if(has_rights_to_see($_SESSION['status'], 23)){
              ?>
                <li class="nav-item ">
                  <a class="nav-link <?= $page->id == 23 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(23) ?>" url_linv="<?php echo URLROOT.getUrlLang(23, $linv) ?>">
                    <span class="sidebar-mini"> <i class="fad fa-plus-square"></i> </span>
                    <span class="sidebar-normal"> <?= $lang['menu_contests']?> </span>
                  </a>
                </li>
              <?php 
                }
              ?> 
              <?php 
                if(has_rights_to_see($_SESSION['status'], 32)){
              ?>
                <li class="nav-item ">
                  <a class="nav-link <?= $page->id == 32 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(32) ?>" url_linv="<?php echo URLROOT.getUrlLang(32, $linv) ?>">
                    <span class="sidebar-mini"> <i class="fad fa-plus-square"></i> </span>
                    <span class="sidebar-normal"> Éditeur de code </span>
                  </a>
                </li>
              <?php 
                }
              ?> 
              </ul>
            </div>
          </li>
          <?php
            }
          ?> 
          
          <?php 
          // Si au moins le droit de voir systeme de commandes (9)
          // ou voir système de produits(9) ou
          // encore système de promotions (10) alors on affiche
            if(has_rights_to_see($_SESSION['status'], 9) ||
            has_rights_to_see($_SESSION['status'], 10) ||
            has_rights_to_see($_SESSION['status'], 24)
            ){
          ?>
          <li class="nav-item username">
            <a class="nav-link username" data-toggle="collapse" href="#shopExamples">
              <i class="fad fa-cart-arrow-down"></i>
              <p> <?= $lang['menu_shop']?>
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse show" id="shopExamples">
              <ul class="nav"> 
              <?php 
                if(has_rights_to_see($_SESSION['status'], 9)){
              ?>
                <li class="nav-item ">
                  <a class="nav-link <?= $page->id == 9 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(9) ?>" url_linv="<?php echo URLROOT.getUrlLang(9, $linv) ?>">
                    <span class="sidebar-mini"> <i class="fas fa-file-pdf"></i>  </span>
                    <span class="sidebar-normal"> <?= $lang['menu_orders']?> </span>
                  </a>
                </li>
              <?php 
                }
              ?>
              <?php 
                if(has_rights_to_see($_SESSION['status'], 10)){
              ?>
                <li class="nav-item ">
                  <a class="nav-link <?= $page->id == 10 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(10) ?>" url_linv="<?php echo URLROOT.getUrlLang(10, $linv) ?>">
                    <span class="sidebar-mini"> <i class="fad fa-cart-arrow-down"></i> </span>
                    <span class="sidebar-normal"> <?= $lang['menu_products']?> </span>
                  </a>
                </li>
              <?php 
                }
              ?>
              <?php 
                if(has_rights_to_see($_SESSION['status'], 24)){
              ?>
                <li class="nav-item ">
                  <a class="nav-link <?= $page->id == 24 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(24) ?>" url_linv="<?php echo URLROOT.getUrlLang(24, $linv) ?>">
                    <span class="sidebar-mini"> <i class="fad fa-percentage"></i> </span>
                    <span class="sidebar-normal"> <?= $lang['menu_promotions']?> </span>
                  </a>
                </li>
              <?php 
                }
              ?> 
              </ul>
            </div>
          </li>
          <?php 
            }
          ?>
          
          <?php 
          // Si au moins le droit de voir systeme de users (11)
          // ou voir système de compagnies(12) ou
          // encore système de roles (25)
          // ou voir système de dragon users(13) ou
          // ou voir système de roles dragon(17) ou
            if(has_rights_to_see($_SESSION['status'], 11) ||
            has_rights_to_see($_SESSION['status'], 12) ||
            has_rights_to_see($_SESSION['status'], 25) ||
            has_rights_to_see($_SESSION['status'], 13) ||
            has_rights_to_see($_SESSION['status'], 17) 
            ){
          ?>
          <li class="nav-item username">
            <a class="nav-link username" data-toggle="collapse" href="#accountsExamples">
              <i class="far fa-users-cog"></i>
              <p> <?= $lang['menu_accounts']?>
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse show" id="accountsExamples">
              <ul class="nav"> 
              <?php 
                if(has_rights_to_see($_SESSION['status'], 11)){
              ?>
                <li class="nav-item ">
                  <a class="nav-link <?= $page->id == 11? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(11) ?>" url_linv="<?php echo URLROOT.getUrlLang(11, $linv) ?>">
                    <span class="sidebar-mini"> <i class="fad fa-user-friends"></i>  </span>
                    <span class="sidebar-normal"> <?= $lang['website_accounts']?> </span>
                  </a>
                </li>
              <?php 
                }
              ?>
              <?php 
                if(has_rights_to_see($_SESSION['status'], 12)){
              ?>
                <li class="nav-item ">
                  <a class="nav-link <?= $page->id == 12 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(12) ?>" url_linv="<?php echo URLROOT.getUrlLang(12, $linv) ?>">
                    <span class="sidebar-mini"> <i class="fad fa-building"></i>  </span>
                    <span class="sidebar-normal"> <?= $lang['company_accounts']?> </span>
                  </a>
                </li>
              <?php 
                }
              ?>
              <?php 
                if(has_rights_to_see($_SESSION['status'], 25)){
              ?>
                <li class="nav-item ">
                  <a class="nav-link <?= $page->id == 25 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(25) ?>" url_linv="<?php echo URLROOT.getUrlLang(25, $linv) ?>">
                    <span class="sidebar-mini"> <i class="fad fa-cogs"></i> </span>
                    <span class="sidebar-normal"> <?= $lang['menu_roles_website']?> </span>
                  </a>
                </li>
              <?php 
                }
              ?>
              <?php 
                if(has_rights_to_see($_SESSION['status'], 13)){
              ?>
                <li class="nav-item ">
                  <a class="nav-link <?= $page->id == 13 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(13) ?>" url_linv="<?php echo URLROOT.getUrlLang(13, $linv) ?>">
                    <span class="sidebar-mini"> <i class="fad fa-user-cog"></i> </span>
                    <span class="sidebar-normal"> <?= $lang['tess_accounts']?> </span>
                  </a>
                </li>
              <?php 
                }
              ?>
              <?php 
                if(has_rights_to_see($_SESSION['status'], 17)){
              ?>
                <li class="nav-item ">
                  <a class="nav-link <?= $page->id == 17 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(17) ?>" url_linv="<?php echo URLROOT.getUrlLang(17, $linv) ?>">
                    <span class="sidebar-mini"> <i class="fad fa-cogs"></i> </span>
                    <span class="sidebar-normal"> <?= $lang['menu_roles']?> </span>
                  </a>
                </li>
              <?php 
                }
              ?> 
              </ul>
            </div>
          </li>
              <?php 
                }
              ?> 
			  
          <?php 
          // Si au moins le droit de voir dictionnaire (14)
          // ou voir categ(15)  
            if(has_rights_to_see($_SESSION['status'], 14) ||
            has_rights_to_see($_SESSION['status'], 15)  
            ){
			?>
          <li class="nav-item username">
            <a class="nav-link username" data-toggle="collapse" href="#dictionaryExamples">
              <i class="fad fa-globe-americas"></i>
              <p> <?= $lang['menu_dictionary']?>
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse show" id="dictionaryExamples">
              <ul class="nav"> 
              <?php 
                if(has_rights_to_see($_SESSION['status'], 14)){
              ?>
                <li class="nav-item ">
                  <a class="nav-link <?= $page->id == 14 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(14) ?>" url_linv="<?php echo URLROOT.getUrlLang(14, $linv) ?>">
                    <span class="sidebar-mini"> <i class="fad fa-spell-check"></i>  </span>
                    <span class="sidebar-normal"> <?= $lang['menu_terms']?> </span>
                  </a>
                </li>
              <?php 
                }
              ?>
              <?php 
                if(has_rights_to_see($_SESSION['status'],15)){
              ?>
                <li class="nav-item ">
                  <a class="nav-link <?= $page->id == 15 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(15) ?>" url_linv="<?php echo URLROOT.getUrlLang(15, $linv) ?>">
                    <span class="sidebar-mini"> <i class="fad fa-folder-tree"></i> </span>
                    <span class="sidebar-normal"> <?= $lang['menu_category']?> </span>
                  </a>
                </li>
              <?php 
                }
              ?> 
              </ul>
            </div>
          </li>
		  <?php 
                }
              ?> 
			  
          <?php 
          // Si au moins le droit de voir module (6)
          // ou voir templates(8)  
          // ou voir champs(7)    
            if(has_rights_to_see($_SESSION['status'], 6) ||
            has_rights_to_see($_SESSION['status'], 8)   ||
            has_rights_to_see($_SESSION['status'], 7) 
            ){
			?>
          <li class="nav-item username">
            <a class="nav-link username" data-toggle="collapse" href="#toolExamples">
              <i class="fad fa-tools"></i>
              <p> <?= $lang['menu_tool']?>
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse show" id="toolExamples">
              <ul class="nav"> 
              <?php 
                if(has_rights_to_see($_SESSION['status'], 6)){
              ?>
                <li class="nav-item ">
                  <a class="nav-link <?= $page->id == 6 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(6) ?>" url_linv="<?php echo URLROOT.getUrlLang(6, $linv) ?>">
                    <span class="sidebar-mini"> <i class="fad fa-tools"></i> </span>
                    <span class="sidebar-normal"> <?= $lang['menu_modules']?> </span>
                  </a>
                </li>
              <?php 
                }
              ?>
              <?php 
                if(has_rights_to_see($_SESSION['status'], 8)){
              ?>
                <li class="nav-item ">
                  <a class="nav-link <?= $page->id == 8 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(8) ?>" url_linv="<?php echo URLROOT.getUrlLang(8, $linv) ?>">
                    <span class="sidebar-mini"> <i class="fad fa-copy"></i> </span>
                    <span class="sidebar-normal"> <?= $lang['menu_templates']?> </span>
                  </a>
                </li>
              <?php 
                }
              ?>
              <?php 
                if(has_rights_to_see($_SESSION['status'], 7)){
              ?>
                <li class="nav-item ">
                  <a class="nav-link <?= $page->id == 7 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(7) ?>" url_linv="<?php echo URLROOT.getUrlLang(7, $linv) ?>">
                    <span class="sidebar-mini"> <i class="fad fa-align-center"></i> </span>
                    <span class="sidebar-normal"> <?= $lang['menu_inputs']?> </span>
                  </a>
                </li>
              <?php 
                }
              ?> 
              </ul>
            </div>
          </li>
          <?php 
			}
            if(has_rights_to_see($_SESSION['status'], 16)){
          ?>
          <li class="nav-item">
            <a class="nav-link <?= $page->id == 16 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(16) ?>" url_linv="<?php echo URLROOT.getUrlLang(16, $linv) ?>">
              <i class="fad fa-photo-video"></i>
              <p> <?= $lang['menu_media']?> </p>
            </a>
          </li>
           <?php 
            }
          ?> 
          <?php 
            if(has_rights_to_see($_SESSION['status'], 29)){
          ?>
          <li class="nav-item">
            <a class="nav-link <?= $page->id == 29 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(29) ?>" url_linv="<?php echo URLROOT.getUrlLang(29, $linv) ?>">
              <i class="fad fa-envelope"></i>
              <p> <?= $lang['menu_newsletter'] ?> </p>
            </a>
          </li>
          <?php 
            }
          ?> 
          <?php 
            if(has_rights_to_see($_SESSION['status'], 30)){
          ?>
          <li class="nav-item">
            <a class="nav-link <?= $page->id == 30 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(30) ?>" url_linv="<?php echo URLROOT.getUrlLang(30, $linv) ?>">
              <i class="fad fa-cogs"></i>
              <p> <?= $lang['menu_quotes'] ?> </p>
            </a>
          </li>
          <?php 
            }
          ?>  
		</ul>
      </div>
    </div>
	<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " data-image="<?= URLROOT.'/images/bg11.jpg' ?>">
        <div class="imgafter"></div>
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-minimize">
              <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                <i class="fad fa-ellipsis-v text_align-center visible-on-sidebar-regular"></i>
                <i class="fad fa-ellipsis-h material-icons design_bullet-list-67 visible-on-sidebar-mini"></i>
              </button>
            </div>
			<img class="logocms" src="<?= URLROOT ?>/images/logo/logo.png" height="60px" >
			<div style="position:relative;">
             
			<div class="coolbar"></div>
          </div>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" name="search" value="" class="form-control search" placeholder="<?= $lang['menu_search'] ?>">
                <button type="submit" class="btn btn-white btn-round btn-just-icon finder-icon">
                  <i class="fad fa-search"></i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-items <?= $page->id == 1 ? "active" : "" ?>">
                <a class="nav-link <?= $page->id == 1 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(1) ?>">
                  <i class="fad fa-tachometer-alt dashboard_menu_bar <?= $page->id == 1 ? "active" : "" ?>"></i>
                  <p class="d-lg-none d-md-block">
                  </p>
                </a>
              </li>
              <li class="user">
                <a class="nav-x username" href="<?php echo URLROOT.getUrlLang($page->id, $linv) ?>"><?= ucfirst($linv) ?></a>
			  </li>
              <li class="nav-item username dropdown">
                <a class="nav-link" href="#" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <div class="btn btn-white btn-round btn-just-icon">
                    <img src="<?= (file_exists(URLIMG.$_SESSION['profile_image']) ? URLIMG.$_SESSION['profile_image'] :  URLROOT.'/images/logo/logo.png') ?>" />
                  </div>
                  <p class="d-lg-none d-md-block">
                    <?= $lang['welcome_back'].$_SESSION['user_name'] ?>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item <?= $page->id == 5 ? "active" : "" ?>" href="<?php echo URLROOT.getUrlLang(5) ?>">
                     <?= $lang['menu_profile']?>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item <?= $page->id == 3 ? "active" : "" ?>" href="<?php echo URLROOT ?>fr/tess/logout"><?= $lang['menu_logout']?></a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    <div class="main-panel <?= (!isset($_SESSION['user_id']) ? 'isLoginPage' : '') ?>">
      <!-- Navbar -->
      
<?php
    }
?>
