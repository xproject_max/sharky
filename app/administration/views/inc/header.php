<?php
    //Declare $variables;

    include APPROOT ."/config/lang.php";
    require_once APPROOT."/helpers/function.php";
    $data = isset($data) ? $data : [];
    $page = isset($data['page']) ? $data['page'][0] : [];
    if($language == "en")
        $l = "_en";
    $base_url = URLROOT . "/" . ($language ? $language : $data['lang']);
    if(!isset($lol))
        $lol = true;

    if(isset($_SESSION['status']) && $page->rights < $_SESSION['status']){
        die('no rights');
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title><?= $page->{'title'.$l} . " | " . SITENAME ?></title>
        <link rel="apple-touch-icon" sizes="180x180" href="<?= URLROOT ?>/images/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= URLROOT ?>/images/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= URLROOT ?>/images/favicons/favicon-16x16.png">
        <link rel="manifest" href="<?= URLROOT ?>/images/favicons/site.webmanifest">
        <!-- CSS --><script src="<?= URLROOT ?>/js/pace.min.js"></script>
        <link rel="stylesheet" href="<?= URLROOT ?>/css/custom_bootstrap.css" />
        <link rel="stylesheet" href="<?= URLROOT ?>/css/style.min.css" />
        <!-- <link rel="stylesheet" href="<?= URLROOT ?>/css/fontawesome/all/css/all.min.js"><!--     Fonts and icons     -->
        <!--script src="<?= dirname(URLROOT) ?>/css/fontawesome/all/js/all.min.js"  ></script><!--     Fonts and icons     -->  
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous">

    </head>
    <body class="<?= (!isset($_SESSION['user_id']) ? 'isLoginPage' : '') ?>">

    <?php include APPROOT . '/views/inc/navbar.php' ?>
    <div id="container" class="<?= (!isset($_SESSION['user_id']) ? 'isLoginPage' : '') ?>">
