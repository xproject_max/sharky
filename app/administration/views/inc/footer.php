    </div> 
    <script src="<?= URLROOT ?>/js/imports/core/jquery.min.js?v=1"></script>
    <script src="<?= URLROOT ?>/js/imports/core/popper.min.js?v=1"></script>
    <script src="<?= URLROOT ?>/js/imports/core/bootstrap-material-design.min.js?v=1"></script>
    <script src="<?= URLROOT ?>/js/imports/plugins/perfect-scrollbar.jquery.min.js?v=1"></script>
     
    <!-- Plugin for the momentJs  -->
    <script src="<?= URLROOT ?>/js/imports/plugins/moment.min.js?v=1"></script>
    <div id="url_scripts" data-url-scripts="<?= URLROOT ?>/"></div>
    <!--  Plugin for Sweet Alert -->
    <script src="<?= URLROOT ?>/js/imports/plugins/sweetalert2.js?v=1"></script>
    <!-- Forms Validations Plugin -->
    <script src="<?= URLROOT ?>/js/imports/plugins/jquery.validate.min.js?v=1"></script>
    <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
    <script src="<?= URLROOT ?>/js/imports/plugins/jquery.bootstrap-wizard.js?v=1"></script>
    <!--	Plugin for Select, full documentation here: https://silviomoreto.github.io/bootstrap-select -->
    <script src="<?= URLROOT ?>/js/imports/plugins/bootstrap-selectpicker.js?v=1"></script>
    <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
    <script src="<?= URLROOT ?>/js/imports/plugins/bootstrap-datetimepicker.min.js?v=1"></script>
    <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
    <script src="<?= URLROOT ?>/js/imports/plugins/bootstrap-tagsinput.js?v=1"></script>
    <!-- Plugin for Fileupload, full documentation here: https://www.jasny.net/bootstrap/javascript/#fileinput -->
    <script src="<?= URLROOT ?>/js/imports/plugins/jasny-bootstrap.min.js?v=1"></script>
    <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
    <script src="<?= URLROOT ?>/js/imports/plugins/fullcalendar.min.js?v=1"></script>
    <!-- Vector Map plugin, full documentation here: https://jvectormap.com/documentation/ -->
    <script src="<?= URLROOT ?>/js/imports/plugins/jquery-jvectormap.js?v=1"></script>
    <!--  Plugin for the Sliders, full documentation here: https://refreshless.com/nouislider/ -->
    <script src="<?= URLROOT ?>/js/imports/plugins/nouislider.min.js?v=1"></script>
    <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js?v=1"></script>
    <!-- Library for adding dinamically elements -->
    <script src="<?= URLROOT ?>/js/imports/plugins/arrive.min.js?v=1"></script> 
    <!-- Chartist JS -->
    <script src="<?= URLROOT ?>/js/imports/plugins/chartist.min.js?v=1"></script>
    <!--  Notifications Plugin    -->
    <script src="<?= URLROOT ?>/js/imports/plugins/bootstrap-notify.js?v=1"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="<?= URLROOT ?>/js/imports/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
    <script src="<?= dirname(URLROOT) ?>/plugins/ckeditor/ckeditor.js?v=1"></script>
      
    <script src="<?= URLROOT ?>/js/notify.js?v=1"></script>
    <script src="<?= URLROOT ?>/js/bootstrap.min.js?v=1"></script>
    <script src="<?= URLROOT ?>/js/scripts.js?v=1"></script>
    <script src="<?= URLROOT ?>/js/wow.min.js?v=1"></script>

	<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script> 
<script src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>


    <script> 
        new WOW().init();
    </script>    
	<script>
		$(window).on('load', function() { 
            tess.initialize();
            $('body').on('click', '.The_wizard_preview', function(e){
                $(this).next(".The_wizard_images").trigger("click");
            });              

            $(".ckeditor").each(function () { 
                CKEDITOR.replace($(this).attr("id"));
            });             
		});   
		$(document).ready(function() {
			
			$('.capture').each(function(e){
				var div = $(this);
				
				var id = div.data('id');
				var url = div.data('url');
				var categ = div.data('category');
				var lastedit = div.data('lastedit');
				
				$.ajax({
					url: '<?= PROTOCOL ?>://<?= DOMAINNAME ?>/administration/screenajax.php',
					async: true, 
					type: "POST",
					data: {id:id, url:url, category: categ, lastedit: lastedit},
					success: function(objServerResponse) {
						div.html(objServerResponse);
						  
					}
				}).done(function (msg) {
					
				});
				
			});
			 
		});   
		 
	</script> 
	
	<div class="previewweb"></div>
</body>
</html>
