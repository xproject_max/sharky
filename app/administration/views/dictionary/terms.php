<?php
	require_once APPROOT . "/views/inc/header.php";

	$menu_array = $data['menu_array'];
	$db = new Database();
	// var_dump($menu_array);
?>
<div id="content" url="<?= URLROOT."".getUrlLang(14) ?>" class="categories terms">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_terms'] ?></h1>
	</div>
	<input type="button" class="btn btn-success new_terms" value="<?= $lang['add']; ?>"   />
	</br>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="update_status_terms" name="update_status_terms">
					<table class="table" style=" margin: 0 auto;">
						<thead>
							<tr>
								<th style="padding-right:15px;"><?= $lang['pages_th_order'] ?></th>
								<th><?= $lang['menu_terms'] ?></th>
								<th></th>
								<th class="text-right" style="padding-right:15px;"><?= $lang['pages_th_actions'] ?></th>
							</tr>
						</thead>
						<tbody>
							<?= generate_menu_terms(0, $menu_array); ?>
						</tbody>
					</table></br>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
require_once APPROOT . "/views/inc/footer.php";
?>
