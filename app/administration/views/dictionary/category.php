<?php
	if(((is_array($data) && empty($data)) || (is_object($data) && !property_exists($data, "id")))){
		redirect("pages/index");
	}

	require_once APPROOT."/config/lang.php";
	include_once APPROOT."/helpers/function.php";

	//Initialize $page
	$page_id = 15; //categories form page

	if(!is_object($data)){
		if(isset($data[1])){
			$page = $data[1];
		}else{
			$page = $data;
		}
		if(isset($page['load_add_categories'])){
			$add = true;
			$form_kind = "form_add_categories";
		}else{
			$form_kind = "form_edit_categories";
		}
	}else{
		$page = $data;
		$add = false;
		$form_kind = "form_edit_categories";
	}

	//Initialize classes
	$db = new Database();
	$form = new Form();
	// var_dump($page);

?>
<div id="content" class="pages category categories">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_category'] ?></h1>
	</div>
	<a class="btn btn-success go_back" href="<?= URLROOT.getUrlLang(15) ?>" ><?= $lang['back'] ?></a>
	<?= (isset($add) ? $lang['menu_category'] : $page->{'title'.$l}) ?></br>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="<?= $form_kind ?>" page-id="<?= (!$add ? $page->id : $page_id) ?>"  enctype="multipart/form-data">
					<?php //input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="",$note_position="", $note="", $attributes) ?>
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("name", "text", $lang['title'], false, (!$add ? $page->name : '')).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("list_order", "text",$lang['order_rank'], false, (!$add ? $page->list_order : '')).'</div>'; ?>
					<?= '<div class="clear"></div>'; ?> 
					<?= (!$add ? '<input type="hidden" value="'.$page->id.'" name="inputs_id" />' : '') ?>
					<?= '<div class="clear"></div><div class="grid_12 grid_1600_12"><input type="submit" class="btn btn-success" value="'.(!$add ? $lang['edit'] : $lang['add']).'" name="submit" /></div>'; ?>
				</form>
			</div>
		</div>
	</div>
</div>
