<?php
	if(((is_array($data) && empty($data)) || (is_object($data) && !property_exists($data, "id")))){
		redirect("pages/index");
	}

	require_once APPROOT."/config/lang.php";
	include_once APPROOT."/helpers/function.php";
	// var_dump($data);
	//Initialize $page
	$page_id = 15; //terms form page

	if(!is_object($data)){
		
		if(isset($data['page'])){
			$page = $data['page'];
			$add= true;
			
				$form_kind = "form_add_terms";
		}else{
			if(isset($data[1])){
				$page = $data[1];
			}else{
				$page = $data;
			}
		
			if(isset($page['load_add_terms'])){
				$add = true;
				$form_kind = "form_add_terms";
			}else{
				$form_kind = "form_edit_terms";
			}
		}
	}else{
		
		$page = $data;
		$add = false;
		$form_kind = "form_edit_terms";
	}
	

	//Initialize classes
	$db = new Database();
	$form = new Form();
	
	//initialize parents 
	$select_parent = "";
	$parents = $db->query("SELECT * FROM dictionary_categ"); 
	$db->execute($parents);
	while($categ = $db->fetch($parents)){
		$select_parent .= "<option ".(isset($page->categ) && $page->categ == $categ->id ? "selected='selected'" : "")." value='".$categ->id."'>".$categ->name."</option>";
	}
	 
	//initialize termes
	$dictionary = "";
	
	$add_terms = false;
	if(isset($page->categ)){
		$query_terms_exists = $db->query("SELECT * FROM dictionary WHERE term_id = :categ");
		$db->bind($query_terms_exists, ":categ", $page->id);
		if($db->rowCount($query_terms_exists) > 0){
			$add_terms = true;
		}
	}
	if(!$add && $add_terms){
		//edit
		$all_entries = $db->query("
			SELECT *
			FROM dictionary
			WHERE term_id = :id
		");
		$db->bind($all_entries, ":id", $page->id);
		$db->execute($all_entries);
		while($entries = $db->fetch($all_entries)){
			$dictionary .= '<div class="clear"></div><div class="delta grid_12 grid_1600_12">'.$form->input("text_value_string[".$entries->language."]", "textarea",$entries->language, false, (!$add ? $entries->text_value : ''),"","").'</div>';
		}
	}else{
		//add
		if(MULTI_LANG){
			$insert1 = "";
			$insert2 = "";
			$insert3 = array();
			foreach(LANGUAGES as $l){ 
					
				$dictionary .= '<div class="clear"></div><div class="delta grid_12 grid_1600_12">'.$form->input("text_value_string[".$l."]", "textarea",$l, false, "","","").'</div>';
		 
			}
		}		
	}

?>
<div id="content" class="pages term terms">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_terms'] ?></h1>
	</div>
	<a class="btn btn-success go_back" href="<?= URLROOT.getUrlLang(14) ?>" ><?= $lang['back'] ?></a>
	<?= (isset($add) ? $lang['menu_terms'] : $page->{'title'.$l}) ?></br>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="<?= $form_kind ?>" page-id="<?= (!$add ? $page->id : $page_id) ?>"  enctype="multipart/form-data">
					<?php //input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="",$note_position="", $note="", $attributes) ?>
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("term_id", "text", $lang['title'], false, (!$add ? $page->term_id : '')).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("list_order", "text",$lang['order_rank'], false, (!$add ? $page->list_order : '')).'</div>'; ?>
					<?= '<div class="clear"></div>'; ?> 
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("user_note", "text", "notes", false, (!$add ? $page->user_note : '')).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("categ", "select",$lang['menu_category'], false, (!$add ? $page->categ : ''),"","",$select_parent).'</div>'; ?>
					<?= '<div class="clear"></div>'; ?> 
					<?= $dictionary; ?>
					<?= (!$add ? '<input type="hidden" value="'.$page->id.'" name="inputs_id" />' : '') ?>
					<?= '<div class="clear"></div><div class="grid_12 grid_1600_12"><input type="submit" class="btn btn-success" value="'.(!$add ? $lang['edit'] : $lang['add']).'" name="submit" /></div>'; ?>
				</form>
			</div>
		</div>
	</div>
</div>
