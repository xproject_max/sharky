<?php
	require_once APPROOT . "/views/inc/header.php";

	$db = new Database();
?>
<div id="content" url="<?= URLROOT."".getUrlLang(15) ?>" class="categories dictionary">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_category'] ?></h1>
	</div>
	<input type="button" class="btn btn-success new_categories" value="<?= $lang['add']; ?>"   />
	</br>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="update_status_categories" name="update_status_categories">
					<table class="table" style=" margin: 0 auto;">
						<thead>
							<tr>
								<th style="padding-right:15px;"><?= $lang['pages_th_order'] ?></th>
								<th><?= $lang['menu_category'] ?></th>
								<th></th>
								<th class="text-right" style="padding-right:15px;"><?= $lang['pages_th_actions'] ?></th>
							</tr>
						</thead>
						<tbody>
						<?php
							try{
								$query_categories = $db->query("SELECT * FROM dictionary_categ ORDER BY `list_order` ASC");
								$result = $db->execute($query_categories);
								$counted = $db->rowCount($query_categories);
								while ($categories = $query_categories->fetch($result)) {

									echo '
										<tr>
											<td>
												<p>'.$categories->list_order.'</p>
											</td>
											<td>
												<p data-tooltip="'.$categories->name.'">'.$categories->name.'</p>
											</td> 
											<td>
											</td>
											<td class="td-actions text-right" style="line-height:1.3; width:43px;">
												<a title="Modifier" id="' . $categories->id .'" class="btn btn-warning add_categories" href="#">
												<i class="fas fa-pen-square"></i>
												</a>
												<a id="'.$categories->id.'" title="Supprimer" class="btn btn-danger delete-categories" >
												<i class="fas fa-times-square"></i>
												</a>
											</td>
										</tr>
									';
								}
							}catch(PDOException $e){
								return false;
							}
						?>
						</tbody>
					</table></br>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
require_once APPROOT . "/views/inc/footer.php";
?>
