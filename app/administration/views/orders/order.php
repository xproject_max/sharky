<?php
	if(((is_array($data) && empty($data)) || (is_object($data) && !property_exists($data, "id")))){
		redirect("pages/index");
	}

	require_once APPROOT."/config/lang.php";
	include_once APPROOT."/helpers/function.php";
	 
	//Initialize $page
	$page_id = 32; //inputs form page

	if(!is_object($data)){
		if(isset($data[1])){
			$page = $data[1];
		}else{
			$page = $data;
		}
		if(isset($page['load_add_order'])){
			$add = true;
			$form_kind = "form_add_order";
		}else{
			$form_kind = "form_edit_order";
		}
	}else{
		$page = $data;
		$add = false;
		$form_kind = "form_edit_order";
	}

	//Initialize classes
	$db = new Database();
	$form = new Form();
 

?>
<div id="content" class="pages category orders">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_orders'] ?></h1>
	</div>
	<a class="btn btn-success go_back" href="<?= URLROOT.getUrlLang(9) ?>" ><?= $lang['back'] ?></a>
	<?= (isset($add) ? 'Commande' : $page->{'title'.$l}) ?></br>
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post" id="<?= $form_kind ?>" page-id="<?= $page_id ?>"  enctype="multipart/form-data">
					<?php //input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="",$note_position="", $note="", $attributes) ?>
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("first_name", "text", $lang['first_name'], false, (!$add ? $page->first_name : '')).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("last_name", "text",$lang['last_name'], false, (!$add ? $page->last_name : '')).'</div>'; ?>
					<?= '<div class="clear"></div>'; ?>
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("email", "text", $lang['email'], false, (!$add ? $page->email : '')).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("phone", "text",$lang['phone'], false, (!$add ? $page->phone : '')).'</div>'; ?>
					<?= '<div class="clear"></div>'; ?> 
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("address", "text", $lang['address'], false, (!$add ? $page->address : '')).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("city", "text",$lang['city'], false, (!$add ? $page->city : '')).'</div>'; ?>
					<?= '<div class="clear"></div>'; ?> 
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("zipcode", "text", $lang['zipcode'], false, (!$add ? $page->zipcode : '')).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("title", "text",$lang['title'], false, (!$add ? $page->title : '')).'</div>'; ?>
					<?= '<div class="clear"></div>'; ?> 
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("quantity", "text", $lang['quantity'], false, (!$add ? $page->quantity : '')).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("price", "text",$lang['price'], false, (!$add ? $page->price : '')).'</div>'; ?>
					<?= '<div class="clear"></div>'; ?> 
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("texte", "wysiwyg","text", false, (!$add ? $page->texte : '')).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("texte_en", "wysiwyg","text EN", false, (!$add ? $page->texte_en : '')).'</div>'; ?>
					<?= '<div class="clear"></div>'; ?> 
					<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("date_created", "date","Date created", false, (!$add ? $page->date_created : '')).'</div>'; ?>
					<?= '<div class="omega grid_6 grid_1600_12"> </div>'; ?>
					<?= '<div class="clear"></div>'; ?> 
					<?= (!$add ? '<input type="hidden" value="'.$page->id.'" name="orders_id" />' : '') ?>
					<?= '<div class="clear"></div><div class="grid_12 grid_1600_12"><input type="submit" class="btn btn-success" value="'.(!$add ? $lang['edit'] : $lang['add']).'" name="submit" /></div>'; ?>
				</form>
			</div>
		</div>
	</div>
</div>
