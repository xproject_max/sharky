<?php
	require_once APPROOT . "/views/inc/header.php";
	$add = false;
	$db = new Database();
	$form = new Form();
	$page = $_POST;
?>
<div id="content" url="<?= URLROOT."".getUrlLang(8) ?>" class="categories orders">
	<div class="card header-title text-center">
		<h1><?= $lang['menu_orders'] ?></h1>
	</div>
	<?php
	if($_SESSION['status'] < 2){ 
	?>	
	<input type="button" class="btn btn-success new_order" value="<?= $lang['add']; ?>"   />
	<?php
	}
	?>
	<div class="grid_12 grid_1024_12">
		<form method="post" target="_blank" action="<?= URLROOT ?>/ajax.php?export_btn=true" id="form_export"  >
			<p><?= $lang['filter_export'] ?></p>
			<?php //input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="",$note_position="", $note="", $attributes) ?>
			<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("start_date", "text", $lang['date_start'], false,"","","datetimepicker").'</div>'; ?>
			<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("end_date", "text", $lang['date_end'], false,"","","datetimepicker").'</div>'; ?>
			<?= '<div class="clear"></div>'; ?>
			<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("first_name", "text", $lang['first_name'], false).'</div>'; ?>
			<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("last_name", "text", $lang['last_name'], false).'</div>'; ?>
			<?= '<div class="clear"></div>'; ?>
			<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("id", "text", $lang['order_rank'], false).'</div>'; ?>
			<?= '<div class="omega grid_6 grid_1600_12">'.$form->input("email", "text", $lang['email'], false).'</div>'; ?>
			<?= '<div class="clear"></div>'; ?>
			<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("status", "text", $lang['status'], false).'</div>'; ?> 
			<?= '<div class="alpha grid_6 grid_1600_12">'.$form->input("delivery_status", "text", 'Livraison '.$lang['status'], false).'</div>'; ?> 
			<?= '<div class="clear"></div>'; ?>
			<input type="submit" class="btn btn-success" value="Export csv"></input>
			<input type="button" class="btn btn-success filter_orders" value="Search"></input>
		</form>
	</div>
	<div class="clear"></div>  
	<div class="mtop">
		<div class="card-content">
			<div class="table-responsive">
				<form action="" method="post">
					<table class="table" style=" margin: 0 auto;table-layout:auto !important;">
						<thead>
							<tr>
								<th style="padding-right:15px;"><?= $lang['orders_number'] ?></th>
								<th><?= $lang['product_name'] ?></th>
								<th><?= $lang['quantity'] ?></th>
								<th><?= $lang['first_name'] ?></th>
								<th><?= $lang['last_name'] ?></th>
								<th><?= $lang['email'] ?></th>
								<th><?= $lang['price'] ?></th>
								<th><?= $lang['status'] ?></th>
								<th>Livraison <?= $lang['status'] ?></th>
								<th><?= $lang['date'] ?></th>
								<th class="text-right" style="padding-right:15px;"><?= $lang['pages_th_actions'] ?></th>
							</tr>
						</thead>
						<tbody class="result_seek">
						<?php
							try{
								$query_orders = $db->query("
									SELECT o.*, GROUP_CONCAT(DISTINCT p.title,CONCAT(\"<img src='".URLIMG."\",p.image,\"' height='75px'>\"),CONCAT(\"(\",po.position,\")\") SEPARATOR ' --- ') as title,
									u.email as email,
									o.id as id,
									um.first_name as first_name,
									um.last_name as last_name,
									o.status as status,
									d.status as delivery_status,
									o.date_created as date_created
									FROM tbl_orders o
                                    LEFT JOIN tbl_products_orders po ON find_in_set(po.id,o.fk_products)
									LEFT JOIN tbl_products p ON p.id = po.fk_products
									LEFT JOIN tbl_companies c ON c.id = o.fk_delivery
									LEFT JOIN tbl_users u ON u.id = o.fk_users
									LEFT JOIN tbl_users_meta um ON um.id = u.fk_user_meta
									LEFT JOIN tbl_delivery d ON d.fk_orders = o.id 
									WHERE 
				 					o.fk_users is not null
                                    GROUP BY o.id
									ORDER BY o.id DESC
								");
								$result = $db->execute($query_orders);
								$counted = $db->rowCount($query_orders);
								while ($orders = $query_orders->fetch($result)) {

									echo '
										<tr>
											<td>
												<input class="text-center" type="hidden" name="id[]"
												style="text-align:center;width:15px !important;"
												value="'.$orders->id.'">
												<p>
													'.$orders->id.'
												</p>
											</td>
											<td>
												'.$orders->title.''.'
											</td>
											<td>
												<p data-tooltip="'.$orders->quantity.'">'.$orders->quantity.'</p>
											</td>
											<td>
												<p data-tooltip="'.$orders->first_name.'">'.$orders->first_name.'</p>
											</td>
											<td>
												<p data-tooltip="'.$orders->last_name.'">'.$orders->last_name.'</p>
											</td>
											<td>
												<p data-tooltip="'.$orders->email.'">'.$orders->email.'</p>
											</td>
											<td>
												<p data-tooltip="'.$orders->final_price.'">'.$orders->final_price.'</p>
											</td>
											<td>
												<p data-tooltip="'.$orders->status.'">'.$orders->status.'</p>
											</td>
											<td>
												<p data-tooltip="'.$orders->delivery_status.'">'.$orders->delivery_status.'</p>
											</td>
											<td>
												<p data-tooltip="'.$orders->date_created.'">'.$orders->date_created.'</p>
											</td>
											<td class="td-actions text-right">
												<a title="Export" target="_blank" class="btn btn-info" href="'.URLCLIENT.'ajax.php?bill='.base64_encode($orders->id.'||'.$orders->date_created).'">
													<i class="fas fa-file-pdf"></i>
												</a> 												
												<a title="Export" target="_blank" class="btn btn-info" href="'.URLCLIENT.'ajax.php?
													shippingsend=true&email='.$orders->email.'&orderid='.$orders->id.'">
													expédier
												</a> 
											</td>
										</tr>
									';
								}
							}catch(PDOException $e){
								return false;
							}
						?>
						
						</tbody>
					</table></br>

				</form>
			</div>
		</div>
	</div>
</div>
<?php
require_once APPROOT . "/views/inc/footer.php";
?>
