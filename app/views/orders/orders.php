<?php
	//Require headers and lang
	require_once APPROOT . "/views/inc/header.php";
	
	//gather user info and redirect if not logged please.
	if (!isset($_SESSION['user_id_website'])) {
		header('location: ' . getUrlLang(1));
	} else {
		$account = new User();
		$infoAccount = $account->getUserWebsite($_SESSION['user_id_website']);
		if (is_array($infoAccount)) {
			$infoAccount = $infoAccount[0];
		}
	}
?>
<div id="content" class="orders wrapper">
	 
</div>
<?php
	//Require footer
	require_once APPROOT . "/views/inc/footer.php";
?>