<?php
	//Declare $variables;

	include APPROOT . "/config/lang.php";
	require_once APPROOT . "/helpers/function.php";
	// var_dump($data);
	$db = new Database();
	$update_utf = $db->query(
		"
	SET NAMES utf8
	"
	);
	$db->execute($update_utf);
	if (is_object($data)) {
		$page = $data;
		// var_dump($page);
	} else {
		$data = isset($data) ? $data : [];
		$page = (isset($data['page'][0]) ? $data['page'][0] : (isset($data['page']) ? $data['page'] : []));
	}
	if ($language == "en")
		$l = "_en";
	if (!isset($page->id)) {
		$query_404 = $db->query("SELECT * FROM tbl_pages WHERE id = ".__404ID__."");
		$page = $db->single($query_404);
		$join = $db->query("SELECT t.*, i.name FROM tbl_pages_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_pages = :id_page");
		$db->bind($join, ":id_page", $page->id);
		$db->execute($join);
		while ($joins = $db->fetch($join)) {
			if($joins->name != "")
				$page->{$joins->name} = $joins->value;
		}
	}else{
		$join = $db->query("SELECT t.*, i.name FROM tbl_pages_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_pages = :id_page");
		$db->bind($join, ":id_page", $page->id);
		$db->execute($join);
		while ($joins = $db->fetch($join)) {
			if($joins->name != "")
				$page->{$joins->name} = $joins->value;
		}
	}
	$useragent=$_SERVER['HTTP_USER_AGENT'];

	include "top-cache.php";
?>
<!DOCTYPE html>
<html lang="<?= $language ?>">
<head>
	<meta charset="UTF-8">
	<?php
		if (
			$page->id == __DELIVERYPOLICYID__ || $page->id ==  __404ID__ || $page->id ==  __SITEMAPID__ || $page->id == __CONDITIONSID__  || $page->id == __POLITIQUESID__ || $page->id == __THANKYOUID__  || $page->id ==  __CGVSID__.' || $page->id =='.__RETURNPOLICYID__ || $page->id == __PAYMENTFAILID__
		) {
			//rien
		} else {
			?>
			<meta name="description"
			      content="<?= ((substr(rtrim(($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l))))))), -1) == ".") ? ($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l)))))) . "" : ($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l))))))) ?>"
			/>
			<?php
		}
		if (MULTI_LANG) {
			if (isset($_GET['seo'])) {
				if ($l == '') {
					$slug = 'ville';
					$slugInv = 'city';
				} else {
					$slug = 'city';
					$slugInv = 'ville';
				}
				$seo = str_replace('_', ' ', $_GET['seo']);
				$theSEOq = $db->query("SELECT * FROM seo_nom WHERE url_id" . $l . " = :seo");
				$db->bind($theSEOq, ":seo", $seo);
				$theSEO = object_to_array($db->single($theSEOq));
				echo '<link rel="alternate" hreflang="' . $language . '-ca" href="' . rtrim(URLROOT, '/') . '/' . $language . '/' . $slug . '/' . $theSEO['url_id' . $l] . '"/>';
				echo '<link rel="alternate" hreflang="' . $language . '" href="' . rtrim(URLROOT, '/') . '/' . $language . '/' . $slug . '/' . $theSEO['url_id' . $l] . '"/>';
				echo '<link rel="alternate" hreflang="' . $linv . '-ca" href="' . rtrim(URLROOT, '/') . '/' . $linv . '/' . $slugInv . '/' . $theSEO['url_id' . ($l == '' ? '_en' : '')] . '"/>';
				echo '<link rel="alternate" hreflang="' . $linv . '" href="' . rtrim(URLROOT, '/') . '/' . $linv . '/' . $slugInv . '/' . $theSEO['url_id' . ($l == '' ? '_en' : '')] . '"/>';
				echo '<link rel="alternate" hreflang="x-default" href="' . rtrim(URLROOT, '/') . '/' . $language . '/' . $slug . '/' . $theSEO['url_id' . $l] . '"/>';
			} else {

				echo '<link rel="alternate" hreflang="' . $language . '-ca" href="' . rtrim(URLROOT, '/') . (isset($product_page) ? getUrlLangProducts($page->id) : (isset($blog_page) ? getUrlLangBlogs($page->id) : (isset($contest_page) ? getUrlLangContests($page->id) : (isset($promotion_page) ? getUrlLangPromotions($page->id) : getUrlLang($page->id))))) . '"/>';
				// echo '<link rel="alternate" hreflang="'.$language.'" href="'.rtrim(URLROOT, '/').getUrlLang($page->id).'"/>';
				echo '<link rel="alternate" hreflang="' . $linv . '-ca" href="' . rtrim(URLROOT, '/') . (isset($product_page) ? getUrlLangProducts($page->id, $linv) : (isset($blog_page) ? getUrlLangBlogs($page->id, $linv) : (isset($contest_page) ? getUrlLangContests($page->id, $linv) : (isset($promotion_page) ? getUrlLangPromotions($page->id, $linv) : getUrlLang($page->id, $linv))))) . '"/>';
				echo '<link rel="alternate" hreflang="' . $linv . '" href="' . rtrim(URLROOT, '/') . (isset($product_page) ? getUrlLangProducts($page->id, $linv) : (isset($blog_page) ? getUrlLangBlogs($page->id, $linv) : (isset($contest_page) ? getUrlLangContests($page->id, $linv) : (isset($promotion_page) ? getUrlLangPromotions($page->id, $linv) : getUrlLang($page->id, $linv))))) . '"/>';
				echo '<link rel="alternate" hreflang="x-default" href="' . rtrim(URLROOT, '/') . (isset($product_page) ? getUrlLangProducts($page->id) : (isset($blog_page) ? getUrlLangBlogs($page->id) : (isset($contest_page) ? getUrlLangContests($page->id) : (isset($promotion_page) ? getUrlLangPromotions($page->id) : getUrlLang($page->id))))) . '"/>';
			}
		}
	?>
	<?php

		if (
			$page->id == __DELIVERYPOLICYID__ || $page->id ==  __404ID__ || $page->id ==  __SITEMAPID__ || $page->id == __CONDITIONSID__  || $page->id == __POLITIQUESID__ || $page->id == __THANKYOUID__  || $page->id ==  __CGVSID__.' || $page->id =='.__RETURNPOLICYID__ || $page->id == __PAYMENTFAILID__
		) {
			//rien
		} else {
			echo '
			<meta property="og:title" content="' . ($page->{'seo_meta_title' . $l} ? $page->{'seo_meta_title' . $l} : $page->{'title' . $l} . ' | ' . SITENAME) . '"/>';
		}
		echo '
		<meta property="og:url" content="' . URLROOT . ltrim(
				(isset($product_page) ? getUrlLangProducts($page->id) : (isset($blog_page) ? getUrlLangBlogs($page->id) : (isset($contest_page) ? getUrlLangContests($page->id) : (isset($promotion_page) ? getUrlLangPromotions($page->id) : getUrlLang($page->id))))), '/'
			) . '"/>
			<meta property="og:type" content="website"/>
			';
		if ($page->image !== '') {
			echo '
				<meta property="og:image" content="' . DragonHide(URLROOT . 'image.php?path=' . $page->image) . '"/>
				';
		} elseif (file_exists(DragonHide(URLROOT . 'image.php?path=' . 'images/logo_share_fb.png'))) {
			echo '
				<meta property="og:image" content="' . DragonHide(URLROOT . 'image.php?path=' . 'images/logo_share_fb.png') . '"/>
				';
		} else {
			echo '
				<meta property="og:image" content="' . URLROOT . 'images/logos/logo.png" />

				';
		}
		if (
			$page->id == __DELIVERYPOLICYID__ || $page->id ==  __404ID__ || $page->id ==  __SITEMAPID__ || $page->id == __CONDITIONSID__  || $page->id == __POLITIQUESID__ || $page->id == __THANKYOUID__  || $page->id ==  __CGVSID__.' || $page->id =='.__RETURNPOLICYID__ || $page->id == __PAYMENTFAILID__
		) {
			//rien
		} else {
			if (constant("__SEO_META__".$l) != '') {
				echo '
					<meta property="og:description" content="' . ((substr(rtrim(CUT($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l))))), 158)), -1) == ".") ? CUT($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l))))), 158) . "" : CUT($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l))))), 158) . "...") . '"/>
					';
			} else {
				echo '
					<meta property="og:description" content="' . ((substr(rtrim(CUT($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l))))), 158)), -1) == ".") ? CUT($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l))))), 158) . "" : CUT($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l))))), 158) . "...") . '"/>
					';
			}
		}
		echo '
			<meta property="og:site_name" content="' . SITENAME . '" />
			<meta property="fb:app_id" content="" />
			<meta property="og:image:width" content="500" />
			<meta property="og:image:height" content="500" />
			<meta name="twitter:card" content="summary" />
			';
	?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?= ucfirst($page->{'title' . $l}) . " | " . ($l == "" ? SITENAME : 'Dragon Media') ?></title>
 
	<script async src="https://cdn.ampproject.org/v0.js"></script>
	<!-- You can use the `amp-bind` extension for dynamically changing an image. -->
	<?php 
	
		if(isset($page->critical)  && $page->critical !== "") {
			echo '<style>'.$page->critical.'</style>';
		}else{
	?>
	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script> 
	<script rel="preconnect" defer src="https://kit.fontawesome.com/265d503de0.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" async href="<?= URLROOT ?>css/style.min.css?v=2" /> 
	<link rel="stylesheet" href="<?= URLROOT ?>plugins/fancybox/dist/jquery.fancybox.min.css?v=2" /> 
	<link rel="stylesheet" href="<?= URLROOT ?>plugins/slick-master/slick/slick.css?v=2" /> 
	<link defer rel='stylesheet' type='text/css'  href="<?= __FONT1__ ?>"/>
	<link defer rel='stylesheet' type='text/css' href="<?= __FONT2__ ?>"/> 
	<link rel="preconnect" defer  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" integrity="sha512-Fo3rlrZj/k7ujTnHg4CGR2D7kSs0v4LLanw2qksYuRlEzO+tcaEPQogQ0KaoGN26/zrn20ImR1DfuLWnOo7aBA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	 <?php 
		}
	?>
	<?php
		$address = '
			"address": {
				"@type": "PostalAddress",
				"streetAddress": "'.__ADDRESS__.'",
				"addressLocality": "'.__CITY__.'",
				"addressRegion": "QC",
				"postalCode": "'.__POSTAL_CODE__.'",
				"addressCountry": "CA"
			}
			';
	?>
	<script type="application/ld+json">
	{
	  "@context": "http://schema.org",
	  "@type": "BreadcrumbList",
	  "itemListElement": [

			{
				"@type": "ListItem",
				"position": 1,
				"item": {
					"@type": "WebSite",
					"@id": "<?= URLROOT ?>",
					"name": "<?= SITENAME ?>"
				}
			}
			<?php
				if(
				$page->id !=  __HOMEID__  &&
				$page->id !=  __404ID__  &&
				$page->id !=  __SITEMAPID__  &&
				$page->id !=  __CONDITIONSID__  &&
				$page->id !=  __POLITIQUESID__  &&
				$page->id !=  __THANKYOUID__  &&
				$page->id !=  __PRODUCTSID__  &&
				$page->id !=  __CHECKOUTID__  &&
				$page->id !=  __DELIVERYPOLICYID__  &&
				$page->id !=  __RETURNPOLICYID__  &&
				$page->id !=  __PAYMENTFAILID__  &&
				$page->id !=  __RECOVERYACCOUNTID__  &&
				$page->id !=  __MAINTENANCEID__  &&
				$page->id !=  __ACTIVATIONACCOUNTID__  &&
				$page->id !=  __APRODUCT__  &&
				$page->id !=  __PAYMENTTHANKYOUID__  &&
				$page->id !=  __ANARTICLE__  &&
				$page->id !=  __APROMO__  &&
				$page->id !=  __PROMOSID__  &&
				$page->id !=  __ACONTEST__  &&
				$page->id !=  __AREALISATION__  &&
				$page->id !=  __ASUBSCRIPTION__  &&
				$page->id !=  __CATEGCONTESTS__  &&
				$page->id !=  __CATEGPROMOS__  &&
				$page->id !=  __CATEGARTICLES__  &&
				$page->id !=  __CATEGPRODUCTS__  &&
				$page->id !=  __CAROUSELID__  &&
				$page->id !=  __ASLIDEID__  &&
				$page->id !=  __BFULLTEXTID__  &&
				$page->id !=  __BFULLIMGID__  &&
				$page->id !=  __BTEXTIMGID__  &&
				$page->id !=  __BDOUBLETEXTID__  &&
				$page->id !=  __BDOUBLEIMGID__  &&
				$page->id !=  __BVIDEOID__  &&
				$page->id !=  __BTEXTVIDEOID__  &&
				$page->id !=  __ORDERSID__  &&
				$page->id !=  __USERAREAID__  &&
				$page->id !=  __LOGINAREAID__  &&
				$page->id !=  __AQUESTIONID__  &&
				$page->id !=  __CGVSID__ ){

					echo ',
					{
						"@type": "ListItem",
						"position": 2,
						"item": {
							"@type": "WebPage",
							"@id": "'.rtrim(URLROOT,"/").getUrlLang($page->id).'",
							"name": "'.$page->{'title'.$l}.'"
						}
					}';
				}
			?>
		]
	}
	</script>
	<script type="application/ld+json">
			{
				"@context": "http://schema.org",
				"@type": "Organization",
				<?= $address ?>,
				"contactPoint" : [
					{
						"@type" : "ContactPoint",
						"telephone" : "+1-<?= str_replace(array('.', ' '), '-', str_replace(array('(', ')'), '', CLIENT_PHONE)) ?>",
						"contactType" : "customer service",
						"email": "<?= CLIENT_EMAIL ?>"
					}
				],
				"name": "<?= SITENAME ?>",
				"url": "<?= URLROOT ?>",
				"telephone": "<?= CLIENT_PHONE ?>",
				"description": "
				<?php
			if (
				$page->id == __DELIVERYPOLICYID__ || $page->id ==  __404ID__ || $page->id ==  __SITEMAPID__ || $page->id == __CONDITIONSID__  || $page->id == __POLITIQUESID__ || $page->id == __THANKYOUID__  || $page->id ==  __CGVSID__.' || $page->id =='.__RETURNPOLICYID__ || $page->id == __PAYMENTFAILID__
			) {
				//rien
			} else {
				?>
				<?= ((substr(rtrim(($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l))))))), -1) == ".") ? ($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l)))))) . "" : ($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l))))))) ?>
				<?php
			} ?>",
				"email": "<?= CLIENT_EMAIL ?>"
			}


	</script>
	<script type="application/ld+json">
			{
				"@context": "http://schema.org",
				"@type": "LocalBusiness",
				<?= $address ?>,
				"contactPoint" : [
					{
						"@type" : "ContactPoint",
						"telephone" : "+1-<?= str_replace(array('.', ' '), '-', str_replace(array('(', ')'), '', CLIENT_PHONE)) ?>",
						"contactType" : "customer service",
						"email": "<?= CLIENT_EMAIL ?>"
					}
				],
				"name": "<?= SITENAME ?>",
				"url": "<?= URLROOT ?>",
				"telephone": "<?= CLIENT_PHONE ?>",
				"description": "
				<?php
			if (
				$page->id == __DELIVERYPOLICYID__ || $page->id ==  __404ID__ || $page->id ==  __SITEMAPID__ || $page->id == __CONDITIONSID__  || $page->id == __POLITIQUESID__ || $page->id == __THANKYOUID__  || $page->id ==  __CGVSID__.' || $page->id =='.__RETURNPOLICYID__ || $page->id == __PAYMENTFAILID__
			) {
				//rien
			} else {
				?>
				<?= ((substr(rtrim(($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l))))))), -1) == ".") ? ($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l)))))) . "" : ($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l))))))) ?>
				<?php
			} ?>",
				"email": "<?= CLIENT_EMAIL ?>",
				"image": "<?= URLROOT ?>images/logos/logo.png"
			}


	</script>

	<script type="application/ld+json">
			{
				"@context": "http://schema.org",
				"@type": "Website",
				"name": "<?= SITENAME ?>",
				"url": "<?= URLROOT ?>",
				"description": "
				<?php
			if (
				$page->id == __DELIVERYPOLICYID__ || $page->id ==  __404ID__ || $page->id ==  __SITEMAPID__ || $page->id == __CONDITIONSID__  || $page->id == __POLITIQUESID__ || $page->id == __THANKYOUID__  || $page->id ==  __CGVSID__.' || $page->id =='.__RETURNPOLICYID__ || $page->id == __PAYMENTFAILID__
			) {
				//rien
			} else {
				?>
				<?= ((substr(rtrim(($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l))))))), -1) == ".") ? ($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l)))))) . "" : ($page->{'seo_meta_description' . $l} ? $page->{'seo_meta_description' . $l} : strip_tags(str_replace('&nbsp;', ' ', str_replace('&#__MAINTENANCEID__;', "'", html_entity_decode(constant("__SEO_META__".$l))))))) ?>
				<?php
			} ?>",
				"image": "<?= URLROOT ?>images/logos/logo.png"
			}


	</script> 
</head>
<body data-barba="wrapper" data-scroll-container>
	<div data-barba="container" >

<?php include APPROOT . '/views/inc/navbar.php' ?>
