
	<div class="container container_page smooth-scroll"  >	
		<div   id="nav-menu"  data-scroll-section >
			<div class="nav-wrapper">
				<div class="nav-logo-wrapper">
				
					<div class="nav-hamb-wrapper"> 	
						
						<div class="middle_logo">
							<a href="<?= URLROOT.getUrlLang(0) ?>" data-barba-prevent="self" class="hover-target">
								<img src="<?=(file_exists(URLROOT . 'images/logo/logo.webp') ? 
								URLROOT . 'images/logo/logo.webp' : URLROOT . 'images/logo/logo.png')  ?>"  height="80px">
							</a>
						</div>
						<div class="hambdiv">
							<div class="table-cell">
						
								<div class="container toggler hambWrapper hover-target" onclick="jQuery('.ham4').toggleClass('active');jQuery('body').toggleClass('nav-active');">
									 
										<?php 
											//if(file_exists(URLROOT."images/menu.svg")){
												echo file_get_contents(URLROOT."images/menu.svg"); 
											/*}else{
												echo "menus";
											}*/
										?>
									
									<div class=" hover-target"  onclick="jQuery('.ham4').toggleClass('active');jQuery('body').toggleClass('nav-active');">
									</div>			
								</div>	
							</div>	
						</div>	
						 <!--a class="container menu-icon hover-target lang" href="<?= URLROOT.
							($page->id == 1 ? getUrlLang(1, $linv) :
							(isset($product_page) ? getUrlLangProducts($page->id, $linv) :
							 (isset($blog_page) ? getUrlLangBlogs($page->id, $linv) :
							 (isset($contest_page) ? getUrlLangContests($page->id, $linv) :
							 (isset($promotion_page) ? getUrlLangPromotions($page->id, $linv) :
							 getUrlLang($page->id, $linv)))))) ?>">
							<div><div class="menu_table"><div class="menu_table_cell"><?= $linv ?></div></div></div>
						</a-->
						 
						<a href="<?= URLROOT.getUrlLang(__CONTACTID__) ?>" class="services_nav container menu-icon hover-target"  >
							<div class="menu_table">
								<div class="menu_table_cell"> 
									<div class="submission_btn">Faire une soumission</div>		
								</div>		
							</div>		
						</a>
										
					</div>	
				</div>
								
			</div>	
		</div>		 
		<progress  id="scrollProgress" max="100" value="0"></progress>	
		<?php if(!isset($page->headerImage)){
			$page->headerImage = "";
		}?>
		<div   id="menu-fullscreen">
			<div class="menu-content">
				<div class="table-cell">
					<div class="bigText">
						SHARKY
					</div>
					<div class="whiteBg">
						<div class="mediasociaux">
							<ul>
								<li><a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
							</ul>
						</div>
						<ul class="nav__list">
							<li class="nav__list-item active-nav"><a href="<?= URLROOT.getUrlLang(0) ?>" data-barba-prevent="self" class="hover-target"><?= $lang['menu_home'] ?></a></li> 
							<li class="nav__list-item"><a href="<?= URLROOT.getUrlLang(71) ?>" class="hover-target">Calculateur de béton</a></li>
							<li class="nav__list-item"><a href="<?= URLROOT.getUrlLang(__SERVICESID__) ?>" class="hover-target">Nos produits</a></li>
							<li class="nav__list-item"><a href="<?= URLROOT.getUrlLang(__REALISATIONSID__) ?>" class="hover-target">Nos réalisations</a></li>
							<li class="nav__list-item"><a href="<?= URLROOT.getUrlLang(__ABOUTID__) ?>" class="hover-target">Notre entreprise</a></li>
							<li class="nav__list-item"><a href="<?= URLROOT.getUrlLang(__CONTACTID__) ?>" class="hover-target"><?= $lang['menu_contact'] ?></a></li> 
						</ul>  
					</div>
				</div>
				<img  src="<?=(file_exists(URLROOT . 'upload/'.SITENAME.'/'.$page->headerImage.'~l=1920') ? 	URLROOT . 'upload/'.SITENAME.'/'.$page->headerImage.'~l=1920' : URLROOT . 'images/shark.png') ?>" class="imgSharkMenu menu-cup-wine" /> 
			</div>
		</div>	 	
		<div   data-scroll-section data-scroll id="header" style="min-height: 500px" <?=($page->id != 1 ? 'class="not_home"' : '' ) ?>>
			<div class="wrapper_logo_header">
				<div class="table">
					<div class="table-cell">

						<div class="header-logo">
							<?php if($page->id == 1){ ?>
							<!--image  data-src="<?=URLROOT . 'images/logo/10.png' ?>" src="<?=URLROOT . 'images/logo/10.png' ?>" class="lazyload"  height="500" width="500"  alt="logo-header"-->
							<h1 class="mainTitleSharky">Sharky</h1>
							<span class="bgSharky">Sharky</span>
							<div class="bottomContent">
								<h2 class="suptitle"><?= $page->{'subtitle'.$l} ?></h2>
								<a href="<?= URLROOT.getUrlLang(4) ?>" class="btn-sharky">Voir davantage</a>
							</div>
							<?php } else { ?>
							<div class="header_title">
								<h1 class="h1"><?=$page->{'title'.$l} ?></h1> 
							</div> 
							<?php } ?>
						</div>
						
					</div>
				</div>
			</div>
			<?php
				if($page->type == 39){
					
					$getProductHeader = $db->query("SELECT * FROM tbl_pages WHERE id = 5");
					$page->headerImage = $db->single($getProductHeader)->image;
				}else{
					$page->headerImage = $page->image;
				}
			?>
			<amp-img  layout="responsive" width="1920px" height="800" style="/* object-fit:cover; */" 
			class="bg-header lazyload" alt="background-header"
			data-src="<?=  URLROOT . 'images/bg.jpg~l=1920'  ?>" src="<?=  URLROOT . 'images/bg.jpg~l=1920'  ?>">
			</amp-img> 
			  
			
		</div>  