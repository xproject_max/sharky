 
	<div data-scroll-section  data-scroll class="footer footer-black footer-big" > 
		<div class="container"   data-scroll> 
			<div class="main_footer">
				<div class="wrapper_footer_main">
					<div class="toppart_footer">
						<div class="left_part_footer">
							<ul>
								<li><a href="<?= URLROOT.getUrlLang(2) ?>">À propos de nous</a></li>
								<li><a href="<?= URLROOT.getUrlLang(4) ?>">Nos produits</a></li>
							</ul>
						</div>
						<div class="middle_part_footer">
							<div class="logo_wrapper">
								<img src="<?= URLROOT.'images/logo/logo.png'?>" width="250" />
							</div>
						</div>
						<div class="right_part_footer">
							<ul>
								<li><a href="<?= URLROOT.getUrlLang(71) ?>">Calculateur de béton</a></li>
								<li><a href="<?= URLROOT.getUrlLang(3) ?>">Nous joindre</a></li>
							</ul>
						</div>
					</div>
					<div class="bar_footer"></div>
					<div class="bottompart_footer">
						<div class="socialpart">
							<ul>
								<li><a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
							</ul>
						</div>
					</div>
							
				</div>
			</div>
			<div  class="copyright"    data-scroll >
				<div class="links pull-right">
					<ul>
						<li>
							<a href="<?= URLROOT.getUrlLang(__SITEMAPID__) ?>">
								<?= $lang['sitemap'] ?>
							</a>
						</li>
						<li>
							<a  href="<?= URLROOT.getUrlLang(__CONDITIONSID__) ?>">
								<?= $lang['conditions'] ?>
							</a>
						</li>
						<li>
							<a  href="<?= URLROOT.getUrlLang(__POLITIQUESID__) ?>">
								<?= $lang['privacy_policy'] ?>
							</a>
						</li>
						 
					</ul>
				</div>
			</div>
			<div   class="dragon text-center"   data-scroll>
				<div class="table-cell-footer">
				© <?= date('Y') . " " . SITENAME ?> | Conception web par Dragon Média inc. 
				</div>
				<div class="table-cell-footer">
				<img layout="responsive" width="50" height="50"  
				class="logo-footer lazyload" alt="logo footer"
				data-src="<?=(file_exists(URLROOT . 'images/logo/5.webp') ? 
				URLROOT . 'images/logo/5.webp' : URLROOT . 'images/logo/5.png')  ?>"
				src="<?=(file_exists(URLROOT . 'images/5.webp') ?
				URLROOT . 'images/logo/5.webp': URLROOT . 'images/logo/5.png') ?>" >
				</img> 
				</div>
				<div class="table-cell-footer">
				| <a target="_blank" data-barba-prevent='self' href="<?= URLROOT ?>administration/">Administration</a>
				</div>
			</div>
		</div>
	</div>
</div>
</div> 
<div id="url_scripts" data-url-scripts="<?= URLROOT ?>"></div>
<div id="url_cart" data-url-cart="<?= URLROOT . getUrlLang(__CARTID__) ?>"></div> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
 <script defer src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/gsap.min.js"></script>
<script src="<?= URLROOT ?>plugins/fancybox/dist/jquery.fancybox.js?v=2" ></script>
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/ScrollTrigger.min.js"></script> 
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/ScrollToPlugin.min.js"></script>
<script type="text/javascript" src="<?= URLROOT ?>js/third-parties/wow.min.js"></script>  
<script type="text/javascript" src="<?= URLROOT ?>js/menu.js?v=2"></script>  
<script defer type="text/javascript" src="<?= URLROOT ?>js/scripts.min.js?v=2"></script>  
<script defer type="text/javascript" src="https://cdn.jsdelivr.net/npm/@barba/core"></script> 
<!-- IE -->
<script defer type="text/javascript" rel="preconnect" nomodule src="https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/7.6.0/polyfill.min.js" crossorigin="anonymous"></script>
<script defer type="text/javascript" rel="preconnect" nomodule src="https://polyfill.io/v3/polyfill.min.js?features=Object.assign%2CElement.prototype.append%2CNodeList.prototype.forEach%2CCustomEvent%2Csmoothscroll" crossorigin="anonymous"></script>

<script type="text/javascript" rel="preconnect" src="https://cdn.jsdelivr.net/npm/locomotive-scroll@4.1.3/dist/locomotive-scroll.min.js"></script>
<script type="text/javascript" src="<?= URLROOT ?>js/third-parties/lazysizes.min.js" async></script> 
  
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>
<script src="<?= URLROOT ?>plugins/DistortionHoverEffect/js/imagesloaded.pkgd.min.js"></script>
<script src="<?= URLROOT ?>plugins/DistortionHoverEffect/js/three.min.js"></script>
<script src="<?= URLROOT ?>plugins/DistortionHoverEffect/js/hover.js"></script>
<script src="<?= URLROOT ?>plugins/slick-master/slick/slick.min.js?v=2" ></script> 
 
<script type="module">
import { gsap } from '<?= URLROOT ?>plugins/gsap/all.js'; 
</script>
<script>
 
    $(document).ready(function () {  
	$('.gallerylink').each(function(e){  
		$(this).fancybox();
	});
	gsap.registerPlugin(MorphSVGPlugin);
	gsap.registerPlugin(gsap);
	  
		CLIENT.initialize(); 
		MENU.initialize();    
		<?php if($page->id !== 1){ ?>
		barba.init({
			views: [{
				beforeEnter(data) { 
					CLIENT.initialize(); 
					MENU.initialize();
				}
			}],
		  transitions: [{
			sync: true,
			name: 'opacity-transition',
			leave(data) {  
				return gsap.to(data.current.container, {
				opacity: 0.9
				}); 
			},
			enter(data) {   
			  return gsap.from(data.next.container, {
				opacity: 0.1
			  });
			  
			}
		  }]
		}); 
		<?php } ?>
		
		
		/*
		$('#slick1').slick({
			rows: 2,
			dots: false,
			arrows: true,
			infinite: true,
			autoplay: true,
			speed: 300,
			slidesToShow: 1,
			slidesToScroll: 1,
			centerMode: true,
			centerPadding: '600px', 
			responsive: [
			{
				breakpoint: 768,
				rows:1, 
				arrows: false,
				centerMode: false,
				centerPadding: '0px',
				slidesToShow: 1 
			},
			{
				breakpoint: 480,
				rows:1, 
				arrows: false,
				centerMode: false,
				centerPadding: '0px',
				slidesToShow: 1 
			}
		  ]
		});
		*/
		
		 function resetSlick($slick_slider, settings) {
            $(window).on('resize', function() {
                if ($(window).width() < 320) {
                    if ($slick_slider.hasClass('slick-initialized')) {
                        $slick_slider.slick('unslick');
                    }
                    return
                }

                if (!$slick_slider.hasClass('slick-initialized')) {
                    return $slick_slider.slick(settings);
                }
            });
        }

		slick_slider = $('#slick1');
		settings = {
			rows: 2,
			slidesToShow: 3,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2000,  
			pauseOnHover: true,

			responsive: [
				{
					breakpoint: 1024,
					settings: {
						rows: 1,
						slidesToShow: 1,
						slidesToScroll: 1, 
					}
				}, 
				{
					breakpoint: 480,
					settings: {
						rows: 1,
						slidesToShow: 1,
						slidesToScroll: 1, 
					}
				}
				// You can unslick at a given breakpoint now by adding:
				// settings: "unslick"
				// instead of a settings object
			]

		};
		
		slick_slider.slick(settings);
		resetSlick(slick_slider, settings);
	
					
    });
</script>

					
 <?php
	unset($_SESSION['xd']);
	unset($_SESSION['mdsp']);
	unset($_SESSION['ppl_products']);
	unset($_SESSION['ppl_charges']);
	unset($_SESSION['secure_post']);
	unset($_SESSION['orderID']);
	unset($_SESSION['idn']);
	unset($_SESSION['ItemDesc']);
	unset($_SESSION['cart_id']);
	unset($_SESSION['postedData']);
	unset($_SESSION['idT']);
	unset($_SESSION['passwordTemp']);
	unset($_SESSION['user_id']);
	unset($_SESSION['first_name_account']);
	unset($_POST);
	include "bottom-cache.php";
?>
</body>
</html>