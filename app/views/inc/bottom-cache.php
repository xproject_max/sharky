<?php
	// Cache the contents to a cache file 
	
		if (( (file_exists($cachefile) && ((time() - $cachetime) < filemtime($cachefile)))
		|| ($page->edit_ts != $page->last_edit) || is_null($page->last_edit) || ($lastversion > $page->last_edit)
		 )) { 
		
			if($page->type == 40 || $page->type == 48){ // BLOG 
		
				$update_last_edit = $db->query("
					UPDATE tbl_blogs SET last_edit = :edit WHERE id = :id
				");
			}else if($page->type == 39 || $page->type == 49){ //PRODUCTS
				
				$update_last_edit = $db->query("
					UPDATE tbl_products SET last_edit = :edit WHERE id = :id
				");
			}else if($page->type ==  41 || $page->type == 47){ //PROMOS
				
				$update_last_edit = $db->query("
					UPDATE tbl_promotions SET last_edit = :edit WHERE id = :id
				");
			}else if ($page->type == 46 || $page->type == 42){ //CONTESTS
			
				$update_last_edit = $db->query("
					UPDATE tbl_contests SET last_edit = :edit WHERE id = :id
				");
			}else{
				$update_last_edit = $db->query("
					UPDATE tbl_pages SET last_edit = :edit WHERE id = :id
				");
			}
			$db->bind($update_last_edit, ":edit", $lastversion);
			$db->bind($update_last_edit, ":id", $page->id);
			$db->execute($update_last_edit);
			
			$url = $_GET['url'];  
			$newname = 'cache/cached-' . $file . "-".$l . '-'. (!is_mobile($useragent) ? 'desktop' : 'mobile') . '-'.$lastversion.'.html'; 
			$cached = fopen($cachefile, 'w');
			fwrite($cached, ob_get_contents());
			fclose($cached);
			ob_end_flush(); // Send the output to the browser  
				
		}
?>