<?php
	//Require headers and lang
	require_once APPROOT . "/views/inc/header.php"; 
?>
<div id="content" data-scroll-section   data-barba-namespace="contact"  class="contact">
<div class="wrapper_contact">
 <?php
		
		$query_get_alls = $db->query("SELECT * ,tbl_pages_inputs.value as value, i.name as name FROM tbl_pages_inputs LEFT JOIN inputs i ON i.id = tbl_pages_inputs.type WHERE fk_pages = :id");
		
		$db->bind($query_get_alls, ":id", $page->id);
		$db->execute($query_get_alls);
		while ($cust = $db->fetch($query_get_alls)) {
			$page->{$cust->name} = $cust->value;
		}
		echo '
		<div class="table-custom section "
			style="width: 100%;/*background: url(' . URLROOT . 'image.php?path=' . $page->image . '~l=1920);*/ background-size: cover;
			background-position: center;
			background-attachment:fixed;
			background-repeat: no-repeat;">
			<div class="alpha table-cell-custom grid_6 grid_1024_12 fadeIn wow" data-wow-duration="1s" data-wow-delay="0s">
				<div class=" "> 
					<div class="square">
					<div > 
						<h4 class="wrap-h2">Nos informations</h4>
						<div class="orange-bar"></div> 
						<span class="suptitle">'.$page->{'suptitle'.$l}.'</span>
					</div>
							' . $page->{'texte' . $l} . '
					</div>
					<div class="info_phone">
					<div class="row">
						<div class="table-cell">
							<span>Téléphone  :</span>
						</div>
						<div class="table-cell">
							<span>'.$page->phone.'</span>
						</div>
					</div>
					<div class="row">
						<div class="table-cell">
							<span>Courriel :</span>
						</div>
						<div class="table-cell">
							<span>projetsharky@gmail.com</span>
						</div>
					</div>
					<div class="row">
						<div class="table-cell">
							<span>Nos résaux sociaux :</span>
						</div>
						<div class="table-cell">
							<div class="socialpart">
								<ul>
									<li><a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					</div>
				</div>
				<div class=" "> 
					<div class="square">
					<div >   
						<span class="suptitle">Notre horaire</span>
					</div>
							' . $page->{'texte' . $l} . '
					</div>
					<div class="info_hours">
					';
					
					$arrayHours = $page->{'opening_hours'.$l};
					$arrayHours = explode("&&", $arrayHours);
					foreach($arrayHours as $rowHours){
						if($rowHours !== ""){
							$array= explode("|", $rowHours);
							$day = $array[0];
							$time = $array[1];
							echo'
							<div class="row">
								<div class="table-cell">
									<span>'.$day.':</span>
								</div>
								<div class="table-cell">
									<span>'.$time.'</span>
								</div>
							</div>';
						}
					}
					 echo '
					</div>
				</div>
			</div>
			<div class="omega about_section table-cell-custom grid_6 grid_1024_12  fadeIn wow" data-wow-duration="1s" data-wow-delay="0s">
				<div class="div_wrap_about" style="background: url(' . URLROOT . 'image.php?path=' . $page->image . '~l=1920) center; background-size: cover;background-attachment: scroll;background-position: center;">
					<img class="about_img" src="' . URLROOT . 'image.php?path=' . $page->image . '~l=1920" alt="' . $page->alt . '" >
				</div>
			</div>
		</div> 
			';
	
	?>
</div>
 <section id="section_contact" data-scroll>
		 
			<div class="wrapper_contact">
				<div class="form_contact">
					<form id="contactUs" name="contactusform" action="" method="post">
						<div class="topheadform">
							<div class="square">
								<?php
									$queryContact = $db->query('SELECT * FROM tbl_pages WHERE id = :id');
									$db->bind($queryContact, ':id', __CONTACTID__);
									$db->execute($queryContact);
									$sectionContact = $db->single($queryContact);
									$join = $db->query("SELECT t.*, i.name FROM tbl_pages_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_pages = :id_page");
									$db->bind($join, ":id_page", $sectionContact->id); 
									$db->execute($join); 
									while ($joins = $db->fetch($join)) {
										if($joins->name != "")
											$sectionContact->{$joins->name} = $joins->value;
									}
								?>
								<h2><?= $sectionContact->{'title'.$l} ?></h2>
								<div class="orange-bar"></div>
								<span class="suptitle"><?= $sectionContact->{'suptitle'.$l} ?></span>
							</div>
						</div>
						<div class="mainform">
						<?php
							$form = new Form();  
							//input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="",$note_position="", $note="", $attributes) ?>
							<div class="alpha grid_6 grid_1024_12"><?= $form->input("first_name", "text", $lang['first_name'], true, (isset($data['secure_post']) ? $data['secure_post']['first_name']['value'] : ""), (isset($data['secure_post']) ? $data['secure_post']['first_name']['message_error'] : ""), " " . (isset($data['secure_post']) ? $data['secure_post']['first_name']['message_error'] !== "" ? "is-invalid" : "is-valid" : "")) ?></div>
							<div class="grid_6 grid_1024_12 omega"><?= $form->input("email", "text", $lang['form_email'], true, (isset($data['secure_post']) ? $data['secure_post']['email']['value'] : ""), (isset($data['secure_post']) ? $data['secure_post']['email']['message_error'] : ""), " " . (isset($data['secure_post']) ? $data['secure_post']['email']['message_error'] !== "" ? "is-invalid" : "is-valid" : "")) ?></div>
							<div class="alpha grid_6 grid_1024_12"><?= $form->input("last_name", "text", $lang['last_name'], true, (isset($data['secure_post']) ? $data['secure_post']['last_name']['value'] : ""), (isset($data['secure_post']) ? $data['secure_post']['last_name']['message_error'] : ""), " " . (isset($data['secure_post']) ? $data['secure_post']['last_name']['message_error'] !== "" ? "is-invalid" : "is-valid" : "")) ?></div>
							<div class="grid_6 grid_1024_12 omega"><?= $form->input("phone", "text", $lang['phone_number'], true, (isset($data['secure_post']) ? $data['secure_post']['phone']['value'] : ""), (isset($data['secure_post']) ? $data['secure_post']['phone']['message_error'] : ""), " " . (isset($data['secure_post']) ? $data['secure_post']['phone']['message_error'] !== "" ? "is-invalid" : "is-valid" : "")); ?></div>
							<div class="delta grid_12 grid_1024_12"><?= $form->input("message", "textarea", $lang['form_message'], true, (isset($data['secure_post']) ? $data['secure_post']['message']['value'] : ""), (isset($data['secure_post']) ? $data['secure_post']['message']['message_error'] : ""), " " . (isset($data['secure_post']) ? $data['secure_post']['message']['message_error'] !== "" ? "is-invalid" : "is-valid" : "")); ?></div>
							<div class="clear"></div>
							<input type="hidden" name="contact_form" value="1">
							<input type="submit" value="Faire une soumission" class="myBtn">
							
							<?php
								unset($_POST);
								unset($data['secure_post']);
							?>
						</div>
					</form>
				</div>
			</div>
		 </section>
</div>
<?php
	//Require footer
	require_once APPROOT . "/views/inc/footer.php";
?>