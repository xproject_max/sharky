<?php
	//Require headers and lang
	require_once APPROOT . "/views/inc/header.php";
?>
<div id="content" data-scroll-section   data-barba-namespace="services"  class="services">
	<div class="rectangle_filter">
		<div class="wrapper_rectangle">
			<div class="filters_rectangle">
				<a data-barba-prevent="self"  href="#section_0">
				<div class="table">
					<div class="table-cell">
						<img src="<?= URLROOT."images/processus.png"?>" height="50" alt="processus" />
					</div>
					<div class="table-cell">
						Notre processus
					</div>
				</div>
				</a>
			</div>
			<div class="filters_rectangle">
				<a data-barba-prevent="self"  href="#section_56">
				<div class="table">
					<div class="table-cell">
						<img src="<?= URLROOT."images/bet.png"?>" height="50" alt="beton" />
					</div>
					<div class="table-cell">
						Notre béton
					</div>
				</div>
				</a>
			</div>
			<div class="filters_rectangle">
				<a data-barba-prevent="self"  href="<?= URLROOT.getUrlLang($page->id) ?>#section_57">
				<div class="table">
					<div class="table-cell">
						<img src="<?= URLROOT."images/coff.png"?>" height="50" alt="coffrage" />
					</div>
					<div class="table-cell">
						coffrage
					</div>
				</div>
				</a>
			</div> 
			<div class="filters_rectangle">
				<a data-barba-prevent="self"  href="#section_58">
				<div class="table">
					<div class="table-cell">
						<img src="<?= URLROOT."images/acrylique.png"?>" height="50" alt="acrylique" />
					</div>
					<div class="table-cell">
						acrylique
					</div>
				</div>
				</a>
			</div>
		</div>
	</div>
	<div class="wrapper_content">
   <?php
		
		$query_get_alls = $db->query("SELECT * ,tbl_pages_inputs.value as value, i.name as name FROM tbl_pages_inputs LEFT JOIN inputs i ON i.id = tbl_pages_inputs.type WHERE fk_pages = :id");
		
		$db->bind($query_get_alls, ":id", $page->id);
		$db->execute($query_get_alls);
		while ($cust = $db->fetch($query_get_alls)) {
			$page->{$cust->name} = $cust->value;
		}
		echo '
		<div id="section_0" class="table-custom section "
			style="background-size: cover;
			background-position: center;
			background-attachment:fixed;
			background-repeat: no-repeat;">
			<div class="alpha table-cell-custom grid_6 grid_1024_12 fadeIn wow" data-wow-duration="1s" data-wow-delay="0s">
				<div class="square">
					<div > 
						<h4 class="wrap-h2">Notre processus</h4>
						<div class="orange-bar"></div> 
						
					</div>
				</div>
				' . $page->{'texte' . $l} . '
			</div>
			<div class="omega about_section table-cell-custom grid_6 grid_1024_12  fadeIn wow" data-wow-duration="1s" data-wow-delay="0s">
				<div class="timeline">
					<span class="suptitle">'.$page->{'suptitle' . $l}.'</span> <br>
					<div class="wrapper">
					';
		$queryEvents = $db->query("
			SELECT * FROM tbl_pages WHERE parent = :parent
		");
		$db->bind($queryEvents, ":parent", 66);
		$db->execute($queryEvents);
		if($db->rowCount($queryEvents) > 0){
			$i = 1;
			while($event = $db->fetch($queryEvents)){
				
				echo '
				<div class="events">
					<div class="wrapper_events">
						<span class="number">'.($i < 10 ? $i = "0".$i : $i = $i).'</span>
						<span class="title">'.$event->{'title'.$l}.'</span>
						<div class="wrapper_text">
							<div class="float_right_text">
								'.$event->{'texte'.$l}.'
							</div>
						</div>
					</div>
				</div>
				';
				$i++;
			}
		}
		
		echo'
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		'; 
		$query_child_block = $db->query(
			"
	 SELECT * FROM tbl_pages
	 WHERE parent = :id AND id != 66 ORDER BY `order` ASC"
		);
		$db->bind($query_child_block, ":id", $page->id);
		$counted_childs = $db->rowCount($query_child_block);
 
		if ($counted_childs > 0) {
			$html .= '
			   <div id="collapse' . $i_block . '" class=" delta grid_12 panel-collapse collapse in" aria-expanded="true" style="">
				  <div class="panel-body" style="color:black">

	 ';
			$db->execute($query_child_block);
			$i_child_block = 0;
			$html_child = "";
			while ($child_block = $db->fetch($query_child_block)) {
				$query_get_alls = $db->query("SELECT * ,tbl_pages_inputs.value as value, i.name as name FROM tbl_pages_inputs LEFT JOIN inputs i ON i.id = tbl_pages_inputs.type WHERE fk_pages = :id");
				
				$db->bind($query_get_alls, ":id", $child_block->id);
				$db->execute($query_get_alls);
				while ($cust = $db->fetch($query_get_alls)) {
					$child_block->{$cust->name} = $cust->value;
				}
				if ($i_child_block % 2 == 0) {
					$alternate_slide_child = '
					   <div class="alpha table-cell-custom grid_6 grid_1024_12 block_1 fadeInLeft wow" data-wow-duration="1.1s" data-wow-delay="0.1s">
						   <div class=" "> 
							   <div class="square">
									<div > 
										<h4 class="wrap-h2">' . $child_block->{'title' . $l} . '</h4>
										<div class="orange-bar"></div>
										<span class="suptitle">'.$child_block->{'suptitle' . $l}.'</span> <br>
										
									</div>
								</div>
							   <div style="color:#000;"> 
								   ' . $child_block->{'texte' . $l} . ' 
							   </div><br>
							   <a href="'.URLROOT.getUrlLang($child_block->id).'" class="submission_button btn"> En savoir plus</a>
						   </div>
					   </div>
					   <div class="omega about_section table-cell-custom grid_6 block_2 grid_1024_12 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="0.1s">
						   <div class="div_wrap_about" style="background: url(' . URLROOT . 'uploads/'.SITENAME.'/' . $child_block->image2 . '~l=1024) center; background-size: contain;background-attachment: scroll;background-position: center;
background-repeat: no-repeat;">
							   <img class="about_img" src="' . URLROOT . 'uploads/'.SITENAME.'/' . $child_block->image2 . '~l=1024" alt="' . $child_block->alt . '" >
						   </div>
					   </div>
					   ';
							} else {

								$alternate_slide_child = '
					   <div class="alpha about_section table-cell-custom grid_6 block_1 grid_1024_12 fadeInLeft wow" data-wow-duration="1.1s" data-wow-delay="0.1s">
						   <div class="div_wrap_about" style="background: url(' . URLROOT . 'uploads/'.SITENAME.'/' . $child_block->image2 . '~l=1024) center;
background-repeat: no-repeat; background-size: contain;background-attachment: scroll;background-position: center;">
							   <img class="about_img" src="' . URLROOT . 'uploads/'.SITENAME.'/' . $child_block->image2 . '~l=1024" alt="' . $child_block->alt . '" >
						   </div>
					   </div>
					   <div class="omega table-cell-custom grid_6 grid_1024_12 block_2 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="0.1s">
						   <div class=" ">
							   <div class="square">
									<div > 
										<h4 class="wrap-h2">' . $child_block->{'title' . $l} . '</h4>
										<div class="orange-bar"></div>
										<span class="suptitle">'.$child_block->{'suptitle' . $l}.'</span> <br>
										
									</div>
								</div> 
							   ' . $child_block->{'texte' . $l} . '
							   <br> 
							   <a href="'.URLROOT.getUrlLang($child_block->id).'" class="submission_button btn"> En savoir plus</a>
						   </div>
					   </div>
		   ';
				}
				$html_child .= '
		<div id="section_'.$child_block->id.'" class="table-custom section  "
		   style="width: 100%;/*background: url(' . URLROOT . 'image.php?path=' . $child_block->image2 . '~l=1024);*/ background-size: cover;
		   background-position: center;
		   background-attachment:fixed;
		   background-repeat: no-repeat;">
		   <div id="acordeon">
			  <div class="panel-group" id="accordion' . $i_child_block . '">
				 <div class="panel panel-default ' . ($i_child_block % 2 == 1 ? 'lets_flex' : '') . '">
				 ' . $alternate_slide_child . '
					<div class="clear"></div>
				</div>
			  </div>
		   </div>
		</div>
		';
				$i_child_block++;
			}
			$html .= $html_child;
			$html .= '
			  </div>
		   </div>
	 ';
		}
		$html .= ' 
  ';
		$i_block++;
		echo $html.'
		<div class="clear"></div>
	</div>
			';
		//call to action
		$queryCTA = $db->query('SELECT * FROM tbl_pages WHERE id = :idCta');
		$db->bind($queryCTA, ":idCta", 65);
		$db->execute($queryCTA);
		$cta = $db->single($queryCTA);
		getCta($cta);
	?>
</div>
<?php
	//Require footer
	require_once APPROOT . "/views/inc/footer.php";
?>