<?php 
	require_once APPROOT . "/views/inc/header.php"; 
	
	echo '
<div id="content" class="faq text-center">  
	<div class="delta grid_12">
	' . $page->{'texte' . $l} . '
	';
	$db = new Database();
	$getQuestions = $db->query("
		SELECT * FROM tbl_pages 
		WHERE parent = ".__FAQID__."");
	if($db->rowCount($getQuestions) > 0 ){
		
		while($question = $db->fetch($getQuestions)){
			echo '<h2>'.$question->{'title'.$l}. "</h2><br><span>" .$question->{'texte'.$l}."</span>";
		}
	}
	echo'

	</div>
	<div class="clear"></div> 
</div>
';
	
	require_once APPROOT . "/views/inc/footer.php";
?>
