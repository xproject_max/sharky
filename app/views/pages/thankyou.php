<?php 
	require_once APPROOT . "/views/inc/header.php";
	
	// echo print_r($_SESSION);
	echo '
<div id="content" data-scroll-section   data-barba-namespace="thankyou" class="thankyou"> 
	<h4 style="color:#000;">' . $page->{'title' . $l} . '</h4>
	
';
	if (isset($_SESSION['secure_post']['payment_method']['value']) && $_SESSION['secure_post']['payment_method']['value'] == "interac") {
		echo '<p>' . str_replace("__ORDERID__", $_SESSION['order_id'], $lang['thanks_interac']) . '</p>';
	} else {
		echo '<p>' . $lang['thanks_purchase'] . '</p>';
		echo(isset($_SESSION['order_id']) ? '<p>' . str_replace("__ORDERID__", $_SESSION['order_id'], $lang['thanks_payment']) . '</p>' : '');
	}
	
	if (isset($_SESSION['secure_post']['delivery_method']['value']) && ($_SESSION['secure_post']['delivery_method']['value'] == "Ramassage" || $_SESSION['secure_post']['delivery_method']['value'] == "Pickup")) {
		echo '<p>' . $lang['add_edit_pickup_time'] . '</p>';
	}
	
	if (isset($_SESSION['passwordTemp'])) {
		echo '
		<p>
			' . $lang['your_password_is'] . '
			</br>
			</br>
			' . $_SESSION['passwordTemp'] . '
		</p>
	';
	}
	echo '<p>' . $lang['might_be_in_spam'] . '</p>';
	if (isset($_SESSION['secure_post']['account_method']['value']) && $_SESSION['secure_post']['account_method']['value'] == "guest") {
		echo '<a class="make_order_account btn btn-custom" data-email="' . $_SESSION['secure_post']['email']['value'] . '" >' . $lang['create_account_from_info'] . '</a>';
	}
	echo '
	<a class="btn btn-warning" href="' . getUrlLang(1) . '">' . $lang['go_home'] . '</a>
		';
?>
	<?php
		$items = array();
		for ($i = 1; $i < 16; $i++) {
			echo '<img  style="visibility: hidden;position:absolute !important;left:-100%;top:0;" src="'.URLROOT . 'images/thanks/' . $i . '.0.jpg">'; 
		}
		echo implode(",",$items); 
		?> 
	<div class="clear"></div> 
	</div>
<?php 
	
	unset($_SESSION['cart']);
	unset($_SESSION['order_id']);
	unset($_SESSION['is_pay_order']);
	require_once APPROOT . "/views/inc/footer.php";
?>