<?php
 
	require_once APPROOT . "/views/inc/header.php";
	
	
	echo '
<div id="content" data-scroll-section   data-barba-namespace="realisations" class="realisations text-center"> 
	 
 ';?>
<div data-scroll class="realisations_wrapper">
<div class="square">
	<?php
		$queryRealisations = $db->query('SELECT * FROM tbl_pages WHERE id = :id');
		$db->bind($queryRealisations, ':id', __REALISATIONSID__);
		$db->execute($queryRealisations);
		$sectionRealisations = $db->single($queryRealisations);
	?>
	<h2><?= $sectionRealisations->{'title'.$l} ?></h2>
	<div class="orange-bar"></div>
</div>
	<div class="text_realisation">
	<?= $page->{'texte'.$l} ?>
	</div>
	<div class="filter_categs">
	<?php
		//query all categs then for each of them subcateg for filters;
		
		$queryRealisationsCateg = $db->query('SELECT * FROM tbl_pages WHERE parent = :id');
		$db->bind($queryRealisationsCateg, ':id', __REALISATIONSID__);
		$db->execute($queryRealisationsCateg);
		$i = 0;
		while($categ = $db->fetch($queryRealisationsCateg)){
			//get sub categ if exists
			$queryRealisationsSubCateg = $db->query('SELECT * FROM tbl_pages WHERE parent = :id');
			$db->bind($queryRealisationsSubCateg, ':id', $categ->id);
			$db->execute($queryRealisationsSubCateg);
			$countedChild = $db->rowCount($queryRealisationsSubCateg);
			echo '<div class="categ '.($countedChild > 0 ? 'has-child' : '').' '.($i == 1 ? 'active' : '').'">'.$categ->{'title'.$l}."";
			
			if($countedChild > 0){
				echo '<div class="subCategRow">';
				$i2 = 0;
				while($subCateg = $db->fetch($queryRealisationsSubCateg)){
					echo '<div data-filter-realisation="'.$subCateg->id.'" class="subcateg  '.($i2 == 1 ? 'active' : '').'">'.$subCateg->{'title'.$l}.'</div>';
					$i2++;
				}
				echo'</div>';
			}
			echo '</div>';
			$i++;
		}
	?>
	</div>
</div>
<div  data-scroll id="section_realisation" >
	<?php
		$querySlides = $db->query("SELECT * FROM tbl_pages WHERE parent = 59");
		$db->execute($querySlides);
		$i=0;
		while($galeries = $db->fetch($querySlides)){
			if($galeries->fk_gallery !==""){
												
				echo getGalleryblockNoSlider($galeries->fk_gallery,0,true,false,$i);
				  
			}							
		}
	?> 
</div> 
				<?php
				//call to action
		$queryCTA = $db->query('SELECT * FROM tbl_pages WHERE id = :idCta');
		$db->bind($queryCTA, ":idCta", 65);
		$db->execute($queryCTA);
		$cta = $db->single($queryCTA);
		getCta($cta);
 echo'</div>';
	
	require_once APPROOT . "/views/inc/footer.php";
?>
