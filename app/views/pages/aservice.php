<?php
	//Require headers and lang
	require_once APPROOT . "/views/inc/header.php";


?>
	<div id="content" data-scroll-section   data-barba-namespace="single_service" class="single_service">
 <?php
		
		$query_get_alls = $db->query("SELECT * ,tbl_pages_inputs.value as value, i.name as name FROM tbl_pages_inputs LEFT JOIN inputs i ON i.id = tbl_pages_inputs.type WHERE fk_pages = :id");
		
		$db->bind($query_get_alls, ":id", $page->id);
		$db->execute($query_get_alls);
		while ($cust = $db->fetch($query_get_alls)) {
			$page->{$cust->name} = $cust->value;
		}
		echo '
		<div class="table-custom section "
			style="background-size: cover;
			background-position: center;
			background-attachment:fixed;
			background-repeat: no-repeat;">
			<div class="alpha table-cell-custom grid_6 grid_1024_12 fadeIn wow" data-wow-duration="1s" data-wow-delay="0s">
				<div class="square">
					<div > 
						<h4 class="wrap-h2">'.$page->{'title' . $l}.'</h4>
						<div class="orange-bar"></div>
						<span class="suptitle">'.$page->{'suptitle' . $l}.'</span> <br>
						
					</div>
				</div>
				' . $page->{'texte' . $l} . '
			</div>
			<div class="omega about_section table-cell-custom grid_6 grid_1024_12  fadeIn wow" data-wow-duration="1s" data-wow-delay="0s">
				<div class="div_wrap_about" style="">
					<img class="about_img" src="' . URLROOT . 'uploads/lesprojetssharky/' . $page->image.'~l=1920" alt="' . $page->alt . '" >
				</div>
			</div>
		</div>
		<div class="clear"></div>
		' . getBlocksByType($page->id) . '
		<div class="clear"></div>
			';
		//call to action
		$queryCTA = $db->query('SELECT * FROM tbl_pages WHERE id = :idCta');
		$db->bind($queryCTA, ":idCta", 65);
		$db->execute($queryCTA);
		$cta = $db->single($queryCTA);
		getCta($cta);
	?>
	</div>
<?php
	//Require footer
	require_once APPROOT . "/views/inc/footer.php";
?>
