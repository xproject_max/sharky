<?php 
	require_once APPROOT . "/views/inc/header.php";
	
	
	echo '
<div id="content" class="maintenance text-center"> 
	<div class="skewthis"></div>
	<div class="delta grid_12">
	' . $page->{'texte' . $l} . '
	</div>
	<div class="clear"></div> 
</div>
';
	
	require_once APPROOT . "/views/inc/footer.php";
?>
