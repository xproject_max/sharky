<?php
	if (isset($data['menu_array'])) {
		$data = ($data['menu_array']);
	}
	require_once APPROOT . "/views/inc/header.php";
	
	
	echo '
<div id="content" data-scroll-section   data-barba-namespace="sitemap" class="sitemap wrapper text-center">
	<div class="skewthis"></div>
	<h4 style="color:#000;">' . $page->{'title' . $l} . '</h4>
';
	$i = 0;
	$allPages = $db->query(
		'SELECT tbl_pages.*, tc.has_slug FROM tbl_pages LEFT JOIN (SELECT id as idtc, has_slug FROM tess_controller) tc
 ON tc.idtc = tbl_pages.type WHERE tbl_pages.parent=0  AND tbl_pages.online = 1 
         AND  tbl_pages.id != '. __404ID__ .'
		AND  tbl_pages.id != '. __SITEMAPID__  .'
		AND  tbl_pages.id != '.__CONDITIONSID__  .'
		AND  tbl_pages.id != '.__POLITIQUESID__  .'
		AND  tbl_pages.id != '.__THANKYOUID__ .'
			AND  tbl_pages.id != '.__PRODUCTSID__ .'
			AND  tbl_pages.id != '.__BLOGID__ .'
			AND  tbl_pages.id != '.__CHECKOUTID__ .'
			AND  tbl_pages.id != '.__DELIVERYPOLICYID__ .'
			AND  tbl_pages.id != '.__RETURNPOLICYID__ .'
			AND  tbl_pages.id != '.__PAYMENTFAILID__ .'
			AND  tbl_pages.id != '.__RECOVERYACCOUNTID__ .'
			AND  tbl_pages.id != '.__MAINTENANCEID__ .'
			AND  tbl_pages.id != '.__ACTIVATIONACCOUNTID__.'
			AND  tbl_pages.id != '.__APRODUCT__.'
			AND  tbl_pages.id != '.__PAYMENTTHANKYOUID__.'
			AND  tbl_pages.id != '.__ANARTICLE__.'
			AND  tbl_pages.id != '.__APROMO__.'
			AND  tbl_pages.id != '.__PROMOSID__.'
			AND  tbl_pages.id != '.__ACONTEST__.'
			AND  tbl_pages.id != '.__AREALISATION__.'
			AND  tbl_pages.id != '.__ASUBSCRIPTION__.'
			AND  tbl_pages.id != '.__CATEGCONTESTS__.'
			AND  tbl_pages.id != '.__CATEGPROMOS__.'
			AND  tbl_pages.id != '.__CATEGARTICLES__.'
			AND  tbl_pages.id != '.__CATEGPRODUCTS__.'
			AND  tbl_pages.id != '.__CAROUSELID__.'
			AND  tbl_pages.id != '.__ASLIDEID__.' 
			AND  tbl_pages.id != '.__BFULLTEXTID__.'
			AND  tbl_pages.id != '.__BFULLIMGID__.'
			AND  tbl_pages.id != '.__BTEXTIMGID__.'
			AND  tbl_pages.id != '.__BDOUBLETEXTID__.'
			AND  tbl_pages.id != '.__BDOUBLEIMGID__.'
			AND  tbl_pages.id != '.__BVIDEOID__.'
			AND  tbl_pages.id != '.__BTEXTVIDEOID__.'
			AND  tbl_pages.id != '.__AQUESTIONID__.'
			' . (!isset($_SESSION['user_id_website']) ?
			'AND  tbl_pages.id != '.__ORDERSID__.'
			AND  tbl_pages.id != '.__USERAREAID__.'
			' : 'AND  tbl_pages.id != '.__LOGINAREAID__.'') . '
			AND  tbl_pages.id != '. __CGVSID__.''
	); // 3 = Thank you
	 
	$db->execute($allPages);
	while ($section = $db->fetch($allPages, 'array')) {
		$show = true;
		$content = '';
		$class = '';
		if ($i % 3 == 0) {
			$class .= 'alpha ';
		} elseif ($i % 3 == 1) {
			$class .= '';
		} elseif ($i % 3 == 2) {
			$class .= 'omega ';
		}
		if ($i % 3 == 0) {
			$class .= 'alpha_1024 noomega_1024';
		} elseif ($i % 3 == 1) {
			$class .= 'noalpha_1024 omega_1024';
		}
		
		$accordeon = false;
		
		
		$modules = $db->query("SELECT module FROM tess_controller WHERE id= :type");
		$db->bind($modules, ":type", $section['type']);
		$module = $db->single($modules)->module;
		// if($section['has_slug'] == 1 && file_exists(APPROOT.'/views/'.$module.'.php')){
		$content .= '<li><a href="' . getUrlLang($section['id']) . '" title="' . $section['title' . $l] . '">' . $section['title' . $l] . '</a></li>';
		// }else{
		// $show = false;
		// }
		
		$allLinks = $db->query('SELECT tbl_pages.*, tc.has_slug FROM tbl_pages LEFT JOIN (SELECT id as idtc, has_slug FROM tess_controller) tc ON tc.idtc= tbl_pages.type WHERE (id=:root OR parent=:parent)  AND tbl_pages.online = 1  ORDER BY `order` ASC');
		$db->bind($allLinks, ":root", $section['id']);
		$db->bind($allLinks, ":parent", $section['id']);
		$db->execute($allLinks);
		while ($link = $db->fetch($allLinks, 'array')) {
			$modules = $db->query("SELECT module FROM tess_controller WHERE id= :type");
			$db->bind($modules, ":type", $link['type']);
			$module = $db->single($modules)->module;
			if ($link['has_slug'] == 1 && file_exists(APPROOT . '/views/' . $module . '.php') && $link['title' . $l] != $section['title' . $l]) {
				if (!$accordeon) {
					$content .= '<ul class="accordeon">';
					$accordeon = true;
				}
				$content .= '<li><a href="' . getUrlLang($link['id']) . '" title="' . $link['title' . $l] . '">' . $link['title' . $l] . '</a></li>';
				$show = true; //Permet de s'assurer que le bloc est afficher si celui-ci à des enfants qui sont naviguables
			}
		}
		if ($accordeon)
			$content .= '</ul>';
		
		// $content .= '</div>';
		
		if ($i % 3 == 1) {
			// $content .= '<div class="clear hide show_1024"></div>';
		}
		if ($i % 3 == 2) {
			// $content .= '<div class="clear hide_1024"></div>';
		}
		if ($show) {
			echo $content;
			$i++;
		}
	}
	$queryproducts = $db->query("SELECT * FROM tbl_pages WHERE id = ".__PRODUCTSID__."");
	$products = $db->single($queryproducts);
	$i = 0;
	$allPages = $db->query('SELECT tbl_products.*, tc.has_slug FROM tbl_products LEFT JOIN (SELECT id as idtc, has_slug FROM tess_controller) tc ON tc.idtc = tbl_products.type WHERE parent=0  AND tbl_products.online = 1 ORDER BY parent, `order` ASC'); // 3 = Thank you
	$db->execute($allPages);
	while ($section = $db->fetch($allPages, 'array')) {
		$show = true;
		$content = '';
		$class = '';
		if ($i % 3 == 0) {
			$class .= 'alpha ';
		} elseif ($i % 3 == 1) {
			$class .= '';
		} elseif ($i % 3 == 2) {
			$class .= 'omega ';
		}
		if ($i % 3== 0) {
			$class .= 'alpha_1024 noomega_1024';
		} elseif ($i % 3== 1) {
			$class .= 'noalpha_1024 omega_1024';
		}
		
		$accordeon = false;
		
		
		$modules = $db->query("SELECT module FROM tess_controller WHERE id= :type");
		$db->bind($modules, ":type", $section['type']);
		$module = $db->single($modules)->module;
		if ($i == 0) {
			$content .= '<li><a href="' . getUrlLang($products->id) . '" title="' . $products->{'title' . $l} . '">' . $products->{'title' . $l} . '</a></li>';
		}
		if ($section['has_slug'] == 1 && file_exists(APPROOT . '/views/' . $module . '.php')) {
			
			$content .= '<li><a href="' . getUrlLangProducts($section['id']) . '" title="' . $section['title' . $l] . '">' . $section['title' . $l] . '</a></li>';
		} else {
			$show = false;
		}
		
		$allLinks = $db->query('SELECT tbl_products.*, tc.has_slug FROM tbl_products LEFT JOIN (SELECT id as idtc, has_slug FROM tess_controller) tc ON tc.idtc = tbl_products.type WHERE (id=:root OR parent=:parent)  AND tbl_products.online = 1  ORDER BY `order` ASC');
		$db->bind($allLinks, ":root", $section['id']);
		$db->bind($allLinks, ":parent", $section['id']);
		$db->execute($allLinks);
		while ($link = $db->fetch($allLinks, 'array')) {
			$modules = $db->query("SELECT module FROM tess_controller WHERE id= :type");
			$db->bind($modules, ":type", $link['type']);
			$module = $db->single($modules)->module;
			if ($link['has_slug'] == 1 && file_exists(APPROOT . '/views/' . $module . '.php') && $link['title' . $l] != $section['title' . $l]) {
				if (!$accordeon) {
					$content .= '<ul class="accordeon">';
					$accordeon = true;
				}
				
				$content .= '<li><a href="' . getUrlLangProducts($link['id']) . '" title="' . $link['title' . $l] . '">' . $link['title' . $l] . '</a></li>';
				$show = true; //Permet de s'assurer que le bloc est afficher si celui-ci à des enfants qui sont naviguables
			}
		}
		if ($accordeon)
			$content .= '</ul>';
		
		$content .= ' ';
		
		if ($i % 3 == 1) {
			// $content .= '<div class="clear hide show_1024"></div>';
		}
		if ($i % 3 == 2) {
			// $content .= '<div class="clear hide_1024"></div>';
		}
		if ($show) {
			echo $content;
			$i++;
		}
	}
	
	$queryblog = $db->query("SELECT * FROM tbl_pages WHERE id = ".__BLOGID__."");
	$blog = $db->single($queryblog);
	
	$i = 0;
	$allPages = $db->query('SELECT tbl_blogs.*, tc.has_slug FROM tbl_blogs LEFT JOIN (SELECT id as idtc, has_slug FROM tess_controller) tc ON tc.idtc = tbl_blogs.type WHERE parent=0 AND tbl_blogs.online = 1  ORDER BY parent, `order` ASC'); // 3 = Thank you
	$db->execute($allPages);
	while ($section = $db->fetch($allPages, 'array')) {
		
		$show = true;
		$content = '';
		$class = '';
		if ($i % 3 == 0) {
			$class .= 'alpha ';
		} elseif ($i % 3 == 1) {
			$class .= '';
		} elseif ($i % 3 == 2) {
			$class .= 'omega ';
		}
		if ($i % 3== 0) {
			$class .= 'alpha_1024 noomega_1024';
		} elseif ($i % 3 == 1) {
			$class .= 'noalpha_1024 omega_1024';
		}
		
		$accordeon = false;
		
		
		$modules = $db->query("SELECT module FROM tess_controller WHERE id= :type");
		$db->bind($modules, ":type", $section['type']);
		$module = $db->single($modules)->module;
		
		if ($i == 0) {
			$content .= '<li><a href="' . getUrlLang($blog->id) . '" title="' . $blog->{'title' . $l} . '">' . $blog->{'title' . $l} . '</a></li>';
		}
		if (file_exists(APPROOT . '/views/' . $module . '.php') && $section['has_slug'] == 1) {
			$content .= '<li><a href="' . getUrlLangBlogs($section['id']) . '" title="' . $section['title' . $l] . '">' . $section['title' . $l] . '</a></li>';
		} else {
			$show = false;
		}
		
		$allLinks = $db->query('SELECT tbl_blogs.*, tc.has_slug FROM tbl_blogs LEFT JOIN (SELECT id as idtc, has_slug FROM tess_controller) tc ON tc.idtc = tbl_blogs.type WHERE (id=:root OR parent=:parent)  AND tbl_blogs.online = 1 ORDER BY `order` ASC');
		$db->bind($allLinks, ":root", $section['id']);
		$db->bind($allLinks, ":parent", $section['id']);
		$db->execute($allLinks);
		while ($link = $db->fetch($allLinks, 'array')) {
			$modules = $db->query("SELECT module FROM tess_controller WHERE id= :type");
			$db->bind($modules, ":type", $link['type']);
			$module = $db->single($modules)->module;
			if ($link['has_slug'] == 1 && file_exists(APPROOT . '/views/' . $module . '.php') && $link['title' . $l] != $section['title' . $l]) {
				if (!$accordeon) {
					$content .= '<ul class="accordeon">';
					$accordeon = true;
				}
				
				$content .= '<li><a href="' . getUrlLangBlogs($link['id']) . '" title="' . $link['title' . $l] . '">' . $link['title' . $l] . '</a></li>';
				$show = true; //Permet de s'assurer que le bloc est afficher si celui-ci à des enfants qui sont naviguables
			}
		}
		if ($accordeon)
			$content .= '</ul>';
		
		// $content .= '</div>';
		
		if ($i % 3 == 1) {
			// $content .= '<div class="clear hide show_1024"></div>';
		}
		if ($i % 3 == 2) {
			// $content .= '<div class="clear hide_1024"></div>';
		}
		if ($show) {
			echo $content;
			$i++;
		}
	}
	echo '

					<div class="clear"> </div>
</div>
';
	
	require_once APPROOT . "/views/inc/footer.php";
?>
