<?php
 
	require_once APPROOT . "/views/inc/header.php";
	
	$query_get_alls = $db->query("SELECT * ,tbl_pages_inputs.value as value, i.name as name FROM tbl_pages_inputs LEFT JOIN inputs i ON i.id = tbl_pages_inputs.type WHERE fk_pages = :id");
	
	$db->bind($query_get_alls, ":id", $page->id);
	$db->execute($query_get_alls);
	while ($cust = $db->fetch($query_get_alls)) {
		$page->{$cust->name} = $cust->value;
	}
	echo '
<div id="content" data-scroll-section   data-barba-namespace="calculator" class="calculator text-center"> 
	 
 ';?>
<div data-scroll class="calculator_wrapper">
<div class="square">
 
	<span class="suptitle"><?= $page->{'suptitle'.$l} ?></span>
	<div class="orange-bar"></div>
</div>
	<div class="text_realisation">
	<?= $page->{'texte'.$l} ?>
	</div> 
</div>
<div  data-scroll id="section_calculator" >
	<div  class="wrapper_calculator_section">
		<div class="left_part">
		
			<div class="tabs tabs--md">
				<ul class="tabs__list list-unstyled">
					<li class="tabs__item  tabs__item--active">
						<a href="#tab-1" data-barba-prevent="self"  class="tabs__link">Rectangulaire</a>
					</li>
					<li class="tabs__item">
						<a href="#tab-2" data-barba-prevent="self"  class="tabs__link">Cylindrique</a>
					</li>
					 
				</ul>
				<div class="tabs__content">
					<div id="tab-1" class="tabs__area tabs__area--active">
						<div class="left_part_tabs">
							<div class="row">
								<div class="fullwidth">
									<span>Nom surface</span>
								</div>
								<div class="fullwidth">
									<div class="table-cell nom_input">
										<input type="text" placeholder="Nom" value="" name="nom">
									</div> 
								</div>
							</div>
							<div class="row">
								<div class="fullwidth">
									<span>Longueur</span>
								</div>
								<div class="fullwidth">
									<div class="table-cell longeur_input">
										<input type="text" placeholder="Longueur" value="" name="longueur">
									</div>
									<div class="table-cell longeur_mesure">
										<span>M</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="fullwidth">
									<span>Largeur</span>
								</div>
								<div class="fullwidth">
									<div class="table-cell largeur_input">
										<input type="text" placeholder="Largeur" value="" name="largeur">
									</div>
									<div class="table-cell largeur_mesure">
										<span>M</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="fullwidth">
									<span>Épaisseur</span>
								</div>
								<div class="fullwidth">
									<div class="table-cell epaisseur_input">
										<input type="text" placeholder="Épaisseur" value="" name="epaisseur">
									</div>
									<div class="table-cell epaisseur_mesure">
										<span>CM</span>
									</div>
								</div>
							</div>
						</div>
						<div class="right_part_tabs">
							<div class="top_part">
								<div class="img_type">
									<?= file_get_contents(URLROOT.'images/rectangular.svg'); ?>
								</div>
							</div>
							<div class="bottom_part">
								<div class="table">
									<div class="table-cell">
										<a href="#" data-barba-prevent="self"  class="submisson_btn convertImp">
											<div class="table-i"><i class="far fa-exchange-alt"></i></div>
											<span>Conversion en impérial </span>
										</a>
									</div>
									<div class="table-cell">
										<a href="#" data-barba-prevent="self"  class="submisson_btn addTotal">
											<div class="table-i"><i class="far fa-plus"></i></div>
											<span>Ajouter au total</span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="tab-2" class="tabs__area">
						<form class="rectangle_form" action="" method="post" name="rectangle_form">
						<div class="left_part_tabs">
							<div class="row">
								<div class="fullwidth">
									<span>Nom surface</span>
								</div>
								<div class="fullwidth">
									<div class="table-cell nom_inputCylindre">
										<input type="text" placeholder="Nom" value="" name="nom">
									</div> 
								</div>
							</div>
							<div class="row">
								<div class="fullwidth">
									<span>Diamètre</span>
								</div>
								<div class="fullwidth">
									<div class="table-cell diametre_inputCylindre">
										<input type="text" placeholder="Longueur" value="" name="longueur">
									</div>
									<div class="table-cell diametre_mesureCylindre">
										<span>CM</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="fullwidth">
									<span>Hauteur</span>
								</div>
								<div class="fullwidth">
									<div class="table-cell hauteur_inputCylindre">
										<input type="text" placeholder="Largeur" value="" name="largeur">
									</div>
									<div class="table-cell hauteur_mesureCylindre">
										<span>CM</span>
									</div>
								</div>
							</div>
							 
						</div>
						<div class="right_part_tabs">
							<div class="top_part">
								<div class="img_type">
									<?= file_get_contents(URLROOT.'images/cylinder.svg'); ?>
								</div>
							</div>
							<div class="bottom_part">
								<div class="table">
									<div class="table-cell">
										<a href="#" data-barba-prevent="self"  class="submisson_btn convertImpCylindre">
											<div class="table-i"><i class="far fa-exchange-alt"></i></div>
											<span>Conversion en impérial </span>
										</a>
									</div>
									<div class="table-cell">
										<a href="#" data-barba-prevent="self"  class="submisson_btn addTotalCylindre">
											<div class="table-i"><i class="far fa-plus"></i></div>
											<span>Ajouter au total</span>
										</a>
									</div>
								</div>
							</div>
						</div>	
						</form>
						<div class="clear"></div>
					</div>
					 
				</div>
			</div> 
		</div> 
		
		<div class="right_part">
			<div class="results">
				<div class="wrapper_results">
					<div class="bgwhite">
						<div class="title">
							<span>Résultat</span>
						</div> 
						<div class="total_volume">
							<div class="table">
								<div class="table-cell">
									<span>Volume total</span>
								</div>
								<div class="table-cell">
								<span class="resultTotal">0</span> M<sup>3</sup>
								</div>
							</div>  
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div class="clear"></div>
		<div class="left_options">
			<div class="wrapper_options">
				<div class="row_option">
					<div class="heighty">
						<span class="option">Longueur : 355.54 x  Largeur: 123.455 x Épaisseur : 123.14 X =  456.45 M³</option>
					</div>
					<div class="twenty">
						<div class="actions">
							<a href="#" data-barba-prevent="self" class="remove"><i class="fas fa-times-circle"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div> 
				<?php
				//call to action
		$queryCTA = $db->query('SELECT * FROM tbl_pages WHERE id = :idCta');
		$db->bind($queryCTA, ":idCta", 65);
		$db->execute($queryCTA);
		$cta = $db->single($queryCTA);
		getCta($cta);
 echo'</div>';
	unset($_SESSION['calculator']['total']);
	unset($_SESSION['calculator']['totals']); 
	unset($_SESSION['calculator']['options']); 
	require_once APPROOT . "/views/inc/footer.php";
?>
