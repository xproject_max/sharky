<?php
	// Simple page redirect
	use PHPMailer\PHPMailer\Exception;
	use PHPMailer\PHPMailer\PHPMailer;


	function is_mobile($useragent){


		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
		{
			return true;
		}else{
			return false;
		}

	}
	function redirect($page, $lang = "")
	{
		$language = new Lang();
		$lang = ($lang ? $lang : $language->lang);
		header('location: ' . $page);
	}

	if (!defined('CAL_GREGORIAN')) {
		define('CAL_GREGORIAN', 0);
	}
	if (!function_exists('cal_days_in_month')) {
		function cal_days_in_month($calendar, $month, $year)
		{
			return date('t', mktime(0, 0, 0, $month, 1, $year));
		}
	}
	//nice hide imgs
	function DragonHide($img)
	{
		//return 'data: '.mime_content_type($img).';base64,'.base64_encode(file_get_contents($img));
		return $img;
	}
	function getPhone($phone){
		$phone = preg_replace('/\D+/', '', $phone);
		return $phone;
	}

	function add_visitor($view){
		global $_SESSION;
		if (isset($_SESSION['viewed']) && (time() - $_SESSION['viewed'] > 30)) {
			unset($_SESSION['viewed']);
		}
		if(!isset($_SESSION['viewed'])){

			$_SESSION['viewed'] = time();

			$file = dirname(dirname(__DIR__)).'/public/plugins/counter.txt';

			// default the counter value to 1
			$counter = 1;

			// add the previous counter value if the file exists
			if (file_exists($file)) {
				$counter += file_get_contents($file);
			}

			// write the new counter value to the file
			file_put_contents($file, $counter);
		}
	}

	/*
	 * Get months options list.
	 */
	function getAllMonths($selected = '')
	{
		global $language;
		$options = '';
		if ($language == "fr") {
			setlocale(LC_TIME, "fr_FR");
		} else {

			setlocale(LC_TIME, "en_US");
		}
		for ($i = 1; $i <= 12; $i++) {
			$value = ($i < 10) ? '0' . $i : $i;
			$selectedOpt = ($value == $selected) ? 'selected' : '';
			$options .= '<option value="' . $value . '" ' . $selectedOpt . ' >' . changetoFr(date("F", mktime(0, 0, 0, $i + 1, 0, 0))) . '</option>';
		}
		return $options;
	}

	function changetoFr($value)
	{
		global $language, $lang, $l;
		if ($l !== "_en") {
			switch ($value) {
				case "January":
					return "Janvier";
					break;
				case "February":
					return "Février";
					break;
				case "March":
					return "Mars";
					break;
				case "April":
					return "Avril";
					break;
				case "May":
					return "Mai";
					break;
				case "June":
					return "Juin";
					break;
				case "July":
					return "Juillet";
					break;
				case "August":
					return "Août";
					break;
				case "September":
					return "Septembre";
					break;
				case "October":
					return "Octobre";
					break;
				case "November":
					return "Novembre";
					break;
				case "December":
					return "Décembre";
					break;
			}
		} else {
			return $value;
		}
	}

	/*
	 * Get years options list.
	 */
	function getYearList($selected = '')
	{
		$options = '';
		for ($i = 2015; $i <= 2025; $i++) {
			$selectedOpt = ($i == $selected) ? 'selected' : '';
			$options .= '<option value="' . $i . '" ' . $selectedOpt . ' >' . $i . '</option>';
		}
		return $options;
	}

	function getCalender($year = '', $month = '')
	{
		global $_SESSION;
		$dateYear = ($year != '') ? $year : date("Y");
		$dateMonth = ($month != '') ? $month : date("m");
		$date = $dateYear . '-' . $dateMonth . '-01';
		$currentMonthFirstDay = date("N", strtotime($date));
		$totalDaysOfMonth = cal_days_in_month(CAL_GREGORIAN, $dateMonth, $dateYear);
		$totalDaysOfMonthDisplay = ($currentMonthFirstDay == 7) ? ($totalDaysOfMonth) : ($totalDaysOfMonth + $currentMonthFirstDay);
		$boxDisplay = ($totalDaysOfMonthDisplay <= 35) ? 35 : 42;
		?>
		<div id="calender_section">
			<h2>
				<a href="javascript:void(0);" onclick="getCalendar('calendar_div','<?php
					echo date("Y", strtotime($date . ' - 1 Month')); ?>','<?php
					echo date("m", strtotime($date . ' - 1 Month')); ?>');"
				>&lt;&lt;</a>
				<select name="month_dropdown" class="month_dropdown dropdown"><?php
						echo getAllMonths($dateMonth); ?></select>
				<select name="year_dropdown" class="year_dropdown dropdown"><?php
						echo getYearList($dateYear); ?></select>
				<a href="javascript:void(0);" onclick="getCalendar('calendar_div','<?php
					echo date("Y", strtotime($date . ' + 1 Month')); ?>','<?php
					echo date("m", strtotime($date . ' + 1 Month')); ?>');"
				>&gt;&gt;</a>
			</h2>
			<div id="calender_section_top">
				<ul>
					<li>Dim</li>
					<li>Lun</li>
					<li>Mar</li>
					<li>Mer</li>
					<li>Jeu</li>
					<li>Ven</li>
					<li>Sam</li>
				</ul>
			</div>
			<div id="calender_section_bot">
				<ul>
					<?php
						$dayCount = 1;
						$db = new Database();
						date_default_timezone_set('America/Montreal');
						// Make a DateTime object with the current date and time
						$today = new DateTime('now');

						// Make an empty array to contain the hours
						$aHours = array();

						// Make another DateTime object with the current date and time
						$oStart = new DateTime('now');

						// Set current time to midnight
						$oStart->setTime(0, 0);

						// Clone DateTime object (This is like 'copying' it)
						$oEnd = clone $oStart;

						// Add 1 day (24 hours)
						$oEnd->add(new DateInterval("P1D"));

						// Add each hour to an array
						while ($oStart->getTimestamp() < $oEnd->getTimestamp()) {
							$aHours[] = $oStart->format('H');
							$oStart->add(new DateInterval("PT1H"));
						}

						// Create an array with halfs
						$halfs = array(
							'0', '30',
						);

						// Get the current quarter
						$currentHalf = $today->format('i') - ($today->format('i') % 30);
						for ($cb = 1; $cb <= $boxDisplay; $cb++) {
							if (($cb >= $currentMonthFirstDay + 1 || $currentMonthFirstDay == 7) && $cb <= ($totalDaysOfMonthDisplay)) {
								//Current date
								$currentDate = $dateYear . '-' . $dateMonth . '-' . $dayCount;
								$eventNum = 0;
								//Get number of events based on the current date
								$result = $db->query(
									"SELECT * FROM
                           reservations WHERE date = :date AND fk_orders = :fk_orders AND status = 1"
								);
								$db->bind($result, ":date", $currentDate);
								$db->bind($result, ":fk_orders", $_SESSION['session_order']);
								$db->execute($result);
								$eventNum = $db->rowCount($result);
								//Define date cell color
								if (($eventNum > 0) && ((strtotime($currentDate)) == strtotime(date("Y-m-d")))) {
									echo '<li   date="' . $currentDate . '" class="grey date_cell">';
								} else if (strtotime($currentDate) == strtotime(date("Y-m-d"))) {
									echo '<li date="' . $currentDate . '" class="grey date_cell">';
								} elseif ($eventNum > 0) {
									echo '<li date="' . $currentDate . '" class="light_sky date_cell">';
								} else {
									echo '<li date="' . $currentDate . '" class="date_cell">';
								}

								//Date cell
								echo '<span>';
								echo $dayCount;
								echo '</span>';

								//Hover event popup
								echo '<div id="date_popup_' . $currentDate . '" class="date_popup_wrap none">';
								echo '<div class="date_window">';
								echo '<div class="popup_event">Réservations (' . $eventNum . ')</div>';
								echo ($eventNum > 0) ? '<input  class="btn" type="button" value="Voir les plages réservées" onclick="getEvents(\'' . $currentDate . '\', \'' . $_SESSION['session_order'] . '\');"></input>' : '';
								echo ' <input type="button" class="btn" value="Sélectionner une plage horaire" onclick="addEvent(\'' . $currentDate . '\');"></input>';
								echo '</div></div>';

								echo '</li>';
								$dayCount++;
								?>
							<?php
							} else { ?>
								<li><span>&nbsp;</span></li>
							<?php
							}
						}
					?>
				</ul>
			</div>
			<div id="event_list"></div>
			<div id="event_add" class="none">
				<p class="paragraph">Réservations du <span id="eventDateView"></span></p>
				<span id="fermer" onclick="document.getElementById('event_add').style.display='none';close_event();">Fermer</span>
				<!-- Sujet -->
				<div id='sujetdiv'></div>
				<center>
					<table id="mytable"><br>
						<tr>
							<th>Laissez une note</th>
						</tr>
						<tr>
							<td>
								<p><textarea type="textarea" id="eventTitle" value=""></textarea></p>
							</td>
						</tr>
						<tr>
							<th>Heure de passage</th>
						</tr>
						<tr>
							<td>
								<select id="eventTime" onchange="this.options[this.selectedIndex].value;">
									<option value="0__PRODUCTSID__h00 à 10h00">Entre 0__PRODUCTSID__h00 et 10h00</option>
									<option value="10h00 à 11h00">Entre 10h00 et 11h00</option>
									<option value="11h00 à 12h00">Entre 11h00 et 12h00</option>
									<option value="12h00 à 13h00">Entre 12h00 et 13h00</option>
									<option value="13h00 à __404ID__h00">Entre 13h00 et __404ID__h00</option>
									<option value="__404ID__h00 à 15h00">Entre __404ID__h00 et 15h00</option>
									<option value="15h00 à __SITEMAPID__h00">Entre 15h00 et __SITEMAPID__h00</option>
									<option value="__SITEMAPID__h00 à'.__CONDITIONSID__  .'h00">Entre __SITEMAPID__h00 et'.__CONDITIONSID__  .'h00</option>
									<option value="__CONDITIONSID__h00 à __POLITIQUESID__h00">Entre'.__CONDITIONSID__  .'h00 et __POLITIQUESID__h00</option>
									<option value="__POLITIQUESID__h00 à 19h00">Entre __POLITIQUESID__h00 et 19h00</option>
									<option value="19h00 à 20h00">Entre 19h00 et 20h00</option>
								</select>
							</td>
						</tr>
						<tr>
							<th>Créé par</th>
						</tr>
						<tr>
							<td>
								<p><input type="text" class="btn" disabled="disabled" id="eventUser" value="<?PHP
										echo $_SESSION['session_email'] ?>"
									></p>
							</td>
						</tr>
						<tr>
							<td>
								<input type="hidden" id="eventDate" value=""/>
								<input type="hidden" id="fkorders" value="<?PHP
									echo $_SESSION['session_order'] ?>"
								/>
								<input type="button" class="btn" width="30px" id="addEventBtn" value="Confirmer"/>
							</td>
						</tr>
					</table>
				</center>
			</div>
		</div>

		<?php
	}

	function getEvents($date = '', $fk_orders = '')
	{
		//Include db configuration file
		$eventListHTML = '';
		?>
		<script>$("#mytable").fadeIn();</script><?PHP
		$date = $date ? $date : date("Y-m-d");
		//Get events based on the current date
		$db = new Database();
		$result = $db->query("SELECT a.* FROM reservations a WHERE a.date = :date AND a.status = 1 and a.fk_orders = :fk_orders");
		$db->bind($result, ":date", $date);
		$db->bind($result, ":fk_orders", $fk_orders);
		$db->execute($result);
		if ($db->rowCount($result) > 0) {
			?>

			<?PHP

			while ($row = $db->fetch($result, "array")) {
				?>
				<div id='eventget<?= $row['idEvent']; ?>' class='eventGet'>
					<span id="fermer" onclick="document.getElementById('eventget<?PHP
						echo $row['idEvent']; ?>').style.display='none';close_event();"
					>Fermer</span><!-- Sujet -->


					<center>
						<table id="mytable">
							<tr>
								<th>Note</th>
							</tr>
							<tr>
								<td>
									<p><textarea type="textarea" id="eventTitle" disabled="disabled" value=""><?PHP
												echo $row['title']; ?></textarea></p>
								</td>
							</tr>
							<tr>
								<th>Heure du ramassage</th>
							</tr>
							<tr>
								<td>
									<select id="eventTime" disabled="disabled"
									        onchange="this.options[this.selectedIndex].value;"
									>
										<option value="12h00"><?PHP
												echo $row['timeres']; ?></option>
									</select>
								</td>
							</tr>
							<tr>
								<th>Créé par</th>
							</tr>
							<tr>
								<td>
									<p><input type="text" disabled="disabled" id="eventUser" value="<?PHP
											echo $row['fk_users']; ?>"
										></p>
								</td>
							</tr>

						</table>
					</center>
				</div>
				<?PHP
			}

		}

	}

	function reservationsExists($order)
	{
		global $_SESSION;

		$db = new Database();
		$query_check = $db->query(
			"
         SELECT * FROM reservations
         WHERE fk_orders = :orders
         AND status = 1"
		);
		$db->bind($query_check, ":orders", $order);
		$db->execute($query_check);
		if ($db->rowCount($query_check) > 0) {
			return true;
		} else {
			return false;
		}
	}

	//On archive la reservation
	function archivedReservations($order)
	{
		global $_SESSION;

		$db = new Database();
		$query_update = $db->query(
			"
         UPDATE reservations
         SET status = 0
         WHERE fk_orders = :order"
		);
		$db->bind($query_update, ":order", $order);
		$db->execute($query_update);
	}

	function addEvent($date, $title, $fk_users, $fk_status, $timeres, $fk_orders)
	{

		$currentDate = date("Y-m-d H:i:s");
		$db = new Database();

		if (reservationsExists($fk_orders)) {
			archivedReservations($fk_orders);
		}

		//Insert the event data into database
		$sql = $db->query(
			"INSERT INTO reservations
      (title,fk_orders,fk_users,timeres,presence,date,created,modified)
      SELECT :title, :fk_orders, :fk_users, :timeres, '', :date, :currentDate, :currentDate FROM DUAL
      WHERE NOT EXISTS
      (SELECT date FROM reservations WHERE fk_orders = :fk_orders AND date = :date AND status=1);"
		);
		$db->bind($sql, ":title", $title);
		$db->bind($sql, ":fk_orders", $fk_orders);
		$db->bind($sql, ":fk_users", $fk_users);
		$db->bind($sql, ":timeres", $timeres);
		$db->bind($sql, ":date", $date);
		$db->bind($sql, ":currentDate", $currentDate);
		$db->bind($sql, ":title", $title);
		if ($db->execute($sql)) {
			echo "success";
			DragonMediaMail("Une date de ramassage a été réservée", "La commande #" . $fk_orders . " va être ramassée le : " . $date . " " . $timeres . "", CLIENT_EMAIL, "Masquez-vous", "maxime.mercierv4si@gmail.com", "Date de ramassage");

		} else {

		}
	}

	function DragonMediaMail($subject, $message, $de, $de_name, $to, $to_name, $reply_to = "", $file_array = false)
	{

		include_once dirname(dirname(__DIR__)) . "/public/plugins/mailtest/vendor/autoload.php";
		$cemail = new PHPMailer(true);                              // Passing `true` enables exceptions
		try {
			//Server settings
			// $cemail->SMTPDebug = 2;                                 // Enable verbose debug output
			$cemail->isSMTP();                                      // Set mailer to use SMTP
          $cemail->Host = 'mail.dragon-media.ca';  // Specify main and backup SMTP servers
          $cemail->SMTPAuth = true;                               // Enable SMTP authentication
          $cemail->Username = 'info@dragon-media.ca';                 // SMTP username
          $cemail->Password = 'T,xOC&Irwkc1';                           // SMTP password
          $cemail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
          $cemail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            ));                          // Enable TLS encryption, `ssl` also accepted
          $cemail->Port = 465;            // TCP port to connect to

			//Recipients
			$cemail->setFrom($de, $de_name);
			$cemail->addAddress($to, $to_name);                     // Add a recipient

			//Attachments

			$cemail->CharSet = 'UTF-8';
			//Content
			$cemail->isHTML(true);                                  // Set email format to HTML
			$cemail->Subject = $subject;
			$cemail->Body = "<html><body>" . $message . "</body></html>";
			$cemail->AltBody = $message;

			$cemail->send();
		} catch (Exception $e) {
			echo 'Message could not be sent. Mailer Error: ', $cemail->ErrorInfo;
		}
	}

	function forgotPasswordTemplate($email)
	{
		global $l, $lang;
		require_once dirname(__DIR__) . "/config/config.php";
		//define the key
		$clemdp = md5(microtime(TRUE) * 100000);

		$mail = htmlspecialchars($email, ENT_QUOTES, 'UTF-8');
		if (!preg_match("#^[a-z0-__PRODUCTSID__._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) {
			$passage_ligne = "\r\n";
		} else {
			$passage_ligne = "\n";
		}

		//gather user info
		$db = new Database();
		$query_user = $db->query(
			"
         SELECT *, tbl_users.id as id FROM tbl_users
         LEFT JOIN tbl_users_meta as meta ON meta.id = tbl_users.fk_user_meta
         WHERE tbl_users.username = :email
      "
		);
		$db->bind($query_user, ":email", $email);
		$row = $db->single($query_user);
		$first_name = htmlspecialchars($row->first_name, ENT_QUOTES, 'UTF-8');
		$last_name = htmlspecialchars($row->last_name, ENT_QUOTES, 'UTF-8');

		//Set key in database
		$update_key = $db->query(
			"
         UPDATE tbl_users as u LEFT JOIN tbl_users_meta meta
         ON meta.id = u.fk_user_meta
         SET meta.key_password = :new_key
         WHERE u.email = :email
      "
		);
		$db->bind($update_key, ":new_key", $clemdp);
		$db->bind($update_key, ":email", $email);
		$db->execute($update_key);

		//Let's start message
		$message = $lang['template_forgot_email'];
		$message = str_replace('__FIRST_NAME__', $first_name, $message);
		$message = str_replace('__SITE_URL__', rtrim(URLROOT, '/'), $message);
		$message = str_replace('__LINK__', rtrim(URLROOT, '/') . getUrl(__RECOVERYACCOUNTID__) . "?email=" . urlencode($email) . "&clemdp=" . urlencode($clemdp), $message);

		$subject = $lang['forgotten_password'] . " 🗝";
		$de = CLIENT_EMAIL;
		$de_name = "Masquez-vous";
		$to = $email;
		$to_name = $first_name . " " . $last_name;
		// send mail
		if (DragonMediaMail($subject, '<html>' . $message . '</html>', $de, $de_name, $to, $to_name)) {
			return true;
		} else {
			return false;
		}
	}

	function newAccountTemplate($email)
	{
		global $l, $lang, $_SESSION;
		require_once dirname(__DIR__) . "/config/config.php";

		//define the key
		$cle = md5(microtime(TRUE) * 100000);

		$mail = htmlspecialchars($email, ENT_QUOTES, 'UTF-8');
		if (!preg_match("#^[a-z0-__PRODUCTSID__._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) {
			$passage_ligne = "\r\n";
		} else {
			$passage_ligne = "\n";
		}

		//gather user info
		$db = new Database();
		$query_user = $db->query(
			"
         SELECT *, tbl_users.id as id FROM tbl_users
         LEFT JOIN tbl_users_meta as meta ON meta.id = tbl_users.fk_user_meta
         WHERE tbl_users.username = :email
      "
		);
		$db->bind($query_user, ":email", $email);
		$row = $db->single($query_user);
		$first_name = htmlspecialchars($row->first_name, ENT_QUOTES, 'UTF-8');
		$last_name = htmlspecialchars($row->last_name, ENT_QUOTES, 'UTF-8');

		//Set key in database
		$update_key = $db->query(
			"
         UPDATE tbl_users as u LEFT JOIN tbl_users_meta meta
         ON meta.id = u.fk_user_meta
         SET meta.key = :new_key
         WHERE u.email = :email
      "
		);
		$db->bind($update_key, ":new_key", $cle);
		$db->bind($update_key, ":email", $email);
		$db->execute($update_key);

		//Let's start message
		$message = $lang['template_new_account_email'];
		$message = str_replace('__FIRST_NAME__', $first_name, $message);
		$message = str_replace('__SITE_URL__', rtrim(URLROOT, '/'), $message);
		$message = str_replace('__LINK__', rtrim(URLROOT, '/') . getUrl(__ACTIVATIONACCOUNTID__) . "?email=" . urlencode($email) . "&activation=" . urlencode($cle), $message);
		if (isset($_SESSION['passwordTemp'])) {
			$message = str_replace("__TEMPPASS__", "<br>" . $lang['your_temporary_password'] . $_SESSION['passwordTemp'], $message);
		} else {
			$message = str_replace("__TEMPPASS__", "", $message);
		}
		$subject = $lang['new_account'] . " 🌈";
		$de = CLIENT_EMAIL;
		$de_name = "Masquez-vous";
		$to = $email;
		$to_name = $first_name . " " . $last_name;
		// send mail
		if (DragonMediaMail($subject, '<html>' . $message . '</html>', $de, $de_name, $to, $to_name)) {
			return true;
		} else {
			return false;
		}
	}
	function getGalleryblock($pageid=0, $limit=0, $is_preview=false, $mixed=false,$passed=0){
		global $lang, $l;
		$str="";
		$i = 1;
		foreach(getGallery($pageid,$mixed) as $pictures){ 
			if($pictures->picture && trim($pictures->picture) !== ""){
				$str .= "
				<div class='slick-slide'>
					<a class='gallerylink'  data-wow-delay='0.".$i."s' data-wow-duration='1.5s' data-fancybox='gallery".$i."' data-barba-prevent='self' href='".URLROOT."/uploads/".SITENAME."/".$pictures->picture."'>
						<img class='prev_drawings' src='".URLROOT."/uploads/".SITENAME."/".$pictures->picture."' alt='".(isset($pictures->{'title'.$l}) ? $pictures->{'title'.$l} : "")."'  >
					</a>
				</div>"; 
				$i++; 
			}else{
				$str .= '';
			}
		}
		if($str == "" && $passed == 0){
			
			$str .= '<div class="slick-slide isEmpty">Il n\'y a pas de réalisation pour cette catégorie pour le moment.</div>';
		}
		return $str;
	}
	function getGalleryblockNoSlider($pageid=0, $limit=0, $is_preview=false, $mixed=false,$passed=0){
		global $lang, $l, $i;
		$str="";
		$i = ($i ? $i : 0);
		foreach(getGallery($pageid,$mixed) as $pictures){ 
			if($pictures->picture && trim($pictures->picture) !== ""){
				if($i == 0 || $i % 3 == 0){
					$str .= "<div class='row'>";
				}	
				$str .= "
				<div class='a_realisation ".$i."'>
					<a class='gallerylink'  data-wow-delay='0.".$i."s' data-wow-duration='1.5s' data-fancybox='gallery".$i."' data-barba-prevent='self' href='".URLROOT."/uploads/".$pictures->picture."'>
						<img class='prev_drawings' src='".URLROOT."/uploads/".$pictures->picture."' alt='".(isset($pictures->{'title'.$l}) ? $pictures->{'title'.$l} : "")."'  >
					</a>
				</div>";
				if($i == 2 || $i % 3 == 2){
					$str .= "</div>";
				}					
				$i++; 
			}else{
				$str .= '';
			}
		}
		 
		return $str;
	}
	function shippedTemplate($email, $order_id)
	{
		global $l, $lang;
		require_once dirname(__DIR__) . "/config/config.php";
		//define the key

		$mail = htmlspecialchars($email, ENT_QUOTES, 'UTF-8');
		if (!preg_match("#^[a-z0-__PRODUCTSID__._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) {
			$passage_ligne = "\r\n";
		} else {
			$passage_ligne = "\n";
		}

		//gather user info
		$db = new Database();
		$query_user = $db->query(
			"
         SELECT *, tbl_users.id as id FROM tbl_users
         LEFT JOIN tbl_users_meta as meta ON meta.id = tbl_users.fk_user_meta
         WHERE tbl_users.username = :email
      "
		);
		$db->bind($query_user, ":email", $email);
		$row = $db->single($query_user);
		$first_name = htmlspecialchars($row->first_name, ENT_QUOTES, 'UTF-8');
		$last_name = htmlspecialchars($row->last_name, ENT_QUOTES, 'UTF-8');

		//Let's start message
		$message = $lang['template_shipped'];
		$message = str_replace('__FIRST_NAME__', $first_name, $message);
		$message = str_replace('__SITE_URL__', rtrim(URLROOT, '/'), $message);
		$message = str_replace('__ORDERNUMBER__', $order_id, $message);

		$subject = "(#" . $order_id . ") 🚚 " . $lang['shipped_order'] . "";
		$de = CLIENT_EMAIL;
		$de_name = "Masquez-vous";
		$to = $email;
		$to_name = $first_name . " " . $last_name;
		// send mail
		if (DragonMediaMail($subject, '<html>' . $message . '</html>', $de, $de_name, $to, $to_name)) {
			return true;
		} else {
			return false;
		}
	}

	function newOrderTemplate($orderid)
	{
		global $l, $lang, $_SESSION;
		require_once dirname(__DIR__) . "/config/config.php";

		$db = new Database();

		//get order info
		$qorders = $db->query(
			"
         SELECT *
         FROM tbl_orders
         WHERE id = :order
      "
		);
		$db->bind($qorders, ":order", $orderid);
		$order = $db->single($qorders);

		//get user info
		$qusers = $db->query(
			"
         SELECT u.*,meta.*, u.id as id
         FROM tbl_users u
         LEFT JOIN tbl_users_meta meta
         ON meta.id = u.fk_user_meta
         WHERE u.id = :user
      "
		);
		$db->bind($qusers, ":user", $order->fk_users);
		$user = $db->single($qusers);

		//get delivery info
		$qdelivery = $db->query(
			"
         SELECT *
         FROM tbl_delivery
         WHERE id = :delivery
      "
		);
		$db->bind($qdelivery, ":delivery", $order->fk_delivery);
		$delivery = $db->single($qdelivery);

		$html = '
		<table width="600" style="margin:0 auto;table-layout:fixed;background:#111111;">
			<tr width="600">
				<td width="25"></td>
               <td width="250" style="text-align:left;">

					<span style="display:block;margin:0 auto;padding:0;font-family:arial, sans-serif;color:#ffffff;font-size:12px;line-height:1.4;font-weight:400;">
                     Dragon Média
					</span>
					<span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#ffffff;font-size:12px;line-height:1.4;font-weight:400;">
                     993 rue sauvé
					</span>
					<span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#ffffff;font-size:12px;line-height:1.4;font-weight:400;">
                     Mascouche, Québec
					</span>
					<span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#ffffff;font-size:12px;line-height:1.4;font-weight:400;">
                     J7K 3L6
					</span>

               </td>
			   <td width="250" style="text-align:center;"><img src="' . URLROOT . 'images/logo/dragon.png" width="200px" heigh="auto"></td>
			   <td width="25"></td>
            </tr>
			</table>
				';
		if ($template == 'bill') {
			// $html .=' <table><tr><td>Commande # '.$bill->id.'</td></tr></table>';
			$html .= '
			<table width="600" style="margin:0 auto; background:#111111;">

            <tr width="600">
				<td width="600" colspan="4" style="text-align:center;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#ffffff;font-size:25px;line-height:1.4;font-weight:400;">
                     ' . $lang['thankyou_for_order'] . '
                  </span>
				</td>
			</tr><tr>
               <td width="25"></td>
               <td width="550" colspan="2" style="text-align:center;">
                  <a title="Export" target="_blank" href="' . URLROOT . 'ajax.php?bill=' . base64_encode($order->id . '||' . $order->date_created) . '">
                     ' . $lang['see_invoice'] . '
                  </a>
                  <a title="Export" target="_blank" href="' . URLROOT . 'ajax.php?bill=' . base64_encode($order->id . '||' . $order->date_created) . '&download=true">
                      ' . $lang['download_invoice'] . '
                  </a>
               </td>
               <td width="25"></td>
            </tr>
            <tr width="600">
				<td width="600" colspan="4" style="text-align:center;">
				</td>
			</tr>
          </table>
         <table width="600" style="margin:0 auto;background:#ffffff;">
            <tr width="600">
				<td width="600" colspan="4" style="text-align:center;">
				</td>
			</tr>
			<tr width="600">
               <td width="600" colspan="4" style="text-align:center;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $lang['we_process_order'] . '
                  </span>
               </td>
            </tr>
            <tr>
               <td colspan="4" width="600" height="20"></td>
            </tr>
            <tr>
               <td width="25"></td>
               <td width="275" style="text-align:left;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $user->first_name . ' ' . $user->last_name . '
                  </span>
               </td>
               <td width="275" style="text-align:right;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $lang['order_number'] . ' : #' . $order->id . '
                  </span>
               </td>
               <td width="25"></td>
            </tr>
            <tr>
               <td width="25"></td>
               <td width="275" style="text-align:left;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $user->address . '
                  </span>
               </td>
               <td width="275" style="text-align:right;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $lang['delivery_type'] . ' : ' . $order->delivery_type . '
                  </span>
               </td>
               <td width="25"></td>
            </tr>
            <tr>
               <td width="25"></td>
               <td width="275" style="text-align:left;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $user->city . ' (' . $user->state . ') ' . $user->zipcode . '
                  </span>
               </td>
               <td width="275" style="text-align:right;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     Date : ' . $order->date_created . '
                  </span>
               </td>
               <td width="25"></td>
            </tr>
            <tr>
               <td width="25"></td>
               <td width="275" style="text-align:right;">

               </td>
               <td width="275" style="text-align:right;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . ($order->delivery_type == "Livraison" ? $lang['delivery_date'] . ' : ' . $order->date_delivery : '') . '
                  </span>
               </td>
               <td width="25"></td>
            </tr>
            <tr>
               <td width="25"></td>
               <td width="275" style="text-align:right;">

               </td>
               <td width="275" style="text-align:right;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $lang['payment_methods'] . ": " . $order->payment_method . '
                  </span>
               </td>
               <td width="25"></td>
            </tr>
            <tr>
               <td colspan="4" width="600" height="20"></td>
            </tr>
         </table>
      ';
			if ($order->delivery_type == "Livraison") {
				$html .= '
        <table width="600" style="margin:0 auto;background:#ffffff;">
            <tr>
               <td colspan="4" width="600" height="20"></td>
            </tr>
            <tr>
               <td width="25"></td>
               <td width="550" colspan="2"style="text-align:center;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $lang['we_delivery_to_this_address'] . '
                  </span>
               </td>
               <td width="25"></td>
            </tr>
            <tr>
               <td colspan="4" width="600" height="20"></td>
            </tr>
         </table>
         <table width="600" style="margin:0 auto;background:#ffffff;">
            <tr>
               <td colspan="4" width="600" height="20"></td>
            </tr>
            <tr>
               <td width="25"></td>
               <td width="275" style="text-align:left;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $delivery->first_name . ' ' . $delivery->last_name . '
                  </span>
               </td>
               <td width="275" style="text-align:left;">

               </td>
               <td width="25"></td>
            </tr>
            <tr>
               <td width="25"></td>
               <td width="275" style="text-align:left;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $delivery->address . '
                  </span>
               </td>
               <td width="275" style="text-align:left;">

               </td>
               <td width="25"></td>
            </tr>
            <tr>
               <td width="25"></td>
               <td width="275" style="text-align:left;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $delivery->city . ' (' . $delivery->region . ') ' . $delivery->zipcode . '
                  </span>
               </td>
               <td width="275" style="text-align:center;">

               </td>
               <td width="25"></td>
            </tr>
            <tr>
               <td colspan="4" width="600" height="20"></td>
            </tr>
         </table>
         ';

			}
			$html .= '
         <table width="600" style="margin:0 auto;background:#ffffff;">
            <tr>
               <td colspan="4" width="600" height="20"></td>
            </tr>
            <tr>
               <td width="25"></td>
               <td colspan="2" width="550" style="text-align:center;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $lang['summary_order'] . '
                  </span>
               </td>
               <td width="25"></td>
            </tr>
            <tr>
               <td colspan="4" width="600" height="20"></td>
            </tr>
         </table>
         <table width="600" border="1" style="border-collapse:collapse;border-spacing:0;margin:0 auto;background:#ffffff;">

            <tr>
               <td width="125" style="text-align:center; padding: 10px;">
                  ' . $lang['products'] . '
               </td>
               <td width="125" style="text-align:center; padding: 10px;">
                  ' . $lang['unit_price'] . '
               </td>
               <td width="125" style="text-align:center; padding: 10px;">
                  ' . $lang['quantity'] . '
               </td>
               <td width="125" style="text-align:center; padding: 10px;">
                  ' . $lang['sub_total'] . '
               </td>
            </tr>
      ';
			//gather products in order

			$qproducts = $db->query(
				"
         SELECT tbl_products_orders.*, tbl_products.title, tbl_products.title_en, tbl_products.texte FROM tbl_products_orders
         LEFT JOIN tbl_products
         ON tbl_products.id = tbl_products_orders.fk_products
         WHERE tbl_products_orders.fk_orders = :order
      "
			);
			$db->bind($qproducts, ":order", $bill->id);
			$db->execute($qproducts);
			if ($db->rowCount($qproducts) > 0) {

				while ($product = $db->fetch($qproducts)) {
					if (strtolower($product->position) == "droite") {
						$lposition = "right";
					} else {
						$lposition = "left";
					}
					$html .= '
            <tr>
               <td width="125" style="text-align:center; padding: 10px;">
                  ' . $product->title . (isset($product->image) && $product->image !== "0" ? " (Personnalisé à " . $product->position . ")" : "") . ' / ' . $product->title_en . (isset($product->image) && $product->image !== "0" ? " (Personalized at " . $lposition . ")" : "") . '<br>
				  '.$product->texte.'
               </td>
               <td width="125" style="text-align:center; padding: 10px;">
                  ' . number_format($product->price, 2, ".", "") . ' $
               </td>
               <td width="125" style="text-align:center; padding: 10px;">
                  ' . $product->quantity . '
               </td>
               <td width="125" style="text-align:center; padding: 10px;">
                  ' . number_format(($product->price * $product->quantity), 2, ".", "") . ' $
               </td>
            </tr>
            ';
				}
			}
			$html .= '
         </table>
         <table width="600" style="margin:0 auto;background:#ffffff;">

            <tr>
               <td colspan="4">

               </td>
            </tr>
            <tr>
               <td colspan="4">
                  <br>
               </td>
            </tr>
            <tr>
               <td colspan="4">

               </td>
            </tr>
         </table>
         <table width="600" border="1" style="border-collapse:collapse;border-spacing:0;margin:0 auto;background:#ffffff;">


            <tr>
               <td width="300" style="text-align:center;">
                  ' . $lang['sub_total'] . '
               </td>
               <td width="300" style="text-align:center;">
                  ' . number_format($order->sub_total, 2, ".", "") . ' $
               </td>
            </tr>
         '.($order->rebate > 0 ? '
         <tr>
            <td width="300" style="text-align:center;">
               Rabais/discount
            </td>
            <td width="300" style="text-align:center;">
               ' . number_format((-1*$order->rebate), 2, ".", "") . ' $
            </td>
         </tr>' : '').'
            <tr>
               <td width="300" style="text-align:center;">
                  ' . $lang['delivery_cost'] . '
               </td>
               <td width="300" style="text-align:center;">
                  ' . number_format($order->shipping, 2, ".", "") . ' $
               </td>
            </tr>
            <tr>
               <td width="300" style="text-align:center;">
                  TPS (724837737 RT0001)
               </td>
               <td width="300" style="text-align:center;">
                  ' . number_format($order->tps, 2, ".", "") . ' $
               </td>
            </tr>
            <tr>
               <td width="300" style="text-align:center;">
                 TVQ (1227581103 TQ0001)
               </td>
               <td width="300" style="text-align:center;">
                  ' . number_format($order->tvq, 2, ".", "") . ' $
               </td>
            </tr>
            <tr>
               <td width="300" style="text-align:center;">
                 Total
               </td>
               <td width="300" style="text-align:center;">
                  ' . number_format($order->final_price, 2, ".", "") . ' $
               </td>
            </tr>
            <tr>
               <td width="300" style="text-align:center;">
                 ' . $lang['amount_to_pay'] . '
               </td>
               <td width="300" style="text-align:center;">
                  ' . number_format($order->amount_to_pay, 2, ".", "") . ' $
               </td>
            </tr>
         </table>
         <table width="600" style="border-collapse:collapse;border-spacing:0;background:#ffffff;">

            <tr>
               <td colspan="2">

               </td>
            </tr>
            <tr>
               <td colspan="2">
                  <br>
               </td>
            </tr>
            <tr>
               <td colspan="2">

               </td>
            </tr>
         </table>
         ';
		}

		if ($template == 'payment_bill') {
			$html .= ' <table><tr><td>Paiement # ' . $bill->id . '</td></tr></table>';
		}

		if ($template == 'payment_bill_credit') {
			$html .= ' <table><tr><td>Paiement solde</td></tr></table>';
		}

		$html .= '
		<table width="600" style="margin:0 auto;background:#111111;">
         <tr>
            <td colspan="4" width="600" height="20"></td>
         </tr>
         <tr>
            <td width="25"></td>
            <td width="550" colspan="2" style="text-align:center;">
               <span style="display:inline-block;margin:0;padding:0;font-family:arial, sans-serif;color:#fff;font-size:12px;line-height:1.4;font-weight:400;">
                  Suivez-nous
               </span><br/>
               <span style="display:inline-block;margin:0;padding:0;">
                 <a href="#"><img src="' . URLROOT . 'images/fb.jpg" width="17" height="17" alt=""></a>&nbsp;&nbsp;<a href="#"><img src="' . URLROOT . 'images/tw.jpg" width="17" height="17" alt=""></a>&nbsp;&nbsp;<a href="#"><img src="' . URLROOT . 'images/in.jpg" width="17" height="17" alt=""></a>
               </span><br/><br/>
               <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#bebdbe;font-size:10px;line-height:1.4;font-weight:400;">
               <span style="text-transform:uppercase; font-weight:500">© '. date('Y') .' Dragon média inc.</span>
                  Tous droits réservés
               </span>
            </td>
            <td width="25"></td>
         </tr>
         <tr>
            <td colspan="4" width="600" height="20"></td>
         </tr>
      </table>';
		$email = $user->email;
		$mail = htmlspecialchars($email, ENT_QUOTES, 'UTF-8');
		if (!preg_match("#^[a-z0-__PRODUCTSID__._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) {
			$passage_ligne = "\r\n";
		} else {
			$passage_ligne = "\n";
		}

		//gather user info
		$query_user = $db->query(
			"
         SELECT *, tbl_users.id as id FROM tbl_users
         LEFT JOIN tbl_users_meta as meta ON meta.id = tbl_users.fk_user_meta
         WHERE tbl_users.username = :email
      "
		);
		$db->bind($query_user, ":email", $email);
		$row = $db->single($query_user);
		$first_name = htmlspecialchars($row->first_name, ENT_QUOTES, 'UTF-8');
		$last_name = htmlspecialchars($row->last_name, ENT_QUOTES, 'UTF-8');

		$subject = $lang['new_order'] . " (#" . $order->id . ")  📦 ";
		$de = CLIENT_EMAIL;
		$de_name = "Masquez-vous";
		$to = $email;
		$to_name = $user->first_name . " " . $user->last_name;
		// send mail
		if (DragonMediaMail($subject, '<html>' . $html . '</html>', $de, $de_name, $to, $to_name)) {
			DragonMediaMail($subject, '<html>' . $html . '</html>', $de, $de_name, "orders@masquez-vous-qc.ca", "Orders");
			return true;
		} else {
			return false;
		}
	}

	function char($text)
	{
		$text = htmlentities($text, ENT_NOQUOTES, "UTF-8");
		$text = htmlspecialchars_decode($text);
		return $text;
	}

	function thumbs_exists($video_id, $type = "youtube", $i = 1)
	{
		$actual = $i;
		if ($actual > 5) {
			return false;
		}
		if ($type == "youtube") {
			$thumbnail[1] = "https://img.youtube.com/vi/" . $video_id . "/maxresdefault.jpg";
			$thumbnail[2] = "https://img.youtube.com/vi/" . $video_id . "/mqdefault.jpg";
			$thumbnail[3] = "https://img.youtube.com/vi/" . $video_id . "/default.jpg";
			$thumbnail[4] = "https://img.youtube.com/vi/" . $video_id . "/0.jpg";
			$thumbnail[5] = "https://img.youtube.com/vi/" . $video_id . "/1.jpg";
		} else if ($type == "vimeo") {
			$hash = unserialize(file_get_contents("https://vimeo.com/api/v2/video/" . $video_id . ".php"));
			$thumbnail[1] = $hash[0]['thumbnail_large'];
			$thumbnail[2] = $hash[0]['thumbnail_medium'];
			$thumbnail[3] = $hash[0]['thumbnail_small'];
		}

		$curl = curl_init();
		curl_setopt_array(
			$curl, array(
			CURLOPT_URL => $thumbnail[$actual], CURLOPT_HEADER => true, CURLOPT_RETURNTRANSFER => true, CURLOPT_NOBODY => true
		)
		);

		$header = explode("\n", curl_exec($curl));
		curl_close($curl);
		// var_dump($thumbnail[$actual]);
		//If maxres image exists do something with it.
		if (strpos($header[0], '200') !== false) {
			if (!is_array($thumbnail[$actual])) {
				// var_dump($thumbnail[$actual]);
				return $thumbnail[$actual];
			} else {
				// var_dump($thumbnail[$actual]);
				$thumbnail[$actual] = explode(',', $thumbnail[0]);
				return $thumbnail[0];
			}
		} else {
			$actual++;
			return thumbs_exists($video_id, $type, $actual);
		}
	}

	function getMyVideoFunction($id, $l = "")
	{

		$Tess_db = new Database();

		// $iaCore = databaseTess::getTess();
		$sqlq = "SELECT * FROM tbl_pages WHERE id = :id";
		$sqls = $Tess_db->query($sqlq);
		$Tess_db->bind($sqls, ":id", $id);
		$sql = $Tess_db->single($sqls);
		$join = $Tess_db->query("SELECT t.*, i.name FROM tbl_pages_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_pages = :id_page");
		$Tess_db->bind($join, ":id_page", $id);
		$Tess_db->execute($join);
		while ($joins = $Tess_db->fetch($join)) {
			if(isset($sql->{$joins->name}) && $joins->name != "")
				$sql->{$joins->name} = $joins->value;
		}
		// var_dump($join);
		if (strpos($sql->{'video_url' . $l}, 'youtu') !== false) {
			$arrayId = explode('?v=', $sql->{'video_url' . $l});
			$idbg = end($arrayId);
			$idbg = explode('&', $idbg);
			$idbg = $idbg[0];
			$thumbs = thumbs_exists($idbg, 'youtube');
		} else if (strpos($sql->{'video_url' . $l}, 'vimeo') !== false) {

			$arrayId = explode('/', $sql->{'video_url' . $l});
			$idbg = end($arrayId);
			$thumbs = thumbs_exists($idbg, 'vimeo');
		}

		$content = '

      <section class="video" id="video">

      <div class="video_iframe" id="video_iframe"></div>

      <div class="video_staticBg" id="video_staticBg"></div>

      <!--svg focusable="false" role="presentational" class="svg-crosshatch"><use xlink:href="#crosshatch"></use></svg-->



      <div class="video_column">

      <div class="video_column-center"  id="video_column_center">

      <div class=video_slate>

      <div class=logo_bg></div>

      </div>

      </div>

      </div>

      <!--div class=report id=report></div-->

      </section>
      <button class=video_btn aria-label="Pause video">
      <svg focusable="false" role="presentational" class="svg-pausePlay"><use xlink:href="#icon-pause"></use></svg>
      </button>


      <svg class=visually-hidden>
      <defs>

      <symbol viewBox="0 0'.__PAYMENTFAILID__ .'__PAYMENTFAILID__" id="icon-play">
      <path d="M25'.__CONDITIONSID__  .'.2l-__404ID__ 7v-__404ID__l__404ID__ 7z"></path>
      </symbol>

      <symbol viewBox="0 0'.__PAYMENTFAILID__ .'__PAYMENTFAILID__" id="icon-pause">
      <path d="M20 11h2v12h-2V11zm-8 0h2v12h-2V11z"></path>
      </symbol>

      <pattern id="crosshatch1" patternUnits="userSpaceOnUse" width="4" height="4" patternTransform="rotate(45)">
      <line stroke="#000" x1="2" y1="0" x2="2" y2="4" />
      <line stroke="#000" x1="0" y1="2" x2="4" y2="2" />
      </pattern>

      <rect id="crosshatch" width="100%" height="100%" fill="url(#crosshatch1)" />

      </defs>
      </svg>
      <script>

          // Support down to IE__PRODUCTSID__.

          // Device is set via a server detection script
          // Just emulated here:

          var device = window.innerWidth >= 600 ? "desktop" : "mobile";

          //device = "mobile";

          // I don\'t know how to get this function out of the global scope! - Help anyone?

          function onYouTubePlayerAPIReady() {
              videoHeader.onYouTubePlayerAPIReady();
          }


          var videoHeader = (function () {

              if (device === "mobile") {return;}

              var videoBlock = document.getElementById("video");
              if (!videoBlock) {return;}

              var video_iframe_id = "video_iframe";
              var video_iframe = document.getElementById(video_iframe_id);
              if (!video_iframe) {return;}

              var video_staticBg = document.getElementById("video_staticBg");
              if (!video_staticBg) {return;}

              var videos = [
                  {
                      videoId: "' . $idbg . '",
                      startSeconds: 15,
                      //endSeconds: 60.3, // May need to adjust animation timing too
                      suggestedQuality: "large"
                  }
                  // Will handle more videos defined here
              ];

              var noOfVideos = videos.length;
              var currentVideo = 0;

              var player;
              var isPaused = false;

              var playerDefaults = {
              autoplay: 1,
              autohide: 1,
              modestbranding: 0,
              rel: 0,
              showinfo: 0,
              controls: 0,
              disablekb: 1,
              enablejsapi: 1,
              iv_load_policy: 3
              };


              // Debounce window resize - https://john-dugan.com/javascript-debounce/
              var debounce=function(e,t,n){var a;return function(){var r=this,i=arguments,o=function(){a=null,n||e.apply(r,i)},s=n&&!a;clearTimeout(a),a=setTimeout(o,t||200),s&&e.apply(r,i)}};


              var embedScripts = (function () {

                  // Append YouTube iframe video API
                  // Calls onYouTubePlayerAPIReady() in the global scope when ready

                  var videoScript = document.createElement("script");
                  videoScript.setAttribute("async", true);
                  videoScript.src = "https://www.youtube.com/player_api";
                  document.head.appendChild(videoScript);

              })();


              function playVideo () {
                  if (!isPaused) {
                      // console.log("Playing: " + currentVideo);
                      player.loadVideoById(videos[currentVideo]);
                      if (videos[currentVideo].startSeconds > 0) {
                          player.seekTo(videos[currentVideo].startSeconds);
                      }
                  }
              }


              function onPlayerStateChange(e) {

                  requestAnimationFrame(function() {

                      // Sequence appears to be:
                      // -1
                      //  3
                      // -1
                      //  3
                      //  1

                      //  2
                      //  0
                      // -1
                      //  0

                      switch (e.data) {

                          case -1 : {break;} // Unstarted

                          case 0 : { // Video has ended

                              video_iframe.classList.remove("-js-active");
                              video_staticBg.classList.remove("-js-hidden");

                              playVideo();
                              break;
                          }

                          case 1 : { // Video is playing

                              video_iframe.classList.add("-js-active");
                              video_staticBg.classList.add("-js-hidden");

                              // Increment ready for the next video
                              currentVideo = (currentVideo === videos.length - 1) ? 0 : currentVideo + 1;
                              break;
                          }

                          case'.__CONTACTID__.': {break;} // Video is paused
                          case 3 : {break;} // Video is buffering
                          case 4 : {break;} // Unknown
                          case 5 : {break;} // Video is queued
                      }

                  });

              }


              function onPlayerReady() {
                  player.loadVideoById(videos[currentVideo]);
                  player.mute();
              }


              function onYouTubePlayerAPIReady() {

                  // Called from globally scoped function of the same name.
                  player = new YT.Player(video_iframe_id, {
                      events: {
                          onReady: onPlayerReady,
                          onStateChange: onPlayerStateChange
                      },
                      playerVars: playerDefaults
                  });
              }



              // The play / pause button

              function toggleVideoPause(e) {

                  var btn = e.target;
                  var video_staticBg = document.querySelector(".video_staticBg");
                  if (!btn || !video_staticBg) {return;}

                  var txt = btn.getAttribute("aria-label");
                  var isPlaying = txt.match("Pause");

                  requestAnimationFrame(function () {

                      if (isPlaying) {

                          video_staticBg.classList.add("-js-paused");
                          btn.setAttribute("aria-label", txt.replace("Pause", "Play"));
                          player.pauseVideo();
                          btn.querySelector("use").setAttributeNS("https://www.w3.org/19__PRODUCTSID____PRODUCTSID__/xlink", "href", "#icon-play");
                          isPaused = true;

                      } else {

                          btn.setAttribute("aria-label", txt.replace("Play", "Pause"));
                          player.playVideo();
                          btn.querySelector("use").setAttributeNS("https://www.w3.org/19__PRODUCTSID____PRODUCTSID__/xlink", "href", "#icon-pause");
                          isPaused = false;
                          video_staticBg.classList.remove("-js-paused");

                      }

                  });
              }

              var video_btn = document.querySelector(".video_btn");
              if (video_btn) {
              video_btn.addEventListener("click", toggleVideoPause, false);
              }

              // https://plainjs.com/javascript/styles/get-the-position-of-an-element-relative-to-the-document-24/
              function offset(el) {
                  var rect = el.getBoundingClientRect();
                  var scrollLeft = window.pageXOffset
                  || document.documentElement.scrollLeft;
                  var scrollTop = window.pageYOffset
                  || document.documentElement.scrollTop;
                  return {top: rect.top + scrollTop, left: rect.left + scrollLeft, width: rect.width, height: rect.height}
              }


              // Adjust width and height of the iframe to accomodate content height and aspect ratio.

              function report (str) {
                  var report = document.getElementById("report");
                  if (report) {
                      report.innerHTML = str;
                  }
              }


              function aspectRatio() {

                  var LTH = offset(videoBlock);

                  var screenW = window.innerWidth;
                  var screenH = LTH.height;
                  var videoW = screenW;
                  var videoH = screenH;

                  if (screenH *'. __SITEMAPID__  .'/ __PRODUCTSID__ < screenW) {
                      videoW = screenW;
                      videoH = screenW * __PRODUCTSID__ / __SITEMAPID__;
                  } else {
                      videoW = screenH *'. __SITEMAPID__  .'/ __PRODUCTSID__;
                      videoH = screenH;
                  }

                  // 3 decimal places is accurate enough for pixels
                  videoW = Math.round(videoW * 1000) / 1000;
                  videoH = Math.round(videoH * 1000) / 1000;

                  return {w: videoW, h: videoH};
              }


              // The video aspect ratio closest to full-screen
              function setVideoBlockSize() {

                  requestAnimationFrame(function() {

                      var AR = aspectRatio();

                      report("AR.h: " + AR.h + "px");

                      video_iframe.setAttribute("style", "width: " + AR.w + "px; height: " + AR.h + "px;");

                  });
              }


              setVideoBlockSize();
              window.addEventListener("resize", debounce(setVideoBlockSize, 100, false), false);


              // Reduce motion
              // if (window.matchMedia("prefers-reduced-motion").matches) {
              //   toggleVideo();
              // }

              return {
              onYouTubePlayerAPIReady: onYouTubePlayerAPIReady
              }

          })();
      </script>

      ';


		return $content;
	}

	function Cut($string, $max_length)
	{
		$string = strip_tags($string);

		if (strlen($string) > $max_length) {
			$string = substr($string, 0, $max_length);
			$pos = strrpos($string, " ");
			if ($pos === false) {
				return substr($string, 0, $max_length);
			}
			return substr($string, 0, $pos);
		} else {
			return $string;
		}
	}

	function object_to_array($data)
	{
		if (is_array($data) || is_object($data)) {
			$result = array();
			foreach ($data as $key => $value) {
				$result[$key] = object_to_array($value);
			}
			return $result;
		}
		return $data;
	}

	// function to get size of website folder
	function convertToReadableSize($size)
	{
		$base = log($size) / log(1024);
		$suffix = array("", "KB", "MB", "GB", "TB");
		$f_base = floor($base);
		return round(pow(1024, $base - floor($base)), 2) . '||' . $suffix[$f_base] . ' / 50 GB';
	}

	function getEmptyValue($table)
	{
		$db = new Database();
		try {
			$query = $db->query("SELECT * FROM " . $table . " LIMIT 1");
			$query = $db->single($query);
			foreach ($query as $key => $value) {
				$query->$key = "";
			}
			return $query;
		} catch (PDOException $e) {
			return false;
		}
	}

	function getUrlLang($id, $lang = "", $only_slug = false)
	{
		global $l, $language;
		//Initiate db
		if(!is_object($language)){
			$language = new Lang();
			$language->lang = "fr";
		}
		//$language = new Lang();
		$lang = ($lang !== "" ? $lang : $language->lang);
		$db = new Database();
		$prefix_lang = '' . (!empty($lang) ? ($lang == 'fr' ? 'fr' : 'en') : $language)."/";
		$suffix = !empty($lang) ? ($lang == 'fr' ? '' : '_en') : $l;
		if(!MULTI_LANG){
			$prefix_lang = "";
		}
		if(!MULTI_LANG){
			if($id == 1 && ($lang == "" || $lang == "fr")){
				return "";
			} 
		}
		
		if($id == 0  && ( $language->lang == "fr" ||  $language->lang == "en")){ 
			$lang = $language->lang;
			$prefix_lang = '' . (!empty($lang) ? ($lang == 'fr' ? 'fr' : 'en') : $language)."/";
			$suffix = !empty($lang) ? ($lang == 'fr' ? '' : '_en') : $l;
			return "";
		}elseif($id == 0){
			$lang = $language->lang;
			$prefix_lang = '' . (!empty($lang) ? ($lang == 'fr' ? 'fr' : 'en') : $language)."/";
			$suffix = !empty($lang) ? ($lang == 'fr' ? '' : '_en') : $l;
			return "";
		}
		try {
			$r = $db->query(
				"SELECT tbl_custom_urls.url_id" . $suffix . " ,
                tbl_pages.parent,
                tbl_pages.root,
                tbl_pages.id
                FROM tbl_custom_urls
                LEFT JOIN tbl_pages
                ON tbl_custom_urls.page_id = tbl_pages.id
                LEFT JOIN ( SELECT id as idtess,has_slug FROM tess_controller) tc
                ON tc.idtess = tbl_pages.type
                WHERE tbl_custom_urls.page_id = :id
                AND tbl_pages.online = 1
                AND tc.has_slug = 1
                AND  tbl_custom_urls.url_id" . $suffix . " is not null
                ORDER BY tbl_custom_urls.date_creation DESC LIMIT 1"
			);
			$db->bind($r, ':id', $id);
			$r = $db->single($r);

			if (!$only_slug) {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} != '0')
						return $prefix_lang . '' . getUrlLang($r->{'parent'}, $lang, 'recursive') . $r->{'url_id' . $suffix} . '/'; else if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} == '0')
						return $prefix_lang . '' . $r->{'url_id' . $suffix} . '/'; else
						return $prefix_lang . '' . getUrlLang($r->{'parent'}, $lang, 'recursive') . $id . '/';
				} else {
					$parent = $db->query("SELECT parent FROM tbl_pages WHERE id = :id");
					$db->bind($parent, ':id', $id);
					$parent = $db->single($parent);
					if ($parent)
						return $prefix_lang . '' . getUrlLang($parent->{'parent'}, $lang, 'recursive') . $id . '/'; else
						return $prefix_lang . '' . $id . '/';
				}
			} else {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}))
						return ($only_slug == 'recursive' ? getUrlLang($r->parent, $lang, 'recursive') : '') . $r->{'url_id' . $suffix} . '/'; else
						return ($only_slug == 'recursive' ? getUrlLang($r->parent, $lang, 'recursive') : '') . $id . '/';
				} else {
					if ($id) {
						$parent = $db->query("SELECT parent FROM tbl_pages JOIN tbl_custom_urls t on tbl_pages.id = t.page_id WHERE tbl_pages.id = :id AND");
						$db->bind($parent, ':id', $id);
						$parent = $db->resultSet($parent);
						if ($page->{'parent'})
							return getUrlLang($parent->{'parent'}, $lang, 'recursive');
					}
				}
			}
		} catch (PDOException $e) {
			return false;
		}
	}

	function getUrlLangProducts($id, $lang = "", $only_slug = false)
	{
		global $l, $language;
		//Initiate db
		$language = new Lang();
		$lang = ($lang ? $lang : $language->lang);
		$db = new Database();
		$id_page_products = __PRODUCTSID__;
		$query_getproducts = $db->query("SELECT url_id" . ($lang !== "fr" ? "_" . $lang : "") . " as geturl FROM tbl_custom_urls WHERE page_id = :id");
		$db->bind($query_getproducts, ":id", $id_page_products);
		$products_url = $db->single($query_getproducts)->geturl;
		// $prefix_lang = '/' . (!empty($lang) ? ($lang == 'fr' ? 'fr' : 'en') : $language) . "/" . $products_url;
		$prefix_lang = $products_url;
		// $suffix = !empty($lang) ? ($lang == 'fr' ? '' : '_en') : $l;
		$suffix = "";


		try {
			$r = $db->query(
				"SELECT tbl_products_urls.url_id" . $suffix . " ,
                tbl_products.parent,
                tbl_products.root,
                tbl_products.id
                FROM tbl_products_urls
                LEFT JOIN tbl_products
                ON tbl_products_urls.product_id = tbl_products.id
                LEFT JOIN ( SELECT id as idtess,has_slug FROM tess_controller) tc
                ON tc.idtess = tbl_products.type
                WHERE tbl_products_urls.product_id = :id
                AND tbl_products.online = 1
                AND tc.has_slug = 1
                AND  tbl_products_urls.url_id" . $suffix . " is not null
                ORDER BY tbl_products_urls.date_creation DESC LIMIT 1"
			);
			$db->bind($r, ':id', $id);
			$r = $db->single($r);

			if (!$only_slug) {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} != '0')
						return $prefix_lang . '/' . getUrlLangProducts($r->{'parent'}, $lang, 'recursive') . $r->{'url_id' . $suffix} . '/'; else if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} == '0')
						return $prefix_lang . '/' . $r->{'url_id' . $suffix} . '/'; else
						return $prefix_lang . '/' . getUrlLangProducts($r->{'parent'}, $lang, 'recursive') . $id . '/';
				} else {
					$parent = $db->query("SELECT parent FROM tbl_products WHERE id = :id");
					$db->bind($parent, ':id', $id);
					$parent = $db->single($parent);
					if ($parent)
						return $prefix_lang . '/' . getUrlLangProducts($parent->{'parent'}, $lang, 'recursive') . $id . '/'; else
						return $prefix_lang . '/' . $id . '/';
				}
			} else {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}))
						return ($only_slug == 'recursive' ? getUrlLangProducts($r->parent, $lang, 'recursive') : '') . $r->{'url_id' . $suffix} . '/'; else
						return ($only_slug == 'recursive' ? getUrlLangProducts($r->parent, $lang, 'recursive') : '') . $id . '/';
				} else {
					if ($id) {
						$parent = $db->query("SELECT parent FROM tbl_products JOIN tbl_products_urls t on tbl_products.id = t.product_id WHERE tbl_products.id = :id AND");
						$db->bind($parent, ':id', $id);
						$parent = $db->resultSet($parent);
						if ($page->{'parent'})
							return getUrlLangProducts($parent->{'parent'}, $lang, 'recursive');
					}
				}
			}
		} catch (PDOException $e) {
			return false;
		}
	}

	/* ------------------ URL BLOGS --------------- */
	function getUrlLangBlogs($id, $lang = "", $only_slug = false)
	{
		global $l, $language;
		//Initiate db
		$language = new Lang();
		$lang = ($lang ? $lang : $language->lang);
		$db = new Database();
		$id_page_products = __BLOGID__;
		$query_getproducts = $db->query("SELECT url_id" . ($lang !== "fr" ? "_" . $lang : "") . " as geturl FROM tbl_custom_urls WHERE page_id = :id");
		$db->bind($query_getproducts, ":id", $id_page_products);
		$products_url = $db->single($query_getproducts)->geturl;
		$prefix_lang = '/' . (!empty($lang) ? ($lang == 'fr' ? 'fr' : 'en') : $language) . "/" . $products_url;
		$suffix = !empty($lang) ? ($lang == 'fr' ? '' : '_en') : $l;


		try {
			$r = $db->query(
				"SELECT tbl_blogs_urls.url_id" . $suffix . " ,
                tbl_blogs.parent,
                tbl_blogs.root,
                tbl_blogs.id
                FROM tbl_blogs_urls
                LEFT JOIN tbl_blogs
                ON tbl_blogs_urls.blog_id = tbl_blogs.id
                LEFT JOIN ( SELECT id as idtess,has_slug FROM tess_controller) tc
                ON tc.idtess = tbl_blogs.type
                WHERE tbl_blogs_urls.blog_id = :id
                AND tbl_blogs.online = 1
                AND tc.has_slug = 1
                AND  tbl_blogs_urls.url_id" . $suffix . " is not null
                ORDER BY tbl_blogs_urls.date_creation DESC LIMIT 1"
			);
			$db->bind($r, ':id', $id);
			$r = $db->single($r);

			if (!$only_slug) {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} != '0')
						return $prefix_lang . '/' . getUrlLangBlogs($r->{'parent'}, $lang, 'recursive') . $r->{'url_id' . $suffix} . '/';

				} else {
					$parent = $db->query("SELECT parent FROM tbl_blogs WHERE id = :id");
					$db->bind($parent, ':id', $id);
					$parent = $db->single($parent);
				}
			} else {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}))
						return ($only_slug == 'recursive' ? getUrlLangBlogs($r->parent, $lang, 'recursive') : '') . $r->{'url_id' . $suffix} . '/';

				} else {
					if ($id) {
						$parent = $db->query("SELECT parent FROM tbl_blogs JOIN tbl_blogs_urls t on tbl_blogs.id = t.blog_id WHERE tbl_blogs.id = :id AND");
						$db->bind($parent, ':id', $id);
						$parent = $db->resultSet($parent);
						if ($page->{'parent'})
							return getUrlLangBlogs($parent->{'parent'}, $lang, 'recursive');
					}
				}
			}
		} catch (PDOException $e) {
			return false;
		}
	}

	/* ------------------ URL contests --------------- */
	function getUrlLangContests($id, $lang = "", $only_slug = false)
	{
		global $l, $language;
		//Initiate db
		$language = new Lang();
		$lang = ($lang ? $lang : $language->lang);
		$db = new Database();
		$id_page_products = __CONTESTSID__;
		$query_getproducts = $db->query("SELECT url_id" . ($lang !== "fr" ? "_" . $lang : "") . " as geturl FROM tbl_custom_urls WHERE page_id = :id");
		$db->bind($query_getproducts, ":id", $id_page_products);
		$products_url = $db->single($query_getproducts)->geturl;
		$prefix_lang = '/' . (!empty($lang) ? ($lang == 'fr' ? 'fr' : 'en') : $language) . "/" . $products_url;
		$suffix = !empty($lang) ? ($lang == 'fr' ? '' : '_en') : $l;


		try {
			$r = $db->query(
				"SELECT tbl_contests_urls.url_id" . $suffix . " ,
                tbl_contests.parent,
                tbl_contests.root,
                tbl_contests.id
                FROM tbl_contests_urls
                LEFT JOIN tbl_contests
                ON tbl_contests_urls.contest_id = tbl_contests.id
                WHERE tbl_contests_urls.contest_id = :id
                AND  tbl_contests_urls.url_id" . $suffix . " is not null
                ORDER BY tbl_contests_urls.date_creation DESC LIMIT 1"
			);
			$db->bind($r, ':id', $id);
			$r = $db->single($r);

			if (!$only_slug) {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} != '0')
						return $prefix_lang . '/' . getUrlLangContests($r->{'parent'}, $lang, 'recursive') . $r->{'url_id' . $suffix} . '/'; else if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} == '0')
						return $prefix_lang . '/' . $r->{'url_id' . $suffix} . '/'; else
						return $prefix_lang . '/' . getUrlLangContests($r->{'parent'}, $lang, 'recursive') . $id . '/';
				} else {
					$parent = $db->query("SELECT parent FROM tbl_contests WHERE id = :id");
					$db->bind($parent, ':id', $id);
					$parent = $db->single($parent);
					if ($parent)
						return $prefix_lang . '/' . getUrlLangContests($parent->{'parent'}, $lang, 'recursive') . $id . '/'; else
						return $prefix_lang . '/' . $id . '/';
				}
			} else {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}))
						return ($only_slug == 'recursive' ? getUrlLangContests($r->parent, $lang, 'recursive') : '') . $r->{'url_id' . $suffix} . '/'; else
						return ($only_slug == 'recursive' ? getUrlLangContests($r->parent, $lang, 'recursive') : '') . $id . '/';
				} else {
					if ($id) {
						$parent = $db->query("SELECT parent FROM tbl_contests JOIN tbl_contests_urls t on tbl_contests.id = t.contest_id WHERE tbl_contests.id = :id AND");
						$db->bind($parent, ':id', $id);
						$parent = $db->resultSet($parent);
						if ($page->{'parent'})
							return getUrlLangContests($parent->{'parent'}, $lang, 'recursive');
					}
				}
			}
		} catch (PDOException $e) {
			return false;
		}
	}

	/* ------------------ URL Promotions --------------- */
	function getUrlLangPromotions($id, $lang = "", $only_slug = false)
	{
		global $l, $language;
		//Initiate db
		$language = new Lang();
		$lang = ($lang ? $lang : $language->lang);
		$db = new Database();
		$id_page_products = __PROMOSID__;
		$query_getproducts = $db->query("SELECT url_id" . ($lang !== "fr" ? "_" . $lang : "") . " as geturl FROM tbl_custom_urls WHERE page_id = :id");
		$db->bind($query_getproducts, ":id", $id_page_products);
		$products_url = $db->single($query_getproducts)->geturl;
		$prefix_lang = '/' . (!empty($lang) ? ($lang == 'fr' ? 'fr' : 'en') : $language) . "/" . $products_url;
		$suffix = !empty($lang) ? ($lang == 'fr' ? '' : '_en') : $l;


		try {
			$r = $db->query(
				"SELECT tbl_promotions_urls.url_id" . $suffix . " ,
                tbl_promotions.parent,
                tbl_promotions.root,
                tbl_promotions.id
                FROM tbl_promotions_urls
                LEFT JOIN tbl_promotions
                ON tbl_promotions_urls.promotion_id = tbl_promotions.id
                WHERE tbl_promotions_urls.promotion_id = :id
                AND  tbl_promotions_urls.url_id" . $suffix . " is not null
                ORDER BY tbl_promotions_urls.date_creation DESC LIMIT 1"
			);
			$db->bind($r, ':id', $id);
			$r = $db->single($r);

			if (!$only_slug) {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} != '0')
						return $prefix_lang . '/' . getUrlLangPromotions($r->{'parent'}, $lang, 'recursive') . $r->{'url_id' . $suffix} . '/'; else if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} == '0')
						return $prefix_lang . '/' . $r->{'url_id' . $suffix} . '/'; else
						return $prefix_lang . '/' . getUrlLangPromotions($r->{'parent'}, $lang, 'recursive') . $id . '/';
				} else {
					$parent = $db->query("SELECT parent FROM tbl_promotions WHERE id = :id");
					$db->bind($parent, ':id', $id);
					$parent = $db->single($parent);
					if ($parent)
						return $prefix_lang . '/' . getUrlLangPromotions($parent->{'parent'}, $lang, 'recursive') . $id . '/'; else
						return $prefix_lang . '/' . $id . '/';
				}
			} else {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}))
						return ($only_slug == 'recursive' ? getUrlLangPromotions($r->parent, $lang, 'recursive') : '') . $r->{'url_id' . $suffix} . '/'; else
						return ($only_slug == 'recursive' ? getUrlLangPromotions($r->parent, $lang, 'recursive') : '') . $id . '/';
				} else {
					if ($id) {
						$parent = $db->query("SELECT parent FROM tbl_promotions JOIN tbl_promotions_urls t on tbl_promotions.id = t.promotion_id WHERE tbl_promotions.id = :id AND");
						$db->bind($parent, ':id', $id);
						$parent = $db->resultSet($parent);
						if ($page->{'parent'})
							return getUrlLangPromotions($parent->{'parent'}, $lang, 'recursive');
					}
				}
			}
		} catch (PDOException $e) {
			return false;
		}
	}

	function getUrl($id, $lang = "", $only_slug = false)
	{
		global $l, $language;
		//Initiate db
		$language = new Lang();
		$lang = ($lang ? $lang : $language->lang);
		$db = new Database();

		$prefix_lang = '/' . (!empty($lang) ? ($lang == 'fr' ? 'fr' : 'en') : $language);
		$suffix = !empty($lang) ? ($lang == 'fr' ? '' : '_en') : $l;


		try {
			$r = $db->query(
				"SELECT tbl_custom_urls.url_id" . $suffix . " ,
                tbl_pages.parent,
                tbl_pages.root,
                tbl_pages.id
                FROM tbl_custom_urls
                LEFT JOIN tbl_pages
                ON tbl_custom_urls.page_id = tbl_pages.id
                WHERE tbl_custom_urls.page_id = :id
                AND  tbl_custom_urls.url_id" . $suffix . " is not null
                ORDER BY tbl_custom_urls.date_creation DESC LIMIT 1"
			);
			$db->bind($r, ':id', $id);
			$r = $db->single($r);

			if (!$only_slug) {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} != '1')
						return $prefix_lang . '/' . getUrl($r->{'parent'}, $lang, 'recursive') . $r->{'url_id' . $suffix} . '/'; else if (!empty($r->{'url_id' . $suffix}) && $r->{'parent'} == '1')
						return $prefix_lang . '/' . $r->{'url_id' . $suffix} . '/'; else
						return $prefix_lang . '/' . getUrl($r->{'parent'}, $lang, 'recursive') . $id . '/';
				} else {
					$parent = $db->query("SELECT parent FROM tbl_pages WHERE id = :id");
					$db->bind($parent, ':id', $id);
					$parent = $db->single($parent);
					if ($parent)
						return $prefix_lang . '/' . getUrl($parent->parent, $lang, 'recursive') . $id . '/'; else
						return $prefix_lang . '/' . $id . '/';
				}
			} else {
				if ($r) {
					if (!empty($r->{'url_id' . $suffix}))
						return ($only_slug == 'recursive' ? getUrl($r->parent, $lang, 'recursive') : '') . $r->{'url_id' . $suffix} . '/'; else
						return ($only_slug == 'recursive' ? getUrl($r->parent, $lang, 'recursive') : '') . $id . '/';
				} else {
					if ($id) {
						$parent = $db->query("SELECT parent FROM tbl_pages WHERE id = :id ");
						$db->bind($parent, ':id', $id);
						$parent = $db->resultSet($parent);
						if ($parent[0]->parent)
							return getUrl($parent[0]->parent, $lang, 'recursive');
					}
				}
			}
		} catch (PDOException $e) {
			return false;
		}
	}

	function array_to_object($arr)
	{
		$obj = new stdClass();
		foreach ($arr as $key => $val) {
			$obj->{$key} = $val;
		}
		return $obj;
	}



	function getInputsType()
	{
		$array_types = [
			/* Text, textarea & globals */
			"Text", "Textarea", "Color", "Datepicker", /* WYSIWYG */
			"WYSIWYG", /* UPLOAD IMAGE */
			"Upload Image", /* UPLOAD OTHER */
			"Upload Autre", /* Join relation tables */
			"Jointure page", /* Radio button (multiple with labels) */
			"Radio", /* Categories & checkboxes */
			"Cat&eacute;gorie", "Checkbox", /* Nice dual radio switch */
			"toggle", /* Categories radios */
			"Cat&eacute;gorie radio", /* SELECT */
			"Select"
		];
		return $array_types;
	}


	function getNotesPosition()
	{
		$array_positions = [
			"haut" => "En haut", "droit" => "À droite", "gauche" => "À gauche", "bas" => "En bas", "title" => "Dessous le title"
		];
		return $array_positions;
	}

 function pdf_header()
	{
		global $l, $lang, $language, $base_url, $nomDuSite;

		$html = '
      <page backtop="10mm" backleft="5mm" backright="5mm" backbottom="10mm" format="A3">
      <table width="1035" style="border-collapse:collapse;border-spacing:0;background:#111111;">
         <tr>
            <td colspan="3" width="1035" height="20"></td>
         </tr>
         <tr>
           <td width="20"></td>
               <td width="487.5" style="text-align:left;">

					<span style="display:block;margin:0 auto;padding:0;font-family:arial, sans-serif;color:#ffffff;font-size:12px;line-height:1.4;font-weight:400;">
                     Dragon Média
					</span><br>
					<span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#ffffff;font-size:12px;line-height:1.4;font-weight:400;">
                     993 rue sauvé
					</span><br>
					<span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#ffffff;font-size:12px;line-height:1.4;font-weight:400;">
                     Mascouche, Québec
					</span><br>
					<span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#ffffff;font-size:12px;line-height:1.4;font-weight:400;">
                     J7K 3L6
					</span>

               </td>
			   <td width="487.5" style="text-align:center;"><img src="' . URLROOT . 'images/logo/dragon.png" style="width: 200px" width="auto" height="200"></td>
			   <td width="20"></td>

         </tr>
         <tr>
            <td colspan="3" width="1035" height="20"></td>
         </tr>
      </table>
      ';

		return $html;
	}

	function pdf_footer()
	{
		global $l, $lang, $language, $base_url, $nomDuSite;

		$html = '
      <table width="1035" style="border-collapse:collapse;border-spacing:0;background:#111111;">
         <tr>
            <td colspan="3" width="1035" height="20"></td>
         </tr>
         <tr>
            <td width="20"></td>
            <td width="995" style="text-align:center;"> <span style="display:inline-block;margin:0;padding:0;">
                 <a href="#"><img src="' . URLROOT . 'images/fb.jpg" width="17" height="17" alt=""></a>&nbsp;&nbsp;<a href="#"><img src="' . URLROOT . 'images/tw.jpg" width="17" height="17" alt=""></a>&nbsp;&nbsp;<a href="#"><img src="' . URLROOT . 'images/in.jpg" width="17" height="17" alt=""></a>
               </span><br/><br/>

               <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#bebdbe;font-size:10px;line-height:1.4;font-weight:400;">
               <span style="text-transform:uppercase; font-weight:500">© ' . date('Y') . ' Dragon média inc.</span>
                  Tous droits réservés
               </span>
            </td>
         </tr>
         <tr>
            <td colspan="3" width="1035" height="20"></td>
         </tr>
      </table>
      ';

		$html .= '
      </page>
      ';

		return $html;
	}


	function create_pdf_template($template, $download = false, $mail = true, $bill = [], $type = "bill")
	{
		global $l, $lang, $language, $Tess, $doc_root;

		require "plugins/html2pdf/html2pdf.class.php";
		$db = new Database();
		$html = pdf_header();
		if($type=="bill"){
		//get order info
		$qorders = $db->query(
			"
         SELECT *
         FROM tbl_orders
         WHERE id = :order
      "
		);
		$db->bind($qorders, ":order", $bill->id);
		$order = $db->single($qorders);

		//get user info
		$qusers = $db->query(
			"
         SELECT u.*,meta.*, u.id as id
         FROM tbl_users u
         LEFT JOIN tbl_users_meta meta
         ON meta.id = u.fk_user_meta
         WHERE u.id = :user
      "
		);
		$db->bind($qusers, ":user", $order->fk_users);
		$user = $db->single($qusers);

		//get delivery info
		$qdelivery = $db->query(
			"
         SELECT *
         FROM tbl_delivery
         WHERE id = :delivery
      "
		);
		$db->bind($qdelivery, ":delivery", $order->fk_delivery);
		$delivery = $db->single($qdelivery);

		
		if ($template == 'order_bill') {
			// $html .=' <table><tr><td>Commande # '.$bill->id.'</td></tr></table>';
			$html .= '
         <table width="1035" style="border-collapse:collapse;border-spacing:0;background:#3b3b3b;">
            <tr>
               <td colspan="3" width="1035" height="20"></td>
            </tr>
            <tr>
               <td width="20"></td>
               <td width="995" style="text-align:center;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#ffffff;font-size:25px;line-height:1.4;font-weight:400;">
                     ' . $lang['thankyou_for_order'] . '
                  </span>
               </td>
            </tr>
            <tr>
               <td colspan="3" width="1035" height="20"></td>
            </tr>
         </table>
         <table width="1035" style="border-collapse:collapse;border-spacing:0;background:#ffffff;">
            <tr>
               <td colspan="3" width="1035" height="20"></td>
            </tr>
            <tr>
               <td width="20"></td>
               <td width="975" style="text-align:center;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $lang['we_process_order'] . '
                  </span>
               </td>
               <td width="20"></td>
            </tr>
            <tr>
               <td colspan="3" width="1035" height="20"></td>
            </tr>
         </table>
         <table width="1035" style="border-collapse:collapse;border-spacing:0;background:#ffffff;">
            <tr>
               <td colspan="3" width="1035" height="20"></td>
            </tr>
            <tr>
               <td width="20"></td>
               <td width="487.5" style="text-align:left;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $user->first_name . ' ' . $user->last_name . '
                  </span>
               </td>
               <td width="487.5" style="text-align:right;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $lang['order_number'] . ' : #' . $order->id . '
                  </span>
               </td>
            </tr>
            <tr>
               <td width="20"></td>
               <td width="487.5" style="text-align:left;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $user->address . '
                  </span>
               </td>
               <td width="487.5" style="text-align:right;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $lang['delivery_type'] . ' : ' . $order->delivery_type . '
                  </span>
               </td>
            </tr>
            <tr>
               <td width="20"></td>
               <td width="487.5" style="text-align:left;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $user->city . ' (' . $user->state . ') ' . $user->zipcode . '
                  </span>
               </td>
               <td width="487.5" style="text-align:right;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     Date : ' . $order->date_created . '
                  </span>
               </td>
            </tr>
            <tr>
               <td width="20"></td>
               <td width="487.5" style="text-align:right;">

               </td>
               <td width="487.5" style="text-align:right;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . ($order->delivery_type == "Livraison" ? $lang['delivery_date'] . ' : ' . $order->date_delivery : '') . '
                  </span>
               </td>
            </tr>
            <tr>
               <td width="20"></td>
               <td width="487.5" style="text-align:right;">

               </td>
               <td width="487.5" style="text-align:right;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $lang['payment_methods'] . ": " . $order->payment_method . '
                  </span>
               </td>
            </tr>
            <tr>
               <td colspan="3" width="1035" height="20"></td>
            </tr>
         </table>
      ';
			if ($order->delivery_type == "Livraison") {
				$html .= '
         <table width="1035" style="border-collapse:collapse;border-spacing:0;background:#ffffff;">
            <tr>
               <td colspan="3" width="1035" height="20"></td>
            </tr>
            <tr>
               <td width="20"></td>
               <td width="975" style="text-align:center;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $lang['we_delivery_to_this_address'] . '
                  </span>
               </td>
               <td width="20"></td>
            </tr>
            <tr>
               <td colspan="3" width="1035" height="20"></td>
            </tr>
         </table>
         <table width="1035" style="border-collapse:collapse;border-spacing:0;background:#ffffff;">
            <tr>
               <td colspan="3" width="1035" height="20"></td>
            </tr>
            <tr>
               <td width="20"></td>
               <td width="487.5" style="text-align:left;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $delivery->first_name . ' ' . $delivery->last_name . '
                  </span>
               </td>
               <td width="487.5" style="text-align:left;">

               </td>
            </tr>
            <tr>
               <td width="20"></td>
               <td width="487.5" style="text-align:left;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $delivery->address . '
                  </span>
               </td>
               <td width="487.5" style="text-align:left;">

               </td>
            </tr>
            <tr>
               <td width="20"></td>
               <td width="487.5" style="text-align:left;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $delivery->city . ' (' . $delivery->region . ') ' . $delivery->zipcode . '
                  </span>
               </td>
               <td width="487.5" style="text-align:center;">

               </td>
            </tr>
            <tr>
               <td colspan="3" width="1035" height="20"></td>
            </tr>
         </table>
         ';

			}
			$html .= '
         <table width="1035" style="border-collapse:collapse;border-spacing:0;background:#ffffff;">
            <tr>
               <td colspan="3" width="1035" height="20"></td>
            </tr>
            <tr>
               <td width="20"></td>
               <td width="975" style="text-align:center;">
                  <span style="display:block;margin:0;padding:0;font-family:arial, sans-serif;color:#000000;font-size:12px;line-height:1.4;font-weight:400;">
                     ' . $lang['summary_order'] . '
                  </span>
               </td>
               <td width="20"></td>
            </tr>
            <tr>
               <td colspan="3" width="1035" height="20"></td>
            </tr>
         </table>
         <table width="1035" border="1" style="margin:0 auto;border-collapse:collapse;border-spacing:0;background:#ffffff;">

            <tr>
               <td width="258,75" style="text-align:center;">
                  ' . $lang['products'] . '
               </td>
               <td width="258,75" style="text-align:center;">
                  ' . $lang['unit_price'] . '
               </td>
               <td width="258,75" style="text-align:center;">
                  ' . $lang['quantity'] . '
               </td>
               <td width="258,75" style="text-align:center;">
                  ' . $lang['sub_total'] . '
               </td>
            </tr>
      ';
			//gather products in order

			$qproducts = $db->query(
				"
         SELECT tbl_products_orders.*, tbl_products.title, tbl_products.title_en, tbl_products.texte FROM tbl_products_orders
         LEFT JOIN tbl_products
         ON tbl_products.id = tbl_products_orders.fk_products
         WHERE tbl_products_orders.fk_orders = :order
      "
			);
			$db->bind($qproducts, ":order", $bill->id);
			$db->execute($qproducts);
			if ($db->rowCount($qproducts) > 0) {

				while ($product = $db->fetch($qproducts)) {
					if (strtolower($product->position) == "droite") {
						$lposition = "right";
					} else {
						$lposition = "left";
					}
					$html .= '
            <tr style="text-align:center" align="center">
               <td width="240.5" style="text-align:center;">
                  ' . $product->title . (isset($product->image) && $product->image !== "0" ? " (Personnalisé à " . $product->position . ")" : "") . ' / ' . $product->title_en . (isset($product->image) && $product->image !== "0" ? " (Personalized at " . $lposition . ")" : "") . '
				 <br>'.$product->texte.'
			  </td>
               <td width="240.5" style="text-align:center;">
                  ' . number_format($product->price, 2, ".", "") . ' $
               </td>
               <td width="240.5" style="text-align:center;">
                  ' . $product->quantity . '
               </td>
               <td width="240.5" style="text-align:center;">
                  ' . number_format(($product->price * $product->quantity), 2, ".", "") . ' $
               </td>
            </tr>
            ';
				}
			}
			$html .= '
         </table>
         <table width="1035" style="border-collapse:collapse;border-spacing:0;background:#ffffff;">

            <tr>
               <td colspan="2">

               </td>
            </tr>
            <tr>
               <td colspan="2">
                  <br>
               </td>
            </tr>
            <tr>
               <td colspan="2">

               </td>
            </tr>
         </table>
         <table width="1035" border="1" style="border-collapse:collapse;border-spacing:0;background:#ffffff;">


            <tr>
               <td width="487.5" style="text-align:center;">
                  ' . $lang['sub_total'] . '
               </td>
               <td width="487.5" style="text-align:center;">
                  ' . number_format($order->sub_total, 2, ".", "") . ' $
               </td>
            </tr>
         '.($order->rebate > 0 ? '
         <tr>
            <td width="487.5" style="text-align:center;">
               Rabais/discount
            </td>
            <td width="487.5" style="text-align:center;">
               ' . number_format((-1*$order->rebate), 2, ".", "") . ' $
            </td>
         </tr>' : '').'
            <tr>
               <td width="487.5" style="text-align:center;">
                  ' . $lang['delivery_cost'] . '
               </td>
               <td width="487.5" style="text-align:center;">
                  ' . number_format($order->shipping, 2, ".", "") . ' $
               </td>
            </tr>
            <tr>
               <td width="487.5" style="text-align:center;">
                  TPS (724837737 RT0001)
               </td>
               <td width="487.5" style="text-align:center;">
                  ' . number_format($order->tps, 2, ".", "") . ' $
               </td>
            </tr>
            <tr>
               <td width="487.5" style="text-align:center;">
                 TVQ (1227581103 TQ0001)
               </td>
               <td width="487.5" style="text-align:center;">
                  ' . number_format($order->tvq, 2, ".", "") . ' $
               </td>
            </tr>
            <tr>
               <td width="487.5" style="text-align:center;">
                 Total
               </td>
               <td width="487.5" style="text-align:center;">
                  ' . number_format($order->final_price, 2, ".", "") . ' $
               </td>
            </tr>
            <tr>
               <td width="487.5" style="text-align:center;">
                 ' . $lang['amount_to_pay'] . '
               </td>
               <td width="487.5" style="text-align:center;">
                  ' . number_format($order->amount_to_pay, 2, ".", "") . ' $
               </td>
            </tr>
         </table>
         <table width="1035" style="border-collapse:collapse;border-spacing:0;background:#ffffff;">

            <tr>
               <td colspan="2">

               </td>
            </tr>
            <tr>
               <td colspan="2">
                  <br>
               </td>
            </tr>
            <tr>
               <td colspan="2">

               </td>
            </tr>
         </table>
         ';
		}
		}

		if ($template == 'payment_bill') {
			$html .= ' <table><tr><td>Paiement # ' . $bill->id . '</td></tr></table>';
		}

		if ($template == 'payment_bill_credit') {
			$html .= ' <table><tr><td>Paiement solde</td></tr></table>';
		}

		$html .= pdf_footer();

		//Il n'affiche pas les "à" à moins qu'ils soient en html entities
		$contentPDF = str_replace('à', '&agrave;', $html);
		$contentPDF = str_replace('é', '&eacute;', $html);
		// echo $contentPDF;
		try {
			ob_end_clean();
			$pdf = new \HTML2PDF('P', 'A3', 'En');
			$pdf->writeHTML($contentPDF);
			if ($download === true) {
				$pdf->output('export_command.pdf', 'D'); //D pour télécharger
			} elseif ($download == 'view') {
				$pdf->output('export_command.pdf');
			} else {
				return $pdf->output('', true);
			}
		} catch (HTML2PDF_exception $e) {
			die($e);
		}
	}

	function getHighestValue($data_points, $value = '')
	{
		$max = 0;
		foreach ($data_points as $point) {
			if ($value) {
				if ($max < (float)$point->{$value}) {
					$max = $point->{$value};
				}
			} else {
				if ($max < (float)$point->total_one) {
					$max = $point->total_one;
				}
				if ($max < (float)$point->total_two) {
					$max = $point->total_two;
				}
				if ($max < (float)$point->total_third) {
					$max = $point->total_third;
				}
			}
		}
		return $max;
	}

	/* -- Function to gather all countries -- */
	function fetch_countries()
	{
		$countries = array(
			"Canada"
		);
		return $countries;
	}

	/* -- Function to gather all states -- */
	function fetch_states($country = "Canada")
	{
		$states = array();
		if (!isset($country) || $country == "") {
			$country = "Canada";
		}
		$states["Canada"] = array(
			"Alberta", "Colombie-Britannique", "Île-du-Prince-Édouard", "Manitoba", "Nouveau-Brunswick", "Nouvelle-Écosse", "Ontario", "Québec", "Saskatchewan", "Terre-Neuve-et-Labrador", "Nunavut", "Territoires du Nord-Ouest", "Yukon"
		);
		return $states[$country];
	}

	/* -- Function to gather all regions -- */
	function fetch_regions($state = "Québec")
	{
		$regions = array();
		if (!isset($state) || $state == "") {
			$state = "Québec";
		}
		$regions["Québec"] = array(
			"Abitibi-Témiscamingue", "Bas-Saint-Laurent", "Capitale-Nationale", "Centre-du-Québec", "Chaudière-Appalaches", "Côte-Nord", "Estrie", "Gaspésie–Îles-de-la-Madeleine", "Lanaudière", "Laurentides", "Laval", "Mauricie", "Montérégie", "Montréal", "Nord-du-Québec", "Outaouais", "Saguenay–Lac-Saint-Jean"
		);
		return $regions[$state];
	}

	/* -- Function to gather all genders -- */
	function fetch_genders()
	{
		$genders = array(
			"Homme", "Femme", "Autre"
		);
		return $genders;
	}

	function rename_payments($value)
	{
		switch ($value) {
			case 'check':
				$value = 'Chèque';
				break;
			case 'cash':
				$value = 'Comptant';
				break;
			case '':
				$value = 'Non spécifié';
				break;
			case 'credit_card':
				$value = 'Carte de crédit';
				break;
			case 'interac_transfer':
				$value = 'Transfert Interac';
				break;
			case 'bank_transfer':
				$value = 'Transfert bancaire';
				break;
			case 'manual_input':
				$value = 'Saisies manuelles';
				break;
			case 'credit':
				$value = 'Solde au compte';
				break;
			case 'refund':
				$value = 'Annulation';
				break;
			case 'group_orders':
				$value = 'Groupe anciennes commandes';
				break;
		}
		return $value;
	}

	/* -- FUNCTION TO GET A PAGINATION -- */
	function pagination($query, $per_page = 3, $page = 1, $url = '&', $bind = [])
	{

		$db = new Database();
		if (isset($_GET['page'])) {
			$url = str_replace('&page=' . $_GET['page'], '', $url);
		}
		$query_page = $db->query(
			"
           SELECT COUNT(*) as `num` FROM {$query}
       "
		);
		// var_dump($bind);
		foreach ($bind as $key => $value) {
			$db->bind($query_page, ":" . $key, $value);
		}
		$row = $db->single($query_page);
		$total = $row->num;
		$adjacents = "2";
		$page = ($page == 0 ? 1 : $page);
		$start = ($page - 1) * $per_page;
		$prev = (($page == 1) ?'.__CONTACTID__.': $page) - 1;
		$next = $page + 1;
		$lastpage = ceil($total / $per_page);
		$lpm1 = $lastpage - 1;
		$pagination = "";
		if ($lastpage > 1) {
			$pagination .= "
               <span>Page $page de $lastpage</span>
               <ul class='pagination'>
           ";
			$pagination .= "
                   <li class='page-item' colspan='4'>
           ";
			$pagination .= "
                       <a class='page-link' href='{$url}page=1'><<</a>
           ";
			$pagination .= "
                       <a class='page-link' href='{$url}page=$prev'>Précédent</a>
           ";
			if ($lastpage < 7 + ($adjacents * 2)) {
				for ($counter = 1; $counter <= $lastpage; $counter++) {
					if ($counter == $page)
						$pagination .= "
                       <a href='{$url}page=$counter' class='page-link current active'>$counter</a>
                       "; else
						$pagination .= "
                       <a class='page-link' href='{$url}page=$counter'>$counter</a>
                       ";
				}
			} elseif ($lastpage > 5 + ($adjacents * 2)) {
				if ($page < 1 + ($adjacents * 2)) {
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
						if ($counter == $page)
							$pagination .= "
                       <a class='page-link' href='{$url}page=$counter' class='current'>$counter</a>
                           "; else
							$pagination .= "
                       <a class='page-link' href='{$url}page=$counter'>$counter</a>
                           ";
					}
					$pagination .= "
                       <a href='#'>...</a>
                   ";
					$pagination .= "
                       <a class='page-link' href='{$url}page=$lpm1'>$lpm1</a>
                   ";
					$pagination .= "
                       <a  class='page-link' href='{$url}page=$lastpage'>$lastpage</a>
                   ";
				} elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
					$pagination .= "
                       <a class='page-link' href='{$url}page=1'>1</a>
                   ";
					$pagination .= "
                       <a class='page-link' href='{$url}page=2'>2</a>
                   ";
					$pagination .= "
                       <a href='#'>...</a>
                   ";
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
						if ($counter == $page)
							$pagination .= "
                       <a class='page-link' href='{$url}page=$counter' class='current'>$counter</a>
                           "; else
							$pagination .= "
                       <a class='page-link' href='{$url}page=$counter'>$counter</a>";
					}
					$pagination .= "
                       <a href='#'>..</a>
                   ";
					$pagination .= "
                       <a class='page-link' href='{$url}page=$lpm1'>$lpm1</a>
                   ";
					$pagination .= "
                       <a class='page-link' href='{$url}page=$lastpage'>$lastpage</a>
                   ";
				} else {
					$pagination .= "
                       <a href='{$url}page=1'>1</a>
                   ";
					$pagination .= "
                       <a href='{$url}page=2'>2</a>
                   ";
					$pagination .= "
                       <a href='#'>..</a>
                   ";
					for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
						if ($counter == $page)
							$pagination .= "
                       <a href='{$url}page=$counter' class='page-item current active'>$counter</a>
                           "; else
							$pagination .= "
                       <a  class='page-link' href='{$url}page=$counter'>$counter</a>
                           ";
					}
				}
			}

			if ($page < $counter - 1) {
				$pagination .= "
                       <a  class='page-link' href='{$url}page=$next'>Suivant</a>
               ";
				$pagination .= "
                       <a class='page-link' href='{$url}page=$lastpage'>>></a>
               ";
			} else {
				$pagination .= "
                       <a class='page-link' href='{$url}page=$counter' class='current'>Suivant</a>
               ";
				$pagination .= "
                       <a class='page-link' href='{$url}page=$counter' class='current'>>></a>
               ";
			}
			$pagination .= "
                   </li>
               </ul>\n
           ";
		}
		echo $pagination;
	}

	/* Payments */
	function getMethodPayments($type = '')
	{
		$array_payment_method = array(
			'credit_card' => array(
				'' => 'Carte de crédit', '_en' => 'Credit card'
			), 'check' => array(
				'' => 'Chèque', '_en' => 'Check'
			), 'cash' => array(
				'' => 'Argent comptant', '_en' => 'Cash'
			), 'interac_transfer' => array(
				'' => 'Virement Interac', '_en' => 'Interact transfer'
			), 'bank_transfer' => array(
				'' => 'Virement bancaire', '_en' => 'bank transfer'
			)
		);

		if ($type == "") {
			$return = $array_payment_method;
		} else {
			$return = $array_payment_method[$type];
		}
		return $return;
	}


	function getBlocksByType($id)
	{
		global $lang, $l;
		$db = new Database();
		$query_blocks = $db->query(
			"
         SELECT * FROM tbl_pages
         WHERE parent = :id
         AND online = 1
         ORDER BY `order` ASC
      "
		);
		$i = 0;

		//increment variable for sliders
		$i_slide = 0;
		$i_block = 1;
		$db->bind($query_blocks, ":id", $id);
		if ($db->rowCount($query_blocks) > 0) {

			$html = "";
			while ($block = $db->fetch($query_blocks)) {

				$query_get = $db->query("SELECT * ,tbl_pages_inputs.value as value, i.name as name FROM tbl_pages_inputs LEFT JOIN inputs i ON i.id = tbl_pages_inputs.type WHERE fk_pages = :id");

				$db->bind($query_get, ":id", $block->id);
				$db->execute($query_get);
				while ($joinit = $db->fetch($query_get)) {
					if(isset($block->{$joinit->name}))
						$block->{$joinit->name} = $joinit->value;
				}
				switch ($block->type) {
					/* --- HOME --- */ //Slides carousel (Une slide)
					case __ASLIDEID__:
						if ($block->id == 40) { //video
							$alternate_slide = getMyVideoFunction($page->id, $l);;
						} else {
							if ($i_slide % 2 == 0) {
								//Align left
								$alternate_slide = '
                        <div class="alpha grid_6  grid_1024_12 ' . ($block->id == 51 ? ' hide_mobile ': '').' text-center">
                           <div class="wow block " data-wow-delay="0.2s" >
                          ' . ($block->id == 51 ? '<div class="table"><div class="table-cell"><img class="logo_slide lazyload wow fadeIn" data-wow-delay="0.5s"  data-wow-transitions="0.2s"  alt="logo in slide"  data-src="' . URLROOT . 'images/logo/Dragon.png~l=500" width="500px" heigh="auto" /></div></div>' : '') . '

                           </div>
                        </div>
                        <div class="grid_6 grid_1024_12">
                           <div class="block wow fadeIn"  data-wow-delay="0.2s">
                           <span class="title_slide">' . ($block->type == 51 ?  $block->{'title' . $l} :  ($block->{'primary_title' . $l} ?  $block->{'primary_title' . $l} : "") ). '</span>
                           ' .  $block->{'texte' . $l}   . '
                           </div>
                        </div>
                        <div class="omega clear">
                        </div>
                        ';
							} else {

								//Align right
								$alternate_slide = '
                        <div class="alpha table grid_6  grid_1024_12">
                           <div class="wow block table-cell  " data-wow-delay="0.2s">
                             <span class="title_slide">'.$block->{'title' . $l}.'</span>
                           ' . (isset($block->{'texte' . $l}) ? $block->{'texte' . $l} : "") . '
                           </div>
                        </div>
                        <div class="grid_6 grid_1024_12 text-center">
                            <div class="wow block fadeIn" >
                           ' . ($block->id == 51 ? '<div class="table"><div class="table-cell"><img class="logo_slide"  alt="logo in slide" src="' . URLROOT . 'images/logo/Dragon.png" width="500px" heigh="auto" /></div></div>' : '') . '
                           ' . (isset($block->{'texte2' . $l}) ? $block->{'texte2' . $l} : "") . '
                           </div>
                        </div>
                        <div class="omega clear">
                        </div>';
							}
						}
						$html .= '
						<div class="desktop">
						  <div  data-speed="6000"   class="slide ' . ($i == 0 ? 'active' : '') . '" style="' . (trim($block->image) !== "" ? 'background-size: cover;background-repeat:no-repeat;background-position: center;background-image: url(' . '\'' . (URLROOT . 'image.php?path=' . $block->image) . '~l=1920\'' . ')' : '') . '">
							 <div class="page-header" >
								<div class="filter"></div>
								<div class="wrap_eighty">
								   <div class="">
									  <div class="row">
										 ' . $alternate_slide . '
									  </div>
								   </div>
								</div>
							 </div>
						  </div>
						</div>
						<div class="mobile">
						  <div  data-speed="6000"   class="slide ' . ($i == 0 ? 'active' : '') . '" style="' . (trim($block->image) !== "" ? 'background-size: cover;background-repeat:no-repeat;background-position: center;background-image: url(' . '\'' . (URLROOT . 'image.php?path=' . $block->image) . '~l=1024\'' . ')' : '') . '">
							 <div class="page-header" >
								<div class="filter"></div>
								<div class="wrap_eighty">
								   <div class="">
									  <div class="row">
										 ' . $alternate_slide . '
									  </div>
								   </div>
								</div>
							 </div>
						  </div>
						</div>
                  ';
						$i_slide++;
						break;
					//Slides carousel (Une slide avec produits vedettes)
					case 19:

						if ($i_slide %'.__CONTACTID__.'== 0) {
							//Align left
							$alternate_slide = '
                     <div class="alpha grid_6   grid_1024_12 text-center">
                        ' . ($block->{'title' . $l} ? '<h2 class=" text-center wow fadeInLeft" data-wow-delay="1s" >' . $block->{'title' . $l} . '</h2>' : '') . '
                        <br/>
                        <div class="wow fadeInLeft" >
                        ' . $block->{'texte' . $l} . '
                        </div>
                        <div class="buttons wow fadeInLeft" data-wow-delay="1.4s">
                           <a href="' . getUrlLang(__PRODUCTSID__) . '" class="btn btn btn-primary btn-round ">
                           ' . $lang['see_shop'] . '
                           </a>
                           <br>
                        </div>
                     </div>
                     <div class="grid_6 grid_1024_12 bg-black">

                     </div>
                     <div class="omega clear">
                     </div>
                     ';
						} else {

							//Align right
							$alternate_slide = '
                     <div class="alpha grid_6 bg-black grid_1024_12">
                        <!--getProductsVedettes()-->
                         ' . ($block->{'title' . $l} ? '<h2 class=" text-center wow fadeInLeft" data-wow-delay="1s" >' . $block->{'title' . $l} . '</h2>' : '') . '
                        <br/>
                        <div class="wow fadeInLeft" >
                        ' . $block->{'texte' . $l} . '
                        </div>
                        <div class="buttons wow fadeInLeft" data-wow-delay="1.4s">
                           <a href="' . getUrlLang(__PRODUCTSID__) . '" class="btn btn btn-primary btn-round ">
                           ' . $lang['see_shop'] . '
                           </a>
                           <br>
                        </div>
                     </div>
                     <div class="grid_6 grid_1024_12 text-center">

                     </div>
                     <div class="omega clear">
                     </div>';
						}
						$html .= '
                  <div class="carousel-item ' . ($i == 0 ? 'active' : '') . '" style="' . ($i == 0 ? 'active' : '') . ' ' . (trim($block->image) !== "" ?
				   'background-size: cover;  /* -webkit-transform: translate3d(0, 0, 0) !important;*/  background-repeat:no-repeat;background-position: center;
				   background-image: url(' . '\'' . DragonHide(URLROOT . 'image.php?path=' . $block->image) . '\'' . ')' : '') . '">
                     <div class="page-header" >
                        <div class="filter"></div>
                        <div class="wrap_eighty">
                           <div class="">
                              <div class="row">
                                 ' . $alternate_slide . '
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  ';
						$i_slide++;
						break;

					case __BTEXTIMGID__:
						$i_block++;
						$query_child_block = $db->query(
							"
                     SELECT * FROM tbl_pages
                     WHERE parent = :id"
						);
						$db->bind($query_child_block, ":id", $block->id);
						$counted_childs = $db->rowCount($query_child_block);

						if ($i_block % 2 == 0) {
							$alternate_slide = '
                     <div class="alpha table-cell-custom grid_6 grid_1024_12 block_1 fadeInLeft wow" data-wow-duration="1.5s" data-wow-delay="0.1s">
                         <div class=" ">
						   <div class="square">
								<div > 
									<h4 class="wrap-h2">' . $block->{'title' . $l} . '</h4>
									<div class="orange-bar"></div>
									<span class="suptitle">'.$block->{'suptitle' . $l}.'</span> <br>
									
								</div>
							</div>
                             <div style="color:#000;"> 
                                 ' . $block->{'texte' . $l} . '
                                 <br>
                                 ' . ($counted_childs > 0 ? '
                                 <div style="text-align:center">
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeA  btn btn btn-primary btn-round ">
                                 <span class="">' . $lang['see_more'] . '</span>
                                 </a>
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeB  btn btn btn-primary btn-round ">
                                 <span >' . $lang['see_less'] . '</span>
                                 </a></div>' : '') . '
                             </div>
                         </div>
                     </div>
                     <div class="omega about_section table-cell-custom grid_6 block_2 grid_1024_12 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="0.4s">
                         <div class="div_wrap_about" style="background: url(' . URLROOT . 'uploads/'.SITENAME.'/' . $block->image . '~l=1024) center; background-size: contain;background-attachment: scroll;background-position: center;
    background-repeat: no-repeat;">
                             <img class="about_img" src="' . URLROOT . 'uploads/'.SITENAME.'/' . $block->image . '~l=1024" alt="' . $block->alt . '" >
                         </div>
                     </div>
                     ';
						} else {

							$alternate_slide = '
                     <div class="alpha about_section table-cell-custom grid_6 block_1 grid_1024_12 fadeInLeft wow" data-wow-duration="1.5s" data-wow-delay="0.4s">
                         <div class="div_wrap_about" style="background: url(' . URLROOT . 'uploads/'.SITENAME.'/' . $block->image . '~l=1024) center; background-size: contain;
    background-repeat: no-repeat;background-attachment: scroll;background-position: center;">
                             <img class="about_img" src="' . URLROOT . 'uploads/'.SITENAME.'/' . $block->image . '~l=1024" alt="' . $block->alt . '" >
                         </div>
                     </div>
                     <div class="omega table-cell-custom grid_6 grid_1024_12 block_2 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="0.1s">
                         <div class=" ">
						 
						   <div class="square">
								<div > 
									<h4 class="wrap-h2">' . $block->{'title' . $l} . '</h4>
									<div class="orange-bar"></div>
									<span class="suptitle">'.$block->{'suptitle' . $l}.'</span> <br>
									
								</div>
							</div>
                             <div style="color:#000;"> 
                                 ' . $block->{'texte' . $l} . '
                                 <br>
                                 ' . ($counted_childs > 0 ? '
                                 <div style="text-align:center">
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeA  btn btn btn-primary btn-round ">
                                 <span class="">' . $lang['see_more'] . '</span>
                                 </a>
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeB  btn btn btn-primary btn-round ">
                                 <span >' . $lang['see_less'] . '</span>
                                 </a></div>' : '') . '
                             </div>
                         </div>
                     </div>
                     ';
						}

						$html .= '
                  <div class="table-custom section  "
                     style="/*background: url(' . URLROOT . 'uploads/'.SITENAME.'/' . $block->image . '~l=1024);*/ background-size: cover;
                     background-position: center;
                     background-attachment:fixed;
                     background-repeat: no-repeat;">
                     <div id="acordeon' . $i_block . '">
                        <div class="panel-group" id="accordion' . $i_block . '">
                           <div class="panel panel-default ' . ($i_block % 2 == 1 ? 'lets_flex' : '') . '">
                           ' . $alternate_slide . '
								<div class="clear"></div>
                  ';
						if ($counted_childs > 0) {
							$html .= '
							   <div id="collapse' . $i_block . '" class=" delta grid_12 panel-collapse collapse in" aria-expanded="true" style="">
								  <div class="panel-body" style="color:black">

                     ';
							$db->execute($query_child_block);
							$i_child_block = 0;
							$html_child = "";
							while ($child_block = $db->fetch($query_child_block)) {
								if ($i_child_block % 2 == 0) {
									$alternate_slide_child = '
									   <div class="alpha table-cell-custom grid_6 grid_1024_12 block_1 fadeInLeft wow" data-wow-duration="1.1s" data-wow-delay="0.1s">
										   <div class=" "> 
											   <div class="square">
													<div > 
														<h4 class="wrap-h2">' . $child_block->{'title' . $l} . '</h4>
														<div class="orange-bar"></div>
														<span class="suptitle">'.$child_block->{'suptitle' . $l}.'</span> <br>
														
													</div>
												</div>
											   <div style="color:#000;"> 
												   ' . $child_block->{'texte' . $l} . ' 
											   </div>
										   </div>
									   </div>
									   <div class="omega about_section table-cell-custom grid_6 block_2 grid_1024_12 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="0.1s">
										   <div class="div_wrap_about" style="background: url(' . URLROOT . 'uploads/'.SITENAME.'/' . $child_block->image . '~l=1024) center; background-size: contain;background-attachment: scroll;background-position: center;
    background-repeat: no-repeat;">
											   <img class="about_img" src="' . URLROOT . 'uploads/'.SITENAME.'/' . $child_block->image . '~l=1024" alt="' . $child_block->alt . '" >
										   </div>
									   </div>
									   ';
											} else {

												$alternate_slide_child = '
									   <div class="alpha about_section table-cell-custom grid_6 block_1 grid_1024_12 fadeInLeft wow" data-wow-duration="1.1s" data-wow-delay="0.1s">
										   <div class="div_wrap_about" style="background: url(' . URLROOT . 'uploads/'.SITENAME.'/' . $child_block->image . '~l=1024) center;
    background-repeat: no-repeat; background-size: contain;background-attachment: scroll;background-position: center;">
											   <img class="about_img" src="' . URLROOT . 'uploads/'.SITENAME.'/' . $child_block->image . '~l=1024" alt="' . $child_block->alt . '" >
										   </div>
									   </div>
									   <div class="omega table-cell-custom grid_6 grid_1024_12 block_2 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="0.1s">
										   <div class=" ">
											   <div class="square">
													<div > 
														<h4 class="wrap-h2">' . $child_block->{'title' . $l} . '</h4>
														<div class="orange-bar"></div>
														<span class="suptitle">'.$child_block->{'suptitle' . $l}.'</span> <br>
														
													</div>
												</div> 
											   ' . $child_block->{'texte' . $l} . '
											   <br> 
										   </div>
									   </div>
                           ';
								}
								$html_child .= '
                        <div class="table-custom section  "
                           style="width: 100%;/*background: url(' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024);*/ background-size: cover;
                           background-position: center;
                           background-attachment:fixed;
                           background-repeat: no-repeat;">
                           <div id="acordeon">
                              <div class="panel-group" id="accordion' . $i_child_block . '">
                                 <div class="panel panel-default ' . ($i_child_block % 2 == 1 ? 'lets_flex' : '') . '">
                                 ' . $alternate_slide_child . '
									<div class="clear"></div>
								</div>
                              </div>
                           </div>
                        </div>
                        ';
								$i_child_block++;
							}
							$html .= $html_child;
							$html .= '
                              </div>
                           </div>
                     ';
						}
						$html .= '
                        </div>
                     </div>
                  </div>
              </div>
                  ';
						$i_block++;
						break;
					case __BFULLTEXTID__:
						$i_block++;
						$html .= '
							<div class="delta grid_12">
								<div class="wrapper_text">
								'.$block->{'texte'.$l}.'
								</div>
							</div>
						';
				}
				$i++;
			}
			return $html;
		} else {
			return false;
		}
	}

	function getBlocksByTypeBlogs($id)
	{
		global $lang, $l;
		$db = new Database();
		$query_blocks = $db->query(
			"
         SELECT * FROM tbl_blogs
         WHERE parent = :id
		 AND online = 1
         ORDER BY `order` ASC
      "
		);
		$i = 0;

		//increment variable for sliders
		$i_slide = 0;
		$i_block = 1;
		$db->bind($query_blocks, ":id", $id);
		if ($db->rowCount($query_blocks) > 0) {

			$html = "";
			while ($block = $db->fetch($query_blocks)) {

				$query_get = $db->query("SELECT * ,tbl_blogs_inputs.value as value, i.name as name FROM tbl_blogs_inputs LEFT JOIN inputs i ON i.id = tbl_blogs_inputs.type WHERE fk_blogs = :id");

				$db->bind($query_get, ":id", $block->id);
				$db->execute($query_get);
				while ($joinit = $db->fetch($query_get)) {
					$block->{$joinit->name} = $joinit->value;
				}
				switch ($block->type) {
					/* --- HOME --- */ //Slides carousel (Une slide)
					case __ASLIDEID__:
						if ($block->id == 40) { //video
							$alternate_slide = getMyVideoFunction($page->id, $l);;
						} else {
							if ($i_slide % 2 == 0) {
								//Align left
								$alternate_slide = '
                        <div class="alpha grid_6  grid_1024_12 ' . ($block->id == 51 ? ' hide_mobile ': '').' text-center">
                           <div class="wow block " data-wow-delay="0.2s" >
                          ' . ($block->id == 51 ? '<div class="table"><div class="table-cell"><img class="logo_slide lazyload wow fadeIn" data-wow-delay="0.5s"  data-wow-transitions="0.2s"  alt="logo in slide"  data-src="' . URLROOT . 'images/logo/Dragon.png~l=500" width="500px" heigh="auto" /></div></div>' : '') . '

                           </div>
                        </div>
                        <div class="grid_6 grid_1024_12">
                           <div class="block wow fadeIn"  data-wow-delay="0.2s">
                           <span class="title_slide">' . ($block->type == 51 ?  $block->{'title' . $l} :  ($block->{'primary_title' . $l} ?  $block->{'primary_title' . $l} : "") ). '</span>
                           ' .  $block->{'texte' . $l}   . '
                           </div>
                        </div>
                        <div class="omega clear">
                        </div>
                        ';
							} else {

								//Align right
								$alternate_slide = '
                        <div class="alpha table grid_6  grid_1024_12">
                           <div class="wow block table-cell  " data-wow-delay="0.2s">
                             <span class="title_slide">'.$block->{'title' . $l}.'</span>
                           ' . (isset($block->{'texte' . $l}) ? $block->{'texte' . $l} : "") . '
                           </div>
                        </div>
                        <div class="grid_6 grid_1024_12 text-center">
                            <div class="wow block fadeIn" >
                           ' . ($block->id == 51 ? '<div class="table"><div class="table-cell"><img class="logo_slide"  alt="logo in slide" src="' . URLROOT . 'images/logo/Dragon.png" width="500px" heigh="auto" /></div></div>' : '') . '
                           ' . (isset($block->{'texte2' . $l}) ? $block->{'texte2' . $l} : "") . '
                           </div>
                        </div>
                        <div class="omega clear">
                        </div>';
							}
						}
						$html .= '
						<div class="desktop">
						  <div  data-speed="6000"   class="slide ' . ($i == 0 ? 'active' : '') . '" style="' . (trim($block->image) !== "" ? 'background-size: cover;background-repeat:no-repeat;background-position: center;background-image: url(' . '\'' . (URLROOT . 'image.php?path=' . $block->image) . '~l=1920\'' . ')' : '') . '">
							 <div class="page-header" >
								<div class="filter"></div>
								<div class="wrap_eighty">
								   <div class="text-left">
									  <div class="row">
										 ' . $alternate_slide . '
									  </div>
								   </div>
								</div>
							 </div>
						  </div>
						</div>
						<div class="mobile">
						  <div  data-speed="6000"   class="slide ' . ($i == 0 ? 'active' : '') . '" style="' . (trim($block->image) !== "" ? 'background-size: cover;background-repeat:no-repeat;background-position: center;background-image: url(' . '\'' . (URLROOT . 'image.php?path=' . $block->image) . '~l=1024\'' . ')' : '') . '">
							 <div class="page-header" >
								<div class="filter"></div>
								<div class="wrap_eighty">
								   <div class="">
									  <div class="row">
										 ' . $alternate_slide . '
									  </div>
								   </div>
								</div>
							 </div>
						  </div>
						</div>
                  ';
						$i_slide++;
						break;
					//Slides carousel (Une slide avec produits vedettes)
					case 19:

						if ($i_slide %'.__CONTACTID__.'== 0) {
							//Align left
							$alternate_slide = '
                     <div class="alpha grid_6   grid_1024_12 text-center">
                        ' . ($block->{'title' . $l} ? '<h2 class=" text-center wow fadeInLeft" data-wow-delay="1s" >' . $block->{'title' . $l} . '</h2>' : '') . '
                        <br/>
                        <div class="wow fadeInLeft" >
                        ' . $block->{'texte' . $l} . '
                        </div>
                        <div class="buttons wow fadeInLeft" data-wow-delay="1.4s">
                           <a href="' . getUrlLang(__PRODUCTSID__) . '" class="btn btn btn-primary btn-round ">
                           ' . $lang['see_shop'] . '
                           </a>
                           <br>
                        </div>
                     </div>
                     <div class="grid_6 grid_1024_12 bg-black">

                     </div>
                     <div class="omega clear">
                     </div>
                     ';
						} else {

							//Align right
							$alternate_slide = '
                     <div class="alpha grid_6 bg-black grid_1024_12">
                        <!--getProductsVedettes()-->
                         ' . ($block->{'title' . $l} ? '<h2 class=" text-center wow fadeInLeft" data-wow-delay="1s" >' . $block->{'title' . $l} . '</h2>' : '') . '
                        <br/>
                        <div class="wow fadeInLeft" >
                        ' . $block->{'texte' . $l} . '
                        </div>
                        <div class="buttons wow fadeInLeft" data-wow-delay="1.4s">
                           <a href="' . getUrlLang(__PRODUCTSID__) . '" class="btn btn btn-primary btn-round ">
                           ' . $lang['see_shop'] . '
                           </a>
                           <br>
                        </div>
                     </div>
                     <div class="grid_6 grid_1024_12 text-center">

                     </div>
                     <div class="omega clear">
                     </div>';
						}
						$html .= '
                  <div class="carousel-item ' . ($i == 0 ? 'active' : '') . '" style="' . ($i == 0 ? 'active' : '') . ' ' . (trim($block->image) !== "" ?
				   'background-size: cover;  /* -webkit-transform: translate3d(0, 0, 0) !important;*/  background-repeat:no-repeat;background-position: center;
				   background-image: url(' . '\'' . DragonHide(URLROOT . 'image.php?path=' . $block->image) . '\'' . ')' : '') . '">
                     <div class="page-header" >
                        <div class="filter"></div>
                        <div class="wrap_eighty">
                           <div class="">
                              <div class="row">
                                 ' . $alternate_slide . '
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  ';
						$i_slide++;
						break;
					case __BFULLTEXTID__:
						$query_child_block = $db->query(
									"
							 SELECT * FROM tbl_pages
							 WHERE parent = :id"
								);
								$db->bind($query_child_block, ":id", $block->id);
								$counted_childs = $db->rowCount($query_child_block);
								$alternate_slide = '
								 <div class="delta table-cell-custom grid_12 grid_1024_12 block_1 fadeInLeft wow" style="max-width:100%!important;text-align:left !important;" data-wow-duration="1.5s" style="max-width:100%!important;text-align:left !important;" data-wow-delay="0.1s">
									 <div class="text-left">
										 <div style="color:#000;">
											 <h2>' . $block->{'title' . $l} . '</h2><br>
											 ' . $block->{'texte' . $l} . '
											 <br>
											 ' . ($counted_childs > 0 ? '
											 <div style="text-align:center">
											 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeA  btn btn btn-primary btn-round ">
											 <span class="">' . $lang['see_more'] . '</span>
											 </a>
											 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeB  btn btn btn-primary btn-round ">
											 <span >' . $lang['see_less'] . '</span>
											 </a></div>' : '') . '
										 </div>
									 </div>
								 </div>
								';

								$html .= '
								  <div class="table-custom section  "
									 style="width: 100%;/*background: url(' . URLROOT . 'image.php?path=' . $block->image . '~l=1024);*/ background-size: cover;
									 background-position: center;
									 background-attachment:fixed;
									 background-repeat: no-repeat;">
									 <div id="acordeon' . $i_block . '">
										<div class="panel-group" id="accordion' . $i_block . '">
										   <div class="panel panel-default ' . ($i_block % 2 == 1 ? 'lets_flex' : '') . '">
										   ' . $alternate_slide . '
												<div class="clear"></div>
								';
								if ($counted_childs > 0) {
									$html .= '
									   <div id="collapse' . $i_block . '" class=" delta grid_12 panel-collapse collapse in" aria-expanded="true" style="">
										  <div class="panel-body" style="color:black">

									';
									$db->execute($query_child_block);
									$i_child_block = 0;
									$html_child = "";
									while ($child_block = $db->fetch($query_child_block)) {
										$alternate_slide_child = '
										   <div class="delta table-cell-custom grid_12 grid_1024_12 block_1 fadeInLeft wow" data-wow-duration="1.1s" data-wow-delay="0.1s">
											   <div class=" ">
												   <div style="color:#000;">
													   <h2>' . $child_block->{'title' . $l} . '</h2><br>
													   ' . $child_block->{'texte' . $l} . '
													   <br>
												   </div>
											   </div>
										   </div>
										';
										$html_child .= '
											<div class="table-custom section  "
											   style="width: 100%;/*background: url(' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024);*/ background-size: cover;
											   background-position: center;
											   background-attachment:fixed;
											   background-repeat: no-repeat;">
											   <div id="acordeon">
												  <div class="panel-group" id="accordion' . $i_child_block . '">
													 <div class="panel panel-default ' . ($i_child_block % 2 == 1 ? 'lets_flex' : '') . '">
													 ' . $alternate_slide_child . '
														<div class="clear"></div>
													</div>
												  </div>
											   </div>
											</div>
										';
										$i_child_block++;
									}
									$html .= $html_child;
									$html .= '
									  </div>
								   </div>
									';
								}
								$html .= '
									</div>
								</div>
							</div>
						</div>
						  ';
						$i_block++;
					break;
					case __BFULLIMGID__:
						$query_child_block = $db->query(
									"
							 SELECT * FROM tbl_pages
							 WHERE parent = :id"
								);
								$db->bind($query_child_block, ":id", $block->id);
								$counted_childs = $db->rowCount($query_child_block);


								$html .= '
								  <div class="table-custom section">
										<img class="lazyload" src="'.URLROOT.'image.php?path='.$block->image.'~l=1920" alt="'.$block->{'title'.$l}.'">
								  </div>
						  ';
						$i_block++;
					break;
					case __BTEXTIMGID__:

						$query_child_block = $db->query(
							"
                     SELECT * FROM tbl_pages
                     WHERE parent = :id"
						);
						$db->bind($query_child_block, ":id", $block->id);
						$counted_childs = $db->rowCount($query_child_block);

						if ($i_block % 2 == 0) {
							$alternate_slide = '
                     <div class="alpha table-cell-custom grid_6 grid_1024_12 block_1 fadeInLeft wow" data-wow-duration="1.5s" data-wow-delay="0.1s">
                         <div class="text-left">
                             <div style="color:#000;">
                                 '.(strlen($block->{'title'.$l}) > 1 ? '
										<h2>' . $block->{'title' . $l} . '</h2><br>
								': '').'
                                 ' . $block->{'texte' . $l} . '
                                 <br>
                                 ' . ($counted_childs > 0 ? '
                                 <div style="text-align:center">
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeA  btn btn btn-primary btn-round ">
                                 <span class="">' . $lang['see_more'] . '</span>
                                 </a>
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeB  btn btn btn-primary btn-round ">
                                 <span >' . $lang['see_less'] . '</span>
                                 </a></div>' : '') . '
                             </div>
                         </div>
                     </div>
                     <div class="omega about_section table-cell-custom grid_6 block_2 grid_1024_12 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="0.4s">
                         <div class="div_wrap_about" style="background: url(' . URLROOT . 'image.php?path=' . $block->image . '~l=1024) center; background-size: cover;background-attachment: scroll;background-position: center;">
                             <img class="about_img" src="' . URLROOT . 'image.php?path=' . $block->image . '~l=1024" alt="' . $block->alt . '" >
                         </div>
                     </div>
                     ';
						} else {

							$alternate_slide = '
                     <div class="alpha about_section table-cell-custom grid_6 block_1 grid_1024_12 fadeInLeft wow" data-wow-duration="1.5s" data-wow-delay="0.4s">
                         <div class="div_wrap_about" style="background: url(' . URLROOT . 'image.php?path=' . $block->image . '~l=1024) center; background-size: cover;background-attachment: scroll;background-position: center;">
                             <img class="about_img" src="' . URLROOT . 'image.php?path=' . $block->image . '~l=1024" alt="' . $block->alt . '" >
                         </div>
                     </div>
                     <div class="omega table-cell-custom grid_6 grid_1024_12 block_2 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="0.1s">
                         <div class="text-left">
                             <div style="color:#000;">
							 '.(strlen($block->{'title'.$l}) > 1 ? '
                                 <h2>' . $block->{'title' . $l} . '</h2><br>' : '').'
                                 ' . $block->{'texte' . $l} . '
                                 <br>
                                 ' . ($counted_childs > 0 ? '
                                 <div style="text-align:center">
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeA  btn btn btn-primary btn-round ">
                                 <span class="">' . $lang['see_more'] . '</span>
                                 </a>
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeB  btn btn btn-primary btn-round ">
                                 <span >' . $lang['see_less'] . '</span>
                                 </a></div>' : '') . '
                             </div>
                         </div>
                     </div>
                     ';
						}

						$html .= '
                  <div class="table-custom section  "
                     style="width: 100%;/*background: url(' . URLROOT . 'image.php?path=' . $block->image . '~l=1024);*/ background-size: cover;
                     background-position: center;
                     background-attachment:fixed;
                     background-repeat: no-repeat;">
                     <div id="acordeon' . $i_block . '">
                        <div class="panel-group" id="accordion' . $i_block . '">
                           <div class="panel panel-default ' . ($i_block % 2 == 1 ? 'lets_flex' : '') . '">
                           ' . $alternate_slide . '
								<div class="clear"></div>
                  ';
						if ($counted_childs > 0) {
							$html .= '
							   <div id="collapse' . $i_block . '" class=" delta grid_12 panel-collapse collapse in" aria-expanded="true" style="">
								  <div class="panel-body" style="color:black">

                     ';
							$db->execute($query_child_block);
							$i_child_block = 0;
							$html_child = "";
							while ($child_block = $db->fetch($query_child_block)) {
								if ($i_child_block % 2 == 0) {
									$alternate_slide_child = '
									   <div class="alpha table-cell-custom grid_6 grid_1024_12 block_1 fadeInLeft wow" data-wow-duration="1.1s" data-wow-delay="0.1s">
										   <div class=" ">
											   <div style="color:#000;">
												   <h2>' . $child_block->{'title' . $l} . '</h2><br>
												   ' . $child_block->{'texte' . $l} . '
												   <br>
											   </div>
										   </div>
									   </div>
									   <div class="omega about_section table-cell-custom grid_6 block_2 grid_1024_12 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="0.1s">
										   <div class="div_wrap_about" style="background: url(' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024) center; background-size: cover;background-attachment: scroll;background-position: center;">
											   <img class="about_img" src="' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024" alt="' . $child_block->alt . '" >
										   </div>
									   </div>
									   ';
											} else {

												$alternate_slide_child = '
									   <div class="alpha about_section table-cell-custom grid_6 block_1 grid_1024_12 fadeInLeft wow" data-wow-duration="1.1s" data-wow-delay="0.1s">
										   <div class="div_wrap_about" style="background: url(' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024) center; background-size: cover;background-attachment: scroll;background-position: center;">
											   <img class="about_img" src="' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024" alt="' . $child_block->alt . '" >
										   </div>
									   </div>
									   <div class="omega table-cell-custom grid_6 grid_1024_12 block_2 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="0.1s">
										   <div class=" ">
											   <div style="color:#000;">
												   <h2>' . $child_block->{'title' . $l} . '</h2><br>
												   ' . $child_block->{'texte' . $l} . '
												   <br>
											   </div>
										   </div>
									   </div>
                           ';
								}
								$html_child .= '
                        <div class="table-custom section  "
                           style="width: 100%;/*background: url(' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024);*/ background-size: cover;
                           background-position: center;
                           background-attachment:fixed;
                           background-repeat: no-repeat;">
                           <div id="acordeon">
                              <div class="panel-group" id="accordion' . $i_child_block . '">
                                 <div class="panel panel-default ' . ($i_child_block % 2 == 1 ? 'lets_flex' : '') . '">
                                 ' . $alternate_slide_child . '
									<div class="clear"></div>
								</div>
                              </div>
                           </div>
                        </div>
                        ';
								$i_child_block++;
							}
							$html .= $html_child;
							$html .= '
                              </div>
                           </div>
                     ';
						}
						$html .= '
                        </div>
                     </div>
                  </div>
              </div>
                  ';
						$i_block++;
						break;
				case __BDOUBLETEXTID__:

						$query_child_block = $db->query(
							"
                     SELECT * FROM tbl_pages
                     WHERE parent = :id"
						);
						$db->bind($query_child_block, ":id", $block->id);
						$counted_childs = $db->rowCount($query_child_block);

							$alternate_slide = '
							'.(strlen($block->{'title'.$l}) > 1 ? '
					 <div class="delta table-cell-custom grid_6 grid_1024_12 fadeInLeft wow" data-wow-duration="1.5s" data-wow-delay="0.1s">
                         <div class="text-left">
							<div style="color:#000;">
								<h2>'.$block->{'title'.$l}.'</h2>
							</div>
						</div>
					 </div>
					 <div class="clear"></div>' : '').'
                     <div class="alpha table-cell-custom grid_6 grid_1024_12 block_1 fadeInLeft wow" data-wow-duration="1.5s" data-wow-delay="0.1s">
                         <div class="text-left">
                             <div style="color:#000;">
                                 ' . $block->{'texte' . $l} . '
                                 <br>

                             </div>
                         </div>
                     </div>
                     <div class="omega about_section table-cell-custom grid_6 block_2 grid_1024_12 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="0.4s">
                          <div class="text-left">
                             <div style="color:#000;">
                                 ' . $block->{'textetwo' . $l} . '
                                 <br>

                             </div>
                         </div>
                     </div>
                     ';


						$html .= '
                  <div class="table-custom section  "
                     style="width: 100%;/*background: url(' . URLROOT . 'image.php?path=' . $block->image . '~l=1024);*/ background-size: cover;
                     background-position: center;
                     background-attachment:fixed;
                     background-repeat: no-repeat;">
                     <div id="acordeon' . $i_block . '">
                        <div class="panel-group" id="accordion' . $i_block . '">
                           <div class="panel panel-default ' . ($i_block % 2 == 1 ? 'lets_flex' : '') . '">
                           ' . $alternate_slide . '
								<div class="clear"></div>
                  ';

						$html .= '
                        </div>
                     </div>
                  </div>
              </div>
                  ';
						$i_block++;
						break;
				}
				$i++;
			}
			return $html;
		} else {
			return false;
		}
	}

	function getBlocksByTypeProducts($id)
	{
		global $lang, $l;
		$db = new Database();
		$query_blocks = $db->query(
			"
         SELECT * FROM tbl_products
         WHERE parent = :id
         ORDER BY `order` ASC
      "
		);
		$i = 0;

		//increment variable for sliders
		$i_slide = 0;
		$i_block = 1;
		$db->bind($query_blocks, ":id", $id);
		if ($db->rowCount($query_blocks) > 0) {

			$html = "";
			while ($block = $db->fetch($query_blocks)) {

				$query_get = $db->query("SELECT * ,tbl_products_inputs.value as value, i.name as name FROM tbl_products_inputs LEFT JOIN inputs i ON i.id = tbl_products_inputs.type WHERE fk_products = :id");

				$db->bind($query_get, ":id", $block->id);
				$db->execute($query_get);
				while ($joinit = $db->fetch($query_get)) {
					$block->{$joinit->name} = $joinit->value;
				}
				switch ($block->type) {
					/* --- HOME --- */ //Slides carousel (Une slide)
					case 10:
						if ($i_slide %'.__CONTACTID__.'== 0) {
							//Align left
							$alternate_slide = '
                     <div class="alpha grid_6  grid_1024_12  text-center">
                        ' . ($block->{'title' . $l} ? '<h2 class=" text-center wow fadeInLeft" data-wow-delay="1s" >' . $block->{'title' . $l} . '</h2>' : '') . '
                        <br/>
                        <div class="wow fadeInLeft" >
                        ' . $block->{'texte' . $l} . '
                        </div>
                     </div>
                     <div class="grid_6 grid_1024_12">
                     </div>
                     <div class="omega clear">
                     </div>
                     ';
						} else {

							//Align right
							$alternate_slide = '
                     <div class="alpha grid_6  grid_1024_12">
                     </div>
                     <div class="grid_6 grid_1024_12 text-center">
                        ' . ($block->{'title' . $l} ? '<h2 class=" text-center wow fadeInLeft" data-wow-delay="1s" >' . $block->{'title' . $l} . '</h2>' : '') . '
                        <br/>
                        <div class="wow fadeInLeft" >
                        ' . $block->{'texte' . $l} . '
                        </div>
                     </div>
                     <div class="omega clear">
                     </div>';
						}
						$html .= '
                  <div class="carousel-item ' . ($i == 0 ? 'active' : '') . '" style="' . (trim($block->image) !== "" ?
				   'background-size: cover;background-attachment:fixed;background-repeat:no-repeat;background-position: center;
				   background-image: url(' . '\'' . DragonHide(URLROOT . 'image.php?path=' . $block->image) . '~l=1024\'' . ')' : '') . '">
                     <div class="page-header" >
                        <div class="filter"></div>
                        <div class="wrap_eighty">
                           <div class="">
                              <div class="row">
                                 ' . $alternate_slide . '
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  ';
						$i_slide++;
						break;
					//Slides carousel (Une slide avec produits vedettes)
					case 19:

						if ($i_slide %'.__CONTACTID__.'== 0) {
							//Align left
							$alternate_slide = '
                     <div class="alpha grid_6   grid_1024_12 text-center">
                        ' . ($block->{'title' . $l} ? '<h2 class=" text-center wow fadeInLeft" data-wow-delay="1s" >' . $block->{'title' . $l} . '</h2>' : '') . '
                        <br/>
                        <div class="wow fadeInLeft" >
                        ' . $block->{'texte' . $l} . '
                        </div>
                        <div class="buttons wow fadeInLeft" data-wow-delay="1.4s">
                           <a href="' . getUrlLang(__PRODUCTSID__) . '" class="btn btn btn-primary btn-round ">
                           ' . $lang['see_shop'] . '
                           </a>
                           <br>
                        </div>
                     </div>
                     <div class="grid_6 grid_1024_12 bg-black">
                        <!--getProductsVedettes()-->
                     </div>
                     <div class="omega clear">
                     </div>
                     ';
						} else {

							//Align right
							$alternate_slide = '
                     <div class="alpha grid_6 bg-black grid_1024_12">
                        <!--getProductsVedettes()-->
                     </div>
                     <div class="grid_6 grid_1024_12 text-center">
                        ' . ($block->{'title' . $l} ? '<h2 class=" text-center wow fadeInLeft" data-wow-delay="1s" >' . $block->{'title' . $l} . '</h2>' : '') . '
                        <br/>
                        <div class="wow fadeInLeft" >
                        ' . $block->{'texte' . $l} . '
                        </div>
                        <div class="buttons wow fadeInLeft" data-wow-delay="1.4s">
                           <a href="' . getUrlLang(__PRODUCTSID__) . '" class="btn btn btn-primary btn-round ">
                           ' . $lang['see_shop'] . '
                           </a>
                           <br>
                        </div>
                     </div>
                     <div class="omega clear">
                     </div>';
						}
						$html .= '
                  <div class="carousel-item ' . ($i == 0 ? 'active' : '') . '" style="' . ($i == 0 ? 'active' : '') . ' ' . (trim($block->image) !== "" ?
				   'background-size: cover;  /* -webkit-transform: translate3d(0, 0, 0) !important;*/ background-attachment:fixed;background-repeat:no-repeat;
				   background-position: center;background-image: url(' . '\'' . DragonHide(URLROOT . 'image.php?path=' . $block->image) . '~l=1024\'' . ')' : '') . '">
                     <div class="page-header" >
                        <div class="filter"></div>
                        <div class="wrap_eighty">
                           <div class="">
                              <div class="row">
                                 ' . $alternate_slide . '
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  ';
						$i_slide++;
						break;

					case 31:

						$query_child_block = $db->query(
							"
                     SELECT * FROM tbl_products
                     WHERE parent = :id"
						);
						$db->bind($query_child_block, ":id", $block->id);
						$counted_childs = $db->rowCount($query_child_block);

						if ($i_block %'.__CONTACTID__.'== 0) {
							$alternate_slide = '
                     <div class="alpha table-cell-custom grid_6 grid_1024_12 block_1 fadeInLeft wow" data-wow-duration="1.5s" data-wow-delay="1s">
                         <div class=" ">
                             <div style="color:#000;">
                                 <h2>' . $block->{'title' . $l} . '</h2><br>
                                 ' . $block->{'texte' . $l} . '
                                 <br>
                                 ' . ($counted_childs > 0 ? '
                                 <div style="text-align:center">
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeA  btn btn btn-primary btn-round ">
                                 <span class="">' . $lang['see_more'] . '</span>
                                 </a>
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeB  btn btn btn-primary btn-round ">
                                 <span >' . $lang['see_less'] . '</span>
                                 </a></div>' : '') . '
                             </div>
                         </div>
                     </div>
                     <div class="omega about_section table-cell-custom grid_6 block_2 grid_1024_12 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="1.4s">
                         <div class="div_wrap_about" style="background: url(' . URLROOT . 'image.php?path=' . $block->image . '~l=1024) center; background-size: cover;background-attachment: scroll;background-position: center;">
                             <img class="about_img" src="' . URLROOT . 'image.php?path=' . $block->image . '~l=1024" alt="' . $block->alt . '" >
                         </div>
                     </div>
                     ';
						} else {

							$alternate_slide = '
                     <div class="alpha about_section table-cell-custom grid_6 block_1 grid_1024_12 fadeInLeft wow" data-wow-duration="1.5s" data-wow-delay="1.4s">
                         <div class="div_wrap_about" style="background: url(' . URLROOT . 'image.php?path=' . $block->image . '~l=1024) center; background-size: cover;background-attachment: scroll;background-position: center;">
                             <img class="about_img" src="' . URLROOT . 'image.php?path=' . $block->image . '~l=1024" alt="' . $block->alt . '" >
                         </div>
                     </div>
                     <div class="omega table-cell-custom grid_6 grid_1024_12 block_2 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="1s">
                         <div class=" ">
                             <div style="color:#000;">
                                 <h2>' . $block->{'title' . $l} . '</h2><br>
                                 ' . $block->{'texte' . $l} . '
                                 <br>
                                 ' . ($counted_childs > 0 ? '
                                 <div style="text-align:center">
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeA  btn btn btn-primary btn-round ">
                                 <span class="">' . $lang['see_more'] . '</span>
                                 </a>
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeB  btn btn btn-primary btn-round ">
                                 <span >' . $lang['see_less'] . '</span>
                                 </a></div>' : '') . '
                             </div>
                         </div>
                     </div>
                     ';
						}

						$html .= '
                  <div class="table-custom section  "
                     style="width: 100%;/*background: url(' . URLROOT . 'image.php?path=' . $block->image . '~l=1024);*/ background-size: cover;
                     background-position: center;
                     background-attachment:fixed;
                     background-repeat: no-repeat;">
                     <div id="acordeon' . $i_block . '">
                        <div class="panel-group" id="accordion' . $i_block . '">
                           <div class="panel panel-default ' . ($i_block %'.__CONTACTID__.'== 1 ? 'lets_flex' : '') . '">
                           ' . $alternate_slide . '
                           <div class="clear"></div>
                  ';
						if ($counted_childs > 0) {
							$html .= '
                           <div id="collapse' . $i_block . '" class=" delta grid_12 panel-collapse collapse in" aria-expanded="true" style="">
                              <div class="panel-body" style="color:black">

                     ';
							$db->execute($query_child_block);
							$i_child_block = 0;
							$html_child = "";
							while ($child_block = $db->fetch($query_child_block)) {
								if ($i_child_block %'.__CONTACTID__.'== 0) {
									$alternate_slide_child = '
                           <div class="alpha table-cell-custom grid_6 grid_1024_12 block_1 fadeInLeft wow" data-wow-duration="1.1s" data-wow-delay="0.2s">
                               <div class=" ">
                                   <div style="color:#000;">
                                       <h2>' . $child_block->{'title' . $l} . '</h2><br>
                                       ' . $child_block->{'texte' . $l} . '
                                       <br>
                                   </div>
                               </div>
                           </div>
                           <div class="omega about_section table-cell-custom grid_6 block_2 grid_1024_12 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="0.4s">
                               <div class="div_wrap_about" style="background: url(' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024) center; background-size: cover;background-attachment: scroll;background-position: center;">
                                   <img class="about_img" src="' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024" alt="' . $child_block->alt . '" >
                               </div>
                           </div>
                           ';
								} else {

									$alternate_slide_child = '
                           <div class="alpha about_section table-cell-custom grid_6 block_1 grid_1024_12 fadeInLeft wow" data-wow-duration="1.1s" data-wow-delay="0.2s">
                               <div class="div_wrap_about" style="background: url(' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024) center; background-size: cover;background-attachment: scroll;background-position: center;">
                                   <img class="about_img" src="' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024" alt="' . $child_block->alt . '" >
                               </div>
                           </div>
                           <div class="omega table-cell-custom grid_6 grid_1024_12 block_2 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="0.4s">
                               <div class=" ">
                                   <div style="color:#000;">
                                       <h2>' . $child_block->{'title' . $l} . '</h2><br>
                                       ' . $child_block->{'texte' . $l} . '
                                       <br>
                                   </div>
                               </div>
                           </div>
                           ';
								}
								$html_child .= '
                        <div class="table-custom section "
                           style="width: 100%;/*background: url(' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024);*/ background-size: cover;
                           background-position: center;
                           background-attachment:fixed;
                           background-repeat: no-repeat;">
                           <div id="acordeon">
                              <div class="panel-group" id="accordion">
                                 <div class="panel panel-default ' . ($i_child_block %'.__CONTACTID__.'== 1 ? 'lets_flex' : '') . ' ">
                                 ' . $alternate_slide_child . '
									<div class="clear"></div>
								</div>
                              </div>
                           </div>
                        </div>
                        ';
								$i_child_block++;
							}
							$html .= $html_child;
							$html .= '
                              </div>
                           </div>
                     ';
						}
						$html .= '
                        </div>
                     </div>
                  </div>
              </div>
                  ';
						$i_block++;
						break;
				}
				$i++;
			}
			return $html;
		} else {
			return false;
		}
	}

	function getBlocksByTypeContests($id)
	{
		global $lang, $l;
		$db = new Database();
		$query_blocks = $db->query(
			"
         SELECT * FROM tbl_contests
         WHERE parent = :id
         ORDER BY `order` ASC
      "
		);
		$i = 0;

		//increment variable for sliders
		$i_slide = 0;
		$i_block = 1;
		$db->bind($query_blocks, ":id", $id);
		if ($db->rowCount($query_blocks) > 0) {

			$html = "";
			while ($block = $db->fetch($query_blocks)) {

				$query_get = $db->query("SELECT * ,tbl_contests_inputs.value as value, i.name as name FROM tbl_contests_inputs LEFT JOIN inputs i ON i.id = tbl_contests_inputs.type WHERE fk_contests = :id");

				$db->bind($query_get, ":id", $block->id);
				$db->execute($query_get);
				while ($joinit = $db->fetch($query_get)) {
					$block->{$joinit->name} = $joinit->value;
				}
				switch ($block->type) {
					/* --- HOME --- */ //Slides carousel (Une slide)
					case __ASLIDE__:
						if ($i_slide %'.__CONTACTID__.'== 0) {
							//Align left
							$alternate_slide = '
                     <div class="alpha grid_6  grid_1024_12  text-center">
                        ' . ($block->{'title' . $l} ? '<h2 class=" text-center wow fadeInLeft" data-wow-delay="1s" >' . $block->{'title' . $l} . '</h2>' : '') . '
                        <br/>
                        <div class="wow fadeInLeft" >
                        ' . $block->{'texte' . $l} . '
                        </div>
                     </div>
                     <div class="grid_6 grid_1024_12">
                     </div>
                     <div class="omega clear">
                     </div>
                     ';
						} else {

							//Align right
							$alternate_slide = '
                     <div class="alpha grid_6  grid_1024_12">
                     </div>
                     <div class="grid_6 grid_1024_12 text-center">
                        ' . ($block->{'title' . $l} ? '<h2 class=" text-center wow fadeInLeft" data-wow-delay="1s" >' . $block->{'title' . $l} . '</h2>' : '') . '
                        <br/>
                        <div class="wow fadeInLeft" >
                        ' . $block->{'texte' . $l} . '
                        </div>
                     </div>
                     <div class="omega clear">
                     </div>';
						}
						$html .= '
                  <div class="carousel-item ' . ($i == 0 ? 'active' : '') . '" style="' . (trim($block->image) !== "" ? 'background-size: cover;background-attachment:fixed;background-repeat:no-repeat;background-position: center;background-image: url(' . '\'' . DragonHide(URLROOT . 'image.php?path=' . $block->image) . '~l=1024\'' . ')' : '') . '">
                     <div class="page-header" >
                        <div class="filter"></div>
                        <div class="wrap_eighty">
                           <div class="">
                              <div class="row">
                                 ' . $alternate_slide . '
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  ';
						$i_slide++;
						break;

					case 31:

						$query_child_block = $db->query(
							"
                     SELECT * FROM tbl_contests
                     WHERE parent = :id"
						);
						$db->bind($query_child_block, ":id", $block->id);
						$counted_childs = $db->rowCount($query_child_block);

						if ($i_block %'.__CONTACTID__.'== 0) {
							$alternate_slide = '
                     <div class="alpha table-cell-custom grid_6 grid_1024_12 block_1 fadeInLeft wow" data-wow-duration="1.5s" data-wow-delay="1s">
                         <div class=" ">
                             <div style="color:#000;">
                                 <h2>' . $block->{'title' . $l} . '</h2><br>
                                 ' . $block->{'texte' . $l} . '
                                 <br>
                                 ' . ($counted_childs > 0 ? '
                                 <div style="text-align:center">
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeA  btn btn btn-primary btn-round ">
                                 <span class="">' . $lang['see_more'] . '</span>
                                 </a>
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeB  btn btn btn-primary btn-round ">
                                 <span >' . $lang['see_less'] . '</span>
                                 </a></div>' : '') . '
                             </div>
                         </div>
                     </div>
                     <div class="omega about_section table-cell-custom grid_6 block_2 grid_1024_12 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="1.4s">
                         <div class="div_wrap_about" style="background: url(' . URLROOT . 'image.php?path=' . $block->image . '~l=1024) center; background-size: cover;background-attachment: scroll;background-position: center;">
                             <img class="about_img" src="' . URLROOT . 'image.php?path=' . $block->image . '~l=1024" alt="' . $block->alt . '" >
                         </div>
                     </div>
                     ';
						} else {

							$alternate_slide = '
                     <div class="alpha about_section table-cell-custom grid_6 block_1 grid_1024_12 fadeInLeft wow" data-wow-duration="1.5s" data-wow-delay="1.4s">
                         <div class="div_wrap_about" style="background: url(' . URLROOT . 'image.php?path=' . $block->image . '~l=1024) center; background-size: cover;background-attachment: scroll;background-position: center;">
                             <img class="about_img" src="' . URLROOT . 'image.php?path=' . $block->image . '~l=1024" alt="' . $block->alt . '" >
                         </div>
                     </div>
                     <div class="omega table-cell-custom grid_6 grid_1024_12 block_2 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="1s">
                         <div class=" ">
                             <div style="color:#000;">
                                 <h2>' . $block->{'title' . $l} . '</h2><br>
                                 ' . $block->{'texte' . $l} . '
                                 <br>
                                 ' . ($counted_childs > 0 ? '
                                 <div style="text-align:center">
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeA  btn btn btn-primary btn-round ">
                                 <span class="">' . $lang['see_more'] . '</span>
                                 </a>
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeB  btn btn btn-primary btn-round ">
                                 <span >' . $lang['see_less'] . '</span>
                                 </a></div>' : '') . '
                             </div>
                         </div>
                     </div>
                     ';
						}

						$html .= '
                  <div class="table-custom section  "
                     style="width: 100%;/*background: url(' . URLROOT . 'image.php?path=' . $block->image . '~l=1024);*/ background-size: cover;
                     background-position: center;
                     background-attachment:fixed;
                     background-repeat: no-repeat;">
                     <div id="acordeon' . $i_block . '">
                        <div class="panel-group" id="accordion' . $i_block . '">
                           <div class="panel panel-default ' . ($i_block %'.__CONTACTID__.'== 1 ? 'lets_flex' : '') . '">
                           ' . $alternate_slide . '
                           <div class="clear"></div>
                  ';
						if ($counted_childs > 0) {
							$html .= '
                           <div id="collapse' . $i_block . '" class=" delta grid_12 panel-collapse collapse in" aria-expanded="true" style="">
                              <div class="panel-body" style="color:black">

                     ';
							$db->execute($query_child_block);
							$i_child_block = 0;
							$html_child = "";
							while ($child_block = $db->fetch($query_child_block)) {
								if ($i_child_block %'.__CONTACTID__.'== 0) {
									$alternate_slide_child = '
                           <div class="alpha table-cell-custom grid_6 grid_1024_12 block_1 fadeInLeft wow" data-wow-duration="1.1s" data-wow-delay="0.2s">
                               <div class=" ">
                                   <div style="color:#000;">
                                       <h2>' . $child_block->{'title' . $l} . '</h2><br>
                                       ' . $child_block->{'texte' . $l} . '
                                       <br>
                                   </div>
                               </div>
                           </div>
                           <div class="omega about_section table-cell-custom grid_6 block_2 grid_1024_12 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="0.4s">
                               <div class="div_wrap_about" style="background: url(' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024) center; background-size: cover;background-attachment: scroll;background-position: center;">
                                   <img class="about_img" src="' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024" alt="' . $child_block->alt . '" >
                               </div>
                           </div>
                           ';
								} else {

									$alternate_slide_child = '
                           <div class="alpha about_section table-cell-custom grid_6 block_1 grid_1024_12 fadeInLeft wow" data-wow-duration="1.1s" data-wow-delay="0.2s">
                               <div class="div_wrap_about" style="background: url(' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024) center; background-size: cover;background-attachment: scroll;background-position: center;">
                                   <img class="about_img" src="' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024" alt="' . $child_block->alt . '" >
                               </div>
                           </div>
                           <div class="omega table-cell-custom grid_6 grid_1024_12 block_2 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="0.4s">
                               <div class=" ">
                                   <div style="color:#000;">
                                       <h2>' . $child_block->{'title' . $l} . '</h2><br>
                                       ' . $child_block->{'texte' . $l} . '
                                       <br>
                                   </div>
                               </div>
                           </div>
                           ';
								}
								$html_child .= '
                        <div class="table-custom section  "
                           style="width: 100%;/*background: url(' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024);*/ background-size: cover;
                           background-position: center;
                           background-attachment:fixed;
                           background-repeat: no-repeat;">
                           <div id="acordeon">
                              <div class="panel-group" id="accordion">
                                 <div class="panel panel-default ' . ($i_child_block %'.__CONTACTID__.'== 1 ? 'lets_flex' : '') . '">
                                 ' . $alternate_slide_child . '
									<div class="clear"></div>
								</div>
                              </div>
                           </div>
                        </div>
                        ';
								$i_child_block++;
							}
							$html .= $html_child;
							$html .= '
                              </div>
                           </div>
                     ';
						}
						$html .= '
                        </div>
                     </div>
                  </div>
              </div>
                  ';
						$i_block++;
						break;
				}
				$i++;
			}
			return $html;
		} else {
			return false;
		}
	}

	function getBlocksByTypePromotions($id)
	{
		global $lang, $l;
		$db = new Database();
		$query_blocks = $db->query(
			"
         SELECT * FROM tbl_promotions
         WHERE parent = :id
         ORDER BY `order` ASC
      "
		);
		$i = 0;

		//increment variable for sliders
		$i_slide = 0;
		$i_block = 1;
		$db->bind($query_blocks, ":id", $id);
		if ($db->rowCount($query_blocks) > 0) {

			$html = "";
			while ($block = $db->fetch($query_blocks)) {

				$query_get = $db->query("SELECT * ,tbl_promotions_inputs.value as value, i.name as name FROM tbl_promotions_inputs LEFT JOIN inputs i ON i.id = tbl_promotions_inputs.type WHERE fk_promotions = :id");

				$db->bind($query_get, ":id", $block->id);
				$db->execute($query_get);
				while ($joinit = $db->fetch($query_get)) {
					$block->{$joinit->name} = $joinit->value;
				}
				switch ($block->type) {
					/* --- HOME --- */ //Slides carousel (Une slide)
					case 10:
						if ($i_slide %'.__CONTACTID__.'== 0) {
							//Align left
							$alternate_slide = '
                     <div class="alpha grid_6  grid_1024_12  text-center">
                        ' . ($block->{'title' . $l} ? '<h2 class=" text-center wow fadeInLeft" data-wow-delay="1s" >' . $block->{'title' . $l} . '</h2>' : '') . '
                        <br/>
                        <div class="wow fadeInLeft" >
                        ' . $block->{'texte' . $l} . '
                        </div>
                     </div>
                     <div class="grid_6 grid_1024_12">
                     </div>
                     <div class="omega clear">
                     </div>
                     ';
						} else {

							//Align right
							$alternate_slide = '
                     <div class="alpha grid_6  grid_1024_12">
                     </div>
                     <div class="grid_6 grid_1024_12 text-center">
                        ' . ($block->{'title' . $l} ? '<h2 class=" text-center wow fadeInLeft" data-wow-delay="1s" >' . $block->{'title' . $l} . '</h2>' : '') . '
                        <br/>
                        <div class="wow fadeInLeft" >
                        ' . $block->{'texte' . $l} . '
                        </div>
                     </div>
                     <div class="omega clear">
                     </div>';
						}
						$html .= '
                  <div class="carousel-item ' . ($i == 0 ? 'active' : '') . '" style="' . (trim($block->image) !== "" ? 'background-size: cover;background-attachment:fixed;background-repeat:no-repeat;background-position: center;background-image: url(' . '\'' . DragonHide(URLROOT . 'image.php?path=' . $block->image) . '~l=1024\'' . ')' : '') . '">
                     <div class="page-header" >
                        <div class="filter"></div>
                        <div class="wrap_eighty">
                           <div class="">
                              <div class="row">
                                 ' . $alternate_slide . '
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  ';
						$i_slide++;
						break;
					//Slides carousel (Une slide avec produits vedettes)
					case 19:

						if ($i_slide %'.__CONTACTID__.'== 0) {
							//Align left
							$alternate_slide = '
                     <div class="alpha grid_6   grid_1024_12 text-center">
                        ' . ($block->{'title' . $l} ? '<h2 class=" text-center wow fadeInLeft" data-wow-delay="1s" >' . $block->{'title' . $l} . '</h2>' : '') . '
                        <br/>
                        <div class="wow fadeInLeft" >
                        ' . $block->{'texte' . $l} . '
                        </div>
                        <div class="buttons wow fadeInLeft" data-wow-delay="1.4s">
                           <a href="' . getUrlLang(__PRODUCTSID__) . '" class="btn btn btn-primary btn-round ">
                           ' . $lang['see_shop'] . '
                           </a>
                           <br>
                        </div>
                     </div>
                     <div class="grid_6 grid_1024_12 bg-black">
                        <!--getProductsVedettes()-->
                     </div>
                     <div class="omega clear">
                     </div>
                     ';
						} else {

							//Align right
							$alternate_slide = '
                     <div class="alpha grid_6 bg-black grid_1024_12">
                        <!--getProductsVedettes()-->
                     </div>
                     <div class="grid_6 grid_1024_12 text-center">
                        ' . ($block->{'title' . $l} ? '<h2 class=" text-center wow fadeInLeft" data-wow-delay="1s" >' . $block->{'title' . $l} . '</h2>' : '') . '
                        <br/>
                        <div class="wow fadeInLeft" >
                        ' . $block->{'texte' . $l} . '
                        </div>
                        <div class="buttons wow fadeInLeft" data-wow-delay="1.4s">
                           <a href="' . getUrlLang(__PRODUCTSID__) . '" class="btn btn btn-primary btn-round ">
                           ' . $lang['see_shop'] . '
                           </a>
                           <br>
                        </div>
                     </div>
                     <div class="omega clear">
                     </div>';
						}
						$html .= '
                  <div class="carousel-item ' . ($i == 0 ? 'active' : '') . '" style="' . ($i == 0 ? 'active' : '') . ' ' . (trim($block->image) !== "" ? 'background-size: cover;  /* -webkit-transform: translate3d(0, 0, 0) !important;*/ background-attachment:fixed;background-repeat:no-repeat;background-position: center;background-image: url(' . '\'' . DragonHide(URLROOT . 'image.php?path=' . $block->image) . '~l=1024\'' . ')' : '') . '">
                     <div class="page-header" >
                        <div class="filter"></div>
                        <div class="wrap_eighty">
                           <div class="">
                              <div class="row">
                                 ' . $alternate_slide . '
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  ';
						$i_slide++;
						break;

					case 31:

						$query_child_block = $db->query(
							"
                     SELECT * FROM tbl_promotions
                     WHERE parent = :id"
						);
						$db->bind($query_child_block, ":id", $block->id);
						$counted_childs = $db->rowCount($query_child_block);

						if ($i_block %'.__CONTACTID__.'== 0) {
							$alternate_slide = '
                     <div class="alpha table-cell-custom grid_6 grid_1024_12 block_1 fadeInLeft wow" data-wow-duration="1.5s" data-wow-delay="1s">
                         <div class=" ">
                             <div style="color:#000;">
                                 <h2>' . $block->{'title' . $l} . '</h2><br>
                                 ' . $block->{'texte' . $l} . '
                                 <br>
                                 ' . ($counted_childs > 0 ? '
                                 <div style="text-align:center">
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeA  btn btn btn-primary btn-round ">
                                 <span class="">' . $lang['see_more'] . '</span>
                                 </a>
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeB  btn btn btn-primary btn-round ">
                                 <span >' . $lang['see_less'] . '</span>
                                 </a></div>' : '') . '
                             </div>
                         </div>
                     </div>
                     <div class="omega about_section table-cell-custom grid_6 block_2 grid_1024_12 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="1.4s">
                         <div class="div_wrap_about" style="background: url(' . URLROOT . 'image.php?path=' . $block->image . '~l=1024) center; background-size: cover;background-attachment: scroll;background-position: center;">
                             <img class="about_img" src="' . URLROOT . 'image.php?path=' . $block->image . '~l=1024" alt="' . $block->alt . '" >
                         </div>
                     </div>
                     ';
						} else {

							$alternate_slide = '
                     <div class="alpha about_section table-cell-custom grid_6 block_1 grid_1024_12 fadeInLeft wow" data-wow-duration="1.5s" data-wow-delay="1.4s">
                         <div class="div_wrap_about" style="background: url(' . URLROOT . 'image.php?path=' . $block->image . '~l=1024) center; background-size: cover;background-attachment: scroll;background-position: center;">
                             <img class="about_img" src="' . URLROOT . 'image.php?path=' . $block->image . '~l=1024" alt="' . $block->alt . '">
                         </div>
                     </div>
                     <div class="omega table-cell-custom grid_6 grid_1024_12 block_2 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="1s">
                         <div class=" ">
                             <div style="color:#000;">
                                 <h2>' . $block->{'title' . $l} . '</h2><br>
                                 ' . $block->{'texte' . $l} . '
                                 <br>
                                 ' . ($counted_childs > 0 ? '
                                 <div style="text-align:center">
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeA  btn btn btn-primary btn-round ">
                                 <span class="">' . $lang['see_more'] . '</span>
                                 </a>
                                 <a  data-toggle="collapse" href="#collapse' . $i_block . '"   aria-expanded="true" class=" changeB  btn btn btn-primary btn-round ">
                                 <span >' . $lang['see_less'] . '</span>
                                 </a></div>' : '') . '
                             </div>
                         </div>
                     </div>
                     ';
						}

						$html .= '
                  <div class="table-custom section  "
                     style="width: 100%;/*background: url(' . URLROOT . 'image.php?path=' . $block->image . '~l=1024);*/ background-size: cover;
                     background-position: center;
                     background-attachment:fixed;
                     background-repeat: no-repeat;">
                     <div id="acordeon' . $i_block . '">
                        <div class="panel-group" id="accordion">
                           <div class="panel panel-default ' . ($i_block %'.__CONTACTID__.'== 1 ? 'lets_flex' : '') . '">
                           ' . $alternate_slide . '
                           <div class="clear"></div>
                  ';
						if ($counted_childs > 0) {
							$html .= '
                           <div id="collapse' . $i_block . '" class=" delta grid_12 panel-collapse collapse in" aria-expanded="true" style="">
                              <div class="panel-body" style="color:black">

                     ';
							$db->execute($query_child_block);
							$i_child_block = 0;
							$html_child = "";
							while ($child_block = $db->fetch($query_child_block)) {
								if ($i_child_block %'.__CONTACTID__.'== 0) {
									$alternate_slide_child = '
                           <div class="alpha table-cell-custom grid_6 grid_1024_12 block_1 fadeInLeft wow" data-wow-duration="1.1s" data-wow-delay="0.2s">
                               <div class=" ">
                                   <div style="color:#000;">
                                       <h2>' . $child_block->{'title' . $l} . '</h2><br>
                                       ' . $child_block->{'texte' . $l} . '
                                       <br>
                                   </div>
                               </div>
                           </div>
                           <div class="omega about_section table-cell-custom grid_6 block_2 grid_1024_12 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="0.4s">
                               <div class="div_wrap_about" style="background: url(' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024) center; background-size: cover;background-attachment: scroll;background-position: center;">
                                   <img class="about_img" src="' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024" alt="' . $child_block->alt . '" >
                               </div>
                           </div>
                           ';
								} else {

									$alternate_slide_child = '
                           <div class="alpha about_section table-cell-custom grid_6 block_1 grid_1024_12 fadeInLeft wow" data-wow-duration="1.1s" data-wow-delay="0.2s">
                               <div class="div_wrap_about" style="background: url(' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024) center; background-size: cover;background-attachment: scroll;background-position: center;">
                                   <img class="about_img" src="' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024" alt="' . $child_block->alt . '" >
                               </div>
                           </div>
                           <div class="omega table-cell-custom grid_6 grid_1024_12 block_2 fadeInRight wow" data-wow-duration="1.5s" data-wow-delay="0.4s">
                               <div class=" ">
                                   <div style="color:#000;">
                                       <h2>' . $child_block->{'title' . $l} . '</h2><br>
                                       ' . $child_block->{'texte' . $l} . '
                                       <br>
                                   </div>
                               </div>
                           </div>
                           ';
								}
								$html_child .= '
                        <div class="table-custom section  "
                           style="width: 100%;/*background: url(' . URLROOT . 'image.php?path=' . $child_block->image . '~l=1024);*/ background-size: cover;
                           background-position: center;
                           background-attachment:fixed;
                           background-repeat: no-repeat;">
                           <div id="acordeon' . $i_child_block . '">
                              <div class="panel-group" id="accordion' . $i_child_block . '">
                                 <div class="panel panel-default ' . ($i_child_block %'.__CONTACTID__.'== 1 ? 'lets_flex' : '') . '">
                                 ' . $alternate_slide_child . '
									<div class="clear"></div>
								</div>
                              </div>
                           </div>
                        </div>
                        ';
								$i_child_block++;
							}
							$html .= $html_child;
							$html .= '
                              </div>
                           </div>
                     ';
						}
						$html .= '
                        </div>
                     </div>
                  </div>
              </div>
                  ';
						$i_block++;
						break;
				}
				$i++;
			}
			return $html;
		} else {
			return false;
		}
	}

	function ariane($id, $ariane = '', $loop = '')
	{
		global $l, $lang;

		$db = new Database();
		$qpage = $db->query('SELECT * FROM tbl_pages WHERE id= :id and online=1');
		$db->bind($qpage, ":id", $id);
		$page = $db->single($qpage);
		if ($page) {
			$idParent = $page->{'parent'};
			$qparent = $db->query('SELECT * FROM tbl_pages WHERE id= :id and online=1');
			$db->bind($qparent, ":id", $idParent);
			$parent = $db->single($qparent);

			if ($page->type) {
				$qmod = $db->query('SELECT module FROM tess_controller WHERE id= :id AND has_slug = 1');
				$db->bind($qmod, ":id", $page->type);
				$module = $db->single($qmod);
			}

			if ($module) {
				if ($ariane == '' && $loop == "") { //No link on current page
					$ariane = '<li>' . $page->{'title' . $l} . '</li>' . $ariane;
				} else {
					$ariane = '<li><a href="' . getUrlLang($page->id) . '" title="' . $page->{'title' . $l} . '"' . ($page->id == 1 ? ' rel="home"' : '') . '>' . $page->{'title' . $l} . '</a> <div class="wrapperSeparator"><div class="SepTable-cell"><div class="sep"></div></div></div> </li>' . $ariane;
				}
			}
			if ($page->{'parent'} != 0) {
				$ariane = ariane($page->{'parent'}, $ariane);
			} else if ($page->id != 1) {
				$ariane = ariane(1, $ariane); //Home page
			}
		}
		return $ariane;
	}

	function arianeProducts($id, $ariane = '')
	{
		global $l, $lang;

		$db = new Database();
		$qpage = $db->query('SELECT * FROM tbl_products WHERE id= :id and online=1');
		$db->bind($qpage, ":id", $id);
		$page = $db->single($qpage);
		if ($page) {
			$idParent = $page->{'parent'};
			$qparent = $db->query('SELECT * FROM tbl_products WHERE id= :id and online=1');
			$db->bind($qparent, ":id", $idParent);
			$parent = $db->single($qparent);

			if ($page->type) {
				$qmod = $db->query('SELECT module FROM tess_controller WHERE id= :id AND has_slug = 1');
				$db->bind($qmod, ":id", $page->type);
				$module = $db->single($qmod);
			}

			if ($module) {
				if ($ariane == '') { //No link on current page
					$ariane = '<li>' . $page->{'title' . $l} . '</li>' . $ariane;
				} else {
					$ariane = '<li><a href="' . getUrlLangProducts($page->id) . '" title="' . $page->{'title' . $l} . '"' . ($page->id == 1 ? ' rel="home"' : '') . '>' . $page->{'title' . $l} . '</a> <div class="wrapperSeparator"><div class="SepTable-cell"><div class="sep"></div></div></div> </li>' . $ariane;
				}
			}
			if ($page->{'parent'} != 0) {
				$ariane = arianeProducts($page->{'parent'}, $ariane);
			}
			// else if($page->id != 1){
			// $ariane = arianeProducts(1, $ariane); //Home page
			// }
		}
		return $ariane;
	}

	function arianeBlogs($id, $ariane = '')
	{
		global $l, $lang;

		$db = new Database();
		$qpage = $db->query('SELECT * FROM tbl_blogs WHERE id= :id and online=1');
		$db->bind($qpage, ":id", $id);
		$page = $db->single($qpage);
		if ($page) {
			$idParent = $page->{'parent'};
			$qparent = $db->query('SELECT * FROM tbl_blogs WHERE id= :id and online=1');
			$db->bind($qparent, ":id", $idParent);
			$parent = $db->single($qparent);

			if ($page->type) {
				$qmod = $db->query('SELECT module FROM tess_controller WHERE id= :id AND has_slug = 1');
				$db->bind($qmod, ":id", $page->type);
				$module = $db->single($qmod);
			}

			if ($module) {
				if ($ariane == '') { //No link on current page
					$ariane = '<li>' . $page->{'title' . $l} . '</li>' . $ariane;
				} else {
					$ariane = '<li><a href="' . getUrlLangBlogs($page->id) . '" title="' . $page->{'title' . $l} . '"' . ($page->id == 1 ? ' rel="home"' : '') . '>' . $page->{'title' . $l} . '</a> / </li>' . $ariane;
				}
			}
			if ($page->{'parent'} != 0) {
				$ariane = arianeBlogs($page->{'parent'}, $ariane);
			} else if ($page->id != 1) {
				$ariane = arianeBlogs(1, $ariane); //Home page
			}
		}
		return $ariane;
	}

	function arianeContests($id, $ariane = '')
	{
		global $l, $lang;

		$db = new Database();
		$qpage = $db->query('SELECT * FROM tbl_contests WHERE id= :id and online=1');
		$db->bind($qpage, ":id", $id);
		$page = $db->single($qpage);
		if ($page) {
			$idParent = $page->{'parent'};
			$qparent = $db->query('SELECT * FROM tbl_contests WHERE id= :id and online=1');
			$db->bind($qparent, ":id", $idParent);
			$parent = $db->single($qparent);

			if ($page->type) {
				$qmod = $db->query('SELECT module FROM tess_controller WHERE id= :id AND has_slug = 1');
				$db->bind($qmod, ":id", $page->type);
				$module = $db->single($qmod);
			}

			if ($module) {
				if ($ariane == '') { //No link on current page
					$ariane = '<li>' . $page->{'title' . $l} . '</li>' . $ariane;
				} else {
					$ariane = '<li><a href="' . getUrlLangContests($page->id) . '" title="' . $page->{'title' . $l} . '"' . ($page->id == 1 ? ' rel="home"' : '') . '>' . $page->{'title' . $l} . '</a> / </li>' . $ariane;
				}
			}
			if ($page->{'parent'} != 0) {
				$ariane = arianeContests($page->{'parent'}, $ariane);
			} else if ($page->id != 1) {
				$ariane = arianeContests(1, $ariane); //Home page
			}
		}
		return $ariane;
	}

	function arianePromotions($id, $ariane = '')
	{
		global $l, $lang;

		$db = new Database();
		$qpage = $db->query('SELECT * FROM tbl_promotions WHERE id= :id and online=1');
		$db->bind($qpage, ":id", $id);
		$page = $db->single($qpage);
		if ($page) {
			$idParent = $page->{'parent'};
			$qparent = $db->query('SELECT * FROM tbl_promotions WHERE id= :id and online=1');
			$db->bind($qparent, ":id", $idParent);
			$parent = $db->single($qparent);

			if ($page->type) {
				$qmod = $db->query('SELECT module FROM tess_controller WHERE id= :id  AND has_slug = 1');
				$db->bind($qmod, ":id", $page->type);
				$module = $db->single($qmod);
			}

			if ($module) {
				if ($ariane == '') { //No link on current page
					$ariane = '<li>' . $page->{'title' . $l} . '</li>' . $ariane;
				} else {
					$ariane = '<li><a href="' . getUrlLangPromotions($page->id) . '" title="' . $page->{'title' . $l} . '"' . ($page->id == 1 ? ' rel="home"' : '') . '>' . $page->{'title' . $l} . '</a> / </li>' . $ariane;
				}
			}
			if ($page->{'parent'} != 0) {
				$ariane = arianePromotions($page->{'parent'}, $ariane);
			} else if ($page->id != 1) {
				$ariane = arianePromotions(1, $ariane); //Home page
			}
		}
		return $ariane;
	}


	function getProductsVedettes($id = "sync1", $limit = "")
	{
		global $lang, $l;
		$db = new Database();
		$html = "";
		$query_products_vedettes = $db->query(
			"
         SELECT * FROM
         tbl_products WHERE online = 1 AND parent != 0
         ORDER BY `order` ASC " . ($limit !== "" ? "LIMIT " . $limit : "")
		);
		$counted_products = $db->rowCount($query_products_vedettes);
		if ($counted_products > 0) {
			$html .= '
            <div id="' . $id . '" class="owl-carousel owl-theme">
         ';
			while ($product = $db->fetch($query_products_vedettes)) {
				$join_product = $db->query("SELECT t.*, i.name FROM tbl_products_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_products = :id_product");
				$db->bind($join_product, ":id_product", $product->id);
				$db->execute($join_product);
				while ($joins = $db->fetch($join_product)) {

					if(isset($product->{$joins->name}) && $joins->name != "")
						$product->{$joins->name} = $joins->value;
				}
				$parent_product = $db->query("SELECT * FROM tbl_products WHERE id = :parent");
				$db->bind($parent_product, ":parent", $product->parent);
				$parent = $db->single($parent_product);
				$html .= '
            <div class="item" style="-webkit-transform: translate3d(0, 0, 0) !important;">
             <h4 class="collection_title">
            ' . $parent->{'title' . $l} . '
             </h4>
             <img class="d-block w-100" src="' . URLROOT . 'image.php?path=' . str_replace(" ", "%20", $product->image) . '~l=200" alt="Le héroique">
             <div class="carousel-caption  d-md-block">
               <h4>
               ' . $product->{'title' . $l} . '
               </h4>
               <h5>
               ' . number_format((isset($product->price) ? $product->price :  0), 2, '.', '.') . ' $ CAD
               </h5>

               <div class="buttons">
                  <a href="' . getUrlLangProducts($product->id) . '" class="btn">
                  ' . $lang['view_product'] . '
                  </a>
                  ' . ((isset($product->quantity) ? $product->quantity :  0) <= 0 ? '<br><span class="error">' . $lang['out_of_stock'] . '</span>' : '
                  <div class="quantity">
                     <a class="btn btn-custom qty_decrease">-</a>
                     <input class="quantity_input" type="number" min="1" max="' . (isset($product->quantity) ? $product->quantity :  0) . '" disabled value="1" >
                     <a class="btn btn-custom qty_increase">+</a>
                  </div>
                  <a href="' . getUrlLangProducts($product->id) . '" data-product-id="' . $product->id . '" class="btn add_to_cart ">
                  ' . $lang['add_cart'] . '
                  </a>') . '
                  <br>
               </div>
            </div>
             </div>
            ';
			}
			$html .= '
           </div>
         ';
		}
		return $html;
	}

	function getAllCustoms($id, $table = "tbl_contests_inputs", $fk = "fk_contests")
	{
		try { 
		} catch (PDOException $e) {
			return false;
		}
	}

	function getCta($query, $isShop = false)
	{

		global $lang, $l;
		$query = $query;
		echo "
      <div class='cta' style=' 
         background-attachment: fixed;background-size: cover; background-repeat: no-repeat;background-position:center;'>
		 <div class='skewthis' ></div>
         <div class='delta grid_12 grid_1024_12'>
            <div class='cta_txt' data-wow-duration='1s' data-wow-delay='0.1s'>
               <div class='section_left'><h2>" . $query->{'title' . $l} . "</h2>
               " . $query->{'texte' . $l} . "</div>
			   <div class='section_right'>
                " . ($isShop ? "
               <br><a href='" . getUrlLang(__PRODUCTSID__) . "' class='blue-btn' >" . $lang['see_shop'] . "</a>
               " : "") . " <br><a href='" . URLROOT.getUrlLang(__CONTACTID__) . "' class=' submission_btn' >Faire une soumission</a>
			   </div>
            </div>

         </div>
         
         <div class='clear'></div>
      </div>";
	}

	function arrayContainsDuplicate($array)
	{
		return count($array) != count(array_unique($array));
	}

	function getBlog($offset = 0, $limit = 20, $is_home = false)
	{
		global $l, $lang;

		$db = new Database();

		$query_blogs = $db->query(
			"
         SELECT *
         FROM tbl_blogs
         WHERE type = ".__ANARTICLE__."
		 AND online = 1
         ORDER BY create_ts DESC LIMIT :offset, :limit
       "
		);
		$db->bind($query_blogs, ":offset", $offset);
		$db->bind($query_blogs, ":limit", $limit);
		$counted_articles = $db->rowCount($query_blogs);
		if ($counted_articles > 0) {
			echo '
      <div id="preview_blog" ' . ($is_home ? 'style="
       margin: 0px auto;
    "' : '') . '>
		 <div class="skewthis" ></div>
         ';
			$i = 0;
			while ($article = $db->fetch($query_blogs)) {
				$query_categ = $db->query(
					"
               SELECT *
               FROM tbl_blogs
               WHERE id = :parent
            "
				);
				$db->bind($query_categ, ":parent", $article->parent);
				$categ = $db->single($query_categ);
				$join_categ = $db->query("SELECT t.*, i.name FROM tbl_blogs_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_blogs = :id_page");
				$db->bind($join_categ, ":id_page", $categ->id);
				$db->execute($join_categ);
				while ($joins = $db->fetch($join_categ)) {
					if(isset($categ->{$joins->name}) && $joins->name != "")
						$categ->{$joins->name} = $joins->value;
				}

					if ($i == 0 || ($i % 3 == 0 && $i > 0)) {
						echo '<div class="row">';
						$side = 'alpha';
					} else if ($i % 3 == 2 || $i == $counted_articles) {
						$side = 'omega';
					} else {
						$side = "";
					}
					echo '
					 <div class="' . $side . ' grid_4 grid_1024_12    " >
					 <a href="' . getUrlLangBlogs($article->id) . '"  >
						   <div class="card card-raised card-background" style=" border-radius:0px;padding: 10px;overflow: hidden;background-image: url(' . (isset($article->image) ? URLROOT . 'image.php?path=' . str_replace(" ", "%20", $article->image) : URLROOT.'images/bg2.jpg') . '~l=500)">
							   <div class="card-content" style="z-index:2">
								  <span class="date_articles" ><i class="fad fa-clock"></i> ' . date('Y-m-d', $article->create_ts) . '</span>
								  <div class="pcard" >
									 <h2>' . $article->{'title' . $l} . '</h2>
									 ' . cut($article->{'texte' . $l}, 200) . ' ...<br>
								  </div>
								  <div class="">
									 <div class="table-align">
										<div class="table_cell_align">
										   <img src="' . DragonHide(URLROOT . 'image.php?path=' . $article->image) . '~l=500" alt="' . $article->{'title' . $l} . '">
										</div>
									 </div>
								  </div>
								  <!--span class="categ_articles" style="
								  border: 2.5px solid black;background: ' .(isset($article->color) ? $categ->color : "white") . '"> ' . $categ->title . '</span-->
								  <span class="filter_product" style="background-attachment:fixed !important;background: url(' . (isset($article->image) ? URLROOT . 'image.php?path=' . str_replace(" ", "%20", $article->image) : URLROOT.'images/bg2.jpg') . '~l=500) center;"></span>
							   </div>
							</div>
						</a>
					 </div>
						   ';
					if (($i % 3 == 2 && $i > 0) || $i == $counted_articles) {
						echo '</div><div class="clear"></div>';
					}
				$i++;
			}

			echo '
         <div class="clear"></div>
         ' . ($is_home ? '</div><div class="clear"></div>
         <div class="footer_blog_preview">
            <a href="' . getUrlLang(__BLOGID__) . '" class="blue-btn" style="margin-bottom: 1%;">' . $lang['see_all_articles'] .
			             '</a>
         </div>
         ' : '') . '
      </div>
      <div class="clear"></div>';
		} else {
			echo '<p class="text-center">'.$lang['no_blogs'].'</p>';
		}
	}

	function getGallery($id = "", $mixed = false, $ordre = "tbl_pictures.`order` ASC")
	{
		if (!empty($id) && is_numeric($id)) {
			$db = new Database();
			if($mixed){
				$gallery = $db->query(
					"
				 SELECT tbl_pictures.*
				 FROM tbl_pictures_categories
				 LEFT JOIN tbl_pictures
				 ON tbl_pictures.categ = CONCAT('-', tbl_pictures_categories.id, '-')
				 LEFT JOIN tbl_pages tp ON tp.fk_gallery = tbl_pictures_categories.id
				 WHERE tp.parent = ".__REALISATIONSID__."
				 ORDER BY tbl_pictures.date DESC, :order
				 "
				);
				$db->bind($gallery, ":order", $ordre);
				$count = $db->rowCount($gallery);
				$result = $db->resultSet($gallery);
			}else{
				$gallery = $db->query(
					"
				 SELECT tbl_pictures.*
				 FROM tbl_pictures_categories
				 LEFT JOIN tbl_pictures
				 ON tbl_pictures.categ = CONCAT('-', tbl_pictures_categories.id, '-')
				 WHERE tbl_pictures_categories.id = :id
				 ORDER BY :order
				 "
				);
				$db->bind($gallery, ":id", $id);
				$db->bind($gallery, ":order", $ordre);
				$count = $db->rowCount($gallery);
				$result = $db->resultSet($gallery);
			}
			if ($count > 0) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/* --------- Paypal ------------------- */
	function _GET($label = '', $default = '', $set_default = false)
	{

		$value = $default;

		if (isset($_GET[$label]) && !empty($_GET[$label])) {

			$value = $_GET[$label];
		}

		if ($set_default === true && (!isset($_GET[$label]) || $_GET[$label] == '')) {

			$_GET[$label] = $default;
		}

		return $value;
	}

	function _POST($label = '', $default = '', $set_default = false)
	{

		$value = $default;

		if (isset($_POST[$label]) && !empty($_POST[$label])) {

			$value = $_POST[$label];
		}

		if ($set_default === true && (!isset($_POST[$label]) || $_POST[$label] == '')) {

			$_POST[$label] = $default;
		}

		return $value;
	}

	function _SESSION($label = '', $default = '', $set_default = false)
	{

		$value = $default;

		if (isset($_SESSION[$label]) && !empty($_SESSION[$label])) {

			$value = $_SESSION[$label];
		}

		if ($set_default === true && (!isset($_SESSION[$label]) || $_SESSION[$label] == '')) {

			$_SESSION[$label] = $default;
		}

		return $value;
	}

	function DragonProtectv1($value)
	{
		if (
			strpos($value, '<') !== false || strpos($value, '<script') !== false || strpos($value, '</script') !== false || strpos($value, '>') !== false || strpos($value, '%') !== false || strpos($value, '&#0__MAINTENANCEID__;') !== false || strpos($value, '&LT;') !== false || strpos($value, 'SCRIPT') !== false || strpos($value, '&gt;') !== false
		) {
			$value = str_replace('<script', '*', $value);
			$value = str_replace('SCRIPT', '*', $value);
			$value = str_replace('&LT;SCRIPT&GT;', '*', $value);
			$value = str_replace('</script', '*', $value);
			$value = str_replace('>', '*', $value);
			$value = str_replace('<', '*', $value);
			$value = str_replace('%', '-', $value);
			$value = str_replace('&LT;', '*', $value);
			$value = str_replace('&lt;', '*', $value);
			$value = str_replace('&GT;', '*', $value);
			$value = str_replace('&gt;', '*', $value);
			$value = str_replace('<script', '*', $value);
			$value = str_replace('/script>', '*', $value);
			$value = str_replace('&#0__MAINTENANCEID__;', '\'', $value);
		}

		$value = str_replace('`', '\'', $value);
		$value = str_replace('\r', ' ', $value);
		$value = str_replace('\n', ' ', $value);
		$value = str_replace('"', '\'', $value);
		$value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
		$value = trim($value);
		$value = html_entity_decode($value);
		$value = nl2br($value);
		$value = stripslashes($value);
		return $value;
	}

	function randomPassword()
	{
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ12__PAYMENTFAILID__5678__PRODUCTSID__0$^,.()_';
		$pass = array();                      //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}

	function getOrders($id, $limit = "")
	{

		global $_SESSION, $l, $lang;

		$db = new Database();
		$return = "";
		//get orders
		$qorders = $db->query(
			"
         SELECT tbl_orders.*, tbl_delivery.status as delivery_status FROM
         tbl_orders
         LEFT JOIN tbl_delivery
         ON tbl_delivery.id = tbl_orders.fk_delivery
         WHERE tbl_orders.fk_users = :id
         ORDER BY tbl_orders.id DESC
         " . ($limit !== "" ? "LIMIT :limit" : "") . "
      "
		);
		$db->bind($qorders, ":id", $id);
		if ($limit !== "") {
			$db->bind($qorders, ":limit", $limit);
		}
		$db->execute($qorders);
		if ($db->rowCount($qorders) > 0) {
			$return .= '<table class="orders_table">
                        <tr><th>' . $lang['order_number'] . '</th><th>' . $lang['price'] . '</th><th>Status</th><th>' . $lang['delivery'] . '</th><th></th></tr>
         ';
			while ($order = $db->fetch($qorders)) {
				$return .= '
            <tr class="an_order">
               <td>#' . $order->id . '</td>
               <td>' . number_format($order->final_price, 2, '.', '') . '</td>
               <td>' . ($order->status == "paid" ? $lang['paid'] : $lang['not_paid']) . '</td>
               <td>' . ($order->delivery_status == "shipped" ? $lang['shipped'] : $lang['not_shipped']) . '</td>
               <td>
                  <a title="Export" target="_blank" class="btn" href="' . URLROOT . 'ajax.php?bill=' . base64_encode($order->id . '||' . $order->date_created) . '">
                     <i class="fas fa-file-pdf"></i> ' . $lang['see_invoice'] . '
                  </a>
                  <a title="Export" target="_blank" class="btn" href="' . URLROOT . 'ajax.php?bill=' . base64_encode($order->id . '||' . $order->date_created) . '&download=true">
                     <i class="fas fa-file-pdf"></i> ' . $lang['download_invoice'] . '
                  </a>
               </td>
            </tr>';
			}
			$return .= '</table>';

		} else {
			$return = $lang['no_orders'];
		}
		return $return;
	}

	function is_https()
	{
		if (isset($_SERVER['HTTPS']) and $_SERVER['HTTPS'] == 1) {
			return TRUE;
		} elseif (isset($_SERVER['HTTPS']) and $_SERVER['HTTPS'] == 'on') {
			return TRUE;
		} else {
			return FALSE;
		}
	}


	function generateSitemap()
	{
		global $l, $lang, $doc_root, $nomDuSite, $base_url, $bilingue, $install_SEO, $array_no_index;

		$modules_path = APPROOT . '/views/';

		$path_directory = rtrim(APPROOT, '/');
		$filename = 'sitemap.xml';

		if (!file_exists(dirname(APPROOT) . '/public/' . $filename)) {
			fopen(dirname(APPROOT) . '/public/' . $filename, "w");
		}

		$db = new Database();
		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.__PRODUCTSID__"></urlset>');
		$request_pages = $db->query(
			'
		SELECT
			tp.*,
			tc.module
		FROM tbl_pages tp
		LEFT JOIN tess_controller tc ON tp.type = tc.id
		WHERE
			tc.module != "" AND
            tp.online = 1
			AND  tp.id != '. __404ID__ .'
			AND  tp.id != '. __SITEMAPID__  .'
			AND  tp.id != '.__CONDITIONSID__  .'
			AND  tp.id != '.__POLITIQUESID__  .'
			AND  tp.id != '.__THANKYOUID__ .'
			AND  tp.id != '.__PRODUCTSID__ .'
			AND  tp.id != '.__CHECKOUTID__ .'
			AND  tp.id != '.__DELIVERYPOLICYID__ .'
			AND  tp.id != '.__RETURNPOLICYID__ .'
			AND  tp.id != '.__PAYMENTFAILID__ .'
			AND  tp.id != '.__RECOVERYACCOUNTID__ .'
			AND  tp.id != '.__MAINTENANCEID__ .'
			AND  tp.id != '.__ACTIVATIONACCOUNTID__.'
			AND  tp.id != '.__APRODUCT__.'
			AND  tp.id != '.__PAYMENTTHANKYOUID__.'
			AND  tp.id != '.__ANARTICLE__.'
			AND  tp.id != '.__APROMO__.'
			AND  tp.id != '.__PROMOSID__.'
			AND  tp.id != '.__ACONTEST__.'
			AND  tp.id != '.__AREALISATION__.'
			AND  tp.id != '.__ASUBSCRIPTION__.'
			AND  tp.id != '.__CATEGCONTESTS__.'
			AND  tp.id != '.__CATEGPROMOS__.'
			AND  tp.id != '.__CATEGARTICLES__.'
			AND  tp.id != '.__CATEGPRODUCTS__.'
			AND  tp.id != '.__CAROUSELID__.'
			AND  tp.id != '.__ASLIDEID__.'
			AND  tp.id != '.__BFULLTEXTID__.'
			AND  tp.id != '.__BFULLIMGID__.'
			AND  tp.id != '.__BTEXTIMGID__.'
			AND  tp.id != '.__BDOUBLETEXTID__.'
			AND  tp.id != '.__BDOUBLEIMGID__.'
			AND  tp.id != '.__BVIDEOID__.'
			AND  tp.id != '.__BTEXTVIDEOID__.'
			AND  tp.id != '. __CGVSID__.'
		ORDER BY tp.id ASC
	'
		);
		$db->execute($request_pages);
		if ($db->rowCount($request_pages) > 0) {
			while ($page = $db->fetch($request_pages)) {
				// if(file_exists($modules_path . $page->module . '.php')){
				$url = $xml->addChild('url');
				// error_log($modules_path. $page->module.".php", 0);
				// error_log(rtrim(URLROOT, "/") . getUrlLang($page->id, 'fr'), 0);
				$url->addChild('loc', rtrim(URLROOT, "/") . getUrlLang($page->id, 'fr'));
				if ($page->edit_ts != '') {
					$url->addChild('lastmod', date('c', $page->edit_ts));
				}
				$url_en = $xml->addChild('url');
				$url_en->addChild('loc', rtrim(URLROOT, "/") . getUrlLang($page->id, 'en'));
				if ($page->edit_ts != '') {
					$url_en->addChild('lastmod', date('c', $page->edit_ts));
				}
				// }
			}
		}
		$queryproducts = $db->query(
			'
		SELECT tbl_products.*
        FROM tbl_products
        LEFT JOIN (SELECT id as idtc, has_slug FROM tess_controller) tc
        ON tc.idtc = tbl_products.type
        WHERE tbl_products.online = 1 ORDER BY parent, `order` ASC
	'
		);
		$db->execute($queryproducts);
		if ($db->rowCount($queryproducts) > 0) {
			while ($page = $db->fetch($queryproducts)) {
				// if(file_exists($modules_path . $page->module . '.php')){
				$url = $xml->addChild('url');
				// error_log($modules_path. $page->module.".php", 0);
				// error_log(rtrim(URLROOT, "/") . getUrlLang($page->id, 'fr'), 0);
				$url->addChild('loc', rtrim(URLROOT, "/") . getUrlLangProducts($page->id, 'fr'));
				if ($page->edit_ts != '') {
					$url->addChild('lastmod', date('c', $page->edit_ts));
				}
				$url_en = $xml->addChild('url');
				$url_en->addChild('loc', rtrim(URLROOT, "/") . getUrlLangProducts($page->id, 'en'));
				if ($page->edit_ts != '') {
					$url_en->addChild('lastmod', date('c', $page->edit_ts));
				}
				// }
			}
		}
		$queryproducts = $db->query(
			'
		SELECT tbl_blogs.*
        FROM tbl_blogs
        LEFT JOIN (SELECT id as idtc, has_slug FROM tess_controller) tc
        ON tc.idtc = tbl_blogs.type
        WHERE tbl_blogs.online = 1 ORDER BY parent, `order` ASC
	'
		);
		$db->execute($queryproducts);
		if ($db->rowCount($queryproducts) > 0) {
			while ($page = $db->fetch($queryproducts)) {
				// if(file_exists($modules_path . $page->module . '.php')){
				$url = $xml->addChild('url');
				// error_log($modules_path. $page->module.".php", 0);
				// error_log(rtrim(URLROOT, "/") . getUrlLang($page->id, 'fr'), 0);
				$url->addChild('loc', rtrim(URLROOT, "/") . getUrlLangBlogs($page->id, 'fr'));
				if ($page->edit_ts != '') {
					$url->addChild('lastmod', date('c', $page->edit_ts));
				}
				$url_en = $xml->addChild('url');
				$url_en->addChild('loc', rtrim(URLROOT, "/") . getUrlLangBlogs($page->id, 'en'));
				if ($page->edit_ts != '') {
					$url_en->addChild('lastmod', date('c', $page->edit_ts));
				}
				// }
			}
		}
		$xml->asXml(dirname(APPROOT) . '/public/' . $filename);
	}

	function generate_menu($parent){
		$db = new Database();
		$qchild = $db->query("
			SELECT tbl_pages.*
			FROM tbl_pages
			LEFT JOIN tess_controller
			ON tbl_pages.type = tess_controller.id
			WHERE tbl_pages.id = :parent
			AND tbl_pages.online = 1
			AND tess_controller.has_slug = 1
		");
		$db->bind($qchild, ":parent", $parent);
		$db->execute($qchild);
		$countChild = $db->rowCount($qchild);
		if($countChild > 0){

			echo '<ul>';
			while($child = $db->fetch($qchild)){

				echo '
				<li>
					<a href="' . getUrlLang($child->id) . '">
						' . $child->{'title' . $l} . '
					</a>
				';
				$qchild2 = $db->query("
					SELECT * FROM tbl_pages
					WHERE parent = :parent
				");
				$db->bind($qchild2, ":parent", $child->id);
				$countchild2 = $db->rowCount($qchild2);
				if($countchild2 > 0){
					generate_menu($child->id);
				}
				echo'
				</li>
				';
			}

			echo '</ul>';

		}
	}
	function generate_menu_blogs($parent){

		$db = new Database();
		$qchild = $db->query("
			SELECT tbl_blogs.*
			FROM tbl_blogs
			LEFT JOIN tess_controller
			ON tbl_blogs.type = tess_controller.id
			WHERE tbl_blogs.id = :parent
			AND tbl_blogs.online = 1
			AND tess_controller.has_slug = 1
		");
		$db->bind($qchild, ":parent", $parent);
		$db->execute($qchild);
		$countChild = $db->rowCount($qchild);
		if($countChild > 0){

			echo '<ul>';
			while($child = $db->fetch($qchild)){

				echo '
				<li>
					<a href="' . getUrlLang($child->id) . '">
						' . $child->{'title' . $l} . '
					</a>
				';
				$qchild2 = $db->query("
					SELECT * FROM tbl_blogs
					WHERE parent = :parent
				");
				$db->bind($qchild2, ":parent", $child->id);
				$countchild2 = $db->rowCount($qchild2);
				if($countchild2 > 0){
					generate_menu_blogs($child->id);
				}
				echo'
				</li>
				';
			}

			echo '</ul>';

		}
	}
	function generate_menu_products($parent){
		$db = new Database();
		$qchild = $db->query("
			SELECT tbl_products.*
			FROM tbl_products
			LEFT JOIN tess_controller
			ON tbl_products.type = tess_controller.id
			WHERE tbl_products.id = :parent
			AND tbl_products.online = 1
			AND tess_controller.has_slug = 1
		");
		$db->bind($qchild, ":parent", $parent);
		$db->execute($qchild);
		$countChild = $db->rowCount($qchild);
		if($countChild > 0){

			echo '<ul>';
			while($child = $db->fetch($qchild)){

				echo '
				<li>
					<a href="' . getUrlLang($child->id) . '">
						' . $child->{'title' . $l} . '
					</a>
				';
				$qchild2 = $db->query("
					SELECT * FROM tbl_products
					WHERE parent = :parent
				");
				$db->bind($qchild2, ":parent", $child->id);
				$countchild2 = $db->rowCount($qchild2);
				if($countchild2 > 0){
					generate_menu_products($child->id);
				}
				echo'
				</li>
				';
			}

			echo '</ul>';

		}
	}
	function generate_menu_promos($parent){
		$db = new Database();
		$qchild = $db->query("
			SELECT tbl_promotions.*
			FROM tbl_promotions
			LEFT JOIN tess_controller
			ON tbl_promotions.type = tess_controller.id
			WHERE tbl_promotions.id = :parent
			AND tbl_promotions.online = 1
			AND tess_controller.has_slug = 1
		");
		$db->bind($qchild, ":parent", $parent);
		$db->execute($qchild);
		$countChild = $db->rowCount($qchild);
		if($countChild > 0){

			echo '<ul>';
			while($child = $db->fetch($qchild)){

				echo '
				<li>
					<a href="' . getUrlLang($child->id) . '">
						' . $child->{'title' . $l} . '
					</a>
				';
				$qchild2 = $db->query("
					SELECT * FROM tbl_promotions
					WHERE parent = :parent
				");
				$db->bind($qchild2, ":parent", $child->id);
				$countchild2 = $db->rowCount($qchild2);
				if($countchild2 > 0){
					generate_menu_promos($child->id);
				}
				echo'
				</li>
				';
			}

			echo '</ul>';

		}
	}
   //initialize the dictionary
   function dictionary_initialize($the_lang=""){
      global $l, $linv, $language, $Tess_db;

      $Tess_db = new Database();
      if(!$the_lang){

         $the_lang = $language;
      }
      $all_dictionary_entries = $Tess_db->query("SELECT dictionary_term.term_id,
      dictionary.text_value,
      dictionary_term.data_type
      FROM dictionary
      INNER JOIN dictionary_term
      ON dictionary_term.id = dictionary.term_id
      WHERE dictionary.language = :lang
      ");

      $Tess_db->bind($all_dictionary_entries, ":lang", $the_lang);
      $Tess_db->execute($all_dictionary_entries);
      $temp_array = array();
      while($current = $Tess_db->fetch($all_dictionary_entries, 'array')){
         switch($current['data_type']){
               case 'array':
                  $temp_array[$current['term_id']] = unserialize($current['text_value']);
               break;

               default: // String
                  $temp_array[$current['term_id']] = $current['text_value'];
               break;
            }
      }

      return $temp_array;
   }
	function generate_menu_contests($parent){

		$db = new Database();
		$qchild = $db->query("
			SELECT tbl_contests.*
			FROM tbl_contests
			LEFT JOIN tess_controller
			ON tbl_contests.type = tess_controller.id
			WHERE tbl_contests.id = :parent
			AND tbl_contests.online = 1
			AND tess_controller.has_slug = 1
		");
		$db->bind($qchild, ":parent", $parent);
		$db->execute($qchild);
		$countChild = $db->rowCount($qchild);
		if($countChild > 0){

			echo '<ul>';
			while($child = $db->fetch($qchild)){

				echo '
				<li>
					<a href="' . getUrlLang($child->id) . '">
						' . $child->{'title' . $l} . '
					</a>
				';
				$qchild2 = $db->query("
					SELECT * FROM tbl_contests
					WHERE parent = :parent
				");
				$db->bind($qchild2, ":parent", $child->id);
				$countchild2 = $db->rowCount($qchild2);
				if($countchild2 > 0){
					generate_menu_contests($child->id);
				}
				echo'
				</li>
				';
			}

			echo '</ul>';

		}

	}

	function getRealisations($limit=""){

		global $lang, $l;
		$db = new Database();
		if($limit !== ""){
			$queryRealisations = $db->query("
			   SELECT * FROM tbl_pages
			   WHERE parent = 28 AND online = 1
			   ORDER BY `order` DESC LIMIT :limit
		   ");
		   $db->bind($queryRealisations, ":limit", $limit);
		   $db->execute($queryRealisations);
		   $countedRealisations = $db->rowCount($queryRealisations);
		}else{
			$queryRealisations = $db->query("
			   SELECT * FROM tbl_pages
			   WHERE parent = 28 AND online = 1
			   ORDER BY `order` DESC
		   ");
		   $db->execute($queryRealisations);
		   $countedRealisations = $db->rowCount($queryRealisations);
	   }
		echo'
		<div class="delta grid_12 grid_1024_12" >
			<div class="section wrapper_realisations" >
		';
		$query_single_realisations_page = $db->query("
				SELECT * FROM tbl_pages WHERE id = 28
		");
		$realisations_page = $db->single($query_single_realisations_page);
		?>

		<?php
		/*<h2><?= ($limit !=="" ? $realisations_page->{'title'.$l}: "") ?> <div class="redbar" ></div></h2>
		echo'<div class="contain_particles">';
		for ($i = 1; $i <= 100; $i++){
		echo '
			<div class="circle-container">
				<div class="circle">
				</div>
			</div>
			';
		}
		echo '</div>';
		*/
		if($limit == ""){
			echo getFilterRealisations();
		}
		if($countedRealisations > 0 ){
			$ir = 0;
			echo '<div class="filter_realisations">';
			while($project = $db->fetch($queryRealisations)){

				$join_projects = $db->query("SELECT t.*, i.name FROM tbl_pages_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_pages = :id_product");
				$db->bind($join_projects,":id_product",$project->id);
				$db->execute($join_projects);
				while($joins = $db->fetch($join_projects)){
					if($joins->name != "")
						$project->{$joins->name} = $joins->value;
				}

				$labels = getLabel($project->labels);
				if($ir % 2 == 0){
					echo '
					<div class="alpha grid_6 grid_1024_12 '.($limit == "" ? 'fadeIn wow' : '').'" data-wow-duration="1.5s" data-wow-delay="0.5s">
						<div class="table">
							<div class="table-cell">

								<div class="imgr" data-project-id="'.$project->id.'" >
									<div class="box" style="background: url('.URLROOT.'image.php?path='.$project->image.'~l=800);">
										<div class="overlay-logo" >
											<div class="content-label">'.$labels.'</div>
											<div class="background_logo" style="background: '.$project->color.'; color: white; "></div>
											<div class="table">
												<div class="table_cell">
													<img class="lazyload" data-src="'.URLROOT.'image.php?path='.$project->logo.'~l=200" alt="logo '.$project->{'title'.$l}.'">
													<a  href="'.rtrim(URLROOT, "/").getUrlLang($project->id).'" class="blue-btn" >'.$lang['view_project'].'</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							   <!--div class="content-details fadeIn-bottom" style="border-bottom: 2px solid '.$project->color.';">
								  <h3 class="content-title" style="color:'.$project->color.'; text-shadow: 0px 0px 10px;">'.$project->{'title'.$l}.'</h3>
								  <p class="content-text">'.$project->{'suptitle'.$l}.'</p>
							   </div-->
							</div>
						</div>
					</div><div class="clear_1024"></div>';
				}
				else{
					echo '
					<div class="omega grid_6 grid_1024_12 '.($limit == "" ? 'fadeIn wow' : '').'" data-wow-duration="1.5s" data-wow-delay="0.5s">
						<div class="table">
							<div class="table-cell">

								<div class="imgr" data-project-id="'.$project->id.'" >
									<div class="box" style="background: url('.URLROOT.'image.php?path='.$project->image.'~l=800);">
										<div class="overlay-logo" >
											<div class="content-label">'.$labels.'</div>
											<div class="background_logo" style="background: '.$project->color.'; color: white; "></div>
											<div class="table">
												<div class="table_cell">
													<img class="lazyload" data-src="'.URLROOT.'image.php?path='.$project->logo.'~l=200" alt="logo '.$project->{'title'.$l}.'">
													<a  href="'.rtrim(URLROOT, "/").getUrlLang($project->id).'" class="blue-btn" >'.$lang['view_project'].'</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							   <!--div class="content-details fadeIn-bottom"  style="border-bottom: 2px solid '.$project->color.';">
								  <h3 class="content-title"  style="color:'.$project->color.'; text-shadow: 0px 0px 10px;">'.$project->{'title'.$l}.'</h3>
								  <p class="content-text">'.$project->{'suptitle'.$l}.'</p>
							   </div-->
							</div>
						</div>
					</div><div class="clear"></div>';
				}

				$ir++;
			}

			echo '</div>';
			if($limit !== ""){

				echo '
				<div class="buttons_projects">
					<a href="'.getUrlLang(64).'" class="blue-btn">
						'.$lang['all_realisations'].'
					</a>
				</div>';
			}
		}
		echo'
			</div>
		</div>
		<div class="clear"></div>
		';
	}

	function getLabel($labels=""){
		global $l, $lang;
		if(!is_null($labels)){

			$labels = explode("-", $labels);
			$str = "<div class='tags'>";
			$s = 0.1;
			foreach($labels as $label){

				if($label !== ""){

					$db = new Database();
					$getDetails = $db->query('SELECT * FROM tbl_pages WHERE id = :id');
					$db->bind($getDetails, ":id", $label);
					$db->execute($getDetails);
					if($db->rowCount($getDetails) > 0){
						$detail = $db->single($getDetails);
						$detailinputs = getPageInputs($label);
							$str .= '<div class="tag wow fadeInUp" data-wow-delay="'.$s.'s" data-wow-duration="0.5s" style="color: '.$detailinputs->color.'" >'.$detail->{'title'.$l}.'</div>';
					}
					$s += 0.2;
				}
			}
			$str .= '</div>';

			return $str;
		}
	}
	function getFilterRealisations(){
		global $lang, $l;

		$db =  new Database();
		echo '
			<div class="filter_container">
				<h2 class="text-center">'.$lang['filter_by_cat'].'</h2>

				<ul class="categories">
					<li class="active"> '.$lang['see_all'].'</li>
		';
		$getLabels = $db->query("SELECT * FROM tbl_pages WHERE parent = 28");
		$db->execute($getLabels);
		if($db->rowCount($getLabels) > 0){
			while($label = $db->fetch($getLabels)){
				$joinlabel = $db->query("SELECT t.*, i.name FROM tbl_pages_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_pages = :id_product");
				$db->bind($joinlabel,":id_product",$label->id);
				$db->execute($joinlabel);
				while($joins = $db->fetch($joinlabel)){
					if($joins->name != "")
						$label->{$joins->name} = $joins->value;
				}
				echo '<li data-idlabel="'.$label->id.'" style="color:'.$label->color.'">'.$label->{'title'.$l}.'</li>';
			}
		}
		echo '	</ul>
			</div>';
	}

	function getPageInputs($id, $object=""){
		global $l, $lang;
		$db = new Database();
		$detail = new stdClass();
		$join_service = $db->query("SELECT t.*, i.name FROM tbl_pages_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_pages = :id_pages");
		$db->bind($join_service,":id_pages",$id);
		$db->execute($join_service);
		while($joins = $db->fetch($join_service)){
			if($joins->name != "")
				$detail->{$joins->name} = $joins->value;
		}
		if($object){
			$detail = (object) array_merge((array) $detail, (array) $object);
		}
		return $detail;
	}
	function getContact($page="", $data=""){
		global $l, $lang;
		$form = new Form();
		echo '
		<div class="wrapper80">
			<div class="alpha grid_6 grid_1024_12">
				<div class="table-custom section section_preservices "
					data-parallax="true" data-stellar-background-ratio="0.5" style="width: 100%;  background-position-x: 0 !important;
					background-size: cover;
					background-position: center;
					background-attachment: fixed;
					background-repeat: no-repeat;">
					<div class="delta table-cell-custom grid_12 grid_1024_12" data-wow-duration="0.5s" data-wow-delay="3s">
					<div class="blackbox" >
							<div class="firstdiv">
								<div class="fadeIn wow" data-wow-duration="1s" data-wow-delay="0s">';

					echo '<p><span class="info"><div class="table"><div class="table-cell"><i class="fad fa-phone"></i></div><div class="table-cell"><a href="tel:' . getPhone(123) . '">450-824-3575</a></div></div></span></p>';


					echo '<p><span class="info"><div class="table"><div class="table-cell"><i class="fad fa-map"></i></div><div class="table-cell">3195 suite 103, Blv. de la pinière, Terrebonne, J6X 4P7</div></div></span></p>';

					echo '<p><span class="info"><div class="table"><div class="table-cell"><i class="fad fa-calendar"></i></div><div class="table-cell">Lundi à vendredi / 9h a 17h</div></div></span></p>';

					echo '			<div class="text-left">N’hésitez pas à communiquer avec nous pour toutes informations sur nos produits et services, pour effectuer une commande ou pour tous commentaires.
							</div>
							<div class="carte"><img src="'.URLROOT.'images/carte.jpg" /></div>
									</div>
								</div>
								<div class="div_wrap_about " style="    background-repeat: no-repeat !important;  background-size: cover;background-attachment: scroll;background-position: center;">
								</div>
							</div>

							<div class="clear"></div>
						</div>
					</div>

			</div>
			<div class="grid_6 grid_1024_12 omega">

				<div class="table-custom section section_preservices "
					data-parallax="true" data-stellar-background-ratio="0.5" style="width: 100%;  background-position-x: 0 !important;
					background-size: cover;
					background-position: center;
					background-attachment: fixed;
					background-repeat: no-repeat;">
					<div class="delta table-cell-custom grid_12 grid_1024_12" data-wow-duration="0.5s" data-wow-delay="3s">
					<div class="blackbox" >
							<div class="firstdiv">
								<div class="fadeIn wow" data-wow-duration="1s" data-wow-delay="0s">
									<h2 class="title">Envoyer nous un message</h2>
									<form class="form_contact" name="form_contact" method="post" >
										'.$form->input("email", "text", "Courriel", true, (isset($data['secure_post']) ? $data['secure_post']['email']['value'] : ""), (isset($data['secure_post']) ? $data['secure_post']['email']['message_error'] : ""), " " . (isset($data['secure_post']) ? $data['secure_post']['email']['message_error'] !== "" ? "is-invalid" : "is-valid" : "")).'
										'.$form->input("message", "textarea", "Votre message", true, (isset($data['secure_post']) ? $data['secure_post']['message']['value'] : ""), (isset($data['secure_post']) ? $data['secure_post']['message']['message_error'] : ""), " " . (isset($data['secure_post']) ? $data['secure_post']['message']['message_error'] !== "" ? "is-invalid" : "is-valid" : "")).'
										<input type="hidden" name="contact_form" value="1">
										<input type="submit" value="Envoyer" class="blue-btn">


									</form>
								</div>
							</div>
						</div>
						<div class="div_wrap_about " style="    background-repeat: no-repeat !important;  background-size: cover;background-attachment: scroll;background-position: center;">
						</div>
					</div>

					<div class="clear"></div>
				</div>

			</div>
			<div class="clear"></div>

		</div>';
	}
	
	function getSubmission(){
	   global $l, $lang;

	   $db = new Database();
	   $query_questions = $db->query("
		  SELECT * FROM tbl_pages WHERE
		  parent = 67 ORDER BY `order` ASC
	   ");
	   $db->execute($query_questions);
	   $count = $db->rowCount($query_questions);
	   if($count > 0){

		  echo'
		  <!-- multistep form -->
		  <form id="msform" method="post">

			 <div class="container_fieldset">
			 <!-- progressbar -->
			 <ul id="progressbar">
		  ';
		  for($inc=0;$inc<= $count;$inc++){
			 echo'
				<li '.($inc==0 ? 'class="active"' : '').'></li>
			';
		  }
		  echo'
			 </ul>
		  ';
		  $i = 1;
		  $qorder = 1;
		  while($questions = $db->fetch($query_questions)){

			 $query_answers = $db->query("
				SELECT * FROM tbl_pages WHERE parent = :parent
			 ");
			 $db->bind($query_answers, ":parent", $questions->id);
			 $db->execute($query_answers);

			 echo '
			 <!-- fieldsets -->
			 <fieldset>
				<h2 class="fs-title">'.$questions->{'title'.$l}.'</h2>
			   <div class="help-block"> <ul class="ul_checkout">


			 ';
			 if($db->rowCount($query_answers) > 0){
				 while($answer = $db->fetch($query_answers)){
					 echo'
					 <li>
					  <label for="answer'.$answer->order.'q'.$questions->order.'">'.$answer->{'title'.$l}.'</label>
					  <input type="radio" id="answer'.$answer->order.'q'.$questions->order.'" data-question="'.$questions->id.'" name="answer'.$answer->order.'q'.$questions->order.'|'.$questions->{'title'.$l}.'" value="'.$answer->{'title'.$l}.'" >
					 </li>';
				 }
			 }
			 echo '
			   </ul></div>
			 </fieldset>
			 ';
			 $i++;
			 $qorder++;
		  }
		  echo '
			 <fieldset>
				<h2 class="fs-title">'.$lang['more_informations'].'</h2>
				<div class="help-block">
				   <input type="text" id="answerq'.($qorder+1).'" name="answerq'.($qorder+1).'|'.$lang['contact_name'].'" placeholder="'.$lang['contact_name'].'" >
				   <div class="error"></div>
				   <input type="text" id="answerq'.($qorder+2).'" name="answerq'.($qorder+2).'|'.$lang['company_name'].'" placeholder="'.$lang['company_name'].'" >
				   <div class="error"></div>
				   <input type="text" id="answerq'.($qorder+3).'" name="answerq'.($qorder+3).'|'.$lang['email_address'].'" placeholder="'.$lang['email_address'].'" >
				   <div class="error"></div>
				   <input type="text" id="answerq'.($qorder+4).'" name="answerq'.($qorder+4).'|'.$lang['phone_number'].'" placeholder="'.$lang['phone_number'].'" >
				   <div class="error"></div>
				   <input type="hidden" name="is_quote" value="1" >
				   <span class="hide msg_thank_you" data-msg="'.$lang['thankyou_message'].'"></span>
				   <textarea  id="answerq'.($qorder+5).'" rows="15" name="answerq'.($qorder+5).'|'.$lang['describe_project'].'" placeholder="'.$lang['describe_project'].'"></textarea>
				   <div class="error"></div>
				</div>
			 </fieldset>
			 <br>
			 <div class="buttons">'.($i !== 1 ? '<input type="button" name="previous" class="previous action-button" value="'.$lang['previous'].'" />' :'').'
				<input type="button" name="next" class="next action-button" value="'.$lang['next'].'" />
				<input type="submit" name="submit" class="submit action-button" value="'.$lang['submit'].'" /></div>
			 </div>
			<div class="recap_container">
			<h2 class="fs-title">'.$lang['summary_submission'].'</h2>
			';
			if($db->rowCount($query_questions)){
				echo'
				<div class="help-block">
				';
				echo '<div></div>';
				while($questions_recap = $db->fetch($query_questions)){
					 echo '<div class="hide"><div class="left_questswers"><h3 class="recap_questions">'.$questions_recap->{'title'.$l}.'</h3><span class="recap_answers answer_'.$questions_recap->id.'">NA</span></div><div class="right_edit"><span class="edit_question" data-index="'.$questions_recap->order.'">'.$lang['edit'].'</span></div></div>';
				}
				echo'
				</div>
				';
			}
			echo'
			</div>
		  </form>
		  ';
	   }
	}
	function getServices($limit=""){
	   global $l, $lang;

	   $db = new Database();
		$query_services = $db->query("
			SELECT * FROM
			tbl_pages WHERE parent = ".__SERVICESID__."
			AND online = 1 ORDER BY `order` ASC ".($limit !== "" ? 'LIMIT '.$limit : '')."
		");
		$db->execute($query_services);
		$countedProducts = $db->rowCount($query_services);
		if($countedProducts > 0){
			$i = 0;
		
			while($service = $db->fetch($query_services)){
				$join_service = $db->query("SELECT t.*, i.name FROM tbl_pages_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_pages = :id_product");
				$db->bind($join_service,":id_product",$service->id);
				$db->execute($join_service);
				while($joins = $db->fetch($join_service)){
					if(isset($joins->name) && $joins->name != "")
						$service->{$joins->name} = $joins->value;
				}
				if($i == 0 || ($i % 3 == 0 && $i > 0)){
				   echo '<div class="row">';
					$side = 'alpha alpha_1024';
				} else if($i % 3 == 2 || $i == $countedProducts){
					$side = 'omega omega_1024';
				}else{
					$side = "";
				}
				echo '
				<div class=" clear_1024 '.$side.' '.($i == 0 ? "scroll_here" : "").' grid_4 grid_1024_12 "  >
					<a href="'.URLROOT.getUrlLang($service->id).'">
						<div class="box-services  fadeIn wow"   data-wow-duration="1.7s"  data-wow-delay="0.'.$i.'s">
							<div class="table table-text">
								<div class=" ">
									<div class="zoomimg">
										<img width="auto" height="500" style="object-fit:contain;" src="'.URLROOT.'uploads/'.str_replace('+','%20',urlencode(SITENAME)).'/'.$service->image.'~l=400" alt="Image"/>
										<span class="learn_more">En savoir plus</span>
									</div>	 
									<div><div class="text_services">'.$service->{'title'.$l}.'</div></div>
								 
									<span >'.Cut($service->{'texte'.$l},150).' ..
									</br></br></br>
									</span>  
								</div>
								 
							</div> 
						</div>
					</a>
				</div>
				';

				if(($i % 3 == 2 && $i > 0) || $i == $countedProducts){
					echo '</div><div class="clear"></div>';
				}
				$i++;
			}
	
		}
	}