<?php
	/*
	 * Uploading any files in corresponding folders
	 */
	class Uploading extends Core{
		public function __construct()
		{
		}
		
		public function upload($post, $files=[]){
			
			$db = new Database();
			$upload_dir = dirname(APPROOT)."/public/uploads/personalized/";
			$allowed_ext = APPEXT;
			
			foreach($files as $key => $val){
				if(isset($_POST['current_img']))
					$key = $_POST['current_img'];
				if ($files[$key]['size'] != "0" && $files[$key]['tmp_name'] != '') {
					try{
						
						
						$query_folder = date('Y-m-d');
						
						
						/* Create that folder if not exists */
						if(!is_dir($upload_dir.$query_folder ."/")) {
							mkdir($upload_dir.$query_folder ."/");
						}
						$path = $query_folder ."/";
						$value = $query_folder."-".time()."-".DragonProtectv1($files[$key]['name']);
						
						
						/* Get extension */
						$ext = strtolower(pathinfo($value, PATHINFO_EXTENSION));
						
						/* if extension allowed */
						if(in_array($ext, $allowed_ext)){
							if(move_uploaded_file($files[$key]['tmp_name'], $upload_dir.$path . $value)){
								
								
								return ltrim($path) . rtrim($value);
							}
						}
					}catch(PDOException $e){
						return false;
					}
					exit();
				}
				
			}
		}
	}
?>