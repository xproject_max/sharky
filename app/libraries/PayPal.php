<?php
	
	class PayPal {
		
		function GetItemTotalPrice($item){
			
			//(Item Price x Quantity = Total) Get total amount of product;
			return $item['ItemPrice'] * $item['ItemQty'];
		}
		
		function GetProductsTotalAmount($products){
			
			$ProductsTotalAmount=0;
			
			foreach($products as $p => $item){
				
				$ProductsTotalAmount = $ProductsTotalAmount + $this -> GetItemTotalPrice($item);
			}
			
			return $ProductsTotalAmount;
		}
		
		function GetGrandTotal($products, $charges){
			
			//Grand total including all tax, insurance, shipping cost and discount
			
			$GrandTotal = $this -> GetProductsTotalAmount($products);
			
			foreach($charges as $charge){
				
				$GrandTotal = $GrandTotal + $charge;
			}
			
			return $GrandTotal;
		}
		
		function SetExpressCheckout($products, $charges, $noshipping='1',$infoAccount,$post){
			
			//Parameters for SetExpressCheckout, which will be sent to PayPal 
			$padata  = 	'&METHOD=SetExpressCheckout';
			
			$padata .= 	'&RETURNURL='.urlencode(PPL_RETURN_URL);
			$padata .=	'&CANCELURL='.urlencode(PPL_CANCEL_URL);
			//$padata .=	'&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("Authorization");
			
			foreach($products as $p => $item){
				
				$padata .=	'&L_PAYMENTREQUEST_0_NAME'.$p.'='.urlencode($item['ItemName']);
				$padata .=	'&L_PAYMENTREQUEST_0_NUMBER'.$p.'='.urlencode($item['ItemNumber']);
				$padata .=	'&L_PAYMENTREQUEST_0_DESC'.$p.'='.urlencode($item['ItemDesc']);
				$padata .=	'&L_PAYMENTREQUEST_0_AMT'.$p.'='.urlencode($item['ItemPrice']);
				$padata .=	'&L_PAYMENTREQUEST_0_QTY'.$p.'='. urlencode($item['ItemQty']);
			}
			
			/* 
			
			//Override the buyer's shipping address stored on PayPal, The buyer cannot edit the overridden address.
			
			$padata .=	'&ADDROVERRIDE=1';
			$padata .=	'&PAYMENTREQUEST_0_SHIPTONAME=J Smith';
			$padata .=	'&PAYMENTREQUEST_0_SHIPTOSTREET=1 Main St';
			$padata .=	'&PAYMENTREQUEST_0_SHIPTOCITY=San Jose';
			$padata .=	'&PAYMENTREQUEST_0_SHIPTOSTATE=CA';
			$padata .=	'&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=US';
			$padata .=	'&PAYMENTREQUEST_0_SHIPTOZIP=__PRODUCTSID__5131';
			$padata .=	'&PAYMENTREQUEST_0_SHIPTOPHONENUM=408-__PRODUCTSID__67-4444';
			
			*/
			
			$padata .=	'&NOSHIPPING='.$noshipping; //set 1 to hide buyer's shipping address, in-case products that does not require shipping
			
			$padata .=	'&PAYMENTREQUEST_0_ITEMAMT='.urlencode($this -> GetProductsTotalAmount($products));
			
			$padata .=	'&PAYMENTREQUEST_0_TAXAMT='.urlencode($charges['TotalTaxAmount']);
			$padata .=	'&PAYMENTREQUEST_0_SHIPPINGAMT='.urlencode($charges['ShippinCost']);
			$padata .=	'&PAYMENTREQUEST_0_HANDLINGAMT='.urlencode($charges['HandalingCost']);
			$padata .=	'&PAYMENTREQUEST_0_SHIPDISCAMT='.urlencode($charges['ShippinDiscount']);
			$padata .=	'&PAYMENTREQUEST_0_INSURANCEAMT='.urlencode($charges['InsuranceCost']);
			$padata .=	'&PAYMENTREQUEST_0_AMT='.urlencode($this->GetGrandTotal($products, $charges));
			$padata .=	'&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode(PPL_CURRENCY_CODE);
			
			//paypal custom template
			
			$padata .=	'&LOCALECODE='.PPL_LANG; //PayPal pages to match the language on your website;
			$padata .=	'&LOGOIMG='.PPL_LOGO_IMG; //site logo
			$padata .=	'&CARTBORDERCOLOR=FFFFFF'; //border color of cart
			$padata .=	'&ALLOWNOTE=1';
			
			############# set session variable we need later for "DoExpressCheckoutPayment" #######
			
			$_SESSION['ppl_products'] =  $products;
			$_SESSION['ppl_charges'] 	=  $charges;
			
			//check if is not from existant order
			if(!isset($_SESSION['is_pay_order'])){
				//create cart
				$cart = new Cart();
				$cartid = $cart->create_cart_user($_SESSION['cart'],$infoAccount);
				
				$_SESSION['cart_id'] = $cartid;
			}
			$_SESSION['postedData'] = $post;
			
			$httpParsedResponseAr = $this->PPHttpPost('SetExpressCheckout', $padata);
			
			//Respond according to message we receive from Paypal
			if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])){
				
				$paypalmode = (PPL_MODE== 'sandbox') ? '.sandbox' : '';
				
				//Redirect user to PayPal store with Token received.
				
				$paypalurl ='https://www'.$paypalmode.'.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$httpParsedResponseAr["TOKEN"].'';
				
				header('Location: '.$paypalurl);
			}
			else{
				
				//Show error message
				
				echo '<div style="color:red"><b>Error : </b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]).'</div>';
				
				echo '<pre>';
				
				print_r($httpParsedResponseAr);
				
				echo '</pre>';
			}
		}
		
		
		function DoExpressCheckoutPayment(){
			global $_POST,$infoAccount;
			date_default_timezone_set('America/Montreal');
			if(!empty(_SESSION('ppl_products'))&&!empty(_SESSION('ppl_charges'))){
				
				$products=_SESSION('ppl_products');
				
				$charges=_SESSION('ppl_charges');
				
				$padata  = 	'&TOKEN='.urlencode(_GET('token'));
				$padata .= 	'&PAYERID='.urlencode(_GET('PayerID'));
				//$padata .= 	'&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("Authorization");
				
				//set item info here, otherwise we won't see product details later	
				
				foreach($products as $p => $item){
					$padata .=	'&L_PAYMENTREQUEST_0_NAME'.$p.'='.urlencode($item['ItemName']);
					$padata .=	'&L_PAYMENTREQUEST_0_NUMBER'.$p.'='.urlencode($item['ItemNumber']);
					$padata .=	'&L_PAYMENTREQUEST_0_DESC'.$p.'='.urlencode($item['ItemDesc']);
					$padata .=	'&L_PAYMENTREQUEST_0_AMT'.$p.'='.urlencode($item['ItemPrice']);
					$padata .=	'&L_PAYMENTREQUEST_0_QTY'.$p.'='. urlencode($item['ItemQty']);
					$idn = $item['ItemNumber'];
					$ItemDesc = $item['ItemDesc'];
					$_SESSION['idn'] = $idn;
					$_SESSION['ItemDesc'] = $ItemDesc;
				}
				
				$padata .= 	'&PAYMENTREQUEST_0_ITEMAMT='.urlencode($this -> GetProductsTotalAmount($products));
				$padata .= 	'&PAYMENTREQUEST_0_TAXAMT='.urlencode($charges['TotalTaxAmount']);
				$padata .= 	'&PAYMENTREQUEST_0_SHIPPINGAMT='.urlencode($charges['ShippinCost']);
				$padata .= 	'&PAYMENTREQUEST_0_HANDLINGAMT='.urlencode($charges['HandalingCost']);
				$padata .= 	'&PAYMENTREQUEST_0_SHIPDISCAMT='.urlencode($charges['ShippinDiscount']);
				$padata .= 	'&PAYMENTREQUEST_0_INSURANCEAMT='.urlencode($charges['InsuranceCost']);
				$padata .= 	'&PAYMENTREQUEST_0_AMT='.urlencode($this->GetGrandTotal($products, $charges));
				$padata .= 	'&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode(PPL_CURRENCY_CODE);
				$mdsp = serialize($padata);
				
				//We need to execute the "DoExpressCheckoutPayment" at this point to Receive payment from user.
				
				$httpParsedResponseAr = $this->PPHttpPost('DoExpressCheckoutPayment', $padata);
				
				// var_dump($httpParsedResponseAr);
				
				//Check if everything went ok..
				if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])){
					
					$db = new Database();
					$_SESSION['xd'] = $httpParsedResponseAr;
					
					$mdsp = serialize($padata);
					
					//check if is not from existant order
					if(!isset($_SESSION['is_pay_order'])){
						//update products quantity and put in foreign keys of orders
						$userid_query = $db->query("SELECT fk_users FROM tbl_cart WHERE id = :idcart");
						$db->bind($userid_query,":idcart", $_SESSION['cart_id']);
						$userid = $db->single($userid_query)->fk_users;
						
						
						//gather user info
						$query_user = $db->query("SELECT * FROM tbl_users WHERE id = :fkusers");
						$db->bind($query_user, ":fkusers", $userid);
						$infoAccount = $db->single($query_user);
						
						//create order
						$order = new Order();
						$order_id = $order->create_order($_SESSION['postedData'],$infoAccount);
						$_SESSION['orderID'] = $order_id;
						
						//Transaction pour soustraire le solde
						$transaction = new Transaction();
						$tid = $transaction->add_transaction($_SESSION['postedData'], $infoAccount, true);
						$transaction->add_transaction_details($tid, $order_id, $_SESSION['postedData'], $infoAccount, true);

						$_SESSION['idT'] = $tid;
						
						//update quantity inventory of each products
						$order->update_products($_SESSION['cart'],$_SESSION['orderID']);
						$order->add_delivery_date($_SESSION['orderID']);
					}
					
					$_SESSION['mdsp'] = $mdsp;
					$response = urldecode($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"]);
					
					//pay order
					$query_update_order = $db->query("
						UPDATE tbl_orders SET paid = '1', status = 'paid', amount_to_pay = 0, paypal_id = :pid,
						date_paid = CURRENT_TIMESTAMP						
						WHERE id = :orderid
					");
					$db->bind($query_update_order, ":pid", $response);
					$db->bind($query_update_order, ":orderid", $_SESSION['orderID']);
					$db->execute($query_update_order);

					$getOrder = $db->query("SELECT * FROM tbl_orders WHERE id = :orderid ");
					$db->bind($getOrder,":orderid", $_SESSION['orderID']);
					$order = $db->single($getOrder);
					
					//update user solde
					$update_solde = $db->query("UPDATE tbl_users SET solde = (solde + :solde) WHERE id = :id");
					$db->bind($update_solde, ":solde", $order->final_price);
					$db->bind($update_solde, ":id", $order->fk_users);
					$db->execute($update_solde);
					
					//check if is not from existant order
					if(!isset($_SESSION['is_pay_order'])){
						//pay cart 
						$query_update_cart = $db->query("
							UPDATE tbl_cart SET paid = '1', fk_orders = :orderid					
							WHERE id = :idcart
						");
						$db->bind($query_update_cart, ":orderid", $_SESSION['orderID']);
						$db->bind($query_update_cart, ":idcart", $_SESSION['cart_id']);
						$db->execute($query_update_cart);
						
						newOrderTemplate($order_id);
					}else{
						
						//get user info from orders
						$gatherAccountInfo = $db->query("
							SELECT u.* FROM tbl_users u LEFT JOIN tbl_orders o ON o.fk_users = u.id WHERE
							o.id = :id");
						$db->bind($gatherAccountInfo, ":id", $_SESSION['orderID']);
						$infoAccount = $db->single($gatherAccountInfo);
						
						//update payment method for paypal 
						$update_payment_method = $db->query("
							UPDATE tbl_orders SET payment_method = :payment_method WHERE id = :id
						");
						$db->bind($update_payment_method, ":payment_method", "paypal");
						$db->bind($update_payment_method, ":id", $_SESSION['orderID']);
						$db->execute($update_payment_method);
						
						//refresh order content 
						$getOrder = $db->query("SELECT * FROM tbl_orders WHERE id = :orderid");
						$db->bind($getOrder,":orderid", $_SESSION['orderID']);
						$order = $db->single($getOrder);
						
						//add transaction 
						$transaction1 = new Transaction();
						$tid = $transaction1->add_transaction($_SESSION['postedData'], $infoAccount, false);
						$_SESSION['orderID'] = $order_id;
						$_SESSION['idT'] = $tid;
						
					}


					//Update and create promotions part
					$promo = new Promotion();
					$promo->update_promotions($_SESSION['rebates'], $_SESSION['orderID'], $infoAccount);
					
					//transaction details
					$transaction =  new Transaction();
					$transaction->add_transaction_details_from_order($_SESSION['idT'],  $_SESSION['orderID'], $order, $infoAccount, false);
					
					/*
					//Sometimes Payment are kept pending even when transaction is complete. 
					//hence we need to notify user about it and ask him manually approve the transiction
					*/
					if('Completed' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"]){
						
						/******/
						
						/******/
						
					}
					elseif('Pending' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"]){
						
					}
					$this->GetTransactionDetails();
				}
				else{
					
					
					
					
					echo '<div style="color:red"><b>Error : </b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]).'</div>';
					
					echo '<pre>';
					
					print_r($httpParsedResponseAr);
					
					echo '</pre>';
					die();
				}
			}
			else{
				
				// Request Transaction Details
				
				
				$this->GetTransactionDetails();
			}
		}
		
		function GetTransactionDetails(){
			
			// we can retrive transection details using either GetTransactionDetails or GetExpressCheckoutDetails
			// GetTransactionDetails requires a Transaction ID, and GetExpressCheckoutDetails requires Token returned by SetExpressCheckOut
			
			
			$padata = 	'&TOKEN='.urlencode(_GET('token'));
			
			$httpParsedResponseAr = $this->PPHttpPost('GetExpressCheckoutDetails', $padata, PPL_API_USER, PPL_API_PASSWORD, PPL_API_SIGNATURE, PPL_MODE);
			
			if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])){
				
				
				header('Location: '.URLROOT.getUrlLang(__THANKYOUID__));
				
				
				$buyerName = $httpParsedResponseAr["FIRSTNAME"].' '.$httpParsedResponseAr["LASTNAME"];
				$buyerEmail = $httpParsedResponseAr["EMAIL"];
				
				
				//Output any connection error
				if ($mysqli->connect_error) {
					die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
				}
				
				
			}
			
			
			
			else  {
				
				echo '<div style="color:red"><b>GetTransactionDetails failed:</b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]).'</div>';
				
				echo '<pre>';
				
				print_r($httpParsedResponseAr);
				
				echo '</pre>';
				
			}
		}
		
		function PPHttpPost($methodName_, $nvpStr_) {
			
			// Set up your API credentials, PayPal end point, and API version.
			$API_UserName = urlencode(PPL_API_USER);
			$API_Password = urlencode(PPL_API_PASSWORD);
			$API_Signature = urlencode(PPL_API_SIGNATURE);
			
			$paypalmode = (PPL_MODE== 'sandbox') ? '.sandbox' : '';
			
			$API_Endpoint = "https://api-3t".$paypalmode.".paypal.com/nvp";
			$version = urlencode('204.0');
			
			// Set the curl parameters.
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
			curl_setopt($ch, CURLOPT_VERBOSE, 1);
			//curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
			
			// Turn off the server and peer verification (TrustManager Concept).
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			
			// Set the API operation, version, and API signature in the request.
			$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
			
			// Set the request as a POST FIELD for curl.
			curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
			
			// Get response from the server.
			$httpResponse = curl_exec($ch);
			
			if(!$httpResponse) {
				exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
			}
			
			// Extract the response details.
			$httpResponseAr = explode("&", $httpResponse);
			
			$httpParsedResponseAr = array();
			foreach ($httpResponseAr as $i => $value) {
				
				$tmpAr = explode("=", $value);
				
				if(sizeof($tmpAr) > 1) {
					
					$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
				}
			}
			
			if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
				
				exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
			}
			
			return $httpParsedResponseAr;
		}
	}
