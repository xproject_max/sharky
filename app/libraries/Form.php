<?php
	/*
	 * Form class for creating form faster and more efficient.
	*/
	class Form extends Core{

		public function __construct()
		{
		}

		public function input($name, $type, $label="", $required=false, $value="", $error="", $class="",$options="",$note_position="", $note="", $attributes=""){

			switch($type){

				//text
				case "text":
					//password
				case "password":
					//hidden
				case "hidden":
					//date
				case "date":
					//tel
				case "tel":
					//email
				case "email":
					//color
				case "color":
					return '
                <div class="form-group bmd-form-group">
                <label for="'.$name.'" class="bmd-label-floating '.($error !== "" ? 'error_label' : "").'">
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').' <input type="'.$type.'" placeholder="'.$label.'"  class="'.$class.' form-control" '.$attributes.' value="'.$value.'" name="'.$name.'" id="'.$name.'" />
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
				<span>'.$label. ($required ? '<sup>*</sup>' : '').'</span>
                </label> '.($error !== "" ? '</br><span class="error">'.$error.'</span>' : "").'
                </div>
                ';
					break;
				//Datepicker
				case "Datepicker":
					return '
                <div class="form-group bmd-form-group">
                <label for="'.$name.'" class="bmd-label-floating"><span>'.$label. ($required ? '<sup>*</sup>' : '').'</span>
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').' <input type="'.$type.'" id="datetimepicker" placeholder="'.$label. ($required ? '<sup>*</sup>' : '').'" class="datetimepicker '.$class.' form-control" '.$attributes.' value="'.$value.'" name="'.$name.'" id="'.$name.'" />
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                </label> '.($error !== "" ? '</br><span class="error">'.$error.'</span>' : "").'
                </div>
                ';
					break;


				//textarea
				case "textarea":
					return '
                <div class="form-group bmd-form-group textarea_group">
				<label for="'.$name.'" class="bmd-label-floating">
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').' <textarea placeholder="'.$label.'" class="form-control '.$class.'"  id="'.$name.'" '.$attributes.'  name="'.$name.'" rows="4">'.$value.'</textarea>
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
				<span>'.$label. ($required ? '<sup>*</sup>' : '').'</span>
                </label>
                  '.($error !== "" ? '</br><span class="error">'.$error.'</span>' : "").'
                </div>';
					break;


				//wysiwyg
				case "WYSIWYG":
					return '
                <div class="form-group bmd-form-group">
                <label for="'.$name.'" style="position: initial;" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                </label>
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').'  <textarea class="ckeditor '.$class.'" cols="80" id="'.$name.'" '.$attributes.'  name="'.$name.'" rows="30">'.$value.'</textarea>
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                </div>';
					break;


				//upload_img
				case "Upload Image":
					return '
                <div class="form-group bmd-form-group card-wizard">
                <label for="'.$name.'" style="position: initial;" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                </label>
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').'
                <div id="waitingImg_'.$name.'"></div>
                '.
						   ($value == "" ? '
                <div class="picture-container">
                <div class="picture">
                <img src="'.DragonHide(URLROOT.'image.php?path='.'default-avatar.png').'" class="picture-src The_wizard_preview_'.$name.'" title="">
                <input type="file"  id-page="'.$options.'" parent="The_wizard_preview_'.$name.'" class="The_wizard_images" '.$attributes.' name="'.$name.'">
                <input type="hidden" class="'.$name.'" value=""  />
                </div>
                </div>'
							   :
							   '
                <div class="picture-container">
                <div class="picture" >
                <div class="table-cell">
                  <img src="'.DragonHide(URLROOT.'image.php?path='.$value).'" class="picture-src The_wizard_preview_'.$name.'" title="">
                </div>
                <input type="file"  id-page="'.$options.'" class="The_wizard_images" parent="The_wizard_preview_'.$name.'" '.$attributes.' name="'.$name.'">
                <input type="hidden" class="'.$name.'" value=""  />
                </div>
                </div>
                ').'
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                </div>';
					break;


				//upload_other
				case "Upload Autre":
					return '
                <div class="form-group bmd-form-group">
                <label for="'.$name.'" style="position: initial;" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                </label>
                <div>
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').'
                '.($value == "" ? '<input class="btn btn-info" type="file" '.$attributes.' name="' .
								  $name . '" >
                <input type="hidden" name="*' . $name .'*">' : '<input class="btn btn-danger" type="submit" '.$attributes.' value="Supprimer - ' .
															   $name . '" name="supp"><a href="' .DragonHide(URLROOT.'image.php?path='. $value). '">' . $name .
															   '</a>').'
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                </div>
                </div>';
					break;


				//joint_pages
				case "Jointure page":

					$select ='
                <div class="dropdown bootstrap-select">
                <select class="selectpicker" name="'.html_entity_decode($name).'" data-size="7" data-style="btn btn-info '.$class.' " '.$attributes.' title="'.$label.'" tabindex="-__PRODUCTSID__8">
                <option disabled="" selected="">'.$label.'</option>
                '.$options.'
                </select>
                </div>';
					return $select;
					break;


				//radio
				case "Radio":

					$radios ='
                <div class="form-group bmd-form-group">
                <label for="'.$name.'" style="position: initial;" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                </label>
                <div>
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').''.$options.'
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                </div>
                </div>';

					return $radios;

					break;

				//Catégories
				case "Cat&eacute;gorie":
					//checkboxes
				case "Checkbox":
					$checkboxes = '
                <div class="form-group bmd-form-group">
                <label for="'.$name.'" style="position: initial;" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                </label>
                <div>
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').''.$options.'
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                </div>
                </div>';
					return  $checkboxes.'<input type="hidden" name="' .  html_entity_decode($name) .'[]" checked>';
					break;

				//Nice toggle
				case "toggle":
					$toggle = '
                <div class="form-group bmd-form-group">
                <label for="'.$name.'" style="position: initial;" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                </label>
                <div class="togglebutton">
                <label onclick="$(this).find(`input`).attr(`checked`);">
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').'<input name="'.$name.'" value="0" type="hidden" />
                <input name="'.$name.'" value="1" '.$attributes.'  type="checkbox" ><span class="toggle"></span>
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                </label>
                </div>
                '.($error !== "" ? '</br><span class="error">'.$error.'</span>' : "").'
                </div>';
					return  $toggle;
					break;

				//Catégories radios
				case "Cat&eacute;gorie radio":
					$categ_radios = '
                <div class="form-group bmd-form-group">
                <label for="'.$name.'" style="position: initial;" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                </label>
                <div>
                '.($note_position=="haut" ? $note.'<br/>' : '').'
                '.($note_position=="gauche" ? $note : '').''.$options.'
                '.($note_position=="droit" ? $note : '').'
                '.($note_position =="bas" ? '<br/>'.$note : '').'
                </div>
                </div>';
					return  $categ_radios;
					break;

				//select
				case "select":
					$select ='
					<div class="custom-select-wrapper">
					 <label for="'.$name.'" style="position: initial;" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
					'.($note_position=="titre" ? '<br/><br/>'.$note : '').'
					</label>
						<div class="custom-select">
							<div class="custom-select__trigger"><span>'.($note !== "" ? $note : "Choisissez"). '</span>
								<div class="arrow"></div>
							</div>
							<div class="custom-options"> 
							'.$options.' 
							</div>
						</div>
					</div> 

                ';
				/* <div class="dropdown bootstrap-select">
                 <label for="'.$name.'" class="bmd-label-floating">'.$label. ($required ? '<sup>*</sup>' : '').'
                 '.($note_position=="titre" ? '<br/><br/>'.$note : '').'
                 </label>
                 <select class="selectpicker" value="0"  name="'.$name.'" data-size="7" '.$attributes.' data-style="btn  '.$class.' " title="'.$label.'" tabindex="-__PRODUCTSID__8">
                 <option  value="0" disabled="disabled" selected="">'.$label.'</option>
                 '.$options.'
                 </select>
                 </div>*/
					return $select;
					break;
			}
		}
	}
