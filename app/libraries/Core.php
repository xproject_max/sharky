<?php
	/*
	 * App core class
	 * create url & loads class
	 */
	class Core extends Lang {
		protected $currentController = "Pages";
		protected $currentMethod = "index";
		public $url = "";
		public $parent_value = "";
		protected $params = [];
		
		public function __construct()
		{
			
			
			$this->currentController = (isset($_SESSION['user_id']) ? "Pages" : "Pages");
			$this->currentMethod = (isset($_SESSION['user_id']) ? "index" : "index");
			$lang = $this->lang;
			if(!isset($_GET['url'])){  
				require_once dirname(__DIR__)."/config/lang.php";
				// $_GET['url'] = strtolower($lang['menu_home']);
			}
			$url = $this->getUrl();
		   // if (! is_https()) {
			   // header("location: https://{$_SERVER['HTTP_HOST']}");
		   // }
			//GetRealPathMethod
			if(isset($_GET['url']) && ($_GET['url'] == "logout" || $_GET['url'] == "deconnexion")){
				session_destroy();
				header("location: ".URLROOT.getUrlLang(1));
			}
			if(isset($_GET['url']) && $_GET['url'] !== "pages/logout"){
				if((strpos($_GET['url'], "boutique") !== false || strpos($_GET['url'], "shop") !== false) &&
				   $_GET['url'] !== "boutique" && $_GET['url'] !== "shop"){
					$array_products = $this->getRealPathUrlProducts($url, $lang);
					if(isset($array_products[0])){
						$urls = explode("/",$array_products[0]);
						$url = $urls;
						$parent_value = $array_products[1];
					}else{
						$url = array('pages','error');
						$parent_value =__404ID__;
					}
				}else if((strpos($_GET['url'], "articles") !== false ||
						  strpos($_GET['url'], "blog") !== false||
						  strpos($_GET['url'], "blogue") !== false) &&
						 $_GET['url'] !== "articles" && $_GET['url'] !== "blog" && $_GET['url'] !== "notre-blogue" && $_GET['url'] !== "our-blog"){
					$array_blogs = $this->getRealPathUrlBlogs($url, $lang);
					if(isset($array_blogs[0])){
						$urls = explode("/",$array_blogs[0]);
						$url = $urls;
						$parent_value = $array_blogs[1];
					}else{
						$url = array('pages','error');
						$parent_value =__404ID__;
					}
				}else if((strpos($_GET['url'], "concours") !== false ||
						  strpos($_GET['url'], "contests") !== false) &&
						 $_GET['url'] !== "concours" && $_GET['url'] !== "contests"){
					$array_contests = $this->getRealPathUrlContests($url, $lang);
					if(isset($array_contests[0])){
						$urls = explode("/",$array_contests[0]);
						$url = $urls;
						$parent_value = $array_contests[1];
					}else{
						$url = array('pages','error');
						$parent_value =__404ID__;
					}
				}else if((strpos($_GET['url'], "promotions") !== false ||
						  strpos($_GET['url'], "promotions") !== false) &&
						 $_GET['url'] !== "promotions" && $_GET['url'] !== "promotions"){
					$array_promotions = $this->getRealPathUrlPromotions($url, $lang);
					if(isset($array_promotions[0])){
						$urls = explode("/",$array_promotions[0]);
						$url = $urls;
						$parent_value = $array_promotions[1];
					}else{
						$url = array('pages','error');
						$parent_value =__404ID__;
					}
					// var_dump($array_promotions);
				}else{
					$array_pages = $this->getRealPathUrl($url, $lang);
					
					if(isset($array_pages[0])){
						$urls = explode("/",$array_pages[0]);
						$url = $urls;
						$parent_value = $array_pages[1];
						
					}else{
						$url = array('pages','error');
						$parent_value =__404ID__;
					}
				}
			}
			if(!isset($url[0])){
				$url[0] = "";
			}
			// Look in controllers for first value
			if(file_exists(dirname(__DIR__)."/controllers/".ucwords($url[0]).".php")){
				$this->currentController = ucwords($url[0]);
				
			}
			
			//Require the controller
			if(!require_once dirname(__DIR__)."/controllers/".$this->currentController . ".php"){
				//rien
			}else{
				if(isset($_GET['url']) && $_GET['url'] !== ""){
					if($url[0] == ""){
						$this->currentController = "Pages";
						$this->currentMethod = "error";
					}
					else if(strpos($url[0],"products") !== false){
						$this->currentController = "Products";
						$this->currentMethod = "products";
					}
					else if(strpos($url[0],"blog") !== false
					|| strpos($url[0],"blogue") !== false){
						
						$this->currentController = "Blogs";
						$this->currentMethod = "blogs";
					}
					else if(strpos($url[0],"contests") !== false){
						
						$this->currentController = "Contests";
						$this->currentMethod = "contests";
					}
					else if(strpos($url[0],"promotions") !== false){
						
						$this->currentController = "Promotions";
						$this->currentMethod = "promotions";
					}else{
						$this->currentController = "Pages";
						$this->currentMethod = "error";
					}
				}
			}
			
			require_once dirname(__DIR__)."/controllers/".$this->currentController . ".php";
			//Instantiate controller class
			$this->currentController = new $this->currentController;
			
			
			//Check for second part of url
			if(isset($url[1])){
				if(method_exists($this->currentController, $url[1])){
					$this->currentMethod = $url[1];
					unset($url[1]);
				}else{
					$this->currentMethod = "error";
				}
			}
			
			// Get params
			$this->params = (isset($parent_value) ? [$parent_value] : ($url ? array_values($url) : []));
			// Call a callback with array of params
			call_user_func_array([$this->currentController, $this->currentMethod], $this->params);
			
		}
		
		//Pages
		public function getRealPathUrl($url){
			global $l, $lang, $current;
			$db = new Database();
			$l = "";
			if($this->getLang() == "en"){
				$l = "_en";
			}
			$url = (array_filter($url));
			$i=1;
			$message = array();
			
			foreach($url as $key => $slug){
				
				// on va chercher les infos actuels
				$query_info = $db->query("SELECT *, tbl_pages.id as id FROM tbl_pages LEFT JOIN
                        tbl_custom_urls tpu ON tpu.page_id = tbl_pages.id
                        LEFT JOIN tess_controller tc ON tc.id = tbl_pages.type
                        WHERE tpu.url_id".$l." = :url AND tc.has_slug = 1
						AND tbl_pages.online = 1
                        ORDER BY tpu.date_creation DESC LIMIT 1
                    ");
				$db->bind($query_info, ":url", $slug);
				if($db->rowCount($query_info) > 0){
					$info = $db->single($query_info);
					$add=true;
					$path=$info->module;
					$id=$info->id;
				}else{
					$add=true;
					$path="pages/404";
					$id=7;
				}
				// var_dump($url[$key]." ".$key);
				if(isset($info)){
					//on va chercher le parent
					$query_parents = $db->query("SELECT *, tbl_pages.id as id FROM tbl_pages LEFT JOIN
                        tbl_custom_urls tpu ON tpu.page_id = tbl_pages.id
                        LEFT JOIN tess_controller tc ON tc.id = tbl_pages.type
                        WHERE tbl_pages.id = :parent
                        ORDER BY tpu.date_creation DESC LIMIT 1 ");
					$db->bind($query_parents, ":parent", $info->parent);
					if($db->rowCount($query_parents) > 0){
						$parent = $db->single($query_parents);
					}
					if(isset($parent)){
						if(in_array($parent->{'url_id'.$l}, $url) && $parent->has_slug == 1){
							// $message[$i] = ' '.$i. " ". $key;
							$add = true;
							$id = $info->id;
							$path = $info->module;
						}else{
							// $message[$i] = ''.$i. " ". $key;
							$add=false;
						}
					}else{
						// $message[$i] = 'pas de parent'.$i. " ". $key;
					}
					
				}else{
					$add= false;
					// $message[$i] = 'pas de id info'.$i. " ". $key;
				}
				$i++;
			}
			// var_dump($message);
			if($add){
				if($this->notSameTypeInUrl($url, "pages")){
					
					return array($path, $id);
				}else{
					// $this->getRealPathUrlPromotions($url,$current++);
					return false;
				}
			}
		}
		
		// Produits
		public function getRealPathUrlProducts($url, $current=1){
			global $l, $lang, $current;
			$db = new Database();
			$l = "";
			if($this->getLang() == "en"){
				$l = "_en";
			}
			$url = (array_filter($url));
			$i=1;
			$message = array();
			
			foreach($url as $key => $slug){
				if(is_null($slug)){
					$add=true;
					$path="products/products";
					$id = 1;
					$message[$i] = 'boutique initial bref'.$i;
				}else{
					// on va chercher les infos actuels
					$query_info = $db->query("SELECT *, tbl_products.id as id FROM tbl_products LEFT JOIN
                        tbl_products_urls tpu ON tpu.product_id = tbl_products.id
                        LEFT JOIN tess_controller tc ON tc.id = tbl_products.type
                        WHERE tpu.url_id".$l." = :url AND tbl_products.online = 1
                        ORDER BY tpu.date_creation DESC LIMIT 1
                    ");
					$db->bind($query_info, ":url", $slug);
					if($db->rowCount($query_info) > 0){
						$info = $db->single($query_info);
					}else{
						$add=true;
						// return false;
					}
					// var_dump($url[$key]." ".$key);
					if(isset($info) ){
						$id = $info->id;
						$path = $info->module;
						//on va chercher le parent
						$query_parents = $db->query("SELECT *, tbl_products.id as id FROM tbl_products LEFT JOIN
                        tbl_products_urls tpu ON tpu.product_id = tbl_products.id
                        LEFT JOIN tess_controller tc ON tc.id = tbl_products.type
                        WHERE tbl_products.id = :parent
                        ORDER BY tpu.date_creation DESC LIMIT 1 ");
						$db->bind($query_parents, ":parent", $info->parent);
						if($db->rowCount($query_parents) > 0){
							$parent = $db->single($query_parents);
						}
						if(isset($parent)){
							if(in_array($parent->{'url_id'.$l}, $url) && $parent->has_slug == 1){
								// $message[$i] = ' '.$i. " ". $key;
								$add = true;
								$id = $info->id;
								$path = $info->module;
							}else{
								if($parent->parent == "0" && $parent->has_slug == 0){
									
									$add = true;
									$id = $info->id;
									$path = $info->module;
								}else{
									// $message[$i] = 'd'.$i. " ". $key;
									$add=false;
								}
							}
						}else{
							// $message[$i] = 'pas de parent'.$i. " ". $key;
						}
						
					}else{
						// $add= false;
						// $message[$i] = 'pas de id info'.$i. " ". $key;
					}
				}
				$i++;
			}
			// var_dump($message);
			if($add){
				if($this->notSameTypeInUrl($url, "products")){
					
					return array($path, $id);
				}else{
					// $this->getRealPathUrlPromotions($url,$current++);
					return false;
				}
			}
		}
		
		/*
		* FUNCTION FOR URLS BLOGS
		*/
		public function getRealPathUrlBlogs($url, $current=1){
			global $l, $lang, $current;
			$db = new Database();
			$l = "";
			if($this->getLang() == "en"){
				$l = "_en";
			}
			$url = (array_filter($url));
			$i=1;
			$message = array();
			
			foreach($url as $key => $slug){
				if($i == 1){
					$add=true;
					$path="blogs/blog";
					$id = 1;
					$message[$i] = 'boutique initial bref'.$i;
				}else{
					// on va chercher les infos actuels
					$query_info = $db->query("SELECT *, tbl_blogs.id as id FROM tbl_blogs LEFT JOIN
                        tbl_blogs_urls tpu ON tpu.blog_id = tbl_blogs.id
                        LEFT JOIN tess_controller tc ON tc.id = tbl_blogs.type
                        WHERE tpu.url_id".$l." = :url AND tc.has_slug = 1
                        ORDER BY tpu.date_creation DESC LIMIT 1
                    ");
					$db->bind($query_info, ":url", $slug);
					if($db->rowCount($query_info) > 0){
						$info = $db->single($query_info);
					}else{
						$add=false;
						return false;
					}
					// var_dump($url[$key]." ".$key);
					if(isset($info)){
						//on va chercher le parent
						$query_parents = $db->query("SELECT *, tbl_blogs.id as id FROM tbl_blogs LEFT JOIN
                        tbl_blogs_urls tpu ON tpu.blog_id = tbl_blogs.id
                        LEFT JOIN tess_controller tc ON tc.id = tbl_blogs.type
                        WHERE tbl_blogs.id = :parent
                        ORDER BY tpu.date_creation DESC LIMIT 1 ");
						$db->bind($query_parents, ":parent", $info->parent);
						if($db->rowCount($query_parents) > 0){
							$parent = $db->single($query_parents);
						}
						if(isset($parent)){
							if((in_array($parent->{'url_id'.$l}, $url) && $parent->has_slug == 1)){
								$message[$i] = ' '.$i. " ". $key;
								$add = true;
								$id = $info->id;
								$path = $info->module;
							}else{
								if($parent->parent == "0" && $parent->has_slug == 0){
									
									$add = true;
									$id = $info->id;
									$path = $info->module;
								}else{
									$message[$i] = 'd'.$i. " ". $key;
									$add=false;
								}
							}
						}else{
							$message[$i] = 'pas de parent'.$i. " ". $key;
						}
						
					}else{
						$add= false;
						$message[$i] = 'pas de id info'.$i. " ". $key;
					}
				}
				$i++;
			}
			// var_dump($message);
			if($add){
				if($this->notSameTypeInUrl($url, "blogs")){
					
					return array($path, $id);
				}else{
					// $this->getRealPathUrlPromotions($url,$current++);
					return false;
				}
			}
		}
		
		/*
		* FUNCTION FOR URLS CONTESTS
		*/
		public function getRealPathUrlContests($url, $current=1){
			global $l, $lang, $current;
			$db = new Database();
			$l = "";
			if($this->getLang() == "en"){
				$l = "_en";
			}
			$url = (array_filter($url));
			$i=1;
			$message = array();
			
			foreach($url as $key => $slug){
				if($i == 1){
					$add=true;
					$path="contests/contest";
					$id = 1;
					$message[$i] = 'boutique initial bref'.$i;
				}else{
					// on va chercher les infos actuels
					$query_info = $db->query("SELECT *, tbl_contests.id as id FROM tbl_contests LEFT JOIN
                        tbl_contests_urls tpu ON tpu.contest_id = tbl_contests.id
                        LEFT JOIN tess_controller tc ON tc.id = tbl_contests.type
                        WHERE tpu.url_id".$l." = :url AND tc.has_slug = 1
                        ORDER BY tpu.date_creation DESC LIMIT 1
                    ");
					$db->bind($query_info, ":url", $slug);
					if($db->rowCount($query_info) > 0){
						$info = $db->single($query_info);
					}else{
						$add=false;
						return false;
					}
					// var_dump($url[$key]." ".$key);
					if(isset($info)){
						//on va chercher le parent
						$query_parents = $db->query("SELECT *, tbl_contests.id as id FROM tbl_contests LEFT JOIN
                        tbl_contests_urls tpu ON tpu.contest_id = tbl_contests.id
                        LEFT JOIN tess_controller tc ON tc.id = tbl_contests.type
                        WHERE tbl_contests.id = :parent
                        ORDER BY tpu.date_creation DESC LIMIT 1 ");
						$db->bind($query_parents, ":parent", $info->parent);
						if($db->rowCount($query_parents) > 0){
							$parent = $db->single($query_parents);
						}
						if(isset($parent)){
							if((in_array($parent->{'url_id'.$l}, $url) && $parent->has_slug == 1)){
								$message[$i] = ' '.$i. " ". $key;
								$add = true;
								$id = $info->id;
								$path = $info->module;
							}else{
								if($parent->parent == "0" && $parent->has_slug == 0){
									
									$add = true;
									$id = $info->id;
									$path = $info->module;
								}else{
									$message[$i] = 'd'.$i. " ". $key;
									$add=false;
								}
							}
						}else{
							$message[$i] = 'pas de parent'.$i. " ". $key;
						}
						
					}else{
						$add= false;
						$message[$i] = 'pas de id info'.$i. " ". $key;
					}
				}
				$i++;
			}
			// var_dump($message);
			if($add){
				if($this->notSameTypeInUrl($url, "contests")){
					
					return array($path, $id);
				}else{
					// $this->getRealPathUrlPromotions($url,$current++);
					return false;
				}
			}
		}
		
		/*
		* FUNCTION FOR URLS Promotions
		*/
		public $current = 1;
		public function getRealPathUrlPromotions($url, $current=1){
			global $l, $lang, $current;
			$db = new Database();
			$l = "";
			if($this->getLang() == "en"){
				$l = "_en";
			}
			$url = (array_filter($url));
			$i=1;
			$message = array();
			
			foreach($url as $key => $slug){
				if($i == 1){
					$add=true;
					$path="promotions/promotion";
					$id = 1;
					$message[$i] = 'boutique initial bref'.$i;
				}else{
					// on va chercher les infos actuels
					$query_info = $db->query("SELECT *, tbl_promotions.id as id FROM tbl_promotions LEFT JOIN
                        tbl_promotions_urls tpu ON tpu.promotion_id = tbl_promotions.id
                        LEFT JOIN tess_controller tc ON tc.id = tbl_promotions.type
                        WHERE tpu.url_id".$l." = :url AND tc.has_slug = 1
                        ORDER BY tpu.date_creation DESC LIMIT 1
                    ");
					$db->bind($query_info, ":url", $slug);
					if($db->rowCount($query_info) > 0){
						$info = $db->single($query_info);
					}else{
						$add=false;
						return false;
					}
					// var_dump($url[$key]." ".$key);
					if(isset($info)){
						//on va chercher le parent
						$query_parents = $db->query("SELECT *, tbl_promotions.id as id FROM tbl_promotions LEFT JOIN
                        tbl_promotions_urls tpu ON tpu.promotion_id = tbl_promotions.id
                        LEFT JOIN tess_controller tc ON tc.id = tbl_promotions.type
                        WHERE tbl_promotions.id = :parent
                        ORDER BY tpu.date_creation DESC LIMIT 1 ");
						$db->bind($query_parents, ":parent", $info->parent);
						if($db->rowCount($query_parents) > 0){
							$parent = $db->single($query_parents);
						}
						if(isset($parent)){
							if((in_array($parent->{'url_id'.$l}, $url) && $parent->has_slug == 1)){
								$message[$i] = ' '.$i. " ". $key;
								$add = true;
								$id = $info->id;
								$path = $info->module;
							}else{
								if($parent->parent == "0" && $parent->has_slug == 0){
									
									$add = true;
									$id = $info->id;
									$path = $info->module;
								}else{
									$message[$i] = 'd'.$i. " ". $key;
									$add=false;
								}
							}
						}else{
							$message[$i] = 'pas de parent'.$i. " ". $key;
						}
						
					}else{
						$add= false;
						$message[$i] = 'pas de id info'.$i. " ". $key;
					}
				}
				$i++;
			}
			// var_dump($message);
			if($add){
				if($this->notSameTypeInUrl($url, "promotions")){
					
					return array($path, $id);
				}else{
					// $this->getRealPathUrlPromotions($url,$current++);
					return false;
				}
			}
		}
		public function getUrl(){
			$url = [];
			if(isset($_GET['url'])){
				$url = rtrim($_GET['url'], "/");
				$url = filter_var($url, FILTER_SANITIZE_URL);
				$url = explode("/", $url);
			}
			return $url;
		}
		public function setProductId($id){
			return $this->parent_value = $id;
		}
		public function getProductId(){
			
			return $this->parent_value;
		}
		
		public function is_dictionary_initialized(){
			if(empty($_LANG)){
				//need import
				return false;
			}
			
			return true;
		}
		
		public function middleIsAParent($theParentUrl, $pageid) {
			global $l, $lang;
			$db = new Database();
			// var_dump($theParentUrl, $pageid);
			$ar = $db->query('SELECT parent FROM tbl_promotions WHERE id = :id');
			$db->bind($ar, ":id", $pageid);
			$getParentId = current($db->single($ar, "array"));
			$requete = $db->query('SELECT url_id'.$l.' FROM tbl_promotions_urls WHERE promotion_id = :parentid ORDER BY `tbl_promotions_urls`.`date_creation` DESC LIMIT 1 ');
			$db->bind($requete, ":parentid", $getParentId);
			if($db->rowCount($requete) != 0){
				$getParentSlug = current($db->single($requete, "array"));
			}
			if(isset($getParentSlug)) {
				if($getParentId == 0) return false;
				else return $this->middleIsAParent($theParentUrl, $getParentId);
			} else {
				return true;
			}
		}
		
		public function notSameTypeInUrl($urls, $type="promotions"){
			global $l, $lang;
			$db = new Database();
			
			$array_types=[];
			$i=0;
			foreach($urls as $url){
				if($i >= 10){
					die("too much");
				}
				if($i !== 0){
					switch($type){
						case "promotions":
							$q = "SELECT t.* FROM
                            tbl_promotions t LEFT JOIN tbl_promotions_urls u ON u.promotion_id = t.id
                            WHERE u.url_id".$l." = :url";
							break;
						case "contests":
							$q = "SELECT t.* FROM
                            tbl_contests t LEFT JOIN tbl_contests_urls u ON u.contest_id = t.id
                            WHERE u.url_id".$l." = :url";
							break;
						case "products":
							$q = "SELECT t.* FROM
                            tbl_products t LEFT JOIN tbl_products_urls u ON u.product_id = t.id
                            WHERE u.url_id".$l." = :url";
							break;
						case "blogs":
							$q = "SELECT t.* FROM
                            tbl_blogs t LEFT JOIN tbl_blogs_urls u ON u.blog_id = t.id
                            WHERE u.url_id".$l." = :url";
							break;
						case "pages":
							$q = "SELECT t.* FROM
                            tbl_pages t LEFT JOIN tbl_custom_urls u ON u.page_id = t.id
                            WHERE u.url_id".$l." = :url";
							break;
					}
					$select_custom = $db->query($q);
					
					$db->bind($select_custom, ":url", $url);
					$type = $db->single($select_custom)->type;
					$array_types[] = $type;
				}
				$i++;
			}
			if(arrayContainsDuplicate($array_types, $type)){
				return false;
			}else{
				return true;
			}
		}
		
		
		
	}
