<?php
	class Category extends Database
	{
		
		protected $x ="";
		public $table_name;
		public function __construct($table_name="tbl_pages"){
			$this->table_name = $table_name;
		}
		
		function cat($cat_id, $space = 1, $x = '')
		{
			
			$db = new Database();
			$lang = new Lang();
			if($lang->lang == "en"){
				$l = "_en";
			}else{
				$l = "";
			}
			try{
				$sth = $db->query('SELECT * FROM '.$this->table_name.'  WHERE parent = :id');
				$db->bind($sth,':id', $cat_id);
				$sth = $db->resultSet($sth);
				
				foreach($sth as $select){
					if ($select->{'parent'} == "0") {
						$name = $select->{'title'. $l};
						$repeat = str_repeat('-', $space);
					} else {
						$name = "&nbsp;" . $select->{'title'. $l} . "&nbsp;";
						$repeat = str_repeat('-', $space);
					}
					
					$this->x = $this->x . $repeat . $name . '+' . $select->id . '*';
					$this->x = $this->cat($select->id, ($space + 1), $this->x);
				}
				
				return $this->x;
			}catch(PDOException $e){
				return false;
			}
		}
		
		function get_cat($cat_id)
		{
			$list = $this->cat($cat_id);
			$arr = explode('*', $list);
			unset($arr[(count($arr) - 1)]);
			return $arr;
		}
		
	}
?>