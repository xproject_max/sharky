<?php
	/*
	 * Base Controller
	 * This loads the models and views
	 */
	class Controller extends Core {
		public  function __construct(){
			$lang = $this->lang;
			$parent_value = $this->parent_value;
		}
		//Load model
		public function model($model){
			//Require model file
			require_once dirname(__DIR__)."/models/".$model.".php";
			
			// Instantiate model
			return new $model();
		}
		
		// Load view
		public function view($view, $data = []) {
			global $lang, $l, $language, $_LANG,$parent_value;
			
			// Check view file
			if(file_exists(dirname(__DIR__)."/views/".$view.".php")) {
				require_once dirname(__DIR__) . "/views/" . $view . ".php";
			}else{
				die("file doesnt exists");
			}
			
		}
	}
