<?php
	class Role extends Controller {
		private $db;
		public $lang;
		public $_LANG;
		public $attempts = 3;
		public $delay_attempts = 5;
		
		public function __construct()
		{
			$this->db = new Database();
			$this->lang = $this->getLang();
			
		}
		public function getPage($id){
			try{
				$getPage = $this->db->query("SELECT * FROM tbl_pages WHERE id = " .$id);
				return $this->db->resultSet($getPage);
			}catch(PDOException $e){
				return false;
			}
		}
		
		/* ---------------------------------------------TESS ROLES------------------------------------- */
		
		
		/*
		* Get Everything From Tess ROLES
		*/
		public function getrolestess($id){
			try{
				$query = $this->db->query("SELECT * FROM tess_members_roles WHERE id = " .$id);
				return $this->db->resultSet($query);
			}catch(PDOException $e){
				return false;
			}
		}
		
		/*
		* Update rights order
		*/
		public function update_rights($postData){
			$p = 0;
			foreach ($postData['id'] as $key => $value) {
				global $lang;
				if (isset($postData['right'][$p])){
					try{
						$query_update_orders = $this->db->query("UPDATE tess_members_roles SET `right` = :right WHERE id= :id");
						$this->db->bind($query_update_orders,":right", $postData['right'][$p]);
						$this->db->bind($query_update_orders,":id", $value);
						if($this->db->execute($query_update_orders)){
							$msg = $lang['edited_success'];
						}else{
							$msg =  $lang['edited_error'];
						}
					}catch(PDOException $e){
						return false;
					}
				}
				$p++;
			}
			return $msg;
		}
		
		
		
		//Search roles
		public function seekInrolesTess($id){
			try{
				$response = [];
				$query_get_roles = $this->db->query("SELECT * FROM tess_members_roles WHERE id = :id ");
				$this->db->bind($query_get_roles,":id", $id);
				$result_id = $this->db->resultSet($query_get_roles);
				if($result_id){
					$response['from_id'] = $result_id;
				}
				$query_seek = $this->db->query("SELECT * FROM tess_members_roles WHERE
                name LIKE CONCAT('%',:id,'%') ||
                right LIKE CONCAT('%',:id,'%') 
                ");
				$this->db->bind($query_seek,":id", $id);
				$result_text = $this->db->resultSet($query_seek);
				if($result_text){
					$response['from_text'] = $result_text;
				}
				return $response;
			}catch(PDOException $e){
				return false;
			}
		}
		
		//add or edit tess roles
		public function AddEditTessroles($postData,$add=false){
			global $lang;
			if($add == false)
				unset($add);
			$response = [];
			$response["text"] = "";
			$response["color"] = "";
			$updates = "";
			$adds = "";
			$insert_sql1 = "";
			$insert_sql2 = "";
			$insert_inputs1 = "";
			$insert_inputs2 = "";
			$update_inputs = "";
			$can_insert = true;
			$add_customs = false;
			$binded = [];
			
			if($postData['name'] == "" )
				$can_insert = false;
			try{
				//Build string update
				foreach($postData as $key => $value){
					if ($key != 'B1' and $key != 'query'  and $key != 'supp'
															  and $key != 'url_id' and $key != 'url_id_en' and $key != 'inputs_id'
																											   and $key != 'edit_roles' and $key != 'add_roles' and $key != 'id_roles' and $key != 'edit_ts'
					) {
						
						$key = str_replace('*', '', $key);
						
						$updates .= "`$key`= :$key, ";
						$insert_sql1 .="`$key`, ";
						$insert_sql2 .=":$key, ";
						$binded[$key] = ( isset($value) ? $value : 1);
						
					}
					
				}
				$updates = substr($updates, 0, -2);
				$insert_sql1 = substr($insert_sql1, 0, -2);
				$insert_sql2 = substr($insert_sql2, 0, -2);
				
				//updates
				if(!isset($add)){
					$update = $this->db->query("UPDATE tess_members_roles SET ".$updates." WHERE id = :id");
					foreach($binded as $key => $value){
						$this->db->bind($update,":$key", $value);
					}
					$this->db->bind($update,":id", $postData["id_roles"]);
					if($this->db->execute($update)){
						
						$response["message"] = $lang['edited_success'];
						$response["color"] = "success";
					}else{
						$response["message"] = $lang['edited_error'];
						$response["color"] = "danger";
					}
				}else{
					//ADD
					if($can_insert){
						$adds = $this->db->query("insert ignore into tess_members_roles ($insert_sql1) VALUES ($insert_sql2)");
						
						foreach($binded as $key => $value){
							$this->db->bind($adds,":$key", $value);
						}
						if($this->db->execute($adds)){
							
							$response["message"] = $lang['added_success'];
							$response["color"] = "success";
							
						}else{
							$response["message"] = $lang['added_error'];
							$response["color"] = "danger";
						}
					}else{
						$response["message"] = $lang['added_error'];
						$response["color"] = "danger";
					}
				}
				return $response;
			}catch(PDOException $e){
				return false;
			}
		}
		
		
		//DELETE TESS ROLES
		public function DeleteTessRoles($id){
			global $lang;
			$response = [];
			$response["text"] = "";
			$response["color"] = "";
			try{
				$query_delete_roles = $this->db->query("DELETE FROM tess_members_roles WHERE id = :id");
				$this->db->bind($query_delete_roles,":id", $id);
				if($this->db->execute($query_delete_roles)){
					
					$response["message"] = $lang['deleted_success'];
					$response["color"] = "success";
					
				}else{
					
					$response["message"] = $lang['deleted_error'];
					$response["color"] = "danger";
				}
				return $response;
			}catch(PDOException $e){
				return false;
			}
		}
		
		/* ---------------------------WEBSITE ROLES------------------------------------- */
		
		
		/*
		*   Get Everything From Website Roles table
		*/
		public function getWebsiteRoles($id){
			try{
				$query = $this->db->query("SELECT * FROM tbl_roles WHERE id = " .$id);
				return $this->db->resultSet($query);
			}catch(PDOException $e){
				return false;
			}
		}
		
		
		/*
		* Update rights WEBSITE order
		*/
		public function update_website_rights($postData){
			$p = 0;
			foreach ($postData['id'] as $key => $value) {
				global $lang;
				if (isset($postData['rights'][$p])){
					try{
						$query_update_orders = $this->db->query("UPDATE tbl_roles SET `rights` = :rights WHERE id= :id");
						$this->db->bind($query_update_orders,":rights", $postData['rights'][$p]);
						$this->db->bind($query_update_orders,":id", $value);
						if($this->db->execute($query_update_orders)){
							$msg = $lang['edited_success'];
						}else{
							$msg =  $lang['edited_error'];
						}
					}catch(PDOException $e){
						return false;
					}
				}
				$p++;
			}
			return $msg;
		}
		
		
		
		//Search WEBSITE roles
		public function seekInWebsiteRoles($id){
			try{
				$response = [];
				$query_get_roles = $this->db->query("SELECT * FROM tbl_roles WHERE id = :id ");
				$this->db->bind($query_get_roles,":id", $id);
				$result_id = $this->db->resultSet($query_get_roles);
				if($result_id){
					$response['from_id'] = $result_id;
				}
				$query_seek = $this->db->query("SELECT * FROM tbl_roles WHERE
                role LIKE CONCAT('%',:id,'%') ||
                rights LIKE CONCAT('%',:id,'%') 
                ");
				$this->db->bind($query_seek,":id", $id);
				$result_text = $this->db->resultSet($query_seek);
				if($result_text){
					$response['from_text'] = $result_text;
				}
				return $response;
			}catch(PDOException $e){
				return false;
			}
		}
		
		//add or edit WEBSITE roles
		public function AddEditWebsiteRoles($postData,$add=false){
			global $lang;
			if($add == false)
				unset($add);
			$response = [];
			$response["text"] = "";
			$response["color"] = "";
			$updates = "";
			$adds = "";
			$insert_sql1 = "";
			$insert_sql2 = "";
			$insert_inputs1 = "";
			$insert_inputs2 = "";
			$update_inputs = "";
			$can_insert = true;
			$add_customs = false;
			$binded = [];
			
			if($postData['role'] == "" )
				$can_insert = false;
			try{
				//Build string update
				foreach($postData as $key => $value){
					if ($key != 'B1' and $key != 'query'  and $key != 'supp'
															  and $key != 'url_id' and $key != 'url_id_en' and $key != 'inputs_id'
																											   and $key != 'edit_website_roles' and $key != 'add_website_roles' and $key != 'id_roles' and $key != 'edit_ts'
					) {
						
						$key = str_replace('*', '', $key);
						
						$updates .= "`$key`= :$key, ";
						$insert_sql1 .="`$key`, ";
						$insert_sql2 .=":$key, ";
						$binded[$key] = ( isset($value) ? $value : 1);
						
					}
					
				}
				$updates = substr($updates, 0, -2);
				$insert_sql1 = substr($insert_sql1, 0, -2);
				$insert_sql2 = substr($insert_sql2, 0, -2);
				
				//updates
				if(!isset($add)){
					$update = $this->db->query("UPDATE tbl_roles SET ".$updates." WHERE id = :id");
					foreach($binded as $key => $value){
						$this->db->bind($update,":$key", $value);
					}
					$this->db->bind($update,":id", $postData["id_roles"]);
					if($this->db->execute($update)){
						
						$response["message"] = $lang['edited_success'];
						$response["color"] = "success";
					}else{
						$response["message"] = $lang['edited_error'];
						$response["color"] = "danger";
					}
				}else{
					//ADD
					if($can_insert){
						$adds = $this->db->query("insert ignore into tbl_roles ($insert_sql1) VALUES ($insert_sql2)");
						
						foreach($binded as $key => $value){
							$this->db->bind($adds,":$key", $value);
						}
						if($this->db->execute($adds)){
							
							$response["message"] = $lang['added_success'];
							$response["color"] = "success";
							
						}else{
							$response["message"] = $lang['added_error'];
							$response["color"] = "danger";
						}
					}else{
						$response["message"] = $lang['added_error'];
						$response["color"] = "danger";
					}
				}
				return $response;
			}catch(PDOException $e){
				return false;
			}
		}
		
		
		//DELETE WEBSITE ROLES
		public function DeleteWebsiteRoles($id){
			global $lang;
			$response = [];
			$response["text"] = "";
			$response["color"] = "";
			try{
				$query_delete_roles = $this->db->query("DELETE FROM tbl_roles WHERE id = :id");
				$this->db->bind($query_delete_roles,":id", $id);
				if($this->db->execute($query_delete_roles)){
					
					$response["message"] = $lang['deleted_success'];
					$response["color"] = "success";
					
				}else{
					
					$response["message"] = $lang['deleted_error'];
					$response["color"] = "danger";
				}
				return $response;
			}catch(PDOException $e){
				return false;
			}
		}
		
	}
?>