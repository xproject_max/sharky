<?php
	class Blog {
		private $db;
		
		public function __construct()
		{
			$this->db = new Database();
		}
		public function getPage($id){
			// var_dump($id);
			$getPage = $this->db->query("SELECT * FROM tbl_blogs WHERE id = ".$id."");
			$all = $this->db->single($getPage);
			$allc = $this->getAllCustoms($all->id);
			if($this->db->rowCount($allc) > 0){
				while($cust = $this->db->fetch($allc)){
					if(isset($all->{$cust->name}) && $cust->name != "")
						$all->{$cust->name} = $cust->value;
				}
			}
			return $all;
			
			
		}
		public function getPagenormal($id){
			try{
				$query = $this->db->query("SELECT tbl_pages.*, i.type as input_type, i.value FROM tbl_pages LEFT JOIN tbl_pages_inputs i ON
                i.fk_pages = tbl_pages.id WHERE tbl_pages.id = " .$id);
				return $this->db->resultSet($query);
			}catch(PDOException $e){
				return false;
			}
		}
		public function is_maintenance(){
			$query = $this->db->query("
                SELECT * FROM tbl_pages WHERE id ='.__MAINTENANCEID__ .'and online = 1
            ");
			$this->db->execute($query);
			if($this->db->rowCount($query) > 0){
				return true;
			}else{
				return false;
			}
		}
		public function checkIfExistJoinInputs($key, $fkblogs){
			$exist = $this->db->query("SELECT * FROM tbl_blogs_inputs WHERE type = :key AND fk_blogs = :fkblogs");
			$this->db->bind($exist, ":key", $key);
			$this->db->bind($exist, ":fkblogs", $fkblogs);
			if($this->db->single($exist)){
				return true;
			}else{
				return false;
			}
		}
		public function checkIfUrlExist($blog_id){
			$exist = $this->db->query("SELECT * FROM tbl_blogs_urls WHERE blog_id = :blog_id");
			$this->db->bind($exist, ":blog_id", $blog_id);
			if($this->db->single($exist)){
				return true;
			}else{
				return false;
			}
		}
		public function checkIfModulesExist($inputid){
			$exist = $this->db->query("SELECT * FROM tess_controller WHERE id = :inputid");
			$this->db->bind($exist, ":inputid", $inputid);
			if($this->db->single($exist)){
				return true;
			}else{
				return false;
			}
		}
		public function getblogWebsite($id){
			try{
				$query = $this->db->query("SELECT * FROM tbl_blogs WHERE id = " .$id);
				return $this->db->resultSet($query);
			}catch(PDOException $e){
				return false;
			}
		}
		public function getAll(){
			try{
				$query_get_all = $this->db->query("SELECT tbl_blogs.*, tess_controller.add, tess_controller.mod, tess_controller.del, tess_controller.dtype FROM tbl_blogs LEFT JOIN tess_controller ON tbl_blogs.type = tess_controller.id WHERE parent = 0");
				return $this->db->resultSet($query_get_all);
			}catch(PDOException $e){
				return false;
			}
		}
		public function getAllCustoms($id){
			try{
				$query_get_all = $this->db->query("SELECT * ,i.name as name FROM tbl_blogs_inputs LEFT JOIN inputs i ON i.id = tbl_blogs_inputs.type WHERE fk_blogs = :id");
				$this->db->bind($query_get_all, ":id", $id);
				$this->db->execute($query_get_all);
				return $query_get_all;
			}catch(PDOException $e){
				return false;
			}
		}
		
		public function getChildList($id, $level){
			
			global $Tess_db, $linv, $Tess, $Functions, $install_page_order, $_SESSION, $l;
			
			require_once APPROOT.'/helpers/function.php';
			$lang = new Lang();
			$l = "";
			if($lang->lang == "en")
				$l="_en";
			
			try{
				$query = $this->db->query("SELECT * FROM tbl_blogs WHERE parent = :id ORDER BY `order` ASC");
				$this->db->bind($query,":id",$id);
				$child_bool = false;
				$childMenu ='';
				$key=0;
				$currents = $this->db->resultSet($query);
				$custom = array();
				$custom['prix'] = "";
				$custom['Quantité'] = "";
				$custom['Compagnies'] = "";
				foreach($currents as $current){
					$custom['prix'] = "";
					$custom['Quantité'] = "";
					$custom['Compagnies'] = "";
					$custom_inputs = $this->getAllCustoms($current->id);
					
					
					foreach($custom_inputs as $array){
						$get_type = $this->db->query("SELECT * FROM inputs WHERE id = :id");
						$this->db->bind($get_type, ":id", $array->type);
						$key = $this->db->single($get_type);
						
						if($key->title == "Compagnies"){
							$query_title_company = $this->db->query("SELECT cm.company_name FROM tbl_companies t JOIN tbl_companies_meta cm ON cm.id = t.fk_companies_meta WHERE t.id = :id");
							$this->db->bind($query_title_company, ":id", $array->value);
							$array->value = $this->db->single($query_title_company)->company_name;
						}
						
						$custom[$key->title] = $array->value;
						
					}
					$childMenu.='<tr level="'.($level+1).'" mid="'.$current->id.'" url="'.URLROOT.getUrl($current->id).'" url_linv="'.URLROOT.getUrl($current->id, $linv).'" class="firstchild  Level_'.($level+1).'">';
					
					$child_check = $this->db->query("SELECT * FROM tbl_blogs WHERE parent = :id ");
					$this->db->bind($child_check,":id", $current->id);
					$child_check = $this->db->single($child_check);
					if(!empty($child_check)){
						$child_bool = true;
					}
					
					$i = 0;
					$level_i ="";
					$level_i .= '<img border="0"  src="images/icon1.png"><img border="0"  src="images/icon4.png">';
					$childMenu.= '<td level="'.($level+1).'"   >';
					$childMenu.= '<input type="text" name="order[]" style="text-align:center;width:30px !important;background:black;color:white;" value="'.$current->order.'"><input type="hidden" name="id[]" style="width:15px !important;" value="'.$current->id.'"> ';
					
					$bold=true;
					
					
					$rights = $this->db->query('SELECT * FROM tess_controller WHERE id = :rights ');
					$this->db->bind($rights,':rights', $current->type);
					$rights = $this->db->resultSet($rights);
					if(isset($rights[0]))
						$rights = $rights[0];
					
					$childMenu.='</td>
                    <td>
                        <div style="    height: 40px;
                        width: 40px;
                        display: table;
                        background: rgb(0, 0, 0);
                        overflow: hidden;
                        border-radius: 50%;
                        /* border: 1px solid rgba(0,0,0,0.2); */
                        margin: 0 auto;">
                            <div style="display:table-cell;vertical-align:middle">
                                <img src="'.DragonHide(URLROOT.'image.php?path='.($current->image ? $current->image : "default-avatar.png")).'" width="40px"  />
                            </div>
                        </div> 
                    </td>
                    <td>';
					
					if ($_SESSION['status'] <= $rights->mod or $_SESSION['status']=="1")
						$childMenu.= '<div class="subcat">'.file_get_contents('../images/sub.svg').'</div><a style="line-height: 1.3" data-toggle="tooltip" data-placement="top"  data-original-title="ID: ' . $current->id . ' & Parent: ' . $current->parent . '"  data-description="'.$rights->desc.'" href="index.php?cp=page&mod=addmod&id=' . $current->id . '&parent='.$current->parent.'">';
					$childMenu.= $bold ? "<b style='font-weight:800;'>" : "";
					if($current->{'title'.$l} != ''){
						$childMenu .= $current->{'title'.$l};
					}
					else{
						$childMenu .= '(Sans titre)';
					}
					$childMenu.= $bold ? "</b>" : "";
					if ($_SESSION['status'] <= $rights->mod or $_SESSION['status']=="1")
						$childMenu.= '</a>';
					
					$childMenu.='</td>';
					$childMenu.='</td>';
					//Déroulement
					$childMenu.='<td style="width:76px">';
					
					if($child_bool){
						if(isset($_SESSION['pages-actives']) && $_SESSION['pages-actives'] == "actif"){
							$childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler-blog page active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fa fa-arrow-alt-square-right arrows_menu"></i> </a>';
							$look_for_child = true;
						} else if(isset($_SESSION['pages-actives']) && $_SESSION['pages-actives'] == "inactif") {
							$childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler-blog page" style="cursor:pointer;" data-tooltip="Ouvrir la catégorie"><button type="button" rel="tooltip" class="btn btn-warning" data-original-title="" title="">
                                            <i class="fa fa-arrow-alt-square-down arrows_menu"></i>
                                        </button></a>';
							$look_for_child = false;
						} else if(3 <= $level) {
							$childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler-blog page" style="cursor:pointer;" data-tooltip="Ouvrir la catégorie">
                                            <i class="fa fa-arrow-alt-square-right arrows_menu"></i> </a>';
							$look_for_child = false;
						} else if(3  > $level) {
							$childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler-blog page active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fa fa-arrow-alt-square-right arrows_menu"></i> </a>';
							$look_for_child = true;
						}else {
							$childMenu.= 'noob';
						}
					}
					$child_bool = false;
					$childMenu.= '</td> ';
					$childMenu.= '
								<td>'.date("Y-m-d h:i:s", $current->create_ts).'</td>
                                <td class="td-actions text-right">
                                    <div class="togglebutton">
                                        <label onclick="$(this).find(`input`).attr(`checked`);">
                                            <input name="online[]['.$current->id.']" value="0" type="hidden" />
                                            <input name="online[]['.$current->id.']" value="1" type="checkbox" '.($current->online == 1 ? 'checked=""' : '').'><span class="toggle"></span>
                                        </label>
                                    </div>
                                </td>
                                ';
					$childMenu.= '<td class="td-actions text-right"   >';
					$childMenu.= '<span class="td-actions text-right">
                                    ';
					if ($_SESSION['status'] <= $rights->add){
						if($rights->dtype != "") {
							$childMenu .=  '<a class="btn btn-info add_child" data-tooltip="Ajouter" dtype="'.$rights->dtype.'" parent="'.$current->id.'" href="index.php?cp=page&amp;mod=addmod&amp;ajout=1&amp;parent='.$current->id.'&amp;templates=""><i class="fas fa-plus-square"></i></a> ';
						}
					}
					if ($_SESSION['status'] <= $rights->mod or $_SESSION['status'] == "1") {
						$childMenu.= '<a class="btn btn-warning" data-toggle="tooltip" data-placement="top"  data-original-title="Type: ' . $current->type . '" title="Type: ' . $current->type . '" href="index.php?cp=page&mod=addmod&id=' . $current->id . '&parent='.$current->parent.'"><i class="fas fa-pen-square"></i><div class="ripple-container"></div></a> ';
					}
					if ($_SESSION['status'] <= $rights->del) {
						$childMenu.= '<a id="'.$current->id.'"  class="btn btn-danger delete-blog"data-tooltip="Supprimer" ><i class="fas fa-times-square"></i></a> ';
					}
					$childMenu.='
                                    </span>';
					
					$childMenu.='</td>'."\n";
					$childMenu.= ' ';
					if($child_bool&&$look_for_child)
						
						$childMenu.= '</td>'."\n";
					$childMenu.="</tr>";
					$key++;
				}
				$_SESSION['pages-actives'][$id] = "actif";
				echo $childMenu;
			}catch(PDOException $e){
				return false;
			}
			
		}
		
		public function getMenu(){
			global $lang, $l;
			$lang = new Lang();
			$l = "";
			if($lang->lang == "en")
				$l="_en";
			$menu_array = [];
			$custom = [];
			$result = $this->getAll();
			$i=0;
			foreach($result as $row){
				
				$custom_inputs = $this->getAllCustoms($row->id);
				foreach($custom_inputs as $array){
					$get_type = $this->db->query("SELECT * FROM inputs WHERE id = :id");
					$this->db->bind($get_type, ":id", $array->type);
					$key = $this->db->single($get_type);
					
					if($key->title == "Compagnies"){
						$query_title_company = $this->db->query("SELECT cm.company_name FROM tbl_companies t JOIN tbl_companies_meta cm ON cm.id = t.fk_companies_meta WHERE t.id = :id");
						$this->db->bind($query_title_company, ":id", $array->value);
						$array->value = $this->db->single($query_title_company)->company_name;
					}
					
					$custom += [$key->title => $array->value];
					
				}
				$menu_array[$row->id] = array('name' => $row->{'title'.$l},'parent' => $row->parent,'mid' => $row->id,'order' => $row->order, 'add' => $row->{'add'}, 'del' => $row->{'del'}, 'mod' => $row->{'mod'}, 'type' => $row->type, 'dtype' => $row->dtype, 'online' => $row->online, 'image' => $row->image, 'date_created' => $row->create_ts);
				$menu_array[$row->id] += $custom;
				$i++;
			}
			return $menu_array;
		}
		
		public function update_orders($postData){
			$p = 0;
			foreach ($postData['id'] as $key => $value) {
				global $lang;
				if (isset($postData['order'][$p])){
					try{
						$query_update_orders = $this->db->query("UPDATE tbl_blogs SET `order` = :orders WHERE id= :id");
						$this->db->bind($query_update_orders,":orders", $postData['order'][$p]);
						$this->db->bind($query_update_orders,":id", $value);
						if($this->db->execute($query_update_orders)){
							$msg = $lang['edited_success'];
						}else{
							$msg =  $lang['edited_error'];
						}
					}catch(PDOException $e){
						return false;
					}
				}
				$p++;
			}
			return $msg;
		}
		
		public function update_onlines($postData){
			global $lang;
			foreach ($postData['online'] as $key => $value) {
				foreach($value as $key => $val){
					try{
						$query_update_online = $this->db->query("UPDATE tbl_blogs SET online= :onlines WHERE id = :id");
						$this->db->bind($query_update_online,":onlines", $val);
						$this->db->bind($query_update_online,":id", $key);
						if($this->db->execute($query_update_online)){
							$msg = $lang['edited_success'];
						}else{
							$msg =  $lang['edited_error'];
						}
					}catch(PDOException $e){
						return false;
					}
				}
			}
			return $msg;
		}
		//Search blogs
		public function seekInblogWebsite($id){
			try{
				$response = [];
				$query_get_blog = $this->db->query("SELECT * FROM tbl_blogs WHERE id = :id ");
				$this->db->bind($query_get_blog,":id", $id);
				$result_id = $this->db->resultSet($query_get_blog);
				if($result_id){
					$response['from_id'] = $result_id;
				}
				$query_seek = $this->db->query("SELECT * FROM tbl_blogs WHERE
                    notes LIKE CONCAT('%',:id,'%') ||
                    sub_total LIKE CONCAT('%',:id,'%') ||
                    final_price LIKE CONCAT('%',:id,'%')
                    ");
				$this->db->bind($query_seek,":id", $id);
				$result_text = $this->db->resultSet($query_seek);
				if($result_text){
					$response['from_text'] = $result_text;
				}
				return $response;
			}catch(PDOException $e){
				return false;
			}
		}
		//DELETE blog
		public function DeleteblogWebsite($id){
			global $lang;
			$response = [];
			$response["text"] = "";
			$response["color"] = "";
			try{
				$query_delete_blog = $this->db->query("DELETE FROM tbl_blogs WHERE id = :id");
				$this->db->bind($query_delete_blog,":id", $id);
				if($this->db->execute($query_delete_blog)){
					$query_delete_urlblog = $this->db->query("DELETE FROM tbl_blogs_urls WHERE blog_id = :id");
					$this->db->bind($query_delete_urlblog,":id", $id);
					if($this->db->execute($query_delete_urlblog)){
						$response["message"] = $lang['deleted_success'];
						$response["color"] = "success";
					}else{
						$response["message"] = $lang['deleted_error'];
						$response["color"] = "danger";
					}
				}else{
					
					$response["message"] = $lang['deleted_error'];
					$response["color"] = "danger";
				}
				return $response;
			}catch(PDOException $e){
				return false;
			}
		}
		public function updateblogInputs($id){
			try{
				$select_ids = $this->db->query("SELECT * FROM tbl_blogs_inputs WHERE fk_blogs = :id");
				$this->db->bind($select_ids, ":id", $id);
				$serialized = "";
				$this->db->execute($select_ids);
				$i = 0;
				while($inputs = $this->db->fetch($select_ids)){
					$serialized .= ($i != 0 ? '-' : '').$inputs->id;
					$i++;
				}
				$update = $this->db->query("UPDATE tbl_blogs SET custom_inputs = :data WHERE id = :id");
				$this->db->bind($update, ":data", $serialized);
				$this->db->bind($update, ":id", $id);
				return $this->db->execute($update);
			}catch(PDOException $e){
				return false;
			}
		}
		public function AddEditJoinInputs($key,$value,$blog_id, $id=false){ // If id then edit
			if($id){
				$edit = $this->db->query("UPDATE tbl_blogs_inputs SET `value` = :value WHERE `type` = :type AND `fk_blogs` = :fk_blogs");
				$this->db->bind($edit,":value", $value);
				$this->db->bind($edit,":type", $key);
				$this->db->bind($edit,":fk_blogs", $blog_id);
				if($this->db->execute($edit)){
					$this->updateblogInputs($blog_id);
					return true;
				}else{
					return false;
				}
			}else{
				$add = $this->db->query("INSERT INTO tbl_blogs_inputs (`type`, `value`, `fk_blogs`) VALUES(:type, :value, :fk_blogs )");
				$this->db->bind($add,":type", $key);
				$this->db->bind($add,":value", $value);
				$this->db->bind($add,":fk_blogs", $blog_id);
				if($this->db->execute($add)){
					$this->updateblogInputs($blog_id);
					return true;
				}else{
					// var_dump('test2'.$this->db->errorInfo($add)[2]);
					return false;
				}
			}
		}
		//add or edit blog
		public function AddEditblogWebsite($postData,$add=false){
			global $lang;
			if($add == false)
				unset($add);
			$response = [];
			$response["text"] = "";
			$response["color"] = "";
			
			
			$updates = "";
			$adds = "";
			$insert_sql1 = "";
			$insert_sql2 = "";
			
			/* custom inputs */
			$insert_inputs1 = "";
			$insert_inputs2 = "";
			$update_inputs = "";
			$can_insert = true;
			$add_customs = false;
			$binded = [];
			if($postData['title'] == "" || (!isset($postData['type'])))
				$can_insert = false;
			try{
				//Build string update
				foreach($postData as $key => $value){
					if ($key != 'B1' and $key != 'query'  and $key != 'supp'
															  and $key != 'url_id' and $key != 'url_id_en'
																					   and $key != 'edit_blog' and $key != 'add_blog' and $key != 'id_blog' and $key != 'edit_ts'
					) {
						
						$key = str_replace('*', '', $key);
						
						//Get inputs details
						$query_inputs = $this->db->query("SELECT * FROM inputs where `name` = :key ");
						$this->db->bind($query_inputs,":key", $key);
						$inputs = $this->db->single($query_inputs);
						if($inputs){
							if ($inputs->type == "Upload Image") {
							
							}
							else if ($inputs->type == "Upload Autre") {
								
								if ($_FILES[$key]['size'] != "0") {
									
									// Move the file
									$rand = rand("10000", "__PRODUCTSID__0000"). "-";
									$value = $rand . nomValide($_FILES[$key]['name']);
									if (move_uploaded_file($_FILES[$key]['tmp_name'], URLIMG ."files/". $value)) {
										//uploaded
									}
								}
							}
							else if ($inputs->type == "Checkbox" || $inputs->type == "Cat&eacute;gorie" ) {
								$str = '';
								foreach($_POST[$key] as $current){
									if($current != "")
										$str .= "-$current-";
								}
								$value = $str;
								
							}
							else {
								//Nothing else yet
							}
							
							if($key == 'dtype' && is_array($_POST['dtype'])) {
								$value="";
								foreach($_POST['dtype'] as $page){
									if($i != 0){$value.= '-';}
									$value .= $page;
									$i++;
								}
							}
						}
						
						if($key !== "type" AND $key !== "parent"
											   AND $key !== "order"
												   AND $key !== "seo_meta_title"
													   AND $key !== "seo_meta_title_en"
														   AND $key !== "seo_meta_description"
															   AND $key !== "seo_meta_description_en"
																   AND $key !== "seo_meta_title_tag"
																	   AND $key !== "seo_meta_title_tag_en"
																		   AND $key !== "title"
																			   AND $key !== "title_en"
																				   AND $key !== "texte"
																					   AND $key !== "texte_en"
																						   AND $key !== "image"
																							   AND $key !== "alt"
																								   AND $key !== "is_blog"
																									   AND $key !== "create_ts"
																										   AND $key !== "online"
																											   AND $key !== "custom_inputs"
						){
							if(isset($postData["id_blog"])){
								if($this->checkIfExistJoinInputs($inputs->id, $postData["id_blog"])){
									//edit
									$this->AddEditJoinInputs($inputs->id,$value,$postData["id_blog"],$inputs->id);
								}else{
									//add
									$this->AddEditJoinInputs($inputs->id,$value,$postData["id_blog"]);
								}
							}else{
								$add_customs = true;
							}
							
						}else{
							$updates .= "`$key`= :$key, ";
							$insert_sql1 .="`$key`, ";
							$insert_sql2 .=":$key, ";
							
							$binded[$key] = ( isset($value) ? $value : 1);
						}
					}
					
				}
				// var_dump($updates);
				//Delete last comma
				$updates = substr($updates, 0, -2);
				
				//updates
				if(!isset($add)){
					$update = $this->db->query("UPDATE tbl_blogs SET ".$updates.", edit_ts = :time WHERE id = :id");
					foreach($binded as $key => $value){
						$this->db->bind($update,":$key", $value);
					}
					$this->db->bind($update,":time", time());
					$this->db->bind($update,":id", $postData["id_blog"]);
					if($this->db->execute($update)){
						if(isset($postData['url_id'])){
							$upd = "";
							$add1 = "";
							$add2 = "";
							
							if(MULTI_LANG){
								foreach(LANGUAGES as $l){
									if($l != "fr"){
										$upd .= ", `url_id_".$l."` = :url_id_".$l."";
										$add1 .= ", `url_id_".$l."`";
										$add2 .= ", :url_id_".$l."";
									}
								}
							}
							if($this->checkIfUrlExist($postData['id_blog'])){
								$query_url = $this->db->query("UPDATE tbl_blogs_urls SET url_id = :url_id ". $upd." WHERE blog_id = :blog_id");
							}else{
								$query_url = $this->db->query("INSERT INTO tbl_blogs_urls (`blog_id`, `url_id` ". $add1.") VALUES ( :blog_id, :url_id ".$add2.")");
							}
							$this->db->bind($query_url,":url_id", $postData['url_id']);
							if(MULTI_LANG){
								foreach(LANGUAGES as $l){
									if($l != "fr"){
										$this->db->bind($query_url,":url_id_".$l, $postData['url_id_'.$l]);
									}
								}
							}
							$this->db->bind($query_url,":blog_id", $postData["id_blog"]);
							if($this->db->execute($query_url)){
							}else{
								$response["message"] = "URL couldn be added";
								$response["color"] = "danger";
							}
							
						}
						$response["message"] = $lang['edited_success'];
						$response["color"] = "success";
					}else{
						$response["message"] = $lang['edited_error'];
						$response["color"] = "danger";
					}
				}else{
					//ADD
					if($can_insert){
						// var_dump($postData);
						$order = $this->db->query("SELECT `order` FROM tbl_blogs WHERE parent= :parent order by `order` desc limit 1");
						$this->db->bind($order,":parent", (isset($postData['parent']) ? $postData['parent'] : 0));
						$order = $this->db->single($order);
						
						if(!isset($order->order) || is_null($order->order)){
							$lo = 1;
						}else{
							$lo = (1 + $order->order);
						}
						// var_dump("insert ignore into tbl_pages ($insert_sql1, `order`, `create_ts`) VALUES ($insert_sql3 '$lo', '".time()."')");
						$adds = $this->db->query("insert ignore into tbl_blogs ($insert_sql1 `order`, `create_ts`) VALUES ($insert_sql2 '$lo', '".time()."')");
						foreach($binded as $key => $value){
							$this->db->bind($adds,":$key", $value);
						}
						if($this->db->execute($adds)){
							if(isset($postData['url_id'])){
								$current_page_id = $this->db->query("SELECT LAST_INSERT_ID() as 'lastid' FROM tbl_blogs");
								$current_page_id = $this->db->single($current_page_id);
								if($current_page_id->lastid !== 0){
									$current_page_id = $current_page_id->lastid;
								}
								if($add_customs){
									$this->AddEditJoinInputs($inputs->id,$value,$current_page_id);
								}
								if(MULTI_LANG){
									$insert1 = "";
									$insert2 = "";
									$insert3 = array();
									foreach(LANGUAGES as $l){
										if($l != "fr"){
											$insert1 .= ", url_id_".$l;
											$insert2 .= ", :url_id_".$l;
										}
									}
									
									$insert_url = $this->db->query("INSERT INTO tbl_blogs_urls(blog_id, url_id ".$insert1.") VALUES( :blog_id, :url_id". $insert2.")");
									$this->db->bind($insert_url,":blog_id", $current_page_id);
									$this->db->bind($insert_url,":url_id", $postData['url_id']);
									foreach(LANGUAGES as $l){
										if($l != "fr"){
											$this->db->bind($insert_url,":url_id_".$l, $postData['url_id_'.$l]);
										}
									}
									if($this->db->execute($insert_url)){
										$response["message"] = $lang['added_success'];
										$response["color"] = "success";
									}else{
										$response["message"] = $lang['added_error'];
										$response["color"] = "danger";
									}
									
								}else{
									$insert_url = $this->db->query("INSERT INTO tbl_blogs_urls(blog_id, url_id) VALUES( :blog_id, :url_id)");
									$this->db->bind($insert_url,":blog_id", $current_page_id);
									$this->db->bind($insert_url,":url_id", $postData['url_id']);
									if($this->db->single($insert_url)){
										$response["message"] = $lang['added_success'];
										$response["color"] = "success";
									}else{
										$response["message"] = $lang['added_error'];
										$response["color"] = "danger";
									}
									
								}
							}else{
								$response["message"] = $lang['added_success'];
								$response["color"] = "success";
							}
						}else{
							$response["message"] = $lang['added_error'];
							$response["color"] = "danger";
						}
					}else{
						$response["message"] = $lang['added_error'];
						$response["color"] = "danger";
					}
				}
				return $response;
			}catch(PDOException $e){
				return false;
			}
		}
		
		public function filterByCateg($id){
			global $lang, $l;
			$html = "";
			$getCategArticles = $this->db->query("
                SELECT * FROM tbl_blogs
                WHERE parent = :id 
                AND type = ".__ANARTICLE__."
            ");
			$this->db->bind($getCategArticles, ":id", $id);
			$counted_articles = $this->db->rowCount($getCategArticles);
			if($counted_articles  > 0){
				$this->db->execute($getCategArticles);
				$i = 0;
				while($article = $this->db->fetch($getCategArticles)){
					$query_categ = $this->db->query("
                       SELECT *
                       FROM tbl_blogs 
                       WHERE id = :parent
                    ");
					$this->db->bind($query_categ, ":parent", $article->parent);
					$categ = $this->db->single($query_categ);
					$join_categ = $this->db->query("SELECT t.*, i.name FROM tbl_blogs_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_blogs = :id_page");
					$this->db->bind($join_categ,":id_page",$categ->id);
					$this->db->execute($join_categ);
					while($joins = $this->db->fetch($join_categ)){
						if(isset($categ->{$joins->name}) && $joins->name != "")
							$categ->{$joins->name} = $joins->value;
					}
					if($i == 0 || $counted_articles < 2){if ($i == 0 || ($i % 4 == 0 && $i > 0)) {
						$html .= '<div class="row">';
						$side = 'alpha';
					} else if ($i % 4 == __CONTACTID__|| $i == $counted_articles) {
						$side = 'omega';
					} else {
						$side = "";
					}
						$html .= '
         <div class="' . $side . ' grid_4 grid_1024_12 fadeInRight wow" data-wow-duration="1s" data-wow-delay="0.5s">
         <a href="' . getUrlLangBlogs($article->id) . '"  >
		       <div class="card card-raised card-background" style=" border-radius:0px;padding: 10px;overflow: hidden;background-image: url(' . URLROOT . 'image.php?path=' . $article->image . '~l=500)">
		           <div class="card-content" style="z-index:2">
		              <span class="date_articles" ><i class="fad fa-clock"></i> ' . date('Y-m-d', $article->create_ts) . '</span>
		              <div class="pcard" >
		                 <h4>' . $article->{'title' . $l} . '</h4>
		                 ' . cut($article->{'texte' . $l}, 200) . ' ...<br> 
		              </div>
		              <div class=""> 
		                 <div class="table-align">
		                    <div class="table_cell_align">
		                       <img src="' . DragonHide(URLROOT . 'image.php?path=' . $article->image) . '~l=500" alt="' . $article->{'title' . $l} . '">
		                    </div>
		                 </div>
		              </div>
		              <!--span class="categ_articles" style="
		              border: 2.5px solid black;background: ' . $categ->color . '"> ' . $categ->title . '</span--> 
		              <span class="filter_product" style="background-attachment:fixed !important;background: url(' . URLROOT . 'image.php?path=' . str_replace(" ", "%20", $article->image) . '~l=500) center;"></span>
		           </div>
		        </div>
            </a>
         </div>
               ';
						if (($i % 4 == __CONTACTID__&& $i > 0) || $i == $counted_articles) {
							$html .= '</div><div class="clear"></div>';
						}
					}else{
						if ($i == 0 || ($i % 4 == 0 && $i > 0)) {
							$html .= '<div class="row">';
							$side = 'alpha';
						} else if ($i % 4 == __CONTACTID__|| $i == $counted_articles) {
							$side = 'omega';
						} else {
							$side = "";
						}
						$html .= '
         <div class="' . $side . ' grid_4 grid_1024_12 fadeInRight wow" data-wow-duration="1s" data-wow-delay="0.5s">
         <a href="' . getUrlLangBlogs($article->id) . '"  >
		       <div class="card card-raised card-background" style=" border-radius:0px;padding: 10px;overflow: hidden;background-image: url(' . URLROOT . 'image.php?path=' . $article->image . '~l=500)">
		           <div class="card-content" style="z-index:2">
		              <span class="date_articles" ><i class="fad fa-clock"></i> ' . date('Y-m-d', $article->create_ts) . '</span>
		              <div class="pcard" >
		                 <h4>' . $article->{'title' . $l} . '</h4>
		                 ' . cut($article->{'texte' . $l}, 200) . ' ...<br> 
		              </div>
		              <div class=""> 
		                 <div class="table-align">
		                    <div class="table_cell_align">
		                       <img src="' . DragonHide(URLROOT . 'image.php?path=' . $article->image) . '~l=500" alt="' . $article->{'title' . $l} . '">
		                    </div>
		                 </div>
		              </div>
		              <!--span class="categ_articles" style="
		              border: 2.5px solid black;background: ' . $categ->color . '"> ' . $categ->title . '</span--> 
		              <span class="filter_product" style="background-attachment:fixed !important;background: url(' . URLROOT . 'image.php?path=' . str_replace(" ", "%20", $article->image) . '~l=500) center;"></span>
		           </div>
		        </div>
            </a>
         </div>
               ';
						if (($i % 4 == __CONTACTID__&& $i > 0) || $i == $counted_articles) {
							$html .= '</div><div class="clear"></div>';
						}
					}
					$i++;
				}
				$html .='
                     <div class="clear"></div>
                      
                  </div>
                  <div class="clear"></div>';
			}
			return $html;
		}
	}