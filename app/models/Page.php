<?php
	class Page {
		private $db;
		
		public function __construct()
		{
			$this->db = new Database();
		}
		
		public function getPage($id){
			try{
				$query = $this->db->query("SELECT tbl_pages.*, i.type as input_type, i.value FROM tbl_pages LEFT JOIN tbl_pages_inputs i ON
                i.fk_pages = tbl_pages.id WHERE tbl_pages.id = " .$id);
				return $this->db->resultSet($query);
			}catch(PDOException $e){
				return false;
			}
		}
		
		public function initalize_schedule($postData){
			$array = [];
			$array['email'] = [];
			$array['email']['value'] = $postData['email'];
			$array['email']['message_error'] = "";
			$array['orderid'] = [];
			$array['orderid']['value'] = $postData['orderid'];
			$array['orderid']['message_error'] = "";
			$error = "";
			if($postData['email'] == ""){
				
				$array['email']['message_error'] .= "Votre courriel est vide.";
				$error .= $array['email']['message_error'];
			}else if (!filter_var($postData['email'], FILTER_VALIDATE_EMAIL)) {
				$array['email']['message_error'] .= "Votre courriel est invalid.";
				$error .= $array['email']['message_error'];
			}
			
			if($postData['orderid'] == ""){
				
				$array['orderid']['message_error'] .= "Votre numéro de commande est vide.";
				$error .= $array['orderid']['message_error'];
			}else{
				$orders = new Order();
				if($orders->checkIfOrderExists($postData['email'], $postData['orderid'])){
				
				}else{
					$array['orderid']['message_error'] .= "Votre numéro de commande n'existe pas.";
					$error .= $array['orderid']['message_error'];
				}
			}
			if($error !== ""){
				return $array;
			}else{
				$infoOrders = $this->db->query("
                    SELECT * FROM tbl_orders WHERE id = :id
                ");
				$this->db->bind($infoOrders, ":id", $postData['orderid']);
				$order = $this->db->single($infoOrders);
				$_SESSION['active_plage'] = true;
				$_SESSION['session_order'] = $postData['orderid'];
				$_SESSION['session_email'] = $postData['email'];
				
				$account = new User();
				$infoAccount = $account->getUserWebsiteFromEmail($postData['email']);
				
				
			}
		}
		public function check_unpaid_orders($postData){
			$array = [];
			$array['email'] = [];
			$array['email']['value'] = $postData['email'];
			$array['email']['message_error'] = "";
			$array['orderid'] = [];
			$array['orderid']['value'] = $postData['orderid'];
			$array['orderid']['message_error'] = "";
			$error = "";
			if($postData['email'] == ""){
				
				$array['email']['message_error'] .= "Votre courriel est vide.";
				$error .= $array['email']['message_error'];
			}else if (!filter_var($postData['email'], FILTER_VALIDATE_EMAIL)) {
				$array['email']['message_error'] .= "Votre courriel est invalid.";
				$error .= $array['email']['message_error'];
			}
			
			if($postData['orderid'] == ""){
				
				$array['orderid']['message_error'] .= "Votre numéro de commande est vide.";
				$error .= $array['orderid']['message_error'];
			}else{
				$orders = new Order();
				if($orders->checkIfOrderExistsAndUnpaid($postData['email'], $postData['orderid'])){
				
				}else{
					$array['orderid']['message_error'] .= "Votre numéro de commande n'existe pas.";
					$error .= $array['orderid']['message_error'];
				}
			}
			if($error !== ""){
				return $array;
			}else{
				$infoOrders = $this->db->query("
                    SELECT * FROM tbl_orders WHERE id = :id
                ");
				$this->db->bind($infoOrders, ":id", $postData['orderid']);
				$order = $this->db->single($infoOrders);
				$_SESSION['active_plage'] = true;
				$_SESSION['session_order'] = $postData['orderid'];
				$_SESSION['session_email'] = $postData['email'];
				
				$account = new User();
				$infoAccount = $account->getUserWebsiteFromEmail($postData['email']);
				
				
			}
		}
		public function getPagenormal($id){
			try{
				$query = $this->db->query("SELECT tbl_pages.*, i.type as input_type, i.value FROM tbl_pages LEFT JOIN tbl_pages_inputs i ON
                i.fk_pages = tbl_pages.id WHERE tbl_pages.id = " .$id);
				return $this->db->resultSet($query);
			}catch(PDOException $e){
				return false;
			}
		}
		public function is_maintenance(){
			$query = $this->db->query("
                SELECT * FROM tbl_pages WHERE id =".__MAINTENANCEID__ ." and online = 1
            ");
			$this->db->execute($query);
			if($this->db->rowCount($query) > 0){
				return true;
			}else{
				return false;
			}
		} 
		public function validate_contact_form($postData){
            $array = []; 
            $array['first_name'] = [];
            $array['first_name']['value'] = $postData['first_name'];
            $array['first_name']['message_error'] = ""; 
            $array['last_name'] = [];
            $array['last_name']['value'] = $postData['last_name'];
            $array['last_name']['message_error'] = ""; 
            $array['email'] = [];
            $array['email']['value'] = $postData['email'];
            $array['email']['message_error'] = ""; 
            $array['phone'] = [];
            $array['phone']['value'] = $postData['phone'];
            $array['phone']['message_error'] = ""; 
            $array['message'] = [];
            $array['message']['value'] = $postData['message'];
            $array['message']['message_error'] = ""; 
            $error = "";
            $array_success['success'] = "";
            if($postData['email'] == ""){
                
                $array['email']['message_error'] .= "Votre courriel est vide."; 
                $error .= $array['email']['message_error'];
            }else if (!filter_var($postData['email'], FILTER_VALIDATE_EMAIL)) {
                $array['email']['message_error'] .= "Votre courriel est invalid."; 
                $error .= $array['email']['message_error'];
            }
            if($postData['phone'] == ""){
                
                $array['phone']['message_error'] .= "Votre téléphone est vide."; 
                $error .= $array['phone']['message_error'];
            }else if (!preg_match("/^[0-9 \-]+$/", $postData['phone'])) {
				$array['phone']['message_error'] .= "Invalide numéro de téléphone."; 
                $error .= $array['phone']['message_error']; 
			}
            
            if($postData['message'] == ""){
                
                $array['message']['message_error'] .= "Votre message est vide."; 
                $error .= $array['message']['message_error'];
            } 
            if($postData['first_name'] == ""){
                
                $array['first_name']['message_error'] .= "Votre prénom est vide."; 
                $error .= $array['first_name']['message_error'];
            } 
            if($postData['last_name'] == ""){
                
                $array['last_name']['message_error'] .= "Votre nom est vide."; 
                $error .= $array['last_name']['message_error'];
            } 

            if($error !== ""){
                return $array;
            }else{
                
                $array_success['success'] = "1";
                if(DragonMediaMail("Prise de contact", $postData['message'], $postData['email'], $postData['email'], "info@dragon-media.ca", "Dragon Média")){
                   return $array_success['success'];
                }else{
                    // return "Erreur de email";
                }
            }
            
        }

        public function validate_quote_form($postData){
			global $lang, $l;
			
            $array = []; 
            $return=[];
			$i=0;
			$error=false;
			$email = "";
			$message = "";
			foreach($postData as $postdat => $value){
				$id = explode("|", $postdat)[0];

				$return[$i][0] = $id;
				$return[$i][1] = "#00FF00";
				$return[$i][2] = "";


				if($value == ""){

					$return[$i][0] = $id;
					$return[$i][1] = "#FF0000";
					$return[$i][2] = $lang['error_empty'];
					$error=true;
				}
				if($id == "answerq14"){
					if (!filter_var($value, FILTER_VALIDATE_EMAIL) && $value !== "") {
						$return[$i][0] = $id;
						$return[$i][1] = "#FF0000";
						$return[$i][2] = $lang['erreur_courriel2'];
						$error=true;
					}else{
						$email = $value;
					}
				}
				if($id == "answerq15"){
					if(!preg_match("/^[0-9 \-]+$/", $value)) {
						$return[$i][0] = $id;
						$return[$i][1] = "#FF0000";
						$return[$i][2] = $lang['erreur_telephone2'];
						$error=true;
					}
				}
					$question = (isset(explode("|", $postdat)[1]) ? explode("|", $postdat)[1] : "une erreur est survenue");
				 
				$message .= "<p>".str_replace("_", " ", $question)." : <br> ".$value."</p>";

				$i++;
			}
			if(!$error){
				$serialized = serialize($postData);
				$query_insert = $this->db->query("
					INSERT INTO 
						tbl_quotes
					(`quotes`,`status`, `date`)
					VALUES 
					(:quotes, 'En cours', CURRENT_TIMESTAMP)
				");
				$this->db->bind($query_insert, ":quotes", $serialized);
				$this->db->execute($query_insert); 
				if(DragonMediaMail("Demande soumission", $message, $email, $email, CLIENT_EMAIL, "Dragon Média")){

				}else{ 
				}
				$return = ['success'];
			}
			
			return $return;

        }
		
		public function filterRealisationsByCateg($id=""){
			global $l, $lang;
			$db = new Database();
			//seek projects
			$getRealisations = $db->query("
				SELECT * FROM tbl_pages WHERE parent = 64 ORDER BY `order` DESC
			");
			$str = "";
			$db->execute($getRealisations);
			if($db->rowCount($getRealisations)>0){
				
				$ir = 0;
				while($project = $db->fetch($getRealisations)){
					$joinlabel = $db->query("SELECT t.*, i.name FROM tbl_pages_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_pages = :id_product");
					$db->bind($joinlabel,":id_product",$project->id);
					$db->execute($joinlabel);
					while($joins = $db->fetch($joinlabel)){
						if($joins->name != "")
							$project->{$joins->name} = $joins->value;
					}
					$labels = getLabel($project->labels);
					if($id == ""){
						if($ir % 2 == 0){
							$str.= '
							<div class="alpha grid_6 grid_1024_12 '.(isset($limit) && $limit == "" ? 'fadeIn wow' : '').'" data-wow-duration="1.5s" data-wow-delay="0.5s">
								<div class="table">
									<div class="table-cell">
										<div class="imgr" data-project-id="'.$project->id.'" >
											<div class="box" style="background: url('.URLROOT.'image.php?path='.$project->image.'~l=800);"> 
												<div class="overlay-logo" >
													<div class="content-label">'.$labels.'</div>
													<div class="background_logo" style="background: '.$project->color.'; color: white; "></div>
													<div class="table">
														<div class="table_cell">
															<img src="'.URLROOT.'image.php?path='.$project->logo.'~l=200" alt="logo '.$project->{'title'.$l}.'">
															<a  href="'.rtrim(URLROOT, "/").getUrlLang($project->id).'" class="blue-btn">'.$lang['view_project'].'</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									   <!--div class="content-details fadeIn-bottom" style="border-bottom: 2px solid '.$project->color.';">
										  <h3 class="content-title" style="color:'.$project->color.'; text-shadow: 0px 0px 10px;">'.$project->{'title'.$l}.'</h3>
										  <p class="content-text">'.$project->{'suptitle'.$l}.'</p>
									   </div-->
									</div>
								</div>
							</div><div class="clear_1024"></div>';
						}
						else{
							$str.= '
							<div class="omega grid_6 grid_1024_12 '.(isset($limit) && $limit == "" ? 'fadeIn wow' : '').'" data-wow-duration="1.5s" data-wow-delay="0.5s">
								<div class="table"">
									<div class="table-cell">
										<div class="imgr" data-project-id="'.$project->id.'" >
											<div class="box" style="background: url('.URLROOT.'image.php?path='.$project->image.'~l=800);"> 
												<div class="overlay-logo" >
													<div class="content-label">'.$labels.'</div>
													<div class="background_logo" style="background: '.$project->color.'; color: white; "></div>
													<div class="table">
														<div class="table_cell">
															<img src="'.URLROOT.'image.php?path='.$project->logo.'~l=200" alt="logo '.$project->{'title'.$l}.'">
															<a  href="'.rtrim(URLROOT, "/").getUrlLang($project->id).'" class="blue-btn" >'.$lang['view_project'].'</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									   <!--div class="content-details fadeIn-bottom"  style="border-bottom: 2px solid '.$project->color.';">
										  <h3 class="content-title"  style="color:'.$project->color.'; text-shadow: 0px 0px 10px;">'.$project->{'title'.$l}.'</h3>
										  <p class="content-text">'.$project->{'suptitle'.$l}.'</p>
									   </div-->
									</div>
								</div>
							</div><div class="clear"></div>';
						}
 
					}else{ 
						$arrayLabels = explode("-", $project->labels);
						foreach($arrayLabels as $label){
							if($label == $id){
								
								if($ir % 2 == 0){
									$str.= '
									<div class="alpha grid_6 grid_1024_12 '.(isset($limit) && $limit == "" ? 'fadeIn wow' : '').'" data-wow-duration="1.5s" data-wow-delay="0.5s">
										<div class="table">
											<div class="table-cell">
											<div class="imgr" data-project-id="'.$project->id.'" >
												<div class="box" style="background: url('.URLROOT.'image.php?path='.$project->image.'~l=800);"> 
													<div class="overlay-logo" >
														<div class="content-label">'.$labels.'</div>
														<div class="background_logo" style="background: '.$project->color.'; color: white; "></div>
														<div class="table">
															<div class="table_cell">
																<img src="'.URLROOT.'image.php?path='.$project->logo.'~l=200" alt="logo '.$project->{'title'.$l}.'">
																<a  href="'.rtrim(URLROOT, "/").getUrlLang($project->id).'" class="blue-btn" >'.$lang['view_project'].'</a>
															</div>
														</div>
													</div>
												</div>
											</div>
											   <!--div class="content-details fadeIn-bottom" style="border-bottom: 2px solid '.$project->color.';">
												  <h3 class="content-title" style="color:'.$project->color.'; text-shadow: 0px 0px 10px;">'.$project->{'title'.$l}.'</h3>
												  <p class="content-text">'.$project->{'suptitle'.$l}.'</p>
											   </div-->
											</div>
										</div>
									</div><div class="clear_1024"></div>';
								}
								else{
									$str.= '
									<div class="omega grid_6 grid_1024_12 '.(isset($limit) && $limit == "" ? 'fadeIn wow' : '').'" data-wow-duration="1.5s" data-wow-delay="0.5s">
										<div class="table"">
											<div class="table-cell">
												
												<div class="imgr" data-project-id="'.$project->id.'" >
													<div class="box" style="background: url('.URLROOT.'image.php?path='.$project->image.'~l=800);"> 
														<div class="overlay-logo" >
															<div class="content-label">'.$labels.'</div>
															<div class="background_logo" style="background: '.$project->color.'; color: white; "></div>
															<div class="table">
																<div class="table_cell">
																	<img src="'.URLROOT.'image.php?path='.$project->logo.'~l=200" alt="logo '.$project->{'title'.$l}.'">
																	<a  href="'.rtrim(URLROOT, "/").getUrlLang($project->id).'" class="blue-btn" >'.$lang['view_project'].'</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											   <!--div class="content-details fadeIn-bottom"  style="border-bottom: 2px solid '.$project->color.';">
												  <h3 class="content-title"  style="color:'.$project->color.'; text-shadow: 0px 0px 10px;">'.$project->{'title'.$l}.'</h3>
												  <p class="content-text">'.$project->{'suptitle'.$l}.'</p>
											   </div-->
											</div>
										</div>
									</div><div class="clear"></div>';
								}
							}
						}
					}
							$ir++;
				}
			}
			return $str;
		}
	}
