<?php
	
	class Tess_model extends Controller
	{
		public $lang;
		public $_LANG;
		public $attempts = 3;
		public $delay_attempts = 5;
		private $db;
		
		public function __construct()
		{
			$this->db = new Database();
			$this->lang = $this->getLang();
			
		}
		
		
		/* ---------------------------------------- Login part -------------------------------------- */
		
		public function getUser($id)
		{
			try {
				$query_get_user = $this->db->query("SELECT * FROM tbl_users WHERE id = " . $id);
				return $this->db->resultSet($query_get_user);
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function validate_users($data)
		{
			global $lang;
			//Declare variables
			$_LANG = [];
			$Message_error = [];
			$Message_error['username'] = "";
			$Message_error['password'] = "";
			$Message_error['login'] = "";
			$Message_error['login_max'] = "";
			$Message_error['login_form'] = "";
			$Message_error['global'] = "";
			//Require dictionary
			
			
			//Constraints and verifications
			
			if (!$this->usernameExist($data['username']) && $data['username'] !== "") {
				$Message_error['username'] .= $lang['error_username_not_exist'];
			}
			if ($this->userAttempts($data['username']) >= $this->attempts) {
				$Message_error['login_max'] .= $lang['error_attempts_max'];
			}
			
			if (strlen($data['username']) == 0) {
				$Message_error['username'] .= $lang['error_username_empty'];
			}
			
			if (strlen($data['password']) == 0) {
				$Message_error['password'] .= $lang['error_password_empty'];
			}
			
			if (
				!$this->userExist($data['username'], $this->encryptedPassword($data['password'])) && $this->usernameExist($data['username']) && $data['username'] !== ""
			) {
				$attempts_left = "";
				if ($this->userAttempts($data['username']) < $this->attempts) {
					$this->addAttempts($data['username'], $this->userAttempts($data['username']));
				}
				$attempts = $this->userAttempts($data['username']);
				$attempts_left = $this->attempts - $attempts;
				
				if ($attempts_left < 0)
					$attempts_left = 0;
				
				$Message_error['login'] .= $lang['error_attempts'] . $attempts_left . "</br>";
				$Message_error['password'] .= $lang['error_user_not_exist'];
			} else {
				if ($this->userAttempts($data['username']) >= $this->attempts) {
					// Do nothing
					$first_date = strtotime($this->userDelayAttempts($data['username']));
					$second_date = strtotime(date('Y-m-d H:i:s'));
					
					if ($this->userDelayAttempts($data['username']) !== "" && $first_date <= $second_date) {
						unset($Message_error["login_max"]);
						$this->resetAttempts($data['username']);
						$this->createUserSession($this->getUserInfo($data['username'], $this->encryptedPassword($data['password'])));
					}
				} else {
					$this->resetAttempts($data['username']);
					$this->createUserSession($this->getUserInfo($data['username'], $this->encryptedPassword($data['password'])));
				}
			}
			
			//Prepare new data array to return
			
			$data_return = [];
			foreach ($data as $key => $value) {
				if ($Message_error[$key] != "")
					$data_return[$key] = ["name" => $key, "message_error" => $Message_error[$key], "value" => $value];
				$Message_error['global'] .= $Message_error[$key];
			}
			
			//Global data login messages
			if ($Message_error["login"] != "")
				$data_return['login'] = ["name" => "login", "message_error" => $Message_error["login"], "value" => ""];
			if (isset($Message_error["login_max"]) && $Message_error["login_max"] != "")
				$data_return['login_max'] = ["name" => "login_max", "message_error" => $Message_error["login_max"], "value" => ""];
			
			if (empty($Message_error['global'])) {
				$data_return['success'] = $this->getUserInfo($data['username'], $this->encryptedPassword($data['password']));
				$data_return['login']['message_error'] = $lang['message_success'] . " " . $data_return['success']->pseudo . " </br>";
			}
			if (!empty($data_return['login_max'])) {
				unset($data_return);
				$data_return['login_max'] = ["name" => "login_max", "message_error" => "veuillez communiquer avec un de nos agents, vous n'avez plus de tentatives", "value" => ""];
			}
			//return data
			return $data_return;
		}
		
		public function usernameExist($user)
		{
			try {
				$query_username_exist = $this->db->query("SELECT * FROM tess_members WHERE (pseudo = :username OR email = :username)");
				$this->db->bind($query_username_exist, ":username", $user);
				if ($this->db->single($query_username_exist)) {
					return true;
				}
				return false;
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function userAttempts($user)
		{
			try {
				$query_attemps = $this->db->query("SELECT attempts FROM tess_members WHERE pseudo = :username");
				$this->db->bind($query_attemps, ":username", $user);
				if ($this->db->single($query_attemps)) {
					return $this->db->single($query_attemps)->attempts;
				}
				return false;
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function userExist($user, $pass)
		{
			$query_exist = $this->db->query("SELECT * FROM tess_members WHERE (pseudo = :username OR email = :username) AND pass = :password");
			$this->db->bind($query_exist, ":username", $user);
			$this->db->bind($query_exist, ":password", $pass);
			if ($this->db->single($query_exist)) {
				return true;
			}
			return false;
		}
		
		public function encryptedPassword($pass)
		{
			$static_salt = SALT_PASS;
			return hash('sha512', $pass . $static_salt);
		}
		
		public function addAttempts($user, $attempted)
		{
			/*
			 * This function will add an attempt to the user;
			 */
			try {
				$attempts_query = $this->db->query("UPDATE tess_members SET attempts = attempts + 1 WHERE pseudo = :username");
				$this->db->bind($attempts_query, ":username", $user);
				if ($this->db->execute($attempts_query)) {
					/*
					 * If current attempt equals max attempts,
					 * Start delay before next tries;
					 */
					if (($attempted + 1) == $this->attempts) {
						//Add delay;
						$this->addDelay($user);
					}
					return true;
				}
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function addDelay($user)
		{
			try {
				$delay_query = $this->db->query("UPDATE tess_members SET attempts_delay = DATE_ADD(NOW(), INTERVAL :minutes MINUTE); WHERE pseudo = :username");
				$this->db->bind($delay_query, ":minutes", $this->delay_attempts);
				$this->db->bind($delay_query, ":username", $user);
				if ($this->db->execute($delay_query)) {
					//Added time;
					
				}
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function userDelayAttempts($user)
		{
			try {
				$query_attemps_delay = $this->db->query("SELECT attempts_delay FROM tess_members WHERE pseudo = :username");
				$this->db->bind($query_attemps_delay, ":username", $user);
				if ($this->db->single($query_attemps_delay)) {
					return $this->db->single($query_attemps_delay)->attempts_delay;
				}
				return false;
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function resetAttempts($user)
		{
			/*
			 * This function will reset attempts to the user;
			 */
			try {
				$reset_attempts = $this->db->query("UPDATE tess_members SET attempts = 0, attempts_delay = null WHERE pseudo = :username");
				$this->db->bind($reset_attempts, ":username", $user);
				if ($this->db->execute($reset_attempts)) {
					//remove time;
					return true;
				}
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function createUserSession($data)
		{
			if (!empty($data)) {
				$_SESSION['user_id'] = $data->id;
				$_SESSION['user_email'] = $data->email;
				$_SESSION['user_name'] = $data->pseudo;
				$_SESSION['status'] = $data->status;
				$_SESSION['profile_image'] = $data->profile_image;
			}
		}
		
		public function getUserInfo($user, $pass)
		{
			$query_get_user_info = $this->db->query("SELECT * FROM tess_members WHERE (pseudo = :username OR email = :username) AND pass = :password");
			$this->db->bind($query_get_user_info, ":username", $user);
			$this->db->bind($query_get_user_info, ":password", $pass);
			if ($this->db->single($query_get_user_info)) {
				return $this->db->single($query_get_user_info);
			}
			return false;
		}
		
		public function gettessUserWebsite($id)
		{
			try {
				$query = $this->db->query("SELECT * FROM tess_members WHERE id = " . $id);
				return $this->db->resultSet($query);
			} catch (PDOException $e) {
				return false;
			}
		}
		
		
		/* ---------------------------- USER TESS --------------------------------- */
		
		public function getMenu($id = "")
		{
			global $lang, $l;
			$lang = new Lang();
			$l = "";
			if ($lang->lang == "en")
				$l = "_en";
			$menu_array = [];
			$custom = [];
			$result = $this->getAll($id);
			$i = 0;
			if ($id) {
				$menu_array[$i] = array(
					'id' => $result->id, 'profile_image' => $result->profile_image, 'pseudo' => $result->pseudo, 'pass' => $result->pass, 'email' => $result->email, 'status' => $result->status, 'date_created' => $result->date_created, 'permissions' => $result->status, 'online' => $result->online,
				);
			} else {
				foreach ($result as $row) {
					
					$menu_array[$i] = array(
						'id' => $row->id, 'profile_image' => $row->profile_image, 'pseudo' => $row->pseudo, 'pass' => $row->pass, 'email' => $row->email, 'status' => $row->status, 'date_created' => $row->date_created, 'permissions' => $row->status, 'online' => $row->online,
					);
					$i++;
				}
			}
			return $menu_array;
		}
		
		public function getAll($id = "")
		{
			try {
				if ($id !== "") {
					$query_get_all = $this->db->query("SELECT tess_members.*, r.name as status FROM tess_members LEFT JOIN tess_members_roles r ON r.id = tess_members.status WHERE tess_members.id = :id");
					$this->db->bind($query_get_all, ":id", $id);
					return $this->db->single($query_get_all);
				} else {
					$query_get_all = $this->db->query("SELECT tess_members.*, r.name as status FROM tess_members LEFT JOIN tess_members_roles r ON r.id = tess_members.status ORDER BY id ASC");
					
					return $this->db->resultSet($query_get_all);
				}
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function update_onlines($postData)
		{
			global $lang;
			foreach ($postData['online'] as $key => $value) {
				foreach ($value as $key => $val) {
					try {
						$query_update_online = $this->db->query("UPDATE tess_members SET online= :onlines WHERE id = :id");
						$this->db->bind($query_update_online, ":onlines", $val);
						$this->db->bind($query_update_online, ":id", $key);
						if ($this->db->execute($query_update_online)) {
							$msg = $lang['edited_success'];
						} else {
							$msg = $lang['edited_error'];
						}
					} catch (PDOException $e) {
						return false;
					}
				}
			}
			return $msg;
		}
		
		public function DeletetessUsersWebsite($id)
		{
			global $lang;
			$response = [];
			$response["text"] = "";
			$response["color"] = "";
			try {
				$query_delete_tessUsers = $this->db->query("DELETE FROM tess_members WHERE id = :id");
				$this->db->bind($query_delete_tessUsers, ":id", $id);
				if ($this->db->execute($query_delete_tessUsers)) {
					
					$response["message"] = $lang['deleted_success'];
					$response["color"] = "success";
					
				} else {
					
					$response["message"] = $lang['deleted_error'];
					$response["color"] = "danger";
				}
				return $response;
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function AddEditTessMembers($postData, $add = false)
		{
			global $lang;
			if ($add == false)
				unset($add);
			$response = [];
			$response["text"] = "";
			$response["verif"] = "";
			$response["color"] = "";
			
			
			$updates = "";
			$adds = "";
			$insert_sql1 = "";
			$insert_sql2 = "";
			$permission_string = "";
			
			/* custom inputs */
			$insert_inputs1 = "";
			$insert_inputs2 = "";
			$update_inputs = "";
			$can_insert = true;
			$binded = [];
			if ($postData['pseudo'] == "")
				$can_insert = false;
			try {
				//Build string update
				foreach ($postData as $key => $value) {
					if ($key == "inputs") {
						foreach ($value as $valkey => $val) {
							if ($val !== "")
								$permission_string .= "-" . $val . "-";
						}
					}
					if (
						$key != 'B1' and $key != 'query' and $key != 'supp' and $key != 'edit_tess_user' and $key != 'add_tess_user' and $key != 'id_page' and $key != 'inputs' and $key != 'date_created' and $key != 'passe2' and $key != 'id_tess_user'
					) {
						
						if ($key == "pass") {
							if ($value == $postData['passe2']) {
								if (!$this->checkIfSamePassword($value, $postData["id_tess_user"])) {
									$value = $this->encryptedPassword($value);
								} else {
									
									$response["verif"] .= $lang['password_is_same'] . "</br>";
								}
							} else {
								$response["verif"] .= $lang['passwords_not_match'] . "</br>";
							}
						}
						
						if ($key == "email") {
							if (isset($add)) {
								if ($this->CheckIfEmailExist($value)) {
									$response["verif"] .= $lang['email_exists'] . "</br>";
								}
							} else {
								if ($this->CheckIfEmailExist($value, $postData["id_tess_user"])) {
									$response["verif"] .= $lang['email_exists'] . "</br>";
								}
							}
						}
						
						if ($key == "pseudo") {
							if (isset($add)) {
								if ($this->CheckIfUsernameExist($value)) {
									$response["verif"] .= $lang['username_exists'] . "</br>";
								}
							} else {
								if ($this->CheckIfUsernameExist($value, $postData["id_tess_user"])) {
									$response["verif"] .= $lang['username_exists'] . "</br>";
								}
							}
						}
						
						$key = str_replace('*', '', $key);
						
						$updates .= "`$key`= :$key, ";
						$insert_sql1 .= "`$key`, ";
						$insert_sql2 .= ":$key, ";
						
						$binded[$key] = (isset($value) ? $value : 1);
					}
					
				}
				// var_dump($updates);
				//Delete last comma
				$updates = substr($updates, 0, -2);
				
				//updates
				if (!isset($add)) {
					if ($response["verif"] !== "") {
						$response["message"] = substr($response["verif"], 0, -5);
						$response["color"] = "danger";
					} else {
						$update = $this->db->query("UPDATE tess_members SET " . $updates . ", `permissions`=:permissions  WHERE id = :id");
						foreach ($binded as $key => $value) {
							$this->db->bind($update, ":$key", $value);
						}
						//DEBUG
						// echo "UPDATE tess_members SET ".$updates.", `permissions`=:permissions  WHERE id = :id </br>";
						// foreach($binded as $key => $value){
						// echo "this->db->bind(update,:".$key,", ".$value.") </br>";
						// }
						
						// echo "this->db->bind(permissions,:permissions, ".$permission_string.") </br>";
						// echo "this->db->bind(id,:id, ".$postData["id_tess_user"].")";
						$this->db->bind($update, ":permissions", $permission_string);
						$this->db->bind($update, ":id", $postData["id_tess_user"]);
						if ($this->db->execute($update)) {
							
							$response["message"] = $lang['edited_success'];
							$response["color"] = "success";
						} else {
							$response["message"] = $lang['edited_error'];
							$response["color"] = "danger";
						}
					}
				} else {
					//ADD
					if ($can_insert) {
						if ($response["verif"] !== "") {
							$response["message"] = $response["verif"];
							$response["color"] = "danger";
						} else {
							
							$adds = $this->db->query("insert ignore into tess_members ($insert_sql1 `permissions`, `date_created`) VALUES ($insert_sql2 :permissions, '" . time() . "')");
							foreach ($binded as $key => $value) {
								$this->db->bind($adds, ":$key", $value);
							}
							$this->db->bind($adds, ":permissions", $permission_string);
							if ($this->db->execute($adds)) {
								
								$response["message"] = $lang['added_success'];
								$response["color"] = "success";
							} else {
								$response["message"] = $lang['added_error'];
								$response["color"] = "danger";
							}
						}
					} else {
						$response["message"] = $lang['added_error'];
						$response["color"] = "danger";
					}
				}
				return $response;
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function checkIfSamePassword($pass, $id)
		{
			$query_exist = $this->db->query("SELECT * FROM tess_members WHERE (pass = :password OR pass = :passwordhashed) AND id = :id");
			$this->db->bind($query_exist, ":password", $pass);
			$pass2 = $this->encryptedPassword($pass);
			$this->db->bind($query_exist, ":passwordhashed", $pass2);
			$this->db->bind($query_exist, ":id", $id);
			if ($this->db->single($query_exist)) {
				return true;
			}
			return false;
		}
		
		public function CheckIfEmailExist($email, $userid = "")
		{
			try {
				if ($userid !== "") {
					$query = $this->db->query("SELECT * FROM tess_members WHERE email = :email AND id != :id");
					$this->db->bind($query, ":email", $email);
					$this->db->bind($query, ":id", $userid);
					$this->db->execute($query);
					if ($this->db->rowCount($query) > 0) {
						return true;
					} else {
						return false;
					}
					
				} else {
					$query = $this->db->query("SELECT * FROM tess_members WHERE email = :email");
					$this->db->bind($query, ":email", $email);
					$this->db->execute($query);
					if ($this->db->rowCount($query) > 0) {
						return true;
					} else {
						return false;
					}
				}
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function CheckIfUsernameExist($username, $userid = "")
		{
			try {
				if ($userid !== "") {
					$query = $this->db->query("SELECT * FROM tess_members WHERE pseudo = :username AND id != :id");
					$this->db->bind($query, ":username", $username);
					$this->db->bind($query, ":id", $userid);
					$this->db->execute($query);
					if ($this->db->rowCount($query) > 0) {
						return true;
					} else {
						return false;
					}
					
				} else {
					$query = $this->db->query("SELECT * FROM tess_members WHERE pseudo = :username");
					$this->db->bind($query, ":username", $username);
					$this->db->execute($query);
					if ($this->db->rowCount($query) > 0) {
						return true;
					} else {
						return false;
					}
				}
			} catch (PDOException $e) {
				return false;
			}
		}
	}

?>
