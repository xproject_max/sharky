<?php
	class Cart {
		private $db;
		
		public function __construct()
		{
			$this->db = new Database();
		}
		public function getPage($id){
			// var_dump($id);
			$getPage = $this->db->query("SELECT * FROM tbl_cart WHERE id = :id");
			$this->db->bind($getPage, ":id", $id);
			$all = $this->db->single($getPage);
			$allc = $this->getAllCustoms($all->id);
			if($this->db->rowCount($allc) > 0){
				while($cust = $this->db->fetch($allc)){
					if(isset($cust->name) && $cust->name != "")
						$all->{$cust->name} = $cust->value;
				}
			}
			return $all;
		}
		
		public function add_to_cart($postData,$user_id=""){
			global $_SESSION;
			//Check si ya le id du produit
			if(isset($postData['product_id'])){
				$code = array();
				if(!isset($postData['is_custom'])){
					$postData['is_custom'] = "0";
				}
				if(!isset($postData['sidemasklogo'])){
					$postData['sidemasklogo'] = "";
				}
				$inc = $this->getLastInc($_SESSION['cart'],$postData['product_id'],$postData['is_custom'],$postData['sidemasklogo'],$postData['cart_caisse']);
				// var_dump($inc);
				// die();
				if($this->checkIfExistInCartSession($postData['product_id'],$inc)){
					$code['message'] = 1;
				}else{
					$code['message'] = 0;
				}
				
				//fetch les datas du produits
				$Products_query = $this->db->query("
                    SELECT * 
                    FROM tbl_products
                    WHERE id = :id
                ");
				$this->db->bind($Products_query, ":id", $postData['product_id']);
				$Counted = $this->db->rowCount($Products_query);
				
				//if exists products
				if($Counted > 0){
					
					$this->db->execute($Products_query);
					
					//fetch product detail
					while($product = $this->db->fetch($Products_query)){
						$query_get_metas = $this->db->query("SELECT * ,tbl_products_inputs.value as value, i.name as name FROM tbl_products_inputs LEFT JOIN inputs i ON i.id = tbl_products_inputs.type WHERE fk_products = :id");
						$this->db->bind($query_get_metas, ":id", $product->id);
						$this->db->execute($query_get_metas);
						while($cust = $this->db->fetch($query_get_metas)){
							if(isset($cust->name) && $cust->name != "")
								$product->{$cust->name} = $cust->value;
						}
						$product->cart_quantity = (isset($postData['cart_quantity']) && $postData['cart_quantity'] !== "" ? $postData['cart_quantity'] : 1);
						
						$arrayCaisse = explode("**", $product->quantity_box);
						foreach($arrayCaisse as $Caisse){
							$array = explode("||", $Caisse);
							if($postData['cart_caisse'] == $array[0]){
								$product->caisse = $postData['cart_caisse'];
								$product->quantity = $array[1];
								$getCaisseNombreBouteille = trim(explode(" ", $array[0])[0]); 
								$product->price = number_format(($getCaisseNombreBouteille*$array[2]),2,'.',''); 
								$product->title = $product->title." (".$product->caisse.")";
							}
						}
						
						//Check if enough product for add cart..
						// var_dump($product->id, $product->quantity_box);
						if(!$this->enoughQuantityforAdd($product->id, $postData['cart_quantity'],$inc, $postData['cart_caisse'])){
							
							$code['message'] = 2;
							return $code;
						}
						if(isset($postData['is_custom'])){
							$product->is_custom = $postData['is_custom'];
							$product->sidemasklogo = $postData['sidemasklogo'];
						}
						//check si existe deja dans le panier
						if($this->checkIfExistInCartSession($postData['product_id'],$inc,$postData['cart_caisse'])){
							error_log("EXIST NOOB",0);
							$this->update_cart($postData['product_id'],$inc,$postData['cart_caisse']);
							
						}else{
							error_log("EXIST PAS NOOB",0);
							//On créer le panier
							$this->create_cart($product,$inc,$postData['cart_caisse']);
						}
						
					}
					
					
				}
				
				return $code;
				
			}
			
		}
		
		//Check if enought quantity to add cart
		public function enoughQuantityforAdd($product_id, $qty, $inc, $cart_caisse=""){
			global $_SESSION;
			
			$total = 0;
			$i = 0;
			$inc = (int)$inc;
			//gettotalquantityleft
			$getQty = $this->db->query("SELECT * FROM tbl_products WHERE id = :id");
			$this->db->bind($getQty, ":id", $product_id);
			$Counted = $this->db->rowCount($getQty);
				
			//if exists products
			if($Counted > 0){
				
				$this->db->execute($getQty);
				
				//fetch product detail
				while($qtys = $this->db->fetch($getQty)){
					$query_get_metas = $this->db->query("SELECT * ,tbl_products_inputs.value as value, i.name as name FROM tbl_products_inputs LEFT JOIN inputs i ON i.id = tbl_products_inputs.type WHERE fk_products = :id");
					$this->db->bind($query_get_metas, ":id", $qtys->id);
					$this->db->execute($query_get_metas);
					while($cust = $this->db->fetch($query_get_metas)){
						if(isset($cust->name) && $cust->name != "")
							$qtys->{$cust->name} = $cust->value;
					}
					 
					$arrayCaisse = explode("**", $qtys->quantity_box);
					foreach($arrayCaisse as $Caisse){
						$array = explode("||", $Caisse);
						if($cart_caisse == $array[0]){
							 
							$totalqty = $array[1];
							 
						}
					}
				}
			}
			if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){
				foreach($_SESSION['cart'] as $cart){
					$cart = unserialize($cart);
					if($cart->id == $product_id){
						$arrayCaisse = explode("**", $cart->quantity_box);
						foreach($arrayCaisse as $Caisse){
							$array = explode("||", $Caisse);
							if($product_id == $cart->id && $cart_caisse == $array[0]){
								// var_dump($cart->id." | caisse:".$cart_caisse." | cart_caisse: ".$cart->caisse . " | cart quantity:". $cart->quantity. " | qty: ".$qty);
								$cart->caisse = $cart_caisse;
								$cart->quantity = $array[1];
							}
						}
						// var_dump($cart->caisse, $cart->quantity);
						$totalqty = $cart->quantity;
						if(($cart->id == $product_id) && ($i !== $inc) && $cart->caisse != $cart_caisse){
							$total += $cart->cart_quantity;
						}
					}
					$i++;
				}
				// var_dump($qty ." and ".$total);
				// var_dump($totalqty);
				$realTotal = $qty + $total;
				if($realTotal > $totalqty){
					return false;
				}else{
					return true;
				}
			}else{
				return true;
			}
		}
		//Remove item from cart
		public function remove_from_cart($postData){
			
			if(isset($_SESSION['cart'][$postData['inc']])){
				unset($_SESSION['cart'][$postData['inc']]);
			}
		}
		
		//Create session cart
		public function create_cart($product=[], $inc){
			global $_SESSION;
			
			$_SESSION['cart'][$inc] = serialize($product);
			return $_SESSION['cart'];
		}
		
		//Create session cart
		public function create_cart_user($postData,$infoAccount){
			global $_SESSION;
			if(!$this->checkIfCartExists($postData, $infoAccount)){
				$query_insert_cart = $this->db->query("
                    INSERT into tbl_cart
                    (`fk_users`,`fk_orders`,`cart`,`date`)
                    VALUES (:userid,:orderid, :cart, ".time().")
                ");
				$this->db->bind($query_insert_cart, ":userid", (is_array($infoAccount) ? $infoAccount[0]->id : $infoAccount->id));
				$this->db->bind($query_insert_cart, ":orderid", (!isset($orderid) ? 0 : $orderid));
				$this->db->bind($query_insert_cart, ":cart", serialize($postData));
				if($this->db->execute($query_insert_cart)){
				
				}else{
				}
				$query_last = $this->db->query("SELECT LAST_INSERT_ID() as last FROM tbl_cart");
				return $this->db->single($query_last)->last;
			}else{
				$query_cart = $this->db->query("
                    SELECT * FROM tbl_cart WHERE cart = :cart AND fk_users = :fkusers
                ");
				$this->db->bind($query_cart, ":cart", serialize($postData));
				$this->db->bind($query_cart, ":fkusers", (is_array($infoAccount) ? $infoAccount[0]->id : $infoAccount->id));
				return $this->db->single($query_cart)->id;
			}
		}
		
		//check if cart exists in db
		public function checkIfCartExists($postData, $infoAccount){
			
			$query_cart = $this->db->query("
                SELECT * FROM tbl_cart WHERE cart = :cart AND fk_users = :fkusers
            ");
			$this->db->bind($query_cart, ":cart", serialize($postData));
			$this->db->bind($query_cart, ":fkusers", (is_array($infoAccount) ? $infoAccount[0]->id : $infoAccount->id));
			
			if($this->db->rowCount($query_cart) > 0){
				return true;
			}else{
				return false;
			}
		}
		//check si existe deja dans un panier
		public function checkIfExistInCartSession($product_id,$inc,$caisse=""){
			global $_SESSION;
			
			if(isset($_SESSION['cart'][$inc]->id) && $_SESSION['cart'][$inc]->id == $product_id && ($caisse != "" && $_SESSION['cart'][$inc]->caisse == $caisse)){
				return true;
			}else{
				return false;
			}
		}
		
		
		//cette fonction va update le panier;
		public function update_cart($postData){
			$code = array();
			//Check si ya le id du produit
			if(isset($postData['product_id'])){
				
				//fetch les datas du produits
				$Products_query = $this->db->query("
                    SELECT * 
                    FROM tbl_products
                    WHERE id = :id
                ");
				$this->db->bind($Products_query, ":id", $postData['product_id']);
				$Counted = $this->db->rowCount($Products_query);
				
				//if exists products
				if($Counted > 0){
					
					$this->db->execute($Products_query);
					
					//fetch product detail
					while($product = $this->db->fetch($Products_query)){
						$query_get_metas = $this->db->query("SELECT * ,tbl_products_inputs.value as value, i.name as name FROM tbl_products_inputs LEFT JOIN inputs i ON i.id = tbl_products_inputs.type WHERE fk_products = :id");
						$this->db->bind($query_get_metas, ":id", $product->id);
						$this->db->execute($query_get_metas);
						while($cust = $this->db->fetch($query_get_metas)){
							if(isset($cust->name) && $cust->name != "")
								$product->{$cust->name} = $cust->value;
						}
						$product->cart_quantity = (isset($postData['cart_quantity']) && $postData['cart_quantity'] !== "" ? $postData['cart_quantity'] : 1);
						
						if(isset($postData['is_custom'])){
							$product->is_custom = $postData['is_custom'];
							$product->sidemasklogo = $postData['sidemasklogo'];
						}
						$arrayCaisse = explode("**", $product->quantity_box);
						foreach($arrayCaisse as $Caisse){
							$array = explode("||", $Caisse);
							if($postData['cart_caisse'] == $array[0]){
								$product->caisse = $postData['cart_caisse'];
								$product->quantity = $array[1];
								$getCaisseNombreBouteille = trim(explode(" ", $array[0])[0]); 
								$product->price = number_format(($getCaisseNombreBouteille*$array[2]),2,'.',''); 
								$product->title = $product->title." (".$product->caisse.")";
							}
						}
						
						//Check if enough product for add cart..
						if(!$this->enoughQuantityforAdd($product->id, $postData['cart_quantity'],$inc, $postData['cart_caisse'])){
							
							
							$code['message'] = 2;
							
							return $code;
						}
						//check si existe deja dans le panier
						if($this->checkIfExistInCartSession($postData['product_id'],$postData['inc'],$postData['cart_caisse'])){
							
							//On recréer le panier
							$this->create_cart($product,$postData['inc'],$postData['cart_caisse']);
							
						}else{
							//On créer le panier
							$this->create_cart($product,$postData['inc'],$postData['cart_caisse']);
						}
						
					}
					
					
				}
				
			}
			
		}
		
		public function refresh_cart($postData){
			global $_SESSION;
			
			$return = array();
			$subTotal = number_format(0,2,".","");
			$shippingTotal = number_format(0,2,".","");
			if(isset($postData['shipping'])){
				$shippingTotal = number_format($postData['shipping'], 2, ".", "");
			}
			if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){
				foreach($_SESSION['cart'] as $cart_items){
					
					$item = unserialize($cart_items);
					
					$subTotal += number_format(($item->price * $item->cart_quantity), 2, ".", "");
				}
			}
			$rebates_percent = 0;
			$rebates = 0;
			$subTotal2 = $subTotal;
			if(isset($_SESSION['rebates'])){
				foreach($_SESSION['rebates'] as $key){
					$Promo = new Promotion();
					$arrayPromo = $Promo->getPromoValue($key);
					if($arrayPromo['type_promo'] == "Absolute"){
						$rebates += $arrayPromo['value'];
						$subTotal -= number_format($arrayPromo['value'],2,'.','');
					}else{

						$result = number_format((($arrayPromo['value'] * $subTotal)/100),2,'.','');
						$rebates_percent += $arrayPromo['value'];
						$subTotal -= number_format($result,2,'.','');
						$rebates += number_format($result,2,'.','');
					}
				}

			}
			$subTotal = ($subTotal + $shippingTotal);
			if($subTotal < 0){
				$subTotal = 0;
			}
			$taxesTvq = number_format((0.09975 * $subTotal), 2, '.','');
			$taxesTps = number_format((0.05 * $subTotal), 2, '.','');
			$finalPrice = ($subTotal + $taxesTps + $taxesTvq);
			
			$return['counter'] = (isset($_SESSION['cart']) ? count($_SESSION['cart']) : 0);
			$return['subTotal'] = $subTotal2;
			$return['rebates_percent'] = $rebates_percent;
			$return['rebates'] = $rebates;
			$return['subTotal_rebates'] = number_format(($subTotal2 - $rebates), 2, '.','');
			$return['tps'] = $taxesTps;
			$return['tvq'] = $taxesTvq;
			$return['finalTotal'] = number_format($finalPrice, 2, '.','');
			$return['shippingTotal'] = $shippingTotal;
			
			return $return;
		}
		public function force_refresh_cart($postData){
			global $_SESSION;

			$return = array();
			$subTotal = number_format(0,2,".","");
			$shippingTotal = number_format(0,2,".","");
			if(isset($postData['shipping'])){
				$shippingTotal = number_format($postData['shipping'], 2, ".", "");
			}
			if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){
				foreach($_SESSION['cart'] as $cart_items){

					$item = unserialize($cart_items);

					$subTotal += number_format(($item->price * $item->cart_quantity), 2, ".", "");
				}
			}
			$rebates_percent = 0;
			$rebates = 0;
			$subTotal2 = $subTotal;
			if(isset($_SESSION['rebates'])){
				foreach($_SESSION['rebates'] as $key){
					$Promo = new Promotion();
					$arrayPromo = $Promo->getPromoValue($key);
					if($arrayPromo['type_promo'] == "Absolute"){
						$rebates += $arrayPromo['value'];
						$subTotal -= number_format($arrayPromo['value'],2,'.','');
					}else{

						$result = number_format((($arrayPromo['value'] * $subTotal)/100),2,'.','');
						$rebates_percent += $arrayPromo['value'];
						$subTotal -= number_format($result,2,'.','');
						$rebates += number_format($result,2,'.','');
					}
				}

			}
			$subTotal = ($subTotal + $shippingTotal);
			if($subTotal < 0){
				$subTotal = 0;
			}
			$taxesTvq = number_format((0.09975 * $subTotal), 2, '.','');
			$taxesTps = number_format((0.05 * $subTotal), 2, '.','');
			$finalPrice = ($subTotal + $taxesTps + $taxesTvq);

			$return['counter'] = (isset($_SESSION['cart']) ? count($_SESSION['cart']) : 0);
			$return['subTotal'] = $subTotal2;
			$return['rebates_percent'] = $rebates_percent;
			$return['rebates'] = $rebates;
			$return['subTotal_rebates'] = number_format(($subTotal2 - $rebates), 2, '.','');
			$return['tps'] = $taxesTps;
			$return['tvq'] = $taxesTvq;
			$return['finalTotal'] = number_format($finalPrice, 2, '.','');
			$return['shippingTotal'] = $shippingTotal;

			echo '
			<tr><th>Sous-total</th><td class="RsubTotal">' . $subTotal . ' $</td></tr>
							';
			if(!isset( $_SESSION['rebates'])) {
				$showorhide = "hide";
			}else{
				$showorhide = "";
			}
			echo '
							<tr class="'.$showorhide.'"><th>% Rabais/rebates (' . implode(",", $_SESSION['rebates']) .
			     ')</th><td	 class="RrebatesPercent">'. $rebates_percent.' %</td></tr>
							<tr class="'.$showorhide.'"><th>$ Rabais/rebates</th><td class="Rrebates">'.$rebates.' $</td></tr>
							<tr class="'.$showorhide.'"><th>Sous-total après rabais</th><td class="RsubTotalrebates">'.
			     number_format(($subTotal2 - $rebates), 2, '.','').' $</td></tr>
							';
			echo'	
							<tr><th>TPS</th><td class="Rtps">' . number_format($taxesTps, 2, '.', '') . ' $</td></tr>
							<tr><th>TVQ</th><td class="Rtvq">' . number_format($taxesTvq, 2, '.', '') . ' $</td></tr>
							<tr><th>Total</th><td class="RfinalTotal">' . number_format($finalPrice, 2, '.', '') . ' $</td></tr>
			';
		}
		
		public function getLastInc($session,$product_id=0,$iscustom="0",$sidemasklogo="", $caisse=""){
			$i = 0;
			foreach($session as $cart){
				$cart = unserialize($cart);
				if(($cart->id == $product_id) && (isset($cart->caisse) && $caisse == $cart->caisse)){
					return $i;
				}else{
					$i++;
				}
			}
			return $i;
		}
		
		public function setContent($infoProduct){ 
			
			$html = "";
			//getDetails
			$join = $this->db->query("SELECT t.*, i.name FROM tbl_products_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_products = :id_page");
			$this->db->bind($join, ":id_page", $infoProduct->id);
			$this->db->execute($join);
			while ($joins = $this->db->fetch($join)) { 
				$infoProduct->{$joins->name} = $joins->value;
			}
			//gather boxes;
			$arrayCaisse = explode("**",$infoProduct->quantity_box);
			$options="";
			$i=0;
			foreach($arrayCaisse as $caisse){
				$title = explode("||", $caisse)[0];
				$quantity = explode("||", $caisse)[1];
				$price = explode("||", $caisse)[2];
				$is12or6 = explode(" ", $title)[0];
				if($i == 0){
					$fprice = $price;
				}
				if($caisse != ""){
					$options .= '<option value="'.$title.'" data-price="'.number_format(($is12or6*$price),2,".","").'" data-qty="'.$quantity.'">'.$title.'</option>';
				}
				$i++;
			} 
			$html .= '
			<div class="wrapper_80">
				<div class="close"><i class="far fa-times-circle"></i></div>
				
				<div class="right_part">
					<img src="'.URLROOT.'images/preview.jpeg" alt="popup bg" class="img">
					<div class="content">
						<div class="table">
							<ul class="tablewine">
								<li>
									<div class="title_part">
										<div class="wine_pic" style="background: url('.URLROOT.'upload/'.SITENAME.'/'.$infoProduct->image.')" alt="'.$infoProduct->title.'");"> 
										</div>
										<span>'.$infoProduct->title.'
										<div class="undertext"><img src="'.URLROOT.'images/undertext.png" alt="design under the text"></div>
										</span>
										
									</div>
								</li> 
								<li>
									<ul>
										<li>
											Choix de caisse
										</li>
										<li>
											<select id="choixcaisse" name="choixcaisse">
											'.$options.'
											</select>
										</li>
									</ul>
								</li> 
								<li>
									<ul>
										<li>
											Quantité
										</li>
										<li>
											<div class="quantity">
												<a class="btn btn-custom qty_decrease">-</a>
												<input class="quantity_input" type="number" min="1" max="' . (isset($infoProduct->quantity) ? $infoProduct->quantity :  0) . '" disabled value="1" >
												<a class="btn btn-custom qty_increase">+</a>
											</div>
										</li>
									</ul>
								</li>
								<li>
									<ul>
										<li>
											Prix
										</li>
										<li>
											<span class="newPrice">'.number_format($fprice,2,'.','').'</span>$
										</li>
									</ul>
								</li>
							  
								<li>
									<div class="loader_notification"></div>
									<a href="#" data-img="'.URLROOT.'upload/'.SITENAME.'/'.$infoProduct->image.'" data-product-id="'.$infoProduct->id.'" class="add_to_cart blue-btn">Ajouter au panier</a>
									<div class="info_cart">
										<span class="notification_text"></span>
										<a href="'.URLROOT.getUrlLang(__CARTID__).'" class="gotocart">Voir le panier</a>
									</div>
								</li> 
							</ul>
							 
						</div>
					</div>
				</div>
				<div class="clear">
				</div>
			</div>
			';
			return $html;
		}
	}