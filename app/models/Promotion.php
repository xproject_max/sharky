<?php
	class Promotion {
		private $db;
		
		public function __construct()
		{
			$this->db = new Database();
		}
		public function getPage($id){
			// var_dump($id);
			$getPage = $this->db->query("SELECT * FROM tbl_promotions WHERE id = ".$id."");
			$all = $this->db->single($getPage);
			$allc = $this->getAllCustoms($all->id);
			if($this->db->rowCount($allc) > 0){
				while($cust = $this->db->fetch($allc)){ 
					if(isset($all->{$cust->name}) && $cust->name != "")
						$all->{$cust->name} = $cust->value;
				}
			}
			return $all;
			
			
		}
		public function getpromotionWebsite($id){
			try{
				$query = $this->db->query("SELECT * FROM tbl_promotions WHERE id = " .$id);
				return $this->db->resultSet($query);
			}catch(PDOException $e){
				return false;
			}
		}
		public function getAll(){
			try{
				$query_get_all = $this->db->query("SELECT tbl_promotion.*, tess_controller.add, tess_controller.mod, tess_controller.del, tess_controller.dtype FROM tbl_promotions LEFT JOIN tess_controller ON tbl_contests.type = tess_controller.id WHERE parent = 0");
				return $this->db->resultSet($query_get_all);
			}catch(PDOException $e){
				return false;
			}
		}
		public function getAllCustoms($id){
			try{
				$query_get_all = $this->db->query("SELECT * ,i.name as name FROM tbl_promotions_inputs LEFT JOIN inputs i ON i.id = tbl_promotion_inputs.type WHERE fk_promotions = :id");
				$this->db->bind($query_get_all, ":id", $id);
				$this->db->execute($query_get_all);
				return $query_get_all;
			}catch(PDOException $e){
				return false;
			}
		}
		public function findPromo($code=""){
			$query = $this->db->query("
				SELECT tbl_promotions.*, i.type as input_type, i.value FROM tbl_promotions LEFT JOIN tbl_promotions_inputs i ON
                i.fk_promotions = tbl_promotions.id WHERE tbl_promotions.title = :title  AND tbl_promotions.online=1");
			$this->db->bind($query, ":title", $code);
			$this->db->execute($query);
			if($this->db->rowCount($query) >  0){

				while($promo = $this->db->fetch($query)){
					$join = $this->db->query("SELECT t.*, i.name FROM tbl_promotions_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_promotions = :id_page");
					$this->db->bind($join, ":id_page", $promo->id);
					$this->db->execute($join);
					while ($joins = $this->db->fetch($join)) {
						if(isset($promo->{$joins->name}) && $joins->name != "")
							$promo->{$joins->name} = $joins->value;
					}
					$date_now = date("Y-m-d");
					$date_end = date("Y-m-d", strtotime($promo->date_end));
					if($promo->limit > 0 && ($date_end >= $date_now)){
						return true;
					}else{
						return false;
					}
				}
			}
		}

		public function checkIfPromoExists($code=""){
			global $_SESSION;

			if($code == ""){
				echo "Veuillez entrer un code promotionnel";
			}else{
				if($this->findPromo($code)){
					$_SESSION['rebates'][$code] = $code;
					echo "apply";
				}else{
					echo "Ce code n'existe pas ou le quota a été atteint.";
				}
			}

		}

		public function getPromoValue($code){
			$query = $this->db->query("
				SELECT tbl_promotions.*, i.type as input_type, i.value FROM tbl_promotions LEFT JOIN tbl_promotions_inputs i ON
                i.fk_promotions = tbl_promotions.id WHERE tbl_promotions.title = :title  AND tbl_promotions.online=1");
			$this->db->bind($query, ":title", $code);
			$this->db->execute($query);
			if($this->db->rowCount($query) >  0){

				while($promo = $this->db->fetch($query)){
					$join = $this->db->query("SELECT t.*, i.name FROM tbl_promotions_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_promotions = :id_page");
					$this->db->bind($join, ":id_page", $promo->id);
					$this->db->execute($join);
					while ($joins = $this->db->fetch($join)) {
						if(isset($promo->{$joins->name}) && $joins->name != "")
							$promo->{$joins->name} = $joins->value;
					}
					$date_now = date("Y-m-d");
					$date_end = date("Y-m-d", strtotime($promo->date_end));
					if($promo->limit > 0 && ($date_end >= $date_now)){
						return array("type_promo"=>$promo->type_promo, "value"=>$promo->value);
					}else{
						return false;
					}
				}
			}
		}

		public function checkIfEnoughCode($promoarr){
			$error = false;
			$codes = array();
			$i=0;
			if(empty($promoarr)){
				$error = true;
			}else{
				foreach($promoarr as $promo_id) {

					$Promo = new Promotion();
					if(!$Promo->getPromoValue($promo_id)){
						unset($_SESSION["rebates"][$promo_id]);
						$error = true;
					}
				}
			}
			if($error){
				return false;
			}else{
				return true;
			}
		}
		public function update_promotions($promoarr, $order_id, $infoAccount){

			foreach($promoarr as $promo_id) {
				$query_id_promo = $this->db->query("SELECT id FROM tbl_promotions WHERE title = :title");
				$this->db->bind($query_id_promo, ":title", $promo_id);
				$id = $this->db->single($query_id_promo)->id;
				$query_insert = $this->db->query(
					"
				INSERT INTO tbl_orders_promotions (`fk_promotions`, `fk_orders`, `fk_users`) VALUE(
				:fk_promotions,
				:fk_orders,
				:fk_users
				)"
				);
				$this->db->bind($query_insert, ":fk_promotions", $id);
				$this->db->bind($query_insert, ":fk_orders", $order_id);
				$this->db->bind($query_insert, ":fk_users",  (is_array($infoAccount) ? $infoAccount[0]->id : $infoAccount->id));
				$this->db->execute($query_insert);


				//Drop quantity of promotions left
				$query_update = $this->db->query(
					"
					UPDATE tbl_promotions_inputs t LEFT JOIN inputs i ON i.id = t.type SET t.`value` = t.`value` - 1 WHERE t.fk_promotions = :promo_id and t.type = 107
				"
				);
				$this->db->bind($query_update, ":promo_id", $id);
				$this->db->execute($query_update);
			}

		}
	}