<?php
	
	class User extends Controller
	{
		public $lang;
		public $_LANG;
		public $attempts = 5;
		public $delay_attempts = 5;
		private $db;
		
		public function __construct()
		{
			$this->db = new Database();
			$this->lang = $this->getLang();
			
		}
		
		public function usernameExist($user, $registering = false)
		{
			try {
				$query_username_exist = $this->db->query(
					"
            SELECT * FROM tbl_users
            WHERE (username = :username OR email = :username) " . ($registering ? " AND fk_role == 2" : "") . ""
				);
				$this->db->bind($query_username_exist, ":username", $user);
				if ($this->db->single($query_username_exist)) {
					return true;
				}
				return false;
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function checkIfSamePassword($pass, $id)
		{
			$query_exist = $this->db->query(
				"
        SELECT * FROM tbl_users
        WHERE (password = :password OR password = :passwordhashed)
        AND id = :id"
			);
			$this->db->bind($query_exist, ":password", $pass);
			$pass2 = $this->encryptedPassword($pass);
			$this->db->bind($query_exist, ":passwordhashed", $pass2);
			$this->db->bind($query_exist, ":id", $id);
			if ($this->db->single($query_exist)) {
				return true;
			}
			return false;
		}
		
		public function encryptedPassword($pass)
		{
			$static_salt = SALT_PASS;
			return hash('sha512', $pass . $static_salt);
		}
		
		public function getDelayPasswordAttempts($user)
		{
			try {
				$query_attemps_delay = $this->db->query(
					"
            SELECT um.date_recovery_password
            FROM tbl_users u
            LEFT JOIN tbl_users_meta um ON u.fk_user_meta = um.id
            WHERE u.username = :username"
				);
				$this->db->bind($query_attemps_delay, ":username", $user);
				if ($this->db->single($query_attemps_delay)) {
					return $this->db->single($query_attemps_delay)->date_recovery_password;
				}
				return false;
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function resetPasswordDelayAttempts($user)
		{
			/*
			 * This function will reset password delay attempts to the user;
			 */
			try {
				$reset_attempts = $this->db->query(
					"
            UPDATE tbl_users_meta
            LEFT JOIN tbl_users
            ON tbl_users.fk_user_meta = tbl_users_meta.id
            SET
            tbl_users_meta.date_recovery_password = DATE_ADD(NOW(),
            INTERVAL 5 MINUTE)
            WHERE tbl_users.username = :username"
				);
				$this->db->bind($reset_attempts, ":username", $user);
				if ($this->db->execute($reset_attempts)) {
					//remove time;
					return true;
				}
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function createUserSession($data)
		{
			if (!empty($data)) {
				$_SESSION['user_id'] = $data->id;
				$_SESSION['user_email'] = $data->email;
				$_SESSION['user_name'] = $data->username;
				$_SESSION['status'] = $data->status;
				$_SESSION['profile_image'] = $data->profile_image;
			}
		}
		
		public function validate_users($data, $islogging = false)
		{
			global $lang;
			//Declare variables
			$_LANG = [];
			$Message_error = [];
			$Message_error['username'] = "";
			$Message_error['password'] = "";
			$Message_error['login'] = "";
			$Message_error['login_max'] = "";
			$Message_error['login_form'] = "";
			$Message_error['global'] = "";
			//Require dictionary
			
			//if not exist username check as email
			
			//Constraints and verifications
			
			if (!$this->CheckIfUsernameExist($data['username']) && $data['username'] !== "") {
				$Message_error['username'] .= $lang['error_username_not_exist'];
			}
			if ($this->userAttempts($data['username']) >= $this->attempts) {
				$Message_error['login_max'] .= $lang['error_attempts_max'];
			}
			
			if (strlen($data['username']) == 0) {
				$Message_error['username'] .= $lang['error_username_empty'];
			}
			
			if (strlen($data['password']) == 0) {
				$Message_error['password'] .= $lang['error_password_empty'];
			}
			
			if (
				!$this->userExist($data['username'], $this->encryptedPassword($data['password'])) && $this->CheckIfUsernameExist($data['username']) && $data['username'] !== ""
			) {
				$attempts_left = "";
				if ($this->userAttempts($data['username']) < $this->attempts) {
					
					$this->addAttempts($data['username'], $this->userAttempts($data['username']));
				}
				
				$attempts = $this->userAttempts($data['username']);
				$attempts_left = $this->attempts - $attempts;
				
				if ($attempts_left < 0)
					$attempts_left = 0;
				
				$Message_error['login'] .= $lang['error_attempts'] . $attempts_left . "</br>";
				$Message_error['password'] .= $lang['error_user_not_exist'];
			} else {
				if ($this->userAttempts($data['username']) >= $this->attempts) {
					// Do nothing
					$first_date = strtotime($this->userDelayAttempts($data['username']));
					$second_date = strtotime(date('Y-m-d H:i:s'));
					
					if ($this->userDelayAttempts($data['username']) !== "" && $first_date <= $second_date) {
						unset($Message_error["login_max"]);
						$this->resetAttempts($data['username']);
						if ($islogging == true) {
							$this->create_website_session($this->getUserInfo($data['username'], $this->encryptedPassword($data['password'])));
						}
					}
				} else {
					$has_error = false;
					foreach ($Message_error as $key => $value) {
						if ($value !== "") {
							$has_error = true;
						}
					}
					if ($has_error == false) {
						$this->resetAttempts($data['username']);
						if ($islogging == true) {
							$this->create_website_session($this->getUserInfo($data['username'], $this->encryptedPassword($data['password'])));
						}
					}
					// die(print_r($Message_error));
				}
			}
			
			//Prepare new data array to return
			
			$data_return = [];
			foreach ($data as $key => $value) {
				if ($Message_error[$key] != "")
					$data_return[$key] = ["name" => $key, "message_error" => $Message_error[$key], "value" => $value];
				$Message_error['global'] .= $Message_error[$key];
			}
			
			//Global data login messages
			if ($Message_error["login"] != "")
				$data_return['login'] = ["name" => "login", "message_error" => $Message_error["login"], "value" => ""];
			if (isset($Message_error["login_max"]) && $Message_error["login_max"] != "")
				$data_return['login_max'] = ["name" => "login_max", "message_error" => $Message_error["login_max"], "value" => ""];
			
			if (empty($Message_error['global'])) {
				$data_return['success'] = $this->getUserInfo($data['username'], $this->encryptedPassword($data['password']));
				$data_return['login']['message_error'] = $lang['message_success'] . " " . $data_return['success']->username . " </br>";
			}
			if (!empty($data_return['login_max'])) {
				unset($data_return);
				$data_return['login_max'] = ["name" => "login_max", "message_error" => "veuillez communiquer avec un de nos agents, vous n'avez plus de tentatives", "value" => ""];
			}
			//return data
			return $data_return;
		}
		
		//Password delays

		public function CheckIfUsernameExist($username, $userid = "")
		{
			try {
				if ($userid !== "") {
					$query = $this->db->query("SELECT * FROM tbl_users WHERE username = :username AND id != :id AND fk_role = 2");
					$this->db->bind($query, ":username", $username);
					$this->db->bind($query, ":id", $userid);
					$this->db->execute($query);
					if ($this->db->rowCount($query) > 0) {
						return true;
					} else {
						return false;
					}
					
				} else {
					$query = $this->db->query("SELECT * FROM tbl_users WHERE username = :username AND fk_role = 2");
					$this->db->bind($query, ":username", $username);
					$this->db->execute($query);
					if ($this->db->rowCount($query) > 0) {
						return true;
					} else {
						return false;
					}
				}
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function userAttempts($user)
		{
			try {
				$query_attemps = $this->db->query(
					"
            SELECT um.attempts FROM tbl_users u
            LEFT JOIN tbl_users_meta um ON u.fk_user_meta = um.id
            WHERE u.username = :username"
				);
				$this->db->bind($query_attemps, ":username", $user);
				if ($this->db->single($query_attemps)) {
					return $this->db->single($query_attemps)->attempts;
				}
				return false;
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function userExist($user, $pass)
		{
			$query_exist = $this->db->query(
				"
        SELECT * FROM tbl_users
        WHERE (username = :username OR email = :username)
        AND password = :password"
			);
			$this->db->bind($query_exist, ":username", $user);
			$this->db->bind($query_exist, ":password", $pass);
			if ($this->db->single($query_exist)) {
				return true;
			}
			return false;
		}
		
		public function addAttempts($user, $attempted)
		{
			/*
			 * This function will add an attempt to the user;
			 */
			try {
				$attempts_query = $this->db->query(
					"
            UPDATE tbl_users_meta
            LEFT JOIN tbl_users
            ON tbl_users.fk_user_meta = tbl_users_meta.id
            SET tbl_users_meta.attempts = if(tbl_users_meta.attempts is null, 0, tbl_users_meta.attempts) + 1
            WHERE tbl_users.username = :username"
				);
				$this->db->bind($attempts_query, ":username", $user);
				if ($this->db->execute($attempts_query)) {
					/*
					 * If current attempt equals max attempts,
					 * Start delay before next tries;
					 */
					if (($attempted + 1) == $this->attempts) {
						//Add delay;
						$this->addDelay($user);
					}
				} else {
					return false;
				}
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function addDelay($user)
		{
			try {
				$delay_query = $this->db->query(
					"
            UPDATE tbl_users_meta
            LEFT JOIN tbl_users
            ON tbl_users.fk_user_meta = tbl_users_meta.id
            SET
            tbl_users_meta.attempts_delay = DATE_ADD(NOW(),
            INTERVAL :minutes MINUTE)
            WHERE tbl_users.username = :username"
				);
				$this->db->bind($delay_query, ":minutes", $this->delay_attempts);
				$this->db->bind($delay_query, ":username", $user);
				if ($this->db->execute($delay_query)) {
					//Added time;
					
				}
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function userDelayAttempts($user)
		{
			try {
				$query_attemps_delay = $this->db->query(
					"
            SELECT um.attempts_delay
            FROM tbl_users u
            LEFT JOIN tbl_users_meta um ON u.fk_user_meta = um.id
            WHERE u.username = :username"
				);
				$this->db->bind($query_attemps_delay, ":username", $user);
				if ($this->db->single($query_attemps_delay)) {
					return $this->db->single($query_attemps_delay)->attempts_delay;
				}
				return false;
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function resetAttempts($user)
		{
			/*
			 * This function will reset attempts to the user;
			 */
			try {
				$reset_attempts = $this->db->query(
					"
            UPDATE tbl_users_meta
            LEFT JOIN tbl_users
            ON tbl_users.fk_user_meta = tbl_users_meta.id
            SET tbl_users_meta.attempts = 0,
            tbl_users_meta.attempts_delay = null
            WHERE tbl_users.username = :username"
				);
				$this->db->bind($reset_attempts, ":username", $user);
				if ($this->db->execute($reset_attempts)) {
					//remove time;
					return true;
				}
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function create_website_session($infoAccount)
		{
			$_SESSION['user_id_website'] = (is_array($infoAccount) ? $infoAccount[0]->id : $infoAccount->id);
			$_SESSION['user_username_website'] = (is_array($infoAccount) ? $infoAccount[0]->username : (is_array($infoAccount) ? $infoAccount[0]->username : $infoAccount->username));
			$_SESSION['user_email_website'] = (is_array($infoAccount) ? $infoAccount[0]->email : $infoAccount->email);
			$_SESSION['user_first_name_website'] = (is_array($infoAccount) ? $infoAccount[0]->first_name : $infoAccount->first_name);
			$_SESSION['user_status_website'] = (is_array($infoAccount) ? $infoAccount[0]->status : (isset($infoAccount->status) ? $infoAccount->status : ""));
			$_SESSION['user_role_website'] = (is_array($infoAccount) ? $infoAccount[0]->fk_role : $infoAccount->fk_role);
			$_SESSION['user_picture_website'] = (is_array($infoAccount) ? $infoAccount[0]->profile_image : (isset($infoAccount->profile_image) ? $infoAccount->profile_image : ""));
		}
		
		public function getUserInfo($user, $pass)
		{
			$query_get_user_info = $this->db->query(
				"
        SELECT *, um.*, tbl_users.id as id
            FROM tbl_users
            LEFT JOIN tbl_users_meta um
                ON um.id = tbl_users.fk_user_meta
            WHERE (tbl_users.username = :username OR tbl_users.email = :username) AND tbl_users.password = :password"
			);
			$this->db->bind($query_get_user_info, ":username", $user);
			$this->db->bind($query_get_user_info, ":password", $pass);
			if ($this->db->single($query_get_user_info)) {
				return $this->db->single($query_get_user_info);
			}
			return false;
		}
		
		public function getUserWebsiteFromEmail($email)
		{
			try {
				$query = $this->db->query(
					"
            SELECT *, um.*, tbl_users.id as id
            FROM tbl_users
            LEFT JOIN tbl_users_meta um
                ON um.id = tbl_users.fk_user_meta
            WHERE tbl_users.email = :email"
				);
				$this->db->bind($query, ":email", $email);
				return $this->db->resultSet($query);
			} catch (PDOException $e) {
				return false;
			}
		}
		
		public function createAccount($postData, $add = false)
		{
			
			$errors = array();
			//check if empty values
			$errors = $this->validate_form_user_creation($postData);
			
			if (isset($errors['cantAdd'])) {
				$_SESSION['secure_post'] = $errors;
				header("location: " . getUrlLang($postData['page_id']));
				exit();
			} else {
				if ($this->isGuest($postData['email'])) {
					$id = $this->getId($postData);
					$infoAccount = $this->getUserWebsite($id);
					$this->makeThisGuestAnAccount($infoAccount[0], $postData);
					$userid = $id;
				} else {
					
					$userid = $this->createAccountFromCheckout($postData, true);
					
				}
				$_SESSION['user_id'] = $userid;
				$_SESSION['first_name_account'] = $postData['first_name_account'];
				
				header("location: " . getUrlLang($postData['page_id']));
			}
		}
		
		public function validate_form_user_creation($postData)
		{
			global $l, $lang;
			$message_error = array();
			$canAdd = true;
			foreach ($postData as $key => $value) {
				$msg = "";
				if ($key == "accept_terms" && $value == 0) {
					$msg .= $lang['accept_terms_error'];
					$canAdd = false;
				}
				
				if (($key == "email" || $key == "first_name_account" || $key == "password" || $key == "pass2") && $value == "") {
					$msg .= $lang['this_is_empty'];
					$canAdd = false;
				}
				
				if ($key == "pass2" && $value !== $postData["password"]) {
					$msg .= $lang['passwords_not_match'];
					$canAdd = false;
					
				}
				
				if ($key == "email" && $this->CheckIfUsernameExistAndNotGuest($postData[$key])) {
					$msg .= $lang['email_exists'];
					$canAdd = false;
				}
				$message_error[$key] = ["key" => $key, "value" => $value, "message_error" => $msg];
				
			}
			if ($canAdd == false) {
				$message_error['cantAdd'] = ["value" => true];
			}
			// die(print_r($message_error));
			return $message_error;
			
		}
		
		public function CheckIfUsernameExistAndNotGuest($username, $userid = "")
		{
			try {
				if ($userid !== "") {
					$query = $this->db->query("SELECT * FROM tbl_users WHERE username = :username AND id != :id AND fk_role = 2");
					$this->db->bind($query, ":username", $username);
					$this->db->bind($query, ":id", $userid);
					$this->db->execute($query);
					if ($this->db->rowCount($query) > 0) {
						return true;
					} else {
						return false;
					}
					
				} else {
					$query = $this->db->query("SELECT * FROM tbl_users WHERE username = :username AND fk_role = 2");
					$this->db->bind($query, ":username", $username);
					$this->db->execute($query);
					if ($this->db->rowCount($query) > 0) {
						return true;
					} else {
						return false;
					}
				}
			} catch (PDOException $e) {
				return false;
			}
		}
		
		//Check if empty values of posted data

		public function isGuest($email)
		{
			$query_guest = $this->db->query(
				"
            SELECT * FROM tbl_users
            WHERE (username = :email || email = :email)
        "
			);
			$this->db->bind($query_guest, ":email", $email);
			if ($this->db->rowCount($query_guest) > 0) {
				return true;
			} else {
				return false;
			}
		}
		
		public function getId($data)
		{
			$getId = $this->db->query(
				"
            SELECT id FROM tbl_users WHERE email = :email
        "
			);
			$this->db->bind($getId, ":email", $data['email']);
			return $this->db->single($getId)->id;
		}
		
		public function getUserWebsite($id)
		{
			try {
				$query = $this->db->query(
					"
            SELECT *, um.*, tbl_users.id as id
            FROM tbl_users
            LEFT JOIN tbl_users_meta um
                ON um.id = tbl_users.fk_user_meta
            WHERE tbl_users.id = :id"
				);
				$this->db->bind($query, ":id", $id);
				return $this->db->resultSet($query);
			} catch (PDOException $e) {
				return false;
			}
		}
		
		//Create account

		public function makeThisGuestAnAccount($infoAccount, $postData = [])
		{
			global $_SESSION;
			if (!empty($postData)) {
				$query_update = $this->db->query(
					"
                UPDATE tbl_users JOIN tbl_users_meta ON tbl_users_meta.id = tbl_users.fk_user_meta
                SET tbl_users.fk_role = 2,
                tbl_users.password = :password,
                tbl_users.first_name_account = :fna,
                tbl_users_meta.first_name = :firstname,
                tbl_users_meta.last_name = :lastname,
                tbl_users_meta.address = :address,
                tbl_users_meta.phone = :phone,
                tbl_users_meta.state = :state,
                tbl_users_meta.city = :city,
                tbl_users_meta.zipcode = :zipcode,
                tbl_users_meta.newsletter = :newsletter
                WHERE tbl_users.id = :id
            "
				);
				$this->db->bind($query_update, ":password", $this->encryptedPassword($postData['password']));
				$this->db->bind($query_update, ":fna", $postData['first_name_account']);
				$this->db->bind($query_update, ":firstname", $postData['first_name']);
				$this->db->bind($query_update, ":lastname", $postData['last_name']);
				$this->db->bind($query_update, ":address", $postData['address']);
				$this->db->bind($query_update, ":phone", $postData['phone']);
				$this->db->bind($query_update, ":state", $postData['state']);
				$this->db->bind($query_update, ":city", $postData['city']);
				$this->db->bind($query_update, ":zipcode", $postData['zipcode']);
				$this->db->bind($query_update, ":newsletter", (isset($postData['accept_newsletter']) ? 1 : 0));
				$this->db->bind($query_update, ":id", $infoAccount->id);
				if ($this->db->execute($query_update)) {
					
					newAccountTemplate($infoAccount->email);
					return true;
				} else {
					die(print_r($query_update->errorInfo()));
				}
			} else {
				$temporaryPassword = randomPassword();
				$_SESSION['passwordTemp'] = $temporaryPassword;
				$temporaryPassword = $this->encryptedPassword($temporaryPassword);
				$query_update = $this->db->query(
					"
                UPDATE tbl_users SET fk_role = 2, password = :password WHERE id = :id
            "
				);
				$this->db->bind($query_update, ":password", $temporaryPassword);
				$this->db->bind($query_update, ":id", $infoAccount->id);
				if ($this->db->execute($query_update)) {
					
					newAccountTemplate($infoAccount->email);
					return true;
				} else {
					return false;
				}
			}
		}
		
		//generate billing form

		public function createAccountFromCheckout($postData, $add = false)
		{
			global $lang;
			if ($add == false)
				unset($add);
			
			
			$updates = "";
			$updatesd = "";
			$adds = "";
			$insert_sql1 = "";
			$insert_sql2 = "";
			$permission_string = "";
			
			/* custom inputs */
			$insert_inputs1 = "";
			$insert_inputs2 = "";
			$update_inputs = "";
			$can_insert = true;
			$binded = [];
			if (!isset($_POST['username'])) {
				$postData['username'] = $postData['email'];
			}
			if (!isset($postData['password'])) {
				$postData['region '] = "Québec";
				$postData['gender '] = "";
				$postData['birthdate '] = "";
				$postData['profile_image '] = "";
				
				$postData['password'] = randomPassword();
				$_SESSION['passwordTemp'] = $postData['password'];
				$postData['password'] = $this->encryptedPassword($postData['password']);
				if (isset($postData['account_method']) && $postData['account_method'] == "guest") {
					$postData['fk_role'] = 3;
					unset($_SESSION['passwordTemp']);
					
				} else {
					$postData['fk_role'] = 2;
					$postData['password'] = $postData['password'];
				}
				
			} else {
				
				$postData['fk_role'] = 2;
				$postData['password'] = $this->encryptedPassword($postData['password']);
			}
			if ($postData['username'] == "")
				$can_insert = false;
			
			
			try {
				
				//Build string update
				foreach ($postData as $key => $value) {
					if ($key == "inputs") {
						foreach ($value as $valkey => $val) {
							if ($val !== "")
								$permission_string .= "-" . $val . "-";
						}
					}
					if (
						$key != 'B1' and $key != 'query' and $key != 'supp' and $key != 'edit_user' and $key != 'add_user' and $key != 'id_page' and $key != 'inputs' and $key != 'date_created' and $key != 'page_id' and $key != 'first_name_shipping' and $key != 'last_name_shipping' and $key != 'address_shipping' and $key != 'phone_shipping' and $key != 'state_shipping' and $key != 'city_shipping' and $key != 'zipcode_shipping' and $key != 'sub_total' and $key != 'delivery_cost' and $key != 'tps' and $key != 'tvq' and $key != 'total' and $key != 'account_method' and $key != 'payment_method' and $key != 'passe2' and $key != 'accept_terms' and $key != (isset($add) ? '' : 'id_user')
					) {
						
						
						
						if ($key == "email") {
							if (isset($add)) {
								if ($this->CheckIfEmailExist($value)) {
								}
							} else {
								if ($this->CheckIfEmailExist($value, $postData["id_user"])) {
								}
							}
						}
						$key_table = "tbl_users";
						if (
							$key == "first_name" || $key == "last_name" || $key == "state" || $key == "region" || $key == "city" || $key == "address" || $key == "zipcode" || $key == "phone"
						) {
							$key_table = "tbl_users_meta";
						}
						
						if ($key == "username") {
							if (isset($add)) {
								if ($this->CheckIfUsernameExist($value)) {
								}
							} else {
								if ($this->CheckIfUsernameExist($value, $postData["id_user"])) {
								}
							}
						}
						
						$key = str_replace('*', '', $key);
						
						$updates .= "`$key_table`.`$key`= :$key, ";
						// $updatesd .= "`$key_table`.`$key`= '$value', ";
						
						$binded[$key] = (isset($value) ? $value : 1);
					}
					
				}
				//Delete last comma
				$updates = substr($updates, 0, -2);
				
				//updates
				if (!isset($add)) {
					
					$update = $this->db->query("UPDATE tbl_users LEFT JOIN tbl_users_meta ON tbl_users_meta.id = tbl_users.fk_user_meta  SET " . $updates . ", `tbl_users`.`permissions`=:permissions  WHERE tbl_users.id = :id");
					foreach ($binded as $key => $value) {
						$this->db->bind($update, ":$key", $value);
					}
					
					$this->db->bind($update, ":permissions", $permission_string);
					$this->db->bind($update, ":id", $postData["id_user"]);
					if ($this->db->execute($update)) {
					
					
					}
				} else {
					
					//ADD
					if ($can_insert) {
						
						$adds = $this->db->query(
							"
                    insert ignore into tbl_users_meta (
                        `first_name`,
                        `last_name`,
                        `country`,
                        `state`,
                        `region`,
                        `city`,
                        `address`,
                        `zipcode`,
                        `gender`,
                        `birthdate`,
                        `phone`,
                        `newsletter`,
                        `profile_image`
                    ) VALUES (
                        :first_name,
                        :last_name,
                        :country,
                        :state,
                        :region,
                        :city,
                        :address,
                        :zipcode,
                        :gender,
                        :birthdate,
                        :phone,
                        :newsletter,
                        :profile_image
                    )"
						);
						$this->db->bind($adds, ":first_name", (isset($postData['first_name']) ? $postData['first_name'] : ""));
						$this->db->bind($adds, ":last_name", $postData['last_name']);
						$this->db->bind($adds, ":country", (isset($postData['country']) ? $postData['country'] : ""));
						$this->db->bind($adds, ":state", (isset($postData['state']) ? $postData['state'] : ""));
						$this->db->bind($adds, ":region", (isset($postData['region']) ? $postData['region'] : ""));
						$this->db->bind($adds, ":city", (isset($postData['city']) ? $postData['city'] : ""));
						$this->db->bind($adds, ":address", (isset($postData['address']) ? $postData['address'] : ""));
						$this->db->bind($adds, ":zipcode", (isset($postData['zipcode']) ? $postData['zipcode'] : ""));
						$this->db->bind($adds, ":gender", (isset($postData['gender']) ? $postData['gender'] : ""));
						$this->db->bind($adds, ":birthdate", (isset($postData['birthdate']) ? $postData['birthdate'] : ""));
						$this->db->bind($adds, ":phone", (isset($postData['phone']) ? $postData['phone'] : ""));
						$this->db->bind($adds, ":newsletter", (isset($postData['accept_newsletter']) ? $postData['accept_newsletter'] : ""));
						$this->db->bind($adds, ":profile_image", (isset($postData['profile_image']) ? $postData['profile_image'] : ""));
						if ($this->db->execute($adds)) {
							
							$current_page_id = $this->db->query("SELECT LAST_INSERT_ID() as 'lastid' FROM tbl_users_meta");
							$current_page_id = $this->db->single($current_page_id);
							if ($current_page_id->lastid !== 0) {
								$current_page_id = $current_page_id->lastid;
							}
							
							$insert_url = $this->db->query(
								"
                        INSERT INTO tbl_users(
                            `fk_role`,
                            `fk_user_meta`,
                            `email`,
                            `username`,
                            `first_name_account`,
                            `password`,
                            `permissions`,
                            `date_created`,
                            `online`
                        ) VALUES(
                            :fk_role,
                            :fk_user_meta,
                            :email,
                            :username,
                            :first_name_account,
                            :password,
                            :permissions,
                            '" . date("Y-m-d") . "',
                            :online
                        )"
							);
							
							$this->db->bind($insert_url, ":fk_role", $postData['fk_role']);
							$this->db->bind($insert_url, ":fk_user_meta", $current_page_id);
							$this->db->bind($insert_url, ":email", $postData['email']);
							$this->db->bind($insert_url, ":username", $postData['username']);
							$this->db->bind($insert_url, ":first_name_account", (isset($postData['first_name_account']) ? $postData['first_name_account'] : $postData['first_name']));
							$this->db->bind($insert_url, ":password", (isset($postData['password']) ? $postData['password'] : ""));
							$this->db->bind($insert_url, ":permissions", "");
							$this->db->bind($insert_url, ":online", 0);
							
							if ($this->db->execute($insert_url)) {
								$current_order_id = $this->db->query("SELECT LAST_INSERT_ID() as 'lastid' FROM tbl_users");
								$current_order_id = $this->db->single($current_order_id);
								if ($current_order_id->lastid !== 0) {
									
									$current_order_id = $current_order_id->lastid;
									$response = $current_order_id;
								}
							} else {
								// die(print_r($insert_url->errorInfo()));
							}
						} else {
						
						
						}
						if ($postData['fk_role'] == 2) {
							newAccountTemplate($postData['email']);
						}
					}
					
				}
				return $response;
			} catch (PDOException $e) {
				return false;
			}
		}
		
		//generate shipping form

		public function CheckIfEmailExist($email, $userid = "")
		{
			try {
				if ($userid !== "") {
					$query = $this->db->query("SELECT * FROM tbl_users WHERE email = :email AND id != :id");
					$this->db->bind($query, ":email", $email);
					$this->db->bind($query, ":id", $userid);
					$this->db->execute($query);
					if ($this->db->rowCount($query) > 0) {
						return true;
					} else {
						return false;
					}
					
				} else {
					$query = $this->db->query("SELECT * FROM tbl_users WHERE email = :email");
					$this->db->bind($query, ":email", $email);
					$this->db->execute($query);
					if ($this->db->rowCount($query) > 0) {
						return true;
					} else {
						return false;
					}
				}
			} catch (PDOException $e) {
				return false;
			}
		}
		
		
		//Make a guest a real account

		public function generate_billing($id = "")
		{
			global $lang, $l;
			$form = new Form();
			/* Fetch allowed states */
			$state_str = "";
			if (!empty(fetch_states())) {
				foreach (fetch_states((isset($page->country) ? $page->country : "")) as $state) {
					 if($state == "Québec"){
						 
						$currentlabel = $state;
					 }
					$state_str .= "<span class=' custom-option " . ($state == "Québec" ? "selected" : "") . "' value='" . $state . "'>" . $state . "</span>";
				}
			}
			
			if (isset($_SESSION['user_id_website'])) {
				$infoAccount = $this->getUserWebsite($_SESSION['user_id_website'])[0];
			}
			$html = '
                <div class="delta grid_12"><h4 style="color:black;padding:3%;text-align: center !important;">' . $lang['billing_information'] . '</h4></div>
                
                <div class="alpha grid_6 grid_1024_12">' . $form->input('first_name', 'text', $lang['first_name'], true, (isset($infoAccount) ? $infoAccount->first_name : (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['first_name']['value'] : "")), (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['first_name']['message_error'] : ""), " " . (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['first_name']['message_error'] !== "" ? "is-invalid" : "is-valid" : "") . "") . '</div>
                <div class="omega grid_6 grid_1024_12">' . $form->input('last_name', 'text', $lang['last_name'], true, (isset($infoAccount) ? $infoAccount->last_name : (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['last_name']['value'] : "")), (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['last_name']['message_error'] : ""), " " . (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['last_name']['message_error'] !== "" ? "is-invalid" : "is-valid" : "") . "") . '</div>
                <div class="clear"></div>
                <div class="alpha grid_6  hide_pickup grid_1024_12">' . $form->input('address', 'text', $lang['address'], true, (isset($infoAccount) ? $infoAccount->address : (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['address']['value'] : "")), (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['address']['message_error'] : ""), " " . (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['address']['message_error'] !== "" ? "is-invalid" : "is-valid" : "") . "") . '</div>
                <div class="omega grid_6 phone_pickup grid_1024_12">' . $form->input('phone', 'text', $lang['phone'] . $lang['optional'], true, (isset($infoAccount) ? $infoAccount->phone : (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['phone']['value'] : "")), (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['phone']['message_error'] : ""), " " . (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['phone']['message_error'] !== "" ? "is-invalid" : "is-valid" : "") . "") . '</div>
                <div class="clear unclear_pickup"></div>
                <div class="alpha grid_6 hide_pickup grid_1024_12">' . $form->input('state', 'select', $lang['state'], true, (isset($infoAccount) ? $infoAccount->state : (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['state']['value'] : "")), (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['state']['message_error'] : ""), " " . (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['state']['message_error'] !== "" ? "is-invalid" : "is-valid" : "") . "", $state_str, "",$currentlabel) . '</div>
                <div class="omega grid_6 hide_pickup grid_1024_12">' . $form->input('city', 'text', $lang['city'], true, (isset($infoAccount) ? $infoAccount->city : (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['city']['value'] : "")), (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['city']['message_error'] : ""), " " . (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['city']['message_error'] !== "" ? "is-invalid" : "is-valid" : "") . "") . '</div>
                <div class="clear unclear_pickup"></div>
                <div class="alpha hide_pickup grid_6 grid_1024_12">' . $form->input('zipcode', 'text', $lang['zipcode'], true, (isset($infoAccount) ? $infoAccount->zipcode : (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['zipcode']['value'] : "")), (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['zipcode']['message_error'] : ""), " " . (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['zipcode']['message_error'] !== "" ? "is-invalid" : "is-valid" : "") . "") . '</div>
                ' . (!isset($_SESSION['user_id_website']) ? '<div class="omega grid_6 grid_1024_12">' . $form->input('email', 'text', $lang['email'], true, (isset($infoAccount) ? $infoAccount->email : (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['email']['value'] : "")), (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['email']['message_error'] : ""), " " . (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['email']['message_error'] !== "" ? "is-invalid" : "is-valid" : "") . "") . '</div>' : '') . '
                <div class="clear"></div>
        ';
			
			return $html;
		}
		
		public function generate_shipping($id = "")
		{
			global $lang, $l;
			$form = new Form();
			/* Fetch allowed states */
			$state_str = "";
			if (!empty(fetch_states())) {
				foreach (fetch_states((isset($page->country) ? $page->country : "")) as $state) {
					
					$state_str .= "<span class=' custom-option " . ($state == "Québec" ? "selected" : "") . "' value='" . $state . "'>" . $state . "</span>";
				}
			}
			$html = '
                <div class="delta grid_12"><h4 style="padding:3%;color:black;text-align: center !important;">' . $lang['shipping_information'] . '</h4></div>
                
                <div class="alpha grid_6 grid_1024_12">' . $form->input('first_name_shipping', 'text', $lang['first_name'], true, (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['first_name_shipping']['value'] : ""), (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['first_name_shipping']['message_error'] : ""), " " . (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['first_name_shipping']['message_error'] !== "" ? "is-invalid" : "is-valid" : "")) . '</div>
                <div class="omega grid_6 grid_1024_12">' . $form->input('last_name_shipping', 'text', $lang['last_name'], true, (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['last_name_shipping']['value'] : ""), (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['last_name_shipping']['message_error'] : ""), " " . (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['last_name_shipping']['message_error'] !== "" ? "is-invalid" : "is-valid" : "")) . '</div>
                <div class="clear"></div>
                <div class="alpha grid_6 grid_1024_12">' . $form->input('address_shipping', 'text', $lang['address'], true, (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['address_shipping']['value'] : ""), (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['address_shipping']['message_error'] : ""), " " . (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['address_shipping']['message_error'] !== "" ? "is-invalid" : "is-valid" : "")) . '</div>
                <div class="omega grid_6 grid_1024_12">' . $form->input('phone_shipping', 'text', $lang['phone'], true, (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['phone_shipping']['value'] : ""), (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['phone_shipping']['message_error'] : ""), " " . (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['phone_shipping']['message_error'] !== "" ? "is-invalid" : "is-valid" : "")) . '</div>
                <div class="clear"></div>
                <div class="alpha grid_6 grid_1024_12">' . $form->input('state_shipping', 'select', $lang['state'], true, (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['state_shipping']['value'] : ""), (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['state_shipping']['message_error'] : ""), " " . (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['state_shipping']['message_error'] !== "" ? "is-invalid" : "is-valid" : ""), $state_str) . '</div>
                <div class="omega grid_6 grid_1024_12">' . $form->input('city_shipping', 'text', $lang['city'], true, (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['city_shipping']['value'] : ""), (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['city_shipping']['message_error'] : ""), " " . (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['city_shipping']['message_error'] !== "" ? "is-invalid" : "is-valid" : "")) . '</div>
                <div class="clear"></div>
                <div class="alpha grid_6 grid_1024_12">' . $form->input('zipcode_shipping', 'text', $lang['zipcode'], true, (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['zipcode_shipping']['value'] : ""), (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['zipcode_shipping']['message_error'] : ""), " " . (isset($_SESSION['secure_post']) ? $_SESSION['secure_post']['zipcode_shipping']['message_error'] !== "" ? "is-invalid" : "is-valid" : "")) . '</div>
                 <div class="omega clear"></div>
        ';
			
			return $html;
		}
		
		//change address

		public function update_password($email, $newPassword)
		{
			
			$query_update = $this->db->query(
				"
            UPDATE tbl_users SET password = :password
            WHERE email = :email
        "
			);
			$newPassword = $this->encryptedPassword($newPassword);
			$this->db->bind($query_update, ":password", $newPassword);
			$this->db->bind($query_update, ":email", $email);
			
			if ($this->db->execute($query_update)) {
				$update_forgotten_password = $this->db->query(
					"
                UPDATE tbl_users LEFT JOIN tbl_users_meta meta
                ON meta.id = tbl_users.fk_user_meta
                SET meta.key_password = ''
                WHERE tbl_users.email = :email
            "
				);
				$this->db->bind($update_forgotten_password, ":email", $email);
				$this->db->execute($update_forgotten_password);
				
				return true;
			} else {
				return false;
			}
			
			
		}
		
		public function change_address($postData)
		{
			$query_address = $this->db->query(
				"
            UPDATE tbl_users LEFT JOIN tbl_users_meta meta
            ON tbl_users.fk_user_meta = meta.id
            SET
            meta.first_name = :first_name,
            meta.last_name = :last_name,
            meta.address = :address,
            meta.phone = :phone,
            meta.city = :city,
            meta.zipcode = :zipcode
            WHERE tbl_users.email = :email
        "
			);
			$this->db->bind($query_address, ":first_name", $postData['first_name']);
			$this->db->bind($query_address, ":last_name", $postData['last_name']);
			$this->db->bind($query_address, ":address", $postData['address']);
			$this->db->bind($query_address, ":phone", $postData['phone']);
			$this->db->bind($query_address, ":city", $postData['city']);
			$this->db->bind($query_address, ":zipcode", $postData['zipcode']);
			$this->db->bind($query_address, ":email", $postData['email']);
			
			if ($this->db->execute($query_address)) {
				return true;
			} else {
				
				return false;
			}
		}
	}

?>