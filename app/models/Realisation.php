<?php
	class Realisation {
		private $db;
		
		public function __construct()
		{
			$this->db = new Database();
		}
		 
		
		public function filterByCateg($id){
			global $lang, $l;
			$html = "";
			$db = new Database();
			if($id == "all") {
				$querySlides = $db->query("SELECT * FROM tbl_pages WHERE parent = :id");
				$db->bind($querySlides,':id', 59);
				$db->execute($querySlides);
				while($galeries = $db->fetch($querySlides)){
					if($galeries->fk_gallery !==""){
						echo getGalleryblock($galeries->fk_gallery);
					}								
				}
			}else{
				$querySlides = $db->query("SELECT * FROM tbl_pages WHERE id = :id");
				$db->bind($querySlides,':id', $id);
				$db->execute($querySlides); 
				while($galeries = $db->fetch($querySlides)){
					if(is_string($galeries->fk_gallery) && $galeries->fk_gallery !== "" ){
						echo getGalleryblock($galeries->fk_gallery);
					}else{
						echo '<div class="slick-slide isEmpty">Il n\'y a pas de réalisation pour cette catégorie pour le moment.</div>';
					}						
				}
			}
		}
		
		public function filterByCategNoSlider($id){
			global $lang, $l;
			$html = "";
			$db = new Database();
			if($id == "all") {
				$querySlides = $db->query("SELECT * FROM tbl_pages WHERE parent = :id");
				$db->bind($querySlides,':id', 59);
				$db->execute($querySlides);
				while($galeries = $db->fetch($querySlides)){
					if($galeries->fk_gallery !==""){
						echo getGalleryblockNoSlider($galeries->fk_gallery);
					}								
				}
			}else{
				$querySlides = $db->query("SELECT * FROM tbl_pages WHERE id = :id");
				$db->bind($querySlides,':id', $id);
				$db->execute($querySlides); 
				while($galeries = $db->fetch($querySlides)){
					if(is_string($galeries->fk_gallery) && $galeries->fk_gallery !== "" ){
						echo getGalleryblockNoSlider($galeries->fk_gallery);
					}else{
						echo '<div class="slick-slide isEmpty">Il n\'y a pas de réalisation pour cette catégorie pour le moment.</div>';
					}						
				}
			}
		}
	}