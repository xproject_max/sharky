<?php
	class Product {
		private $db;

		public function __construct()
		{
			$this->db = new Database();
		}
		public function getPage($id){
			// var_dump($id);
			$getPage = $this->db->query("SELECT * FROM tbl_products WHERE id = :id");
			$this->db->bind($getPage, ":id", $id);
			$all = $this->db->single($getPage);
			if(isset($all->id)){
				$query_get_all = $this->db->query("SELECT * ,i.name as name FROM tbl_products_inputs LEFT JOIN inputs i ON i.id = tbl_products_inputs.type WHERE fk_products = :id");
				$this->db->bind($query_get_all, ":id", $all->id);
				$this->db->execute($query_get_all);
				if($this->db->rowCount($query_get_all) > 0){
					while($cust = $this->db->fetch($query_get_all)){
						if(isset($cust->name) && $cust->name != "")
							$all->{$cust->name} = $cust->value;
					}
				}
				return $all;
			}
			return false;

		}
		public function getPagenormal($id){
			try{
				$query = $this->db->query("SELECT tbl_pages.*, i.type as input_type, i.value FROM tbl_pages LEFT JOIN tbl_pages_inputs i ON
                i.fk_pages = tbl_pages.id WHERE tbl_pages.id = " .$id);
				return $this->db->resultSet($query);
			}catch(PDOException $e){
				return false;
			}
		}
		public function is_maintenance(){
			$query = $this->db->query("
                SELECT * FROM tbl_pages WHERE id ='.__MAINTENANCEID__ .'and online = 1
            ");
			$this->db->execute($query);
			if($this->db->rowCount($query) > 0){
				return true;
			}else{
				return false;
			}
		}
		public function checkIfExistJoinInputs($key, $fkproducts){
			$exist = $this->db->query("SELECT * FROM tbl_products_inputs WHERE type = :key AND fk_products = :fkproducts");
			$this->db->bind($exist, ":key", $key);
			$this->db->bind($exist, ":fkproducts", $fkproducts);
			if($this->db->single($exist)){
				return true;
			}else{
				return false;
			}
		}
		public function checkIfUrlExist($product_id){
			$exist = $this->db->query("SELECT * FROM tbl_products_urls WHERE product_id = :product_id");
			$this->db->bind($exist, ":product_id", $product_id);
			if($this->db->single($exist)){
				return true;
			}else{
				return false;
			}
		}
		public function checkIfModulesExist($inputid){
			$exist = $this->db->query("SELECT * FROM tess_controller WHERE id = :inputid");
			$this->db->bind($exist, ":inputid", $inputid);
			if($this->db->single($exist)){
				return true;
			}else{
				return false;
			}
		}
		public function getProductWebsite($id){
			try{
				$query = $this->db->query("SELECT * FROM tbl_products WHERE id = " .$id);
				return $this->db->resultSet($query);
			}catch(PDOException $e){
				return false;
			}
		}
		public function getAll(){
			try{
				$query_get_all = $this->db->query("SELECT tbl_products.*, tess_controller.add, tess_controller.mod, tess_controller.del, tess_controller.dtype FROM tbl_products LEFT JOIN tess_controller ON tbl_products.type = tess_controller.id WHERE parent = 0");
				return $this->db->resultSet($query_get_all);
			}catch(PDOException $e){
				return false;
			}
		}
		public function getAllCustoms($id){
			try{
				$query_get_all = $this->db->query("SELECT * ,i.name as name FROM tbl_products_inputs LEFT JOIN inputs i ON i.id = tbl_products_inputs.type WHERE fk_products = :id");
				$this->db->bind($query_get_all, ":id", $id);
				if($this->db->execute($query_get_all)){
					return $query_get_all;
				}else{
					return false;
				}
			}catch(PDOException $e){
				return false;
			}
		}

		public function getChildList($id, $level){

			global $Tess_db, $linv, $Tess, $Functions, $install_page_order, $_SESSION, $l;

			require_once APPROOT.'/helpers/function.php';
			$lang = new Lang();
			$l = "";
			if($lang->lang == "en")
				$l="_en";

			try{
				$query = $this->db->query("SELECT * FROM tbl_products WHERE parent = :id ORDER BY `order` ASC");
				$this->db->bind($query,":id",$id);
				$child_bool = false;
				$childMenu ='';
				$key=0;
				$currents = $this->db->resultSet($query);
				$custom = array();
				foreach($currents as $current){

					$custom['prix'] = "";
					$custom['Quantité'] = "";
					$custom['Compagnies'] = "";
					$custom_inputs = $this->getAllCustoms($current->id);


					foreach($custom_inputs as $array){
						$get_type = $this->db->query("SELECT * FROM inputs WHERE id = :id");
						$this->db->bind($get_type, ":id", $array->type);
						$key = $this->db->single($get_type);

						if($key->title == "Compagnies"){
							$query_title_company = $this->db->query("SELECT cm.company_name FROM tbl_companies t JOIN tbl_companies_meta cm ON cm.id = t.fk_companies_meta WHERE t.id = :id");
							$this->db->bind($query_title_company, ":id", $array->value);
							$array->value = $this->db->single($query_title_company)->company_name;
						}

						$custom[$key->title] = $array->value;

					}
					$childMenu.='<tr level="'.($level+1).'" mid="'.$current->id.'" url="'.URLROOT.getUrl($current->id).'" url_linv="'.URLROOT.getUrl($current->id, $linv).'" class="firstchild  Level_'.($level+1).'">';

					$child_check = $this->db->query("SELECT * FROM tbl_products WHERE parent = :id ");
					$this->db->bind($child_check,":id", $current->id);
					$child_check = $this->db->single($child_check);
					if(!empty($child_check)){
						$child_bool = true;
					}

					$i = 0;
					$level_i ="";
					$level_i .= '<img border="0"  src="images/icon1.png"><img border="0"  src="images/icon4.png">';
					$childMenu.= '<td level="'.($level+1).'"   >';
					$childMenu.= '<input type="text" name="order[]" style="text-align:center;width:30px !important;background:black;color:white;" value="'.$current->order.'"><input type="hidden" name="id[]" style="width:15px !important;" value="'.$current->id.'"> ';

					$bold=true;


					$rights = $this->db->query('SELECT * FROM tess_controller WHERE id = :rights ');
					$this->db->bind($rights,':rights', $current->type);
					$rights = $this->db->resultSet($rights);
					if(isset($rights[0]))
						$rights = $rights[0];

					$childMenu.='</td>
                    <td>
                        <div style="    height: 40px;
                        width: 40px;
                        display: table;
                        background: rgb(0, 0, 0);
                        overflow: hidden;
                        border-radius: 50%;
                        /* border: 1px solid rgba(0,0,0,0.2); */
                        margin: 0 auto;">
                            <div style="display:table-cell;vertical-align:middle">
                                <img src="'.DragonHide(URLROOT.'image.php?path='.($current->image ? $current->image : "default-avatar.png")).'" width="40px"  />
                            </div>
                        </div>
                    </td>
                    <td  class="title_table_list">';

					if ($_SESSION['status'] <= $rights->mod or $_SESSION['status']=="1")
						$childMenu.= '<div class="subcat">'.file_get_contents('../images/sub.svg').'</div><a style="line-height: 1.3" data-toggle="tooltip" data-placement="top"  data-original-title="ID: ' . $current->id . ' & Parent: ' . $current->parent . '"  data-description="'.$rights->desc.'" href="index.php?cp=page&mod=addmod&id=' . $current->id . '&parent='.$current->parent.'">';
					$childMenu.= $bold ? "<b style='font-weight:800;'>" : "";
					if($current->{'title'.$l} != ''){
						$childMenu .= $current->{'title'.$l};
					}
					else{
						$childMenu .= '(Sans titre)';
					}
					$childMenu.= $bold ? "</b>" : "";
					if ($_SESSION['status'] <= $rights->mod or $_SESSION['status']=="1")
						$childMenu.= '</a>';

					$childMenu.='</td>';
					$childMenu.='</td>';
					//Déroulement
					$childMenu.='<td style="width:76px">';

					if($child_bool){
						if(isset($_SESSION['pages-actives']) && $_SESSION['pages-actives'] == "actif"){
							$childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler-product page active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fa fa-arrow-alt-square-right arrows_menu"></i> </a>';
							$look_for_child = true;
						} else if(isset($_SESSION['pages-actives']) && $_SESSION['pages-actives'] == "inactif") {
							$childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler-product page" style="cursor:pointer;" data-tooltip="Ouvrir la catégorie"><button type="button" rel="tooltip" class="btn btn-warning" data-original-title="" title="">
                                            <i class="fa fa-arrow-alt-square-down arrows_menu"></i>
                                        </button></a>';
							$look_for_child = false;
						} else if(3 <= $level) {
							$childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler-product page" style="cursor:pointer;" data-tooltip="Ouvrir la catégorie">
                                            <i class="fa fa-arrow-alt-square-right arrows_menu"></i> </a>';
							$look_for_child = false;
						} else if(3  > $level) {
							$childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler-product page active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fa fa-arrow-alt-square-right arrows_menu"></i> </a>';
							$look_for_child = true;
						}else {
							$childMenu.= 'noob';
						}
					}
					$child_bool = false;
					$childMenu.= '</td> ';
					$childMenu.= '
                                <td>'.$custom['prix'].'</td>
                                <td>'.$custom['Quantité'].'</td>
                                <td>'.$custom['Compagnies'].'</td>
                                <td class="td-actions text-right">
                                    <div class="togglebutton">
                                        <label onclick="$(this).find(`input`).attr(`checked`);">
                                            <input name="online[]['.$current->id.']" value="0" type="hidden" />
                                            <input name="online[]['.$current->id.']" value="1" type="checkbox" '.($current->online == 1 ? 'checked=""' : '').'><span class="toggle"></span>
                                        </label>
                                    </div>
                                </td>
                                ';
					$childMenu.= '<td class="td-actions text-right"   >';
					$childMenu.= '<span class="td-actions text-right">
                                    ';
					if ($_SESSION['status'] <= $rights->add){
						if($rights->dtype != "") {
							$childMenu .=  '<a class="btn btn-info add_child" data-tooltip="Ajouter" dtype="'.$rights->dtype.'" parent="'.$current->id.'" href="index.php?cp=page&amp;mod=addmod&amp;ajout=1&amp;parent='.$current->id.'&amp;templates=""><i class="fas fa-plus-square"></i></a> ';
						}
					}
					if ($_SESSION['status'] <= $rights->mod or $_SESSION['status'] == "1") {
						$childMenu.= '<a class="btn btn-warning" data-toggle="tooltip" data-placement="top"  data-original-title="Type: ' . $current->type . '" title="Type: ' . $current->type . '" href="index.php?cp=page&mod=addmod&id=' . $current->id . '&parent='.$current->parent.'"><i class="fas fa-pen-square"></i><div class="ripple-container"></div></a> ';
					}
					if ($_SESSION['status'] <= $rights->del) {
						$childMenu.= '<a id="'.$current->id.'"  class="btn btn-danger delete-product"data-tooltip="Supprimer" ><i class="fas fa-times-square"></i></a> ';
					}
					$childMenu.='
                                    </span>';

					$childMenu.='</td>'."\n";
					$childMenu.= ' ';
					if($child_bool&&$look_for_child)

						$childMenu.= '</td>'."\n";
					$childMenu.="</tr>";
					$key++;
				}
				$_SESSION['pages-actives'][$id] = "actif";
				echo $childMenu;
			}catch(PDOException $e){
				return false;
			}

		}

		public function getMenu(){
			global $lang, $l;
			$lang = new Lang();
			$l = "";
			if($lang->lang == "en")
				$l="_en";
			$menu_array = [];
			$custom = [];
			$result = $this->getAll();
			$i=0;
			foreach($result as $row){

				$custom_inputs = $this->getAllCustoms($row->id);
				foreach($custom_inputs as $array){
					$get_type = $this->db->query("SELECT * FROM inputs WHERE id = :id");
					$this->db->bind($get_type, ":id", $array->type);
					$key = $this->db->single($get_type);

					if($key->title == "Compagnies"){
						$query_title_company = $this->db->query("SELECT cm.company_name FROM tbl_companies t JOIN tbl_companies_meta cm ON cm.id = t.fk_companies_meta WHERE t.id = :id");
						$this->db->bind($query_title_company, ":id", $array->value);
						$array->value = (isset($this->db->single($query_title_company)->company_name) ? $this->db->single($query_title_company)->company_name : "");
					}

					$custom += [$key->title => $array->value];

				}
				$menu_array[$row->id] = array('name' => $row->{'title'.$l},'parent' => $row->parent,'mid' => $row->id,'order' => $row->order, 'add' => $row->{'add'}, 'del' => $row->{'del'}, 'mod' => $row->{'mod'}, 'type' => $row->type, 'dtype' => $row->dtype, 'online' => $row->online, 'image' => $row->image, 'fk_gallery' => $row->fk_gallery);
				$menu_array[$row->id] += $custom;
				$i++;
			}
			return $menu_array;
		}

		public function update_orders($postData){
			$p = 0;
			foreach ($postData['id'] as $key => $value) {
				global $lang;
				if (isset($postData['order'][$p])){
					try{
						$query_update_orders = $this->db->query("UPDATE tbl_products SET `order` = :orders WHERE id= :id");
						$this->db->bind($query_update_orders,":orders", $postData['order'][$p]);
						$this->db->bind($query_update_orders,":id", $value);
						if($this->db->execute($query_update_orders)){
							$msg = $lang['edited_success'];
						}else{
							$msg =  $lang['edited_error'];
						}
					}catch(PDOException $e){
						return false;
					}
				}
				$p++;
			}
			return $msg;
		}

		public function update_onlines($postData){
			global $lang;
			foreach ($postData['online'] as $key => $value) {
				foreach($value as $key => $val){
					try{
						$query_update_online = $this->db->query("UPDATE tbl_products SET online= :onlines WHERE id = :id");
						$this->db->bind($query_update_online,":onlines", $val);
						$this->db->bind($query_update_online,":id", $key);
						if($this->db->execute($query_update_online)){
							$msg = $lang['edited_success'];
						}else{
							$msg =  $lang['edited_error'];
						}
					}catch(PDOException $e){
						return false;
					}
				}
			}
			return $msg;
		}
		//Search products
		public function seekInProductWebsite($id){
			try{
				$response = [];
				$query_get_product = $this->db->query("SELECT * FROM tbl_products WHERE id = :id ");
				$this->db->bind($query_get_product,":id", $id);
				$result_id = $this->db->resultSet($query_get_product);
				if($result_id){
					$response['from_id'] = $result_id;
				}
				$query_seek = $this->db->query("SELECT * FROM tbl_products WHERE
                    notes LIKE CONCAT('%',:id,'%') ||
                    sub_total LIKE CONCAT('%',:id,'%') ||
                    final_price LIKE CONCAT('%',:id,'%')
                    ");
				$this->db->bind($query_seek,":id", $id);
				$result_text = $this->db->resultSet($query_seek);
				if($result_text){
					$response['from_text'] = $result_text;
				}
				return $response;
			}catch(PDOException $e){
				return false;
			}
		}
		//DELETE Product
		public function DeleteProductWebsite($id){
			global $lang;
			$response = [];
			$response["text"] = "";
			$response["color"] = "";
			try{
				$query_delete_product = $this->db->query("DELETE FROM tbl_products WHERE id = :id");
				$this->db->bind($query_delete_product,":id", $id);
				if($this->db->execute($query_delete_product)){
					$query_delete_urlproduct = $this->db->query("DELETE FROM tbl_products_urls WHERE product_id = :id");
					$this->db->bind($query_delete_urlproduct,":id", $id);
					if($this->db->execute($query_delete_urlproduct)){
						$response["message"] = $lang['deleted_success'];
						$response["color"] = "success";
					}else{
						$response["message"] = $lang['deleted_error'];
						$response["color"] = "danger";
					}
				}else{

					$response["message"] = $lang['deleted_error'];
					$response["color"] = "danger";
				}
				return $response;
			}catch(PDOException $e){
				return false;
			}
		}
		public function updateProductInputs($id){
			try{
				$select_ids = $this->db->query("SELECT * FROM tbl_products_inputs WHERE fk_products = :id");
				$this->db->bind($select_ids, ":id", $id);
				$serialized = "";
				$this->db->execute($select_ids);
				$i = 0;
				while($inputs = $this->db->fetch($select_ids)){
					$serialized .= ($i != 0 ? '-' : '').$inputs->id;
					$i++;
				}
				$update = $this->db->query("UPDATE tbl_products SET custom_inputs = :data WHERE id = :id");
				$this->db->bind($update, ":data", $serialized);
				$this->db->bind($update, ":id", $id);
				return $this->db->execute($update);
			}catch(PDOException $e){
				return false;
			}
		}
		public function AddEditJoinInputs($key,$value,$product_id, $id=false){ // If id then edit
			if($id){
				$edit = $this->db->query("UPDATE tbl_products_inputs SET `value` = :value WHERE `type` = :type AND `fk_products` = :fk_products");
				$this->db->bind($edit,":value", $value);
				$this->db->bind($edit,":type", $key);
				$this->db->bind($edit,":fk_products", $product_id);
				if($this->db->execute($edit)){
					$this->updateProductInputs($product_id);
					return true;
				}else{
					return false;
				}
			}else{
				$add = $this->db->query("INSERT INTO tbl_products_inputs (`type`, `value`, `fk_products`) VALUES(:type, :value, :fk_products )");
				$this->db->bind($add,":type", $key);
				$this->db->bind($add,":value", $value);
				$this->db->bind($add,":fk_products", $product_id);
				if($this->db->execute($add)){
					$this->updateProductInputs($product_id);
					return true;
				}else{
					// var_dump('test2'.$this->db->errorInfo($add)[2]);
					return false;
				}
			}
		}
		//add or edit product
		public function AddEditProductWebsite($postData,$add=false){
			global $lang;
			if($add == false)
				unset($add);
			$response = [];
			$response["text"] = "";
			$response["color"] = "";


			$updates = "";
			$adds = "";
			$insert_sql1 = "";
			$insert_sql2 = "";

			/* custom inputs */
			$insert_inputs1 = "";
			$insert_inputs2 = "";
			$update_inputs = "";
			$can_insert = true;
			$add_customs = false;
			$binded = [];
			if($postData['title'] == "" || (!isset($postData['type'])))
				$can_insert = false;
			try{
				//Build string update
				foreach($postData as $key => $value){
					if ($key != 'B1' and $key != 'query'  and $key != 'supp'
															  and $key != 'url_id' and $key != 'url_id_en'
																					   and $key != 'edit_product' and $key != 'add_product' and $key != 'id_product' and $key != 'edit_ts'
					) {

						$key = str_replace('*', '', $key);

						//Get inputs details
						$query_inputs = $this->db->query("SELECT * FROM inputs where `name` = :key ");
						$this->db->bind($query_inputs,":key", $key);
						$inputs = $this->db->single($query_inputs);
						if($inputs){
							if ($inputs->type == "Upload Image") {

							}
							else if ($inputs->type == "Upload Autre") {

								if ($_FILES[$key]['size'] != "0") {

									// Move the file
									$rand = rand("10000", "__PRODUCTSID__0000"). "-";
									$value = $rand . nomValide($_FILES[$key]['name']);
									if (move_uploaded_file($_FILES[$key]['tmp_name'], URLIMG ."files/". $value)) {
										//uploaded
									}
								}
							}
							else if ($inputs->type == "Checkbox" || $inputs->type == "Cat&eacute;gorie" ) {
								$str = '';
								foreach($_POST[$key] as $current){
									if($current != "")
										$str .= "-$current-";
								}
								$value = $str;

							}
							else {
								//Nothing else yet
							}

							if($key == 'dtype' && is_array($_POST['dtype'])) {
								$value="";
								foreach($_POST['dtype'] as $page){
									if($i != 0){$value.= '-';}
									$value .= $page;
									$i++;
								}
							}
						}

						if($key !== "type" AND $key !== "parent"
											   AND $key !== "order"
												   AND $key !== "seo_meta_title"
													   AND $key !== "seo_meta_title_en"
														   AND $key !== "seo_meta_description"
															   AND $key !== "seo_meta_description_en"
																   AND $key !== "seo_meta_title_tag"
																	   AND $key !== "seo_meta_title_tag_en"
																		   AND $key !== "title"
																			   AND $key !== "title_en"
																				   AND $key !== "texte"
																					   AND $key !== "texte_en"
																						   AND $key !== "image"
																							   AND $key !== "alt"
																								   AND $key !== "is_product"
																									   AND $key !== "create_ts"
																										   AND $key !== "online"
																											   AND $key !== "custom_inputs"
						){
							if(isset($postData["id_product"])){
								if($this->checkIfExistJoinInputs($inputs->id, $postData["id_product"])){
									//edit
									$this->AddEditJoinInputs($inputs->id,$value,$postData["id_product"],$inputs->id);
								}else{
									//add
									$this->AddEditJoinInputs($inputs->id,$value,$postData["id_product"]);
								}
							}else{
								$add_customs = true;
							}

						}else{
							$updates .= "`$key`= :$key, ";
							$insert_sql1 .="`$key`, ";
							$insert_sql2 .=":$key, ";

							$binded[$key] = ( isset($value) ? $value : 1);
						}
					}

				}
				// var_dump($updates);
				//Delete last comma
				$updates = substr($updates, 0, -2);

				//updates
				if(!isset($add)){
					$update = $this->db->query("UPDATE tbl_products SET ".$updates.", edit_ts = :time WHERE id = :id");
					foreach($binded as $key => $value){
						$this->db->bind($update,":$key", $value);
					}
					$this->db->bind($update,":time", time());
					$this->db->bind($update,":id", $postData["id_product"]);
					if($this->db->execute($update)){
						if(isset($postData['url_id'])){
							$upd = "";
							$add1 = "";
							$add2 = "";

							if(MULTI_LANG){
								foreach(LANGUAGES as $l){
									if($l != "fr"){
										$upd .= ", `url_id_".$l."` = :url_id_".$l."";
										$add1 .= ", `url_id_".$l."`";
										$add2 .= ", :url_id_".$l."";
									}
								}
							}
							if($this->checkIfUrlExist($postData['id_product'])){
								$query_url = $this->db->query("UPDATE tbl_products_urls SET url_id = :url_id ". $upd." WHERE product_id = :product_id");
							}else{
								$query_url = $this->db->query("INSERT INTO tbl_products_urls (`product_id`, `url_id` ". $add1.") VALUES ( :product_id, :url_id ".$add2.")");
							}
							$this->db->bind($query_url,":url_id", $postData['url_id']);
							if(MULTI_LANG){
								foreach(LANGUAGES as $l){
									if($l != "fr"){
										$this->db->bind($query_url,":url_id_".$l, $postData['url_id_'.$l]);
									}
								}
							}
							$this->db->bind($query_url,":product_id", $postData["id_product"]);
							if($this->db->execute($query_url)){
							}else{
								$response["message"] = "URL couldn be added";
								$response["color"] = "danger";
							}

						}
						$response["message"] = $lang['edited_success'];
						$response["color"] = "success";
					}else{
						$response["message"] = $lang['edited_error'];
						$response["color"] = "danger";
					}
				}else{
					//ADD
					if($can_insert){
						// var_dump($postData);
						$order = $this->db->query("SELECT `order` FROM tbl_products WHERE parent= :parent order by `order` desc limit 1");
						$this->db->bind($order,":parent", (isset($postData['parent']) ? $postData['parent'] : 0));
						$order = $this->db->single($order);

						if(!isset($order->order) || is_null($order->order)){
							$lo = 1;
						}else{
							$lo = (1 + $order->order);
						}
						// var_dump("insert ignore into tbl_pages ($insert_sql1, `order`, `create_ts`) VALUES ($insert_sql3 '$lo', '".time()."')");
						$adds = $this->db->query("insert ignore into tbl_products ($insert_sql1 `order`, `create_ts`) VALUES ($insert_sql2 '$lo', '".time()."')");
						foreach($binded as $key => $value){
							$this->db->bind($adds,":$key", $value);
						}
						if($this->db->execute($adds)){
							if(isset($postData['url_id'])){
								$current_page_id = $this->db->query("SELECT LAST_INSERT_ID() as 'lastid' FROM tbl_products");
								$current_page_id = $this->db->single($current_page_id);
								if($current_page_id->lastid !== 0){
									$current_page_id = $current_page_id->lastid;
								}
								if($add_customs){
									$this->AddEditJoinInputs($inputs->id,$value,$current_page_id);
								}
								if(MULTI_LANG){
									$insert1 = "";
									$insert2 = "";
									$insert3 = array();
									foreach(LANGUAGES as $l){
										if($l != "fr"){
											$insert1 .= ", url_id_".$l;
											$insert2 .= ", :url_id_".$l;
										}
									}

									$insert_url = $this->db->query("INSERT INTO tbl_products_urls(product_id, url_id ".$insert1.") VALUES( :product_id, :url_id". $insert2.")");
									$this->db->bind($insert_url,":product_id", $current_page_id);
									$this->db->bind($insert_url,":url_id", $postData['url_id']);
									foreach(LANGUAGES as $l){
										if($l != "fr"){
											$this->db->bind($insert_url,":url_id_".$l, $postData['url_id_'.$l]);
										}
									}
									if($this->db->execute($insert_url)){
										$response["message"] = $lang['added_success'];
										$response["color"] = "success";
									}else{
										$response["message"] = $lang['added_error'];
										$response["color"] = "danger";
									}

								}else{
									$insert_url = $this->db->query("INSERT INTO tbl_products_urls(product_id, url_id) VALUES( :product_id, :url_id)");
									$this->db->bind($insert_url,":product_id", $current_page_id);
									$this->db->bind($insert_url,":url_id", $postData['url_id']);
									if($this->db->single($insert_url)){
										$response["message"] = $lang['added_success'];
										$response["color"] = "success";
									}else{
										$response["message"] = $lang['added_error'];
										$response["color"] = "danger";
									}

								}
							}else{
								$response["message"] = $lang['added_success'];
								$response["color"] = "success";
							}
						}else{
							$response["message"] = $lang['added_error'];
							$response["color"] = "danger";
						}
					}else{
						$response["message"] = $lang['added_error'];
						$response["color"] = "danger";
					}
				}
				return $response;
			}catch(PDOException $e){
				return false;
			}
		}

		//search product from search bar
		public function filterProductsSearching($filtering=""){
			$filtres = "";
			$filtres2 = "";
 			$filtres2 .= " value LIKE '%".$filtering."%'";
 			$filtres .= " vx LIKE '%".$filtering."%'";
			$query = "
				SELECT
					prod.*,
					tbl.price,
				    tbl.fk_products,
				    Group_concat(value) as 'vx'
				FROM
				    (SELECT
				        a.fk_products, t2.price,
				        Concat(Group_concat(value)) AS value
				    FROM tbl_products_inputs a
					LEFT JOIN (
						SELECT fk_products, (SPLIT_STRING(SPLIT_STRING(value,'||',3),'**',1) * SPLIT_STRING(SPLIT_STRING(value,'||',1),' ',1)) as price from tbl_products_inputs WHERE type = 40
					)as t2
					ON t2.fk_products = a.fk_products
					LEFT JOIN tbl_products as prod
					ON prod.id = a.fk_products
				     WHERE (
  					".$filtres2.")
					AND prod.id is not null
					".(isset($minprice) ? ' AND t2.price >= '.$minprice.'' : '')."
					".(isset($maxprice) ? ' AND t2.price <= '.$maxprice.'' : '')."
				    GROUP  BY a.fk_products, a.value) tbl
					LEFT JOIN tbl_products prod ON prod.id = tbl.fk_products
				GROUP BY tbl.fk_products
				HAVING (
					".$filtres.")

			";


			//echo($query);
			$db = $this->db;
			$query_products = $db->query($query);
			$db->execute($query_products);
			$countedProducts = $db->rowCount($query_products);

			if ($countedProducts > 0) {
				$i = 0;
				while ($product = $db->fetch($query_products)) {
					//var_dump($product);
					$join_product = $db->query("SELECT t.*, i.name FROM tbl_products_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_products = :id_product");
					$db->bind($join_product, ":id_product", $product->id);
					$db->execute($join_product);
					while ($joins = $db->fetch($join_product)) {
						if(isset($joins->name) && $joins->name != "")
							$product->{$joins->name} = $joins->value;
					}
					$parent_product = $db->query("SELECT * FROM tbl_products WHERE id = :parent");
					$db->bind($parent_product, ":parent", $product->parent);
					$parent = $db->single($parent_product);
					//getDetails

					//gather boxes;
					$arrayCaisse = explode("**",$product->quantity_box);
					$options="";
					$i=0;
					$alreadyOneWithQty = false;
					foreach($arrayCaisse as $caisse){
						$title = explode("||", $caisse)[0];
						$quantity = explode("||", $caisse)[1];
						$price = explode("||", $caisse)[2];
						$is12or6 = explode(" ", $title)[0];
						if(!$alreadyOneWithQty && $quantity > 0){
							$fprice = number_format(($is12or6*$price),2,".","");
							$ftitle = $title;
							$fqty = $quantity;
							$selected="selected";
							$alreadyOneWithQty = true;
						}else{

							$selected="";
						}
						if(!$alreadyOneWithQty && $caisse != "" && $quantity == 0 && ($i+1) == (count($arrayCaisse)-1)){
							$ftitle = $title;
							$selected="selected";
						}
						if($caisse != ""){
							// $options .= '<option value="'.$title.'" data-price="'.number_format(($is12or6*$price),2,".","").'" data-qty="'.$quantity.'">'.$title.'</option>';
							$options .= '<span value="'.$title.'" data-value="'.$title.'" class="custom-option '.$selected.'"  data-price="'.number_format(($is12or6*$price),2,".","").'" data-qty="'.$quantity.'">'.$title.'</span>';
						}
						$i++;
					}
					if(!$alreadyOneWithQty){
						$fqty = 0;
					}
					$i=0;
					// $inc = rand(8, 20) / 10;
					if(($i % 4 == 0 && $i > 2) || $i == 0){
						$alpha = "alpha";
						$clear = false;
						$omega = "";
					}elseif($i % 4 == 3 && $i > 2){

						$clear = true;
						$alpha = "";
						$omega = "omega";
					}else{
						$clear = false;
						$alpha = "";
						$omega = "";
					}
					if(($i % 3 == 0 && $i > 2) || $i == 0){
						$alpha1600 = "alpha_1600";
						$clear1600 = false;
						$omega1600 = "";
					}elseif($i % 3 == 2 && $i > 1){

						$clear1600 = true;
						$alpha1600 = "";
						$omega1600 = "omega_1600";
					}else{
						$clear1600 = false;
						$alpha1600 = "";
						$omega1600 = "";
					}
					if(($i % 2 == 0 && $i > 1) || $i == 0){
						$alpha1400 = "alpha_1400";
						$clear1400 = false;
						$omega1400 = "";
					}elseif($i % 2 == 1 && $i > 0){

						$clear1400 = true;
						$alpha1400 = "";
						$omega1400 = "omega_1400";
					}else{
						$clear1400 = false;
						$alpha1400 = "";
						$omega1400 = "";
					}
					// var_dump($product->pays);
					switch($product->pays){
						case "France":
							$flag = "fr";
						break;
						case "États-Unis":
							$flag = "us";
						break;
						case "Canada":
							$flag = "ca";
						break;
					}
					echo '
					<div class=" ' . ($i == 0 ? "scroll_here" : "") . ' '.$alpha.'  '.$alpha1600.' '.$alpha1400.' grid_3  grid_1600_4 grid_1400_6 grid_1024_12 fadeIn wow '.$omega.' '.$omega1600.' '.$omega1400.'" data-wow-duration="1s" data-wow-delay="0s">
						<div >
							<div class="item" style="-webkit-transform: translate3d(0, 0, 0) !important;">
								<div class="head">
									<div class="title_flag">
										<h4>
										  ' . $product->{'title' . $l} .'
										</h4>
										<div class="flag">
											<span class="flag-icon flag-icon-'.($flag).'"></span>
										</div>
									</div>
									<div class="price_item">
										<h5>
										' . number_format((float) (isset($product->price) ? $product->price :  0), 2, '.', '.') . ' $ CAD
										</h5>
									</div>
								</div>
								<div class="img_overlay">
									<img class="d-block w-100" src="' . (URLROOT . 'upload/'.SITENAME.'/' . $product->image) . '" alt="Le héroique">
									<div class="overlay tablewine">
										<div class="add_cart_wrapper">
											<div class="wrap_input_caisse">
												<label for="choixcaisse">Choix de caisse</label>
												<div class="custom-select-wrapper">
													<div class="custom-select">
														<div class="custom-select__trigger"><span>'.$ftitle.'</span>
															<div class="arrow"></div>
														</div>
														<div class="custom-options">
															'.$options.'
														</div>
													</div>
												</div>
											</div>
											<div class="load_stock">
											'.($fqty < 1 ? '<span class="outofstock">Out of stock</span><div class="wrap_input_qty hide">
												<label for="qtycaisse">Quantités</label>
												<div class="quantity">
													<a class="btn btn-custom qty_decrease">-</a>
													<input class="quantity_input" type="number" min="1" max="'.$fqty.'" disabled value="1" >
													<a class="btn btn-custom qty_increase">+</a>
												</div>
											</div>' : '<span class="hide outofstock">Out of stock</span>
											<div class="wrap_input_qty">
												<label for="qtycaisse">Quantités</label>
												<div class="quantity">
													<a class="btn btn-custom qty_decrease">-</a>
													<input class="quantity_input" type="number" min="1" max="'.$fqty.'" disabled value="1" >
													<a class="btn btn-custom qty_increase">+</a>
												</div>
											</div>
											<span class="newPrice hide">'.$fprice.'</span>
											<a href="#" data-img="'.URLROOT.'upload/'.SITENAME.'/'.$product->image.'"
											data-product-id="'.$product->id.'" class="add_to_cart blue-btn" style="
												margin: 5% auto;
												display: table;
												width: 100%;
												text-align: center;
											">Ajouter au panier</a>
											').'</div>
										</div>
									</div>
								</div>
								<div class="bottom">
									<div class="add">
										<i class="fal fa-plus-circle"></i>
									</div>
									<div class="arrow_wrapper">
										<div class="table">
											<a href="'.URLROOT.getUrlLangProducts($product->id).'">
											<div class="details">Détails</div>
											<div class="arrow">
												<i class="fal fa-long-arrow-right"></i>
											</div></a>
										</div>
									</div>
							   </div>
								</div>
						</div>
					</div>
					';
					 if($clear){

						echo "<div class='clear clear_1800 clear_1700'></div>";
					}
					if($clear1600){

						echo "<div class='clear_1600 clear_1500'></div>";
					}
					if($clear1400){

						echo "<div class='clear_1400 clear_1300 clear_1200 clear_1100'></div>";
					}

					if($i == $countedProducts){
						echo "<div class='clear'></div>";
					}
					// echo "<div class='clear_1024'></div>";
					$i++;
				}

			} else {
				echo '<p>Il n\'y a plus aucun produit pour le moment, revenez plus tard</p>';
			}
		}

		public function filterProducts($postdata){
			$filtres = "";
			$filtres2 = "";

			$normalQuery = false;
			UNSET($_SESSION['filter']);

			//var_dump(trim($postdata['filtering']));
			if(trim($postdata['filtering']) == ""){
				$normalQuery = true;
			}
			if(isset($postdata['minprice'])){
				$minprice = $postdata['minprice'];
				$_SESSION['filter']['min_price'][$minprice] = "1";
			}
			if(isset($postdata['maxprice'])){
				$maxprice = $postdata['maxprice'];
				$_SESSION['filter']['max_price'][$maxprice] = "1";
			}
			$postdata = explode(',', $postdata['filtering']);
			//var_dump(sizeOf($postdata['filtering']));

			$i = 0;
			$checked = array();

			foreach($postdata as $key){
				$key = str_replace("&#039;","'",$key);
				$key = str_replace("'","\"",$key);
				$key = unserialize($key);
				foreach($key as $value => $val){
					if(!$checked[$value]){

						$checked[$value] =0;
						if($i >0){

							$filtres.=")";
							$filtres2.=")";
						}
					}
					if( sizeof($postdata[$value] > 1)){
						$checked[$value]++;
					}
					//var_dump('checked for: '.$value);
					//var_dump('count: '.$checked[$value]);
					//var_dump('Maxcount: '.print_r($key));
					if( $checked[$value] == 1 && sizeof($postdata[$value] > 1)){


						if($i > 0 && $checked[$value] > 0){

							$filtres .= " AND ( ";
							$filtres2 .= " OR ( ";


						}else{

							$filtres .= " ( ";
							$filtres2 .= " ( ";
						}
						//$normalQuery = true;
					}

					$andor = "OR";
					$_SESSION['filter'][$value][$val] = "1";

					if($checked[$value] > 1){
						$filtres .= " ".$andor." ";
						$filtres2 .= " ".$andor." ";

					}
					if($value == "producteurs"){
						$query_products = $this->db->query("SELECT id FROM tbl_pages WHERE parent = 56 AND title = '".$val."'");
						$this->db->execute($query_products);
						$countedProducts = $this->db->single($query_products)->id;
						$val = "-".$countedProducts."-";
					}

					if($value == "cepage"){
						$query_products = $this->db->query("SELECT id FROM tbl_pages WHERE parent = 96 AND title = '".$val."'");
						$this->db->execute($query_products);
						$countedProducts = $this->db->single($query_products)->id;
						$val = "-".$countedProducts."-";
					}
					$filtres .= " vx LIKE '%".$val."%'";
					if($value == 'producteurs' || $value == 'cepage'){


					$filtres2 .= " value LIKE '%".$val."%'";
					}else{

					$filtres2 .= " value = '".$val."'";
					}
					if(($i) == (sizeof($postdata)-1)){
						$filtres .= " ) ";
						$filtres2 .= " ) ";
					}
					$i++;
				}
			}
			if(!$normalQuery){
				$query = "
					SELECT
						prod.*,
						tbl.price,
					    tbl.fk_products,
					    Group_concat(value) as 'vx'
					FROM
					    (SELECT
					        a.fk_products, t2.price,
					        Concat(Group_concat(value)) AS value
					    FROM tbl_products_inputs a
						LEFT JOIN (
							SELECT fk_products, (SPLIT_STRING(SPLIT_STRING(value,'||',3),'**',1) * SPLIT_STRING(SPLIT_STRING(value,'||',1),' ',1)) as price from tbl_products_inputs WHERE type = 40
						)as t2
						ON t2.fk_products = a.fk_products
					     WHERE (
	  					".$filtres2.")
						".(isset($minprice) ? ' AND t2.price >= '.$minprice.'' : '')."
						".(isset($maxprice) ? ' AND t2.price <= '.$maxprice.'' : '')."
					    GROUP  BY a.fk_products, a.value) tbl
						LEFT JOIN tbl_products prod ON prod.id = tbl.fk_products
					GROUP BY tbl.fk_products
					HAVING (
 					".$filtres.")

				";

			}else{
				$query = "
				SELECT
					prod.*,
					tbl.price,
					tbl.fk_products,
					Group_concat(value) as 'vx'
				FROM
					(SELECT
						a.fk_products, t2.price,
						Concat(Group_concat(value)) AS value
					FROM tbl_products_inputs a
					LEFT JOIN (
						SELECT fk_products, (SPLIT_STRING(SPLIT_STRING(value,'||',3),'**',1) * SPLIT_STRING(SPLIT_STRING(value,'||',1),' ',1)) as price from tbl_products_inputs WHERE type = 40
					)as t2
					ON t2.fk_products = a.fk_products
					".(isset($minprice) ? 'WHERE t2.price >= '.$minprice.'' : '')."
					".(isset($maxprice) ? ' AND t2.price <= '.$maxprice.'' : '')."
					GROUP  BY a.fk_products, a.value) tbl
					LEFT JOIN tbl_products prod ON prod.id = tbl.fk_products
					WHERE prod.online=1 AND prod.parent != 0
				GROUP BY tbl.fk_products
				";


			}

			//echo($query);
			$db = $this->db;
			$query_products = $db->query($query);
			$db->execute($query_products);
			$countedProducts = $db->rowCount($query_products);

			if ($countedProducts > 0) {
				$i = 0;
				while ($product = $db->fetch($query_products)) {
					//var_dump($product);
					$join_product = $db->query("SELECT t.*, i.name FROM tbl_products_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_products = :id_product");
					$db->bind($join_product, ":id_product", $product->id);
					$db->execute($join_product);
					while ($joins = $db->fetch($join_product)) {
						if(isset($joins->name) && $joins->name != "")
							$product->{$joins->name} = $joins->value;
					}
					$parent_product = $db->query("SELECT * FROM tbl_products WHERE id = :parent");
					$db->bind($parent_product, ":parent", $product->parent);
					$parent = $db->single($parent_product);
					//getDetails

					//gather boxes;
					$arrayCaisse = explode("**",$product->quantity_box);
					$options="";
					$i=0;
					$alreadyOneWithQty = false;
					foreach($arrayCaisse as $caisse){
						$title = explode("||", $caisse)[0];
						$quantity = explode("||", $caisse)[1];
						$price = explode("||", $caisse)[2];
						$is12or6 = explode(" ", $title)[0];
						if(!$alreadyOneWithQty && $quantity > 0){
							$fprice = number_format(($is12or6*$price),2,".","");
							$ftitle = $title;
							$fqty = $quantity;
							$selected="selected";
							$alreadyOneWithQty = true;
						}else{

							$selected="";
						}
						if(!$alreadyOneWithQty && $caisse != "" && $quantity == 0 && ($i+1) == (count($arrayCaisse)-1)){
							$ftitle = $title;
							$selected="selected";
						}
						if($caisse != ""){
							// $options .= '<option value="'.$title.'" data-price="'.number_format(($is12or6*$price),2,".","").'" data-qty="'.$quantity.'">'.$title.'</option>';
							$options .= '<span value="'.$title.'" data-value="'.$title.'" class="custom-option '.$selected.'"  data-price="'.number_format(($is12or6*$price),2,".","").'" data-qty="'.$quantity.'">'.$title.'</span>';
						}
						$i++;
					}
					if(!$alreadyOneWithQty){
						$fqty = 0;
					}
					$i=0;
					// $inc = rand(8, 20) / 10;
					if(($i % 4 == 0 && $i > 2) || $i == 0){
						$alpha = "alpha";
						$clear = false;
						$omega = "";
					}elseif($i % 4 == 3 && $i > 2){

						$clear = true;
						$alpha = "";
						$omega = "omega";
					}else{
						$clear = false;
						$alpha = "";
						$omega = "";
					}
					if(($i % 3 == 0 && $i > 2) || $i == 0){
						$alpha1600 = "alpha_1600";
						$clear1600 = false;
						$omega1600 = "";
					}elseif($i % 3 == 2 && $i > 1){

						$clear1600 = true;
						$alpha1600 = "";
						$omega1600 = "omega_1600";
					}else{
						$clear1600 = false;
						$alpha1600 = "";
						$omega1600 = "";
					}
					if(($i % 2 == 0 && $i > 1) || $i == 0){
						$alpha1400 = "alpha_1400";
						$clear1400 = false;
						$omega1400 = "";
					}elseif($i % 2 == 1 && $i > 0){

						$clear1400 = true;
						$alpha1400 = "";
						$omega1400 = "omega_1400";
					}else{
						$clear1400 = false;
						$alpha1400 = "";
						$omega1400 = "";
					}
					// var_dump($product->pays);
					switch($product->pays){
						case "France":
							$flag = "fr";
						break;
						case "États-Unis":
							$flag = "us";
						break;
						case "Canada":
							$flag = "ca";
						break;
					}
					echo '
					<div class=" ' . ($i == 0 ? "scroll_here" : "") . ' '.$alpha.'  '.$alpha1600.' '.$alpha1400.' grid_3  grid_1600_4 grid_1400_6 grid_1024_12 fadeIn wow '.$omega.' '.$omega1600.' '.$omega1400.'" data-wow-duration="1s" data-wow-delay="0s">
						<div >
							<div class="item" style="-webkit-transform: translate3d(0, 0, 0) !important;">
								<div class="head">
									<div class="title_flag">
										<h4>
										  ' . $product->{'title' . $l} .'
										</h4>
										<div class="flag">
											<span class="flag-icon flag-icon-'.($flag).'"></span>
										</div>
									</div>
									<div class="price_item">
										<h5>
										' . number_format((float) (isset($product->price) ? $product->price :  0), 2, '.', '.') . ' $ CAD
										</h5>
									</div>
								</div>
								<div class="img_overlay">
									<img class="d-block w-100" src="' . (URLROOT . 'upload/'.SITENAME.'/' . $product->image) . '" alt="Le héroique">
									<div class="overlay tablewine">
										<div class="add_cart_wrapper">
											<div class="wrap_input_caisse">
												<label for="choixcaisse">Choix de caisse</label>
												<div class="custom-select-wrapper">
													<div class="custom-select">
														<div class="custom-select__trigger"><span>'.$ftitle.'</span>
															<div class="arrow"></div>
														</div>
														<div class="custom-options">
															'.$options.'
														</div>
													</div>
												</div>
											</div>
											<div class="load_stock">
											'.($fqty < 1 ? '<span class="outofstock">Out of stock</span><div class="wrap_input_qty hide">
												<label for="qtycaisse">Quantités</label>
												<div class="quantity">
													<a class="btn btn-custom qty_decrease">-</a>
													<input class="quantity_input" type="number" min="1" max="'.$fqty.'" disabled value="1" >
													<a class="btn btn-custom qty_increase">+</a>
												</div>
											</div>' : '<span class="hide outofstock">Out of stock</span>
											<div class="wrap_input_qty">
												<label for="qtycaisse">Quantités</label>
												<div class="quantity">
													<a class="btn btn-custom qty_decrease">-</a>
													<input class="quantity_input" type="number" min="1" max="'.$fqty.'" disabled value="1" >
													<a class="btn btn-custom qty_increase">+</a>
												</div>
											</div>
											<span class="newPrice hide">'.$fprice.'</span>
											<a href="#" data-img="'.URLROOT.'upload/'.SITENAME.'/'.$product->image.'"
											data-product-id="'.$product->id.'" class="add_to_cart blue-btn" style="
												margin: 5% auto;
												display: table;
												width: 100%;
												text-align: center;
											">Ajouter au panier</a>
											').'</div>
										</div>
									</div>
								</div>
								<div class="bottom">
									<div class="add">
										<i class="fal fa-plus-circle"></i>
									</div>
									<div class="arrow_wrapper">
										<div class="table">
											<a href="'.URLROOT.getUrlLangProducts($product->id).'">
											<div class="details">Détails</div>
											<div class="arrow">
												<i class="fal fa-long-arrow-right"></i>
											</div></a>
										</div>
									</div>
							   </div>
								</div>
						</div>
					</div>
					';
					 if($clear){

						echo "<div class='clear clear_1800 clear_1700'></div>";
					}
					if($clear1600){

						echo "<div class='clear_1600 clear_1500'></div>";
					}
					if($clear1400){

						echo "<div class='clear_1400 clear_1300 clear_1200 clear_1100'></div>";
					}

					if($i == $countedProducts){
						echo "<div class='clear'></div>";
					}
					// echo "<div class='clear_1024'></div>";
					$i++;
				}

			} else {
				echo '<p>Il n\'y a plus aucun produit pour le moment, revenez plus tard</p>';
			}

		}

		public function filterByCateg($id){
			global $lang, $l;
			$html = "";
			if($id=="all"){
				$query_products = $this->db->query("SELECT * FROM tbl_products WHERE online = 1 AND parent != 0 ORDER BY `id` DESC");
			}else{
				$query_products = $this->db->query("SELECT * FROM tbl_products WHERE online = 1 AND parent = :id ORDER BY `id` DESC");
				$this->db->bind($query_products,":id", $id);
			}
			$this->db->execute($query_products);
			$countedProducts = $this->db->rowCount($query_products);

			if ($countedProducts > 0) {
				$i = 0;
				while ($product = $this->db->fetch($query_products)) {
					$join_product = $this->db->query("SELECT t.*, i.name FROM tbl_products_inputs t LEFT JOIN inputs i ON i.id = t.type WHERE t.fk_products = :id_product");
					$this->db->bind($join_product, ":id_product", $product->id);
					$this->db->execute($join_product);
					while ($joins = $this->db->fetch($join_product)) {
						if(isset($joins->name) && $joins->name != "")
							$product->{$joins->name} = $joins->value;
					}
					$parent_product = $this->db->query("SELECT * FROM tbl_products WHERE id = :parent");
					$this->db->bind($parent_product, ":parent", $product->parent);
					$parent = $this->db->single($parent_product);

					// $inc = rand(8, 20) / 10;
					$inc = 0.8;

					if($i > 5){
						// if($i==6){
							// echo '<div class="clear"></div>';}
							// $i = $i + 2;
						if (($i % 4 == 0) || $i == 6) {
							// echo '<div class="row">';
							// $side = 'alpha';
						} else if ($i % 4 == 3 || $i == $countedProducts) {
							// $side = 'omega';
						} else {
							// $side = "";
						}

					}else{


						if (($i % 3 == 0 && $i > 0)) {
							// echo '<div class="row">';
							// $side = 'alpha';
						} else if ($i % 3 == 2|| $i == $countedProducts) {
							// $side = 'omega';
						} else {
							// $side = "";
						}

					}if($i > 3){
						if (($i % 3 == 0 || $i == 7)) {
							// $side2 = 'alpha_1300';
						} else if ($i % 2 == 2 || $i == $countedProducts || $i == 6) {
							// $side2 = 'omega_1300';
						} else {
							// $side2 = "";
						}
					}else{
						if (($i % 2 == 0 && $i > 3)) {
							// $side2 = 'alpha_1300';
						} else if ($i % 2 == 2 || $i == $countedProducts) {
							// $side2 = 'omega_1300';
						} else {
							// $side2 = "";
						}
					}
					$html .= '
					<div class=" ' . ($i == 0 ? "scroll_here" : "") . ' grid_3 grid_1300_4 grid_1024_12 fadeIn wow" data-wow-duration="1s" data-wow-delay="0s">
						<div >
							<div class="item" style="-webkit-transform: translate3d(0, 0, 0) !important;">
								<h4 class="collection_title">
							   ' . $parent->{'title' . $l} . " : ".$i. '
								</h4>
								<img class="d-block w-100" src="' . URLROOT . 'image.php?path=' . str_replace(" ", "%20", $product->image) . '~-400x400" alt="Le héroique">
								<div class="carousel-caption  d-md-block">
								  <h4>
								  ' . $product->{'title' . $l} . '
								  </h4>
								  <h5>
								  ' . number_format((isset($product->price) ? $product->price :  0), 2, '.', '.') . ' $ CAD
								  </h5>

								  <div class="buttons">
									 <a href="' . getUrlLangProducts($product->id) . '" class="btn  btn-custom btn-round ">
									 ' . $lang['view_product'] . '
									 </a>
									 ' . ((isset($product->quantity) ? $product->quantity :  0) <= 0 ? '<br><span class="error">' . $lang['out_of_stock'] . '</span>' : '
									 <div class="quantity">
										<a class="btn btn-custom qty_decrease">-</a>
										<input class="quantity_input" type="number" min="1" max="' . (isset($product->quantity) ? $product->quantity :  0) . '" disabled value="1" >
										<a class="btn btn-custom qty_increase">+</a>
									 </div>
									 <a href="' . getUrlLangProducts($product->id) . '" data-product-id="' . $product->id . '" class="btn btn-primary btn-round add_to_cart ">
									 ' . $lang['add_cart'] . '
									 </a>') . '
									 <br>
								  </div>
							   </div>
								</div>
						</div>
					</div>
					';
					if($i > 5){
						if (($i % 4 == 3 ) || $i == 11) {
							// echo '</div>';

						}
						if ($i % 3 == 2|| $i == $countedProducts) {
							$html .= '<div class="clear_1300"></div>';
						}
					}else{
						if ($i % 3 == 2|| $i == $countedProducts) {
							// echo '</div>';
						}
						if ($i % 2 == 1) {
							$html .= '<div class="clear_1300"></div>';
						}
						// if($i == 5)
					}
					$i++;
				}

			} else {
				$html .= '<p>Il n\'y a plus aucun produit pour le moment, revenez plus tard</p>';
			}
			return $html;
		}
	}
