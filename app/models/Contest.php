<?php
	class Contest {
		private $db;
		
		public function __construct()
		{
			$this->db = new Database();
		}
		public function getPage($id){
			// var_dump($id);
			$getPage = $this->db->query("SELECT * FROM tbl_contests WHERE id = ".$id."");
			$all = $this->db->single($getPage);
			$allc = $this->getAllCustoms($all->id);
			if($this->db->rowCount($allc) > 0){
				while($cust = $this->db->fetch($allc)){
					
					if(isset($all->{$cust->name}) && $cust->name != "")
						$all->{$cust->name} = $cust->value;
				}
			}
			return $all;
			
			
		}
		
		public function checkIfExistJoinInputs($key, $fkcontests){
			$exist = $this->db->query("SELECT * FROM tbl_contests_inputs WHERE type = :key AND fk_contests = :fkcontests");
			$this->db->bind($exist, ":key", $key);
			$this->db->bind($exist, ":fkcontests", $fkcontests);
			if($this->db->single($exist)){
				return true;
			}else{
				return false;
			}
		}
		public function checkIfUrlExist($contest_id){
			$exist = $this->db->query("SELECT * FROM tbl_contests_urls WHERE contest_id = :contest_id");
			$this->db->bind($exist, ":contest_id", $contest_id);
			if($this->db->single($exist)){
				return true;
			}else{
				return false;
			}
		}
		public function checkIfModulesExist($inputid){
			$exist = $this->db->query("SELECT * FROM tess_controller WHERE id = :inputid");
			$this->db->bind($exist, ":inputid", $inputid);
			if($this->db->single($exist)){
				return true;
			}else{
				return false;
			}
		}
		public function getcontestWebsite($id){
			try{
				$query = $this->db->query("SELECT * FROM tbl_contests WHERE id = " .$id);
				return $this->db->resultSet($query);
			}catch(PDOException $e){
				return false;
			}
		}
		public function getAll(){
			try{
				$query_get_all = $this->db->query("SELECT tbl_contests.*, tess_controller.add, tess_controller.mod, tess_controller.del, tess_controller.dtype FROM tbl_contests LEFT JOIN tess_controller ON tbl_contests.type = tess_controller.id WHERE parent = 0");
				return $this->db->resultSet($query_get_all);
			}catch(PDOException $e){
				return false;
			}
		}
		public function getAllCustoms($id){
			try{
				$query_get_all = $this->db->query("SELECT * ,i.name as name FROM tbl_contests_inputs LEFT JOIN inputs i ON i.id = tbl_contests_inputs.type WHERE fk_contests = :id");
				$this->db->bind($query_get_all, ":id", $id);
				$this->db->execute($query_get_all);
				return $query_get_all;
			}catch(PDOException $e){
				return false;
			}
		}
		
		public function getChildList($id, $level){
			
			global $Tess_db, $linv, $Tess, $Functions, $install_page_order, $_SESSION, $l;
			
			require_once APPROOT.'/helpers/function.php';
			$lang = new Lang();
			$l = "";
			if($lang->lang == "en")
				$l="_en";
			
			try{
				$query = $this->db->query("SELECT * FROM tbl_contests WHERE parent = :id ORDER BY `order` ASC");
				$this->db->bind($query,":id",$id);
				$child_bool = false;
				$childMenu ='';
				$key=0;
				$currents = $this->db->resultSet($query);
				$custom = array();
				$custom['prix'] = "";
				$custom['Quantité'] = "";
				$custom['Compagnies'] = "";
				foreach($currents as $current){
					$custom['prix'] = "";
					$custom['Quantité'] = "";
					$custom['Compagnies'] = "";
					$custom_inputs = $this->getAllCustoms($current->id);
					
					
					foreach($custom_inputs as $array){
						$get_type = $this->db->query("SELECT * FROM inputs WHERE id = :id");
						$this->db->bind($get_type, ":id", $array->type);
						$key = $this->db->single($get_type);
						
						if($key->title == "Compagnies"){
							$query_title_company = $this->db->query("SELECT cm.company_name FROM tbl_companies t JOIN tbl_companies_meta cm ON cm.id = t.fk_companies_meta WHERE t.id = :id");
							$this->db->bind($query_title_company, ":id", $array->value);
							$array->value = $this->db->single($query_title_company)->company_name;
						}
						
						$custom[$key->title] = $array->value;
						
					}
					$childMenu.='<tr level="'.($level+1).'" mid="'.$current->id.'" url="'.URLROOT.getUrl($current->id).'" url_linv="'.URLROOT.getUrl($current->id, $linv).'" class="firstchild  Level_'.($level+1).'">';
					
					$child_check = $this->db->query("SELECT * FROM tbl_contests WHERE parent = :id ");
					$this->db->bind($child_check,":id", $current->id);
					$child_check = $this->db->single($child_check);
					if(!empty($child_check)){
						$child_bool = true;
					}
					
					$i = 0;
					$level_i ="";
					$level_i .= '<img border="0"  src="images/icon1.png"><img border="0"  src="images/icon4.png">';
					$childMenu.= '<td level="'.($level+1).'"   >';
					$childMenu.= '<input type="text" name="order[]" style="text-align:center;width:30px !important;background:black;color:white;" value="'.$current->order.'"><input type="hidden" name="id[]" style="width:15px !important;" value="'.$current->id.'"> ';
					
					$bold=true;
					
					
					$rights = $this->db->query('SELECT * FROM tess_controller WHERE id = :rights ');
					$this->db->bind($rights,':rights', $current->type);
					$rights = $this->db->resultSet($rights);
					if(isset($rights[0]))
						$rights = $rights[0];
					
					$childMenu.='</td>
                    <td>
                        <div style="    height: 40px;
                        width: 40px;
                        display: table;
                        background: rgb(0, 0, 0);
                        overflow: hidden;
                        border-radius: 50%;
                        /* border: 1px solid rgba(0,0,0,0.2); */
                        margin: 0 auto;">
                            <div style="display:table-cell;vertical-align:middle">
                                <img src="'.DragonHide(URLROOT.'image.php?path='.($current->image ? $current->image : "default-avatar.png")).'" width="40px"  />
                            </div>
                        </div> 
                    </td>
                    <td  class="title_table_list">';
					
					if ($_SESSION['status'] <= $rights->mod or $_SESSION['status']=="1")
						$childMenu.= '<div class="subcat">'.file_get_contents('../images/sub.svg').'</div><a style="line-height: 1.3" data-toggle="tooltip" data-placement="top"  data-original-title="ID: ' . $current->id . ' & Parent: ' . $current->parent . '"  data-description="'.$rights->desc.'" href="index.php?cp=page&mod=addmod&id=' . $current->id . '&parent='.$current->parent.'">';
					$childMenu.= $bold ? "<b style='font-weight:800;'>" : "";
					if($current->{'title'.$l} != ''){
						$childMenu .= $current->{'title'.$l};
					}
					else{
						$childMenu .= '(Sans titre)';
					}
					$childMenu.= $bold ? "</b>" : "";
					if ($_SESSION['status'] <= $rights->mod or $_SESSION['status']=="1")
						$childMenu.= '</a>';
					
					$childMenu.='</td>';
					$childMenu.='</td>';
					//Déroulement
					$childMenu.='<td style="width:76px">';
					
					if($child_bool){
						if(isset($_SESSION['pages-actives']) && $_SESSION['pages-actives'] == "actif"){
							$childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler-contest page active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fa fa-arrow-alt-square-right arrows_menu"></i> </a>';
							$look_for_child = true;
						} else if(isset($_SESSION['pages-actives']) && $_SESSION['pages-actives'] == "inactif") {
							$childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler-contest page" style="cursor:pointer;" data-tooltip="Ouvrir la catégorie"><button type="button" rel="tooltip" class="btn btn-warning" data-original-title="" title="">
                                            <i class="fa fa-arrow-alt-square-down arrows_menu"></i>
                                        </button></a>';
							$look_for_child = false;
						} else if(3 <= $level) {
							$childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler-contest page" style="cursor:pointer;" data-tooltip="Ouvrir la catégorie">
                                            <i class="fa fa-arrow-alt-square-right arrows_menu"></i> </a>';
							$look_for_child = false;
						} else if(3  > $level) {
							$childMenu.= '<a id="page-'.$current->id.'" class="btn-derouler-contest page active" style="cursor:pointer;" data-tooltip="Fermer la catégorie"><i class="fa fa-arrow-alt-square-right arrows_menu"></i> </a>';
							$look_for_child = true;
						}else {
							$childMenu.= 'noob';
						}
					}
					$child_bool = false;
					$childMenu.= '</td> ';
					$childMenu.= '
								<td>'.date("Y-m-d h:i:s", $current->create_ts).'</td>
                                <td class="td-actions text-right">
                                    <div class="togglebutton">
                                        <label onclick="$(this).find(`input`).attr(`checked`);">
                                            <input name="online[]['.$current->id.']" value="0" type="hidden" />
                                            <input name="online[]['.$current->id.']" value="1" type="checkbox" '.($current->online == 1 ? 'checked=""' : '').'><span class="toggle"></span>
                                        </label>
                                    </div>
                                </td>
                                ';
					$childMenu.= '<td class="td-actions text-right"   >';
					$childMenu.= '<span class="td-actions text-right">
                                    ';
					if ($_SESSION['status'] <= $rights->add){
						if($rights->dtype != "") {
							$childMenu .=  '<a class="btn btn-info add_child" data-tooltip="Ajouter" dtype="'.$rights->dtype.'" parent="'.$current->id.'" href="index.php?cp=page&amp;mod=addmod&amp;ajout=1&amp;parent='.$current->id.'&amp;templates=""><i class="fas fa-plus-square"></i></a> ';
						}
					}
					if ($_SESSION['status'] <= $rights->mod or $_SESSION['status'] == "1") {
						$childMenu.= '<a class="btn btn-warning" data-toggle="tooltip" data-placement="top"  data-original-title="Type: ' . $current->type . '" title="Type: ' . $current->type . '" href="index.php?cp=page&mod=addmod&id=' . $current->id . '&parent='.$current->parent.'"><i class="fas fa-pen-square"></i><div class="ripple-container"></div></a> ';
					}
					if ($_SESSION['status'] <= $rights->del) {
						$childMenu.= '<a id="'.$current->id.'"  class="btn btn-danger delete-contest"data-tooltip="Supprimer" ><i class="fas fa-times-square"></i></a> ';
					}
					$childMenu.='
                                    </span>';
					
					$childMenu.='</td>'."\n";
					$childMenu.= ' ';
					if($child_bool&&$look_for_child)
						
						$childMenu.= '</td>'."\n";
					$childMenu.="</tr>";
					$key++;
				}
				$_SESSION['pages-actives'][$id] = "actif";
				echo $childMenu;
			}catch(PDOException $e){
				return false;
			}
			
		}
		
		public function getMenu(){
			global $lang, $l;
			$lang = new Lang();
			$l = "";
			if($lang->lang == "en")
				$l="_en";
			$menu_array = [];
			$custom = [];
			$result = $this->getAll();
			$i=0;
			foreach($result as $row){
				
				$custom_inputs = $this->getAllCustoms($row->id);
				foreach($custom_inputs as $array){
					$get_type = $this->db->query("SELECT * FROM inputs WHERE id = :id");
					$this->db->bind($get_type, ":id", $array->type);
					$key = $this->db->single($get_type);
					
					if($key->title == "Compagnies"){
						$query_title_company = $this->db->query("SELECT cm.company_name FROM tbl_companies t JOIN tbl_companies_meta cm ON cm.id = t.fk_companies_meta WHERE t.id = :id");
						$this->db->bind($query_title_company, ":id", $array->value);
						$array->value = $this->db->single($query_title_company)->company_name;
					}
					
					$custom += [$key->title => $array->value];
					
				}
				$menu_array[$row->id] = array('name' => $row->{'title'.$l},'parent' => $row->parent,'mid' => $row->id,'order' => $row->order, 'add' => $row->{'add'}, 'del' => $row->{'del'}, 'mod' => $row->{'mod'}, 'type' => $row->type, 'dtype' => $row->dtype, 'online' => $row->online, 'image' => $row->image, 'date_created' => $row->create_ts);
				$menu_array[$row->id] += $custom;
				$i++;
			}
			return $menu_array;
		}
		
		public function update_orders($postData){
			$p = 0;
			foreach ($postData['id'] as $key => $value) {
				global $lang;
				if (isset($postData['order'][$p])){
					try{
						$query_update_orders = $this->db->query("UPDATE tbl_contests SET `order` = :orders WHERE id= :id");
						$this->db->bind($query_update_orders,":orders", $postData['order'][$p]);
						$this->db->bind($query_update_orders,":id", $value);
						if($this->db->execute($query_update_orders)){
							$msg = $lang['edited_success'];
						}else{
							$msg =  $lang['edited_error'];
						}
					}catch(PDOException $e){
						return false;
					}
				}
				$p++;
			}
			return $msg;
		}
		
		public function update_onlines($postData){
			global $lang;
			foreach ($postData['online'] as $key => $value) {
				foreach($value as $key => $val){
					try{
						$query_update_online = $this->db->query("UPDATE tbl_contests SET online= :onlines WHERE id = :id");
						$this->db->bind($query_update_online,":onlines", $val);
						$this->db->bind($query_update_online,":id", $key);
						if($this->db->execute($query_update_online)){
							$msg = $lang['edited_success'];
						}else{
							$msg =  $lang['edited_error'];
						}
					}catch(PDOException $e){
						return false;
					}
				}
			}
			return $msg;
		}
		//Search contests
		public function seekIncontestWebsite($id){
			try{
				$response = [];
				$query_get_contest = $this->db->query("SELECT * FROM tbl_contests WHERE id = :id ");
				$this->db->bind($query_get_contest,":id", $id);
				$result_id = $this->db->resultSet($query_get_contest);
				if($result_id){
					$response['from_id'] = $result_id;
				}
				$query_seek = $this->db->query("SELECT * FROM tbl_contests WHERE
                    notes LIKE CONCAT('%',:id,'%') ||
                    sub_total LIKE CONCAT('%',:id,'%') ||
                    final_price LIKE CONCAT('%',:id,'%')
                    ");
				$this->db->bind($query_seek,":id", $id);
				$result_text = $this->db->resultSet($query_seek);
				if($result_text){
					$response['from_text'] = $result_text;
				}
				return $response;
			}catch(PDOException $e){
				return false;
			}
		}
		//DELETE contest
		public function DeletecontestWebsite($id){
			global $lang;
			$response = [];
			$response["text"] = "";
			$response["color"] = "";
			try{
				$query_delete_contest = $this->db->query("DELETE FROM tbl_contests WHERE id = :id");
				$this->db->bind($query_delete_contest,":id", $id);
				if($this->db->execute($query_delete_contest)){
					$query_delete_urlcontest = $this->db->query("DELETE FROM tbl_contests_urls WHERE contest_id = :id");
					$this->db->bind($query_delete_urlcontest,":id", $id);
					if($this->db->execute($query_delete_urlcontest)){
						$response["message"] = $lang['deleted_success'];
						$response["color"] = "success";
					}else{
						$response["message"] = $lang['deleted_error'];
						$response["color"] = "danger";
					}
				}else{
					
					$response["message"] = $lang['deleted_error'];
					$response["color"] = "danger";
				}
				return $response;
			}catch(PDOException $e){
				return false;
			}
		}
		public function updatecontestInputs($id){
			try{
				$select_ids = $this->db->query("SELECT * FROM tbl_contests_inputs WHERE fk_contests = :id");
				$this->db->bind($select_ids, ":id", $id);
				$serialized = "";
				$this->db->execute($select_ids);
				$i = 0;
				while($inputs = $this->db->fetch($select_ids)){
					$serialized .= ($i != 0 ? '-' : '').$inputs->id;
					$i++;
				}
				$update = $this->db->query("UPDATE tbl_contests SET custom_inputs = :data WHERE id = :id");
				$this->db->bind($update, ":data", $serialized);
				$this->db->bind($update, ":id", $id);
				return $this->db->execute($update);
			}catch(PDOException $e){
				return false;
			}
		}
		public function AddEditJoinInputs($key,$value,$contest_id, $id=false){ // If id then edit
			if($id){
				$edit = $this->db->query("UPDATE tbl_contests_inputs SET `value` = :value WHERE `type` = :type AND `fk_contests` = :fk_contests");
				$this->db->bind($edit,":value", $value);
				$this->db->bind($edit,":type", $key);
				$this->db->bind($edit,":fk_contests", $contest_id);
				if($this->db->execute($edit)){
					$this->updatecontestInputs($contest_id);
					return true;
				}else{
					return false;
				}
			}else{
				$add = $this->db->query("INSERT INTO tbl_contests_inputs (`type`, `value`, `fk_contests`) VALUES(:type, :value, :fk_contests )");
				$this->db->bind($add,":type", $key);
				$this->db->bind($add,":value", $value);
				$this->db->bind($add,":fk_contests", $contest_id);
				if($this->db->execute($add)){
					$this->updatecontestInputs($contest_id);
					return true;
				}else{
					// var_dump('test2'.$this->db->errorInfo($add)[2]);
					return false;
				}
			}
		}
		//add or edit contest
		public function AddEditcontestWebsite($postData,$add=false){
			global $lang;
			if($add == false)
				unset($add);
			$response = [];
			$response["text"] = "";
			$response["color"] = "";
			
			
			$updates = "";
			$adds = "";
			$insert_sql1 = "";
			$insert_sql2 = "";
			
			/* custom inputs */
			$insert_inputs1 = "";
			$insert_inputs2 = "";
			$update_inputs = "";
			$can_insert = true;
			$add_customs = false;
			$binded = [];
			if($postData['title'] == "" || (!isset($postData['type'])))
				$can_insert = false;
			try{
				//Build string update
				foreach($postData as $key => $value){
					if ($key != 'B1' and $key != 'query'  and $key != 'supp'
															  and $key != 'url_id' and $key != 'url_id_en'
																					   and $key != 'edit_contest' and $key != 'add_contest' and $key != 'id_contest' and $key != 'edit_ts'
					) {
						
						$key = str_replace('*', '', $key);
						
						//Get inputs details
						$query_inputs = $this->db->query("SELECT * FROM inputs where `name` = :key ");
						$this->db->bind($query_inputs,":key", $key);
						$inputs = $this->db->single($query_inputs);
						if($inputs){
							if ($inputs->type == "Upload Image") {
							
							}
							else if ($inputs->type == "Upload Autre") {
								
								if ($_FILES[$key]['size'] != "0") {
									
									// Move the file
									$rand = rand("10000", "__PRODUCTSID__0000"). "-";
									$value = $rand . nomValide($_FILES[$key]['name']);
									if (move_uploaded_file($_FILES[$key]['tmp_name'], URLIMG ."files/". $value)) {
										//uploaded
									}
								}
							}
							else if ($inputs->type == "Checkbox" || $inputs->type == "Cat&eacute;gorie" ) {
								$str = '';
								foreach($_POST[$key] as $current){
									if($current != "")
										$str .= "-$current-";
								}
								$value = $str;
								
							}
							else {
								//Nothing else yet
							}
							
							if($key == 'dtype' && is_array($_POST['dtype'])) {
								$value="";
								foreach($_POST['dtype'] as $page){
									if($i != 0){$value.= '-';}
									$value .= $page;
									$i++;
								}
							}
						}
						
						if($key !== "type" AND $key !== "parent"
											   AND $key !== "order"
												   AND $key !== "seo_meta_title"
													   AND $key !== "seo_meta_title_en"
														   AND $key !== "seo_meta_description"
															   AND $key !== "seo_meta_description_en"
																   AND $key !== "seo_meta_title_tag"
																	   AND $key !== "seo_meta_title_tag_en"
																		   AND $key !== "title"
																			   AND $key !== "title_en"
																				   AND $key !== "texte"
																					   AND $key !== "texte_en"
																						   AND $key !== "image"
																							   AND $key !== "alt"
																								   AND $key !== "is_contest"
																									   AND $key !== "create_ts"
																										   AND $key !== "online"
																											   AND $key !== "custom_inputs"
						){
							if(isset($postData["id_contest"])){
								if($this->checkIfExistJoinInputs($inputs->id, $postData["id_contest"])){
									//edit
									$this->AddEditJoinInputs($inputs->id,$value,$postData["id_contest"],$inputs->id);
								}else{
									//add
									$this->AddEditJoinInputs($inputs->id,$value,$postData["id_contest"]);
								}
							}else{
								$add_customs = true;
							}
							
						}else{
							$updates .= "`$key`= :$key, ";
							$insert_sql1 .="`$key`, ";
							$insert_sql2 .=":$key, ";
							
							$binded[$key] = ( isset($value) ? $value : 1);
						}
					}
					
				}
				// var_dump($updates);
				//Delete last comma
				$updates = substr($updates, 0, -2);
				
				//updates
				if(!isset($add)){
					$update = $this->db->query("UPDATE tbl_contests SET ".$updates.", edit_ts = :time WHERE id = :id");
					foreach($binded as $key => $value){
						$this->db->bind($update,":$key", $value);
					}
					$this->db->bind($update,":time", time());
					$this->db->bind($update,":id", $postData["id_contest"]);
					if($this->db->execute($update)){
						if(isset($postData['url_id'])){
							$upd = "";
							$add1 = "";
							$add2 = "";
							
							if(MULTI_LANG){
								foreach(LANGUAGES as $l){
									if($l != "fr"){
										$upd .= ", `url_id_".$l."` = :url_id_".$l."";
										$add1 .= ", `url_id_".$l."`";
										$add2 .= ", :url_id_".$l."";
									}
								}
							}
							if($this->checkIfUrlExist($postData['id_contest'])){
								$query_url = $this->db->query("UPDATE tbl_contests_urls SET url_id = :url_id ". $upd." WHERE contest_id = :contest_id");
							}else{
								$query_url = $this->db->query("INSERT INTO tbl_contests_urls (`contest_id`, `url_id` ". $add1.") VALUES ( :contest_id, :url_id ".$add2.")");
							}
							$this->db->bind($query_url,":url_id", $postData['url_id']);
							if(MULTI_LANG){
								foreach(LANGUAGES as $l){
									if($l != "fr"){
										$this->db->bind($query_url,":url_id_".$l, $postData['url_id_'.$l]);
									}
								}
							}
							$this->db->bind($query_url,":contest_id", $postData["id_contest"]);
							if($this->db->execute($query_url)){
							}else{
								$response["message"] = "URL couldn be added";
								$response["color"] = "danger";
							}
							
						}
						$response["message"] = $lang['edited_success'];
						$response["color"] = "success";
					}else{
						$response["message"] = $lang['edited_error'];
						$response["color"] = "danger";
					}
				}else{
					//ADD
					if($can_insert){
						// var_dump($postData);
						$order = $this->db->query("SELECT `order` FROM tbl_contests WHERE parent= :parent order by `order` desc limit 1");
						$this->db->bind($order,":parent", (isset($postData['parent']) ? $postData['parent'] : 0));
						$order = $this->db->single($order);
						
						if(!isset($order->order) || is_null($order->order)){
							$lo = 1;
						}else{
							$lo = (1 + $order->order);
						}
						// var_dump("insert ignore into tbl_pages ($insert_sql1, `order`, `create_ts`) VALUES ($insert_sql3 '$lo', '".time()."')");
						$adds = $this->db->query("insert ignore into tbl_contests ($insert_sql1 `order`, `create_ts`) VALUES ($insert_sql2 '$lo', '".time()."')");
						foreach($binded as $key => $value){
							$this->db->bind($adds,":$key", $value);
						}
						if($this->db->execute($adds)){
							if(isset($postData['url_id'])){
								$current_page_id = $this->db->query("SELECT LAST_INSERT_ID() as 'lastid' FROM tbl_contests");
								$current_page_id = $this->db->single($current_page_id);
								if($current_page_id->lastid !== 0){
									$current_page_id = $current_page_id->lastid;
								}
								if($add_customs){
									$this->AddEditJoinInputs($inputs->id,$value,$current_page_id);
								}
								if(MULTI_LANG){
									$insert1 = "";
									$insert2 = "";
									$insert3 = array();
									foreach(LANGUAGES as $l){
										if($l != "fr"){
											$insert1 .= ", url_id_".$l;
											$insert2 .= ", :url_id_".$l;
										}
									}
									
									$insert_url = $this->db->query("INSERT INTO tbl_contests_urls(contest_id, url_id ".$insert1.") VALUES( :contest_id, :url_id". $insert2.")");
									$this->db->bind($insert_url,":contest_id", $current_page_id);
									$this->db->bind($insert_url,":url_id", $postData['url_id']);
									foreach(LANGUAGES as $l){
										if($l != "fr"){
											$this->db->bind($insert_url,":url_id_".$l, $postData['url_id_'.$l]);
										}
									}
									if($this->db->execute($insert_url)){
										$response["message"] = $lang['added_success'];
										$response["color"] = "success";
									}else{
										$response["message"] = $lang['added_error'];
										$response["color"] = "danger";
									}
									
								}else{
									$insert_url = $this->db->query("INSERT INTO tbl_contests_urls(contest_id, url_id) VALUES( :contest_id, :url_id)");
									$this->db->bind($insert_url,":contest_id", $current_page_id);
									$this->db->bind($insert_url,":url_id", $postData['url_id']);
									if($this->db->single($insert_url)){
										$response["message"] = $lang['added_success'];
										$response["color"] = "success";
									}else{
										$response["message"] = $lang['added_error'];
										$response["color"] = "danger";
									}
									
								}
							}else{
								$response["message"] = $lang['added_success'];
								$response["color"] = "success";
							}
						}else{
							$response["message"] = $lang['added_error'];
							$response["color"] = "danger";
						}
					}else{
						$response["message"] = $lang['added_error'];
						$response["color"] = "danger";
					}
				}
				return $response;
			}catch(PDOException $e){
				return false;
			}
		}
	}