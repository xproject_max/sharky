<?php
	class Order {
		private $db;
		
		public function __construct()
		{
			$this->db = new Database();
		}
		
		public function checkIfOrderExists($email, $id){
			$check = $this->db->query("
				SELECT o.* from tbl_orders o LEFT JOIN tbl_users u ON u.id = o.fk_users
				WHERE o.id = :id AND u.email = :email
			");
			$this->db->bind($check, ":id", $id);
			$this->db->bind($check, ":email", $email);
			if($this->db->single($check)){
				return true;
			}
			return false;
		}
		
		public function checkIfOrderExistsAndUnpaid($email, $id){
			$check = $this->db->query("
				SELECT o.* from tbl_orders o LEFT JOIN tbl_users u ON u.id = o.fk_users
				WHERE o.id = :id AND u.email = :email AND o.paid = 0
			");
			$this->db->bind($check, ":id", $id);
			$this->db->bind($check, ":email", $email);
			if($this->db->single($check)){
				return true;
			}
			return false;
		}
		
		public function create_order($postData, $infoAccount){
			//same as shipping
			if(isset($postData['same_shipping']) && $postData['same_shipping'] == 1){
				
				$postData['first_name_shipping'] = $postData['first_name'];
				$postData['last_name_shipping'] = $postData['last_name'];
				$postData['address_shipping'] = $postData['address'];
				$postData['phone_shipping'] = $postData['phone'];
				$postData['state_shipping'] = $postData['state'];
				$postData['city_shipping'] = $postData['city'];
				$postData['zipcode_shipping'] = $postData['zipcode'];
				
			}
			
			$has_shipping = false;
			//if shipping create it
			if(isset($postData['delivery_method']) && $postData['delivery_method'] == "Livraison" ||
			   $postData['delivery_method'] == "Shipping"){
				$has_shipping = true;
				
				$insert_delivery = $this->db->query("
					INSERT INTO `tbl_delivery` ( 
						`fk_users`,
						`fk_orders`,
						`first_name`,
						`last_name`,
						`address`,
						`region`,
						`city`,
						`zipcode`,
						`notes`,
						`type`,
						`status`,
						`same_billing`
					) VALUES ( 
						:userid,
						null,
						:first_name,
						:last_name,
						:address,
						:region,
						:city,
						:zipcode,
						'',
						'',
						'',
						:same_shipping
					)
				");
				// var_dump($postData);
				//bind 
				$this->db->bind($insert_delivery, ":userid", (is_array($infoAccount) ? $infoAccount[0]->id : $infoAccount->id));
				$this->db->bind($insert_delivery, ":first_name", $postData['first_name_shipping']);
				$this->db->bind($insert_delivery, ":last_name", $postData['last_name_shipping']);
				$this->db->bind($insert_delivery, ":address", $postData['address_shipping']);
				$this->db->bind($insert_delivery, ":region", $postData['state_shipping']);
				$this->db->bind($insert_delivery, ":city", $postData['city_shipping']);
				$this->db->bind($insert_delivery, ":zipcode", $postData['zipcode_shipping']);
				$this->db->bind($insert_delivery, ":same_shipping", (isset($postData['same_shipping']) && $postData['same_shipping'] == 1 ? 1 : 0));
				
				if($this->db->execute($insert_delivery)){
					//true
					//on recupere l'id
					$query_last_delivery_id = $this->db->query("
						SELECT LAST_INSERT_ID() as lastid FROM tbl_delivery
					");
					$idDelivery = $this->db->single($query_last_delivery_id)->lastid;
					
				}else{
					// echo print_r($insert_delivery->errorInfo()); 
					// die();
				}
				
				
			}
			
			$insert_order = $this->db->query("
				INSERT INTO `tbl_orders` ( 
				`fk_products`,
				`fk_users`,
				`fk_companies`,
				`fk_delivery`,
				`quantity`,
				`tps`,
				`tvq`,
				`shipping`,
				`credit_used`,
				`sub_total`,
				`rebate_percent`,
				`rebate`,
				`rebate_subtotal`,
				`final_price`,
				`amount_to_pay`,
				`status`,
				`paid`,
				`date_created`,
				`date_modified`,
				`date_delivery`,
				`delivery_type`,
				`date_paid`,
				`notes`,
				`txn_id`,
				`paypal_id`,
				`payment_method`,
				`refund_id`,
				`archived`)
				VALUES ( 
				'',
				:userid,
				NULL,
				:fkdelivery,
				:quantity,
				:tps,
				:tvq,
				:shipping,
				0,
				:sub_total,
				:rebate_percent,
				:rebate,
				:rebate_subtotal,
				:final_price,
				:amount_to_pay,
				:status,
				'0', 
				CURRENT_TIMESTAMP,
				CURRENT_TIMESTAMP,
				'',
				:delivery_type,
				'',
				'',
				'',
				'',
				:payment_method,
				'0',
				'0')
			");
			
			//bind
			$this->db->bind($insert_order, ":userid", (is_array($infoAccount) ? $infoAccount[0]->id : $infoAccount->id));
			$this->db->bind($insert_order, ":fkdelivery", ($has_shipping ? (isset($idDelivery) ? $idDelivery : 0) : 0));
			$this->db->bind($insert_order, ":quantity", 1);
			$this->db->bind($insert_order, ":tps", $postData['tps']);
			$this->db->bind($insert_order, ":tvq", $postData['tvq']);
			$this->db->bind($insert_order, ":shipping", $postData['delivery_cost']);
			$this->db->bind($insert_order, ":sub_total", $postData['sub_total']);
			$this->db->bind($insert_order, ":rebate_percent", $postData['rebate_percent']);
			$this->db->bind($insert_order, ":rebate", $postData['rebate']);
			$this->db->bind($insert_order, ":rebate_subtotal", $postData['rebate_subtotal']);
			$this->db->bind($insert_order, ":final_price", $postData['total']);
			$this->db->bind($insert_order, ":amount_to_pay",  $postData['total'] );
			$this->db->bind($insert_order, ":status", "unpaid");
			$this->db->bind($insert_order, ":delivery_type", $postData['delivery_method']);
			$this->db->bind($insert_order, ":payment_method", $postData['payment_method']);
			
			if($this->db->execute($insert_order)){
				
				$current_order_id = $this->db->query("SELECT LAST_INSERT_ID() as 'lastid' FROM tbl_orders");
				$current_order_id = $this->db->single($current_order_id);
				if($current_order_id->lastid !== 0){
					$current_order_id = $current_order_id->lastid;
					$update_solde = $this->db->query("
						UPDATE tbl_users SET solde = (solde - :total) WHERE id = :id
					");
					$this->db->bind($update_solde, ":total", $postData['total']);
					$this->db->bind($update_solde, ":id", (is_array($infoAccount) ? $infoAccount[0]->id : $infoAccount->id));
					$this->db->execute($update_solde);
					
					return $current_order_id;
				}
			}else{
				 echo print_r($insert_order->errorInfo());
				 die();
			}
			
			
		}
		
		public function add_delivery_date($order_id){
			$update_order = $this->db->query("
					UPDATE tbl_orders SET date_delivery =  DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 5 DAY) WHERE id = :id
				");
			$this->db->bind($update_order, ":id", $order_id);
			if($this->db->execute($update_order)){
				return true;
			}else{
				return false;
			}
		}
		
		public function update_products($postData, $order_id){
			
			$str_fk_products = array();
			foreach($postData as $idproduct){
				
				$idproduct = unserialize($idproduct);
				if(!isset($idproduct->is_custom)){
					$idproduct->is_custom = "";
				}
				if(!isset($idproduct->sidemasklogo)){
					$idproduct->sidemasklogo = "";
				}
				$insert_product_order = $this->db->query("
					INSERT INTO `tbl_products_orders` (
						`fk_products`,
						`fk_orders`,
						`quantity`,
						`price`,
						`image`,
						`position`
						)
					VALUES (
						:productid,
						:orderid,
						:quantity,
						:price,
						:image,
						:position
					)
				");
				$this->db->bind($insert_product_order, ":productid", $idproduct->id);
				$this->db->bind($insert_product_order, ":orderid", $order_id);
				$this->db->bind($insert_product_order, ":quantity", $idproduct->cart_quantity);
				$this->db->bind($insert_product_order, ":price", $idproduct->price);
				$this->db->bind($insert_product_order, ":image", $idproduct->is_custom);
				$this->db->bind($insert_product_order, ":position", $idproduct->sidemasklogo);
				
				if($this->db->execute($insert_product_order)){
					
				}
				$query_last_porder = $this->db->query("
					SELECT LAST_INSERT_ID() as `lastid` FROM `tbl_products_orders`
				");
				$last_porder = $this->db->single($query_last_porder)->lastid;
				$str_fk_products[] = $last_porder;
				
				//update inventory
				$query_update_product_inventory = $this->db->query("
					UPDATE tbl_products_inputs SET value = (value - :quantity)
					WHERE type = 20 AND fk_products = :fk_products
				");
				$this->db->bind($query_update_product_inventory, ":quantity", $idproduct->cart_quantity);
				$this->db->bind($query_update_product_inventory, ":fk_products", $idproduct->id);
				
				$this->db->execute($query_update_product_inventory);
				
			}
			
			$fk_products = implode(",", $str_fk_products);
			
			$query_update_order = $this->db->query("
				UPDATE tbl_orders SET fk_products = :fk_products WHERE id = :id
			");
			$this->db->bind($query_update_order, ":fk_products", $fk_products);
			$this->db->bind($query_update_order, ":id", $order_id);
			
			$this->db->execute($query_update_order);
		}
		
		public function getOrder($id){
			$query_update_order = $this->db->query("
				SELECT * FROM tbl_orders WHERE id = :id
			"); 
			$this->db->bind($query_update_order, ":id", $id);
			
			return $this->db->single($query_update_order)->final_price;
		}
	}