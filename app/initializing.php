<?php
	session_save_path(realpath(dirname($_SERVER['DOCUMENT_ROOT']) . '/../tmp'));
	session_start();
	
	require_once "libraries/Database.php";
	/*
	 * Load config
	 */
	require_once "config/config.php"; 
	include_once APPROOT . "/helpers/function.php";
	include_once dirname(APPROOT) . "/public/plugins/mailtest/vendor/autoload.php";
	// include_once dirname(APPROOT)."/public/plugins/vendor/autoload.php";
	
	// Autoload Core librairies
	spl_autoload_register(
		function ($classname) {
			if ($classname !== "PHPMailer")
				require_once "libraries/" . $classname . ".php";
		}
	);
